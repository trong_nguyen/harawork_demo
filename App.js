import React from 'react';
import { Platform, StatusBar, View } from 'react-native';
import { SplashScreen } from 'expo';

import AppComponent from './src';
import { ErrorComponent } from './src/public';

import Configs from './src/Haravan/config';

import Sentry from 'sentry-expo';
Sentry.enableInExpoDevelopment = true;
Sentry.config(Configs.sentry).install();

export default class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      isError: false
    }

    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('light-content');
    }
    SplashScreen.preventAutoHide();
  }

  componentDidCatch(error, info) {
    this.setState({ isError: true }, () => {
      if (!__DEV__) {
        Sentry.captureException(error, { extra: info });
      }
    });
  }

  render() {
    const { isError } = this.state;
    if (isError) {
      return <ErrorComponent />
    }
    return (
      <View style={{ flex: 1 }}>
        <AppComponent />
      </View>
    )
  }
}
