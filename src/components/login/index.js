import  Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';

import Layout from './layout';
import HaravanApi from '../../Haravan/api/hrvapi';
import { HaravanHr } from '../../Haravan';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../state/action';
import Config from '../../Haravan/config'

class Login extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            isError: false,
        }

        //this._handleOpenURL = this._handleOpenURL.bind(this);
    }

    componentDidMount() {
        this.permission();
    }

    async permission() {
        const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS)
        if (existingStatus !== 'granted') {
            await Permissions.askAsync(Permissions.NOTIFICATIONS);
        }
    }

    async login() {
        this.setState({ isLoading: true, isError: false }, async () => {
            let colleague = null;
            const infoLogin =  await HaravanApi.OpenAuth();
            if (infoLogin && infoLogin.userInfo && infoLogin.userInfo.sub) {
                colleague = await HaravanHr.getColleague(infoLogin.userInfo.sub);
                if(colleague && colleague.data && !colleague.error) {
                    colleague = colleague.data;
                }
            }
            if(infoLogin && Config.voteFeaturesOrgs.length > 0){
                const userOrg = infoLogin.userInfo.orgid;
                const index = Config.voteFeaturesOrgs.findIndex(org => org == userOrg)
                if(index > -1){
                    this.props.actions.checkOrgVote(true)
                }
            }
            if (infoLogin && colleague) {
                let getInfo = await this.getInfo(colleague)
                this.props.actions.authHaravan(infoLogin);
                this.props.actions.getColleague(colleague);
                this.props.actions.infoBody(getInfo)
                this.props.navigation.navigate('App');
            } else {
                this.setState({ isError: true, isLoading: false });
            }
        });
    }

    getInfo(collegue){
        let newList = [];
        let listID =[];
        let body= {};
        if(collegue &&  collegue.actingPositions && (collegue.actingPositions.length > 0)){
            collegue.actingPositions.map(item => {
                let content = {departmentId:item.departmentId, jobtitleName:item.jobtitleName, departmentName:item.departmentName, jobtitleId:item.jobtitleId}
                newList.push(content)
            })
        }

        if(collegue && collegue.mainPosition){
            let content = { 
                departmentId: collegue.mainPosition.departmentId, 
                departmentName: collegue.mainPosition.departmentName,
                jobtitleName: collegue.mainPosition.jobtitleName,
                jobtitleId: collegue.mainPosition.jobtitleId
            }
            newList.push(content)
        }

        if(newList && (newList.length > 0)){
            newList.map(async (item) =>{
                listID.push(item.departmentId)
                let result = await HaravanHr.getDepartmantID(item.departmentId)
                if(result && result.data && !result.error){
                    if(result.data && result.data.paths && (result.data.paths.length > 0)){
                        result.data.paths.map(e =>{
                            listID.push(e.id)
                        })
                    }
                }
            })
        }
        body={
            departmentIds:listID,
            positionInfos:newList,
        }
        return body
    }

}

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);