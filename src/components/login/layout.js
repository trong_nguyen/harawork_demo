import React, { Component } from 'react';
import { View, Text, ImageBackground, ActivityIndicator, ScrollView } from 'react-native';
import { settingApp, Logo, imgApp, ButtonCustom } from '../../public';
import { Feather } from '@expo/vector-icons';
import * as Animatable from 'react-native-animatable';
import Configs from '../../Haravan/config';
import ConfigsPublic from '../../public/config';
import Collapsible from 'react-native-collapsible';

class Layout extends Component {

    renderViewLogin() {
        const { isLoading } = this.state;
        let renderIcon = <Feather name="arrow-right" size={23} color="#ffffff" />;
        if (isLoading) {
            renderIcon = (
                <ActivityIndicator size='small' color={settingApp.color} />
            )
        }
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1, backgroundColor: 'transparent' }} />
                <Collapsible collapsed={!this.state.isError}>
                    <ScrollView style={{ marginTop: 30, padding: 20 }} scrollEnabled={false}>
                        <View style={{ flex: 1, backgroundColor: '#FCF0ED', flexDirection: 'row' }}>
                            <View style={{ width: 5, backgroundColor: '#D9534F' }} />
                            <View style={{ flex: 1, padding: 20, backgroundColor: 'transparent' }}>
                                <Text style={{ fontSize: 16, color: '#474747' }}>
                                    Đăng nhập không thành công. Vui lòng thử lại sau.
                                </Text>
                            </View>
                        </View>
                    </ScrollView>
                </Collapsible>
                <Animatable.View
                    animation='fadeIn'
                    delay={1000}
                    style={{ flex: 1.5, backgroundColor: 'transparent', alignItems: 'center' }} >
                    <ButtonCustom
                        onPress={() => isLoading ? null : this.login()}
                        disabled={isLoading}
                        style={{
                            width: (settingApp.width - 40),
                            height: 60,
                            backgroundColor: isLoading ? settingApp.colorDisable : '#29bc94',
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: 5,
                            flexDirection: 'row'
                        }}>
                        <Text style={{ color: '#ffffff', fontSize: 16, fontWeight: '500', marginRight: 5 }}>
                            Đăng nhập
                        </Text>
                        {renderIcon}
                    </ButtonCustom>
                </Animatable.View>
                <Animatable.View
                    animation='fadeIn'
                    style={{ width: settingApp.width, height: 32, alignItems: 'center' }}>
                    <Text style={{ color: '#ecf0f1', fontSize: 10 }}>
                        Version {Configs.version}
                    </Text>
                </Animatable.View>
            </View>
        )
    }

    render() {
        return (
            <ImageBackground
                source={imgApp.background}
                style={{ flex: 1, width: undefined, height: undefined, backgroundColor: ConfigsPublic.color }}
                resizeMode='cover'
            >
                {this.renderViewLogin()}
            </ImageBackground>
        )
    }
}

export default Layout;


