import React, { Component } from 'react';
import { Image,View } from 'react-native';
import * as styles from './component/style'

import { createMaterialTopTabNavigator } from 'react-navigation';
import { settingApp, imgApp, IconTabNotCheckIn, SvgCP } from '../../public';
import Home from '../../components/app/home'
import CheckIn from '../../components/app/checkinUser/checkIn'
import News from '../../components/app/discuss/news'
import Profile from '../../components/app/profile'
import Contact from '../../components/app/user/contact/listContact'

const Layout = createMaterialTopTabNavigator(
    {
        HomeTab:{
            screen: Home,
            navigationOptions:{
                    tabBarLabel: '',
                    tabBarIcon: ({ focused }) => <SvgCP.drawerHome focused={focused}/>
            }
        },
        ContactTab: {
            screen: Contact,
            navigationOptions: {
                tabBarLabel: '',
                tabBarIcon: ({ focused }) => <SvgCP.drawerContact focused={focused}/>
            }
        },
        CheckingTab: {
            screen: CheckIn,
            navigationOptions: {
                tabBarLabel: '',
                tabBarIcon: ({ focused }) => 
                    <View style={styles.styles.buttonMid}>
                        <SvgCP.checkIn focused={focused}/>
                    </View>
            }
        },
        NewsTab: {
            screen: News,
            navigationOptions: {
                tabBarLabel: '',
                tabBarIcon: ({ focused }) => (
                    <SvgCP.drawerDisscus focused={focused}/>
                )
            }
        },
        ProfileTab: {
            screen: Profile,
            navigationOptions: {
                tabBarLabel: '',
                tabBarIcon: ({ focused }) => (
                    <SvgCP.drawerNotifyList focused={focused}/>
                )
            }
        }
    }, {
        animationEnabled:false,
        tabBarPosition:'bottom',
        swipeEnabled: false,
        tabBarOptions: {
            showIcon:true,
            showLabel: false,
            activeTintColor: '#0A1F44',
            inactiveTintColor: '#C7C7C7',
            labelStyle: {
                fontSize: 12,
                fontWeight:'bold'
            },
            style: {
                height:((settingApp.isIPhoneX == true) ? 70 : 60),
                backgroundColor: '#FDFBFB',
                ...settingApp.shadow
            },
            indicatorStyle: {
                borderBottomColor:'#FDFBFB',
                borderBottomWidth:4,
            },
            upperCaseLabel:false
        },
       
    },
    {
        headerMode: 'none'
    }
);

export default Layout;