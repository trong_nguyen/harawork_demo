import { StyleSheet } from 'react-native';
import { settingApp } from '../../../public'

const styles = StyleSheet.create({
    buttonMid:{
        width:40,
        height:40,
        borderRadius:20,
        borderColor:settingApp.color,
        borderWidth:2,
        marginBottom:20,
        position:'absolute',
        justifyContent:'center',
        alignItems:'center'
    }
})

export {
    styles
}