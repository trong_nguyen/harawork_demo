import React, { Component } from 'react';
import { ImageBackground, StatusBar } from 'react-native';
import { imgApp } from '../../public';
import Configs from '../../public/config';

class Layout extends Component {

    render() {
        return (
            <ImageBackground
                source={imgApp.background}
                style={{ flex: 1, width: undefined, height: undefined, backgroundColor: Configs.color }}
                resizeMode='cover'
            >
                <StatusBar barStyle="default" />
            </ImageBackground>
        );
    }
}

export default Layout;