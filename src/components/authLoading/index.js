import Layout from './layout';
import { AsyncStorage } from 'react-native';
import { SplashScreen } from 'expo';
import { keyStore } from '../../public';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../state/action';
import HaravanApi from '../../Haravan/api/hrvapi';
import { HaravanHr }  from '../../Haravan'
import Config from '../../Haravan/config'

class AuthLoading extends Layout {

    constructor(props) {
        super(props);

        this.bootstrapAsync();
    }

    bootstrapAsync = async () => {
        let infoLogin = await HaravanApi.GetAuthInfo();
        if (infoLogin) {
            await this.initialData();
            if(Config.voteFeaturesOrgs.length > 0){
                const userOrg = infoLogin.userInfo.orgid;
                const index = Config.voteFeaturesOrgs.findIndex(org => org == userOrg)
                if(index > -1){
                    this.props.actions.checkOrgVote(true)
                }
            }
            this.props.actions.authHaravan(infoLogin);
            this.props.navigation.navigate('Content');
        } else {
            this.props.navigation.navigate('Login');
        }
        await SplashScreen.hide();
    };

    async initialData() {
        let colleague = await AsyncStorage.getItem(keyStore.colleague);
        if (colleague) {
            colleague = JSON.parse(colleague);
            let getInfo = await this.getInfo(colleague)
            this.props.actions.getColleague(colleague);
            this.props.actions.infoBody(getInfo)
        }
        let listNotify = await AsyncStorage.getItem(keyStore.listNotify);
        let listMail = await AsyncStorage.getItem(keyStore.listMail)
        if (listNotify) {
            listNotify = JSON.parse(listNotify);
            this.props.actions.notify.listNotify(listNotify);
        }
        if (listMail) {
            listMail = JSON.parse(listMail);
            this.props.actions.mail.listMail(listMail)
        }

        for (const keyList of keyStore.listBPM) {
            let list = await AsyncStorage.getItem(keyList);
            if (list) {
                list = JSON.parse(list);
                this.props.actions.bpm.listBPM(list, keyList);
            }
        }
    }

    getInfo(collegue){
        let newList = [];
        let listID =[];
        let body= {};
        if(collegue &&  collegue.actingPositions && (collegue.actingPositions.length > 0)){
            collegue.actingPositions.map(item => {
                let content = {departmentId:item.departmentId, jobtitleName:item.jobtitleName, departmentName:item.departmentName, jobtitleId:item.jobtitleId}
                newList.push(content)
            })
        }

        if(collegue && collegue.mainPosition){
            let content = { 
                departmentId: collegue.mainPosition.departmentId, 
                departmentName: collegue.mainPosition.departmentName,
                jobtitleName: collegue.mainPosition.jobtitleName,
                jobtitleId: collegue.mainPosition.jobtitleId
            }
            newList.push(content)
        }

        if(newList && (newList.length > 0)){
            newList.map(async (item) =>{
                listID.push(item.departmentId)
                let result = await HaravanHr.getDepartmantID(item.departmentId)
                if(result && result.data && !result.error){
                    if(result.data && result.data.paths && (result.data.paths.length > 0)){
                        result.data.paths.map(e =>{
                            listID.push(e.id)
                        })
                    }
                }
            })
        }
        body={
            departmentIds:listID,
            positionInfos:newList,
        }
        return body
    }
    
}

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(mapStateToProps, mapDispatchToProps)(AuthLoading);