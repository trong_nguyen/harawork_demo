import React, { Component } from 'react';
import { View, ScrollView, Platform, Text, StatusBar } from 'react-native';
import { HeaderIndex, settingApp } from '../../../public';

import Banner from './components/banner';
import CountWork from './components/countWork';
import ListTask from './components/listTask';
import ListNews from './components/listNews';
import ListBrithDay  from './components/listBrithDay';
//rgba(0, 0, 0, 0.8)
class Layout extends Component {
    renderHEader(){
        let {  statusBarHeight, width } = settingApp;
        statusBarHeight = (Platform.OS === 'android') ? (statusBarHeight) : statusBarHeight;
        return(
            <View style={{
                backgroundColor:'#FFFFFF',
            }}> 
                <StatusBar barStyle={'dark-content'}/>
                <View style={{ width, height: statusBarHeight, backgroundColor:'#FFFFFF' }} />
            </View>
        )
    }

    renderTopView(){
        return(
            <View style={{width:settingApp.width, height:60, backgroundColor:'#FFFFFF', }}>
                <View style={{flex:1, justifyContent:'center', paddingLeft:15,}}>
                    <Text style={{fontSize:18, fontWeight:'bold', color:settingApp.colorText, marginBottom:10}}>ban360</Text>
                    <Text style={{fontSize:16,  color:settingApp.colorText}}>UserName</Text>
                </View>
            </View>
        )
    }

    render() {
        return (
            <View style={{ flex: 1, }}>
                {this.renderHEader()}
                <View style={{ flex: 1 }}>
                {this.renderTopView()}
                    <ScrollView>
                        <Banner />
                        <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                            <CountWork infoUser={this.props.app.colleague} />
                            <ListTask dataTask={this.state.dataTask} />
                        </View>
                        <View style={{paddingBottom:10, paddingTop:10}}>
                            <ListNews navigation={this.props.navigation}/>
                        </View>
                        {/* <View style={{paddingBottom:10, paddingTop:10}}>
                            <ListBrithDay />
                        </View> */}
                        
                        <View style={{ flex: 1, height: 20 }}/>
                    </ScrollView>
                </View>
            </View>
        )
    }
}

export default Layout;