import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { settingApp } from '../../../../public';
import { Ionicons } from '@expo/vector-icons';

class CountWork extends Component {


    sayHi() {
        var d = new Date();
        var n = d.getHours();
        let title = '';
        switch (true) {
            case (0 <= n && n < 12):
                title = 'Chào buổi sáng'
                break;
            case (12 <= n && n < 18):
                title = 'Chào buổi chiều'
                break;
            default:
                title = 'Chào buổi tối'
                break
        }
        return title;
    }

    render() {
        const sayHi = this.sayHi();
        const { fullName, error, message } = this.props.infoUser;
        let content = <View />;
        if (fullName && fullName.hasOwnProperty('length') && fullName.length > 0) {
            content = (
                <View style={{ flex: 1, height: 50, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontSize: 14, color: settingApp.colorText }}>
                        {sayHi},<Text style={{ fontWeight: 'bold' }}> {fullName}!</Text>
                    </Text>
                </View>
            )
        } else if (error && message.hasOwnProperty('length') && message.length > 0) {
            content = (
                <View style={{ flex: 1, flexDirection: 'row', height: 50, marginTop: 10 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', paddingLeft: 5, paddingRight: 5 }}>
                        <Ionicons name='ios-warning' color='#fd7e14' size={23} />
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ color: settingApp.colorText, fontSize: 14, textAlign: 'center' }}>
                            {message}
                        </Text>
                    </View>
                </View>
            )
        }
        return (
            <View style={{ flex: 1 }}>
                {content}
            </View>
        )
    }
}

export default CountWork;