import React, { Component } from 'react';
import { View } from 'react-native';
import { settingApp, imgApp, AsyncImage } from '../../../../public';

class Banner extends Component {

    render() {
        return (
            <View style={{ flex: 1, height: settingApp.imageSize }}>
                <AsyncImage
                    source={imgApp.banner}
                    style={{ flex: 1, width: undefined, height: undefined }}
                    resizeMode='contain'
                />
            </View>
        )
    }
}

export default Banner;