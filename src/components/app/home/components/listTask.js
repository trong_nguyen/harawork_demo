import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';
import { settingApp } from '../../../../public';
import Ticker, { Tick } from "react-native-ticker";
import { LinearGradient } from 'expo-linear-gradient';

class ListTask extends Component {

    render() {
        const { dataTask } = this.props;
        return (
            <View style={{ flex: 1 }}>
                <Text style={{ fontSize: 15, color: settingApp.colorText, fontWeight: '500', marginBottom: 15 }}>
                    Công việc
                </Text>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <FlatList
                        data={dataTask}
                        extraData={this.props}
                        keyExtractor={(item, index) => `${index}`}
                        numColumns={2}
                        renderItem={obj => {
                            const { item, index } = obj;
                            const margin = (index % 2) === 0 ? 16 : 0;
                            return (
                                <View style={{ flex: 1, height: 75, marginRight: margin, marginBottom: margin, ...settingApp.shadow }}>
                                    <View
                                        //colors={item.colors}
                                        style={{ flex: 1, justifyContent: 'center', backgroundColor:'#FFFFFF', paddingLeft:20, ...settingApp.shadow ,borderRadius:5 }}>
                                        <Ticker text={item.count.toString()}
                                            textStyle={{ fontSize: 24, color:item.colorsText, fontWeight: '500' }}
                                            rotateTime={500}
                                        />
                                        <Text style={{ color: item.colorsText , fontSize: 14, fontWeight: '500' }}>{item.label}</Text>
                                    </View>
                                </View>
                            )
                        }}
                    />
                    {/* {dataTask.map((e, i) => (
                        <View
                            key={i}
                            style={{
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderLeftColor: settingApp.colorSperator,
                                borderLeftWidth: i != 0 ? 1 : 0
                            }}>
                            <View style={{ marginBottom: 10 }}>
                                <Ticker text={e.count.toString()}
                                    textStyle={{ fontSize: 30, color: e.color, fontWeight: 'bold' }}
                                    rotateTime={500}
                                />
                            </View>
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <Text style={{ color: settingApp.colorText, fontSize: 12, textAlign: 'center' }}>{e.label}</Text>
                            </View>
                        </View>
                    ))} */}
                </View>
            </View>
        )
    }
}

export default ListTask;