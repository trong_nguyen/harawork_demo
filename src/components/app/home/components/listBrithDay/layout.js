import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';
import { settingApp } from '../../../../../public';
import Item from './components/item';
import ItemLoad from './components/itemLoad';

class Layout extends Component {

    loadMore(){
        if (this.page <= this.totalPage) {
            this.loadData();
        }
    }

    render() {
        const { list } = this.state;
        let content = <View />;
        if (list && list.length > 0) {
            content = (
                <FlatList
                    data={this.state.list}
                    extraData={this.state}
                    keyExtractor={(item, index) => (item.id +'') || (index+'') }
                    horizontal={true}
                    style={{ paddingLeft: 10 }}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    renderItem={({ item }) => <Item item={item} />}
                    onEndReachedThreshold={5}
                    onEndReached={() => this.loadMore()}
                />
            )
        } else {
            content = (
                <FlatList
                    data={this.state.listLoad}
                    extraData={this.state}
                    keyExtractor={(item, index) =>  (item.id +'') || (index+'')}
                    horizontal={true}
                    style={{ paddingLeft: 10 }}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    renderItem={({ item }) => <ItemLoad item={item} />}
                />
            )
        }
        return (
            <View style={{ flex: 1, marginTop: 15 }}>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', paddingRight: 10, paddingLeft: 10, marginBottom: 15 }}>
                    <Text style={{ fontSize: 15, color: settingApp.colorText, fontWeight: '500' }}>
                        Sinh nhật nhân viên
                    </Text>
                </View>
                {content}
            </View>
        )
    }
}

export default Layout;