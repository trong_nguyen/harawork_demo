import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Avatar, Utils } from '../../../../../../public';

class ItemLoad extends Component {

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return false;
    }

    render() {
        return (
            <View style={{ width: 90, alignItems: 'center', marginRight: 10 }}>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Avatar userInfo={{ name: null, picture: null }} v={80} />
                </View>
                <View style={{ height: 60 }} />
            </View>
        )
    }
}

export default ItemLoad;