import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Avatar, Utils, ModalUserInfo , AvatarCustom} from '../../../../../../public';

class Item extends Component {
    constructor(props) {
        super(props);

        this.state = {
            visibleModal: false
        }
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (nextState.visibleModal !== this.state.visibleModal) {
            return true;
        }
        return false
    }


    render() {
        const { item } = this.props;
        let soure  =  Utils.getLinkImage(item.haraId)
        return (
            <View>
                <TouchableOpacity
                    onPress={() => this.setState({ visibleModal: true })}
                    style={{ width: 90, alignItems: 'center', marginRight: 10 }}>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <AvatarCustom userInfo={{ name: item.fullName, picture: soure }} v={80} />
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', marginTop: 10 }}>
                        <Text style={{ fontSize: 13, color: '#000' }}>
                            {Utils.formatLocalTime(item.birthday, 'DD/MM')}
                        </Text>
                        <Text style={{ fontSize: 13, fontWeight: '500', color: '#000', textAlign: 'center', marginTop: 5 }}>
                            {item.fullName}
                        </Text>
                    </View>
                </TouchableOpacity>

                {this.state.visibleModal && <ModalUserInfo
                    visibleModal={this.state.visibleModal}
                    onClose={() => this.setState({ visibleModal: false })}
                    item={item}
                    imageUser={soure}
                />}
            </View>
        )
    }
}

export default Item;