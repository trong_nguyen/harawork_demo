import Layout from './layout';
import { HaravanHr } from '../../../../../Haravan';
import { Utils } from '../../../../../public';

class ListBrithDay extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            list: [],
            listLoad: [
                { id: 0, value: null }, 
                { id: 1, value: null },
                { id: 2, value: null },
                { id: 3, value: null },
                { id: 4, value: null },
                { id: 5, value: null }
            ]
        }
        this.page = 1
        this.totalPage = null
        this.minNum = 20;
        this.fromDate = Utils.formatLocalTime(new Date, 'YYYY/MM/DD');
        this.toDate = null;
    }

    async componentDidMount() {
        this.loadData()
    }

    async loadData(){
        const { list } = this.state
        const { page, totalPage} = this
        let newList = list
        if (!totalPage || (totalPage && !isNaN(totalPage) && page <= totalPage)) {
            const result = await HaravanHr.listBirthday(page)
            if (result && result.data && !result.error) {
                this.page = page + 1;
                this.totalPage = Math.ceil(result.data.totalCount / 20);
                result.data.data.map(item =>{
                    const index = newList.findIndex(m => m.id == item.id)
                    if(index < 0){
                        newList.push(item)
                    }   
                })
                this.setState({ list: newList})
            } 
            else{
                this.setState({
                    list:newList
                })
            }
        }
        else{
            this.setState({
                list:[]
            })
        }

    }

}

export default ListBrithDay;