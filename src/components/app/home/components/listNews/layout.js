import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import { settingApp } from '../../../../../public';
import Item from './components/item';
import ItemLoad from './components/itemLoad';
import * as Animatable from 'react-native-animatable';

class Layout extends Component {

    render() {
        const { list } = this.state;
        let content = <View />;
        let more = false;
        if (list && list.length > 0) {
            more = true;
            content = (
                    <FlatList
                        data={this.state.list}
                        extraData={this.state}
                        keyExtractor={(item, index) => `${item.id}`}
                        horizontal={true}
                        style={{ paddingLeft: 10 }}
                        ListFooterComponent={<View style={{ height: 390, width: 10 }} />}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        onEndReached={() => this.loadData()}
                        onEndReachedThreshold={1}  
                        renderItem={({ item }) => <Item 
                                item={item} navigation={this.props.navigation} 
                                delete={(item) => this.delete(item)}
                                />}
                    />
            )
        } else {
            content = (
                        <FlatList
                            data={this.state.listLoad}
                            extraData={this.state}
                            keyExtractor={(item, index) => `${item.id}`}
                            horizontal={true}
                            style={{ paddingLeft: 10 }}
                            ListFooterComponent={<View style={{ height: 390, width: 10 }} />}
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false}
                            renderItem={({ item }) => <ItemLoad item={item} navigation={this.props.navigation} />}
                        />
            )
        }
        return (
            <View style={{ flex: 1, marginTop: 10 }}>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', paddingRight: 10, paddingLeft: 10 }}>
                    <Text style={{ fontSize: 15, color: settingApp.colorText, fontWeight: '500' }}>
                        Bảng tin
                        </Text>
                    {more && <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('NewList')}
                        style={{ paddingLeft: 10 }}>
                        <Animatable.Text
                            delay={350}
                            animation='fadeInRight'
                            style={{ fontSize: 14, color: '#157BC5' }}>
                            Xem tất cả
                            </Animatable.Text>
                    </TouchableOpacity>}
                </View>
                {content}
            </View>
        )

    }

}

export default Layout;