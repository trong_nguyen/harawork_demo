import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { settingApp, ButtonCustom, Utils, AsyncImage } from '../../../../../../public';
import { LinearGradient } from 'expo-linear-gradient';

class Item extends Component {

    shouldComponentUpdate() {
        return false;
    }

    render() {
        const { item } = this.props;
        return (
            <ButtonCustom
                onPress={() => this.props.navigation.navigate('DetailComment', { 
                    data: item,
                    delete:(value) => this.props.delete(value)
                    })}
                style={{
                    width: (settingApp.width * 0.5),
                    height: 250,
                    ...settingApp.shadow,
                    backgroundColor: '#ffffff',
                    marginBottom: 10,
                    marginTop: 10,
                    marginRight: 15,
                    borderRadius: 8
                }}>
                    <LinearGradient
                        colors={['transparent', '#000000']}
                        style={{flex:1, position:'absolute', top:0, left:0, right:0, bottom:0, zIndex:888, borderRadius:8}}
                    >
                    </LinearGradient>
                    <LinearGradient 
                        colors={['transparent', '#000000']}
                        style={{ flex: 1 , width: (settingApp.width * 0.5), height: 200, borderRadius:8,}}>
                        <AsyncImage
                            source={{ uri: item.banner }}
                            style={{ flex: 1, width: undefined, height: undefined }}
                        />
                    </LinearGradient>
                    <View style={{ flexWrap: "wrap", overflow: 'hidden' , position:'absolute', bottom:15, left:5, zIndex:999 }}>
                        <Text
                            numberOfLines={2}
                            style={{ fontSize: 15, color: '#FFFFFF', fontWeight:'600' , width:(settingApp.width * 0.4)}}>
                            {item.description}
                        </Text>
                    </View>
                
            </ButtonCustom>
        )
    }
}

export default Item;