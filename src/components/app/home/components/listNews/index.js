import Layout from './layout';
import { HaravanIc, HaravanHr } from '../../../../../Haravan';

class ListNew extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            list: [],
            listLoad: [{ id: 0, value: null }, { id: 1, value: null }]
        }

        this.page = 1;
        this.pageSize = 10;
        this.totalPage = null;
    }

    componentDidMount() {
        this.loadData();
    }

    delete(value){
        let { list } = this.state;
        this.setState({list})
        if(value.status == true){
            const index = list.findIndex(m => (m.id == value.data.thread.id))
            if(index > -1){
                list.splice(index, 1)
                this.setState({list})
            }
        }
    }

    async loadData() {
        if (!this.totalPage || (this.totalPage >= 1 && (this.page <= this.totalPage))) {
            const { page, pageSize } = this;
            const { list } = this.state;
            const result = await HaravanIc.list_pinhome(page, pageSize);
            if (result && result.data && !result.error) {
                let data = [...list];
                for (let create of result.data.data) {
                    let createInfo = await HaravanHr.getColleague(create.createdBy);
                    if (createInfo && createInfo.data && !createInfo.error) {
                        data.push({ ...create, fullName: createInfo.data.fullName });
                    } else {
                        data.push({ ...create, fullName: '--' });
                    }
                }
                const { totalCount } = result.data;
                const countPage = Math.ceil(totalCount / pageSize);
                this.totalPage = countPage;
                this.page = page + 1;
                this.setState({ list: data });
            }
        }
    }

}

export default ListNew;