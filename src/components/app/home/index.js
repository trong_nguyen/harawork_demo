import Layout from './layout';

import { HaravanHr, HaravanIc, HaravanTask } from '../../../Haravan';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../state/action';
import Geolocation from 'react-native-geolocation-service';

class Home extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            dataTask: [
                { count: 0, colors: ['#21469B', '#09266A'], label: 'Cần thực hiện', colorsText: '#21469B'},
                { count: 0, colors: ['#5AC8FA', '#179FDC'], label: 'Hôm nay', colorsText: '#5AC8FA' },
                { count: 0, colors: ['#FF3B30', '#ED0000'], label: 'Đang trễ' , colorsText: '#FF3B30'},
                { count: 0, colors: ['#FFCC00', '#FFA800'], label: 'Cần duyệt', colorsText: '#FFCC00' }
            ]
        }
        HaravanIc.registerForPushNotificationsAsync();
    }

    async componentDidMount() {
        // await Utils.permission();
        const checkInStatus = await HaravanHr.getCheckInStatus();
        const timeWorkInfo = await HaravanHr.getconfigtimeworkinginfo();
        if (checkInStatus && checkInStatus.data && !checkInStatus.error) {
            this.props.actions.checkIn.getCheckInStatus(checkInStatus.data);
        } else if (checkInStatus && checkInStatus.message && checkInStatus.error) {
            const data = !!(timeWorkInfo && timeWorkInfo.data) ? timeWorkInfo.data : null;
            this.props.actions.checkIn.getCheckInStatus({
                ...data,
                error: true,
                message: checkInStatus.message
            });
        }

        let { dataTask } = this.state;
        const result = await HaravanTask.getTaks();
        if (result && result.data) {
            const { taskcountbydates } = result.data;
            if (taskcountbydates && taskcountbydates.length > 0) {
                dataTask = [
                    { ...dataTask[0], ...taskcountbydates[taskcountbydates.findIndex(e => e.name == 'Todo')] },
                    { ...dataTask[1], ...taskcountbydates[taskcountbydates.findIndex(e => e.name == 'Today')] },
                    { ...dataTask[2], ...taskcountbydates[taskcountbydates.findIndex(e => e.name == 'Late')] },
                    { ...dataTask[3], ...taskcountbydates[taskcountbydates.findIndex(e => e.name == 'NeedApproval')] }
                ];
            }
            this.setState({ dataTask });
        }
    }
}


const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);