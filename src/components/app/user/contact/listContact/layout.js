import React, { Component } from 'react';
import { View, Text , StyleSheet, TextInput, Keyboard, TouchableOpacity, FlatList, ActivityIndicator} from 'react-native';
import { HeaderNewStyles , settingApp, SvgCP, Loading, iconGroups} from '../../../../../public';
import { EvilIcons, AntDesign } from '@expo/vector-icons'
import lodash from 'lodash';
import Item from './component/item';
import * as Animatable from 'react-native-animatable';

class Layout extends Component{

    renderHeader(){
        return(
            <HeaderNewStyles
                title = {
                    <View style={styles.header}>
                        <Text style={{fontSize:20, color:(settingApp.colorText), fontWeight:'bold'}}>Danh bạ</Text>
                    </View>
                }
            />
        )
    }

    renderSearch(){
        let onChangeCallback = lodash.debounce(this.onSearchBox, 500)
        const { text } = this.state
        return(
            <View 
                style={styles.coverSearch}
                >
                <View
                    style={styles.searchBox}
                    >
                    <EvilIcons name="search" size={25} color={settingApp.colorDisable} />
                    <TextInput
                        ref = {refs => this.TextInput = refs}
                        style={styles.textInput}
                        placeholder='Nhập họ tên, SDT, MNV, email'
                        //autoFocus={true}
                        onChangeText={onChangeCallback.bind(this)}
                        multiline={false}
                    />

                {(text.length > 0) ? <TouchableOpacity
                    onPress={() => this.clearSearch()}
                    style={[styles.buttonFillter]}>
                    <AntDesign name='closecircle' size={15} color={settingApp.colorDisable} />
                </TouchableOpacity>:  <View style={styles.buttonFillter}/>}
                </View>
                <TouchableOpacity
                    onPress={() => this.setState({isShow:!this.state.isShow})}
                    style={[styles.buttonFillter]}>
                    <SvgCP.iconOptionContact focus={this.state.isShow}/>
                </TouchableOpacity>
            </View>
        )
    }

    renderListContact(){
        const { isLoadmore,  isSearching, isShow, disable, listContact, text} = this.state
        let mess = (listContact.length >= 20) ? `Hệ thống chỉ hiển thị 20 kết quả tìm kiếm để bảo mật thông tin` : 
            `Có ${listContact.length} kết quả cho "${text}"`
        if((isShow==true) && (disable==true)){
            return(
                <View style={styles.coverTextNoneSearch}>
                    <Text style={styles.textNoneSearch}>{`Vui lòng nhập tìm kiếm\n để sử dụng bộ lọc`}</Text>
                </View>
            )
        }
        else{
            return(
                <View style={{flex:1}}>
                <View style={styles.alerDisable}>
                    <Text style={styles.alertTextDis}>{mess}</Text>
                </View>
                
                <FlatList 
                    onScroll={() => Keyboard.dismiss()}
                    data={this.state.listContact}
                    extraData={this.state}
                    keyExtractor={(item,index) => item.id ? (item.id+'') : (''+index)}
                    renderItem={(obj) => this._renderItem(obj)}
                    ListFooterComponent={<View style={{width:settingApp.width, height:44, backgroundColor:'transparent'}}/>}
                />
                {isSearching && <Loading/>}
                </View> 
            )
        }
    }

    _renderItem(obj){
        const {item, index} = obj
        return(
            <Item
                item={item}
                index={index}
                navigation = {this.props.navigation}
            />
        )
    }

    renderLoadmore(){
        return(
            <View style={styles.loadMore}>
                <ActivityIndicator color={settingApp.color} size={"small"}/>
            </View>
        )
    }

    renderOption(){
        const { titleDepart, titleJob,disable, listContact, disOption } = this.state;
        let titleDepar = titleDepart ?titleDepart : 'Chọn đơn vị';
        let titleJobtitle =  titleJob ? titleJob : 'Chọn chức danh';
        return(
            <Animatable.View 
                animation="flipInX"
                duration={100}
                style={styles.option}>
                <TouchableOpacity 
                    disabled={disOption}
                    onPress={() => this.goOption(1)}
                    style={[styles.buttonOption,{borderRightWidth:1, borderRightColor:settingApp.colorSperator,opacity:disOption ? 0.4 : 1}]}>
                    <Text 
                        numberOfLines={1}
                        style={styles.textOption}>{titleDepar}</Text>
                    <AntDesign name='down' size={15} color={settingApp.colorDisable} />
                </TouchableOpacity>

                <TouchableOpacity 
                    disabled={disOption}
                    onPress={() => this.goOption(2)}
                    style={[styles.buttonOption,{opacity:disOption ? 0.4 : 1}]} >
                    <Text 
                        numberOfLines={1}
                        style={styles.textOption}>{titleJobtitle}</Text>
                    <AntDesign name='down' size={15} color={settingApp.colorDisable} />
                </TouchableOpacity>
            </Animatable.View>
        )
    }

    render(){
        const { listContact, isShow , isSearching, disable } = this.state
        return(
            <View style={styles.main}>
                <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => Keyboard.dismiss()}
                    style={{flex:1}}
                >
                    {this.renderHeader()}
                    {this.renderSearch()}
                    {isShow && this.renderOption()}
                    {(listContact && (listContact.length > 0) && !isSearching) ? this.renderListContact() : this.renderLoad()}
                </TouchableOpacity>
            </View>
        )
    }

    renderLoad(){
        const { listContact, isSearching, isLoading, disable , isShow, text} = this.state
         if(!isSearching && (listContact.length ==0) && !isLoading){
            return(
                <View style={{justifyContent:'center', alignItems:'center', marginTop:30}}>
                    <iconGroups.ArtWorks />
                    <Text style={{fontSize:16, color:settingApp.colorText, marginTop:10}}>Không tìm thấy nhân viên</Text>
                </View>
            )
        }
        else{
            return(
                <Loading/>
            )
        }
    }
}

const styles = StyleSheet.create({
    main:{
        flex:1, 
        backgroundColor:'#FFFFFF', 
        alignItems:'center'
    },
    header:{
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center', 
        flexDirection: 'row' 
    },
    coverSearch:{
        height:55,
        alignItems:'center', 
        flexDirection:'row', 
        justifyContent:'center', 
        borderBottomColor:settingApp.colorSperator, 
        borderBottomWidth:1,
        width:settingApp.width,
        backgroundColor:'#FFFFFF'
    },
    searchBox:{
        width:settingApp.width - 64,
        paddingLeft:10,
        paddingRight:10,
        borderRadius:10,
        backgroundColor:'rgba(142, 142, 147, 0.12)',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
    },
    buttonFillter:{
        width:40, 
        height:40, 
        justifyContent:'center', 
        alignItems:'center'
    },
    textInput:{
        height: 40,
        width:settingApp.width -140,
        fontSize:16
    },
    loadMore:{
        width:settingApp.width,
        height:60,
        justifyContent:'center',
        alignItems:'center'
    },
    option:{
        flexDirection:'row', 
        width:settingApp.width, 
        height:44, 
        borderBottomColor:settingApp.colorSperator, 
        borderBottomWidth:1
    },
    buttonOption:{
        flexDirection:'row', 
        justifyContent:'space-between', 
        alignItems:'center',
        paddingLeft:20, 
        paddingRight:17,
        width:(settingApp.width/2)
    },
    textOption:{
        fontSize:16,
        color:settingApp.colorText
    },
    alerDisable:{
        width:settingApp.width, 
        minHeight:40, 
        justifyContent:'center', 
        alignItems:'center', 
        padding:10
    },
    alertTextDis:{
        fontSize:16, 
        color:settingApp.blueSky, 
        fontWeight:'600', 
        textAlign:'center'
    },
    coverTextNoneSearch:{
        flex:1, 
        backgroundColor:'#FFFFFF',
        alignItems:'center', 
        marginTop:30
    },
    textNoneSearch:{
        fontSize:16, 
        color:settingApp.colorDisable, 
        textAlign:'center',
    }
})
export default Layout;