import Layout from './layout';
import { HaravanHr } from '../../../../../Haravan'

class Contact extends Layout{
    constructor(props){
        super(props)
        this.state={
            isLoading:true,
            listContact:[],
            isLoadmore:false,
            isShow:false,
            text:'',
            isSearching:false,
            listOption:[],
            departmentId:null,
            jobId:null,
            itemDepart:null,
            itemJob:null,
            titleDepart:null,
            titleJob:null,
            disable:true,
            disOption:true
        }
        this.first = true
        this.page = 1;
        this.totalPage = null;
        this.statusActive = 1;
        this.currentList = [];
        this.text = ''
    }

    componentDidMount(){
        this.loadData()
    }

    clearSearch(text){
        text = text || ''
        this.currentList=[]
        this.TextInput.clear()
        this.setState({disable:true,
            isShow:false,
            text:'', 
            isSearching:true, 
            itemDepart:null,
            itemJob:null,
            departmentId:null, 
            jobtitleId:null, 
            jobId:null,
            titleDepart:null, 
            disOption:true,
            titleJob:null},async() =>{
                await this.refreshList()
                await this.loadData('')})
    }

    goOption(value){
        const { itemDepart, itemJob } = this.state
        this.props.navigation.navigate('ListOption', {
                data:this.currentList, 
                value:value,
                actions:params => this.getParams(params),
                item:((value ==1) ? itemDepart : itemJob),
            })
    }

    getParams(params){
        const  { data, value} = params
        this.first= false
        if(value == 1){
            this.setState({departmentId:data.departmentId, itemDepart:data, titleDepart:data.departmentName, isSearching:true},async () =>{
                await this.refreshList()
                await this.loadData(this.state.text)
            })
        }
        else{
            this.setState({jobId:data.jobtitleId, itemJob:data, titleJob:data.jobtitleName,  isSearching:true},async() =>{
                await this.refreshList()
                await this.loadData(this.state.text)
            })
        }
        
    }

    onSearchBox(text){
        if(text && (text.length != 0)){
            this.first = true
            this.setState({text:text, isSearching:true, disable:false, isShow:true},async() =>{
                this.text = text
                await this.refreshList()
                await this.loadData(text)
                if(this.state.listContact.length > 0){
                    this.setState({disOption:false})
                }
                else{
                    this.setState({disOption:true})
                }
            })
        }
        else{
            this.first = true
            // this.setState({
            //     disable:true,
            //     isShow:false,
            //     disOption:true,
            //     text:text, 
            //     isSearching:true, 
            //     departmentId:null, 
            //     jobtitleId:null, 
            //     titleDepart:null, 
            //     titleJob:null},async() =>{
            //         await this.refreshList()
            //         await this.loadData('')
            // })
            this.clearSearch(text)
        }
    }

    async loadData(text){
        let { totalPage, page, statusActive } = this;
        let { listContact, departmentId, jobId } = this.state;
        let newList = [...listContact];
        text = text || '';
        departmentId = departmentId || '';
        jobId = jobId || '';
        if (!totalPage || (totalPage && !isNaN(totalPage) && page <= totalPage)){
            const result = await HaravanHr.getListContact(page, statusActive, text, departmentId, jobId );
            if(result && result.data && !result.error){
                const { data , totalCount} = result.data;
                newList = [...newList,...data]
                if((this.first ==true) || ((this.text.length >0) && (this.state.text.length > 0) && (this.text !== this.state.text))){
                    this.currentList = newList
                }
                this.totalPage = Math.ceil(totalCount/20)
                this.page =(page + 1)
                this.setState({listContact:newList, isLoading:false, isLoadmore:false, isSearching:false})
            }
            else{
                this.setState({isLoading:false, isLoadmore:false,  isSearching:false})
            }
        }
    }

    loadMore(){
        const { page, totalPage } = this
        const { isShow, disable } = this.state
        if((page <= totalPage) && ((!isShow) && disable)){
            this.setState({isLoadmore:true},() =>{
                this.loadData()
            })
        }
    }

    refreshList(){
        this.page = 1;
        this.totalPage = null;
        this.statusActive = 1
        this.setState({listContact:[]})
    }
}
export default Contact;