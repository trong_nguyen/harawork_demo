import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Linking } from 'react-native';
import { HeaderNewStyles , settingApp, SvgCP, AvatarCustom, Utils} from '../../../../../../public';

class Item extends Component{
    constructor(props){
        super(props)
        this.state={

        }
    }

    render(){
        const {item, index} = this.props
        let imageUser = Utils.getLinkImage(item.haraId)
        const phone = 'tel://' + (item.phone)
        return(
            <View style={[styles.main,{borderTopWidth:(index == 0) ? 0 :1}]}>
                <TouchableOpacity 
                    onPress={() => this.props.navigation.navigate('ContactDetail',{data:item})}
                    style={styles.buttonCover}>
                    <View 
                        style={styles.coverImage}
                    >
                        <AvatarCustom userInfo={{ name: item.fullName, picture: imageUser }} v={40} fontSize={20}/>
                    </View>

                    <View style={styles.infoUser}>
                        <Text style={styles.textName}>{item.fullName}</Text>
                        <Text style={styles.textInfo}>{item.userId} - <Text>{item.phone}</Text></Text>
                        <Text style={[styles.textInfo,{fontWeight:'500'}]}>{item.departmentName}</Text>
                    </View>
                    
                    <TouchableOpacity 
                        onPress={() => Linking.openURL(phone)}
                        style={styles.phone}>
                        <SvgCP.iconPhone/>
                    </TouchableOpacity>
                </TouchableOpacity>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    main:{
        width:settingApp.width - 15,
        minHeight:75,
        backgroundColor:'#FFFFFF',
        marginLeft: 15,
        borderTopColor:settingApp.colorSperator,
    },
    buttonCover:{
        flex:1, 
        flexDirection:'row', 
        alignItems:'center'
    },
    coverImage:{
        width:40, 
        height:40, 
        borderRadius:20,
        backgroundColor:'#22C993', 
        justifyContent:'center',
        alignItems:'center'
    },
    infoUser:{
        marginLeft:12,
        width:settingApp.width - 120,
        minHeight:75,
        justifyContent:'center'
    },
    textName:{
        fontSize:17,
        color:settingApp.colorText,
        fontWeight:'600'
    },
    textInfo:{
        fontSize:14,
        color:settingApp.colorLable
    },
    phone:{
        position:'absolute',
        width:50,
        height:75,
        justifyContent:'center',
        alignItems:'center',
        top:0,
        bottom:0,
        right:15,
    }
})
export default Item;
