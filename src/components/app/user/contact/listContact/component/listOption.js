import React, { Component } from 'react';
import { View, Text, TouchableOpacity, FlatList , StatusBar, Platform, StyleSheet, TextInput} from 'react-native';
import { settingApp, HeaderNewStyles, SvgCP } from '../../../../../../public';
import { AntDesign, EvilIcons,Ionicons } from '@expo/vector-icons'
import lodash from 'lodash';

class ListOption extends Component{
    constructor(props){
        super(props)
        this.state={
            isLoading:true,
            listData: props.navigation.state.params.data,
            listSearch:[]
        }
        this.currentList = props.navigation.state.params.data
        this.itemCheck = props.navigation.state.params.item
        this.listData = props.navigation.state.params.data;
        this.value = props.navigation.state.params.value;
        this.actions = props.navigation.state.params.actions;
        
    }

    componentDidMount(){
        // this.setState({listData:this.currentList},() =>{
            
        // })

        if(this.value == 1){
            this.loadDepart()
        }
        else{
            this.loadJob()
        }
    }

    loadDepart(){
        const{ listData } = this.state;
        let newList = []
        if(listData && (listData.length > 0)) {
            listData.map(item =>{
                let index =  newList.findIndex(m => m.departmentId == item.departmentId)
                if(index < 0){
                    newList.push(item)
                }
            })
            this.currentList = newList
            this.setState({listSearch:newList})
        }
    }
    
    loadJob(){
        const{ listData } = this.state;
        let newList = []
        if(listData && (listData.length > 0)) {
            listData.map(item =>{
                let index =  newList.findIndex(m => m.jobtitleId == item.jobtitleId)
                if(index < 0){
                    newList.push(item)
                }
            })
            this.currentList = newList
            this.setState({listSearch:newList})
        }
    }

    onSearchBox(text){
        let {listSearch, listData} = this.state;
        const { currentList } = this
        let newList = []
        if(text && (text.length !=0)){
            listData.map(item =>{
                if(this.value ==1){
                    if(item.departmentName.toLowerCase().indexOf(text.toLowerCase()) > -1){
                        newList.push(item)
                    }
                    this.setState({listSearch:newList})
                }
                else{
                    if(item.jobtitleName.toLowerCase().indexOf(text.toLowerCase()) > -1){
                        newList.push(item)
                    }
                    this.setState({listSearch:newList})
                }
            }) 
        }
        else{
            if(this.value == 1){
                this.loadDepart()
            }
            else{
                this.loadJob()
            } 
        }
        
    }

    check(){
        let check = null
        const { listSearch } = this.state
        if(this.value == 1){
            // listSearch.map(item =>{
            //     if()
            // })
            for(let i = 0; i < listSearch.length; i++){
                if(listSearch[i].departmentId === this.itemCheck.departmentId){
                    check = listSearch[i]
                    break;
                }
            }
        }
        else{
            for(let i = 0; i < listSearch.length; i++){
                if(listSearch[i].jobtitleId === this.itemCheck.jobtitleId){
                    check = true
                    break;
                }
            }
        }
        return check
    }

    renderHeader(){
        let title = (this.value == 1) ? 'Chọn đơn vị' : 'Chọn chức danh';
        return(
        <View style={[styles.Header]}>
            <StatusBar barStyle={Platform.OS === 'ios' ? 'dark-content' : 'default'}/>
            <View style={[styles.statusBar,{backgroundColor:'#FFFFFF' }]} />
            <View style={styles.coverHeader} >
                <View style={styles.infoHeader}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.pop()}
                        style={styles.backHeader}
                    >
                        <AntDesign name='left' size={25} color={settingApp.colorText} />
                        <Text style={styles.textHeader}>{title}</Text>
                    </TouchableOpacity> 
                </View>
            </View>
        </View>
        )
    }

    _renderItem(obj){
        const {item, index} = obj
        const { value } = this;
        const {listSearch } = this.state
        let content = (value == 1) ? (item.departmentName ? item.departmentName :'--') : (item.jobtitleName ? item.jobtitleName :'--')
        let checkDepart = (value == 1) && (item.departmentId && this.itemCheck && (item.departmentId == this.itemCheck.departmentId)) 
        let checkJod = (value == 2) &&  (item.jobtitleId && this.itemCheck &&(item.jobtitleId == this.itemCheck.jobtitleId))
        return(
            <View style={[styles.itemButton,]}>
                <TouchableOpacity
                    onPress={() => {
                            this.actions({data:item, value:this.value})
                            this.props.navigation.pop()
                            }}
                    style={{ height:44, width:settingApp.width - 30, flexDirection:'row', alignItems:'center'}}
                >
                    <Text style={{fontSize:16, color:settingApp.colorText, width:(settingApp.width -90)}}>{content}</Text>
                    {checkDepart  && <Ionicons name='md-checkmark' size={20} color={settingApp.blueSky} /> }
                    {checkJod  && <Ionicons name='md-checkmark' size={20} color={settingApp.blueSky} /> }
                </TouchableOpacity>
            </View>
        )
    }

    renderSearch(){
        let onChangeCallback = lodash.debounce(this.onSearchBox, 10)
        let title = (this.value == 1) ? 'Chọn đơn vị' : 'Chọn chức danh';
        return(
            <View 
                style={styles.coverSearch}
                >
                <View
                    style={styles.searchBox}
                    >
                    <EvilIcons name="search" size={25} color={settingApp.colorDisable} />
                    <TextInput
                        ref = {refs => this.TextInput = refs}
                        style={styles.textInput}
                        placeholder={title}
                        onChangeText={onChangeCallback.bind(this)}
                        multiline={false}
                    />
                </View>
            </View>
        )
    }

    render(){
        const { listData, listSearch } = this.state;
        return(
            <View style={{flex:1, backgroundColor:'#FFFFFF'}}>
                {this.renderHeader()}
                {this.renderSearch()}
                <FlatList
                    data={listSearch}
                    extraData={this.state}
                    keyExtractor={(item, index) => ''+index}
                    renderItem={(obj) => this._renderItem(obj)}
                />
            </View>
        )
    }

    componentWillUnmount(){
        this.currentList = []
    }
}
const styles = StyleSheet.create({
    Header:{
        width:settingApp.width,
        height:44 + settingApp.statusBarHeight,
        backgroundColor:'#FFFFFF'
    },
    statusBar:{
        width:settingApp.width, 
        height: settingApp.statusBarHeight,
    },
    coverHeader:{
        width:settingApp.width, 
        height: 44, 
        flexDirection: 'row', 
        alignItems:'center'
    },
    infoHeader:{
        flex: 1, 
        backgroundColor: 'transparent', 
        flexDirection: 'row', 
        alignItems:'center' 
    },
    textHeader:{
        fontSize:16, 
        color:settingApp.colorText,
        paddingLeft:5,
        fontWeight:'600'
    },
    backHeader:{
        height:44, 
        justifyContent:'center', 
        alignItems:'center', 
        flexDirection:'row', 
        marginLeft:10
    },
    coverSearch:{
        height:55,
        alignItems:'center', 
        flexDirection:'row', 
        justifyContent:'center', 
        borderBottomColor:settingApp.colorSperator, 
        borderBottomWidth:1,
        width:settingApp.width,
        backgroundColor:'#FFFFFF'
    },
    searchBox:{
        width:settingApp.width - 30,
        paddingLeft:10,
        paddingRight:10,
        borderRadius:10,
        backgroundColor:'rgba(142, 142, 147, 0.12)',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
    },
    textInput:{
        height: 40,
        width:settingApp.width -65,
        fontSize:16
    },
    itemButton:{
        width:settingApp.width, 
        height:44, 
        borderBottomColor:settingApp.colorSperator, 
        borderBottomWidth:1,
        marginLeft:15
    }
})
export default ListOption;