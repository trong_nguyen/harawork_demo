import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { HeaderScreenNews, settingApp } from '../../../../../../public'
import { AntDesign } from '@expo/vector-icons'
import Svg ,{ Path,} from 'react-native-svg';

class Header extends Component{
    constructor(props){
        super(props)
        this.state={
            backgroundHeader:''
        }
    }
    
    renderHeader(){
        return(
                <HeaderScreenNews 
                    style={{backgroundColor:'transparent'}}
                    navigation={this.props.navigation}
                    buttonLeft={
                        <TouchableOpacity
                            onPress={() => this.props.navigation.pop()}
                            style={styles.backHeader}
                        >
                            <AntDesign  name='left' size={25} color={settingApp.colorText} />
                            <Text style={styles.textHeader}>Danh bạ</Text>
                        </TouchableOpacity>
                    }
                />
        )
    }

    avatar(name) {
        let { fontSize} = this.props;
        fontSize = fontSize || 30;
        if (name && name.length > 0) {
            const arr_color = ['#536DFE', '#5AC8FA', '#FF9E00', '#22C993', '#666AD1'];
            let listName = name.split(' ');
            let shortName = '';
            listName.map(e => {
                shortName = shortName + e[0]
            });
            shortName = shortName.substr(-2);
            let bgColor = '';
            const charCodeShortName = shortName.charCodeAt(0);
            switch (true) {
                case (charCodeShortName >= 65 && charCodeShortName < 70) && arr_color[0] !== undefined: bgColor = arr_color[0]; break;
                case (charCodeShortName >= 70 && charCodeShortName < 75) && arr_color[1] !== undefined: bgColor = arr_color[1]; break;
                case (charCodeShortName >= 75 && charCodeShortName < 80) && arr_color[2] !== undefined: bgColor = arr_color[2]; break;
                case (charCodeShortName >= 80 && charCodeShortName < 85) && arr_color[3] !== undefined: bgColor = arr_color[3]; break;
                case (charCodeShortName >= 85 && charCodeShortName <= 90) && arr_color[4] !== undefined: bgColor = arr_color[4]; break;
                default: bgColor = arr_color[0] !== undefined ? arr_color[0] : bgColor; break;
            }
            this.props.bkgColor(bgColor)
            return {avatar : name,
                    backgroundHeader:bgColor
                }
        } else {
            return <View />
        }
    }
    avatarPicture(picture) {
        // if (picture && source.hasOwnProperty('uri')) {
        //     const lengthUri = source.uri.length;
        //     if (lengthUri > 0) {
        //         const picture = source.uri;
        //         const typeImage = picture.substr(picture.lastIndexOf('.'));
        //         source = { uri: picture.replace(typeImage, `_${type}${typeImage}`) }
        //         thumbnailSource = { uri: picture.replace(typeImage, `_${'small'}${typeImage}`) }
        //     } else {
        //         source = imageApp.no_Image;
        //         thumbnailSource = imageApp.no_Image;
        //         resizeMode = 'stretch';
        //     }
        // }

        if (picture && picture.length > 0) {
            return avatar = (
                <Image 
                    source={{ uri: picture }}
                    style={{ flex: 1 }}
                    resizeMode='stretch'
                />
            )
        } else {
            return <View />
        }
    }

    render() {
        let { v } = this.props;
        const { name, picture } = this.props.userInfo;
        const avatar = this.avatar(name);
        const avatarPicture = this.avatarPicture(picture);
        v = v ? (v + 5) : 75;
        return (
            <View style={{ }}>
                <View style={{ width:settingApp.width,height:settingApp.height*0.35, alignItems: 'center', backgroundColor: avatar.backgroundHeader }}>
                    {/* {this.renderHeader()} */}
                    <View style={{ width:settingApp.width-30,height:settingApp.height*0.35, overflow: 'hidden', justifyContent:'flex-end' }}>
                        <Text style={{ color: '#ffffff', fontSize:30, fontWeight: '600', marginBottom:20 }}>
                            {name}
                        </Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    textHeader:{
        fontSize:14, 
        color:settingApp.colorText,
        paddingLeft:5
    },
    backHeader:{
        height:44, 
        justifyContent:'center', 
        alignItems:'center', 
        flexDirection:'row', 
        marginLeft:10
    }
})
export default Header;