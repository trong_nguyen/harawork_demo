import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, FlatList, ActivityIndicator, StatusBar, Platform } from 'react-native';
import { settingApp, Utils, SvgCP, HeaderScreenNews } from '../../../../../public';
import Banner from './components/header'
import { AntDesign } from '@expo/vector-icons'
import { Animated } from 'react-native';
import * as Animatable from 'react-native-animatable';

const noneInfo = 'Chưa cập nhật'
class Layout extends Component{

    renderHeader(){
        const {isShow} = this.state
        return (
            <View style={[styles.Header,{backgroundColor:this.color, }]}>
                <StatusBar barStyle={Platform.OS === 'ios' ? 'dark-content' : 'default'}/>
                <View style={[styles.statusBar,{backgroundColor:this.color, }]} />
                <View style={styles.coverHeader} >
                        <View style={styles.infoHeader}>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.pop()}
                                style={styles.backHeader}
                            >
                                <AntDesign  name='left' size={25} color={'#FFFFFF'} />
                            </TouchableOpacity> 
                            
                            {isShow && <Animatable.View 
                                animation='fadeIn'
                                style={{ width:settingApp.width*0.7, marginLeft:5}}>
                                <Text 
                                    numberOfLines={1}
                                    style={{ fontSize: 17, color:'#ffffff', fontWeight: 'bold'}}>
                                        {this.data.fullName}
                                </Text>
                            </Animatable.View>}
                            
                        </View>
                </View>
            </View>
        )
    }
    renderBanber(){
        return(
            <Banner 
                navigation ={ this.props.navigation }
                bkgColor={color => this.getColor(color)}
                userInfo={{ name: this.data.fullName, picture: this.data.photo }} v={80} fontSize={30}/>
        )
    }

    renderTopOption(){
        const { data } = this
        return(
            <View style={styles.coverTop}>
                <View style={{ minHeight:72, width:settingApp.width * 0.6, padding:15, paddingLeft:0}}>
                   <Text style={styles.infoText}>{data.userId ? data.userId : `${noneInfo} mã nhân viên`} - <Text>{data.phone ? data.phone : `${noneInfo} số điện thoại`}</Text> </Text>
                   <Text style={styles.infoText}>{data.email ? data.email : `${noneInfo} email`}</Text>
                </View>
                <View style={styles.coverButton}>
                    <TouchableOpacity
                        onPress={() => this.Call()}
                        style={styles.button}
                    >
                        <SvgCP.iconPhone />
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.Mail()}
                        style={styles.button}
                    >
                        <SvgCP.iconMail/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    renderLine(title, descrif, type){
        return(
            <View style={styles.coverLine}>
                <Text style={{fontSize:14, color:settingApp.colorDisable}}>{title}</Text>
                <Text style={{fontSize:16, color:(type == 1) ? settingApp.blueSky : settingApp.colorText, paddingTop:5}}>{descrif}</Text>
            </View>
        )
    }

    renderEndLine(){
        const { directapprover, isLoading } = this.state;
        return(
            <View style={styles.coverLine}>
                <Text style={{fontSize:14, color:settingApp.colorDisable}}>Quản lý trực tiếp</Text>
                {(directapprover.length > 0) && !isLoading &&
                    <FlatList 
                        data={directapprover}
                        keyExtractor={(item, index) => ''+index}
                        extraData={this.state}
                        renderItem={this._renderItem}
                        scrollEnabled={false}
                    />
                }
                {(directapprover.length == 0) && isLoading && <ActivityIndicator color={settingApp.colorDisable} size={'small'} />}
                {(directapprover.length == 0) && !isLoading &&
                    <Text style={{fontSize:16, color:settingApp.colorText}}>{noneInfo}</Text>
                }
            </View>
        )
    }

    renderActingPosition(title, list){
        return(
            <View style={styles.coverLine}>
                <Text style={{fontSize:14, color:settingApp.colorDisable}}>{title}</Text>
                {/* <Text style={{fontSize:16, color:(type == 1) ? settingApp.blueSky : settingApp.colorText, paddingTop:5}}>{descrif}</Text> */}
                {(list.length > 0) &&list.map((item, index) =>{
                    return(
                        <Text 
                            key={index}
                            style={styles.textDefaul}>{item.jobtitleName} tại {item.departmentName ? item.departmentName : '--'}</Text>
                    )
                })}
                {(list.length == 0)&&  <Text style={styles.textDefaul}>{noneInfo}</Text>}
            </View>
        )
    }

    _renderItem({item, index}){
        let phone = item.phone ? item.phone : `${noneInfo} số điện thoại`;
        let userId = item.userId ? item.userId : noneInfo
        let email = item.email ?  item.email : `${noneInfo} email`
        return(
            <View style={[styles.coverItem, {borderTopWidth:(index == 0) ? 0 :1}]}>
                <View style={styles.lineInfo}>
                    <Text style={{fontSize:16, color:settingApp.colorText}}>{item.fullName}</Text>
                    <Text style={{fontSize:16, color:settingApp.colorDisable}}> | {userId} - </Text>
                    <Text style={{fontSize:16, color:settingApp.colorDisable}}>{phone}</Text>
                </View>
                <Text style={{fontSize:16, color:settingApp.colorText}}>{email}</Text>
            </View>
        )
    }
    
    render(){
        const { data } = this
        let fromDate =  data.fromDate ? Utils.formatTime(data.fromDate,'DD/MM/YYYY', true) : noneInfo;
        let birthDay =  data.birthday ? Utils.formatTime(data.birthday,'DD/MM/YYYY', true) : noneInfo;
        let job = (data.type && data.type.name) ? data.type.name : noneInfo;
        return(
            <View style={{flex:1, backgroundColor:'#FFFFFF'}}>
                {this.renderHeader()}
                <ScrollView 
                scrollEventThrottle={1}
                    onScroll ={ Animated.event([
                        {nativeEvent:{ contentOffset: { y : this.state.scrollY }}}
                    ])}
                style={{flex:1}}>
                    {this.renderBanber()}
                    {this.renderTopOption()}
                    
                    {this.renderLine('Chức danh', (data.jobtitleName ? data.jobtitleName : `${noneInfo} chức danh`), 1 )}
                    {this.renderLine('Đơn vị', (data.departmentName ? data.departmentName : `${noneInfo} đơn vị`), 1 )}
                    {this.renderLine('Ngày vào làm', fromDate , 2 )}
                    {this.renderLine('Sinh nhật', birthDay , 2 )}
                    {(data.actingPositions) && this.renderActingPosition('Kiêm nhiệm', data.actingPositions , 2)}
                    {this.renderEndLine()}
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    Header:{
        position:'absolute', 
        top:0, 
        left:0, 
        right:0, 
        zIndex:999
    },
    statusBar:{
        width:settingApp.width, 
        height: settingApp.statusBarHeight,
    },
    coverHeader:{
        width:settingApp.width, 
        height: 44, 
        flexDirection: 'row', 
        alignItems:'center'
    },
    infoHeader:{
        flex: 1, 
        backgroundColor: 'transparent', 
        flexDirection: 'row', 
        alignItems:'center' 
    },
    textHeader:{
        fontSize:14, 
        color:settingApp.colorText,
        paddingLeft:5
    },
    backHeader:{
        height:44, 
        justifyContent:'center', 
        alignItems:'center', 
        flexDirection:'row', 
        marginLeft:10
    },
    coverTop:{
        width:settingApp.width -15,
        height:72,
        flexDirection:'row',
        justifyContent:'space-between',
        borderBottomWidth:1,
        borderBottomColor:settingApp.colorSperator,
        marginLeft:15
    },
    nameUser:{
        fontSize:16, 
        color:settingApp.colorText, 
        fontWeight:'600',
        width:settingApp.width * 0.6
    },
    coverButton:{
        height:72, 
        width:settingApp.width * 0.2, 
        alignItems:'flex-end', 
        flexDirection:'row',
        marginRight:30
    },
    infoText:{
        fontSize:14, 
        color:settingApp.colorDisable
    },
    button:{
        width:50,
        height:72, 
        justifyContent:'center', 
        alignItems:'center'
    },
    coverLine:{
        minHeight:52, 
        width:settingApp.width -15, 
        padding:15,
        paddingLeft:0, 
        borderBottomWidth:1,
        borderBottomColor:settingApp.colorSperator,
        marginLeft:15
    },
    coverItem:{
        width:settingApp.width -30, 
        padding:5, 
        paddingLeft:0, 
        borderTopColor:settingApp.colorSperator, 
    },
    lineInfo:{
        flexDirection:'row',
        width:settingApp.width -30, 
        flexWrap:'wrap'
    },
    textDefaul:{
        fontSize:16, 
        color:settingApp.colorText, 
        paddingTop:5
    }
})
export default Layout;