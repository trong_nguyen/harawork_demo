import Layout from './layout';
import { connect } from 'react-redux';
import { HaravanHr } from '../../../../../Haravan';
import { Animated, Linking, Alert } from 'react-native';

class ContactDetail extends Layout{
    constructor(props){
        super(props)
        this.state = {
            scrollY: new Animated.Value(0),
            isLoading:true,
            directapprover:[]
        }
        this.userInfo = props.app.authHaravan.auth.access_token;
        this.data = props.navigation.state.params.data;
        this.color = '#ffffff'
    }

    componentDidMount(){
        this.state.scrollY.addListener(({value}) =>{
            if(value > 130){
                this.setState({isShow:true})
            }else if(value <130){
                this.setState({isShow:false})
            }
        })

        this.loadDirectapprover()
    }

    getColor(color){
        this.color = color
    }

    Call(){
        const { data } = this;
        if(data.phone && (data.phone.length >0)){
            const phone = 'tel://' + (data.phone)
            Linking.openURL(phone)
        }
        else{
            this.showAlert('Người này chưa cập nhật số điện thoại.')
        }
    }

    Mail(){
        const { data } = this;
        this.props.navigation.navigate('MailWrite',{dataContact:this.data, type:'write', data:[]})
        // if(data.email && (data.email.length >0)){
        //     //const email = `mailto://${data.email}`
        //     //href="mailto://support@expo.io"
        //     // Linking.openURL(email)
            
        // }
        // else{
        //     this.showAlert('Người này chưa cập nhật email.')
        // }
    }

    async loadDirectapprover(){
        const result = await HaravanHr.Directapprover(this.data.id)
        if(result && result.data && !result.error){
            this.setState({directapprover:result.data, isLoading:false})
        }
        else{
            this.setState({isLoading:false})
        }
    }

    showAlert(mess){
        Alert.alert(
            'Thông báo',
            `${mess}`,
            [
                { text: 'Đồng ý', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: true }
        )
    }

}

const mapStateToProps = (state) => ({ ...state })
export default connect(mapStateToProps)(ContactDetail);