import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import React, { Component } from 'react';
import { settingApp, Utils } from '../../../../../../public';
import { AntDesign } from '@expo/vector-icons';

class Item extends Component{
    constructor(props){
        super(props)
        this.state={
            total: null
        }
    }

    componentDidMount(){
        const { item } = this.props;
        if(item.totalColumn){
            if(item.data && (item.data.length >0)){
                let total = 0
                for(let i =0; i < item.data.length; i++){
                    let list = item.data[i]
                    let count = list[item.totalColumn]
                    total = total +  (count *1)
                }
                this.setState({total:total})
            }
        }
    }

    goDetail(item){
        if(item.columns){
            this.props.navigation.navigate('DetailSalaryPerday', {
                data:item,
                totalSalary: this.state.total
            })
        }
    }

    render(){
        const { item } = this.props;
        const { total } = this.state;
        let formatSalary = Utils.formatNumber(total)
        return(
            <TouchableOpacity 
                onPress={() => this.goDetail(item)}
                style={styles.main}>
                <View style={styles.info}>
                    <Text style={[styles.textDefaul,{paddingBottom:5}]}>{item.title}</Text>
                    {(total != null) && <Text style={styles.textWeigth}>{formatSalary}</Text>}
                </View>
                <View
                    style={styles.iconRight}
                >
                    <AntDesign name='right' size={20} color={settingApp.colorDisable} />
                </View>
            </TouchableOpacity>
        )
    }
}
const styles = StyleSheet.create({
    main:{
        width:settingApp.width, 
        minHeight:80,
        borderBottomColor:settingApp.colorSperator,
        borderBottomWidth:1,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    info:{
        width:settingApp.width -60,
        minHeight:80,
        paddingLeft:15,
        justifyContent:'center'
    },
    textWeigth:{
        fontSize:16, 
        fontWeight:'bold',
        color:settingApp.colorText
    },
    textDefaul:{
        color:settingApp.colorText,
        fontSize:16
    },
    iconRight:{
        minHeight:80, 
        width:50, 
        justifyContent:'center', 
        alignItems:'center'
    }

})
export default Item;