import Layout from './layout'
import { connect } from 'react-redux'
import { HaravanHr } from '../../../../../Haravan';
import { Utils } from '../../../../../public';

class SalaryPerDay extends Layout{
    constructor(props){
        super(props)
        this.state={
            listData: [],
            isLoading:true
        }
        this.colleague = props.app.colleague;
        this.data = props.navigation.state.params.data;
        this.parentData = props.navigation.state.params.parentData
    }

    componentDidMount(){
        this.loadData()
    }

    async loadData(){
        const { data, colleague, parentData } = this
        const date = Utils.formatTime(data.salaryDay,'YYYY-MM-DD', true)
        const result = await HaravanHr.getSalaryPerDay(data.salaryPeriodId, colleague.id, parentData.formulaId, date)
        if(result && result.data && !result.error){
            this.setState({listData: result.data.datasource, isLoading:false})
        }
        else{
            this.setState({isLoading:false})
        }
    }
}
mapStateToProps = state => ({ ...state });
export default connect(mapStateToProps)(SalaryPerDay);