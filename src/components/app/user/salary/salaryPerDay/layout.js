import React, { Component } from 'react'
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, FlatList , Image} from 'react-native';
import Header from '../component/header'
import { Utils, AvatarCustom, settingApp, imgApp, Loading } from '../../../../../public';
import Item from './components/item'

class Layout extends Component{
    renderHeader(){
        const { data } = this;
        let title = Utils.formatTime(data.salaryDay,'DD/MM/YYYY', true)
        return(
            <Header 
                navigation={this.props.navigation}
                title={title}
            />
        )
    }

    renderUser(){
        const { colleague } = this
        let jobtitleName = colleague.mainPosition.jobtitleName ? colleague.mainPosition.jobtitleName :'--';
        let departmentName = colleague.mainPosition.departmentName ? colleague.mainPosition.departmentName : '--'
        return(
            <View style={styles.coverUser}>
                <AvatarCustom userInfo={{ name: colleague.fullName, picture: colleague.photo }} v={60} fontSize={20}/>
                <View style={styles.infoUser}>
                    <Text style={styles.textWeigth}>{colleague.fullName}</Text>
                    <Text style={styles.textDisable}>{colleague.userId}</Text>
                    <Text style={[styles.textDisable,{fontWeight:'400'}]}>{jobtitleName} - {departmentName}</Text>
                </View>
            </View>
        )
    }

    renderItem(obj){
        const {item,index} = obj;
        return(
            <Item
                item={item}
                index={index}
                navigation={this.props.navigation}
            />
        )
    }

    renderNoneData(){
        return(
            <View style={styles.noneData}>
                <Image
                    source={imgApp.frameNone} 
                    style={{width:settingApp.width*0.5, height:(settingApp.width*0.5), resizeMode:'contain'}}
                />
                <Text style={styles.textNoneData}>Chưa thiết lập kết nối với hệ thống tính thưởng</Text>
            </View>
        )
    }


    render(){
        const {listData, isLoading} = this.state
        return(
            <View style={{flex:1, backgroundColor:'#FFFFFF'}}>
                {this.renderHeader()}
                {this.renderUser()}
                <View style={styles.line}/>
                {(listData.length > 0) && !isLoading && <FlatList
                    data={this.state.listData}
                    extraData={this.state}
                    keyExtractor={(item, index) => ''+index}
                    renderItem={(obj) => this.renderItem(obj)}
                />}
                {(listData.length == 0) && !isLoading && this.renderNoneData()}
                {isLoading && <Loading/>}
            </View>
        )
    }
}

const styles= StyleSheet.create({
    line:{
        width:settingApp.width,
        height:5,
        backgroundColor:'#EEEEEE'
    },
    coverUser:{
        width:settingApp.width, 
        minHeight:120, 
        paddingLeft:15, 
        paddingRight:15, 
        flexDirection:'row',
        alignItems:'center',
        backgroundColor:'#FFFFFF',
        ...settingApp.shadow
    },
    infoUser:{
        minHeight:120,
        width:(settingApp.width -100),
        justifyContent:'center',
        paddingLeft:15
    },
    textWeigth:{
        fontWeight:'600',
        color:settingApp.colorText,
        fontSize:17,
    },
    textDisable:{
        color:settingApp.colorDisable,
        fontSize:14
    },
    noneData:{
        width:settingApp.width, 
        height:settingApp.width, 
        justifyContent:'center', 
        alignItems:'center'
    },
    textNoneData:{
        fontSize:16, 
        color:settingApp.colorText, 
        paddingTop:10, 
        textAlign:'center'
    },
})
export default Layout;