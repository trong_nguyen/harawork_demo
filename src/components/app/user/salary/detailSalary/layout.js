import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, FlatList } from 'react-native';
import { settingApp, SvgCP, Utils, Loading, NotPermission } from '../../../../../public';
import Header from '../component/header';

class Layout extends Component{
    renderHeader(){
        return(
            <Header
                navigation={this.props.navigation}
                title = {'Chi tiết công thức lương theo ngày'}
            />
        )
    }

    renderInfoTop(){
        const { data,  titlePerios} = this;
        let startDate = Utils.formatTime(titlePerios.fromDate, 'DD/MM/YYYY', true);
        let endDate = Utils.formatTime(titlePerios.toDate, 'DD/MM/YYYY', true);
        return(
            <View style={styles.coverInfoTop}>
                <View style={styles.nameInfo}>
                    <Text style={styles.formulaName}>{data.formulaName}</Text>
                    <Text style={styles.date}>{startDate ?  startDate : '--/--'} - {endDate ? endDate : '--/--'}</Text>
                </View>
                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('DetailFomulas',{
                        data:data,
                        titlePerios:titlePerios
                        })} 
                    style={styles.buttonGuild}>
                    <SvgCP.iconGuildFomulas/>
                </TouchableOpacity>
            </View>
        )
    }

    renderItem(obj){
        const { item, index } = obj
        let date = Utils.formatTime(item.salaryDay, 'DD/MM/YYYY', true)
        let formatSalary = Utils.formatNumber(item.total)
        return(
            <TouchableOpacity
                onPress={() => this.props.navigation.navigate('SalaryPerDay', 
                {
                    data:item,
                    parentData:this.data
                    })} 
                style={styles.coverItem}>
                <Text style={styles.textWeight}>{date ? date : '--'}</Text>
                <View style={styles.line2}>
                    <Text style={styles.textInfoItem}>{item.jobtitleName}</Text>
                    <Text style={[styles.textWeight,{width:((settingApp.width-30)*0.4), textAlign:'right'}]}>{formatSalary}</Text>
                </View>
            </TouchableOpacity> 
        )
    }

    renderContent(){
        const { listDetail, isLoading } = this.state;
        if(!isLoading && (listDetail.length > 0)){
            return(
                <View style={{flex:1}}>
                    <FlatList
                        data={listDetail}
                        extraData={this.state}
                        keyExtractor={(item, index) => ''+index}
                        renderItem={(obj) => this.renderItem(obj)}
                    />
                </View>
            )
        }
        else{
            return <Loading />
        }
    }

    render(){
        const { notPermission } = this.state;
        if(!notPermission){
            return(
                <View style={{flex:1, backgroundColor:'#FFFFFF'}}>
                    {this.renderHeader()}
                    {this.renderInfoTop()}
                    <View style={styles.line}/>
                    {this.renderContent()}
                </View>
            )
        }
        else{
            return <NotPermission navigation={this.props.navigation}/>
        }
        
    }
}
const styles= StyleSheet.create({
    line:{
        width:settingApp.width,
        height:5,
        backgroundColor:'#EEEEEE'
    },
    coverInfoTop:{
        flexDirection:'row', 
        paddingLeft:15, 
        paddingRight:15, 
        minHeight:75, 
        width:settingApp.width, 
        justifyContent:'space-between', 
        alignItems:'center',
        borderTopColor:settingApp.colorSperator,
        borderTopWidth:1
    }, nameInfo:{
        width:((settingApp.width-30) *0.7), 
        minHeight:75, 
        justifyContent:'center'
    },
    buttonGuild:{
        height:75,
        width:50,
        alignItems:'center',
        position:'absolute',
        top:20, 
        right:10
    },
    formulaName:{
        fontSize:18,
        color:settingApp.colorText,
        fontWeight:'600'
    },
    date:{
        fontSize:14,
        color:settingApp.colorDisable,
        paddingTop:5
    },
    textWeight:{
        fontSize:16, 
        color:settingApp.colorText, 
        fontWeight:'600'
    },
    coverItem:{
        width:settingApp.width, 
        minHeight:80, 
        justifyContent:'center', 
        paddingLeft:15, 
        paddingRight:15,
        borderBottomColor:settingApp.colorSperator,
        borderBottomWidth:1
    },
    line2:{
        width:settingApp.width -30, 
        minHeight:20, 
        flexDirection:'row', 
        justifyContent:'space-between',
        alignItems:'center'
    },
    textInfoItem:{
        fontSize:16, 
        color:settingApp.colorDisable, 
        width:(settingApp.width-30)*0.6
    }
})
export default Layout;