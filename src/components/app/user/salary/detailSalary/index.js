import Layout from './layout';
import { HaravanHr } from '../../../../../Haravan'
import { connect } from 'react-redux';

class DetailSalary extends Layout{
    constructor(props){
        super(props)
        this.state={
            isLoading: true,
            listDetail: [],
            notPermission:false
        }
        this.colleague = props.app.colleague;
        this.data = props.navigation.state.params.data;
        this.titlePerios = props.navigation.state.params.titlePerios;
        this.page = 1;
    }
    
    componentDidMount(){
        this.loadData()
    }

    async loadData(){
        const { colleague, page, data, titlePerios } = this
        const result = await HaravanHr.getDetailSalary(titlePerios.id, colleague.id, data.formulaId, page);
        if(result && result.data && !result.error){
            this.setState({listDetail:result.data, isLoading:false})
        }
        else if(result.error && (result.status === 403)){
            this.setState({notPermission:true})
        }
        else{
            this.setState({isLoading:false})
        }
    }

}
mapStateToProps = state => ({ ...state });
export default connect(mapStateToProps)(DetailSalary)