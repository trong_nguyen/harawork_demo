import Layout from './layout'

class DetailSalatyPerDay extends Layout{
    constructor(props){
        super(props)
        this.state={
            listTable:[]
        }

        this.data = props.navigation.state.params.data;
        this.totalSalary = props.navigation.state.params.totalSalary
    }

    componentDidMount(){
        this.getData()
    }

    getData(){
        const {data} = this;
        
        if(data && data.data){
            let newList = []
            newList = [{...[data.columns]}, ...newList]
            let listData = data.data
            listData.map(item =>{
                item = {...[item]}
                newList.push(item)
            })
            this.setState({listTable:newList})
        }
    }
}
export default DetailSalatyPerDay;