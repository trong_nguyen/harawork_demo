import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, ScrollView } from 'react-native';
import Header from '../component/header'
import { settingApp, Utils } from '../../../../../public';

class Layout extends Component{

    renderHeader(){
        return(
            <Header
                navigation={this.props.navigation}
                title={this.data.title}
            />
        )
    }

    renderTotal(){
        const { totalSalary } = this
        return(
            <View style={styles.coverTotal}>
                <Text style={styles.textWeigth}>Tổng</Text>
                <Text style={styles.textTotal}>{totalSalary? totalSalary : 0}</Text>
            </View>
        )
    }

    renderItemText(obj){
        const {item, index} = obj;
        let formatSalary = ''
        // if(item.substr(item.indexOf('#')) > -1){
        //     formatSalary = item
        // }
        // else{
            
        // }
        formatSalary = Utils.formatNumber(item)
        return(
            <View style={{minWidth:150, height:60, justifyContent:'center'}}>
                <Text style={{fontSize:14}}>{formatSalary}</Text>
            </View>
        )
    }

    renderItem(obj){
        const {item, index} = obj;
        let list = item[0]
        if((index != 0) && (index != -1)){
            return(
                <View style={[styles.colum,{borderRightWidth:(index ==0) ? 0 : 1}]}>
                    <FlatList
                        data={list}
                        extraData={this.state}
                        keyExtractor={(item, index) => index+''}
                        renderItem={(obj) => this.renderItemText(obj)}
                        scrollEnabled={false}
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                    />
                </View>
            )
        }
        else if( (index == -1) ){
            return(
                <View style={[styles.columTitle,{borderRightWidth:(index ==0) ? 0 : 1}]}>
                    <FlatList
                        data={list}
                        extraData={this.state}
                        keyExtractor={(item, index) => index+''}
                        renderItem={(obj) => this.renderItemText(obj)}
                        scrollEnabled={false}
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                    />
                </View>
            )
        }
        else{
            return <View/>
        }
        
    }

    renderContent(){
        const { listTable } = this.state;
        return(
            <FlatList
                data={listTable}
                extraData={this.state}
                keyExtractor={(item, index) => index+''}
                renderItem={(obj) => this.renderItem(obj)}
                horizontal={true}
                stickyHeaderIndices={[0]}
            />
        )
    }

    render() {
        const { listTable } = this.state;
        let obj = (listTable.length > 0) && { item:listTable[0], index:-1}
        return (
            <View style={{flex:1, backgroundColor:'#FFFFFF'}}>
                {this.renderHeader()}
                {this.renderTotal()}
                <ScrollView style={{flex:1}}>
                    <View style={{width:settingApp.width, flexDirection:'row'}}>
                        {obj && this.renderItem(obj)}
                        {this.renderContent()}
                    </View>
                </ScrollView>
            </View>
        )
    }
}
const styles =  StyleSheet.create({
    coverTotal:{
        width:settingApp.width,
        minHeight:50,
        justifyContent:'space-between',
        alignItems:'center',
        paddingLeft:15,
        paddingRight:15,
        flexDirection:'row',
        backgroundColor:'#F9F9F9'
    },
    textWeigth:{
        fontSize:16,
        fontWeight:'600',
        color:settingApp.colorText
    },
    textTotal:{
        fontSize:18,
        fontWeight:'bold',
        color:'#30C894'
    },
    colum:{
        minWidth:100,
        minHeight:20,
        marginLeft:5,
        borderRightColor:settingApp.colorSperator,
        padding:10
    },
    columTitle:{
        width:150,
        minHeight:20,
        marginLeft:5,
        borderRightColor:settingApp.colorSperator,
        padding:10
    }
})
export default Layout