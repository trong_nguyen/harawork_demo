export function setReportType(type){
    let result = null
    switch (type) {
        case 1:
            result = 'Giá trị trung bình'
            break;
        case 2:
            result = 'Giá trị cuối cùng'
            break;
        case 3:
            result = 'Giá trị đầu tiên'
            break;
        case 4:
            result = 'Giá trị lớn nhất'
            break; 
        default:
            result = 'Giá trị bé nhất'
            break;
    }
    return result;
}