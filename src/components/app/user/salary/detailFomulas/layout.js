import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import Header from '../component/header';
import { Utils, settingApp, AvatarCustom } from '../../../../../public';
import * as UtilsFomulas from './component/UtilsFomulas'

class Layout extends Component{

    renderHeader(){
        return(
            <Header 
                navigation={this.props.navigation}
                title=''
            />
        )
    }

    renderInfoTop(){
        const { data,  titlePerios} = this;
        let startDate = Utils.formatTime(titlePerios.fromDate, 'DD/MM/YYYY', true);
        let endDate = Utils.formatTime(titlePerios.toDate, 'DD/MM/YYYY', true);
        return(
            <View style={styles.coverInfoTop}>
                <View style={styles.nameInfo}>
                    <Text style={styles.formulaName}>{data.formulaName}</Text>
                    <Text style={styles.date}>{startDate ? startDate : '--/--'} - {endDate ? endDate : '--/--'}</Text>
                </View>
            </View>
        )
    }

    renderLineInfo(title, descrif){
        return(
            <View style={styles.coverUser}>
                <Text style={styles.textWeight}>{title}</Text>
                <Text style={styles.text}>{descrif}</Text>
            </View>
        )
    }   

    renderFomulas(){
        const { construs, dataJson } = this.state;
        return(
            <View style={styles.coverUser}>
                <Text style={styles.textWeight}>Công thức</Text>
                <View style={styles.coverContrus}>
                {dataJson.map((item, index)=>{
                    let color = this.checkColor(item)
                    return(
                        <Text
                        key={index}
                        style={{fontSize:14, color:color, paddingRight:3}}>{item.name}</Text>
                    )
                })}
                </View>
            </View>
        )
    }

    render(){
        const {colleague, data} = this
        let content = UtilsFomulas.setReportType(data.formula.reportType)
        return(
            <View style={{flex:1, backgroundColor:'#FFFFFF'}}>
                {this.renderHeader()}
                {this.renderInfoTop()}
                <View style={styles.line}/>
                <ScrollView>
                    <View style={styles.coverUser}>
                        <Text style={styles.textWeight}>Nhân viên</Text>
                        <View style={{flexDirection:'row', alignItems:'center', paddingTop:10}}>
                            <AvatarCustom userInfo={{ name: colleague.fullName, picture: colleague.photo }} v={40} fontSize={20}/>
                            <Text style={[styles.text,{marginLeft:10}]}>{colleague.fullName}</Text>
                        </View>
                    </View>

                    {this.renderLineInfo('Cách thể hiện thưởng', content)}
                    {this.renderLineInfo('Mô tả', data.formula.description)}
                    
                    {this.renderFomulas()}
                </ScrollView>
            </View>
        )
    }
}
const styles= StyleSheet.create({
    line:{
        width:settingApp.width,
        height:5,
        backgroundColor:'#EEEEEE'
    },
    coverInfoTop:{
        flexDirection:'row', 
        paddingLeft:15, 
        paddingRight:15, 
        minHeight:75, 
        width:settingApp.width, 
        justifyContent:'space-between', 
        alignItems:'center',
        borderTopColor:settingApp.colorSperator,
        borderTopWidth:1
    }, 
    nameInfo:{
        width:((settingApp.width-30) *0.7), 
        minHeight:75, 
        justifyContent:'center'
    },
    formulaName:{
        fontSize:18,
        color:settingApp.colorText,
        fontWeight:'600'
    },
    date:{
        fontSize:14,
        color:settingApp.colorDisable,
        paddingTop:5
    },
    textWeight:{
        fontSize:16, 
        color:settingApp.colorText, 
        fontWeight:'600'
    },
    coverUser:{
        width:settingApp.width - 30, 
        minHeight:80, 
        paddingLeft:15, 
        justifyContent:'center'
    },
    text:{
        fontSize:16, 
        color:settingApp.colorText
    },
    coverContrus:{
        flexDirection:'row', 
        alignItems:'center', 
        minHeight:40, 
        width:settingApp.width - 40, 
        paddingRight:10, 
        flexWrap:'wrap'
    }
})
export default Layout;