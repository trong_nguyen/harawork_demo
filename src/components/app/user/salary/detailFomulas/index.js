import Layout from './layout'
import { connect } from 'react-redux';
import { settingApp } from '../../../../../public';

class DetailFomulas extends Layout{
    constructor(props){
        super(props)
        this.state={
            construs: '',
            dataJson:[]
        }
        this.data = props.navigation.state.params.data;
        this.titlePerios = props.navigation.state.params.titlePerios
        this.colleague = props.app.colleague;
    }

    componentDidMount(){
        this.loadData()
    }

    checkColor(item){
        let color = null
        switch (item.type) {
            case 'element':
                color ='#30C894'
                break;
            case 'operator':
                color ='#FC625D'
                break;
            default: color = settingApp.colorText
                break;
        }
        return color
    }

    loadData(){
        const { data } = this
        if(data && data.formula && data.formula.traceInfo){
            let dataJson = JSON.parse(data.formula.traceInfo);
            let construs = ''
            this.setState({dataJson:dataJson})
            // for(let i = 0; i<dataJson.length; i++){
            //     construs = construs + ' ' + dataJson[i].name
            // }

            // console.log('this.data dataJson', dataJson);
            // console.log('this.data dataJson', construs);
        }
    }
}
mapStateToProps = state => ({ ...state });
export default connect(mapStateToProps)(DetailFomulas);