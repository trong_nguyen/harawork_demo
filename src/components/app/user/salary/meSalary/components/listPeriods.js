import React, { Component } from 'react';
import { View, Text, TouchableOpacity, FlatList, StyleSheet } from 'react-native';
import { settingApp } from '../../../../../../public';
import { Feather } from '@expo/vector-icons';
import Header from '../../component/header'

class ListPeriods extends Component{
    constructor(props){
        super(props)
        this.state={
            listData:[],
            titlePerios:''
        }
        this.data = props.navigation.state.params.data;
        this.titlePerios = props.navigation.state.params.titlePerios
        this.actions = props.navigation.state.params.actions
    }  

    componentDidMount(){
        if(this.data){
            this.setState({listData:this.data})
        }

        if(this.titlePerios){
            this.setState({ titlePerios:this.titlePerios})
        }
    }

    setItem(item){
        this.setState({titlePerios:item},() =>{
            this.actions(item)
            this.props.navigation.pop()
        })
    }

    renderHeader(){
        return(
            <Header 
                navigation={this.props.navigation}
                title={'Chu kỳ lương'}
            />
        )
    }

    renderItem(obj){
        let { titlePerios } = this.state;
        const {item, index} = obj;
        let check = titlePerios && (titlePerios.id === item.id);
        return(
            <TouchableOpacity 
                onPress={() => this.setItem(item)}
                style={styles.coverItem}>
                <Text>{item.salaryPeriodName}</Text>
                <View style={styles.iconCheck}>
                    <Feather name='check' size={25} color={check ? settingApp.blueSky : settingApp.colorSperator} />
                </View>
            </TouchableOpacity>
        )
    }

    render(){
        return(
            <View style={{flex:1, backgroundColor:'#FFFFFF'}}>
                {this.renderHeader()}
                <FlatList
                    style={{flex:1}}
                    data={this.state.listData}
                    extraData={this.state}
                    keyExtractor={(item,index) => ''+index}
                    renderItem={(obj) => this.renderItem(obj)}
                />

            </View>
        )
    }
}

const styles = StyleSheet.create({
    coverItem:{
        width:settingApp.width -30, 
        minHeight:60, 
        flexDirection:'row', 
        alignItems:'center', 
        justifyContent:'space-between', 
        marginLeft:15, 
        marginRight:15,
        borderBottomColor:settingApp.colorSperator,
        borderBottomWidth:1
    },
    iconCheck:{
        height:60, 
        width:50, 
        justifyContent:'center', 
        alignItems:'center'
    }
})
export default ListPeriods;