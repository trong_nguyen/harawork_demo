import React, { Component } from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import Collapsible from 'react-native-collapsible';
import { settingApp, SvgCP, Utils } from '../../../../../../public';
import { FlatList } from 'react-native-gesture-handler';
import { AntDesign } from '@expo/vector-icons'

class ItemFomulas extends Component{
    constructor(props){
        super(props)
        this.state={
            isLoadCount:true,
            totalCount: 0,
            isCollap:false
        }
    }

    componentDidMount(){
        const {item}= this.props;
        if(item && item.list && (item.list.length > 0)){
            let { list } = item;
            let number = 0
            for(let i = 0; i < list.length; i++){
                if(list[i].formula && (list[i].formula.isSumGroup ==true)){
                    number = number + (list[i].total*1)
                }
            }
                this.setState({totalCount:number})
        }
    }

    _renderItem(obj){
        const {item, index} = obj
        let color = item.total > 0 ? settingApp.colorText : '#FC625D'
        let formatSalary = Utils.formatNumber(item.total)
        return(
            <View style={[styles.mainCollap]}>
                <TouchableOpacity 
                    onPress={() => this.props.navigation.navigate('DetailSalary', 
                        {   data:item,
                            titlePerios:this.props.titlePerios
                        })}
                    style={[styles.buttonCollap]}>
                    <Text style={[styles.textSalaryCollap]}
                        >{item.formulaName}</Text>
                    
                    <View style={styles.lineLeft}>
                        <View style={[styles.coverSalaryCollap]}>
                            <Text style={[styles.SalaryCollap,{color:color}]}>{formatSalary}</Text>
                        </View>
                        <View style={styles.iconRown}>
                            <AntDesign name='right' size={15} color={settingApp.colorDisable} />
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
    
    render(){
        const { item } = this.props;
        const { totalCount, isCollap } = this.state;
        let color = (totalCount >= 0) ? settingApp.blueSky : '#FC625D';
        let formatSalary = Utils.formatNumber(totalCount)
        return(
            <View style={[{borderBottomWidth:1, borderBottomColor:settingApp.colorSperator, flex:1}]}>
                <TouchableOpacity 
                    onPress={() => this.setState({isCollap:!this.state.isCollap})}
                    style={styles.main}>
                    <View style={styles.coverFomulas}>
                        {!isCollap ? <SvgCP.iconPlus/> : <SvgCP.iconSheep/>}
                        <Text style={styles.textFomulas}>{item.name}</Text>
                    </View>
                    <Text style={[styles.textSalary,{color:color}]}>{formatSalary}</Text>
                    
                </TouchableOpacity>
                <Collapsible collapsed={!isCollap} style={{width:settingApp.width, minHeight:60, flex:1, backgroundColor:'transparent'}}>
                    {isCollap && <FlatList 
                        data={item.list}
                        extraData={this.state}
                        keyExtractor={(item, index) => index+''}
                        renderItem={(obj) => this._renderItem(obj)}
                        style={{flex:1}}
                    />}
                </Collapsible>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main:{
        width:settingApp.width, 
        minHeight:60, 
        flexDirection:'row', 
        justifyContent:'space-between', 
        alignItems:'center'
    },
    coverFomulas:{
        flexDirection:'row', 
        minHeight:60, 
        width:(settingApp.width *0.6), 
        alignItems:'center',
        paddingLeft:15
    },
    textFomulas:{
        fontSize:16, 
        fontWeight:'500', 
        color:settingApp.colorText, 
        paddingLeft:8
    },
    textSalary:{
        fontSize:16, 
        fontWeight:'600', 
        paddingRight:30,
    },
    mainCollap:{
        width:(settingApp.width-40), 
        minHeight:60, 
        flexDirection:'row', 
        justifyContent:'space-between', 
        alignItems:'center',
    },
    buttonCollap:{
        width:(settingApp.width-45), 
        minHeight:60, 
        flexDirection:'row', 
        justifyContent:'space-between', 
        alignItems:'center',
        marginLeft:45,
        borderTopColor:settingApp.colorSperator, 
        borderTopWidth:1,
        
    },
    textSalaryCollap:{
        fontSize:16,
        paddingRight:5,
        color:settingApp.colorText, 
        width:(settingApp.width-40)*0.5, 
    },
    SalaryCollap:{
        fontSize:16,
    },
    coverSalaryCollap:{
        flexDirection:'row', 
        minHeight:60, 
        width:((settingApp.width -40) *0.3),
        alignItems:'center', 
        justifyContent:'flex-end'
    },
    iconRown:{
        alignItems:'center', 
        justifyContent:'center', 
        height:60, 
        width:28
    },
    lineLeft:{
        flexDirection:'row', 
        minHeight:60, 
        width:((settingApp.width -45) *0.4),
    }
})
export default ItemFomulas;