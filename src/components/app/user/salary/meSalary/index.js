import Layout from './layout'
import { HaravanHr } from '../../../../../Haravan';

class MeSalary extends Layout{
    constructor(props){
        super(props)
        this.state={
            isCollap:false,
            isLoadPeriod:true,
            isLoadSalary:true,
            listPeriod:[],
            listSalary:[],
            totalSalary: 0,
            titlePerios:null,
            disable: true,
            notPermission:false
        }
    }

    async componentDidMount(){
        await this.loadPeriod()
        await this.loadFormulas()
    }

    getItemTitle(item){
        this.setState({titlePerios:item, isLoadSalary:true, listSalary:[]},async() =>{
            await this.loadFormulas()
        })
    }

    checkResults(result){
        if(result && result.data && !result.error){
            return result
        }
        else{
            return false
        }
    }

    checkColor(data){
        let color = '#30C894';
        if((data * 1) < 0){
            color = '#FC625D'
        }
        else{
            color = color
        }
        return color
    }

    async loadPeriod(){
        let result = await HaravanHr.getPeriodsAll();
        if(result && result.data && !result.error){
            this.setState({listPeriod:result.data, isLoadPeriod:false, titlePerios:result.data[0], disable:false})
        }
        else{
            this.setState({isLoadPeriod:false})
        }
    }

    async loadFormulas(){
        const { titlePerios }= this.state
        if(titlePerios){
            let result = await HaravanHr.getSalaryFormulagroups(1);
            const resultMe = await HaravanHr.getMeSalary(titlePerios.id);
            result = this.checkResults(result)
            let listFillter = [];
            let grouspFormula = [];
            let totalSalary =0
            if(resultMe && resultMe.data && !resultMe.error){
                const { data } = resultMe.data;
                for(let i  = 0; i < data.length; i++){
                    if(data[i].salaries && (data[i].salaries.length > 0)){
                        let salaries = data[i].salaries;
                        for(let e = 0; e < salaries.length; e++){
                            if(salaries[e].formula && salaries[e].formula.formulaGroupId && salaries[e].formula.visible){
                                listFillter.push(salaries[e])
                            }
                        }
                        totalSalary = totalSalary + data[i].totalSalary
                    }   
                }
                if(listFillter && (listFillter.length > 0) && result){
                    const {data} = result.data
                    for(let i = 0; i < data.length; i++){
                        listFillter.map(item =>{
                            if(item.formula.formulaGroupId == data[i].id){
                                let index = grouspFormula.findIndex(m => m.id == data[i].id)
                                if(index < 0){
                                    grouspFormula.push(data[i])
                                }
                            }
                        })
                    }
                }

                if(grouspFormula && (grouspFormula.length > 0)){
                    for(let i = 0; i < grouspFormula.length; i ++){
                        grouspFormula[i] = {...grouspFormula[i], list:[]}
                        listFillter.map(item =>{
                            let list= []
                            if(item.formula.formulaGroupId == grouspFormula[i].id){
                                list.push(item)
                                grouspFormula[i] = { ...grouspFormula[i], list:[...grouspFormula[i].list, ...list]}
                            }
                        })
                    }
                }
                
                this.setState({isLoadSalary:false, listSalary:grouspFormula, totalSalary:totalSalary})
            }
            else if(resultMe.error && (resultMe.status === 403)){
                this.setState({notPermission:true})
            }
            else{
                this.setState({isLoadSalary:false})
            }
        }
    }
}
export default MeSalary;