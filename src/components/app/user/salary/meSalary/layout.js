import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ScrollView, FlatList, Image } from 'react-native';
import { HeaderNewStyles, settingApp , SvgCP, Loading, imgApp, NotPermission} from '../../../../../public'
import { AntDesign } from '@expo/vector-icons'
import ItemFomulas from './components/itemFomulas'
import { ActivityIndicator } from 'react-native-paper';

class Layout extends Component{

    renderHeader(){
        return(
            <HeaderNewStyles
                title={<View
                            style={styles.heeader}
                        >
                          <Text style={styles.titleHeader}>Thu nhập của tôi</Text>
                        </View>
                    }
            />
        )
    }

    renderNoneData(){
        return(
            <View style={styles.noneData}>
                <Image
                    source={imgApp.frameNone} 
                    style={{width:settingApp.width*0.5, height:(settingApp.width*0.5), resizeMode:'contain'}}
                />
                <Text style={styles.textNoneData}> Chưa có dữ liệu về thu nhập </Text>
            </View>
        )
    }

    renderPeriod(){
        const { listPeriod, isLoadPeriod, titlePerios, disable } = this.state
        return(
            <TouchableOpacity 
                disabled={disable}
                onPress={() => this.props.navigation.navigate('ListPeriods', 
                    {
                        data:listPeriod,
                        titlePerios: this.state.titlePerios,
                        actions:(item) => this.getItemTitle(item)
                    })}
                style={[styles.coverPeriod,{ opacity:disable ? 0.5 :1}]}>
                <View style={styles.infoPer}>
                    <Text style={styles.textPer1}>Chu kỳ lương</Text>
                    {titlePerios ? 
                        <Text style={styles.textPer2}>{titlePerios.salaryPeriodName}</Text>:
                        <ActivityIndicator style={styles.indicator} size='small' color={settingApp.colorDisable}/>
                    }
                    
                </View>

                <View style={styles.buttonPer}>
                    <AntDesign name='right' size={15} color={settingApp.colorDisable} />
                </View>
            </TouchableOpacity>
        )
    }

    renderContent(){
        let {totalSalary, isLoadSalary, listSalary} = this.state
        let color = this.checkColor(totalSalary)
        return(
            <View style={{flex:1}}>
            {(!isLoadSalary && (listSalary.length > 0)) &&
                <View style={styles.viewTotalSalary}>
                    <Text style={{fontSize:16, color:settingApp.colorText, fontWeight:'600'}}>Tổng thu nhập</Text>
                    <Text style={{fontSize:18, color:color, fontWeight:'bold', paddingRight:15}}>{totalSalary}</Text>
                </View>
            }
                {this.listFormulas()}
            </View>
        )
    }

    listFormulas(){
        const { listSalary, isLoadSalary } = this.state;
        if(!isLoadSalary && (listSalary.length > 0)){
            return(
                <View style={{flex:1}}>
                    <FlatList 
                        data={listSalary}
                        extraData={this.state}
                        keyExtractor={(item, index) => ''+index}
                        renderItem={(obj) => this._renderItem(obj)}
                    />
                </View>
            )
        }
        else if(!isLoadSalary && (listSalary.length == 0)){
            return (
                <View style={{flex:1}}>
                    {this.renderNoneData()}
                </View>
            )
        }
        else{
            return(
                <View style={styles.loading}>
                    <Loading />
                </View>
            )
        }
        
    }

    _renderItem(obj){
       const {item, index} = obj
        return(
            <ItemFomulas 
                item={item}
                navigation={this.props.navigation}
                titlePerios={this.state.titlePerios}
            />
        )
    }

    renderBody(){
        const {listPeriod, isLoadPeriod} = this.state;
        if(!isLoadPeriod && (listPeriod.length == 0)){
            return(
                <View style={{flex:1}}>
                    {this.renderNoneData()}
                </View>
            )
        }
        else{
            return(
                <View style={{flex:1}}>
                    {this.renderPeriod()}
                    <View style={styles.line}/>
                    <ScrollView>
                        {this.renderContent()}
                    </ScrollView>
                </View>
            )
        }
        
    }

    render(){
        const { notPermission } = this.state;
        if(!notPermission){
            return(
                <View style={{flex:1, backgroundColor:'#FFFFFF'}}>
                    {this.renderHeader()}
                    {this.renderBody()}
                </View>
            )
        }
        else{
            return <NotPermission navigation={this.props.navigation}/>
        }
    }
}

const styles = StyleSheet.create({
    heeader:{
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center', 
        flexDirection: 'row'
    },
    titleHeader:{
        fontSize:20, 
        color:(settingApp.colorText), 
        fontWeight:'bold'
    },
    coverPeriod:{
        backgroundColor:'#FFFFFF',
        width:settingApp.width, 
        minHeight:75, 
        flexDirection:'row',
        borderTopColor:settingApp.colorSperator,
        borderTopWidth:1,
        ...settingApp.shadow,
        justifyContent:'space-between'
    },
    infoPer:{
        height:75,
        width:settingApp.width-80,
        paddingLeft:15,
        justifyContent:'center'        
    },
    buttonPer:{
        height:75,
        width:50,
        justifyContent:'center',
        alignItems:'center'
    },
    textPer1:{
        fontSize:14, 
        fontWeight:'600', 
        color:settingApp.colorText
    },
    textPer2:{
        fontSize:16, 
        color:settingApp.colorText
    },
    line:{
        width:settingApp.width,
        height:5,
        backgroundColor:'#EEEEEE'
    },
    viewTotalSalary:{
        flexDirection:'row', 
        justifyContent:'space-between', 
        alignItems:'center', 
        padding:15, 
        width:settingApp.width, 
        height:50, 
        backgroundColor:'#F9F9F9',
        borderBottomColor:settingApp.colorSperator,
        borderEndWidth:1
    },
    loading:{
        flex:1, 
        width:settingApp.width, 
        height:80, 
        justifyContent:'center', 
        alignItems:'center'
    },
    noneData:{
        width:settingApp.width, 
        height:settingApp.width, 
        justifyContent:'center', 
        alignItems:'center'
    },
    textNoneData:{
        fontSize:16, 
        color:settingApp.colorText, 
        paddingTop:10, 
        textAlign:'center'
    },
    indicator:{
        alignItems:'flex-start', 
        backgroundColor:'transparent'
    }
})
export default Layout