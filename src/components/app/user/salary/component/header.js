import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StatusBar, Platform, StyleSheet } from 'react-native';
import { settingApp } from '../../../../../public';
import { AntDesign } from '@expo/vector-icons'

class Header extends Component{
    constructor(props){
        super(props)
    }

    render(){
        let {navigation, title} = this.props
        return(
            <View style={[styles.Header]}>
                <StatusBar barStyle={Platform.OS === 'ios' ? 'dark-content' : 'default'}/>
                <View style={[styles.statusBar]} />
                <View style={styles.coverHeader} >
                    <TouchableOpacity
                        onPress={() => this.props.navigation.pop()}
                        style={styles.backHeader}
                    >
                        <AntDesign name='left' size={25} color={settingApp.colorText} />
                    </TouchableOpacity> 
                        
                    <Text 
                        numberOfLines={1}
                        style={{ fontSize: 17, color:settingApp.colorText, fontWeight: '600', marginLeft:15, width:(settingApp.width -100)}}>
                        {title}
                    </Text>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    Header:{
        width:settingApp.width,
        height:44 + settingApp.statusBarHeight,
        backgroundColor:'#FFFFFF',
        ...settingApp.shadow
    },
    statusBar:{
        width:settingApp.width, 
        height: settingApp.statusBarHeight,
    },
    coverHeader:{
        width:settingApp.width, 
        height: 44, 
        flexDirection: 'row', 
        alignItems:'center'
    },
    infoHeader:{
        flex: 1, 
        backgroundColor: 'transparent', 
        flexDirection: 'row', 
        alignItems:'center' 
    },
    textHeader:{
        fontSize:14, 
        color:settingApp.colorText,
        paddingLeft:5
    },
    backHeader:{
        height:44, 
        justifyContent:'center', 
        alignItems:'center', 
        flexDirection:'row', 
        marginLeft:10
    },
})
export default Header;