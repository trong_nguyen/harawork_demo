import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { settingApp, Loading } from '../../../../../../public';

import Header from './components/header';
import Input from './components/input';
import ItemList from './components/itemList'

class Layout extends Component {

    renderContent() {
        const { activeList, list } = this.state;
        return (
            <ScrollView>
                {(activeList.length > 0) && <View style={{ height: 30, justifyContent: 'center', paddingLeft: 15 }}>
                    <Text style={{ fontSize: 13, color: '#8E8E93', fontWeight: 'bold' }}>Người theo dõi</Text>
                </View>}
                <View style={{ backgroundColor: '#ffffff', ...settingApp.shadow }}>
                    {activeList.map((item, index) => {
                        return (
                            <View
                                key={index}
                                style={{
                                    borderTopColor: settingApp.colorSperator,
                                    borderTopWidth: index === 0 ? 0 : 1
                                }}
                            >
                                <ItemList
                                    isActive={true}
                                    idActive={this.state.idActive}
                                    setIdActive={() => this.setState({ idActive: null })}
                                    item={item}
                                    indexKey={index}
                                    activeItem={value => this.activeItem(false, value)}
                                />
                            </View>
                        )
                    })}
                </View>
                {(list.length > 0) && <View style={{ height: 30, justifyContent: 'center', paddingLeft: 15 }}>
                    <Text style={{ fontSize: 13, color: '#8E8E93', fontWeight: 'bold' }}>Kết quả tìm kiếm</Text>
                </View>}
                <View style={{ backgroundColor: '#ffffff', ...settingApp.shadow }}>
                    {list.map((item, index) => {
                        return (
                            <View key={index}
                                style={{
                                    borderTopColor: settingApp.colorSperator,
                                    borderTopWidth: index === 0 ? 0 : 1
                                }}>
                                <ItemList
                                    item={item}
                                    indexKey={index}
                                    activeItem={value => this.activeItem(true, value)}
                                />
                            </View>
                        )
                    })}
                </View>
            </ScrollView>
        )
    }

    render() {
        const { onFocus, isLoading, isLoadEnd, activeList } = this.state;
        return (
            <View style={{ flex: 1 }}>
                <Header
                    navigation={this.props.navigation}
                    activeList={activeList}
                />
                <Input
                    ref={refs => this.input = refs}
                    onFocus={onFocus => this.setState({ onFocus })}
                    getValueSearch={(text) => this.getValueSearch(text)} />
                <View style={{ flex: 1 }}>
                    {this.renderContent()}
                    {onFocus &&
                        <Animatable.View
                            animation='fadeIn'
                            duration={350}
                            ref={refs => this.view = refs}
                            style={{
                                position: 'absolute', backgroundColor: 'rgba(0,0,0,0.5)', top: 0, left: 0, right: 0, bottom: 0
                            }}>
                            <TouchableOpacity
                                disabled={isLoading}
                                onPress={async () => this.onBlur()} style={{ flex: 1 }}>
                                {isLoading ?
                                    <Loading />
                                    :
                                    (isLoadEnd && this.initList.length === 0 && this.isText) ?
                                        <View style={{ flex: 1, alignItems: 'center' }}>
                                            <Text style={{ color: '#ffffff', fontSize: 15, marginTop: 20 }}>Không có kết quả phù hợp</Text>
                                        </View>
                                        :
                                        <View />
                                }
                            </TouchableOpacity>
                        </Animatable.View>
                    }
                </View>
            </View>
        )
    }
}

export default Layout;