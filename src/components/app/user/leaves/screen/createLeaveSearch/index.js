import Layout from './layout';
import { HaravanHr } from '../../../../../../Haravan';

class CreateLeaveSearch extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            list: [],
            activeList: props.navigation.state.params.listUserWatch || [],
            onFocus: false,
            isLoading: false,
            isLoadEnd: false,
            idActive: null
        }

        this.initList = [];
        this.isText = true;
    }


    async getValueSearch(text) {
        if (text) {
            this.isText = true;
            await this.setState({ isLoading: true, isLoadEnd: false });
            const result = await HaravanHr.searchAllColleague(text);
            if (result && result.data && !result.error) {
                this.initList = [...result.data.data];
                this.setState({ list: result.data.data, isLoading: false, isLoadEnd: true });
            }

        } else {
            this.isText = false;
            await this.setState({ isLoading: false, isLoadEnd: true });
        }
    }

    async onBlur() {
        const result = await this.view.fadeOut();
        if (result.finished) {
            this.input.onBlur();
        }
    }

    activeItem(status, item) {
        const { activeList, list } = this.state;
        if (status) {
            const index = activeList.findIndex(e => e.id === item.id);
            if (index > -1) {
                this.setState({ idActive: item.id })
            } else {
                activeList.push(item);
                this.setState({ activeList, list });
            }
        } else {
            const index = activeList.findIndex(e => e.id === item.id);
            activeList.splice(index, 1);
            this.setState({ activeList });
        }
    }
}

export default CreateLeaveSearch;