import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput } from 'react-native';
import { settingApp } from '../../../../../../../public';
import { Ionicons } from '@expo/vector-icons';
import lodash from 'lodash';

class Input extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: ''
        }

        this.onChangeText = lodash.debounce(this.search, 500);
    }

    search() {
        const { value } = this.state;
        if (value.length > 0) {
            this.props.getValueSearch(value)
        } else {
            this.props.getValueSearch(null);
        }
    }

    onBlur() {
        this.input.blur()
    }

    render() {
        const { width } = settingApp;
        const { value } = this.state;
        const isRemove = !!(value && value.length > 0);
        return (
            <View style={{ width, height: 70, padding: 15 }}>
                <View style={{ width: (width - 30), height: 40 }}>
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', paddingLeft: 10, paddingRight: 10, borderRadius: 10, backgroundColor: 'rgba(142, 142, 147, 0.24)' }}>
                        <Ionicons name='ios-search' size={23} color='#8E8E93' />
                        <TextInput
                            autoFocus={true}
                            ref={refs => this.input = refs}
                            value={value}
                            onFocus={() => this.props.onFocus(true)}
                            onBlur={() => this.props.onFocus(false)}
                            onChangeText={value => this.setState({ value }, this.onChangeText.bind(this))}
                            style={{ flex: 1, marginLeft: 5 }}
                            underlineColorAndroid='transparent'
                        />
                        {isRemove && <TouchableOpacity
                            onPress={() => this.setState({ value: '' }, this.onChangeText.bind(this))}
                            style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'flex-end' }}>
                            <Ionicons name='ios-close-circle' color='#8E8E93' size={23} />
                        </TouchableOpacity>}
                    </View>
                </View>
            </View>
        )
    }
}

export default Input;