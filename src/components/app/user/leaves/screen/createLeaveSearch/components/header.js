import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { HeaderWapper } from '../../../../../../../public';
import { Ionicons } from '@expo/vector-icons';

class Header extends Component {

    goBack() {
        this.props.navigation.state.params.action(this.props.activeList);
        this.props.navigation.pop();
    }

    render() {
        return (
            <HeaderWapper style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                <TouchableOpacity
                    onPress={() => this.goBack()}
                    style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                    <Ionicons name='ios-arrow-back' size={23} color='#ffffff' />
                </TouchableOpacity>
                <Text style={{ fontSize: 16, color: '#ffffff', fontWeight: 'bold' }}>
                    Người theo dõi
                </Text>
                <View style={{ width: 50, height: 44, backgroundColor: 'transparent' }} />
            </HeaderWapper>
        )
    }
}

export default Header;