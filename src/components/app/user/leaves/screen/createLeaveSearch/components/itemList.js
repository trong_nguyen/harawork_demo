import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { EvilIcons } from '@expo/vector-icons';
import { settingApp } from '../../../../../../../public';
import * as Animatable from 'react-native-animatable';

class ItemList extends Component {

    async componentWillReceiveProps(nextProps) {
        if (nextProps.idActive && (nextProps.idActive === this.props.item.id)) {
           await this.view.bounceIn();
           this.props.setIdActive();
        }
    }

    render() {
        const { isActive, item, indexKey } = this.props;
        return (
            <Animatable.View
                ref={refs => this.view = refs}
                style={{ flex: 1, paddingLeft: 15 }}>
                <TouchableOpacity
                    onPress={() => this.props.activeItem(item)}
                    style={{ flex: 1 }}
                >
                    <View style={{
                        flex: 1, flexDirection: 'row',
                        padding: 10,
                        paddingRight: 15
                    }}>
                        <View style={{ flex: 0.8, marginLeft: -10 }}>
                            <Text style={{ color: '#212121', fontSize: 15 }}>{item.fullName}</Text>
                            <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{item.jobtitleName}</Text>
                        </View>
                        <View style={{ flex: 0.2, justifyContent: 'center', alignItems: 'flex-end' }}>
                            {isActive ?
                                <EvilIcons name='close' color='#FF3B30' size={30} /> :
                                <EvilIcons name='plus' color={settingApp.color} size={30} />}
                        </View>
                    </View>
                </TouchableOpacity>
            </Animatable.View>
        )
    }
}

export default ItemList;