import Layout from './layout';
import { HaravanHr } from '../../../../../../Haravan';

class CreateLeaveTypeLeave extends Layout {
    constructor(props) {
        super(props);

        const { data, title, selected } = props.navigation.state.params;

        this.data = data;
        this.title = title;
        this.selected = selected;

    }

    async componentDidMount() {

    }

    select(index) {
        this.props.navigation.state.params.actions(index);
        this.props.navigation.pop();
    }


}

export default CreateLeaveTypeLeave;