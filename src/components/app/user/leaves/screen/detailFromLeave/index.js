import Layout from './layout';
import { Alert } from 'react-native';
import { HaravanHr } from '../../../../../../Haravan';
import { Toast, Utils } from '../../../../../../public';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../../../state/action';

class DetailFormLeave extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            modalVisible: false,
            isRequestLeave: props.navigation.state.params.isRequestLeave
        }

        this.item = props.navigation.state.params.item;
    }

    checkRemove() {
        Alert.alert(
            'Thông báo',
            'Bạn có chắc chắn muốn xoá phiếu nghỉ phép này?',
            [
                { text: 'Đóng', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'Xóa phiếu', onPress: () => this.remove(), style: 'destructive' },
            ],
            { cancelable: false }
        )
    }

    remove() {
        this.setState({ isLoading: true }, async () => {
            const result = await HaravanHr.removeLeave(this.item.id);
            if (result && result.data && !result.error) {
                this.props.actions.leave.removeLeave(this.item.id);
                Toast.show('Xóa thành công');
                this.props.navigation.pop();
            } else {
                Alert.alert(
                    'Thông báo',
                    'Xảy ra lỗi vui lòng thử lại?',
                    [
                        { text: 'Đóng', onPress: () => console.log('Error: remove leave'), style: 'cancel' },
                    ],
                    { cancelable: false }
                )
            }
            this.setState({ isLoading: false });
        });
    }

    suggest(note) {
        this.setState({ isLoading: true }, async () => {
            const { id, from, approverId, employeeId } = this.item;
            const requestApproveDate = Utils.formatTime(from, 'YYYY-MM-DD HH:mm', true);
            const requestData = JSON.stringify(this.item)
            const data = {
                RequestData:requestData,
                dataId: id,
                requestApproveDate,
                requestNote: note,
                requesterId: employeeId,
                status: 1,
                type: 0
            }
            const result = await HaravanHr.suggestRemoveLeave(data);
            if (result && result.data && !result.error) {
                this.setState({ isRequestLeave: true });
                this.props.actions.leave.suggestRemove(this.item.id);
            } else {
                Alert.alert(
                    'Thông báo',
                    'Xảy ra lỗi vui lòng thử lại?',
                    [
                        { text: 'Đóng', onPress: () => console.log('Error: remove leave'), style: 'cancel' },
                    ],
                    { cancelable: false }
                )
            }
            this.setState({ isLoading: false })
        });
    }
}

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(mapStateToProps, mapDispatchToProps)(DetailFormLeave);