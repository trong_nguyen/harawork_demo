import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { HeaderWapper, settingApp, Loading } from '../../../../../../public';
import { Ionicons } from '@expo/vector-icons';
import Status from './components/status';
import Detail from './components/detail';
import SuggestInput from './components/suggestInput';

class Layout extends Component {
    renderHeader() {
        return (
            <HeaderWapper style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                <TouchableOpacity
                    onPress={() => this.props.navigation.pop()}
                    style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                    <Ionicons name='ios-arrow-back' size={23} color='#ffffff' />
                </TouchableOpacity>
                <Text style={{ fontSize: 16, color: '#ffffff', fontWeight: 'bold' }}>
                    Chi tiết phiếu nghỉ phép
                </Text>
                <View style={{ width: 40, height: 44 }} />
            </HeaderWapper>
        )
    }

    renderButton(status) {
        if (status === 1) { // chưa duyệt
            return (
                <View style={{ flex: 1, flexDirection: 'row', paddingLeft: 15, padding: 15, paddingTop: 5, backgroundColor: '#ffffff' }}>
                    <TouchableOpacity
                        onPress={() => this.checkRemove()}
                        style={{ flex: 1, marginRight: 5, backgroundColor: '#FF3B30', height: 50, justifyContent: 'center', alignItems: 'center', borderRadius: 3 }}>
                        <Text style={{ color: '#ffffff', fontSize: 15, fontWeight: 'bold' }}>Xoá phiếu</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('UpdateLeave', { item: { ... this.item } })}
                        style={{ flex: 1, marginRight: 5, backgroundColor: settingApp.color, height: 50, justifyContent: 'center', alignItems: 'center', borderRadius: 3 }}>
                        <Text style={{ color: '#ffffff', fontSize: 15, fontWeight: 'bold' }}>Điều chỉnh</Text>
                    </TouchableOpacity>
                </View>
            )
        } else if (status === 2) { // đã duyệt
            return (
                <View style={{ flex: 1, flexDirection: 'row', paddingLeft: 15, padding: 15, paddingTop: 5, backgroundColor: '#ffffff' }}>
                    <TouchableOpacity
                        onPress={() => this.setState({ modalVisible: true })}
                        style={{ flex: 1, marginRight: 5, backgroundColor: '#FF3B30', height: 50, justifyContent: 'center', alignItems: 'center', borderRadius: 3 }}>
                        <Text style={{ color: '#ffffff', fontSize: 15, fontWeight: 'bold' }}>Đề nghị huỷ</Text>
                    </TouchableOpacity>
                </View>
            )
        } else if (status === 3) {
            return (
                <View style={{ flex: 1, flexDirection: 'row', paddingLeft: 15, padding: 15, paddingTop: 5, backgroundColor: '#ffffff' }}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('CreateLeave', { leaveType: null })}
                        style={{ flex: 1, marginRight: 5, backgroundColor: settingApp.color, height: 50, justifyContent: 'center', alignItems: 'center', borderRadius: 3 }}>
                        <Text style={{ color: '#ffffff', fontSize: 15, fontWeight: 'bold' }}>Tạo mới</Text>
                    </TouchableOpacity>
                </View>
            )
        }
        else {
            return <View />
        }
    }

    renderContent() {
        const { status } = this.item;
        return (
            <ScrollView style={{ paddingTop: 15 }}>
                <Status
                    status={this.item.status}
                    approverNote={this.item.approverNote}
                    isRequestLeave={this.state.isRequestLeave}
                />
                <View style={{ ...settingApp.shadow }}>
                    <Detail
                        detail={this.item}
                        leaveType={this.props.navigation.state.params.leaveType}
                    />
                    {!this.state.isRequestLeave && this.renderButton(status)}
                </View>
            </ScrollView>
        )
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {this.renderHeader()}
                <View style={{ flex: 1 }}>
                    {this.renderContent()}
                </View>
                {this.state.isLoading && <Loading />}
                <SuggestInput
                    modalVisible={this.state.modalVisible}
                    close={() => this.setState({ modalVisible: false })}
                    suggest={note => this.suggest(note)}
                />
            </View>
        )
    }
}

export default Layout;