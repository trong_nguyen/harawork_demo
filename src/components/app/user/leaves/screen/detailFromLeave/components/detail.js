import React, { Component } from 'react';
import { View, Text } from 'react-native';
import moment  from 'moment';

class Detail extends Component {

    getAboutTime(typeTimeLeave) {
        let titleTimeLeave = '';
        switch (typeTimeLeave) {
            case 0:
                titleTimeLeave = 'Nghỉ nguyên ngày'
                break;
            case 1:
                titleTimeLeave = 'Nghỉ theo giờ'
                break;
            case 2:
                titleTimeLeave = 'Nghỉ buổi sáng'
                break;
            case 3:
                titleTimeLeave = 'Nghỉ buổi chiều'
                break;
            default:
                titleTimeLeave = 'Không xác định'
                break;
        }
        return titleTimeLeave;
    }

    render() {
        const { shiftName, reason, follower, from, to, typeTimeLeave } = this.props.detail;
        const calculateTime = `${moment(from).format('HH:mm ngày DD/MM/YYYY')} - ${moment(to).format('HH:mm ngày DD/MM/YYYY')}`;
        const checkfollower = follower && follower.length > 0 ? follower : null;
        const aboutTime = this.getAboutTime(typeTimeLeave);
        return (
            <View style={{ flex: 1, marginTop: 15 }}>
                <View style={{
                    flex: 1,
                    backgroundColor: '#ffffff',
                    paddingTop: 10,
                    paddingLeft: 15,
                    paddingRight: 15
                }}>
                    <View style={{ marginBottom: 10 }}>
                        <Text style={{ color: '#000', fontSize: 15 }}>Ca làm việc</Text>
                        <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{shiftName}</Text>
                    </View>
                    <View style={{ marginBottom: 10 }}>
                        <Text style={{ color: '#000', fontSize: 15 }}>Loại phép</Text>
                        <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{this.props.leaveType}</Text>
                    </View>
                    <View style={{ marginBottom: 10 }}>
                        <Text style={{ color: '#000', fontSize: 15 }}>Thời gian nghỉ</Text>
                        <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{aboutTime}</Text>
                    </View>
                    <View style={{ marginBottom: 10 }}>
                        <Text style={{ color: '#000', fontSize: 15 }}>Khoảng thời gian</Text>
                        <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{calculateTime}</Text>
                    </View>
                    <View style={{ marginBottom: 10 }}>
                        <Text style={{ color: '#000', fontSize: 15 }}>Lý do nghỉ phép</Text>
                        <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{reason}</Text>
                    </View>
                    <View style={{ marginBottom: 10 }}>
                        <Text style={{ color: '#000', fontSize: 15 }}>Người duyệt</Text>
                        <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{this.props.detail.approverName} - {this.props.detail.approverUserId} - {this.props.detail.approverJobtitleName}</Text>
                    </View>
                    {checkfollower ? <View style={{ marginBottom: 10 }}>
                        <Text style={{ color: '#000', fontSize: 15 }}>Người theo dõi</Text>
                        {follower.map((e, i) => (
                            <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }} key={i}>
                                {e.employeeName} -  {e.employeeUserId} - {e.employeeJobTitleName}
                            </Text>
                        ))}
                    </View> : <View />}
                </View>
            </View>
        )
    }
}

export default Detail;