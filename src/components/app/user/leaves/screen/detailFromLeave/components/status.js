import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { settingApp } from '../../../../../../../public';

class Status extends Component {

    checkStatus(status) {
        let color, title = '';
        switch (status) {
            case 1:
                color = '#FF9500';
                title = 'Chưa duyệt';
                break;
            case 2:
                if (this.props.isRequestLeave) {
                    color = '#FF9500';
                    title = 'Đề nghị huỷ';
                } else {
                    color = '#4CD964';
                    title = 'Đã duyệt';
                }
                break;
            case 3:
                color = '#FF3B30';
                title = 'Từ chối';
                break;
            default:
                break;
        }
        return { color, title };
    }

    render() {
        titleStatus = this.checkStatus(this.props.status);
        const checkApproverNote = this.props.approverNote && this.props.approverNote.length && this.props.approverNote.length > 0;
        return (
            <View style={{
                flex: 1,
                backgroundColor: '#ffffff',
                paddingLeft: 15,
                ...settingApp.shadow
            }}>
                <View style={{
                    flex: 1,
                    height: 50,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    paddingRight: 15
                }}>
                    <Text style={{ color: '#000', fontSize: 15 }}>Trạng thái</Text>
                    <Text style={{ color: titleStatus.color, fontSize: 15 }}>{titleStatus.title}</Text>
                </View>
                {checkApproverNote && <View style={{
                    flex: 1,
                    borderTopColor: settingApp.colorSperator,
                    borderTopWidth: 1,
                    marginBottom: 10,
                    paddingTop: 10,
                    paddingRight: 15
                }}>
                    <Text style={{ color: '#000', fontSize: 15 }}>Ghi chú người duyệt</Text>
                    <Text style={{ color: '#8E8E93', fontSize: 13, marginTop: 5 }}>{this.props.approverNote}</Text>
                </View>}

            </View>
        )
    }
}

export default Status;