import React, { Component } from 'react';
import { View, Text, Modal, ScrollView, TextInput, TouchableOpacity } from 'react-native';
import { settingApp } from '../../../../../../../public';

class SuggestInput extends Component {
    constructor(props) {
        super(props);

        this.state = {
            note: ''
        }
    }

    render() {
        const width = (settingApp.width * 0.75);
        const marginLeft = (settingApp.width * 0.125);
        const text = this.state.note.trim();
        const disabled = text.length === 0;
        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={this.props.modalVisible}
                onRequestClose={() => this.props.close}
            >
                <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.5)' }}>
                    <ScrollView scrollEnabled={false} >
                        <View style={{ flex: 1, marginTop: 100, width, marginLeft, backgroundColor: '#ffffff', borderRadius: 15 }}>
                            <View style={{ width, height: 40, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: '#000', fontSize: 15, fontWeight: 'bold' }}>Lý do huỷ</Text>
                            </View>
                            <Text style={{ fontSize: 13, textAlign: 'center' }}>
                                Bạn vui lòng cho biết lý do huỷ phiếu nghỉ phép này
                            </Text>
                            <View style={{ height: 100, padding: 10 }}>
                                <TextInput
                                    value={this.state.note}
                                    onChangeText={note => this.setState({ note })}
                                    multiline={true}
                                    style={{ flex: 1, maxHeight: 80, padding: 5, textAlignVertical: 'top', borderColor: settingApp.colorSperator, borderWidth: 1 }}
                                    underlineColorAndroid='transparent'
                                    selectionColor={settingApp.color}
                                />
                            </View>
                            <View style={{ marginTop: 5, flexDirection: 'row', borderTopColor: settingApp.colorSperator, borderTopWidth: 1 }}>
                                <TouchableOpacity
                                    onPress={() => this.props.close()}
                                    style={{ flex: 1, height: 50, borderRightColor: settingApp.colorSperator, borderRightWidth: 1, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ fontSize: 15, color: settingApp.color }}>Đóng</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.suggest(this.state.note);
                                        this.props.close();
                                    }}
                                    disabled={disabled}
                                    style={{ flex: 1, height: 50, opacity: disabled ? 0.3 : 1, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ fontSize: 15, color: '#FF3B30' }}>Xác nhận</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </Modal>
        )
    }

}

export default SuggestInput;