import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { ButtonCustom, settingApp, HeaderWapper } from '../../../../../../public';
import { Ionicons } from '@expo/vector-icons';

class Layout extends Component {

    renderHeader() {
        const { leaveTypeName } = this.item;
        return (
            <HeaderWapper style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                <TouchableOpacity
                    onPress={() => this.props.navigation.pop()}
                    style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                    <Ionicons name='ios-arrow-back' size={23} color='#ffffff' />
                </TouchableOpacity>
                <Text style={{ fontSize: 16, color: '#ffffff', fontWeight: 'bold' }}>
                    {leaveTypeName}
                </Text>
                <View style={{ width: 40, height: 44 }} />
            </HeaderWapper>
        )
    }

    render() {
        const { item } = this;
        return (
            <View style={{ flex: 1 }}>
                {this.renderHeader()}
                <ScrollView style={{ paddingTop: 15 }}>
                    <View style={{ flex: 1, backgroundColor: '#ffffff', paddingLeft: 15, paddingRight: 15, ...settingApp.shadow }}>
                        <View style={{ height: 50, justifyContent: 'center', borderBottomColor: settingApp.colorSperator, borderBottomWidth: 1 }}>
                            <Text style={{ fontSize: 15, color: '#000' }}>Tổng phép: {item.total}</Text>
                        </View>
                        <View style={{ height: 50, justifyContent: 'center', borderBottomColor: settingApp.colorSperator, borderBottomWidth: 1 }}>
                            <Text style={{ fontSize: 15, color: '#000' }}>Đã sử dụng: {item.used}</Text>
                        </View>
                        <View style={{ height: 50, justifyContent: 'center', borderBottomColor: settingApp.colorSperator, borderBottomWidth: 1 }}>
                            <Text style={{ fontSize: 15, color: '#000' }}>Đang chờ duyệt: {item.waitingApproval}</Text>
                        </View>
                        <View style={{ height: 50, justifyContent: 'center', borderBottomColor: settingApp.colorSperator, borderBottomWidth: 1 }}>
                            <Text style={{ fontSize: 15, color: '#000' }}>Đã bị từ chối: {item.reject}</Text>
                        </View>
                        <View style={{ height: 50, justifyContent: 'center', borderBottomColor: settingApp.colorSperator, borderBottomWidth: 1 }}>
                            <Text style={{ fontSize: 15, color: '#000' }}>Được cộng thêm: {item.add}</Text>
                        </View>
                        <View style={{ height: 50, justifyContent: 'center', borderBottomColor: settingApp.colorSperator, borderBottomWidth: 1 }}>
                            <Text style={{ fontSize: 15, color: '#000' }}>Bị trừ: {item.subtract}</Text>
                        </View>
                        <View style={{ height: 50, justifyContent: 'center' }}>
                            <Text style={{ fontSize: 15, color: '#000' }}>Phép còn lại: {item.usable}</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, backgroundColor: '#ffffff', marginTop: 15, padding: 15, ...settingApp.shadow }}>
                        <ButtonCustom
                            onPress={() => this.props.navigation.navigate('CreateLeave', { leaveType: { ...item, id: item.leaveTypeId, name: item.leaveTypeName } })}
                            style={{ height: 55, backgroundColor: settingApp.color, justifyContent: 'center', alignItems: 'center', borderRadius: 3 }}>
                            <Text style={{ fontSize: 15, color: '#ffffff', fontWeight: 'bold' }}>
                                Tạo phiếu nghỉ phép
                            </Text>
                        </ButtonCustom>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

export default Layout;