import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { settingApp, HeaderWapper } from '../../../../../../public';
import { Ionicons } from '@expo/vector-icons';

class Layout extends Component {

    renderHeader() {
        return (
            <HeaderWapper style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                <TouchableOpacity
                    onPress={() => this.props.navigation.pop()}
                    style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                    <Ionicons name='ios-arrow-back' size={23} color='#ffffff' />
                </TouchableOpacity>
                <Text style={{ fontSize: 16, color: '#ffffff', fontWeight: 'bold' }}>
                    {this.title}
                </Text>
                <View style={{ width: 50, height: 44, backgroundColor: 'transparent' }} />
            </HeaderWapper>
        )
    }

    renderContent() {
        const { data, selected } = this;
        const { color, colorSperator } = settingApp;
        return (
            <ScrollView style={{ paddingTop: 15 }}>
                <View style={{ backgroundColor: '#ffffff', paddingLeft: 15, ...settingApp.shadow }}>
                    {data.map((item, index) => {
                        const isCheck = (selected === index);
                        return (
                            <View
                                key={index}
                                style={{
                                    height: 55,
                                    paddingTop: 5,
                                    paddingBottom: 5,
                                    borderTopColor: colorSperator,
                                    borderTopWidth: index === 0 ? 0 : 1
                                }}
                            >
                                <TouchableOpacity
                                    onPress={() => this.select(index)}
                                    style={{ flex: 1, flexDirection: 'row', }}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center', flex: 0.1, borderRightColor: colorSperator, borderRightWidth: 1 }}>
                                        {isCheck && <Ionicons name='ios-checkmark' size={30} color={color} />}
                                    </View>
                                    <View style={{ flex: 1, paddingLeft: 15, paddingRight: 15, justifyContent: 'center' }}>
                                        <Text style={{ fontSize: 15, color: '#212121' }}>{`${item.fullName} - ${item.userId}`}</Text>
                                        <Text style={{ fontSize: 13, color: '#8E8E93', marginTop: 5 }}>{item.mainPosition.jobtitleName}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        )
                    })}
                </View>
                <View style={{ height: 30, backgroundColor: 'transparent' }} />
            </ScrollView>
        )
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {this.renderHeader()}
                <View style={{ flex: 1 }}>
                    {this.renderContent()}
                </View>
            </View>
        )
    }
}

export default Layout;