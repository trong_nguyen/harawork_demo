import React, { Component } from 'react';
import { ActivityIndicator, View, Text, TouchableOpacity } from 'react-native';
import { settingApp, Item, Error } from '../../../../../../public';
import { HaravanHr } from '../../../../../../Haravan';
import { Ionicons } from '@expo/vector-icons';

class Manager extends Component {
    constructor(props) {
        super(props);

        this.state = {
            indexUserApproved: 0,
            usetWatch: null,
            isLoadEnd: false
        }

        this.dataUserApproved = null;
        this.dataHrApproved = null;

        this.userApproved = (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                <ActivityIndicator size='small' color={settingApp.color} />
            </View>
        );

        this.userWatch = (
            <TouchableOpacity
                onPress={() => this.props.navigation.navigate('CreateLeaveSearch', {
                    listUserWatch: this.props.userWatch,
                    action: list => this.props.listUserWatch(list)
                })}
                style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                <Ionicons name='ios-arrow-forward' size={23} color='#D1D1D6' />
            </TouchableOpacity>
        );

        this.dataApproved = null;
        this.isFrist = true;
    }

    checkData(input) {
        if (input && input.data && !input.error) {
            return input;
        }
        return false;
    }

    componentWillReceiveProps(nextProps) {
        if (!this.isFrist) {
            this.loadData();
        } else {
            this.isFrist = false;
        }
    }

    async componentDidMount() {
        const { checkData } = this;
        let userApproved = await HaravanHr.userApproved();
        let hrApproved = await HaravanHr.hrApproved();
        if (checkData(userApproved) && checkData(hrApproved)) {
            userApproved = userApproved && userApproved.data && userApproved.data.length > 0 ? userApproved.data :
                hrApproved && hrApproved.data && hrApproved.data.length > 0 ? hrApproved.data : null;
            this.dataUserApproved = userApproved;
            this.props.updateDate('userApproved', userApproved ? userApproved[0] : null);
            this.loadData();
        } else {
            this.userApproved = (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                    <Error />
                </View>
            )
        }
    }

    loadData() {
        const { indexUserApproved } = this.state;
        const userApproved = this.dataUserApproved;
        let data = userApproved;
        let name = '';
        if (this.props.userWatch.length === 1) {
            name = this.props.userWatch[0].fullName;
        } else if (this.props.userWatch.length > 1) {
            this.props.userWatch.map((e, i) => name = `${name}${i > 0 ? ',': ''} ${e.fullName}`);
            name = name.trim();
        }
        if (data) {
            this.userApproved = (
                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('CreateLeaveItemSelect', {
                        data,
                        title: 'Người duyệt',
                        selected: indexUserApproved,
                        actions: indexUserApproved => this.setState({ indexUserApproved }, () => {
                            this.props.updateDate('userApproved', data[indexUserApproved]);
                            this.loadData();
                        })
                    })}
                    style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 10 }}>
                        <Text style={{ color: '#8E8E93', fontSize: 15 }} numberOfLines={1}>{`${data[indexUserApproved].fullName} - ${data[indexUserApproved].userId}`}</Text>
                    </View>
                    <Ionicons name='ios-arrow-forward' size={23} color='#D1D1D6' />
                </TouchableOpacity>
            );

            this.dataApproved = data;
        } else {
            this.userApproved = (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                    <Text style={{ fontSize: 12, color: '#bdc3c7' }} numberOfLines={1}>Không tìm thấy người duyệt</Text>
                </View>
            )
        }

        this.userWatch = (
            <TouchableOpacity
                onPress={() => this.props.navigation.navigate('CreateLeaveSearch', {
                    listUserWatch: this.props.userWatch,
                    action: list => this.props.listUserWatch(list)
                })}
                style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                {(name.length > 0) && <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 10 }}>
                    <Text style={{ color: '#8E8E93', fontSize: 15 }} numberOfLines={1}>{name}</Text>
                </View>}
                <Ionicons name='ios-arrow-forward' size={23} color='#D1D1D6' />
            </TouchableOpacity>
        );
        
        this.setState({ isLoadEnd: true });
    }

    render() {
        return (
            <View style={{ flex: 1, paddingLeft: 15 }}>
                <Item
                    name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Người duyệt</Text>}
                    value={this.userApproved}
                    noneBorder={true}
                />
                <Item
                    name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Người theo dõi</Text>}
                    value={this.userWatch}
                />
            </View>
        );
    }
}

export default Manager;