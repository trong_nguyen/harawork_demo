import React, { Component } from 'react';
import { View, Text, TextInput, ScrollView, KeyboardAvoidingView } from 'react-native';
import { ButtonCustom, settingApp, Loading } from '../../../../../public';

import Header from './components/header';
import Manager from './components/manager';
import Form from './components/form';
import Time from './components/time';

class Layout extends Component {

    renderContent() {
        return (
            <ScrollView style={{ paddingTop: 15 }}>
                <View style={{ flex: 1, backgroundColor: '#ffffff', ...settingApp.shadow }}>
                    <Manager
                        userWatch={this.state.listUserWatch}
                        navigation={this.props.navigation}
                        listUserWatch={listUserWatch => this.setState({ listUserWatch })}
                        updateDate={(type, data) => this.updateDate(type, data)}
                    />
                </View>

                <View style={{ flex: 1, backgroundColor: '#ffffff', marginTop: 15, ...settingApp.shadow }}>
                    <Form
                        leaveType={this.props.navigation.state.params.leaveType}
                        navigation={this.props.navigation}
                        checkInStatus={this.props.checkIn.checkInStatus}
                        updateDate={(type, data) => this.updateDate(type, data)}
                        getData={(type, data) => this.getData(type, data)}
                    />
                    <Time
                        navigation={this.props.navigation}
                        updateDate={(type, data, isHalfDay) => this.updateDate(type, data, isHalfDay)}
                        actuaLeave={this.state.actuaLeave}
                        isLoadTotaltime={this.state.isLoadTotaltime}
                    />

                    <View style={{
                        marginLeft: 15,
                        paddingRight: 15,
                        borderTopColor:settingApp.colorSperator,
                        borderTopWidth: 1
                    }}>
                        <View style={{ flex: 1, height: 120, marginTop: 10 }}>
                            <TextInput
                                ref={refs => this.note = refs}
                                placeholder='Lý do'
                                multiline={true}
                                style={{ flex: 1, maxHeight: 120, textAlignVertical: 'top' }}
                                underlineColorAndroid='transparent'
                                selectionColor={settingApp.color}
                            />
                        </View>
                    </View>

                    <View style={{ flex: 1, padding: 15 }}>
                        <ButtonCustom
                            disabled={this.state.disabled}
                            onPress={() => this.leave()}
                            style={{ height: 55, backgroundColor: this.state.disabled ? settingApp.colorDisable : settingApp.color, justifyContent: 'center', alignItems: 'center', borderRadius: 3 }}>
                            <Text style={{ fontSize: 15, color: '#ffffff', fontWeight: 'bold' }}>
                                Tạo
                            </Text>
                        </ButtonCustom>
                    </View>
                </View>

                <View style={{ height: 40 }} />
            </ScrollView>
        )
    }


    render() {
        return (
            <KeyboardAvoidingView behavior='padding' style={{ flex: 1 }}>
                <Header navigation={this.props.navigation} />
                <View style={{ flex: 1 }}>
                    {this.renderContent()}
                </View>
                {this.state.isLoading && <Loading />}
            </KeyboardAvoidingView>
        )
    }
}

export default Layout;