import React, { Component } from 'react';
import { ActivityIndicator, View, Text, TouchableOpacity, Image, ScrollView } from 'react-native';
import { Item, settingApp, imgApp, Utils, Error } from '../../../../../../../public';
import { MaterialIcons } from '@expo/vector-icons';
import DatePicker from '../../../../../checkinUser/history/components/datePicker';

class Layout extends Component {

    render() {
        const { dataLeaveType } = this;
        const { isLoadEnd, indexLeaveType } = this.state;

        const date = new Date();
        const formDate = new Date(date.getFullYear(), date.getMonth(), 1);
        return (
            <ScrollView scrollEnabled={false}>
                <View style={{ flex: 1, backgroundColor: '#ffffff', paddingLeft: 15, ...settingApp.shadow, marginBottom: 20 }}>
                    <Item
                        name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Từ ngày</Text>}
                        value={(
                            <TouchableOpacity
                                onPress={() => this.setState({ isVisibleFormDate: true })}
                                style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                                <Text style={{ fontSize: 15, color: '#8E8E93', marginRight: 10 }}>
                                    {this.state.valueFromDate ? Utils.formatTime(this.state.valueFromDate, 'DD/MM/YYYY', true) : 'Vui lòng chọn ngày'}
                                </Text>
                                <Image
                                    source={imgApp.calendar}
                                    style={{ width: 20, height: 20, tintColor: '#D1D1D6' }}
                                    resizeMode='stretch'
                                />
                            </TouchableOpacity>
                        )}
                        noneBorder={true}
                    />

                    <Item
                        name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Đến ngày</Text>}
                        value={(
                            <TouchableOpacity
                                onPress={() => this.setState({ isVisibleToDate: true })}
                                style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                                <Text style={{ fontSize: 15, color: '#8E8E93', marginRight: 10 }}>
                                    {this.state.valueTodate ? Utils.formatTime(this.state.valueTodate, 'DD/MM/YYYY', true) : 'Vui lòng chọn ngày'}
                                </Text>
                                <Image
                                    source={imgApp.calendar}
                                    style={{ width: 20, height: 20, tintColor: '#D1D1D6' }}
                                    resizeMode='stretch'
                                />
                            </TouchableOpacity>
                        )}
                    />

                    <Item
                        name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Loại phép</Text>}
                        value={isLoadEnd ? dataLeaveType ? (
                            <TouchableOpacity
                                onPress={
                                    () =>
                                        this.props.navigation.navigate('ItemSelect',
                                            {
                                                data: dataLeaveType,
                                                title: 'Loại phép',
                                                selected: indexLeaveType,
                                                actions: index => this.setState({ indexLeaveType: index })
                                            }
                                        )
                                }
                                style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 10 }}>
                                    <Text style={{ fontSize: 15, color: '#8E8E93' }} numberOfLines={1}>
                                        {(indexLeaveType || indexLeaveType === 0) ? dataLeaveType[indexLeaveType].name : 'Tất cả'}
                                    </Text>
                                </View>
                                <MaterialIcons name='keyboard-arrow-right' size={23} color='#D1D1D6' />
                            </TouchableOpacity>
                        ) : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                                <Error />
                            </View> : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                                <ActivityIndicator size='small' color={settingApp.color} />
                            </View>
                        }
                    />

                    <View style={{
                        flex: 1,
                        borderTopColor: settingApp.colorSperator,
                        borderTopWidth: 1,
                        paddingTop: 20,
                        paddingRight: 15,
                        marginBottom: 10
                    }}>
                        <TouchableOpacity
                            onPress={() => this.submit()}
                            disabled={!this.props.removeFilter}
                            style={{
                                flex: 1,
                                height: 55,
                                backgroundColor: !this.props.removeFilter ? settingApp.colorDisable : settingApp.color,
                                justifyContent: 'center',
                                alignItems: 'center',

                            }}>
                            <Text style={{ color: '#ffffff', fontSize: 15, fontWeight: 'bold' }}>Áp dụng</Text>
                        </TouchableOpacity>
                    </View>

                    <DatePicker
                        value={this.state.valueFromDate || formDate}
                        validate={value => this.validate('fromDate', value)}
                        isVisible={this.state.isVisibleFormDate}
                        close={() => this.setState({ isVisibleFormDate: false })}
                    />

                    <DatePicker
                        value={this.state.valueTodate || new Date()}
                        validate={value => this.validate('toDate', value)}
                        isVisible={this.state.isVisibleToDate}
                        close={() => this.setState({ isVisibleToDate: false })}
                    />
                </View>
            </ScrollView>
        )
    }
}

export default Layout;