import Layout from './layout';
import { HaravanHr } from '../../../../../../../Haravan';
import { Utils } from '../../../../../../../public';

class Condition extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            valueFromDate: null,
            isVisibleFormDate: false,
            valueTodate: null,
            isVisibleToDate: false,
            isLoadEnd: false,
            indexLeaveType: null
        }

        this.formDate = null;
        this.todate = null;
        this.dataLeaveType = null;
    }

    async componentDidMount() {
        const result = await HaravanHr.leaveType();
        if (result && result.data && !result.error) {
            this.dataLeaveType = result.data.data;
        }
        this.setState({ isLoadEnd: true });
    }

    validate(type, value) {
        if (type === 'fromDate') {
            if (!this.state.valueTodate || !!(value <= this.state.valueTodate)) {
                this.setState({ valueFromDate: value });
            } else {
                this.setState({ valueTodate: value, valueFromDate: value });
            }
        } else {
            if (!!(value >= this.state.valueFromDate)) {
                this.setState({ valueTodate: value });
            } else {
                this.setState({ valueTodate: value, valueFromDate: value });
            }
        }
    }

    formatTime(value) {
        return Utils.formatTime(value, 'DD/MM/YYYYY');
    }

    setInitCondition() {
        this.setState({
            valueFromDate: this.formDate,
            valueTodate: this.todate,
            indexLeaveType: null
        });

        this.props.setConditon(null);
    }

    componentDidUpdate() {
        const { valueFromDate, valueTodate, indexLeaveType } = this.state;
        if (this.formatTime(valueFromDate) === this.formatTime(this.formDate) &&
            this.formatTime(valueTodate) === this.formatTime(this.todate) &&
            (indexLeaveType !== 0 && !indexLeaveType) && this.props.removeFilter) {
            this.props.changeRemoveFilter(false);
        } else if ((this.formatTime(valueFromDate) !== this.formatTime(this.formDate) ||
            this.formatTime(valueTodate) !== this.formatTime(this.todate) ||
            (indexLeaveType === 0 || indexLeaveType)) && !this.props.removeFilter) {
            this.props.changeRemoveFilter(true);
        }
    }

    submit() {
        let { valueFromDate, valueTodate, indexLeaveType } = this.state;
        valueFromDate = valueFromDate ? Utils.formatTime(valueFromDate, 'YYYY-MM-DD', true) : null;
        valueTodate = valueTodate ? Utils.formatTime(valueTodate, 'YYYY-MM-DD', true) : null;
        const leaveType = (indexLeaveType || indexLeaveType === 0) ? this.dataLeaveType[indexLeaveType].id : null;
        const condition = `fromDate=${valueFromDate}&toDate=${valueTodate}&leaveType=${leaveType}`;
        this.props.setConditon(condition);
    }
}

export default Condition;