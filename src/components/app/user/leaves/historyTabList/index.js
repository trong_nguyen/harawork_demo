import Layout from './layout';
import { Utils } from '../../../../../public';
import { HaravanHr } from '../../../../../Haravan';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../../state/action';

class LeaveTabList extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            list: [],
            isLoadEnd: false,
            refreshing: false
        }

        this.url = props.url;
        this.condition = props.condition;
        this.navigation = props.mainNavigation;
    }

    async componentDidMount() {
        this.checkCondition();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.condition !== this.condition) {
            this.condition = nextProps.condition;
            this.checkCondition(nextProps.condition);
        }
        if (!this.props.leave.updateHistory && nextProps.leave.updateHistory) {
            this.checkCondition(this.condition);
            this.props.actions.leave.updateHistory(null);
        }
    }

    async loadData(condition) {
        const result = await HaravanHr.leaveHistory(this.url, condition);
        if (result && result.data && !result.error) {
            const count = result.data.data.length;
            this.props.navigation.setParams({ count });
            this.setState({ list: result.data.data, isLoadEnd: true, refreshing: false });
        } else {
            this.setState({ isLoadEnd: true, refreshing: false })
        }
    }

    checkCondition(condition) {
        if (condition) {
            this.loadData(condition);
        } else {
            const date = new Date();
            const formDate = new Date(date.getFullYear(), date.getMonth(), 1);
            condition = `fromDate=${Utils.formatTime(formDate, 'YYYY-MM-DD', true)}`;
            this.loadData(condition);
        }
    }

}

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(mapStateToProps, mapDispatchToProps)(LeaveTabList);