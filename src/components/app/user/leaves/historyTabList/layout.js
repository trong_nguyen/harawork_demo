import React, { Component } from 'react';
import { View, FlatList, RefreshControl } from 'react-native';
import { Loading, settingApp, NotFound } from '../../../../../public';
import Item from './components/item';

class Layout extends Component {

    render() {
        const { list, isLoadEnd } = this.state;
        if (!isLoadEnd) {
            return (
                <View style={{ flex: 1 }}>
                    <Loading />
                </View>
            )
        } else {
            return (
                <View style={{ flex: 1, paddingTop: 15 }}>
                    <FlatList
                        data={list}
                        extraData={this.state}
                        keyExtractor={(item, index) => `${item.id}`}
                        refreshControl={<RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={() => {
                                this.setState({ refreshing: true }, () =>
                                    this.checkCondition(this.condition))
                            }}
                        />}
                        renderItem={obj => <Item obj={{ ...obj }} navigation={this.navigation} />}
                        ListFooterComponent={<View style={{ height: 15, borderTopColor: settingApp.colorSperator, borderTopWidth: 1 }} />}
                        ListEmptyComponent={<NotFound />}
                        contentContainerStyle={{flexGrow: 1,  paddingBottom: 2, ...settingApp.shadow }}
                        removeClippedSubviews={true}
                    />
                </View>
            )
        }
    }
}

export default Layout;