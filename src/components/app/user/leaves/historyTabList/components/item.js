import React, { Component } from 'react';
import { ActivityIndicator, View, Text } from 'react-native';
import { ButtonCustom, settingApp, Utils, Error } from '../../../../../../public';
import { MaterialIcons } from '@expo/vector-icons';

import { connect } from 'react-redux';

class Item extends Component {
    constructor(props) {
        super(props);

        let leave = (
            <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                <ActivityIndicator size='small' />
            </View>
        );
        let isLoadEnd = false;

        if (props.leave && props.leave.listLeave.list && !props.leave.listLeave.isError) {
            const result = this.renderLeaveType(props.leave);
            leave = result.leave;
            isLoadEnd = result.isLoadEnd;
        } else if (props.leave && props.leave.listLeave.list && props.leave.listLeave.isError) {
            leave = (
                <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                    <Error />
                </View>
            );
            isLoadEnd = true;
        }

        let isRequestLeave = false;
        if (props.obj.item.requestLeave.length > 0) {
            const { requestLeave } = props.obj.item;
            let list = [];
            requestLeave.map(e => {
                if ((e.status === 1 && e.type === 0)) {
                    list.push(e)
                };
            });
            if (list.length > 0 && props.obj.item.status === 2) {
                isRequestLeave = true;
            }
        }

        this.state = {
            leave,
            isLoadEnd,
            isShow: true,
            isRequestLeave
        }

    }

    renderLeaveType(leave) {
        const { list } = leave.listLeave;
        const { leaveTypeId, typeTimeLeave, to, from } = this.props.obj.item;
        let countDay = 0;
        if (to && from) {
            countDay = Math.floor((new Date(to) - new Date(from) + 60000) / ((1000 * 60 * 60 * 24)));
        }
        let titleTimeLeave = '';
        switch (typeTimeLeave) {
            case 0:
                titleTimeLeave = countDay >= 2 ? `Nghỉ ${countDay} ngày` : 'Nghỉ nguyên ngày'
                break;
            case 1:
                titleTimeLeave = 'Nghỉ theo giờ'
                break;
            case 2:
                titleTimeLeave = 'Nghỉ buổi sáng'
                break;
            case 3:
                titleTimeLeave = 'Nghỉ buổi chiều'
                break;
            default:
                titleTimeLeave = 'Không xác định'
                break;
        }
        const index = list.findIndex(e => e.id === leaveTypeId);
        const leaveType = list[index].name;
        leave = (
            <View style={{ flex: 1 }}>
                <Text
                    style={{ color: '#8E8E93', fontSize: 13, marginTop: 5 }}
                    numberOfLines={1}
                >
                    {leaveType} - {titleTimeLeave}
                </Text>
            </View>
        );
        isLoadEnd = true;
        this.leaveType = leaveType;
        return { leave, isLoadEnd }
    }

    componentWillReceiveProps(nextProps) {
        if ((nextProps.leave.removeLeave === this.props.obj.item.id) && this.state.isShow) {
            this.setState({ isShow: false });
        }
        if ((nextProps.leave.suggestRemove === this.props.obj.item.id) && !this.state.isRequestLeave) {
            this.setState({ isRequestLeave: true });
        }
        if (nextProps && nextProps.leave && nextProps.leave.listLeave && nextProps.leave.listLeave.list && !this.state.isLoadEnd) {
            leave = <View />;
            isLoadEnd = <View />;
            if (!props.leave.listLeave.isError) {
                leave = result.leave;
                isLoadEnd = result.isLoadEnd;
            } else {
                leave = (
                    <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                        <Error />
                    </View>
                );
                isLoadEnd = true;
            }

            this.setState({
                leave,
                isLoadEnd
            });
        }
    }

    checkStatus(status) {
        let color, title = '';
        const { isRequestLeave } = this.state;
        switch (status) {
            case 1:
                color = '#FF9500';
                title = 'Chưa duyệt';
                break;
            case 2:
                if (isRequestLeave) {
                    color = '#FF9500';
                    title = 'Đề nghị huỷ';
                } else {
                    color = '#4CD964';
                    title = 'Đã duyệt';
                }
                break;
            case 3:
                color = '#FF3B30';
                title = 'Từ chối';
                break;
            default:
                break;
        }
        return { color, title };
    }

    render() {
        if (this.state.isShow) {
            const { colorSperator } = settingApp;
            const { item, index } = this.props.obj;
            let from = Utils.formatLocalTime(item.from, 'dddd, DD/MM/YYYY', true);
            from = from.charAt(0).toUpperCase() + from.slice(1);
            const to = Utils.formatLocalTime(item.to, 'dddd, DD/MM/YYYY', true);
            const countDay = Math.floor((new Date(item.to) - new Date(item.from) + 60000) / ((1000 * 60 * 60 * 24)));
            const titleStatus = this.checkStatus(item.status);
            if (titleStatus) {
                return (
                    <ButtonCustom
                        onPress={() => this.props.navigation.navigate('DetailFormLeave', { item, isRequestLeave: this.state.isRequestLeave, leaveType: this.leaveType })}
                        style={{ flex: 1, paddingLeft: 15, backgroundColor: '#ffffff' }}>
                        <View style={{
                            paddingRight: 15,
                            paddingTop: 10,
                            paddingBottom: 10,
                            borderTopColor: colorSperator,
                            borderTopWidth: index === 0 ? 0 : 1
                        }}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <View style={{ flex: 1 }}>
                                    {
                                        (item.actualLeavingTime < 1) ?
                                            <Text style={{ color: '#000', fontSize: 15 }} numberOfLines={1}>{from}</Text>
                                            :
                                            countDay >= 2 ?
                                                <View>
                                                    <Text style={{ color: '#000', fontSize: 15 }} numberOfLines={1}>{from}</Text>
                                                    <Text style={{ color: '#000', fontSize: 15, marginTop: 5 }} numberOfLines={1}>đến {to}</Text>
                                                </View>
                                                :
                                                <Text style={{ color: '#000', fontSize: 15 }} numberOfLines={1}>{from}</Text>
                                    }
                                </View>
                                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end' }}>
                                    <Text style={{ color: titleStatus.color, fontSize: 15, marginRight: 5 }}>{titleStatus.title}</Text>
                                    <MaterialIcons name='keyboard-arrow-right' size={23} color='#D1D1D6' />
                                </View>
                            </View>
                            <View style={{ flex: 1 }}>
                                {this.state.leave}
                            </View>
                        </View>
                    </ButtonCustom>
                )
            } else {
                return <View />
            }
        } else {
            return <View />
        }
    }

}


const mapStateToProps = (state) => ({ ...state });
export default connect(mapStateToProps)(Item);