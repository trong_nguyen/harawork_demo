import Layout from './layout';
import { Alert } from 'react-native';
import { HaravanHr } from '../../../../../Haravan';
import { Utils, Toast } from '../../../../../public';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../../state/action';

class UpdateLeave extends Layout {
    constructor(props) {
        super(props);

        const { item } = props.navigation.state.params;

        listUserWatch = item.follower.map(e => ({
            fullName: e.employeeName,
            id: e.employeeId,
            jobtitleName: e.employeeJobTitleName
        }));

        this.state = {
            isLoading: false,
            disabled: false,
            listUserWatch,
            actuaLeave: item.actualLeavingTime,
            isLoadTotaltime: false,
            note: item.reason
        }

        this.shift = { id: item.shiftId };
        this.formDate = Utils.localTime(item.from);
        this.toDate = (item.actualLeavingTime && item.actualLeavingTime < 1) ? Utils.localTime(item.from) : Utils.localTime(item.to);
        this.item = item;
        this.isLoadEnd = false;

        this.timefrom = '00:00';
        this.timeto = '23:59';
    }

    getData(type, data) {
        if (type == 'detailLeave') {
            this.detailLeave = data;
        } else if (type == 'dataLeaveType') {
            this.dataLeaveType = data;
        }
    }

    updateDate(type, data, isHalfDay) {
        switch (type) {
            case 'dataTime':
                if (!this.dataTime || (this.dataTime && this.dataTime.value !== data.value)) {
                    this.dataTime = data;
                    this.getactualeave();
                }
                break;
            case 'shift':
                if (!this.shift || (this.shift && JSON.stringify(this.shift) !== JSON.stringify(data))) {
                    this.shift = data;
                    this.getactualeave();
                }
                break;
            case 'formDate':
                if (!this.formDate || (this.formDate && this.formDate !== data)) {
                    this.formDate = data;
                    if (isHalfDay) {
                        this.toDate = data;
                    }
                    this.getactualeave();
                }
                break;
            case 'toDate':
                if (!this.toDate || (this.toDate && this.toDate !== data)) {
                    this.toDate = data;
                    this.getactualeave();
                }
                break;
            case 'leaveType':
                this.leaveType = data;
                break;
            case 'userApproved':
                this.userApproved = data;
                break;
            default:
                break;
        }

        this.valiDate();
        this.setTime();
    }

    setTime() {
        const { dataTime, shift } = this;
        if (dataTime && shift) {
            if (shift && shift.timeframes && shift.timeframes.length > 0) {
                const { timeframes } = shift;
                if (dataTime.value == 2) {
                    this.timefrom = timeframes[0].checkInTime;
                    this.timeto = timeframes[0].relaxBeginTime;
                } else if (dataTime.value === 3) {
                    this.timefrom = timeframes[0].relaxEndTime;
                    this.timeto = timeframes[0].checkOutTime;
                }
            }
        }
    }

    valiDate() {
        const { formDate, toDate, leaveType } = this;
        let isSubmit = true;
        if (!leaveType || (leaveType && !leaveType.id)) {
            isSubmit = false;
        } else if (!formDate) {
            isSubmit = false;
        } else if (!toDate) {
            isSubmit = false;
        }
        if (isSubmit) {
            this.setState({ disabled: false })
        } else if (!this.state.disabled) {
            this.setState({ disabled: true })
        }
    }

    async getactualeave() {
        const { dataTime, shift, formDate, toDate, isLoadEnd } = this;
        if (isLoadEnd) {
            if (dataTime && shift && formDate && toDate) {
                await this.setState({ isLoadTotaltime: true });
                const timeleave = dataTime.value;
                const employeeId = this.props.app.colleague.id;
                const valueFromDate = Utils.formatTime(formDate, 'YYYY-MM-DD HH:mm', true);
                const valueToDate = Utils.formatTime(toDate, 'YYYY-MM-DD HH:mm', true);
                const shiftId = shift.id;
                const result = await HaravanHr.getactualeave(timeleave, employeeId, valueFromDate, valueToDate, shiftId);
                if (result && result.data && !result.error) {
                    this.setState({ actuaLeave: result.data.actuaLeave, isLoadTotaltime: false });
                } else {
                    alert('Lỗi tạo phép')
                    this.setState({ isLoadTotaltime: false });
                }
            }
        } else {
            this.isLoadEnd = true;
        }
    }


    alert(detail) {
        Alert.alert(
            'Thông báo',
            `${detail}`,
            [
                { text: 'Đồng ý', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false }
        )
    }

    leave() {
        const { formDate, toDate, leaveType, userApproved, timefrom, timeto } = this;
        const { listUserWatch, actuaLeave } = this.state;
        const approverId = userApproved.id;
        const { departmentId } = this.props.checkIn.checkInStatus;
        const note = this.state.note ? this.state.note.trim() : null;
        let follower = [];
        if (listUserWatch.length > 0) {
            follower = listUserWatch.map(e => ({ EmployeeId: e.id, Employees: { ...e } }));
        }
        if ((!note || (note && note.length && note.length === 0))) {
            this.alert('Vui lòng nhập lý do nghỉ phép.');
        } else {
            const indexDetaiLeave = this.detailLeave.findIndex(e => e.leaveTypeId === leaveType.id);
            let usable = null;
            if (indexDetaiLeave > -1) {
                usable = this.detailLeave[indexDetaiLeave];
            }
            const data = {
                actualLeavingTime: actuaLeave,
                approverId,
                departmentId,
                follower,
                from: `${Utils.formatTime(formDate, 'YYYY-MM-DD', true)} ${timefrom}`,
                isHourly: false,
                leaveTypeId: leaveType.id,
                reason: note,
                shiftId: this.shift.id,
                to: `${Utils.formatTime(toDate, 'YYYY-MM-DD', true)} ${timeto}`
            };
            if ((!usable && usable !== 0) || ((usable || usable === 0) && actuaLeave <= usable)) {
                this.setState({ isLoading: true }, async () => {
                    const checkTime = `${Utils.formatTime(formDate, 'YYYY-MM-DD', true)}`;
                    const checkLockSalary = await HaravanHr.checkLockSalary(checkTime);
                    if (checkLockSalary && checkLockSalary.data && !checkLockSalary.error) {
                        if (!checkLockSalary.data.isLock) {
                            const result = await HaravanHr.updateLeave(this.item.id, data);
                            if (result && result.data && !result.error) {
                                this.openHistory();
                                Toast.show('Điều chỉnh phiếu nghỉ phép thành công');
                            } else if (result.error && result.message && result.message.length > 0) {
                                Toast.show(result.message);
                            } else {
                                Toast.show('Điều chỉnh phiếu nghỉ phép thất bại');
                            }
                        } else {
                            Toast.show(checkLockSalary.data.messges);
                        }
                    } else {
                        console.log('Error');
                        this.alert('Xảy ra lỗi Điều chỉnh phép, vui lòng thử lại');
                    }
                    this.setState({ isLoading: false })
                });
            } else {
                Alert.alert(
                    'Thông báo',
                    `Bạn chỉ có thể tạo tối đa ${usable} ngày phép với loại phép này. Khoảng thời gian còn lại, hệ thống sẽ tự động tách thành một phiếu nghỉ phép không lương.`,
                    [
                        { text: 'Đóng', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                        { text: 'Đồng ý', onPress: () => this.notUsable(data) },
                    ],
                    { cancelable: false }
                )
            }
        }
    }

    notUsable(data) {
        const { formDate } = this;
        this.setState({ isLoading: true }, async () => {
            const checkTime = `${Utils.formatTime(formDate, 'YYYY-MM-DD', true)}`;
            const checkLockSalary = await HaravanHr.checkLockSalary(checkTime);
            if (checkLockSalary && checkLockSalary.data && !checkLockSalary.error) {
                if (!checkLockSalary.data.isLock) {
                    const result = await HaravanHr.updateLeave(this.item.id, data, true);
                    if (result && result.data && !result.error) {
                        this.openHistory();
                        Toast.show('Điều chỉnh phiếu nghỉ phép thành công');
                    } else if (result.error && result.message && result.message.length > 0) {
                        Toast.show(result.message);
                    } else {
                        Toast.show('Điều chỉnh phiếu nghỉ phép thất bại');
                    }
                } else {
                    Toast.show(checkLockSalary.data.messges);
                }
            } else {
                console.log('Error');
                this.alert('Xảy ra lỗi Điều chỉnh phép, vui lòng thử lại');
            }
            this.setState({ isLoading: false })
        });
    }

    openHistory() {
        this.props.navigation.pop();
        this.props.actions.leave.updateHistory(true);
        this.props.navigation.navigate('HistoryLeave');
    }

}

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(mapStateToProps, mapDispatchToProps)(UpdateLeave);