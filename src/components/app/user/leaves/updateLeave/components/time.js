import React, { Component } from 'react';
import { ActivityIndicator, View, Text, Image, TouchableOpacity } from 'react-native';
import { Item, imgApp, Utils, settingApp, Toast } from '../../../../../../public';
import { MaterialIcons } from '@expo/vector-icons';
import DatePicker from './datePicker';

class Time extends Component {
    constructor(props) {
        super(props);
        
        this.dataTime = [
            { name: 'Nghỉ nguyên ngày', value: 0 },
            { name: 'Nghỉ buổi sáng', value: 2 },
            { name: 'Nghỉ buổi chiều', value: 3 }
        ]

        let indexTime = this.dataTime.findIndex(e => e.value === props.typeTimeLeave);
        indexTime = indexTime > -1 ? indexTime : 0;

        this.state = {
            isVisibleFormDate: false,
            isVisibleToDate: false,

            indexTime,
            formDate: props.formDate,
            toDate: props.toDate,
            totalTime: 0,
            disable: (this.dataTime[indexTime].value !== 0)
        }

        this.props.updateDate('dataTime', this.dataTime[indexTime]);
    }

    validate(type, value) {
        const { indexTime } = this.state;
        if (this.dataTime[indexTime].value == 0) {
            if (type === 'fromDate') {
                if (!!(!this.state.toDate || value <= this.state.toDate)) {
                    this.setState({ formDate: value }, () => this.props.updateDate('formDate', this.state.formDate))
                } else {
                    Toast.show('Ngày bắt đầu phải bé hơn hoặc bằng ngày kết thúc.');
                    this.setState({ formDate: this.state.toDate }, () => this.props.updateDate('formDate', this.state.formDate))
                }
            } else {
                if (!!(!this.state.formDate || value >= this.state.formDate)) {
                    this.setState({ toDate: value }, () => this.props.updateDate('toDate', this.state.toDate))
                } else {
                    Toast.show('Ngày kết thúc phải lớn hơn hoặc bằng ngày bắt đầu.');
                    this.setState({ toDate: this.state.formDate }, () => this.props.updateDate('toDate', this.state.toDate));
                }
            }
        } else {
            this.props.updateDate('formDate', value, true);
            this.setState({ formDate: value, toDate: value });
        }
    }

    checkTime(index) {
        const { dataTime } = this;
        let disable = false;
        if (dataTime[index].value !== 0) {
            disable = true;
            if (this.state.formDate) {
                this.validate('toDate', this.state.formDate)
            }
        }
        this.setState({ indexTime: index, disable });
        this.props.updateDate('dataTime', this.dataTime[index]);
    }

    render() {
        let { indexTime, formDate, toDate } = this.state;
        formDate = formDate ? Utils.formatTime(formDate, 'DD/MM/YYYY', true) : 'Vui lòng chọn ngày';
        toDate = toDate ? Utils.formatTime(toDate, 'DD/MM/YYYY', true) : 'Vui lòng chọn ngày';
        return (
            <View style={{ flex: 1, paddingLeft: 15 }}>
                <Item
                    name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Thời gian nghỉ<Text style={{ color: '#FF3B30' }}>*</Text></Text>}
                    value={<TouchableOpacity
                        onPress={
                            () =>
                                this.props.navigation.navigate('ItemSelect',
                                    {
                                        data: this.dataTime,
                                        title: 'Thời gian nghỉ',
                                        selected: indexTime,
                                        actions: index => this.checkTime(index)
                                    }
                                )
                        }
                        style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 10 }}>
                            <Text style={{ fontSize: 15, color: '#8E8E93' }} numberOfLines={1}>{this.dataTime[indexTime].name}</Text>
                        </View>
                        <MaterialIcons name='keyboard-arrow-right' size={23} color='#D1D1D6' />
                    </TouchableOpacity>}
                />
                <Item
                    name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Từ ngày<Text style={{ color: '#FF3B30' }}>*</Text></Text>}
                    value={<TouchableOpacity
                        onPress={() => this.setState({ isVisibleFormDate: true })}
                        style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                        <Text style={{ fontSize: 15, color: '#8E8E93', marginRight: 10 }}>{formDate}</Text>
                        <Image
                            source={imgApp.calendar}
                            style={{ width: 20, height: 20, tintColor: '#D1D1D6' }}
                            resizeMode='stretch'
                        />
                    </TouchableOpacity>}
                />
                <Item
                    name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Đến ngày<Text style={{ color: '#FF3B30' }}>*</Text></Text>}
                    value={<TouchableOpacity
                        disabled={this.state.disable}
                        onPress={() => this.setState({ isVisibleToDate: true })}
                        style={{
                            flex: 1,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'flex-end',
                            marginRight: -15,
                            paddingRight: 15,
                            backgroundColor: !this.state.disable ? 'transparent' : '#ecf0f1'
                        }}>
                        <Text style={{ fontSize: 15, color: '#8E8E93', marginRight: 10 }}>{toDate}</Text>
                        <Image
                            source={imgApp.calendar}
                            style={{ width: 20, height: 20, tintColor: '#D1D1D6' }}
                            resizeMode='stretch'
                        />
                    </TouchableOpacity>}
                />
                <Item
                    name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Tổng thời gian nghỉ</Text>}
                    value={this.props.isLoadTotaltime ?
                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                            <ActivityIndicator size='small' color={settingApp.color} />
                        </View>
                        :
                        (this.props.actuaLeave || this.props.actuaLeave === 0) ? <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                            <Text style={{ fontSize: 15, color: '#8E8E93', marginRight: 10 }}>{this.props.actuaLeave}</Text>
                        </View> : <View />}
                />

                <DatePicker
                    value={this.state.formDate || new Date()}
                    validate={value => this.validate('fromDate', value)}
                    isVisible={this.state.isVisibleFormDate}
                    close={() => this.setState({ isVisibleFormDate: false })}
                />

                <DatePicker
                    value={this.state.toDate || new Date()}
                    validate={value => this.validate('toDate', value)}
                    isVisible={this.state.isVisibleToDate}
                    close={() => this.setState({ isVisibleToDate: false })}
                />
            </View>
        )
    }
}

export default Time;