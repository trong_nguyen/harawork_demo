import React, { Component } from 'react';
import { ActivityIndicator, View, Text, TouchableOpacity } from 'react-native';
import { Item, settingApp, Error } from '../../../../../../public';
import { HaravanHr } from '../../../../../../Haravan';
import { MaterialIcons } from '@expo/vector-icons';
import Collapsible from 'react-native-collapsible';

class From extends Component {
    constructor(props) {
        super(props);

        this.state = {
            type: null,
            detailShift: (<View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                <ActivityIndicator size='small' color={settingApp.color} />
            </View>),
            detailLeaveType: (<View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                <ActivityIndicator size='small' color={settingApp.color} />
            </View>)
        }

        this.shift = null;
        this.dataLeaveType = null;
        this.dataInYear = null;
        this.leaveType = props.leaveType;
        this.userCountTypeLeave = [];
    }

    async componentDidMount() {
        const data = [];
        const dataInYear = [];

        const dataShift = await HaravanHr.getShifts();
        let dataLeaveType = await HaravanHr.leaveType();
        const userCountTypeLeave = await HaravanHr.detailLeave();
        if (userCountTypeLeave && userCountTypeLeave.data && !userCountTypeLeave.error) {
            this.props.getData('detailLeave', userCountTypeLeave.data.data);
            this.userCountTypeLeave = userCountTypeLeave.data.data;
        }
        if (dataLeaveType && dataLeaveType.data && !dataLeaveType.error) {
            this.props.getData('dataLeaveType', dataLeaveType.data.data);
            for (const item of dataLeaveType.data.data) {
                if (item.type === 0 && item.numOfLeaveDefault === 0 || item.type === 1) {
                    data.push(item)
                } else {
                    dataInYear.push(item)
                }
            }
        }

        data.push({ name: 'Nghỉ chế độ', regime: true });

        this.dataShift = dataShift;
        this.dataLeaveType = { data: { data } };
        this.dataInYear = dataInYear;
        this.leaveTypeInYear = null;
        this.loadData();
    }

    loadData() {
        const { dataShift, dataLeaveType, dataInYear } = this;
        let detailShift = null;
        let detailLeaveType = null;
        if (dataShift && dataShift.data && !dataShift.error) {
            let indexSelectShift = null;
            if (!this.shift) {
                indexSelectShift = dataShift.data.data.findIndex(e => e.id === this.props.shiftId);
                this.shift = dataShift.data.data[indexSelectShift];
            } else {
                indexSelectShift = dataShift.data.data.findIndex(e => e.id === this.shift.id);
            }
            detailShift = (
                <TouchableOpacity
                    onPress={
                        () =>
                            this.props.navigation.navigate('ItemSelect',
                                {
                                    data: dataShift.data.data,
                                    title: 'Ca làm việc',
                                    selected: indexSelectShift,
                                    actions: index => {
                                        this.shift = dataShift.data.data[index];
                                        this.loadData();
                                    }
                                }
                            )
                    }
                    style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 10 }}>
                        <Text style={{ fontSize: 15, color: '#8E8E93' }} numberOfLines={1}>{this.shift.name}</Text>
                    </View>
                    <MaterialIcons name='keyboard-arrow-right' size={23} color='#D1D1D6' />
                </TouchableOpacity>
            )
        } else {
            detailShift = (<View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                <Error />
            </View>)
        };
        if (dataLeaveType && dataLeaveType.data && !dataLeaveType.error) {
            let indexLeaveType = null;
            let indexRegime = null;
            if (this.leaveType) {
                indexRegime = dataInYear.findIndex(e => e.id === this.leaveType.id);
                if (indexRegime > -1) {
                    this.leaveTypeInYear = dataInYear[indexRegime];
                    indexLeaveType = (dataLeaveType.data.data.length - 1);
                    this.leaveType = dataLeaveType.data.data[indexLeaveType];
                    this.props.updateDate('leaveType', dataInYear[indexRegime]);
                    this.setState({ type: dataLeaveType.data.data[indexLeaveType] });
                } else {
                    indexLeaveType = dataLeaveType.data.data.findIndex(e => e.id === this.leaveType.id);
                }
            }
            detailLeaveType = (
                <TouchableOpacity
                    onPress={
                        () =>
                            this.props.navigation.navigate('CreateLeaveTypeLeave',
                                {
                                    data: {
                                        dataLeaveType: dataLeaveType.data.data,
                                        userCountTypeLeave: this.userCountTypeLeave
                                    },
                                    title: 'Loại phép',
                                    selected: indexLeaveType,
                                    actions: index => {
                                        this.leaveType = dataLeaveType.data.data[index];
                                        this.setState({ type: dataLeaveType.data.data[index] });
                                        this.loadData();
                                    }
                                }
                            )
                    }
                    style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 10 }}>
                        <Text style={{ fontSize: 15, color: '#8E8E93' }} numberOfLines={1}>{indexLeaveType > -1 ? dataLeaveType.data.data[indexLeaveType].name : 'Chọn'}</Text>
                    </View>
                    <MaterialIcons name='keyboard-arrow-right' size={23} color='#D1D1D6' />
                </TouchableOpacity>
            )
        }
        if (!this.leaveType || this.leaveType && !this.leaveType.regime) {
            this.leaveTypeInYear = null;
            this.props.updateDate('leaveType', this.leaveType);
        }
        this.props.updateDate('shift', this.shift);
        this.setState({ detailShift, detailLeaveType });
    }

    regime() {
        const { dataInYear, leaveTypeInYear } = this;
        let indexLeaveTypeInyear = null;
        if (leaveTypeInYear) {
            indexLeaveTypeInyear = dataInYear.findIndex(e => e.id === leaveTypeInYear.id);
        }
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
                    <Item
                        name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Loại chế độ<Text style={{ color: '#FF3B30' }}>*</Text></Text>}
                        value={<TouchableOpacity
                            onPress={
                                () =>
                                    this.props.navigation.navigate('CreateLeaveTypeLeave',
                                        {
                                            data: {
                                                dataLeaveType: dataInYear,
                                                userCountTypeLeave: this.userCountTypeLeave
                                            },
                                            title: 'Loại chế độ',
                                            selected: indexLeaveTypeInyear,
                                            actions: index => {
                                                this.leaveTypeInYear = dataInYear[index];
                                                this.props.updateDate('leaveType', dataInYear[index]);
                                                this.forceUpdate();
                                            }
                                        }
                                    )
                            }
                            style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 10 }}>
                                <Text style={{ fontSize: 15, color: '#8E8E93' }} numberOfLines={1}>{indexLeaveTypeInyear > -1 ? dataInYear[indexLeaveTypeInyear].name : 'Chọn'}</Text>
                            </View>
                            <MaterialIcons name='keyboard-arrow-right' size={23} color='#D1D1D6' />
                        </TouchableOpacity>}
                    />
                </View>
            </View>
        )
    }

    render() {
        const { detailShift, detailLeaveType, type } = this.state;
        return (
            <View style={{ flex: 1, paddingLeft: 15 }}>
                <Item
                    name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Ca làm việc<Text style={{ color: '#FF3B30' }}>*</Text></Text>}
                    value={detailShift}
                    noneBorder={true}
                />
                <Item
                    name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Loại phép<Text style={{ color: '#FF3B30' }}>*</Text></Text>}
                    value={detailLeaveType}
                />
                {(type && type.regime) && this.regime()}
            </View>
        )
    }
}

export default From;