import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { HaravanHr } from '../../../../../../Haravan';
import { Item, settingApp, Loading, NotFound } from '../../../../../../public';
import { EvilIcons } from '@expo/vector-icons';

class Detail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            list: [],
            isLoadEnd: false
        }
    }

    async componentDidMount() {
        const result = await HaravanHr.detailLeave();
        if (result && result.data && !result.error) {
            this.setState({ list: result.data.data, isLoadEnd: true })
        } else {
            this.setState({ isLoadEnd: true });
        }
    }

    renderContent() {
        const { list } = this.state;
        if (list.length > 0) {
            return (
                <View style={{ flex: 1 }}>
                    {list.map((item, index) => {
                        return (
                            <View style={{ paddingLeft: 15 }} key={index}>
                                <Item
                                    name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>{item.leaveTypeName}</Text>}
                                    value={(
                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                            <TouchableOpacity
                                                onPress={() => this.props.navigation.navigate('DetailInfoLeave', { item })}
                                                style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                                                <View style={{ padding: 2, paddingLeft: 7, paddingRight: 7, borderRadius: 10, backgroundColor: '#8E8E93' }}>
                                                    <Text style={{ color: '#ffffff', fontSize: 14 }} numberOfLines={1}>{item.usable}</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity
                                                onPress={() => this.props.navigation.navigate('CreateLeave', { leaveType: { ...item, id: item.leaveTypeId, name: item.leaveTypeName } })}
                                                style={{ paddingLeft: 10, paddingRight: 10, marginRight: -15 }}>
                                                <EvilIcons name='plus' size={25} color={settingApp.color} style={{ marginTop: 5, marginBottom: 5 }} />
                                            </TouchableOpacity>
                                        </View>
                                    )}
                                    noneBorder={(index === 0)}
                                />
                            </View>
                        )
                    })}
                </View>
            )
        } else {
            return (
                <View style={{ flex: 1, paddingBottom: 20 }}>
                    <NotFound />
                </View>
            )
        }

    }

    render() {
        const { isLoadEnd } = this.state;
        let content = (
            <View style={{ height: (settingApp.height / 2) - 100 }}>
                <Loading />
            </View>
        );
        if (isLoadEnd) {
            content = this.renderContent();
        }
        return (
            <View style={{ flex: 1 }}>
                <View style={{ padding: 15 }}>
                    <Text style={{ color: '#8E8E93', fontSize: 13 }}>PHÉP CÒN LẠI</Text>
                </View>
                <View style={{ flex: 1, backgroundColor: '#ffffff', ...settingApp.shadow }}>
                    {content}
                </View>
            </View>
        )
    }
}

export default Detail;