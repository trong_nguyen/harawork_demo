import React, { Component } from 'react';
import { ActivityIndicator, View, Text, TouchableOpacity } from 'react-native';
import { HaravanHr } from '../../../../../../Haravan';
import { Item, settingApp, Utils, Error } from '../../../../../../public';
import { Ionicons } from '@expo/vector-icons';

class History extends Component {
    constructor(props) {
        super(props);

        const load = <ActivityIndicator size='small' color={settingApp.color} style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }} />;
        this.state = {
            notApproved: load,
            reject: load
        }
        this.urls = [
            { type: 'notApproved', url: 'leave/history?status=1', color: '#FF9500', activeTab: 'Second' },
            { type: 'reject', url: 'leave/history?status=3', color: '#FF3B30', activeTab: 'Third' }
        ]
    }

    componentDidMount() {
        this.loadData();
    }

    componentWillReceiveProps(nextProps) {
        if (!this.props.leave.updateHistory && nextProps.leave.updateHistory) {
            this.loadData();
            this.props.actions.leave.updateHistory(null);
        }
    }

    async loadData() {
        const { urls } = this;
        let result = null;
        let count = 0;
        const formDate = new Date(new Date().getFullYear(), new Date().getMonth(), 1);
        condition = `fromDate=${Utils.formatTime(formDate, 'YYYY-MM-DD', true)}`;
        for (const item of urls) {
            result = await HaravanHr.leaveHistory(item.url, condition);
            if (result && result.data && !result.error) {
                count = result.data.data.length;
                this.setState({
                    [`${item.type}`]: (
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('HistoryLeave', { activeTab: item.activeTab })}
                            style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 10 }}>
                                <View style={{ padding: 2, paddingLeft: 7, paddingRight: 7, borderRadius: 10, backgroundColor: item.color }}>
                                    <Text style={{ color: '#ffffff', fontSize: 14 }} numberOfLines={1}>{count}</Text>
                                </View>
                            </View>
                            <Ionicons name='ios-arrow-forward' size={23} color='#D1D1D6' />
                        </TouchableOpacity>
                    )
                })
            } else {
                this.setState({ [`${item.type}`]: (
                    <View style={{ flex: 1, alignItems: 'flex-end' }}>
                        <Error />
                    </View>
                ) });
            }
        }

    }


    render() {
        const { notApproved, reject } = this.state;
        return (
            <View style={{ flex: 1, backgroundColor: '#ffffff', paddingLeft: 15, ...settingApp.shadow }}>
                <Item
                    name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Phép chưa duyệt</Text>}
                    value={notApproved}
                    noneBorder={true}
                />
                <Item
                    name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Phép bị từ chối</Text>}
                    value={reject}
                />
            </View>
        )
    }

}

export default History;