import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { HeaderIndex, settingApp } from '../../../../../../public';
import { Feather } from '@expo/vector-icons';

class Header extends Component {

    render() {
        return (
            <HeaderIndex
                title={
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={settingApp.styleTitle}>
                            Phép còn lại
                        </Text>
                    </View>
                }
                buttonRight={
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('CreateLeave', { leaveType: null })}
                        style={{ width: 55, height: 44, backgroundColor: 'transparent', justifyContent: 'center', alignItems: 'center' }}>
                        <Feather name='plus' size={23} color="#ffffff" />
                    </TouchableOpacity>
                }
            />
        )
    }
}

export default Header;