import Layout from './layout';
import { HaravanHr } from '../../../../../Haravan';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../../state/action';

class DetailLeaves extends Layout {

    async componentDidMount() {
        const result = await HaravanHr.leaveType();
        if (result && result.data && !result.error) {
            this.props.actions.leave.listLeave({ list: result.data.data, isError: false });
        } else {
            this.props.actions.leave.listLeave({ list: [], isError: true });
        }
    }

}

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(mapStateToProps, mapDispatchToProps)(DetailLeaves);