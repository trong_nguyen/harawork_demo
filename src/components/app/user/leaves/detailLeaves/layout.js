import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';

import Detail from './components/detail';
import Header from './components/header';
import History from './components/history';

class Layout extends Component {

    renderContent() {
        return (
            <ScrollView style={{ paddingTop: 15 }}>
                <History
                    navigation={this.props.navigation}
                    {...this.props}
                />
                <Detail navigation={this.props.navigation} />
                <View style={{ height: 30 }}/>
            </ScrollView>
        )
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Header navigation={this.props.navigation} />
                <View style={{ flex: 1 }}>
                    {this.renderContent()}
                </View>
            </View>
        )
    }
}

export default Layout;