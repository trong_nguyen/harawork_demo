import React, { Component } from 'react';
import { View, Text , TouchableOpacity} from 'react-native';
import { HeaderScreen,HeaderIndex ,settingApp } from '../../../../../public'
import { Ionicons } from '@expo/vector-icons'

class Layout extends Component{

    render(){
        const {changeIcon} = this.props
        return(
            <HeaderIndex 
                title={
                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                    >
                        <Text style={settingApp.styleTitle}>
                        Đánh giá 360
                        </Text>
                    </View>
                }
                buttonRight={
                    <TouchableOpacity
                        onPress={() =>this.props.isShow()}
                        style={{width:50, justifyContent:'center', alignItems:'center', height:44}}
                    >
                    {
                        changeIcon? 
                        <Ionicons name='ios-close' size={35} color='#FFFFFF' />
                        :
                        <Ionicons name='md-help-circle' size={20} color='#FFFFFF' />
                    }
                        
                    </TouchableOpacity>
                }
            />
        )
    }
}
export default Layout;