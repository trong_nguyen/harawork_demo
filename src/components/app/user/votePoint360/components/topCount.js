import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { settingApp } from '../../../../../public'

class TopCount extends Component{
    constructor(props){
        super(props)
        this.state={

        }
    }

    checkLengthValue(value, length){
        if((value == length) && (length > 0)){
            return {color:settingApp.color}
        }
        else if(value < length){
            return{ color:'#FF9500'}
        }
        else if(value == 0){
            return{color:settingApp.colorDisable}
        }
        else if(value > length){
            return{color:'#FF3B30'}
        }
        else{
            return{color:settingApp.colorDisable}
        }   
    }
    
    render(){
        const {value1, value2, value3, length1, length2, length3, onlyLimitA} = this.props;
        let color1 = this.checkLengthValue(value1.length, length1);
        let color2 = this.checkLengthValue(value2.length, length2);
        let color3 = this.checkLengthValue(value3.length, length3);
        return(
            <View style={{ flexDirection:'row', height:44, width:settingApp.width, marginBottom:10,backgroundColor:'#FFFFFF', justifyContent:'space-between', alignItems:'center', ...settingApp.shadow}}>
                <View style={{width:(settingApp.width*0.5), padding:5}}>
                    <Text
                        style={{fontSize:16, paddingLeft:10, color:settingApp.colorText,}}
                    >Hạn mức bình chọn</Text>
                </View>
                <View style={{ backgroundColor:'transparent',justifyContent:'space-between', width:(settingApp.width*0.4), flexDirection:'row',marginRight:10}}>
                    <View style={styles.cover}>
                        <View style={[styles.option,{backgroundColor:color1.color}]}>
                            <Text
                                style={styles.text}
                            >{value1.length}/{length1}</Text>
                        </View>     
                    </View>
                    <View style={styles.cover}>
                        <View style={[styles.option,{backgroundColor: (!onlyLimitA ? color2.color : settingApp.colorDisable)}]}>
                            <Text
                            style={styles.text}
                            >{value2.length}</Text>
                            {!onlyLimitA ? 
                                <Text
                                style={styles.text}
                                >/{length2}</Text>
                                : <View/>
                            }
                        </View>     
                    </View>
                    <View style={styles.cover}>
                        <View style={[styles.option,{backgroundColor:(!onlyLimitA ? color3.color : settingApp.colorDisable)}]}>
                            <Text
                            style={styles.text}
                            >{value3.length}
                            </Text>
                            {!onlyLimitA ? 
                                <Text
                                    style={styles.text}
                                >/{length3}</Text>
                                : <View/>
                            }
                        </View>     
                    </View>
                    
                </View>
            </View>
        )
    }
}
const styles ={
    cover:{
        width:50, 
        justifyContent:'center', 
        alignItems:'center'
    },
    option:{
        minWidth:40, 
        height:30, 
        borderRadius:20,
        justifyContent:'center', 
        alignItems:'center',
        flexDirection:'row',
    },
    text:{
        fontSize:14, 
        color:'#FFFFFF',

    }
}
export default TopCount;