import React, { Component } from 'react';
import { ActivityIndicator, View, Text, TouchableOpacity, Image, ScrollView } from 'react-native';
import { settingApp, imgApp, Utils, Error, Loading } from '../../../../../../public';
import { MaterialIcons } from '@expo/vector-icons';
import Fillter from '../fillter';

class Layout extends Component{
    render(){
        const { month, nameDepart, listDepart, isLoad } = this.state;
        // console.log('isLoad', isLoad);
        return (
            <ScrollView>
                <View style={{ flex: 1, backgroundColor: '#ffffff', paddingLeft: 15, ...settingApp.shadow, marginBottom: 20 }}>
                    <Fillter
                        noneBorder={true}
                        name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Kỳ bình chọn</Text>}
                        value={(
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('ListMontVote',{
                                    monthDF:{month},
                                    actions:(value) => this.setMonth(value)
                                })}
                                style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                                <Text style={{ fontSize: 15, color: '#8E8E93', marginRight: 10 }}>Tháng {month}</Text>
                                <MaterialIcons name='keyboard-arrow-right' size={23} color='#D1D1D6' />
                            </TouchableOpacity>
                        )}
                    />
                    <Fillter
                        name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Đơn vị</Text>}
                        value={(
                            <TouchableOpacity
                                activeOpacity={
                                    (listDepart.length > 0 )? 0.5 : 1
                                }
                                onPress={() => 
                                (listDepart.length > 0 )?
                                this.props.navigation.navigate('ListDepart',{
                                    nameDepart:{nameDepart},
                                    listDepart:{listDepart},
                                    actions:(value) => this.setDepart(value)
                                }) : null }
                                style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 10 }}>
                                    {isLoad ?  this.loading()
                                    :
                                    <Text style={{ fontSize: 15, color: '#8E8E93' }} numberOfLines={1}>{ nameDepart ? nameDepart.name : ''}</Text>
                                    }
                                </View>
                                {
                                (listDepart.length > 0 )?
                                    <MaterialIcons name='keyboard-arrow-right' size={23} color='#D1D1D6' />
                                    : <View/>
                                }
                            </TouchableOpacity>
                        ) 
                        }
                    />
                </View>
            </ScrollView>
        )
    }
    
    loading(){
        return (
            <View style={{
                justifyContent: 'center',
                alignItems: 'center',
            }}>
                <ActivityIndicator size='small' color='#8E8E93' />
            </View>
        )
    }
}
 export default Layout;