import Layout from './layout';
import { Utils } from '../../../../../../public';
import moment from 'moment';
import { HaravanHr } from '../../../../../../Haravan';

class Condition extends Layout{
    constructor(props){
        super(props)
        this.state={
            month: ((new Date().getMonth() + 1) + '/' + (new Date().getFullYear())),
            nameDepart:null,
            listDepart:[],
            departVote:null,
            value: false,
            isLoad: false,
            monthDF:((new Date().getMonth() + 1) + '/' + (new Date().getFullYear()))
        }
        this.listDepart = props.listDepart;
    }

    componentWillReceiveProps(nextProps){
        const { value , isLoad} = this.state;
        if(nextProps){
            this.setState({isLoad: nextProps.isLoad})
        }
        if(nextProps.listDepart && !value){
            const { listDepart } = nextProps;
            if(listDepart.length > 0 ){
                this.listDepart = listDepart;
                const index = listDepart[0].department_detail.findIndex(m=> m.id === listDepart[0].voteID)
                if(index > -1){
                    this.setState({
                        nameDepart:listDepart[0].department_detail[index],
                        listDepart:listDepart
                    })
                }
            }
            else{
                this.setState({
                    nameDepart: {name:'Không có đơn vị'},
                    listDepart
                })
            }
        }
        
    }


    componentDidMount(){
        const { listDepart } = this;
        if(listDepart.length >0){
            const index = listDepart[0].department_detail.findIndex(m=> m.id === listDepart[0].voteID)
            if(index > -1){
                this.setState({
                    nameDepart:listDepart[0].department_detail[index]
                })
            }
        }
        this.setState({isLoad:false})
    }

    refreshFiter(){
        let value= { month:((new Date().getMonth() + 1)), year:(new Date().getFullYear()) }
        this.setState({isLoad:this.props.isLoad, month:this.state.monthDF, value:false}, () =>{
            this.props.setValue(value)
            this.loadData()
        })
    }

    loadData(){
        const { listDepart } = this;
        if(listDepart.length >0){
            const index = listDepart[0].department_detail.findIndex(m=> m.id === listDepart[0].voteID)
            if(index > -1){
                this.setState({
                    nameDepart:listDepart[0].department_detail[index]
                },() =>{
                    this.setState({isLoad:false})
                    this.props.setRemoveFilter(false)
                })
            }
        }
    }

    setMonth(value){
        const { month } = this.state
        let newMonth = (value.month) + '/' + (value.year);
        this.setState({isLoad:true},() =>{
            if(newMonth !== month){
                this.setState({
                    month:newMonth,
                    value:false
                },() =>{
                    this.props.setValue(value)
                    this.props.setRemoveFilter(true)
                })
            }
        })
    }

    setDepart(value){
        this.setState({
            value:true,
        }, () =>{
            this.setState({
                nameDepart:{id: value.e.voteID ,name:value.nameDepart}
            })
            this.props.setVoteID(value.e.voteID)
            this.props.setRemoveFilter(true)
        })
    }

}
export default Condition;