import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Platform, Image, ScrollView } from 'react-native';
import { settingApp, imgApp } from '../../../../../public';
import * as Animatable from 'react-native-animatable';
import { MaterialCommunityIcons} from '@expo/vector-icons'

class GuildVote extends Component{
    constructor(props){
        super(props)
        this.state={
            dataText:null,
            linkImg:null
        }
    }

   async componentDidMount(){
        const {general} = this.props;
        if(general){
            const data = await JSON.parse(general.voting_guide)
            this.setState({
                dataText:data.blocks[0].text,
                linkImg:data.entityMap[0].data.src
            })
        }
    }

    render(){
        const {isShow} = this.props;
        const { dataText, linkImg } = this.state;
        let source = linkImg ? {uri:linkImg} : imgApp.noImage
        return(
            <Animatable.View
                ref = {refs => this.Animatable= refs}
                duration={120}
                animation= 'slideInUp'
                style={styles.cover}
            >   
                <ScrollView> 
                    <View style={{width:(settingApp.width), height:44, justifyContent:'center', alignItems:'center'}}>
                        <Text style={{fontSize:16, color:settingApp.colorText, fontWeight:'bold'}}>Hướng dẫn</Text>
                    </View>
                    <Image
                        source={source}
                        style={{width:(settingApp.width), height:(settingApp.height*0.3), padding:10}}
                    />
                    <Text style={{fontSize:14, color:settingApp.colorText, padding:10}}>{dataText? dataText : ''}</Text>
                </ScrollView>
            </Animatable.View>
        )
    }
}
const styles={
    cover:{
        ...Platform.select({
            android: {
                marginTop: settingApp.statusBarHeight + 40,
            },
            ios: {
                marginTop: settingApp.statusBarHeight + 44,
            },
        }),
        position:'absolute', 
        width:settingApp.width, 
        height:settingApp.height,
        alignItems: 'center',
        backgroundColor:'#FFFFFF', 
    }
}
export default GuildVote;