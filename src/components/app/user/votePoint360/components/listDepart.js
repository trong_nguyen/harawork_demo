import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { Utils, settingApp, HeaderWapper } from '../../../../../public';
import { Ionicons } from '@expo/vector-icons';

class ListDepart extends Component{
    constructor(props){
        super(props)
        this.state={
            listMonth:props.navigation.state.params.listDepart.listDepart,
        }
        this.setDepart = props.navigation.state.params.actions;
        this.nameDepart =props.navigation.state.params.nameDepart;
    }

    // componentDidMount(){
    //     this.setState({
    //         listDepart:this.listDepart
    //     })
    // }

    render(){
        const { listMonth } = this.state;
        return (
            <View style={{flex:1}}>
                <HeaderWapper
                    style={{
                        justifyContent: 'space-between',
                        flexDirection: 'row',
                    }}
                >
                    <TouchableOpacity
                        onPress={() => this.props.navigation.pop()}
                        style={{ width: 55, height: 43, justifyContent: 'center', alignItems: 'center' }}
                    >
                        <Ionicons name='ios-arrow-back' size={23} color='#ffffff' />
                    </TouchableOpacity>

                    <Text style={settingApp.styleTitle}>
                        Đơn vị
                    </Text>
                    <View style={{ width: 55, height: 43, justifyContent: 'center', alignItems: 'center' }} />
                </HeaderWapper>

                <View style={{ flex: 1, backgroundColor: '#C8C7CC' }}>
                    <ScrollView style={{ width: settingApp.width, backgroundColor: '#FFFFFF', flexDirection: 'column' }}>
                        {listMonth.map((e, i) => {
                            let checkMonth = (e.voteID) == (this.nameDepart.nameDepart.id);
                            let index = e.department_detail.findIndex(m => m.id == e.voteID)
                            return (
                                <View 
                                key={i}
                                style={{ borderBottomColor: '#C8C7CC', borderBottomWidth: 1, marginLeft: 15 }}>
                                    {(index > -1) ?
                                        <TouchableOpacity
                                        activeOpacity={1}
                                        onPress={() => {
                                            checkMonth ? null : this.setDepart({e, nameDepart:e.department_detail[index].name})
                                            this.props.navigation.pop()
                                        }}
                                        style={{ alignItems: 'center', marginLeft: 10, paddingBottom: 15, paddingTop: 15, flexDirection:'row', width:(settingApp.width-10) }}
                                    >
                                   
                                        {checkMonth ? 
                                            <View style={{width:30}}>
                                                <Ionicons name='ios-checkmark' size={30} color={settingApp.color} /> 
                                            </View>
                                            : null
                                        }
                                        <Text style={{ fontSize: 16, marginLeft:(checkMonth ? 0 :30), color: settingApp.colorText }}>{e.department_detail[index].name}</Text>
                                    </TouchableOpacity>
                                    : 
                                    <View/>
                                    }                                
                                </View>
                            )
                        })}
                    </ScrollView>
                </View>
            </View>
        )
    }
}
export default ListDepart;