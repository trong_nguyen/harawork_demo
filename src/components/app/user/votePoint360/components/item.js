import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { settingApp } from '../../../../../public';

class Item extends Component{
    constructor(props){
        super(props)
        this.state={
            value:0,
            listVoted:null,
            employeeVote: 0,
        }
        this.departmentVote = null
    }
    componentWillReceiveProps(nextProp){
        if((nextProp.departmentVote != this.departmentVote)){
            this.props.clearValue()
            const employeeVote = nextProp.employeeVote ? nextProp.employeeVote.employee_point_list : []
            this.setState({value:0}, () =>{
                this.setValueVote(employeeVote, nextProp.item)
                this.departmentVote = nextProp.departmentVote
            })
        }
        if(nextProp.voteItem && JSON.stringify(nextProp.voteItem) != JSON.stringify(this.props.voteItem)){
            if(nextProp.voteItem.voter_id == this.props.data.id){
              this.setState({value:0}, () =>{
                this.setValueVote(nextProp.voteItem.employee_point_list, this.props.item)
            })  
            }
        }
        if( nextProp.employeeVote && nextProp.employeeVote != this.state.employeeVote){
            this.setState({employeeVote:nextProp.employeeVote})
        }
    }

    componentDidMount(){
        const {employeeVote, item, voteItem } = this.props;
        this.departmentVote = this.props.departmentVote;
        this.setState({employeeVote:employeeVote})
        if(voteItem){
            this.setValueVote(voteItem.employee_point_list, item)
        }
    }

    setValueVote(employeeVote, item){
        if(employeeVote && (employeeVote.length > 0)){
            const index = employeeVote.findIndex(m => m.employee_id == item.employee_id)
            if(index > -1){
                if(employeeVote[index].point_level == 1){
                    this.setState({value:1},()=>{
                        this.props.setValue(1, item)
                       })
                }
                if(employeeVote[index].point_level == 2){
                    this.setState({value:2},()=>{
                        this.props.setValue(2, item)
                       })
                }
                if(employeeVote[index].point_level == 3){
                    this.setState({value:3},()=>{
                        this.props.setValue(3, item)
                       })
                }
            }
            else{
                this.setState({value:0})
            }
        }
        else{
            this.setState({value:0})
        }
    }

    checkValue(value){
        const { item } = this.props
        this.setState({
           value:value
        },() =>{
            this.props.setValue(value, item)
        })
    }

    renderVote(){
        const { value , employeeVote} = this.state
        let isCheck = employeeVote == 1;
        return(
            <View 
                style={styles.voteOption}
            >
                <TouchableOpacity
                    activeOpacity={ isCheck ? 0.5 : 1 }
                    onPress={() => (isCheck ? this.checkValue(1) : null) }
                    style={styles.button}
                >
                    <View 
                        style={(value==1 ?  styles.valueVoteTrue : styles.valueVote)}>
                            <Text style={[styles.textValue,{color:(value ==1? '#FFFFFF' :settingApp.colorDisable)}]}>A</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity
                    activeOpacity={ isCheck ? 0.5 : 1 }
                    onPress={() =>  (isCheck ? this.checkValue(2) : null)}
                    style={styles.button}
                >   
                    <View style={(value==2 ?  styles.valueVoteTrue : styles.valueVote)}>
                        <Text style={[styles.textValue,{color:(value ==2? '#FFFFFF' :settingApp.colorDisable)}]}>B</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity
                    activeOpacity={ isCheck ? 0.5 : 1 }
                    onPress={() =>  (isCheck ? this.checkValue(3) : null)}
                    style={styles.button}
                >
                    <View style={(value==3 ?  styles.valueVoteTrue : styles.valueVote)}>
                        <Text style={[styles.textValue,{color:(value ==3? '#FFFFFF' :settingApp.colorDisable)}]}>C</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    renderUser(item){
        return(
            <View style={styles.coverUser}>
                <Text
                    style={{fontSize:16, color:settingApp.colorText, padding:5}}
                >{item.employee_name}</Text>
                <Text
                    style={{fontSize:14, color:settingApp.colorDisable, paddingLeft:5, paddingBottom:5}}
                >{item.employee_job_title}</Text>
            </View>
        )
    }

    render(){
        const {item} = this.props
        return(
            <View style={[styles.cover,{ backgroundColor:((item.isVote==false) ? 'rgba(255, 59, 48, 0.15)' :'#FFFFFF')}]}>
                {this.renderUser(item)}
                {this.renderVote()}
            </View>
        )
    }
}

const styles ={
    cover:{
        flex:1, 
        flexDirection:'row', 
        justifyContent:'space-between', 
        borderRadius:5,
        minHeight:60,
        alignItems:'center',
        marginBottom:10,
        ...settingApp.shadow
    },
    coverUser:{
        padding:5, 
        maxWidth:(settingApp.width * 0.5), 
        flex:1, 
        minHeight:50, 
        justifyContent:'space-between'
    },
    voteOption:{
        flexDirection:'row', 
        backgroundColor:'transparent', 
        width:(settingApp.width*0.4), 
        justifyContent:'space-between', 
    },
    valueVote:{
        width:30, 
        height:30, 
        borderRadius:15, 
        borderColor:settingApp.colorDisable, 
        borderWidth:1, 
        alignItems:'center', 
        justifyContent:'center',
        backgroundColor:'#FFFFFF'
    },
    valueVoteTrue:{
        width:30, 
        height:30, 
        borderRadius:15, 
        borderColor:'transparent', 
        borderWidth:1, 
        alignItems:'center', 
        justifyContent:'center',
        backgroundColor:'#4CD964'
    },

    textValue:{
        fontSize:16, 
        fontWeight:'bold' 
    },
    button:{
        width:50, 
        height:50, 
        justifyContent:'center',
        alignItems: 'center',
    }
}
export default Item;