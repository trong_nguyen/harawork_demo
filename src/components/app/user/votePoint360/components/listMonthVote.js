import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { Utils, settingApp, HeaderWapper } from '../../../../../public';
import { Ionicons } from '@expo/vector-icons';

class ListMonthVote extends Component{
    constructor(props){
        super(props)
        this.state={
            listMonth:[]
        }
        this.setMonth = props.navigation.state.params.actions;
        this.monthDF =props.navigation.state.params.monthDF;
    }

    componentDidMount(){
        this.loadMonth()
    }

    loadMonth(){
        const {listMonth} = this.state;
        let fromMonth = 10;
        let toMonth = (new Date().getMonth() +1);
        let toYear = new Date().getFullYear();
        let item = null;
        for(let i = 2018; i <= toYear; i++){
            for(let e = 1; e <13; e++)
            if(i == 2018){
                e = fromMonth;
                if(e < 13){
                    e = e + 1;
                    fromMonth = e;
                    item ={ month:e, year:i}
                    listMonth.push(item)
                }
                else{
                    e = 1
                }
            }
            else{
                if(e < 13){
                    if(i < toYear){
                        item ={ month:e, year:i}
                        listMonth.push(item)
                    }
                    else{
                        if(e <=  toMonth){
                            item ={ month:e, year:i}
                            listMonth.push(item)
                        }
                    }
                }
                else{
                    e = 1
                }
            }
        }
        this.setState({listMonth})
    }

    render(){
        const { listMonth } = this.state;
        return (
            <View style={{flex:1}}>
                <HeaderWapper
                    style={{
                        justifyContent: 'space-between',
                        flexDirection: 'row',
                    }}
                >
                    <TouchableOpacity
                        onPress={() => this.props.navigation.pop()}
                        style={{ width: 55, height: 43, justifyContent: 'center', alignItems: 'center' }}
                    >
                        <Ionicons name='ios-arrow-back' size={23} color='#ffffff' />
                    </TouchableOpacity>

                    <Text style={settingApp.styleTitle}>
                        Kỳ bình chọn
                    </Text>
                    <View style={{ width: 55, height: 43, justifyContent: 'center', alignItems: 'center' }} />
                </HeaderWapper>

                <View style={{ flex: 1, backgroundColor: '#C8C7CC' }}>
                    <ScrollView style={{ width: settingApp.width, backgroundColor: '#FFFFFF', flexDirection: 'column' }}>
                        {listMonth.map((e, i) => {
                            let checkMonth = (e.month +'/'+e.year) === (this.monthDF.month);
                            return (
                                <View 
                                key={i}
                                style={{ borderBottomColor: '#C8C7CC', borderBottomWidth: 1, marginLeft: 15, flexDirection:'row' ,alignItems: 'center',}}>
                                {checkMonth ? 
                                    <View style={{width:30}}>
                                        <Ionicons name='ios-checkmark' size={30} color={settingApp.color} /> 
                                    </View>
                                    : null
                                }
                                <TouchableOpacity
                                    activeOpacity={1}
                                    onPress={() => {
                                        this.setMonth(e)
                                        this.props.navigation.pop()
                                    }}
                                    style={{ alignItems: 'center', marginLeft:(checkMonth ? 0 : 30), paddingBottom: 15, paddingTop: 15, flexDirection:'row',width:(settingApp.width - (checkMonth ? 0 : 30)) }}
                                >
                                    <Text style={{ fontSize: 16, color: settingApp.colorText }}>Tháng {e.month}/{e.year}</Text>
                                </TouchableOpacity>
                                </View>
                            )
                        })}
                    </ScrollView>
                </View>
            </View>
        )
    }
}
export default ListMonthVote;