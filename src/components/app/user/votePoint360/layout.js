import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, FlatList, ActivityIndicator } from 'react-native';
import Header from './components/header';
import { Feather, EvilIcons } from '@expo/vector-icons';
import Collapsible from 'react-native-collapsible';
import { Arrow, settingApp, Loading, SvgCP } from '../../../../public';
import TopCount from './components/topCount';
import Condition from './components/condition';
import Item from './components/item';
import GuildVote from './components/guildVote';
import * as Animatable from 'react-native-animatable';

class Layout extends Component{
    renderButton() {
        const { isColap } = this.state;
        return (
            <View style={{ flexDirection: 'row', paddingLeft: 10, paddingRight: 10 }}>
                <View style={{ flex: 1 }}>
                    {(this.state.removeFilter == true) && <TouchableOpacity
                        onPress={() => {
                            this.setState({ isColap: false, isLoad:true},()=>{
                                this.condition.refreshFiter()
                                this.refreshData()
                            })
                        }}
                        style={{ flexDirection: 'row', alignItems: 'center', height: 50 }}>
                        <EvilIcons name='close' color='#FF3B30' size={30} />
                        <Text style={{ color: '#FF3B30', fontSize: 16, marginLeft: 5 }}>Đặt lại bộ lọc</Text>
                    </TouchableOpacity>}
                </View>
                <View style={{ flex: 1, alignItems: 'flex-end' }}>
                    <TouchableOpacity
                        onPress={() => this.setState({ isColap: !this.state.isColap })}
                        style={{ flexDirection: 'row', alignItems: 'center', height: 50 }}>
                        <Text style={{ color: settingApp.color, fontSize: 16, marginRight: 5 }}>
                            {!isColap ? 'Bộ lọc nâng cao' : 'Thu gọn bộ lọc'}
                        </Text>
                        <View>
                            <Arrow isOpen={isColap} color={settingApp.color} />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    renderColap(){
        const { isColap , isLoad} = this.state;
        return(
            <View style={{ backgroundColor:'transparent'}}>
                {this.renderButton()}
                <Collapsible collapsed={!isColap} style={{paddingBottom:1, paddingTop:1}} >
                    <Condition 
                        ref={refs => this.condition = refs}
                        setValue={
                            value => this.setMonth(value)
                        }
                        setVoteID ={ (id)=> this.checkEmploy(id) }
                        removeFilter = {this.state.removeFilter}
                        setRemoveFilter = {removeFilter => this.setState({removeFilter:removeFilter})}
                        navigation ={this.props.navigation}
                        listDepart = {this.state.listDepart}
                        isLoad={isLoad}
                    />
                </Collapsible>
                
            </View>
        )
    }

    notVote(){
        return (
            <View style={{ backgroundColor:'transparent', flex:1,justifyContent:'center', alignItems:'center', padding:10}}>
                <SvgCP.errorVote/>
                <Text style={{fontSize:14, color:settingApp.colorDisable, textAlign:'center', paddingTop:10}}>Không có cuộc đánh giá nào được thiết lập. 
                Vui lòng liên hệ quản lý để biết thêm thông tin.</Text>
            </View>
        )
    }

    renderConten(){
        let content = <View/>
        const { listEmploy, value1, value2, value3, employeeVote, isLoad } = this.state;
        if(listEmploy.length > 0){
            content = (
                <View style={{marginBottom:10, backgroundColor:'transparent', flex:1, justifyContent:'center'}}>
                    {(!isLoad && employeeVote==0)&&
                    <Animatable.View 
                    duration={500}
                    animation='slideInLeft'
                    style={{
                        height:60, 
                        width:(settingApp.width -20), 
                        marginLeft:10, 
                        marginRight:10, 
                        marginBottom:15, 
                        backgroundColor:'rgba(76, 217, 100, 0.1)',
                        flexDirection:'row',
                        alignItems:'center'
                        }}>
                        <View style={{height:60, width:4, backgroundColor:'#4CD964'}}/>
                        <Text style={{marginLeft:10,fontSize:14, color:settingApp.colorText}}>Cuộc bình chọn đã kết thúc.</Text>
                    </Animatable.View>
                    }
                    
                    <TopCount
                        length1={this.length1}
                        length2={this.length2}
                        length3={this.length3}
                        value1={value1}
                        value2={value2}
                        value3={value3}
                        onlyLimitA = {this.onlyLimitA}
                    />

                    <FlatList
                        style={{paddingLeft:10, marginRight:10, paddingTop:10}}
                        data={listEmploy}
                        extraData={this.state}
                        keyExtractor={(item, index) => (`${item.employee_haraId}`) || (index+'')}
                        renderItem={({item, index}) => this.renderItem(item, index)}
                    />
                </View>
            )
        }
        else{
            content=(
                this.notVote()
            )
        }
        return(
            content
        )
    }

    renderItem(item, index){
        return(
            <Item
                item={item}
                data={ this.state.data }
                employeeVote={this.state.employeeVote}
                voteItem ={ this.state.voteItem }
                departmentVote ={ this.departmentVote}
                setValue={(value, item) => this.setValue(value, item)}
                clearValue={() => this.clearValue()}
            />
        )
    }
    renderSave(){
        const { employeeVote, disable } = this.state;
        if( employeeVote == 0 ){
            return (
                <View />
            )
        }
        else{
            return(
                <Animatable.View 
                    duration={200}
                    delay={100}
                    animation= 'slideInUp'
                    style={{width:(settingApp.width), height:80, backgroundColor:'#FFFFFF', justifyContent:'center', alignItems:'center', ...settingApp.shadow}}>
                        <TouchableOpacity
                            onPress={() =>
                             { !disable ?
                                (employeeVote == 2 ?
                                this.setState({employeeVote:1})
                                :
                                this.setState({disable:true},() => this.saveVote() )) : null
                                }}
                            style={{width:(settingApp.width*0.8), borderRadius:5, height:60, backgroundColor:(disable ? settingApp.colorDisable : settingApp.color) , justifyContent:'center', alignItems:'center', flexDirection:'row'}}
                        >
                            <Text style={{fontSize:16, color:'#FFFFFF', fontWeight:'bold'}}>{(employeeVote == 1) ? 'Lưu': 'Chỉnh sửa'}</Text>
                                {
                                   disable && <ActivityIndicator size='small' color={settingApp.colorDisable} />
                                }
                        </TouchableOpacity>
                </Animatable.View>
            )
        }
    }

   
    render(){
        let content = <View/>
        const { message , isLoad, listEmploy, isShow, disable} = this.state;
        content = isLoad ?  <Loading/> : this.renderConten()
        if(!message){
            return(
                <View style={{flex:1}}>
                    <View style={{flex:1}}>
                        <Header 
                            isShow={() => this.setState({isShow:!this.state.isShow})}
                            changeIcon={isShow}
                        />
                        {this.renderColap()}
                        {content}
                        {(!isLoad && (listEmploy.length >0) && !isShow) && this.renderSave()}
                    </View>
                    {
                        isShow && <GuildVote
                            ref ={refs => this.GuildVote = refs}
                            isShow={isShow}
                            general={this.state.general}
                        />
                    }
                </View>
            )
        }
        else{
            return(
                <View style={{flex:1, backgroundColor:'#FFFFFF'}}>
                    <Header 
                        isShow={() => this.setState({isLoad:false})}
                        changeIcon={isShow}
                    />
                        <View
                        style={{flex:1,  alignItems:'center', justifyContent:'center'}}
                        >
                            <Text style={{fontSize:14, color:settingApp.colorText}}>Phiên làm việc đã hết hạn. Vui lòng thử lại.</Text>
                            <TouchableOpacity
                                onPress={() => this.refresh_token()}
                                style={{ marginTop:10,width:(settingApp.width*0.3), height:40, backgroundColor:(settingApp.color), justifyContent:'center', alignItems:'center', borderRadius:5, ...settingApp.shadow}}
                            >
                                <Text style={settingApp.styleTitle}>Thử lại</Text>
                            </TouchableOpacity>
                        </View>
                </View>
            )
        }
    }
}
export default Layout;