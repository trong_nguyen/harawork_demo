import Layout from './layout'
import { HaravanHr, Vote360 } from '../../../../Haravan';
import { Utils, Toast } from '../../../../public';
import { connect } from 'react-redux';
import { Alert } from 'react-native';

class VotePoint360 extends Layout{
    constructor(props){
        super(props)
        this.state= {
            value1:[],
            value2:[],
            value3:[],
            isShow:false,
            message:null,
            isLoad:true,
            isColap: false,
            removeFilter: false,
            data:null,
            listEmploy:[],
            listDepart:[],
            settingLimit:{},
            general:{},
            employeeVote:0,
            monthDF: ( (new Date().getFullYear())+''+(Utils.formatLocalTime((new Date()), 'MM', true))),
            accessToken : null,
            voteItem:null,
            isManager: null,
            isFirts:true,
            disable:false
        }
        this.colleague = props.app.authHaravan ;
        this.length1 = 0;
        this.length2 = 0;
        this.length3 = 0;

        this.onlyLimitA = false

        this.departmentVote = null
    }


    refresh_token(){
        this.clearValue()
        this.setState({
            monthDF: ((new Date().getFullYear())+''+(Utils.formatLocalTime((new Date()), 'MM', true))),
            isLoad:true,
            message:null
        },() =>{
            this.getToken()
        })
        
    }

    componentDidMount(){
        this.getToken()
    }

    async getToken(){
        const { colleague } = this;
        let body = {
            access_token:colleague.auth.access_token,
            refresh_token:colleague.auth.refresh_token,
            params: colleague.auth
        }
        const resultTokent = await Vote360.getUserInfo(body)
        if(resultTokent && !resultTokent.err){
            this.setState({accessToken: resultTokent.access_token},async () =>{
                const result = await HaravanHr.checkPermissionUser();
                 if (result && result.data && !result.error) {
                    if (result.data.length && result.data.length > 0) {
                        this.setState({ isManager: result.data });
                    }
                    else{
                        this.setState({ isManager: null });
                    }
                }
                this.loadData()
                this.loadGeneral()
            })  
        }
        else{
            this.setState({
                message: 'Có lỗi vui lòng thử lại sau', 
                listEmploy:[],
                listDepart:[],
                isLoad:false
            })
        }
    }

    checkShow(dataVote){
        const { isManager } = this.state;
        let timeVote = new Date().getTime();
        let checkShow = false
        if(isManager){
            if((timeVote < dataVote.time_vote_manager.to) && (timeVote > dataVote.time_vote_manager.from)){
                checkShow = true
            }
        }
        else {
            if((timeVote < dataVote.time_vote_employee.to) && (timeVote > dataVote.time_vote_employee.from)){
                checkShow = true
            }
        }
        return checkShow;
    }

   async loadVoter(idVote, idDepart, idUer, dataVote){
        const { accessToken, listEmploy, isManager } = this.state;
        let checkShow = this.checkShow(dataVote)
        const result = await Vote360.voteManager(idVote, idDepart, idUer, accessToken);
        if(isManager){
            const resultEmploy = await Vote360.voteEmploy(idVote, idDepart, accessToken);
            const indexManager = isManager.findIndex(e => e.id == idDepart)
            if(indexManager > -1){
                if(resultEmploy && resultEmploy.items){
                    let newList = []
                    listEmploy.map(employ =>{
                        let newEmploy = null
                        const indexCheck = resultEmploy.items.findIndex(m => m.voter_id == employ.employee_id)
                        if(indexCheck < 0){
                            newEmploy = {...employ, isVote:false}
                            newList.push(newEmploy)
                        }
                        else{
                            newEmploy = {...employ, isVote:true}
                            newList.push(newEmploy)
                        }
                    })
                    this.setState({listEmploy:newList})
                }
            }
        }
        else {
            this.setState({listEmploy})
        }

        if(result && result.items){
            const index = result.items.findIndex(e => e.voter_id == idUer)
            if( index > -1){
                this.setState({employeeVote:((checkShow ==true) ? 2 : 0) , voteItem:result.items[index], isFirts:false })
            }
            else{
                this.setState({employeeVote:((checkShow ==true) ? 1 : 0), isFirts: true })
            }
        }
        else{
            this.setState({employeeVote: 0 })
        }
        this.setState({isLoad:false})
    }
    
    async loadGeneral(){
        const result = await Vote360.general(this.state.accessToken);
        if(result && !result.error){
            this.setState({general:result})
        }
    }
    showWarning(value){
        Alert.alert(
            'Thông báo',
            `Bạn đã bình chọn nhiều hơn định mức của phiếu ${value}. Vui lòng xem lại.`,
            [
                { text: 'Đóng', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false }
        )
    }
    Warning(msg){
        Alert.alert(
            'Thông báo',
            `${msg}`,
            [
                { text: 'Đóng', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false }
        )
    }
    
    async saveVote(){
        const { value1, value2, value3, listDepart , isFirts, accessToken} = this.state
        const { length1, length2, length3 , onlyLimitA } = this 
        if((value1.length > length1)){
            this.showWarning('A')
            this.setState({disable:false})
        }
        else if((value2.length > length2) && !onlyLimitA){
            this.showWarning('B')
            this.setState({disable:false})
        }
        else if((value3.length > length3) && !onlyLimitA){
            this.showWarning('C')
            this.setState({disable:false})
        }
        else if((value1.length < length1) || (value2.length < length2) || (value3.length < length3) && !onlyLimitA){
            let msg = `Bình chọn chưa đạt hạn mức quy định. Vui lòng kiểm tra lại.`
            this.Warning(msg)
            this.setState({disable:false})
        }
        else if((value1.length < length1) && onlyLimitA){
            let msg = `Bình chọn chưa đạt hạn mức quy định. Vui lòng kiểm tra lại.`
            this.Warning(msg)
            this.setState({disable:false})
        }
        else{
            let listPoin = []
            let data= null
            let idDepartVote = null
            if(value1 && value1.length >0){
                value1.map(item =>{
                    idDepartVote = item.departmentVote
                    data ={employee_id:item.employee_id, point_level:item.point_level_vote}
                    listPoin.push(data)
                })
            }
            if(value2 && value2.length > 0 ){
                value2.map(item =>{
                    idDepartVote = item.departmentVote
                    data ={employee_id:item.employee_id, point_level:item.point_level_vote}
                    listPoin.push(data)
                })
            }
            if(value3 && value3.length > 0 ){
                value3.map(item =>{
                    idDepartVote = item.departmentVote
                    data ={employee_id:item.employee_id, point_level:item.point_level_vote}
                    listPoin.push(data)
                })
            }
            const index = listDepart.findIndex(m => m.voteID == idDepartVote)
            let voteID = listDepart[index];
            let body = {
                department_id: idDepartVote,
                employee_point_list:listPoin,
                vote_id: isFirts ? voteID : listDepart[index]._id,
            }
            let result = await Vote360.votePoint(body,accessToken )
            if(result){
                this.setState({disable:false, employeeVote:2},()=>
                Toast.show('Lưu phiếu bình chọn thành công'))
            }
            else{
                this.setState({disable:false, employeeVote:2}, () =>{
                    Toast.show('Có lỗi xảy ra, vui lòng thử lại')
                })
            }
        }
    }

    setMonth(value){
        this.clearValue()
        if(value.month < 10){
            const month = '0'+value.month
            this.setState({monthDF:value.year+''+month, listDepart:[], listEmploy:[], isLoad:true}, ()=>{
                this.loadData()
            })
        }
        else{
            this.setState({monthDF:value.year+''+value.month, listDepart:[], listEmploy:[], isLoad:true} ,() =>{
                this.loadData()
            })
        }
    }
    clearValue(){
        this.setState({
            value1:[],
            value2:[],
            value3:[],
        },()=>{
            this.departmentVote = null
        })
    }
    refreshData(){
        this.length1 = 0;
        this.length2 = 0;
        this.length3 = 0;
        this.onlyLimitA = false
        this.departmentVote = null
        this.setState({
            value1:[],
            value2:[],
            value3:[],
            listDepart:[],
            listEmploy:[]
        },() => this.loadData())
    }

    setValue(value, item){
        if(item){
            item ={...item, point_level_vote:value, departmentVote:item.employee_department_vote}
            const { value1, value2, value3} = this.state;
            const index1= value1.findIndex(m => m.employee_haraId == item.employee_haraId)
            const index2= value2.findIndex(m => m.employee_haraId == item.employee_haraId)
            const index3= value3.findIndex(m => m.employee_haraId == item.employee_haraId)
            if(value == 1){
                if(index1 < 0){
                    value1.push(item)
                }
                if(index2 > -1){
                    value2.splice(index2, 1)
                }
                if(index3 > -1){
                    value3.splice(index3, 1)
                }
            }
            else if(value == 2){
                if(index2<0){
                    value2.push(item)
                }
                if(index1 > -1){
                    value1.splice(index1, 1)
                }
                if(index3 > -1){
                    value3.splice(index3, 1)
                }
            }
            else{
                if(index3<0){
                    value3.push(item)
                }
                if(index2 > -1){
                    value2.splice(index2, 1)
                }
                if(index1 > -1){
                    value1.splice(index1, 1)
                }
            }
            this.setState({value1, value2, value3})
        }
    }

    checkEmploy(departmentVote){
        const { listDepart, data } = this.state;
        if(departmentVote){
            this.departmentVote = departmentVote
            listDepart.map(e =>{
                if(e.voteID == departmentVote){
                    const index = e.employee_list.findIndex(m => m.employee_haraId == data.haraId)
                    if(index > -1){
                        e.employee_list.splice(index, 1)
                    }
                    this.setState({listEmploy:e.employee_list},() =>{
                        this.loadVoter(e._id, e.voteID, data.id, e)
                    })
                }
            })
        }
        else if(listDepart.length >  0){
            for(let i = 0; i < listDepart.length; i++){
                listDepart[i].department_active.map(e =>{
                    if(e == listDepart[i].voteID){
                        const index = listDepart[i].employee_list.findIndex(m => m.employee_haraId == data.haraId)
                        if(index > -1){
                            listDepart[i].employee_list.splice(index, 1)
                        }
                        this.setState({listEmploy:listDepart[i].employee_list},() =>{
                            this.loadVoter(listDepart[i]._id, listDepart[i].voteID, data.id, listDepart[i])
                        })
                    }
                })
                break
            }
        }
        else{
            this.setState({listEmploy:[], isLoad:false })
        }
        this.setState({listDepart},() =>{
                this.settingLimit(this.state.settingLimit)
                this.departmentVote = null
            })
    }

    calculator(method, limit, length){
        let res = null
        if(method == "ROUND"){
            res = Math.round((length*limit)/100);
        }
        else if(method == "CEIL"){
            res = Math.ceil((length*limit)/100);
        }
        else{
            res = Math.floor((length*limit)/100);
        }
        return res;
    }

    settingLimit(data){
        let length = this.state.listEmploy.length
        this.onlyLimitA = data.only_limit_ballot_A;
        if(data.only_limit_ballot_A == false){
            if(data.voting_limit[1]){
                this.length1 = this.calculator(data.voting_limit[1].round_method, data.voting_limit[1].limit, length);
            }
            if(data.voting_limit[2]){
                this.length2 = this.calculator(data.voting_limit[2].round_method, data.voting_limit[2].limit, length);
            }
            if(data.voting_limit[3]){
                this.length3 = length - (this.length1 + this.length2)
            }
        }
        else{
            if(data.voting_limit[1]){
                this.length1 = this.calculator(data.voting_limit[1].round_method, data.voting_limit[1].limit, length);
            } 
        }   
    }

    async loadData(){
        const { colleague } = this;
        const { listDepart, accessToken } = this.state;
            const dataUser = await HaravanHr.getColleague(colleague.userInfo.sub)
            const result = await Vote360.getVoteMonth(this.state.monthDF,accessToken)
            const resultLimit = await Vote360.settingLimit(accessToken);
            if((result && result.items && result.items.length > 0) && dataUser && !dataUser.error){
                    result.items.map(item =>{
                        if(item.employee_list && (item.employee_list.length > 0)){
                            const indexEmploy = item.employee_list.findIndex(m => m.employee_haraId == dataUser.data.haraId)
                            if(indexEmploy > -1 ){
                                const indexDepart = item.department_active.findIndex(e => e == item.employee_list[indexEmploy].employee_department_vote)
                                if(indexDepart > -1){
                                    item = {...item, voteID: item.employee_list[indexEmploy].employee_department_vote}
                                    listDepart.push(item)
                                }
                            }
                            this.setState({ listDepart, data: dataUser.data,listDefaul:listDepart}, () =>{
                                if(resultLimit && !resultLimit.error){
                                    this.setState({settingLimit:resultLimit},() =>{
                                        this.settingLimit(resultLimit)
                                        this.checkEmploy()
                                    })
                                }
                            })
                        }
                    })
                    this.setState({ 
                        message: null,
                        isLoad:false
                    })
                }
                else{
                    if(result.error && result.message.length > 0){
                        this.setState({message: result.message[0], 
                            listEmploy:[],
                            listDepart:[],
                            isLoad:false
                        })
                    }
                    else{
                        this.setState({
                            message:null,
                            listEmploy:[],
                            listDepart:[],
                            isLoad:false
                        })  
                    }
                }
    }
}
const mapStateToProps = (state) => ({ ...state });
export default connect(mapStateToProps, mapDispatchToProps)(VotePoint360);