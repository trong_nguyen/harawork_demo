import React, { Component } from 'react';
import { Text, View, TouchableOpacity, FlatList } from 'react-native';
import HeaderIndex from '../../../../../public/components/headerIndex';
import { Ionicons } from '@expo/vector-icons';
import { EvilIcons } from '@expo/vector-icons';
import Collapsible from 'react-native-collapsible';
import { settingApp, HeaderWapper, Arrow} from '../../../../../public';
import Condition from './component/condition';
import TabList from '../../../../../route/components/user/employeeManager/tabList'

class Layout extends Component{

    renderButton() {
        const { isColap } = this.state;
        return (
            <View style={{ flexDirection: 'row', paddingLeft: 10, paddingRight: 10 }}>
                <View style={{ flex: 1 }}>
                    {this.state.removeFilter && <TouchableOpacity
                        onPress={() => {
                            this.condition.refreshFiter()
                            this.setState({ isColap: false})
                        }}
                        style={{ flexDirection: 'row', alignItems: 'center', height: 50 }}>
                        <EvilIcons name='close' color='#FF3B30' size={30} />
                        <Text style={{ color: '#FF3B30', fontSize: 16, marginLeft: 5 }}>Đặt lại bộ lọc</Text>
                    </TouchableOpacity>}
                </View>
                <View style={{ flex: 1, alignItems: 'flex-end' }}>
                    <TouchableOpacity
                        onPress={() => this.setState({ isColap: !this.state.isColap })}
                        style={{ flexDirection: 'row', alignItems: 'center', height: 50 }}>
                        <Text style={{ color: settingApp.color, fontSize: 16, marginRight: 5 }}>
                            {!isColap ? 'Bộ lọc nâng cao' : 'Thu gọn bộ lọc'}
                        </Text>
                        <View>
                            <Arrow isOpen={isColap} color={settingApp.color} />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    renderColap(){
        const { isColap } = this.state;
        return(
            <View style={{ flex: 1}}>
                {this.renderButton()}
                <Collapsible collapsed={!isColap} style={{paddingBottom:1, paddingTop:1}} >
                    <Condition 
                        ref={refs => this.condition = refs}
                        setCondition={
                            condition => this.setState({ isColap: false },
                                () => this.setState({ condition }))
                        }
                        removeFilter = {this.state.removeFilter}
                        setRemoveFilter = {removeFilter => this.setState({removeFilter})}
                    />
                </Collapsible>
                
                <TabList 
                    ref = {refs => this.TabList = refs}
                    screenProps ={{
                        condition: this.state.condition,
                        mainNavigation: this.props.navigation,
                        collapsed: this.state.isColap
                    }}
                />
            </View>
        )
    }

    renderHeader(){
        return (
            <HeaderIndex
                title={
                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                    >
                        <Text style={settingApp.styleTitle}>
                            Duyệt nghỉ phép
                        </Text>
                    </View>
                }
            />
        )
    }

    render(){
        return(
            <View style ={{flex: 1}}>
                {this.renderHeader()} 
                <View style ={{flex: 1}}> 
                    {this.renderColap()}
                </View>
            </View>
        )
    }

}
export default Layout;