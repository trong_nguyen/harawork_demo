import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Alert } from 'react-native';
import { ButtonCustom, settingApp, Utils } from '../../../../../../public';
import { MaterialIcons, EvilIcons, Ionicons } from '@expo/vector-icons';
import moment from 'moment';
import ApprovedLeaveDetail from '../approvedLeaveDetail';

class Item extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isCheck: false,
        };
        this.listDate = [];
        this.data = []
        this.isCheck = false;
    }

    componentWillReceiveProps(nextProps){
        const {item}= this.props.obj;
        const { listApr } = this.props;
        if(nextProps && nextProps.listApr && nextProps.listApr.length > 0){
            nextProps.listApr.map(e=>{
                if(e.id === item.id){
                    if(e.status !== item.status){
                        item.status = e.status
                    }
                }
            })
        }

    }

    checkType(type) {
        if (type == 0) {
            return { type: 'Nghỉ nguyên ngày', time: '8 giờ' }
        } else if (type == 2) {
            return { type: 'Nghỉ buổi sáng', time: '4 giờ'  }
        } else if (type == 3) {
            return { type: 'Nghỉ buổi chiều', time: '4 giờ'  }
        }
    }

    renderCheck(item, titleLeaves) {
        const {isCheck} = this.state;
        if(this.props.listCheck.length == 0){
            this.isCheck = false
        }
            return (
                <View
                    style={{
                        position: 'absolute',
                        height:'85%',
                        width: 50,
                        justifyContent: 'center',
                        alignItems: 'flex-start',
                        borderRightWidth:1,
                        borderRightColor:'#C8C7CC',
                    }}
                >
                {
                    titleLeaves? null:
                    <TouchableOpacity
                        onPress={()=>{
                            this.isCheck = !this.isCheck 
                            this.props.checkItem(item)
                            }}
                        activeOpacity={1}
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'flex-start',
                            backgroundColor: 'transparent',
                        }}
                    >
                        <Ionicons name= {this.isCheck ? 'ios-checkmark-circle' : 'ios-checkmark-circle-outline'} size={40} color={this.isCheck ? '#21469B':'#D1D1D6'}  />
                    </TouchableOpacity>
                }
                </View>
            );
    }
    
    fillDate(item) {
        if (item.requestLeave.length > 0) {
            return (
                item.requestLeave.map((e, i) => {
                    const newList = item.requestLeave.sort((a, b) => {
                        return new Date(b.requestDate) - new Date(a.requestDate);
                    });
                    this.listDate = newList;
                })
            )
        }
    }

    checkStatus(item) {
        if (this.listDate.length > 0) {
            if (this.listDate[0].status == 1
                 && this.listDate[0].type == 0) {
                return {
                    title: 'Đề nghị hủy',
                    requestNote: this.listDate[0].requestNote
                }
            }
        }
    }

    goDetai(item, titleLeaves, typeTime){
        this.props.navigation.navigate('ApprovedLeaveDetail', {item: item, titleLeaves: titleLeaves, typeTime: typeTime})
    }

    render() {
        const { item, index } = this.props.obj;
        let name = Utils.formatLocalTime(item.from, 'DD/MM/YYYY', true)
        const typeTime = this.checkType(item.typeTimeLeave);
        this.fillDate(item);
        let titleLeaves = this.checkStatus(item);
        let status = item.status == 1;
        return (
            <View style={{ 
                flex: 1, 
                justifyContent: 'center', 
                borderTopWidth:index === 0 ? 0 : 1 ,
                borderTopColor:'#C8C7CC',
                marginLeft: 15 }}>
                <TouchableOpacity
                   onPress={() => 
                    this.props.listCheck.length == 0 ?
                    this.goDetai(item, titleLeaves, typeTime) : null
                    }
                    style={{
                        flex: 1,
                        paddingRight: 15,
                        backgroundColor: '#ffffff',
                        paddingBottom: 10,
                        paddingTop: 10,
                        paddingLeft: status ? 15 : 0,
                        marginLeft: status ? 15 : 0,

                    }}
                >
                    <View style={{
                        flexDirection: 'row',
                    }}>
                        <View style={{ 
                            marginLeft: status ? 20 : 0, 
                            paddingLeft: status ? 15 : 0, 
                            flexDirection: 'column', 
                            flex: 1, 
                            flexWrap: 'wrap', 
                            }}>
                            <Text style={{ fontSize: 16, color: settingApp.colorText }}>{item.employeeName} - {item.employeeUserId}</Text>
                            <Text style={{ fontSize: 14, color: '#8E8E93' }}>{item.leaveTypeName}  {typeTime ? '- ' + typeTime.type : null}</Text>
                            <Text style={{ fontSize: 14, color: '#8E8E93' }}>{name} - {item.shiftName}</Text>
                            <Text style={{ fontSize: 14, color: '#FF9500' }}>{titleLeaves ? titleLeaves.title : null}</Text>
                        </View>

                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <MaterialIcons name='keyboard-arrow-right' size={23} color='#D1D1D6' />
                        </View>

                    </View>
                </TouchableOpacity>
                { status ? this.renderCheck(item, titleLeaves) : null}
            </View>
        );
    }
}
export default Item;
