import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity ,FlatList, ActivityIndicator, RefreshControl,} from 'react-native';
import { settingApp, Loading, NotFound } from '../../../../../../../public';
import Item from '../item';

class Layout extends Component {

    renderButton(listCheck) {
        return (
            listCheck.length > 0 ?
                <View style={{ 
                    width: '100%',
                    height: 120, 
                    backgroundColor: '#FFFFFF', 
                    alignItems:'center', 
                    flexDirection:'column', 
                    borderTopColor:'#C8C7CC', 
                    borderTopWidth:1,
                    position:'absolute',
                    bottom:0
                    }}>
                    <Text style={{fontSize:16, color: settingApp.colorText, marginTop:10}}> Đã chọn {listCheck.length} phiếu nghỉ phép</Text>
                    
                    <View style={{flexDirection:'row', width: '100%',
                        height: '70%', justifyContent: 'space-between'}}> 
                        <TouchableOpacity
                        onPress={()=> this.leaveReject()}
                        style={{ backgroundColor: '#FF3B30', borderRadius: 5, height: 60, width: '45%',marginLeft: 10, marginTop: 10, alignItems: 'center', justifyContent: 'center' }}
                    >
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#FFFFFF' }}>Từ chối</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={()=> this.leaveApprod()}
                        style={{ backgroundColor: '#4CD964', borderRadius: 5, height: 60, width: '45%', marginRight: 10, marginTop: 10, alignItems: 'center', justifyContent: 'center' }}
                    >
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#FFFFFF' }}>Duyệt</Text>
                    </TouchableOpacity>
                    </View>
                  
                </View>
                :
                <View />
        )
    }

    onEndReached(){
        if (!this.state.loadMore) {
            if (this.page <= this.totalPage) {
                this.setState({ loadMore: true }, () => this.loadData());
            }
        }
    }

    render() {
        const { list, isLoading, listCheck, loadMore, refreshing } = this.state;
        const status = listCheck.length > 0;
        if (isLoading && !refreshing) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Loading />
                </View>
            )
        }
        else {
            return (
                <View style={{ flex: 1 }}>
                    <View style={{ ...settingApp.shadow,  marginTop: 10, marginBottom: status ? 120 : 0, backgroundColor:'#FFFFFF', flex: list.length > 0 ? 0 : 1  }}>
                        <FlatList
                            data={list}
                            extraData={this.state}
                            keyExtractor={(item, index) => '' + index}
                            renderItem={({ item, index }) =>
                                <Item
                                    obj={{ item, index }}
                                    navigation={this.navigation}
                                    checkItem={data => this.checkItem(data)}
                                    listCheck={this.state.listCheck}
                                    listApr={this.state.listAppr}
                                />
                            }
                            onEndReached={() => this.onEndReached()}
                            onEndReachedThreshold={1}
                            ListFooterComponent={
                                loadMore &&
                                <ActivityIndicator
                                    style={{ height: 50 }}
                                    size="small"
                                    color={settingApp.color}
                                />
                            }
                            ListEmptyComponent={
                                <View style={{flex: 1, backgroundColor:'transparent'}}>
                                    <NotFound />
                                </View>
                            }
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={() => this.onRefresh()}
                                    tintColor={settingApp.color}
                                    colors={[settingApp.color]}
                                />
                            }
                        />
                    </View>
                    {this.renderButton(listCheck)}
                </View>
            )
        }
    }
}
export default Layout;