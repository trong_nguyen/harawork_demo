import Layout from './layout';
import { HaravanHr } from '../../../../../../../Haravan';
import { Utils, Toast } from '../../../../../../../public';
import actions from '../../../../../../../state/action';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class TabList extends Layout {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            isLoading: true,
            listCheck: [],
            loadMore: false,
            refreshing: false,
            listAppr: []
        }
        this.url = props.url;
        this.navigation = props.mainNavigation;
        this.condition = props.condition;
        this.collapsed = props.collapsed;
        this.page = 1;
        this.totalPage = null;
        this.currenList = [];
        this.checkAprLeave = props.aprov.checkAprLeave;
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.condition !== this.props.condition) {
            if (nextProps.condition) {
                this.condition = nextProps.condition
                let link = this.url
                const lable = this.condition.labels ?  `&leavetype=${this.condition.labels}` : ``;
                this.url = `${link}${lable}&from=${this.condition.valueFromDate}&to=${this.condition.valueTodate}`
                this.page = 1;
                this.totalPage = null;
                this.currenList = [];
                this.setState({ isLoading: true }, () => this.loadData())
            } else {
                this.refreshList()
                this.loadData()
            }
        }
        if (nextProps.collapsed && nextProps.collapsed == true) {
            this.url = this.props.url;
            this.page = 1;
            this.totalPage = null;
            this.currenList = [];
        }


        if (nextProps.aprov.checkAprLeave == true && nextProps.aprov.checkAprLeave != this.props.aprov.checkAprLeave) {
            this.refreshList()
            this.loadData()
        }
        else if (nextProps.aprov.checkAprLeave == false && nextProps.aprov.checkAprLeave != this.props.aprov.checkAprLeave){
            Toast.show('Có lỗi, vui lòng thử lại')
        }
    }


    refreshList() {
        this.setState({ isLoading: true })
        this.url = this.props.url;
        this.page = 1;
        this.totalPage = null;
        this.currenList = [];
    }

    onRefresh() {
        this.setState({ refreshing: true }, async () => {
            this.refreshList();
            this.loadData();
        })
    }

    componentDidMount() {
        this.loadData()
    }

    checkItem(item) {
        const { listCheck } = this.state;
        if (listCheck.length === 0) {
            listCheck.push(item);
        }
        else {
            const indexList = listCheck.findIndex(e => e.id === item.id)
            if (indexList > -1) {
                listCheck.splice(indexList, 1)
            }
            else {
                listCheck.push(item)
            }
        }
        this.setState({ listCheck }, () => this.setState({ listAppr: [] }))
    }

    leaveApprod() {
        const { listCheck, list, listAppr } = this.state;
        const newList = listCheck;
        this.setState({ listCheck: [], isLoading:true })
        newList.map(e => {
            this.setState({}, async () => {
                const noteAprr = ' '
                list.map(m => {
                    if (m.id == e.id) {
                        m = { ...m, status: 2 }
                        listAppr.push(m)
                    }
                })
                const result = await HaravanHr.getLeavesApprove(e.id, noteAprr)
                if(result && result.data && !result.error){
                    this.setState({ listAppr: listAppr}, () => {
                       this.props.actions.aprov.checkAprLeave(true)
                    })
                }
            })
        })
    }

    leaveReject() {
        const { listCheck, list, listAppr } = this.state;
        const newList = listCheck;
        this.setState({ listCheck: [], isLoading:true })
        newList.map(e => {
            this.setState({}, async () => {
                list.map(m => {
                    if (m.id == e.id) {
                        m = { ...m, status: 3 }
                        listAppr.push(m)
                    }
                })
                let noteAprr = ' '
                const result = await HaravanHr.getLeavesReject(e.id, noteAprr)
                if(result && result.data && !result.error){
                    this.setState({ listAppr: listAppr}, () => {
                       this.props.actions.aprov.checkAprLeave(true)
                    })
                }
            })
        })
    }

    async loadData() {
        const { url, page, totalPage, currenList } = this;
        if (!totalPage || (totalPage && !isNaN(totalPage) && page <= totalPage)) {
            let newList = []
            const result = await HaravanHr.getSupervisorLeaves(url, page)
            if (result && result.data && !result.error) {
                const count = result.data.totalCount;
                this.props.navigation.setParams({ count });
                result.data.data.map(item => {
                    if (newList.length === 0) {
                        newList.push(item);
                    }
                    else {
                        const indexList = newList.findIndex(e => e.id === item.id)
                        if (indexList > -1) {
                            newList.splice(indexList, 1)
                        }
                        else {
                            newList.push(item)
                        }
                    }
                })

                if (!this.totalPage) {
                    this.totalPage = Math.ceil(count / 20);
                }
                newList = [...currenList, ...newList]
                this.currenList = newList
                this.page = page + 1;
                this.setState({ list: newList, isLoading: false, loadMore: false, refreshing: false },() =>{
                    this.props.actions.aprov.checkAprLeave(null)
                })
            
            }
            else {
                this.setState({ isLoading: false, refreshing: false },() =>{
                    this.props.actions.aprov.checkAprLeave(null)
                })
            }
        }
    }
}
mapStateToProps = state => ({ ...state });
mapDispatchToProps = dispatch => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch)
    }
    return { actions: acts }
}
export default connect(mapStateToProps, mapDispatchToProps)(TabList);