import Layout from './layout';
import { Utils } from '../../../../../../../public';
import moment from 'moment';
import { HaravanHr } from '../../../../../../../Haravan';

class Condition extends Layout{
    constructor(props){
        super(props);
        const date = new Date();
        const formDate = new Date(date.getFullYear(), date.getMonth(), 1);
        const todate = new Date();

        this.state ={
            valueFromDate: formDate,
            valueTodate: todate,
            isVisibleToDate: false,
            isVisibleFromDate: false,

            visibleLables: false
        }
        this.leaveTypes = []

        this.formDate = formDate
        this.todate = todate
        this.labels = ''
        
    }

    async componentDidMount(){
        const result = await HaravanHr.getLeavesTypes()
        if(result && result.data && !result.error){
            this.leaveTypes = result.data.data
        }
    }

    setRemove(){
        const { valueTodate, valueFromDate, labels} = this.state;
        if (
            (this.formatTime(valueFromDate) !== this.formatTime(this.formDate) ||
                this.formatTime(valueTodate) !== this.formatTime(this.todate)) ||
            this.formatTime(valueFromDate) === this.formatTime(this.formDate) ||
            this.formatTime(valueTodate) === this.formatTime(this.todate) ||
            (labels !== '')) {
            this.props.setRemoveFilter(true)
        }
        else {
            this.props.setRemoveFilter(false)
        }
    }


    validate(type, value) {
        if (type === 'fromDate') {
            if (!!(value <= this.state.valueTodate)) {
                this.setState({ valueFromDate: value })
            } else {
                this.setState({ valueFromDate: value, valueTodate: value })
            }
        } else {
            if (!!(value >= this.state.valueFromDate)) {
                this.setState({ valueTodate: value })
            } else {
                this.setState({ valueTodate: value, valueFromDate: value });
            }
        }
        this.setRemove()
    }

    checkStatus(data){
        this.labels = data
        this.setRemove()
    }

    formatTime(value){
        return Utils.formatTime(value, 'DD/MM/YYYY');
    }

    refreshFiter(){
        this.setState({
            valueTodate: this.todate,
            valueFromDate: this.formDate
        })
        this.labels = '';
        this.props.setCondition(null)
        this.props.setRemoveFilter(false)
    }

    submit(){
        let { valueTodate, valueFromDate } = this.state;
        valueTodate = valueTodate ? Utils.formatTime(valueTodate, 'YYYY-MM-DD', true) : null;
        valueFromDate = valueFromDate ? Utils.formatTime(valueFromDate,'YYYY-MM-DD', true) : null;
        let labels = this.labels ? this.labels.id : 0
        let condition ={
            valueTodate,
            valueFromDate,
            labels
        }
        this.props.setCondition(condition)
    }    
}
export default Condition;