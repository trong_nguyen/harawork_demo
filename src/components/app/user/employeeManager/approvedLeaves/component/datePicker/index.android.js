import React, { Component } from 'react';
import { View, DatePickerAndroid } from 'react-native';

class DatePicker extends Component {

    submit(valueDate) {
        this.props.close();
        this.props.validate(valueDate);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.visible) {
            this.openCalendar();
        }
    }

    async openCalendar() {
        try {
            const { action, year, month, day } = await DatePickerAndroid.open({
                date: new Date(this.props.value)
            });
            if (action !== DatePickerAndroid.dismissedAction) {
                const valueDate = new Date(`${month + 1}/${day}/${year}`);
                this.submit(valueDate);
            } else {
                this.props.close();
            }
        } catch ({ code, message }) {
            console.warn('Cannot open date picker', message);
            this.props.close();
        }
    }


    render() {
        return <View />;
    }
}

export default DatePicker;