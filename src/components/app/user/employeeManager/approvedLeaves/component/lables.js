import React, { Component } from 'react';
import { View, Text, Modal, TouchableOpacity } from 'react-native';
import { settingApp, Utils, HeaderWapper } from '../../../../../../public';
import { Ionicons } from '@expo/vector-icons';

class Label extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        const { leaveTypes } = this.props;
        if (leaveTypes) {
            return (
                <Modal
                    animationType='fade'
                    transparent={true}
                    visible={this.props.visible}
                    onRequestClose={() => this.props.close()}
                >
                    <HeaderWapper
                        isModal={true}
                        style={{
                            justifyContent: 'space-between',
                            flexDirection: 'row',
                        }}
                    >
                        <TouchableOpacity
                            onPress={() => this.props.close()}
                            style={{ width: 55, height: 43, justifyContent: 'center', alignItems: 'center' }}
                        >
                            <Ionicons name='ios-arrow-back' size={23} color='#ffffff' />
                        </TouchableOpacity>

                        <Text style={{ alignItems: 'center', marginTop: 10, fontSize: 16, fontWeight: 'bold', color: '#FFFFFF' }}>
                            Loại phép
                    </Text>
                        <View style={{ width: 55, height: 43, justifyContent: 'center', alignItems: 'center' }} />
                    </HeaderWapper>

                    <View style={{flex: 1, backgroundColor:'#C8C7CC'}}>
                    <View style={{ width: '100%', backgroundColor: '#FFFFFF', flexDirection: 'column'}}>
                        {leaveTypes.map((e, i) => {
                            return (
                                
                                <View 
                                key={i}
                                style={{ borderBottomColor: '#C8C7CC', borderBottomWidth: 1, marginLeft: 15 }}>
                                <TouchableOpacity
                                    
                                    activeOpacity={1}
                                    onPress={() => {
                                        this.props.close()
                                        this.props.data(e)
                                    }}
                                    style={{ justifyContent: 'space-between', marginLeft: 10, paddingBottom: 15, paddingTop: 15 }}
                                >
                                    <Text style={{ fontSize: 16, color: settingApp.colorText }}> {e.name} </Text>
                                </TouchableOpacity>
                                </View>
                            )
                        })}
                    </View>
                    </View>
                </Modal>
            )
        }
        else {
            return (
                <View />
            )
        }
    }
}
export default Label;