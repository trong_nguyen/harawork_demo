import Layout from './layout';
import { settingApp, imgApp, Utils } from '../../../../../../public';
import { HaravanHr } from '../../../../../../Haravan';
import actions from '../../../../../../state/action';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class ApprovedLeaveDetail extends Layout{
    constructor(props){
        super(props);
        this.state= {
            dataDetai: [],
            noteApprod:'',
            modalVisible: false,
            isLoading: true,
            appProved: null,
            status: null,
            isLoad: false,
            isReload:true,
        }
        
        this.item = props.navigation.state.params.item;
        this.titleLeaves = props.navigation.state.params.titleLeaves;
        this.typeTime = props.navigation.state.params.typeTime;
        this.statusApproved = [];
        this.listDate =[]
    }

    componentWillReceiveProps(nextProps){
        if(nextProps && nextProps.app && nextProps.app.reloadNotify && nextProps.app.reloadNotify==true ){
            this.item = nextProps.navigation.state.params.item;
            this.titleLeaves = nextProps.navigation.state.params.titleLeaves;
            this.typeTime = nextProps.navigation.state.params.typeTime;
            this.setState({
                isReload:nextProps.navigation.state.params.isReload,
                dataDetai: [],
                noteApprod:'',
                appProved: null,
                status: null,
                isLoading:true
                },() =>{
                    this.loadData()
                    this.props.actions.reloadNotify(null)
            })
        }

    
    }

    checkReload(){
        const { isReload } = this.state;
        if(isReload == true){
            this.setState({
                isLoading:true
            }, () => { this.loadData() })
        }
    }

    componentDidMount() {
        this.loadData()
    }

    async loadData(){
        const result = await HaravanHr.getLeavesDeatail(this.item.id)
        if (result && result.data && !result.error) {
            this.setState({ dataDetai: result.data, isLoading: false, isLoad:false, isReload:false }, () =>{
                this.fillDate()
            })
        }
        else {
            this.setState({ isLoading: false, isLoad:false, isReload:false })
        }
    }

    fillDate() {
        const { item } = this
        if (item.requestLeave && item.requestLeave.length > 0) {
                item.requestLeave.map((e, i) => {
                   const newList = item.requestLeave.sort((a, b) => {
                        return new Date(b.requestDate) - new Date(a.requestDate);
                    });
                    this.listDate = newList;
                })
        }
    }

    //tu choi
    async leaveReject(id, note){
        const { item, listDate } = this;
        const { noteApprod }= this.state;
        this.setState({ isLoad: true})
        let noteArrp = noteApprod ? noteApprod : ' ';
        const result = await HaravanHr.getLeavesReject(id, noteArrp)
        if(result && result.data && !result.error){
            this.props.actions.aprov.checkAprLeave(true)
            this.props.navigation.pop()
        }
        else{
            this.props.actions.aprov.checkAprLeave(false)
            this.props.navigation.pop()
        }

        if (listDate && listDate.length > 0) {
            const body = {
                ApproverNote: noteApprod ? noteApprod : 'ĐỒNG Ý PHIẾU YÊU CẦU',
                Id: listDate[0].id,
                Status: 2,
            }
            const resultRequest = await HaravanHr.putEmployeeRequest(body)
        }
    }

    //duyet
    async putApproved(id, status, type){
        const { item, listDate } = this;
        this.setState({isLoad:true})
        const { noteApprod }= this.state;
        let noteArrp = noteApprod ? noteApprod : ' ';
        if(type == 4 && listDate && listDate.length > 0){
            let body = {
                ApproverNote: noteApprod ? noteApprod : 'TỪ CHỐI PHIẾU YÊU CẦU',
                Id: listDate[0].id,
                Status: 3
            }
            const resultRequest = await HaravanHr.putEmployeeRequest(body)
            if(resultRequest && resultRequest.data && !resultRequest.error){
                this.props.actions.aprov.checkAprLeave(true)
                this.props.navigation.pop()
            }
            else{
                this.props.actions.aprov.checkAprLeave(false)
                this.props.navigation.pop()
            }
        }
        else{
            const result = await HaravanHr.getLeavesApprove(id, noteArrp)
            if(result && result.data && !result.error){
                this.props.actions.aprov.checkAprLeave(true)
                this.props.navigation.pop()
            }
            else{
                this.props.actions.aprov.checkAprLeave(false)
                this.props.navigation.pop()
            }
        }
    }

    checkType(type, time) {
        if (type == 0) {
            let timeTo =  time.shiftTimeframe.checkOutTime;
            let timeFrom = time.shiftTimeframe.checkInTime;
            return { timeTo: timeTo, timeFrom: timeFrom }
        } else {
            let timeTo =  Utils.formatLocalTime(time.to, 'HH:mm', true)
            let timeFrom =  Utils.formatLocalTime(time.from ,'HH:mm', true);
            return { timeTo: timeTo, timeFrom: timeFrom }
        }
    }

    getNote(note){
        this.setState({ noteApprod: note})
    }

    componentWillUnmount(){
        this.props.actions.aprov.checkAprLeave(null)
    }
}
mapStateToProps = state => ({ ...state });
mapDispatchToProps = dispatch => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch)
    }
    return { actions: acts }
}
export default connect(mapStateToProps, mapDispatchToProps)(ApprovedLeaveDetail);