import React, { Component } from 'react';
import {Text, View, TouchableOpacity, Modal, TextInput, } from 'react-native';
import { settingApp } from '../../../../../../../public';

class ModalDiaLog extends Component{
    constructor(props){
        super(props);
        this.state={
            status: this.props.status,
            approvedBt: this.props.approvedBt,
            note: '',
            data: this.props.data,
        }
    }

    checkStatus(status, approvedBt){
        if(status == 4 && approvedBt == false){
            return{ title: 'từ chối hủy' }
        } else if(status == 4 && approvedBt == true){
            return{ title: 'đồng ý hủy'}
        } else if(approvedBt){
            return{ title: 'muốn duyệt'}
        } else{
            return{ title: 'muốn từ chối'}
        }
    }

    buttonAccept(approvedBt, data, status){
        if(approvedBt == true && !(status == 4)){
            this.props.putApproved(data.id, 2)
        }
        else if(approvedBt == false && !(status == 4)){
            this.props.reject(data.id)
        }
        else if(status == 4 && approvedBt == true){
            this.props.reject(data.id)
        } 
        else if(status == 4 && approvedBt == false){
            this.props.putApproved(data.id, 2)
        } 
        this.props.close()
    }

    render() {
        const { status }= this.state;
        const {  width, height } = settingApp
        let massage = this.checkStatus(status, this.props.approvedBt)
        return (
            <Modal
                animationType='fade'
                visible={this.props.modalVisible}
                transparent={true}
                onRequestClose={() => this.props.close()}
            >
            
                <View style={{flex: 1, justifyContent:'center', alignItems:'center', backgroundColor:'rgba(4, 4, 15, 0.5)'}}>
                <View style={{ width: (width - 60), height:status == 4 ? (height * 0.35) : (height * 0.25), backgroundColor: '#FFFFFF', borderRadius:10, justifyContent:'space-between', flexDirection:'column'}}>
                    <View style={{alignItems:'center', marginTop:10}}>
                        <Text style={{color: settingApp.colorText, fontSize:18, fontWeight:'bold'}}>Thông báo </Text>
                        
                    </View>
                    <View style={{alignItems:'center', marginBottom:5, justifyContent:'center'}}>
                        <Text style={{color: settingApp.colorText, fontSize:14, width:'80%', alignItems:'center', textAlign:'center'}}
                        >Bạn có chắc chắn { massage.title } phiếu nghỉ phép này?</Text>
                    </View>
                        {status == 4 ?
                    <View style={{justifyContent:'center', alignItems:'center'}}>
                            <View style={{height:70, width:'90%', borderWidth:1, borderColor:'#C8C7CC', marginLeft:5, marginRight:5, borderRadius:5}}>
                                <TextInput
                                    underlineColorAndroid='transparent'
                                    placeholder = 'Ghi chú'
                                    placeholderTextColor='#8E8E93'
                                    multiline ={true}
                                    style={{ fontSize:16, padding:10 }}
                                    value={this.state.note}
                                    onChangeText={(note) => this.setState({ note })}
                                />
                            </View>
                    </View>
                            : null}
                    <View style={{ height:50,flexDirection:'row', justifyContent:'space-between', borderTopColor:'#C8C7CC', borderTopWidth:0.5}}>
                        <TouchableOpacity
                            onPress={()=> this.props.close()}
                            style={{ width:'50%',alignItems:'center', justifyContent:'center', borderRightWidth:0.5, borderRightColor:'#C8C7CC'}}
                        >
                            <Text style={{color:'#21469B', fontSize:14}}> Đóng </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={()=> {
                                this.props.note(this.state.note)
                                this.buttonAccept(this.props.approvedBt, this.state.data, status)
                                }}
                            style={{alignItems:'center', justifyContent:'center',  width:'50%'}}
                        >
                            <Text style={{color: this.props.approvedBt == false ? '#FF3B30' : '#4CD964', fontSize:14 }}>{this.props.approvedBt == false ? 'Từ chối': status == 4 ? 'Xác nhận' : 'Duyệt'}</Text>
                        </TouchableOpacity>
                    </View>
                    </View>
                </View>
            </Modal>
        )
    }
}
export default ModalDiaLog;