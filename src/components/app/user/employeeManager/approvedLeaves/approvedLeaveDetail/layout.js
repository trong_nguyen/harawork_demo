import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, ScrollView, TextInput, KeyboardAvoidingView, Alert, Linking, ActivityIndicator} from 'react-native';
import HeaderWapper from '../../../../../../public/components/headerWapper';
import { settingApp, imgApp, Utils, Loading, NotFound } from '../../../../../../public';
import { MaterialIcons, Ionicons } from '@expo/vector-icons';
import { HaravanHr } from '../../../../../../Haravan';
import ModalButton from './component/modalButton';
import moment from 'moment';

class Layout extends Component {

    renderHeader() {
        return (
            <HeaderWapper
                style={{
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                }}
            >
                <TouchableOpacity
                    onPress={() => this.props.navigation.pop()}
                    style={{ width: 55, height: 43, justifyContent: 'center', alignItems: 'center' }}
                >
                    <Ionicons name='ios-arrow-back' size={23} color='#ffffff' />
                </TouchableOpacity>

                <Text style={{ alignItems: 'center', marginTop: 10, fontSize: 16, fontWeight: 'bold', color: '#FFFFFF' }}>
                    Chi tiết phiếu nghỉ phép
                </Text>
                <View style={{ width: 55, height: 43, justifyContent: 'center', alignItems: 'center' }} />
            </HeaderWapper>
        )
    }

    checkApproved(status, titleLeaves) {
        if (status == 1 && !titleLeaves) {
            return { title: 'Chưa duyệt', color: '#FF9500', status: 1 }
        } else if (status == 2 && !titleLeaves) {
            return { title: 'Đã duyệt', color: '#4CD964', status: 2 }
        } else if (status == 3 && !titleLeaves) {
            return { title: 'Từ chối', color: '#FF3B30', status: 3 }
        } else if (titleLeaves) {
            return { title: 'Đề nghị huỷ', color: '#FF9500', status: 4 }
        }
    }

    renderButton(status) {
        if (status == 2 || status == 3) {
            const approd = status == 2;
            return (
                <View style={{
                    width: '100%', height: 80, backgroundColor: '#FFFFFF', borderTopColor: '#C8C7CC',
                    borderTopWidth: 1
                }}>
                    <TouchableOpacity
                        onPress={() =>
                            this.setState({ modalVisible: true }, () => this.setState({ appProved: approd ? false : true }))
                        }
                        style={{ backgroundColor: approd ? '#FF3B30' : '#4CD964', borderRadius: 5, height: '70%', margin: 10, alignItems: 'center', justifyContent: 'center' }}
                    >
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#FFFFFF' }}> {approd ? 'Từ chối' : 'Duyệt'} </Text>
                    </TouchableOpacity>
                </View>
            )
        }
        else {
            const approd = status == 1;
            return (
                <View style={{
                    width: '100%',
                    height: approd ? 150 : 80,
                    backgroundColor: '#FFFFFF',
                    justifyContent: 'space-between',
                    flexDirection: approd ? 'column' : 'row',
                    borderTopColor: '#C8C7CC',
                    borderTopWidth: 1
                }}>
                    {approd ?
                        <View style={{ width: '100%', marginBottom: 10, marginLeft: 10, flexWrap: 'wrap' }}>
                            <TextInput
                                underlineColorAndroid='transparent'
                                placeholder = 'Ghi Chú'
                                style={{ flexWrap: 'wrap', maxWidth: '100%', fontSize:16 }}
                                value={this.state.noteApprod}
                                onChangeText={(noteApprod) => this.setState({ noteApprod })}
                            />
                        </View>
                        : null
                    }
                    <View style={{
                        flexDirection: 'row', width: '100%',
                        height: 80, justifyContent: 'space-between',
                    }}>
                        <TouchableOpacity
                            onPress={() => {
                                this.setState({ modalVisible: true }, () => this.setState({ appProved: false }))
                            }}
                            style={{ backgroundColor: '#FF3B30', borderRadius: 5, height: 60, width: '45%', marginLeft: 10, marginTop: 10, alignItems: 'center', justifyContent: 'center' }}
                        >
                            <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#FFFFFF' }}>Từ chối</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => {
                                this.setState({ modalVisible: true }, () => this.setState({ appProved: true }))
                            }}
                            style={{ backgroundColor: '#4CD964', borderRadius: 5, height: 60, width: '45%', marginRight: 10, marginTop: 10, alignItems: 'center', justifyContent: 'center' }}
                        >
                            <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#FFFFFF' }}>{approd ? 'Duyệt' : 'Đồng ý'}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }
    }

    renderFolower(dataDetai) {
        if (dataDetai.follower.length > 0) {
            return (
                dataDetai.follower.map((e, i) => {
                    return (
                        <Text
                            key={i}
                            style={{ fontSize: 16, color: '#8E8E93' }}>{e.employeeName} - {e.employeeUserId}</Text>
                    )
                })
            )
        } else {
            return (
                <Text style={{ fontSize: 16, color: '#8E8E93' }}>Hiện tại không có người theo dõi</Text>
            )
        }
    }
    renderConten() {
        const { dataDetai, isLoading, noteApprod } = this.state;
        if (!isLoading) {
            if (dataDetai && dataDetai.id) {
                const time = this.checkType(dataDetai.typeTimeLeave, dataDetai)
                const toDate = Utils.formatLocalTime(dataDetai.to, 'DD/MM/YYYY', true);
                const fromDate = Utils.formatLocalTime(dataDetai.from, 'DD/MM/YYYY', true);
                this.statusApproved = this.checkApproved(dataDetai.status, this.titleLeaves);
                const title = this.checkApproved(dataDetai.status, this.titleLeaves);
                let note = dataDetai.approverNote;
                const phone = 'tel://' + (dataDetai.employee.phone)
                return (
                    <View style={{ flex: 1 }}>
                        <KeyboardAvoidingView behavior='padding' style={{ flex: 1 }}>
                            <ScrollView>
                                <View style={{
                                    width: settingApp.width,
                                    backgroundColor: '#FFFFFF',
                                    marginTop: 15
                                }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', borderBottomColor: '#C8C7CC', borderBottomWidth: 1, marginLeft: 15 }}>
                                        <View style={{ flexDirection: 'column', paddingBottom: 5, paddingTop: 5 }}>
                                            <Text style={{ fontSize: 16, color: settingApp.colorText }}>{dataDetai.employeeName} - {dataDetai.employeeUserId} </Text>
                                            <Text style={{ fontSize: 16, color: '#8E8E93' }}>{dataDetai.employee.phone} </Text>
                                        </View>
                                        <TouchableOpacity
                                            onPress={() => Linking.openURL(phone)}
                                            style={{ width: 50, height: 50, alignItems: 'center', justifyContent: 'center' }}
                                        >
                                            <Image style={{ width: 30, height: 30, marginRight: 15 }} source={imgApp.call} />
                                        </TouchableOpacity>
                                    </View>

                                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginLeft: 10, paddingBottom: 15, paddingTop: 15 }}>
                                        <Text style={{ fontSize: 16, color: settingApp.colorText }}> Trạng thái </Text>
                                        <Text style={{ fontSize: 16, color: this.statusApproved.color, marginRight: 10 }}>{this.statusApproved.title} </Text>
                                    </View>
                                    {(note || this.titleLeaves) ? <View style={{ flexDirection: 'column', justifyContent: 'space-between', marginLeft: 10, paddingBottom: 5, paddingTop: 5, borderTopColor: '#C8C7CC', borderTopWidth: 1 }}>
                                        <Text style={{ fontSize: 16, color: settingApp.colorText }}> {this.titleLeaves ? 'Lý do hủy' : 'Ghi chú người duyệt'} </Text>
                                        <Text style={{ fontSize: 16, color: '#8E8E93', marginRight: 10, marginLeft: 5 }}>{this.titleLeaves ? this.titleLeaves.requestNote : note} </Text>
                                    </View> :
                                        null
                                    }
                                </View>

                                <View style={{
                                    width: settingApp.width,
                                    backgroundColor: '#FFFFFF',
                                    marginTop: 15
                                }}>
                                    <View style={{ flexDirection: 'column', justifyContent: 'space-between', marginLeft: 15, paddingBottom: 5, paddingTop: 5 }}>
                                        <Text style={{ fontSize: 16, color: settingApp.colorText }}>Ca làm việc</Text>
                                        <Text style={{ fontSize: 16, color: '#8E8E93' }}>{dataDetai.shiftName}</Text>
                                    </View>

                                    <View style={{ flexDirection: 'column', justifyContent: 'space-between', marginLeft: 15, paddingBottom: 5, paddingTop: 5 }}>
                                        <Text style={{ fontSize: 16, color: settingApp.colorText }}>Loại phép</Text>
                                        <Text style={{ fontSize: 16, color: '#8E8E93' }}>{dataDetai.leaveTypeName}</Text>
                                    </View>


                                    <View style={{ flexDirection: 'column', justifyContent: 'space-between', marginLeft: 15, paddingBottom: 5, paddingTop: 5 }}>
                                        <Text style={{ fontSize: 16, color: settingApp.colorText }}>Thời gian nghỉ</Text>
                                        <Text style={{ fontSize: 16, color: '#8E8E93' }}>{this.typeTime ? (this.typeTime.type + ' ( ' + this.typeTime.time + ' )') : 'Không rõ'} </Text>
                                    </View>

                                    <View style={{ flexDirection: 'column', justifyContent: 'space-between', marginLeft: 15, paddingBottom: 5, paddingTop: 5 }}>
                                        <Text style={{ fontSize: 16, color: settingApp.colorText }}>Khoảng thời gian</Text>
                                        <Text style={{ fontSize: 16, color: '#8E8E93' }}>
                                            {time.timeFrom} ngày {fromDate} - {time.timeTo} ngày {toDate}
                                        </Text>
                                    </View>

                                    <View style={{ flexDirection: 'column', justifyContent: 'space-between', marginLeft: 15, paddingBottom: 5, paddingTop: 5 }}>
                                        <Text style={{ fontSize: 16, color: settingApp.colorText }}>Lý do nghỉ phép</Text>
                                        <Text style={{ fontSize: 16, color: '#8E8E93' }}>{dataDetai.reason}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'column', justifyContent: 'space-between', marginLeft: 15, paddingBottom: 5, paddingTop: 5 }}>
                                        <Text style={{ fontSize: 16, color: settingApp.colorText }}>Người theo dõi</Text>
                                        {this.renderFolower(dataDetai)}
                                    </View>

                                </View>
                                <View style={{ height: (this.statusApproved.status == 1 ? 160 : 80), marginTop: 15 }}>
                                    {this.renderButton(this.statusApproved.status)}
                                </View>
                            </ScrollView>
                        </KeyboardAvoidingView>

                        <ModalButton
                            modalVisible={this.state.modalVisible}
                            close={() => this.setState({ modalVisible: false })}
                            status={this.statusApproved.status}
                            approvedBt={this.state.appProved}
                            data={dataDetai}
                            note={note => this.getNote(note)}
                            reject={(id, note) => this.leaveReject(id, note)}
                            putApproved={(id, status, type) => this.putApproved(id, status, this.statusApproved.status)}
                        />
                    </View>
                )
            }
            else if (dataDetai.length == 0) {
                return (
                    <NotFound />
                )
            }
        }
        else {
            return (
                <Loading />
            )
        }
    }

    render() {
        const { isLoad } = this.state;
            return (
                <View style={{ flex: 1 }}>
                    {this.renderHeader()}
                    { this.renderConten() }
                    {
                        isLoad ?  <View style={{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: 0,
                        backgroundColor: 'transparent',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}> 
                        <ActivityIndicator size='large' color={settingApp.color} />
                    </View> : <View />
                    }
                </View>
            )
    }
}
export default Layout;