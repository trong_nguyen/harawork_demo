import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Modal, TextInput, ScrollView } from 'react-native';
import { settingApp } from '../../../../../../../public';
import { MaterialIcons, Ionicons, Entypo } from '@expo/vector-icons';
import Collapsible from 'react-native-collapsible';

class ModalButon extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isColap: false,
            type: null,
            title: null,
            data: [
                {
                    title: 'Cả ngày', type: 1
                },
                {
                    title: 'Nửa ngày', type: 2
                },
                {
                    title: 'Duyệt như đã chấm', type: 4
                },
            ]
        }
        this.typeDefaul = null;
    }

    render() {
        const { width, height } = settingApp
        const { statusApr, timeKeeping, listCheck, listType } = this.props;
        const { isColap, title, type, data } = this.state;
        return (
            <Modal
                animationType='fade'
                visible={this.props.modalVisible}
                transparent={true}
                onRequestClose={() => this.props.close()}
            >
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(4, 4, 15, 0.5)' }}>
                    <View style={{ width: (width - 60), height: statusApr ?  (height * 0.4) : (height * 0.3), backgroundColor: '#FFFFFF', borderRadius: 10, justifyContent: 'space-between', flexDirection: 'column', alignItems: 'center' }}>
                        <View style={{ alignItems: 'center', marginTop: 10 }}>
                            <Text style={{ color: settingApp.colorText, fontSize: 18, fontWeight: 'bold' }}>Thông báo </Text>
                        </View>

                        <View style={{ alignItems: 'center', marginBottom:10, justifyContent: 'center', width: '90%' }}>
                            <Text
                                numberOfLines={3}
                                style={{ color: settingApp.colorText, fontSize: 14, width: '80%', alignItems: 'center', justifyContent:'center' }}
                            >Bạn có {statusApr ? 'muốn duyệt những phiếu chấm công này thành' : 'chắc chắn muốn từ chối những phiếu chấm công này?'}</Text>
                        </View>

                        {statusApr ?
                            <View style={{width: '90%', borderWidth: 0.5, borderColor: '#C8C7CC', marginBottom: 10, flexDirection: 'column', justifyContent:'center' }}>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState({ isColap: !this.state.isColap }) 
                                        }}
                                    style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%', alignItems:'center' }}
                                >
                                    <View style={{ justifyContent: 'center', alignItems:'center' }}>
                                        <Text style={{ color: '#21469B', fontSize: 14, left: 1, height: 40, paddingTop:5}}> {title ? title : data[0].title } </Text>
                                    </View>
                                    <View style={{ alignItems:'center'}}>
                                      <Entypo name='chevron-small-down' size={20} color = '#D1D1D6' />
                                    </View>
                                </TouchableOpacity>
                                    <Collapsible collapsed={!isColap} style={{ paddingBottom: 1, paddingTop: 1, width: '100%', backgroundColor:'#FFFFFF',height:100 }}  >
                                    <ScrollView >
                                        <View style={{ justifyContent: 'space-between', width: '100%', flexDirection: 'column', }}>
                                          { 
                                                data.map((e, i) => {
                                                    return (
                                                        <TouchableOpacity
                                                            key={i}
                                                            onPress={() => {
                                                                this.setState({ title: e.title, type: e.type, isColap:false,})
                                                            } }
                                                            style={{ height: 40, width: '100%', marginTop: 3, borderTopColor: '#C8C7CC', borderTopWidth: 1,  paddingLeft:5, justifyContent:'center' }}
                                                        >
                                                            <Text>{e.title}</Text>
                                                        </TouchableOpacity>
                                                    )
                                                })
                                          }
                                        </View>
                                        </ScrollView>
                                    </Collapsible>
                            </View>
                            :
                            null}
                        {
                            isColap ? null :
                            <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', borderTopColor: '#C8C7CC', borderTopWidth: 0.5 }}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({ isColap: false, title:null, type: null })
                                    this.props.close()
                                }}
                                style={{ width: '50%', alignItems: 'center', justifyContent: 'center', borderRightWidth: 0.5, borderRightColor: '#C8C7CC' }}
                            >
                                <Text style={{ color: '#21469B', fontSize: 14 }}> Đóng </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => {
                                    this.props.close()
                                    timeKeeping((statusApr ? 3 : 4), (this.state.type ? this.state.type : statusApr ? data[0].type : null))
                                    this.setState({ isColap: false, title:null, type: null})
                                }}
                                style={{ alignItems: 'center', justifyContent: 'center', width: '50%' }}
                            >
                                <Text style={{ color: statusApr ? '#4CD964' : '#FF3B30', fontSize: 14 }}>{statusApr ? 'Duyệt' : 'Từ chối'}</Text>
                            </TouchableOpacity>
                        </View>
                        }
                        
                    </View>
                </View>
            </Modal>
        )
    }
}
export default ModalButon;