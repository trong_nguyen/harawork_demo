import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, FlatList } from 'react-native';
import {
    HeaderWapper,
    index,
    Utils,
    Loading,
    settingApp,
} from '../../../../../../../public';
import { MaterialIcons, Ionicons } from '@expo/vector-icons';

class Item extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.isCheck = false;
    }

    renderCheck(item) {
        if (this.props.listCheck.length == 0) {
            this.isCheck = false;
        }
        return (
            <View
                style={{
                    position: 'absolute',
                    height: '85%',
                    width: 50,
                    justifyContent: 'center',
                    alignItems: 'flex-start',
                }}
            >
                {item.status === 0
                    ? null
                    : <TouchableOpacity
                        onPress={() => {
                            this.isCheck = !this.isCheck;
                            this.props.checkItem(item);
                        }}
                        activeOpacity={1}
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'flex-start',
                            backgroundColor: 'transparent',
                        }}
                    >
                        <Ionicons
                            name={
                                this.isCheck
                                    ? 'ios-checkmark-circle'
                                    : 'ios-checkmark-circle-outline'
                            }
                            size={40}
                            color={this.isCheck ? '#21469B' : '#D1D1D6'}
                        />
                    </TouchableOpacity>}

            </View>
        );
    }
    checkStatus(item) {
        if (item.status === 4) {
            return { title: 'Từ chối', color: '#FF3B30' };
        } else if (item.status === 1 || item.status === 2) {
            return { title: 'Chờ duyệt / ', color: '#FF9500' };
        } else if (item.status === 3) {
            return { title: 'Đã duyệt', color: '#4CD964' };
        }

        // else if (item.status === 2) {
        //     return { title: 'Chưa chấm công', color: '#8E8E93' };
        // }
        else if (item.status === 0) {
            return { title: 'Chưa chấm công', color: '#8E8E93' };
        }
    }

    componentWillReceiveProps(nextProps) {
        const { listReturn, item } = this.props;
        if (nextProps && nextProps.listReturn && nextProps.listReturn.length > 0) {
            nextProps.listReturn.map(e => {
                if (e.id === item.id) {
                    if (e.status !== item.status) {
                        item.status = e.status;
                    }
                    if (e.approverNote != item.approverNote) {
                        item.approverNote = e.approverNote
                    }
                }
            });
        }
    }

    renderItem(employeeName, employeeUserId, checkTime, color, time, status, title) {
        return (
            <View style={{ 
                flexDirection: 'row',
                borderLeftWidth: 1,
                borderLeftColor: '#C8C7CC',
                }}>
                <View
                    style={{
                        marginLeft: 20,
                        flex: 1,
                    }}
                >
                    <Text style={{ fontSize: 16, color: settingApp.colorText, maxWidth: settingApp.width * 0.7 }}>
                        {employeeName} - {employeeUserId}
                    </Text>

                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ fontSize: 14, color: color }}>
                            {/* {status ? status.title : null} */}
                            {title ? title : null}
                        </Text>
                        <Text style={{ fontSize: 14, color: checkTime ? '#FF3B30' : '#21469B' }}>
                            {status == 2 || status == 1 ? '[' + time + ']' : null}
                        </Text>
                    </View>
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <MaterialIcons
                        name="keyboard-arrow-right"
                        size={23}
                        color="#D1D1D6"
                    />
                </View>
            </View>
        )
    }

    render() {
        const { item, index } = this.props;
        const time = Utils.formatTime(item.checkin, 'LT');
        let status = this.checkStatus(item);
        const checkTime = item.actualWorkingHours < item.shiftWorkingHours;
        if (item.statusRequest == 0) {
            return (
                <View
                    key={index}
                    style={{
                        justifyContent: 'center',
                        borderTopWidth: index === 0 ? 0 : 1,
                        borderTopColor: '#C8C7CC',
                        marginLeft: 15,
                        paddingLeft: 10,
                        minHeight:60
                    }}
                >
                    <TouchableOpacity
                        onPress={() => {
                            item.status === 0 ? null : this.props.goDetail(item, status, checkTime, item.statusRequest);
                        }}
                        activeOpacity={1}
                        style={{
                            flex: 1,
                            paddingRight: 15,
                            backgroundColor: '#ffffff',
                            paddingBottom: 10,
                            paddingTop: 10,
                            paddingLeft: 15,
                            marginLeft: 15,
                        }}
                    >
                        {this.renderItem(
                            item.employeeName,
                            item.employeeUserId,
                            checkTime,
                            status.color,
                            time,
                            item.status,
                            status.title
                        )}
                    </TouchableOpacity>
                    {this.renderCheck(item)}
                </View>
            );
        }
        else {
            const time = Utils.formatTime(item.requestApproveDate, 'LT');
            const checkTimeRequest = true;
            const title = 'Yêu cầu tạo công'
            return (
                <View
                    key={index}
                    style={{
                        justifyContent: 'center',
                        borderTopWidth: index === 0 ? 0 : 1,
                        borderTopColor: '#C8C7CC',
                        marginLeft: 15,
                        paddingLeft: 10,
                        minHeight:60
                    }}
                >
                    <TouchableOpacity
                        onPress={() => {
                            this.props.goDetail(item, item.statusRequest);
                        }}
                        activeOpacity={0.5}
                        style={{
                            flex: 1,
                            paddingRight: 15,
                            backgroundColor: '#ffffff',
                            paddingBottom: 10,
                            paddingTop: 10,
                            paddingLeft: 15,
                            marginLeft: 15,
                        }}
                    >
                        {this.renderItem(
                            item.requester.fullName,
                            item.requester.userId,
                            checkTimeRequest,
                            color ='#FF9500',
                            time,
                            status = 5,
                            title
                        )}
                    </TouchableOpacity>
                </View>
            );
        }
    }
}
export default Item;
