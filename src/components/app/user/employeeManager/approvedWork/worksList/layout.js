import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, FlatList, RefreshControl, ActivityIndicator } from 'react-native';
import { HeaderWapper, index, Utils, Loading, settingApp, NotFound } from '../../../../../../public';
import { MaterialIcons, Ionicons, Feather } from '@expo/vector-icons';
import Item from './component/item';
import ModalButton from './component/modal';

class Layout extends Component {
    settupWeed(item) {
        const date = new Date(item.fullDate)
        const weekday = new Array(7)
        weekday[0] = "Chủ nhật";
        weekday[1] = "Thứ 2";
        weekday[2] = "Thứ 3";
        weekday[3] = "Thứ 4";
        weekday[4] = "Thứ 5";
        weekday[5] = "Thứ 6";
        weekday[6] = "Thứ 7";
        return weekday[date.getDay()]
    }

    renderHeader(title) {
        const day = this.settupWeed(title)
        return (
            <HeaderWapper
                style={{ flexDirection: 'row', justifyContent: 'space-between' }}
            >
                <TouchableOpacity
                    onPress={() => this.props.navigation.pop()}
                    style={{ width: 55, height: 43, justifyContent: 'center', alignItems: 'center' }}
                >
                    <Ionicons name='ios-arrow-back' size={23} color='#FFFFFF' />
                </TouchableOpacity>

                <Text style={{ alignItems: 'center', marginTop: 10, fontSize: 16, fontWeight: 'bold', color: '#FFFFFF' }}>
                    {day}, {title.checkin}
                </Text>

                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('CreateNewWorks', {
                        actions: isLoad => this.setState({ isLoad: isLoad }),
                        backContent:(stt => this.backContent(stt))
                    })}
                    style={{ width: 55, height: 43, justifyContent: 'center', alignItems: 'center' }}
                >
                    <Feather name='plus' size={23} color="#ffffff" />
                </TouchableOpacity>
            </HeaderWapper>
        )
    }

    renderContent(item, listCheck, list) {
        const status = listCheck.length > 0;
        return (
            <View style={{ flex: 1 }} >
                <View style={{ flex: 1, backgroundColor: '#FFFFFF', marginBottom: status ? 120 : 0 }}>
                    <FlatList
                        data={list}
                        extraData={this.state || this.props}
                        keyExtractor={(item, index) => '' + index}
                        renderItem={({ item, index }) =>
                            <Item
                                item={item}
                                index={index}
                                listCheck={this.state.listCheck}
                                checkItem={data => this.checkItem(data)}
                                goDetail={(data, status, checkTime, statusRequest) => this.goDetail(data, status, checkTime, statusRequest)}
                                listReturn={this.state.listReturn}
                            />
                        }
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={() => this.onRefresh()}
                                tintColor={settingApp.color}
                                colors={[settingApp.color]}
                            />
                        }
                    />

                </View>
                <ModalButton
                    modalVisible={this.state.modalVisible}
                    close={() => this.setState({ modalVisible: false })}
                    statusApr={this.state.statusApr}
                    timeKeeping={(status, type) => this.timeKeeping(status, type)}
                    listType={this.state.listType}
                />
            </View>
        )
    }

    renderButton(listCheck) {
        return (
            listCheck.length > 0 ?
                <View style={{
                    width: '100%',
                    height: 120,
                    backgroundColor: '#FFFFFF',
                    alignItems: 'center',
                    flexDirection: 'column',
                    borderTopColor: '#C8C7CC',
                    borderTopWidth: 1,
                    position: 'absolute',
                    bottom: 0
                }}>
                    <Text style={{ fontSize: 16, color: settingApp.colorText, marginTop: 10 }}> Đã chọn {listCheck.length} phiếu chấm công</Text>

                    <View style={{
                        flexDirection: 'row', width: '100%',
                        height: 80, justifyContent: 'space-between'
                    }}>
                        <TouchableOpacity
                            onPress={() => {
                                this.setState({ modalVisible: true, statusApr: false })
                            }}
                            style={{ backgroundColor: '#FF3B30', borderRadius: 5, height: 60, width: '45%', marginLeft: 10, marginTop: 10, alignItems: 'center', justifyContent: 'center' }}
                        >
                            <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#FFFFFF' }}>Từ chối</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => {
                                this.setState({ modalVisible: true, statusApr: true })
                            }}
                            style={{ backgroundColor: '#4CD964', borderRadius: 5, height: 60, width: '45%', marginRight: 10, marginTop: 10, alignItems: 'center', justifyContent: 'center' }}
                        >
                            <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#FFFFFF' }}>Duyệt</Text>
                        </TouchableOpacity>
                    </View>

                </View>
                :
                <View />
        )
    }


    render() {
        const { isLoading, listCheck, list, reload } = this.state;
        const { item } = this;
        if (isLoading) {
            return (
                <View style={{ flex: 1}}>
                 {this.renderHeader(item)}
                    <Loading />
                </View>
            )
        }
        else if (reload) {
            return (
                <View style={{ flex: 1}}>
                 {this.renderHeader(item)}
                  <Loading />
                </View>
            )
        }
        else {
            if (list && list.length > 0) {
                return (
                    <View style={{ flex: 1 }}>
                        {this.renderHeader(item)}
                        <Text style={{ color: '#8E8E93', fontSize: 16, padding: 10 }}> {list[0].shift.name} [ {list[0].shift.timeframes[0].checkInTime} - {list[0].shift.timeframes[0].checkOutTime} ]</Text>
                        {this.renderContent(item, listCheck, list)}
                        {this.renderButton(listCheck)}
                    </View>
                )
            }
            else {
                return (
                    <View style={{ flex: 1 }}>
                        {this.renderHeader(item)}
                        <NotFound />
                    </View>
                )
            }
        }
    }
}
export default Layout;