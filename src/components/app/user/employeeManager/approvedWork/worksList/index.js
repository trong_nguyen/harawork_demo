import Layout from './layout';
import actions from '../../../../../../state/action';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { HaravanHr } from '../../../../../../Haravan'
import {Toast, Utils } from '../../../../../../public'

class ApprovedWorkList extends Layout{
    constructor(props){
        super(props);
        this.state={
            isLoading: true,
            listCheck : [],
            modalVisible: false,
            statusApr: null,
            list: [],
            listReturn: [],
            waiting: false,
            listType:[],
            isLoad: false,
            refreshing: false,
            reload: false
        }

        this.isCheck = false;
        this.item = props.navigation.state.params.item;
        this.data = props.navigation.state.params.data;
        this.colleague = props.app.colleague;
        this.name = props.navigation.state.params.name;
        this.status = props.aprov.checkAprov;
        this.refreshList = props.aprov.refreshList;
    }

    componentDidMount(){
       this.loadData()
    }

    onRefresh(){
        this.props.navigation.state.params.actions(true)
        this.props.navigation.state.params.setRefresh(true)
        this.setState({isLoading: true ,refreshing: true, list:[]}, () =>{
            this.loadData()
        })
    }
    
    componentWillReceiveProps(nextProps){
        if(nextProps && JSON.stringify(nextProps.aprov.checkAprov) != JSON.stringify(this.props.aprov.checkAprov)){
            const {id, status, note} = nextProps.aprov.checkAprov
            const {listReturn, list} = this.state
            list.map(e => {
                if(id === e.id){
                    e = {...e, status: status, approverNote: note}
                        listReturn.push(e)
                        this.setState({listReturn: listReturn})
                }
                else if(nextProps.aprov.checkAprov.status === false){
                    Toast.show(nextProps.aprov.checkAprov.message)
                }
            })
        }
        // if( nextProps.aprov.refreshList && nextProps.aprov.refreshList != this.props.aprov.refreshList){
        //     this.props.navigation.pop()
        // }
        if(nextProps && nextProps.aprov.reload.status != this.props.aprov.reload.status){
            this.data = nextProps.aprov.reload.list
            this.setState({ reload: nextProps.aprov.reload.status }, () =>{
                this.loadData()
            })
            
        }
    }

    loadData(){
        const {list,reload } = this.state
        if(!reload){
            this.data.map(e=>{
                if(e.shift && e.shift.date === this.item.checkin && e.shift.name === this.name){
                    e={...e, statusRequest:e.shift.statusRequest}
                    list.push(e)
                }
                else{
                    if( ((Utils.formatTime(e.requestApproveDate, 'DD/MM/YYYY', true)) === this.item.checkin)  && (e.requestShiftName && e.requestShiftName === this.name)){
                        const obj = JSON.parse(e.requestData)
                        e={...e, data: obj}                       
                        list.push(e)
                    }
                }
                this.setState({ list})
            })
            this.props.navigation.state.params.actions(false)
            this.setState({refreshing: false, reload: false, isLoading:false})
        }
        else{
            this.setState({refreshing: false, reload: false})
        }
    }

    checkItem(item){
        const { listCheck, listType } = this.state;
        if (listCheck.length === 0) {
            listCheck.push(item);
            listType.push(item.shift.approveType)
        }
        else {
            const indexList = listCheck.findIndex(e => e.id === item.id)
            if (indexList > -1) {
                listCheck.splice(indexList, 1)
                listType.splice(indexList, 1)
            }
            else {
                listCheck.push(item)
                listType.push(item.shift.approveType)
            }
        }
        this.setState({listCheck: listCheck, listType:listType})
    }

    goDetail(item, status, checkTime, statusRequest){
        this.props.navigation.navigate('ApprovedDetail',{
            item: item, 
            status:status ? status : null,
            checkTime:checkTime ? checkTime : null,
            statusRequest: statusRequest,
            actions:(data => this.delete(data)),
            backContent:(stt => this.backContent(stt))
        },() =>{
            this.setState({listReturn: [], listType:[]})
            this.props.actions.aprov.checkAprov(null)
        })
    }

    backContent(stt){
        if(stt == true){
            this.props.navigation.pop()
        }
    }

    delete(data){
        const { list } = this.state;
        const index = list.findIndex(e => e.id === data.id)
        if(index > -1){
            list.splice(index, 1)
        }
        this.setState({ list },async () =>{
            this.props.navigation.state.params.delete(data)
            const body={
                ApproverNote: "TỪ CHỐI PHIẾU YÊU CẦU",
                Id: data.id,
                Status:3
            }
            const result = await HaravanHr.putEmployeeRequest(body)
        })
    }
    
    timeKeeping(status, type){
        const { colleague, item } = this;
        const { listCheck, list, listReturn } = this.state;
        const newList = listCheck;
        let link = ''
        this.setState({listCheck:[], waiting: true, listType:[]})
        let body ={}
        if(newList.length >= 1){
            body={
                Status: status,
                isQuickApprove:true
            }
        }
        else{
            body = {
                Status: status
            }
        }
        newList.map(e =>{
            this.setState({},async()=>{
                list.map(m =>{
                    if(e.id === m.id){
                        m = {...m, status: status}
                        listReturn.push(m)
                    }
                })
                if(type && type != null){
                    link= `supervisor/checkin/approve?timekeepingId=${e.id}&typeTimeApprove=${type}`
                }
                else{
                    link = `supervisor/checkin/approve?timekeepingId=${e.id}`
                }
                const result = await HaravanHr.postApprovedWorks(link, body, colleague)
                if(result && result.data && !result.error){
                    this.setState({listReturn: listReturn})
                }
            })
        })
    }

    componentWillUnmount(){
        this.data =[]
    }
}
mapStateToProps = state => ({ ...state });
mapDispatchToProps = dispatch => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch)
    }
    return { actions: acts }
}
export default connect(mapStateToProps, mapDispatchToProps)(ApprovedWorkList);