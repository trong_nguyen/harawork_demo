import Layout from './layout'
import { HaravanHr } from '../../../../../../Haravan';
import { Utils } from '../../../../../../public';

class Condition extends Layout {
    constructor(props) {
        super(props);
        const date = new Date();
        const formDate = new Date(date.getFullYear(), date.getMonth(), 1);
        const todate = new Date();

        this.state = {
            isLoadDepart: true,
            valueFromDate: formDate,
            valueTodate: todate,
            isVisibleToDate: false,
            isVisibleFromDate: false,

            visibleDepart: false,
            visibleJod: false,
            visibleEmploy: false,
        }
        this.leaveTypes = []

        this.formDate = formDate
        this.todate = todate
        this.labels = ''
        this.link = null

        this.Department = []
        this.Jod = []
        this.Employ =[]
        this.departName = ''
        this.idDepart = ''

        this.jobliName = ''
        this.idJod = ''
    }

    async componentDidMount() {
        const resDepasrt = await HaravanHr.getMeListDepartments();
        const resJod = await HaravanHr.getJob();
        const resEmploy = await HaravanHr.getEmploy()
        if (resDepasrt || resJod || resEmploy) {
            this.Department = resDepasrt.data
            this.Jod = resJod.data.data
            this.Employ = resEmploy.data.data
        }
        this.setState({ isLoadDepart: false })
    }

    formatTime(value) {
        return Utils.formatTime(value, 'DD/MM/YYYYY');
    }

    setRemove(){
        const { valueTodate, valueFromDate } = this.state;
        const { departName, jobliName } = this;
        if (
            (this.formatTime(valueFromDate) !== this.formatTime(this.formDate) ||
                this.formatTime(valueTodate) !== this.formatTime(this.todate)) ||
            this.formatTime(valueFromDate) === this.formatTime(this.formDate) ||
            this.formatTime(valueTodate) === this.formatTime(this.todate) ||
            (departName !== '') || (jobliName !== '')) {
            this.props.setRemoveFilter(true)
        }
        else {
            this.props.setRemoveFilter(false)
        }
    }

    validate(type, value) {
        if (type === 'fromDate') {
            if (!!(value <= this.state.valueTodate)) {
                this.setState({ valueFromDate: value })
            } else {
                this.setState({ valueFromDate: value, valueTodate: value })
            }
        } else {
            if (!!(value >= this.state.valueFromDate)) {
                this.setState({ valueTodate: value })
            } else {
                this.setState({ valueTodate: value, valueFromDate: value });
            }
        }
        this.setRemove()
    }


    refreshFillter() {
        this.setState({
            valueFromDate: this.formDate,
            valueTodate: this.todate,
        })
        this.departName = ''
        this.id = ''
        this.jobliName = ''
        this.idJod = ''
        this.employName = ''
        this.idEmploy = ''

        this.props.setCondition(null)
        this.props.setRemoveFilter(false)
    }

    checkStatus(data) {
        this.labels = data
    }

    department(data) {
        this.departName = data.name
        this.idDepart = data.id

        this.setRemove()
    }

    jobli(data) {
        this.jobliName = data.name
        this.idJod = data.id

        this.setRemove()
    }

    employ(data){
        this.employName = data.fullName;
        this.idEmploy = data.employeeId

        
        this.setRemove()
    }

    formatTime(value) {
        return Utils.formatTime(value, 'DD/MM/YYYY');
    }


    submit() {
        let { valueTodate, valueFromDate } = this.state;
        let { idDepart, idJod, idEmploy } = this;
        valueTodate = valueTodate ? Utils.formatTime(valueTodate, 'YYYY-MM-DD', true) : null;
        valueFromDate = valueFromDate ? Utils.formatTime(valueFromDate, 'YYYY-MM-DD', true) : null;
        const labels = this.labels ? this.labels.id : 0;
        const idDepartment = idDepart;
        const idJodli = idJod;
        const employID = idEmploy;
        let condition = {
            valueTodate,
            valueFromDate,
            idDepartment,
            idJodli, 
            employID
        }
        this.props.setCondition(condition)
    }
}
export default Condition;