import React, { Component } from 'react';
import { ActivityIndicator, View, Text, TouchableOpacity, Image, ScrollView } from 'react-native';
import { settingApp, imgApp, Utils, Error, Loading } from '../../../../../../public';
import { MaterialIcons } from '@expo/vector-icons';
import Fillter from '../component/fillter';
import DatePicker from './componet/datePicker';
import Modal from './componet/lables';
import ModalJod from './componet/modalJod';
import ModalEmploy from './componet/modalEmploy';


class Layout extends Component{

    loadDing() {
        return (
            <View style={{ paddingRight: 20 }}>
                <ActivityIndicator size='small' color={settingApp.color} />
            </View>
        )
    }

    render(){
        let labels = this.labels;
        const {departName, jobliName, employName } = this;
        const { isLoadDepart } = this.state;
        return (
            <ScrollView>
                <View style={{ flex: 1, backgroundColor: '#ffffff', paddingLeft: 15, ...settingApp.shadow, marginBottom: 20 }}>
                    <Fillter
                        name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Từ ngày</Text>}
                        value={(
                            <TouchableOpacity
                                onPress={() => this.setState({isVisibleFromDate: true})}
                                style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                                <Text style={{ fontSize: 15, color: '#8E8E93', marginRight: 10 }}>{Utils.formatTime(this.state.valueFromDate, 'DD/MM/YYYY', true)}</Text>
                                <Image
                                    source={imgApp.calendar}
                                    style={{ width: 20, height: 20, tintColor: '#D1D1D6' }}
                                    resizeMode='stretch'
                                />
                            </TouchableOpacity>
                        )}
                        noneBorder={true}
                    />
                    <Fillter
                        name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Đến ngày</Text>}
                        value={(
                            <TouchableOpacity
                                onPress={() => this.setState({isVisibleToDate: true})}
                                style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                                <Text style={{ fontSize: 15, color: '#8E8E93', marginRight: 10 }}>{Utils.formatTime(this.state.valueTodate, 'DD/MM/YYYY', true)}</Text>
                                <Image
                                    source={imgApp.calendar}
                                    style={{ width: 20, height: 20, tintColor: '#D1D1D6' }}
                                    resizeMode='stretch'
                                />
                            </TouchableOpacity>
                        )}
                    />
                    <Fillter
                        name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Bộ phận</Text>}
                        value={(
                            <TouchableOpacity
                                onPress = {() => {
                                    isLoadDepart ? null :
                                    this.setState({ visibleDepart: true}) 
                                    }}
                                style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                                {
                                    isLoadDepart ? this.loadDing() :
                                    <View style={{flex:1, flexDirection:'row'}}>
                                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 10 }}>
                                            <Text style={{ fontSize: 15, color: '#8E8E93' }} numberOfLines={1}>{departName ? departName : (this.Department ? 'Tất cả' : 'Không lấy được dữ liệu')}</Text>
                                        </View>
                                        <MaterialIcons name='keyboard-arrow-right' size={23} color='#D1D1D6' />
                                    </View>
                                }
                                </TouchableOpacity>
                        ) 
                        }
                    />

                    <Fillter
                        name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Chức danh</Text>}
                        value={(
                            <TouchableOpacity
                                onPress = {() => this.setState({ visibleJod: true })}
                                style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 10 }}>
                                    <Text style={{ fontSize: 15, color: '#8E8E93' }} numberOfLines={1}>{jobliName ? jobliName : 'Tất cả'}</Text>
                                </View>
                                <MaterialIcons name='keyboard-arrow-right' size={23} color='#D1D1D6' />
                            </TouchableOpacity>
                        ) 
                        }
                    />

                    <Fillter
                        name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Nhân viên</Text>}
                        value={(
                            <TouchableOpacity
                                onPress = {() => this.setState({ visibleEmploy: true })}
                                style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 10 }}>
                                    <Text style={{ fontSize: 15, color: '#8E8E93' }} numberOfLines={1}>{employName ? employName :'Tất cả'}</Text>
                                </View>
                                <MaterialIcons name='keyboard-arrow-right' size={23} color='#D1D1D6' />
                            </TouchableOpacity>
                        ) 
                        }
                    />
                    
                    <View style={{
                        flex: 1,
                        borderTopColor: settingApp.colorSperator,
                        borderTopWidth: 1,
                        paddingTop: 20,
                        paddingRight: 15,
                        marginBottom: 10
                    }}>
                        <TouchableOpacity
                            onPress= {() => 
                            this.props.removeFilter ?
                            this.submit() : null}
                            style={{
                                flex: 1,
                                height: 55,
                                backgroundColor:this.props.removeFilter ?  settingApp.color : '#8E8E93',
                                justifyContent: 'center',
                                alignItems: 'center',

                            }}>
                            <Text style={{ color: '#ffffff', fontSize: 15, fontWeight: 'bold' }}>Áp dụng</Text>
                        </TouchableOpacity>
                    </View>
                        
                    <DatePicker 
                        value={this.state.valueTodate}
                        validate={value => this.validate('toDate', value)}
                        visible={this.state.isVisibleToDate}
                        close={() => this.setState({ isVisibleToDate: false })}
                    />

                    <DatePicker 
                        value={this.state.valueFromDate}
                        validate={value => this.validate('fromDate', value)}
                        visible = { this.state.isVisibleFromDate }
                        close = {()=> this.setState({isVisibleFromDate: false})}
                    />

                    <Modal 
                        visible = { this.state.visibleDepart }
                        close ={()=> this.setState({ visibleDepart: false})}
                        depart ={ this.Department }
                        pushId = { data => this.department(data)}
                    />
                    
                    <ModalJod 
                        visible = {this.state.visibleJod}
                        close ={()=> this.setState({ visibleJod: false})}
                        jod ={ this.Jod }
                        pushId = {data => this.jobli(data)}
                    />

                    <ModalEmploy
                        visible = {this.state.visibleEmploy}
                        close ={()=> this.setState({ visibleEmploy: false})}
                        employ = {this.Employ}
                        pushId = {data => this.employ(data)}
                    />

                </View>
            </ScrollView>
        )
    }
}
 export default Layout;