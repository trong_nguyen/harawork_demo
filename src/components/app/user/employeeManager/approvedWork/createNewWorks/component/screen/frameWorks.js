import React, { Component } from 'react';
import { View, Text, Modal, TouchableOpacity, ScrollView } from 'react-native';
import { settingApp, Utils, HeaderWapper } from '../../../../../../../../public';
import { Ionicons } from '@expo/vector-icons';

class FrameWorks extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
        this.data = props.navigation.state.params.data
        this.check = props.navigation.state.params.check
        this.typeShift= props.navigation.state.params.typeShift
    }

    renderHeader() {
        return(
        <HeaderWapper
            style={{
                justifyContent: 'space-between',
                flexDirection: 'row',
            }}
        >
            <TouchableOpacity
                onPress={() => this.props.navigation.pop()}
                style={{ width: 55, height: 43, justifyContent: 'center', alignItems: 'center' }}
            >
                <Ionicons name='ios-arrow-back' size={23} color='#ffffff' />
            </TouchableOpacity>

            <Text style={{ alignItems: 'center', marginTop: 10, fontSize: 16, fontWeight: 'bold', color: '#FFFFFF' }}>
                Bộ phận
        </Text>
            <View style={{ width: 55, height: 43, justifyContent: 'center', alignItems: 'center' }} />
        </HeaderWapper>
        )
    }
    
    checkTypeShift(typeShift){
            const data = new Array(7)
            data[1] = [{name: 'Tùy chọn', type: 0, status:1}];
            data[2] = [{name: 'Duyệt nguyên ca', type: 1, status:0 }, {name: 'Tùy chọn', type: 0, status:1}];
            data[3] = [{name: 'Duyệt nguyên ca', type: 1, status:0}, {name: 'Duyệt nửa ca', type: 4, status:0}, {name: 'Tùy chọn', type: 0, status:1}];
            data[4] = [{name: 'Duyệt nguyên ca', type: 1, status:0}, {name: 'Duyệt nửa ca đầu', type: 2, status:0}, {name: 'Duyệt nửa ca sau', type: 3, status:0}, {name: 'Tùy chọn', type: 0, status:1}];
            data[5] = [{name: 'Duyệt nguyên ngày', type: 1, status:0}, {name: 'Tùy chọn', type: 0, status:1}];
            data[6] = [{name: 'Duyệt nguyên ngày', type: 1, status:0}, {name: 'Duyệt nửa ngày', type: 2, status:0}];
            data[7] = [{name: 'Duyệt nguyên ngày', type: 1, status:0}, {name: 'Duyệt buổi sáng', type: 2, status:0}, {name: 'Duyệt buổi chiều', type: 3, status:0}, {name: 'Tùy chọn', type: 0, status:1}];
            return data[typeShift]
    }

    render() {
        const { data, check, typeShift } = this;
        const checkType = this.checkTypeShift(typeShift)
        return (
            <View style={{ flex: 1, backgroundColor: '#C8C7CC' }}>
                {this.renderHeader()}
                <ScrollView style={{ width: '100%', backgroundColor: '#FFFFFF', flexDirection: 'column' }}>
                    {checkType.map((e, i) => {
                        return (
                            <View
                                key={i}
                                style={{ borderBottomColor: '#C8C7CC', borderBottomWidth: 1, marginLeft: 15 }}>
                                
                                <TouchableOpacity
                                    activeOpacity={1}
                                    onPress={() => {
                                        this.props.navigation.state.params.actions(e);
                                        this.props.navigation.pop()
                                    }}
                                    style={{ marginLeft: 10, paddingBottom: 15, paddingTop: 15, flexDirection:'row' }}
                                >{
                                    check && check === e.name ?
                                    <Ionicons name='md-checkmark' color='#21469B' size ={20}/> 
                                    : null
                                }
                                    <Text style={{ fontSize: 16, color: settingApp.colorText, marginLeft:15 }}> {e.name} </Text>
                                </TouchableOpacity>
                            </View>
                        )
                    })}
                </ScrollView>
            </View>
        )
    }
}
export default FrameWorks;