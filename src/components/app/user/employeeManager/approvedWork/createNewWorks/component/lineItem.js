import React, { Component } from 'react';
import { Text, View, TouchableOpacity, ActivityIndicator, Image } from 'react-native';
import { Ionicons, EvilIcons, MaterialIcons } from '@expo/vector-icons';
import { settingApp, Utils, HeaderWapper, imgApp, Toast, Loading } from '../../../../../../../public';

class LineItem extends Component {

    row() {
        return (
            <MaterialIcons
                name="keyboard-arrow-right"
                size={23}
                color="#D1D1D6"
            />
        )
    }

    loadDing() {
        return (
            <View style={{ paddingRight: 20 }}>
                <ActivityIndicator size='small' color={settingApp.color} />
            </View>
        )
    }

    error() {
        return (
            <View style={style.viewRight}>
                <Text style={style.textRightItem}>Không lấy được dữ liệu</Text>
            </View>
        )
    }

    render() {
        let { title, isLoad, valueName, source, hideRow } = this.props;
        hideRow = hideRow ? hideRow : false;
        return (
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', flex: 1 }}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ color: settingApp.colorText, fontSize: 16 }}>
                        {title}
                    </Text>
                    <Text style={{ color: '#FF3B30', top: 0 }}> * </Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    {isLoad
                        ? this.loadDing()
                        : valueName
                            ? <View style={style.viewRight}>
                                <Text 
                                numberOfLines={1}
                                style={style.textRightItem}>
                                    {valueName}
                                </Text>
                                {
                                    source ? <Image
                                        source={imgApp.calendar}
                                        style={{ width: 20, height: 20, tintColor: '#D1D1D6' }}
                                        resizeMode='stretch'
                                    /> : null
                                }

                                {hideRow ? null : this.row()}
                            </View>
                            : this.error()
                    }
                </View>
            </View>
        );
    }
}
const style = {
    coverItem: {
        flexDirection: 'row',
        marginLeft: 15,
        justifyContent: 'space-between',
        paddingBottom: 10,
        paddingTop: 10,
        borderBottomColor: '#C8C7CC',
        borderBottomWidth: 0.5,
    },
    textRightItem: {
        color: '#8E8E93',
        paddingRight: 5,
        fontSize: 16,
        maxWidth: 150,
    },
    viewRight: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingRight: 15,
        flexDirection: 'row'
    },
}
export default LineItem;
