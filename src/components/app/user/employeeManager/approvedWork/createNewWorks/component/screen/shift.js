import React, { Component } from 'react';
import { View, Text, Modal, TouchableOpacity, ScrollView } from 'react-native';
import { settingApp, Utils, HeaderWapper } from '../../../../../../../../public';
import { Ionicons } from '@expo/vector-icons';

class Shifts extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
        this.data = props.navigation.state.params.data
        this.frames = props.navigation.state.params.frames
    }

    renderHeader() {
        return (
            <HeaderWapper
                style={{
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                }}
            >
                <TouchableOpacity
                    onPress={() => this.props.navigation.pop()}
                    style={{ width: 55, height: 43, justifyContent: 'center', alignItems: 'center' }}
                >
                    <Ionicons name='ios-arrow-back' size={23} color='#ffffff' />
                </TouchableOpacity>

                <Text style={{ alignItems: 'center', marginTop: 10, fontSize: 16, fontWeight: 'bold', color: '#FFFFFF' }}>
                    Ca làm việc
        </Text>
                <View style={{ width: 55, height: 43, justifyContent: 'center', alignItems: 'center' }} />
            </HeaderWapper>
        )
    }

    render() {
        const { data, frames } = this;
        return (
            <View style={{ flex: 1, backgroundColor: '#C8C7CC' }}>
                {this.renderHeader()}
                <ScrollView style={{ width: '100%', backgroundColor: '#FFFFFF', flexDirection: 'column' }}>
                    {data.map((e, i) => {
                        return (
                            <View
                                key={i}
                                style={{ borderBottomColor: '#C8C7CC', borderBottomWidth: 1, marginLeft: 15 }}>
                                <TouchableOpacity
                                    activeOpacity={1}
                                    onPress={() => {
                                        this.props.navigation.state.params.actions(e);
                                        this.props.navigation.pop()
                                    }}
                                    style={{ marginLeft: 10, paddingBottom: 15, paddingTop: 15, flexDirection:'row' }}
                                >{
                                    frames && frames === e.name ?
                                    <Ionicons name='md-checkmark' color='#21469B' size ={20}/> 
                                    : null
                                }
                                    <Text style={{ fontSize: 16, color: settingApp.colorText, marginLeft:15 }}> {e.name} </Text>
                                </TouchableOpacity>
                            </View>
                        )
                    })}
                </ScrollView>
            </View>
        )
    }
}
export default Shifts;