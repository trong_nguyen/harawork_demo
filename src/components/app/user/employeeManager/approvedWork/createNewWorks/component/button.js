import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { settingApp, } from '../../../../../../../public';

class Button extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { totalHours, valueHoursIn, valueHoursOut, data } = this.props;
        return (
            <View
                style={{
                    flex: 1,
                    flexDirection: 'row',
                    width: settingApp.width,
                    justifyContent: 'space-between',
                    padding: 15,
                }}
            >
                <TouchableOpacity
                
                    onPress={() => {
                        (valueHoursIn > valueHoursOut) ? this.totalTime(false) :
                            (this.props.create(1, 3), this.props.isLoad(true))
                    }}
                    style={{
                        width: data ? settingApp.width * 0.9 : settingApp.width * 0.45,
                        height: 55,
                        backgroundColor: '#4CD964',
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderRadius: 3,
                    }}
                >
                    <Text style={{ fontSize: 18, color: '#ffffff', fontWeight: 'bold' }}>
                        Tạo và duyệt
                    </Text>
                </TouchableOpacity>

                {
                    data ? null :
                        <TouchableOpacity
                            onPress={() => {
                                (valueHoursIn > valueHoursOut) ? this.totalTime(false) :
                                    (this.props.create(2, 1), this.props.isLoad(true))
                            }}
                            style={{
                                width: settingApp.width * 0.45,
                                height: 55,
                                backgroundColor: settingApp.color,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 3,
                            }}
                        >
                            <Text style={{ fontSize: 18, color: '#ffffff', fontWeight: 'bold' }}>
                                Tạo
          </Text>
                        </TouchableOpacity>}
            </View>
        );
    }
}

export default Button;