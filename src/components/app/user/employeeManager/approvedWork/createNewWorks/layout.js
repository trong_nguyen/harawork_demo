import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    Modal,
    TextInput,
    ActivityIndicator, Image, KeyboardAvoidingView
} from 'react-native';
import { settingApp, Utils, HeaderWapper, imgApp, Toast, Loading } from '../../../../../../public';
import { Ionicons, EvilIcons, MaterialIcons } from '@expo/vector-icons';
import DatePicker from './component/datePicker';
import TimePicker from '../timePicker';
import LineItem from './component/lineItem';
import Button from './component/button';

class Layout extends Component {

    renderHeader() {
        return (
            <HeaderWapper
                style={{ justifyContent: 'space-between', alignItems: 'center' }}
            >
                <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                    <View
                        style={{ width: 50, justifyContent: 'center', alignItems: 'center' }}
                    >
                        <Text style={{ color: '#FFFFFF', fontSize: 16 }}> Hủy </Text>
                    </View>
                </TouchableOpacity>

                <Text style={{ color: '#FFFFFF', fontWeight: 'bold', fontSize: 18 }}>
                    Tạo công
                </Text>

                <View style={{ width: 50, height: 50 }} />
            </HeaderWapper>
        );
    }
    renderButton() {
        const { totalHours, valueHoursIn, valueHoursOut } = this.state;
        return (
            <Button
                totalHours={totalHours}
                valueHoursIn={valueHoursIn}
                valueHoursOut={valueHoursOut}
                create={(status, aprStatus) => this.create(status, aprStatus)}
                isLoad={isLoad => this.setState({ isLoad: isLoad })}
                data={this.dataRequest ? this.dataRequest : null}
            />
        );
    }

    row() {
        return (
            <MaterialIcons
                name="keyboard-arrow-right"
                size={23}
                color="#D1D1D6"
            />
        )
    }

    loadDing() {
        return (
            <View style={{ paddingRight: 20 }}>
                <ActivityIndicator size='small' color={settingApp.color} />
            </View>
        )
    }

    error() {
        return (
            <View style={style.viewRight}>
                <Text style={style.textRightItem}>Không lấy được dữ liệu</Text>
            </View>
        )
    }

    render() {
        const { listDepart, listEploy, listShift, frames, aprOut, name, departName, aprIn } = this;
        const { isLoad, typeShift, customTime, isLoadDepart, isLoadShift, isLoadEmploy, noteApprod, valueHoursOut, valueHoursIn, valueMinOut, valueMinIn, totalHours, totalMin, data, timeframes } = this.state;
        return (
            <KeyboardAvoidingView behavior='padding' style={{ flex: 1 }}>
                {this.renderHeader()}
                <ScrollView style={{ backgroundColor: '#E5E5E5' }}>
                    <View
                        style={{
                            flexDirection: 'column',
                            marginTop: 15,
                            flex: 1,
                            backgroundColor: '#ffffff',
                            ...settingApp.shadow,
                        }}
                    >
                        {isLoad == true && <Loading style={{ backgroundColor: 'transparent' }} />}
                        <TouchableOpacity
                            activeOpacity={this.dataRequest ? 1 : 0.5}
                            onPress={() =>
                                this.dataRequest ? null :
                                    this.props.navigation.navigate('DepartDetail', {
                                        data: listDepart,
                                        departName: departName,
                                        actions: data => this.getDepartUp(data)
                                    })}
                            style={style.coverItem}
                        >
                            <LineItem
                                title='Đơn vị'
                                valueName={departName}
                                isLoad={isLoadDepart}
                                hideRow={ this.dataRequest ? true : false}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity
                            activeOpacity={this.dataRequest ? 1 : 0.5}
                            onPress={() =>
                                this.dataRequest ? null :
                                    this.props.navigation.navigate('EmployeeDetail', {
                                        data: listEploy,
                                        nameEmploy: name,
                                        actions: data => this.getEmployUp(data)
                                    })}
                            style={style.coverItem}
                        >
                            <LineItem
                                title='Nhân viên'
                                valueName={name}
                                isLoad={isLoadEmploy}
                                hideRow={ this.dataRequest ? true : false}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity
                            activeOpacity={this.dataRequest ? 1 : 0.5}
                            onPress={() => {
                                this.dataRequest ? null :
                                    this.setState({ isVisibleDate: true })
                            }}
                            style={style.coverItem}
                        >
                            <LineItem
                                title='Ngày'
                                valueName={Utils.formatTime(this.state.valueTodate, 'DD/MM/YYYY', true)}
                                isLoad={this.dataRequest ? isLoadEmploy : false}
                                source={imgApp.calendar}
                                hideRow={ this.dataRequest ? true : false}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('ShiftDetail', {
                                data: listShift,
                                frames: frames,
                                actions: data => this.getShiftUp(data)
                            }, () => this.setState({ dataShifts: [] }))
                            }
                            style={style.coverItem}
                        >
                            <LineItem
                                title='Ca làm việc'
                                valueName={frames}
                                isLoad={isLoadShift}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('FrameWorks', {
                                data: data,
                                check: timeframes,
                                typeShift: typeShift,
                                actions: (data) => this.getFrames(data)
                            })}
                            style={style.coverItem}
                        >
                            <LineItem
                                title='Chấm công'
                                valueName={timeframes}
                                isLoad={isLoadShift}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={style.coverItem}
                            onPress={() => {
                                customTime ?
                                    this.setState({ timeIn: true, }) : null
                            }}
                        >
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ color: settingApp.colorText, fontSize: 16 }}>
                                    Duyệt vào ca
                                </Text>
                                <Text style={{ color: '#FF3B30', top: 0 }}> * </Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                {
                                    isLoadShift ? this.loadDing() :
                                        listShift.length > 0 ?
                                            <View style={style.viewRight}>
                                                <Text style={style.textRightItem}>
                                                    {valueHoursIn && valueMinIn ? valueHoursIn + ':' + valueMinIn :
                                                        aprIn}
                                                </Text>
                                                {customTime ? this.row() : null}
                                            </View> : this.error()
                                }
                            </View>
                        </TouchableOpacity>


                        <TouchableOpacity
                            style={style.coverItem}
                            onPress={() => {
                                customTime ?
                                    this.setState({ timeOut: true, }) : null
                            }}
                        >
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ color: settingApp.colorText, fontSize: 16 }}>
                                    Duyệt kết thúc ca
                                </Text>
                                <Text style={{ color: '#FF3B30', top: 0 }}> * </Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                {
                                    isLoadShift ? this.loadDing() :
                                        listShift.length > 0 ?
                                            <View style={style.viewRight}>
                                                <Text style={style.textRightItem}>
                                                    {
                                                        valueHoursOut && valueMinOut ? valueHoursOut + ':' + valueMinOut :
                                                            aprOut}
                                                </Text>
                                                {customTime ? this.row() : null}
                                            </View>
                                            : this.error()
                                }
                            </View>
                        </TouchableOpacity>

                        <View
                            style={style.coverItem}
                        >
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ color: settingApp.colorText, fontSize: 16 }}>
                                    Tổng giờ công duyệt
                                </Text>
                                <Text style={{ color: '#FF3B30', top: 0 }}> * </Text>
                            </View>
                            <View style={{ borderRadius: 10, justifyContent: 'center', backgroundColor: '#4CD964', marginRight: 15, }}>
                                <Text style={{ paddingLeft: 5, paddingRight: 5, fontSize: 16, color: '#FFFFFF' }}>
                                    {totalHours || totalMin ? totalHours + 'h' + totalMin : '0h0'}
                                </Text>
                            </View>
                        </View>

                        <View style={{ flex: 1, height: 120, marginTop: 10, marginLeft: 15 }}>
                            <TextInput
                                placeholder="Ghi chú"
                                multiline={true}
                                style={{ flex: 1, maxHeight: 120, textAlignVertical: 'top', fontSize: 16 }}
                                underlineColorAndroid="transparent"
                                selectionColor={settingApp.color}
                                value={this.state.noteApprod}
                                onChangeText={(noteApprod) => this.setState({ noteApprod })}
                            />
                        </View>
                        {this.renderButton()}
                    </View>
                </ScrollView>

                <DatePicker
                    value={this.state.valueTodate}
                    validate={data => this.validate('fromDate', data)}
                    isVisible={this.state.isVisibleDate}
                    close={() => this.setState({ isVisibleDate: false })}
                />

                <TimePicker
                    value={this.timeIn}
                    validate={(hour, minute) => this.setTime('timeIn', hour, minute)}
                    visible={this.state.timeIn}
                    close={() => this.setState({ timeIn: false })}
                />

                <TimePicker
                    value={this.timeOut}
                    validate={(hour, minute) => this.setTime('timeOut', hour, minute)}
                    visible={this.state.timeOut}
                    close={() => this.setState({ timeOut: false })}
                />
            </KeyboardAvoidingView>
        )
    };
}
const style = {
    coverItem: {
        flexDirection: 'row',
        marginLeft: 15,
        justifyContent: 'space-between',
        paddingBottom: 10,
        paddingTop: 10,
        borderBottomColor: '#C8C7CC',
        borderBottomWidth: 0.5,
    },
    textRightItem: {
        color: '#8E8E93',
        paddingRight: 5,
        fontSize: 16,
        maxWidth: 150,
    },
    viewRight: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingRight: 15,
        flexDirection: 'row'
    },

}
export default Layout