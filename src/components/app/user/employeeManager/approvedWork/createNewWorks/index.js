import Layout from './layout';
import { HaravanHr } from '../../../../../../Haravan';
import { Utils, Toast } from '../../../../../../public';
import actions from '../../../../../../state/action';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';

class CreateWorks extends Layout {
    constructor(props) {
        super(props);
        const todate = new Date();

        this.timeOut = new Date();
        this.timeIn = new Date();

        this.state = {
            isLoad: false,
            isLoadDepart: true,
            isLoadShift: true,
            isLoadEmploy: true,
            noteApprod: '',
            valueTodate: todate,
            isVisibleDate: false,
            dataShifts: [],

            timeIn: false,
            timeOut: false,

            valueHoursOut: null,
            valueHoursIn: null,
            valueMinIn: null,
            valueMinOut: null,
            totalHours: 0,
            totalMin: 0,

            titleFrames: null,
            customTime: true,
            typeShift: null,
            typeFrames: 0,
            totalRelax: 0

        };
        this.colleague = props.app.colleague;
        this.listDepart = []
        this.listShift = []
        this.listEploy = []

        this.dateDf = ''
        this.aprIn = '';
        this.aprOut = '';
        this.name = '';
        this.departName = '';
        this.frames = '';

        this.DepartUp = null;
        this.ShiftUp = null;
        this.EmployUP = null;

        this.idDPM = null
        this.shiftID = null
        this.idELM = null

        this.dataRequest = props.navigation.state.params.data;
    }

    async componentDidMount() {
        if (!this.dataRequest) {
            await this.getData()
            this.setDefaul()
        }
        else {
            await this.getData()
            this.setDefaulRequest()
        }
    }

    validate(type, data) {
        if (type === 'fromDate') {
            this.setState({ valueTodate: data }, () => this.getShiftUp(this.state.dataShifts))
        } else {
            this.setState({ valueTodate: this.state.valueTodate }, () => this.getShiftUp(this.state.dataShifts))
        }
    }

    totalTime(checkButton) {
        const { valueHoursIn, valueHoursOut, valueMinIn, valueMinOut, totalRelax } = this.state;
        let relaxMin = (totalRelax - (Math.floor(totalRelax))) * 60;
        let totalMin = (valueMinIn > valueMinOut) ? ((valueMinOut * 1) + 60) - valueMinIn : valueMinOut - valueMinIn
        let ours = (valueHoursOut - valueHoursIn) - (Math.floor(totalRelax))
        let totalHours = (ours * 1) <= 0 ? 0 : ours
        let hour = relaxMin > totalMin ? totalHours - 1 : totalHours;
        totalHours = hour;

        let min = relaxMin > totalMin ? ((totalMin + 60) - relaxMin) - totalMin : (totalMin - relaxMin);
        totalMin = min;

        const formatHours = totalHours
        const formatMinute = totalMin
        totalHours = formatHours;
        if (valueHoursIn > valueHoursOut || checkButton == false) {
            Toast.show('Giờ ra phải lớn hơn giờ vào')
        }
        else if ((valueHoursIn === valueHoursOut) && (valueMinIn > valueMinOut)) {
            Toast.show('Giờ ra phải lớn hơn giờ vào')
        }
        else if ((valueHoursIn === valueHoursOut) && (valueMinIn === valueMinOut)) {
            Toast.show('Giờ ra phải lớn hơn giờ vào')
        }
        else if ((valueHoursIn === valueHoursOut) && (valueMinIn < valueMinOut)) {
            totalMin = valueMinIn < valueMinOut ? valueMinOut - valueMinIn : ((valueMinOut * 1) + 60) - valueMinIn
        }
        else if (valueHoursIn < valueHoursOut && valueMinIn > valueMinOut) {
            totalMin = ((valueMinOut * 1) + 60) - valueMinIn
            let ours = ((valueHoursIn * 1) + 1) === valueHoursOut ? 0 : (valueHoursOut - valueHoursIn)
            totalHours = (ours * 1) <= 0 ? 0 : ours - 1
        }
        this.setState({
            totalHours: totalHours,
            totalMin: totalMin,
            totalRelax: 0
        })
    }

    setTime(type, hour, minute) {
        const { valueHoursIn, valueHoursOut, valueMinIn, valueMinOut } = this.state;
        const hoursIn = this.aprIn.slice(0, 2);
        const hoursOut = this.aprOut.slice(0, 2);
        const minIn = this.aprIn.slice(3, 5);
        const minOut = this.aprOut.slice(3, 5)
        const m = moment();
        const formatHours = hour < 10 ? '0' + hour : hour
        const formatMinute = minute < 10 ? '0' + minute : minute
        m.set({ minute: 0, second: 0, millisecond: 0 });
        if (type === 'timeIn') {
            this.setState({
                valueHoursIn: formatHours ? formatHours : this.timeIn.getHours(),
                valueMinIn: formatMinute ? formatMinute : this.timeIn.getMinutes()
            }, () => {
                this.totalTime()
            })
        }
        else {
            this.setState({
                valueHoursOut: formatHours ? formatHours : this.timeOut.getHours(),
                valueMinOut: formatMinute ? formatMinute : m.format('mm')
            }, () => {
                this.totalTime()
            })
        }
    }

    getEmployUp(data) {
        this.name = data.fullName
        this.EmployUP = data
    }

    settupWeed(day) {
        const date = day
        const weekday = new Array(7)
        weekday[0] = { title: 'Chủ nhật', isDay: 'isSun', isSun: true };
        weekday[1] = { title: 'Thứ 2', isDay: 'isMon', isMon: true };
        weekday[2] = { title: 'Thứ 3', isDay: 'isTue', isTue: true };
        weekday[3] = { title: 'Thứ 4', isDay: 'isWed', isWed: true };
        weekday[4] = { title: 'Thứ 5', isDay: 'isThu', isThu: true };
        weekday[5] = { title: 'Thứ 6', isDay: 'isFri', isFri: true };
        weekday[6] = { title: 'Thứ 7', isDay: 'isSat', isSta: true };
        return weekday[date.getDay()]
    }

    checkFrame(data, checkday) {
        const frames = []
        data.timeframes.map(e => {
            if (e.isSun === checkday.isSun) {
                frames.push(e)
            }
            else if (e.isMon === checkday.isMon) {
                frames.push(e)
            }
            else if (e.isTue === checkday.isTue) {
                frames.push(e)
            }
            else if (e.isWed === checkday.isWed) {
                frames.push(e)
            }
            else if (e.isThu === checkday.isThu) {
                frames.push(e)
            }
            else if (e.isFri === checkday.isFri) {
                frames.push(e)
            }
            else if (e.isSat === checkday.isSat) {
                frames.push(e)
            }
        })
        return frames
    }

    getShiftUp(data) {
        const { dataShifts } = this.state;
        const timeframes = 'Tùy chọn'
        const checkday = this.settupWeed(this.state.valueTodate)
        const checkFrame = this.checkFrame(data, checkday)
        this.frames = data.name;
        this.aprIn = checkFrame.length > 0 && checkFrame[0].checkInTime ? checkFrame[0].checkInTime : '00:00';
        this.aprOut = checkFrame.length > 0 && checkFrame[0].checkOutTime ? checkFrame[0].checkOutTime : '00:00';
        this.ShiftUp = data;
        const hoursIn = this.aprIn.slice(0, 2);
        const hoursOut = this.aprOut.slice(0, 2);
        const minIn = this.aprIn.slice(3, 5);
        const minOut = this.aprOut.slice(3, 5)
        const totalHours = hoursOut - hoursIn;
        const totalMin = minOut - minIn;
        this.setState({
            totalRelax: checkFrame.length >0 ? checkFrame[0].totalRelax : 0,
            valueHoursIn: hoursIn,
            valueHoursOut: hoursOut,
            valueMinIn: minIn,
            valueMinOut: minOut,
            totalHours: totalHours,
            totalMin: totalMin,
            typeShift: data.approveType,
            dataShifts: data,
            timeframes: timeframes,
            customTime: true
        }, this.totalTime());

    }

    getDepartUp(data) {
        this.departName = data.name
        this.DepartUp = data
    }

    getFrames(data) {
        let { dataShifts, valueHoursIn, valueHoursOut, valueMinIn, valueMinOut, valueTodate } = this.state;
        const checkday = this.settupWeed(this.state.valueTodate)
        const checkFrame = this.checkFrame(dataShifts, checkday)
        valueHoursIn = checkFrame.length > 0 && checkFrame[0].checkInTime ? checkFrame[0].checkInTime.slice(0, 2) : '00';
        valueHoursOut = checkFrame.length > 0 && checkFrame[0].checkOutTime ? checkFrame[0].checkOutTime.slice(0, 2) : '00';
        valueMinIn = checkFrame.length > 0 && checkFrame[0].checkInTime ? checkFrame[0].checkInTime.slice(3, 5) : '00';
        valueMinOut = checkFrame.length > 0 && checkFrame[0].checkOutTime ? checkFrame[0].checkOutTime.slice(3, 5) : '00';
        let hourIn = '00';
        let houtOut = '00';
        let minIn = '00';
        let minOut = '00';
        if (data && data.status == 1) {
            this.setState({ timeframes: data.name, customTime: true })
        }
        else {
            this.setState({ timeframes: data.name, customTime: false })
            // nguyen ca
            if (data && data.type === 1) {
                hourIn = checkFrame.length > 0 && checkFrame[0].checkInTime ? checkFrame[0].checkInTime.slice(0, 2) : '00';
                houtOut = checkFrame.length > 0 && checkFrame[0].checkOutTime ? checkFrame[0].checkOutTime.slice(0, 2) : '00';
                minIn = checkFrame.length > 0 && checkFrame[0].checkInTime ? checkFrame[0].checkInTime.slice(3, 5) : '00';
                minOut = checkFrame.length > 0 && checkFrame[0].checkOutTime ? checkFrame[0].checkOutTime.slice(3, 5) : '00';
                this.setState({ totalRelax: checkFrame.length > 0 ? checkFrame[0].totalRelax : 0, })
            }
            // 0.5 ca sang
            else if (data && data.type === 2) {
                hourIn = checkFrame.length > 0 && checkFrame[0].checkInTime ? checkFrame[0].checkInTime.slice(0, 2) : '00';
                houtOut = checkFrame.length > 0 && checkFrame[0].relaxBeginTime ? checkFrame[0].relaxBeginTime.slice(0, 2) : (hourIn * 1) + (this.formatHours(checkFrame[0].checkInTime, checkFrame[0].checkOutTime));
                minIn = checkFrame.length > 0 && checkFrame[0].checkInTime ? checkFrame[0].checkInTime.slice(3, 5) : '00';
                minOut = checkFrame.length > 0 && checkFrame[0].relaxBeginTime ? checkFrame[0].relaxBeginTime.slice(3, 5) : (valueMinIn * 1) + (this.formatMin(checkFrame[0].checkInTime, checkFrame[0].checkOutTime));
                let formatMin = minOut > 60 ? minOut - 60 : minOut
                minOut = formatMin
            }
            // 0.5 ca chieu
            else if (data && data.type === 3) {
                hourIn = checkFrame.length > 0 && checkFrame[0].relaxEndTime ? checkFrame[0].relaxEndTime.slice(0, 2) : (valueHoursIn * 1) + (this.formatHours(checkFrame[0].checkInTime, checkFrame[0].checkOutTime));
                houtOut = checkFrame.length > 0 && checkFrame[0].checkOutTime ? checkFrame[0].checkOutTime.slice(0, 2) : '00';
                minIn = checkFrame.length > 0 && checkFrame[0].relaxEndTime ? checkFrame[0].relaxEndTime.slice(3, 5) : (valueMinIn * 1) + (this.formatMin(checkFrame[0].checkInTime, checkFrame[0].checkOutTime));;
                minOut = checkFrame.length > 0 && checkFrame[0].checkOutTime ? checkFrame[0].checkOutTime.slice(3, 5) : '00';
                let formatMin = minIn > 60 ? (minIn * 1) - 60 : minIn
                let formatHours = minIn > 60 ? (hourIn * 1) + 1 : hourIn
                hourIn = formatHours;
                minIn = formatMin;
            }
            //0.5 ca
            else if (data && data.type === 4) {
                hourIn = checkFrame.length > 0 && checkFrame[0].checkInTime ? checkFrame[0].checkInTime.slice(0, 2) : '00';
                houtOut = checkFrame.length > 0 && checkFrame[0].relaxBeginTime ? checkFrame[0].relaxBeginTime.slice(0, 2) : (hourIn * 1) + (this.formatHours(checkFrame[0].checkInTime, checkFrame[0].checkOutTime));
                minIn = checkFrame.length > 0 && checkFrame[0].checkInTime ? checkFrame[0].checkInTime.slice(3, 5) : '00';
                minOut = checkFrame.length > 0 && checkFrame[0].relaxBeginTime ? checkFrame[0].relaxBeginTime.slice(3, 5) : (this.formatMin(checkFrame[0].checkInTime, checkFrame[0].checkOutTime));
                let formatMin = minOut > 60 ? minOut - 60 : minOut - minIn
                minOut = formatMin
            }
            this.setState({

                valueHoursIn: hourIn,
                valueHoursOut: houtOut,
                valueMinIn: minIn,
                valueMinOut: minOut,
                typeFrames: data.type,
            }, () => {
                this.totalTime()
            })
        }
    }

    formatHours(timeIn, timeOut) {
        const hourIn = timeIn.slice(0, 2);
        const hourOut = timeOut.slice(0, 2)
        let totalHours = hourOut - hourIn
        if (totalHours > 1) {
            totalHours = Math.floor(totalHours / 2)
        }
        else {
            totalHours = totalHours
        }
        return totalHours
    }

    formatMin(timeIn, timeOut) {
        const hourIn = timeIn.slice(0, 2);
        const hourOut = timeOut.slice(0, 2)
        const minIn = timeIn.slice(3, 5)
        const minOut = timeOut.slice(3, 5)
        let minMore = 0;
        let totalMin = 0;
        const totalHours = (hourOut - hourIn) / 2
        minMore = (totalHours - Math.floor(totalHours)) * 60
        if (totalHours <= 1) {
            if (minOut === minIn) {
                totalMin = (minIn * 1) + 30
            }
            else {
                totalMin = minOut >= minIn ? (Math.ceil((minOut - minIn) * 1) / 2) + minMore : (Math.ceil(((minOut * 1) + 60) - minIn) / 2) + minMore
            }
        }
        else {
            totalMin = minOut >= minIn ? (Math.ceil((minOut - minIn) * 1) / 2) + minMore : (Math.ceil(((minOut * 1) + 60) - minIn) / 2) + minMore
        }
        return totalMin
    }

    async create(stt, aprStatus) {
        const { noteApprod, valueTodate, valueHoursIn, valueHoursOut, valueMinIn, valueMinOut, typeFrames } = this.state;
        const { DepartUp, ShiftUp, EmployUP, lisEploy } = this;
        const note = noteApprod ? noteApprod : ' ';
        const depart = EmployUP ? EmployUP.departmentId : this.idDPM
        const idEl = EmployUP ? EmployUP.employeeId : this.idELM
        const shift = ShiftUp ? ShiftUp.id : this.shiftID
        let day = Utils.formatTime(this.state.valueTodate, 'YYYY-MM-DD')
        let checkIn = valueHoursIn && valueMinIn ? valueHoursIn + ':' + valueMinIn : (ShiftUp.timeframes[0].checkInTime)
        let checkOut = valueHoursOut && valueMinOut ? valueHoursOut + ':' + valueMinOut : (ShiftUp.timeframes[0].checkOutTime)
        const body = {
            ApproveNote: note,
            Checkin: day + ' ' + checkIn,
            Checkout: day + ' ' + checkOut,
            DepartmentId: depart,
            EmployeeId: idEl,
            OTHours: [],
            ShiftId: shift,
            Status: aprStatus,
        }
        if (stt && stt == 2) {
            const resCreate = await HaravanHr.postCreateWorks(body, typeFrames)
            if (resCreate && resCreate.data && !resCreate.error) {
                this.props.actions.aprov.refreshList(true);
                this.props.navigation.state.params.actions(true);
                this.setDefaul();
                this.setState({ isLoad: false },() =>{
                    this.props.navigation.state.params.backContent(true)
                    this.props.navigation.pop();
                })
            }
            else {
                Toast.show(resCreate.message)
                this.setState({ isLoad: false })
            }
        }
        else {
            const resCreate = await HaravanHr.postCreateWorks(body, typeFrames)
            if (resCreate && resCreate.data && !resCreate.error) {
                if (this.dataRequest) {
                    const data = {
                        ApproverNote: "APPROVED EMPLOYEEREQUEST",
                        Id: this.dataRequest.id,
                        Status: 2,
                    }
                    const resRequest = await HaravanHr.putEmployeeRequest(data)
                }
                this.props.actions.aprov.refreshList(true);
                this.props.navigation.state.params.actions(true);
                this.setDefaul();
                this.setState({ isLoad: false },() =>{
                    this.props.navigation.state.params.backContent(true)
                    this.props.navigation.pop();
                })
            }
            else {
                Toast.show(resCreate.message)
                this.setState({ isLoad: false })
            }
        }
    }

    setDefaul() {
        const { data } = this.state;
        const date = Utils.formatTime((new Date()), 'DD/MM/YYYY', true)
        const currentDate = new Date();
        this.dateDf = date;
        const timeframes = 'Tùy chọn'
        if (this.listShift) {
            if (this.listShift.length > 0) {
                this.frames = this.listShift[0].name
                const checkDate = this.settupWeed(currentDate)
                const checkFrame = this.checkFrame(this.listShift[0], checkDate)
                if (this.listShift[0].timeframes) {
                    this.aprIn = checkFrame.length > 0 ? checkFrame[0].checkInTime : '00:00'
                    this.aprOut = checkFrame.length > 0 ? checkFrame[0].checkOutTime : '00:00'
                    const hoursIn = this.aprIn.slice(0, 2);
                    const hoursOut = this.aprOut.slice(0, 2);
                    const minIn = this.aprIn.slice(3, 5);
                    const minOut = this.aprOut.slice(3, 5)
                    this.shiftID = this.listShift[0].id
                    const totalHours = (hoursOut - hoursIn) - 1;
                    const totalMin = (minOut < minIn) ? ((minOut * 1) + 60) - minIn : minOut - minIn;
                    this.setState({
                        valueHoursIn: hoursIn,
                        valueHoursOut: hoursOut,
                        valueMinIn: minIn,
                        valueMinOut: minOut,
                        totalHours: totalHours,
                        totalMin: totalMin,
                        timeframes: timeframes,
                        typeShift: this.listShift[0].approveType,
                        dataShifts: this.listShift[0],
                        totalRelax: checkFrame.length > 0 && checkFrame[0].totalRelax ? checkFrame[0].totalRelax : 0
                    })
                }
                this.setState({ isLoadShift: false }, () => this.totalTime())
            }
            else {
                this.setState({ isLoadShift: false })
            }
        }
        if (this.listDepart) {
            if (this.listDepart.length > 0) {
                this.departName = this.listDepart[0].name
                this.setState({ isLoadDepart: false })
            }
            else {
                this.setState({ isLoadDepart: false })
            }
        }
        if (this.listEploy) {
            if (this.listEploy.length > 0) {
                this.name = this.listEploy[0].fullName
                this.setState({ isLoadEmploy: false })
            }
            else {
                this.setState({ isLoadEmploy: false })
            }

        }
        else {
            this.dateDf = ''
            this.aprIn = '';
            this.aprOut = '';
            this.name = '';
            this.departName = '';
            this.frames = '';
        }
    }

    setDefaulRequest() {
        const { dataRequest } = this;
        const data = JSON.parse(dataRequest.requestData)
        const date = new Date(Utils.formatTime(dataRequest.requestApproveDate, 'YYYY-MM-DD', true))
        const currentDate = new Date();
        this.dateDf = date;
        const timeframes = 'Tùy chọn'
        const checkDate = this.settupWeed(currentDate)
        const checkFrame = this.checkFrame(dataRequest.requestShift, checkDate)
        if (this.dataRequest) {
            this.idDPM = dataRequest.requestDepartmentId
            this.idELM = dataRequest.requester.id

            this.frames = dataRequest.requestShiftName
            this.aprIn = data.checkinTime
            this.aprOut = data.checkoutTime
            const hoursIn = this.aprIn.slice(0, 2);
            const hoursOut = this.aprOut.slice(0, 2);
            const minIn = this.aprIn.slice(3, 5);
            const minOut = this.aprOut.slice(3, 5)
            this.shiftID = dataRequest.shift.id
            const totalHours = (hoursOut - hoursIn) - 1;
            const totalMin = (minOut < minIn) ? ((minOut * 1) + 60) - minIn : minOut - minIn;
            this.setState({
                valueTodate: date,
                valueHoursIn: hoursIn,
                valueHoursOut: hoursOut,
                valueMinIn: minIn,
                valueMinOut: minOut,
                totalHours: totalHours,
                totalMin: totalMin,
                timeframes: timeframes,
                typeShift: dataRequest.requestShift.approveType,
                dataShifts: dataRequest.requestShift,
                totalRelax: checkFrame.length > 0 && checkFrame[0].totalRelax ? checkFrame[0].totalRelax : 0
            })
            this.setState({ isLoadShift: false }, () => this.totalTime())
        }
        if (this.listDepart || this.dataRequest) {
            if (this.dataRequest) {
                this.departName = this.dataRequest.requesterDepartmentName
                this.setState({ isLoadDepart: false })
            }
            else {
                if (this.listDepart.length > 0) {
                    this.departName = this.listDepart[0].name
                    this.setState({ isLoadDepart: false })
                }
                else {
                    this.setState({ isLoadDepart: false })
                }
            }
        }
        if (this.listEploy || this.dataRequest) {
            if (this.dataRequest) {
                this.name = this.dataRequest.requester.fullName
                this.setState({ isLoadEmploy: false })
            }
            else {
                if (this.listEploy.length > 0) {
                    this.name = this.listEploy[0].fullName
                    this.setState({ isLoadEmploy: false })
                }
                else {
                    this.setState({ isLoadEmploy: false })
                }
            }
        }
        else {
            this.dateDf = ''
            this.aprIn = '';
            this.aprOut = '';
            this.name = '';
            this.departName = '';
            this.frames = '';
        }
    }

    async getData() {
        const idDepartmenet = this.colleague.mainPosition.departmentId
        const resDepart = await HaravanHr.getMeListDepartments()
        const resShift = await HaravanHr.getShifts()
        const resEmploy = await HaravanHr.getEmploy()
        if (resDepart && resDepart.data && !resDepart.error) {
            this.listDepart = resDepart.data
        }
        if (resShift && resShift.data && !resShift.error) {
            this.listShift = resShift.data.data
        }
        if (resEmploy && resEmploy.data.data && !resEmploy.error) {
            this.listEploy = resEmploy.data.data
            this.idDPM = resEmploy.data.data[0].departmentId
            this.idELM = resEmploy.data.data[0].employeeId
        }
        else if (
            resDepart.error == true || resEmploy.error == true || resShift.error == true
            || resEmploy.data.data.length < 0 || resDepart.data.length < 0 || resShift.data.data.length < 0
        ) {
            this.setState({
                isLoadDepart: false,
                isLoadShift: false,
                isLoadEmploy: false
            })
        }
    }

    componentWillUnmount() {
        this.props.actions.aprov.refreshList(null)
    }
}
mapStateToProps = state => ({ ...state });
mapDispatchToProps = dispatch => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch)
    }
    return { actions: acts }
}
export default connect(mapStateToProps, mapDispatchToProps)(CreateWorks);