import React, { Component } from 'react';
import { View, TimePickerAndroid } from 'react-native';
import moment from 'moment';

class TimePicker extends Component {

    constructor(props){
        super(props)
        this.time = new Date(this.props.value).getHours()
    }

    submit(valueDate, minute) {
        this.props.close();
        this.props.validate(valueDate, minute);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.visible) {
            this.openCalendar();
        }
    }

    async openCalendar() {
        try {
            const {action, hour, minute} = await TimePickerAndroid.open({
              hour: this.time,
              minute: 0,
              is24Hour: true, // Will display '2 PM'
            });
            if (action !== TimePickerAndroid.dismissedAction) {
              // Selected hour (0-23), minute (0-59)
              const value = hour
              this.submit(value, minute);
            }
            else{
                this.props.close()
            }
          } catch ({code, message}) {
            console.warn('Cannot open time picker', message);
          }
    }


    render() {
        return <View />;
    }
}

export default TimePicker;