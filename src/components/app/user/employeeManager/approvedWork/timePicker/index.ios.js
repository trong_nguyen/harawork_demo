import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, DatePickerIOS } from 'react-native';
import { settingApp, imgApp, Utils } from '../../../../../../public';
import Modal from "react-native-modal";
import moment from 'moment';

class TimePicker extends Component {
    constructor(props) {
        super(props);
        this.date = new Date().getHours();
        this.minute = new Date().getMinutes()
    }

    setDate(date) {
        const hours = new Date(date).getHours()
        const minute = new Date(date).getMinutes()
        this.date = hours;
        this.minute = minute;
    }

    submit() {
        this.props.validate(this.date, this.minute);
        this.props.close();
        this.date = new Date().getHours();
        this.minute = new Date().getMinutes()
    }

    cancel() {
        this.props.close();
        this.date = new Date().getHours();
        this.minute = new Date().getMinutes()
    }

    render() {
        const { width, color, colorSperator } = settingApp;
        return (
            <Modal isVisible={this.props.visible} style={{ flex: 1 }}>
                <TouchableOpacity
                    onPress={() => this.cancel()}
                    activeOpacity={1}
                    style={{ flex: 1, marginLeft: -20, marginTop: -20, marginRight: -20, backgroundColor: 'transparent' }}
                />
                <View style={{ width, marginLeft: -20, marginBottom: -20, backgroundColor: '#ffffff' }}>
                    <View style={{
                        width,
                        height: 40,
                        alignItems: 'flex-end',
                        backgroundColor: '#ecf0f1',
                        borderTopColor: colorSperator,
                        borderTopWidth: 1,
                        borderBottomColor: colorSperator,
                        borderBottomWidth: 1,
                        paddingRight: 10
                    }}>
                        <TouchableOpacity
                            onPress={() => this.submit()}
                            style={{ width: 50, height: 40, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color, fontSize: 14, fontWeight: 'bold' }}>XONG</Text>
                        </TouchableOpacity>
                    </View>
                    <DatePickerIOS
                        date= {this.props.value}
                        mode='time'
                        onDateChange={date => this.setDate(date)}
                    />
                </View>
            </Modal>
        )
    }
}

export default TimePicker;