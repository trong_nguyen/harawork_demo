import Layout from './layout';
import actions from '../../../../../../state/action';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { HaravanHr } from '../../../../../../Haravan';
import moment from 'moment';
import { Toast, Utils } from '../../../../../../public';

class ApprovedDetail extends Layout {
    constructor(props) {
        super(props)
        this.timeOut = new Date();
        this.timeIn = new Date()
        this.state = {
            noteApprod: '',
            modalVisible: false,
            statusApr: null,
            isLoading: true,
            isEarly: false,
            isLate: false,
            timeIn: false,
            timeOut: false,
            valueHoursOut: null,
            valueHoursIn: null,
            valueMinIn: this.timeOut.getMinutes(),
            valueMinOut: this.timeOut.getHours(),
            totalHours: 0,
            totalMin: 0,
            isLoad: false,
            checkOutTrace: true,
            checkInTrace: true,
            timeData: null
        }

        this.colleague = props.app.colleague;
        this.dataItem = props.navigation.state.params.item;
        this.item = [];
        this.employee = [];
        this.status = props.navigation.state.params.status;
        this.checkTime = props.navigation.state.params.checkTime;
        this.isMustCheckout = null;

        const day = new Date(this.dataItem.checkin).getDay()

    }

    async componentDidMount() {
        const isLate = false
        if (this.dataItem.statusRequest == 0) {
            this.checkIP(this.dataItem)
            const res = await HaravanHr.getApprovedDetail(this.dataItem.id);
            if (res && res.data && !res.error) {
                this.item = res.data
                this.checkTimeFrame(this.item)
                //this.timeIn = res.data.checkin
                this.isMustCheckout = res.data.shift.isMustCheckout
                const timeIn = res.data.checkinTime.slice(0, 5)
                const timeOut = res.data.checkoutTime.slice(0, 5)
                const checkinTimeIn = moment.utc(res.data.checkin).local().format('HH:mm')
                const checkinTimeOut = res.data.checkout ? moment.utc(res.data.checkout).local().format('HH:mm') : null
                const resEmploy = await HaravanHr.getColleagueByEmployeeId(res.data.employeeId)
                if (resEmploy && resEmploy.data && !resEmploy.error) {
                    this.employee = resEmploy.data;
                    this.setState({ isLoading: false })
                }
                // this.checkIP()
                this.setState({
                    isEarly: res.data.status == 3 ? this.item.isEarly : (!checkinTimeOut || timeOut > checkinTimeOut ? true : false),
                    isLate: res.data.status == 3 ? this.item.isLate : (timeIn < checkinTimeIn ? true : false),
                    valueHoursIn: res.data.checkin ? moment.utc(res.data.checkin).local().format('HH') : res.data.checkinTime.slice(0, 2),
                    valueMinIn: res.data.checkin ? moment.utc(res.data.checkin).local().format('mm') : res.data.checkinTime.slice(3, 5),
                    valueHoursOut: res.data.checkout ? moment.utc(res.data.checkout).local().format('HH') : res.data.checkoutTime.slice(0, 2),
                    valueMinOut: res.data.checkout ? moment.utc(res.data.checkout).local().format('mm') : res.data.checkoutTime.slice(3, 5),
                })
            }
            else{
                this.setState({ isLoading: false })
            }
        }
        else{
            this.setState({
                    timeData: this.dataItem.shift.time.timeframes[0].checkinTime,
                    valueHoursIn:this.dataItem.shift.time.timeframes[0].checkinTime,
                    valueHoursOut:this.dataItem.shift.time.timeframes[0].checkOutTime,
            })
            this.setState({isLoading:false})
        }
    }

    backContent(status){
        if(status == true){
            this.props.navigation.state.params.backContent(status)
            this.props.navigation.pop()
        }
    }

    delete(data){
        this.props.navigation.state.params.actions(data)
        this.props.navigation.pop()
    }

    async checkIP(data) {
        const { checkinTrace , departmentId, checkoutTrace} = data;
        if (checkinTrace !== null) {
            const checkIP = await HaravanHr.getisIp(checkinTrace.ipAddress, departmentId)
            if (checkIP && checkIP.data && !checkIP.error) {
                this.setState({ checkInTrace: checkIP.data.isIp })
            }
            else {
                this.setState({ checkInTrace: false })
            }
        }
        else {
            this.setState({ checkInTrace: false })
        }

        if (checkoutTrace !== null) {
            const checkIP = await HaravanHr.getisIp(checkoutTrace.ipAddress, departmentId)
            if (checkIP && checkIP.data && !checkIP.error) {
                this.setState({ checkOutTrace: checkIP.data.isIp })
            }
            else {
                this.setState({ checkOutTrace: false })
            }
        }
        else {
            this.setState({ checkOutTrace: false })
        }
    }

    totalTime(checkButton) {
        const { valueHoursIn, valueHoursOut, valueMinIn, valueMinOut } = this.state
        let totalMin = valueMinOut - valueMinIn
        const totalHours = valueHoursOut - valueHoursIn
        const formatHours = totalHours < 10 ? '0' + totalHours : totalHours

        if (valueHoursIn > valueHoursOut || checkButton == false) {
            Toast.show('Giờ ra phải lớn hơn giờ vào')
        }
        else if ((valueHoursIn === valueHoursOut) && (valueMinIn > valueMinOut)) {
            Toast.show('Giờ ra phải lớn hơn giờ vào')
        }
        else if (totalMin == 60) {
            totalHours = totalHours + 1
        }
        else if (valueHoursIn < valueHoursOut && valueMinIn > valueMinOut) {
            totalMin = ((valueMinOut * 1) + 60) - valueMinIn
        }
        const formatMinute = totalMin < 10 ? '0' + totalMin : totalMin
        this.setState({
            totalHours: formatHours,
            totalMin: formatMinute,
        })
    }

    validate(type, hour, minute) {
        const m = moment();
        let formatHours = hour < 10 ? '0' + hour : hour
        let formatMinute = minute < 10 ? '0' + minute : minute
        m.set({ minute: 0, second: 0, millisecond: 0 });
        if (type === 'timeIn') {
            this.setState({
                valueHoursIn: formatHours ? formatHours : Utils.formatTime(this.timeIn, 'HH'),
                valueMinIn: formatMinute ? formatMinute : Utils.formatTime(this.timeIn, 'mm')
            })
        }
        else {
            this.setState({
                valueHoursOut: formatHours ? formatHours : this.timeOut.getHours(),
                valueMinOut: formatMinute ? formatMinute : m.format('mm')
            })
        }
        this.totalTime()
    }

    async timeKeeping(status) {
        this.setState({ isLoad: true })
        const { colleague, item, isMustCheckout } = this;
        const id = this.dataItem.id
        const { noteApprod, isEarly, isLate, valueHoursIn, valueHoursOut, valueMinIn, valueMinOut } = this.state;
        const note = noteApprod ? noteApprod : null
        const sliceTimeIn = this.item.checkinTime.slice(0, 5);
        const sliceTimeOut = this.item.checkoutTime.slice(0, 5);
        const currentDate = new Date()
        const date = Utils.formatLocalTime(currentDate, 'YYYY-MM-DD', true)
        const checkDate = Utils.formatTime(this.item.checkin, 'YYYY-MM-DD', true)
        const dateIn = item.checkin ? Utils.formatTime(item.checkin, 'YYYY-MM-DD', true) : date;
        const dateOut = item.checkout ? Utils.formatTime(item.checkout, 'YYYY-MM-DD', true) : dateIn;
        const timeCheckIn = valueHoursIn + ':' + valueMinIn;
        const timeCheckOut = valueHoursOut + ':' + valueMinOut
        let body = {}
        const checkTimeIn = timeCheckIn ? timeCheckIn : sliceTimeIn
        let checkTimeOut = sliceTimeOut
        if (isMustCheckout == true) {
            if (timeCheckOut) {
                checkTimeOut = timeCheckOut
            }
            else {
                checkTimeOut = checkTimeIn
            }
        }
        else {
            if (timeCheckOut) {
                checkTimeOut = timeCheckOut
            }
            else {
                checkTimeOut = sliceTimeOut
            }
        }
        if (status === 3) {
            body = {
                ApproveDepartmentId: colleague.mainPosition.departmentId,
                ApproveStart: (dateIn + ' ' + checkTimeIn),
                ApproveEnd: (dateOut + ' ' + checkTimeOut),
                ApproveIsEarly: isEarly,
                ApproveIsLate: isLate,
                ApproveOTs: [],
                ApproveShiftId: item.shift.id,
                Note: note,
                Status: status
            }
        }
        else {
            body = {
                Note: note,
                Status: status
            }
        }
        const link = `supervisor/checkin/approve?timekeepingId=${id}`
        if (isMustCheckout == true && !timeCheckOut && (date === checkDate)) {
            Toast.show('Nhân viên chưa thực hiện kết ca, bạn không thể duyệt phiếu này.')
        }
        else {
            const result = await HaravanHr.postApprovedWorks(link, body, colleague);
            if (result && result.data && !result.error) {
                this.props.actions.aprov.checkAprov({ id: id, status: status, note: note })
            }
            else {
                this.props.actions.aprov.checkAprov({ status: false, message: result.message })
            }
        }
        this.props.navigation.pop()
    }

    checkTimeFrame(item) {
        const timeDay = new Date(item.checkin).getDay()
        const day = this.checkDayAweek(timeDay)
        item.shift.timeframes.map(e => {
            if (day.isDay === 'isSun') {
                if (e.isSun && e.isSun == true) {
                    item = { ...item, totalHour: e.totalHour }
                }
            }
            else if (day.isDay === 'isMon') {
                if (e.isSun && e.isSun == true) {
                    item = { ...item, totalHour: e.totalHour }
                }
            }
            else if (day.isDay === 'isTue') {
                if (e.isSun && e.isSun == true) {
                    item = { ...item, totalHour: e.totalHour }
                }
            }
            else if (day.isDay === 'isWed') {
                if (e.isSun && e.isSun == true) {
                    item = { ...item, totalHour: e.totalHour }
                }
            }
            else if (day.isDay === 'isThu') {
                if (e.isSun && e.isSun == true) {
                    item = { ...item, totalHour: e.totalHour }
                }
            }
            else if (day.isDay === 'isFri') {
                if (e.isSun && e.isSun == true) {
                    item = { ...item, totalHour: e.totalHour }
                }
            }
            else if (day.isDay === 'isSat') {
                if (e.isSat && e.isSat == true) {
                    item = { ...item, totalHour: e.totalHour }
                }
            }
        })
        this.item = item
    }

    checkDayAweek(time) {
        const weekday = new Array(7)
        weekday[0] = { title: 'Chủ nhật', isDay: 'isSun', isSun: true };
        weekday[1] = { title: 'Thứ 2', isDay: 'isMon', isMon: true };
        weekday[2] = { title: 'Thứ 3', isDay: 'isTue', isTue: true };
        weekday[3] = { title: 'Thứ 4', isDay: 'isWed', isWed: true };
        weekday[4] = { title: 'Thứ 5', isDay: 'isThu', isThu: true };
        weekday[5] = { title: 'Thứ 6', isDay: 'isFri', isFri: true };
        weekday[6] = { title: 'Thứ 7', isDay: 'isSat', isSat: true };
        return weekday[time]
    }
}
mapStateToProps = state => ({ ...state });
mapDispatchToProps = dispatch => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch)
    }
    return { actions: acts }
}
export default connect(mapStateToProps, mapDispatchToProps)(ApprovedDetail);