import React, { Component } from 'react';
import { View, Text, TouchableOpacity, KeyboardAvoidingView, ScrollView, TextInput, ActivityIndicator, Image } from 'react-native';
import { settingApp, HeaderWapper, Utils, Loading, NotFound } from '../../../../../../public';
import { MaterialIcons, Ionicons } from '@expo/vector-icons';
import ModalButton from './component/modal';
import moment from 'moment';
import TimePicker from '../timePicker';
import LineItem from './component/lineItem';
import AprItem from './component/aprItem';
import RequestApr from './component/requestApr'

class Layout extends Component {

    renderHeader() {
        return (
            <HeaderWapper
                style={{ flexDirection: 'row', justifyContent: 'space-between' }}
            >
                <TouchableOpacity
                    onPress={() => this.props.navigation.pop()}
                    style={{ width: 55, height: 43, justifyContent: 'center', alignItems: 'center' }}
                >
                    <Ionicons name='ios-arrow-back' size={23} color='#ffffff' />
                </TouchableOpacity>

                <Text style={{ alignItems: 'center', marginTop: 10, fontSize: 16, fontWeight: 'bold', color: '#FFFFFF' }}>
                    Chi tiết chấm công
                </Text>

                <View style={{ width: 55, height: 43, justifyContent: 'center', alignItems: 'center' }} />
            </HeaderWapper>
        )
    }

    renderIsLoad() {
        return (
            <View style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                backgroundColor: 'rgba(0,0,0,0.5)',
                justifyContent: 'center',
                alignItems: 'center',
            }}>
                <ActivityIndicator size='large' color={settingApp.color} />
            </View>
        )
    }

    renderLoad() {
        return (
            <View style={{
                backgroundColor: 'transparent',
                paddingRight: 15,
            }}>
                <ActivityIndicator size='small' color={settingApp.color} />
            </View>
        )
    }

    renderButton(status) {
        const { valueHoursIn, valueHoursOut } = this.state;
        const { width, height } = settingApp
        if (status == 4 || status === 3) {
            const approd = status == 3;
            return (
                <View style={{
                    width: settingApp.width, height:140, backgroundColor: '#FFFFFF'
                }}>
                    <View style={{ width: settingApp.width, marginBottom: 10, marginLeft: 10, flexWrap: 'wrap', ...settingApp.shadow, padding: 2 }}>
                        <TextInput
                            placeholder='Ghi chú'
                            underlineColorAndroid='transparent'
                            style={{ flexWrap: 'wrap', fontSize: 16, color: settingApp.color, width: (settingApp.width *0.9), height:50 , }}
                            value={this.state.noteApprod}
                            multiline={true}
                            onChangeText={(noteApprod) => this.setState({ noteApprod })}
                        />
                    </View>

                    <TouchableOpacity
                        onPress={() => {
                            (valueHoursOut < valueHoursIn) ? this.totalTime(false) :
                                this.setState({ modalVisible: true }, () => this.setState({ statusApr: approd ? false : true }))
                        }}
                        style={{ backgroundColor: approd ? '#FF3B30' : '#4CD964', borderRadius: 5, height: 60, margin: 10, alignItems: 'center', justifyContent: 'center' }}
                    >
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#FFFFFF' }}> {approd ? 'Từ chối' : 'Duyệt'} </Text>
                    </TouchableOpacity>
                </View>
            )
        }
        else {
            const approd = status == 2;
            return (
                <View style={{
                    width: settingApp.width,
                    height: 140,
                    backgroundColor: '#FFFFFF',
                    justifyContent: 'space-between',
                    flexDirection: 'column',
                }}>
                    <View style={{ width: settingApp.width, marginBottom: 10, marginLeft: 10, flexWrap: 'wrap' }}>
                        <TextInput
                            placeholder='Ghi chú'
                            underlineColorAndroid='transparent'
                            style={{ flexWrap: 'wrap', width: (settingApp.width *0.9), height:50 , paddingRight:10}}
                            value={this.state.noteApprod}
                            multiline={true}
                            onChangeText={(noteApprod) => this.setState({ noteApprod })}
                        />
                    </View>

                    <View style={{
                        flexDirection: 'row', width: settingApp.width,
                        justifyContent: 'space-between', paddingBottom: 10
                    }}>
                        <TouchableOpacity
                            onPress={() => {
                                (valueHoursOut < valueHoursIn) ? this.totalTime(false) :
                                    this.setState({ modalVisible: true }, () => this.setState({ statusApr: false }))
                            }}
                            style={{ backgroundColor: '#FF3B30', borderRadius: 5, height: 60, width: (settingApp.width*0.45), marginLeft: 10, alignItems: 'center', justifyContent: 'center' }}
                        >
                            <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#FFFFFF' }}>Từ chối</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => {
                                (valueHoursOut < valueHoursIn) ? this.totalTime(false) :
                                    this.setState({ modalVisible: true }, () => this.setState({ statusApr: true }))
                            }}
                            style={{ backgroundColor: '#4CD964', borderRadius: 5, height: 60, width:(settingApp.width*0.45), marginRight: 10, alignItems: 'center', justifyContent: 'center' }}
                        >
                            <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#FFFFFF' }}>Duyệt</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }
    }

    renderContent() {
        const { status, item, employee, checkTime } = this;
        const { checkOutTrace, checkInTrace, isLoad, noteApprod, isLoading, isEarly, isLate, valueHoursIn, valueHoursOut, valueMinIn, valueMinOut, totalHours, totalMin, timeData } = this.state
        // UTC
        const timeIn =item.checkin ?  Utils.formatTime(item.checkin, 'LT') : timeData
        const timeOut = Utils.formatTime(item.checkout, 'LT')
        const dateIn = Utils.formatTime(item.checkin, 'L', true)
        const dateOut = Utils.formatTime(item.checkout, 'L', true)

        // Local
        const appTimeIn = Utils.formatLocalTime(item.approveIn, 'LT');
        const appTimeOut = Utils.formatLocalTime(item.approveOut, 'LT');
        const appDate = Utils.formatLocalTime(item.approvedDate, 'L');
        const appTimeDate = Utils.formatLocalTime(item.approvedDate, 'LT', true)
        if (this.dataItem.statusRequest == 1) {
            return (
                <RequestApr 
                   data= { this.dataItem }
                   delete={(data) => this.delete(data)}
                   navigation = {this.props.navigation}
                   backContent={status => this.backContent(status)}
                />
            )
        }
        else {
            if (item && !item.length) {
                return (
                    <KeyboardAvoidingView behavior='padding' style={{ flex: 1 }}>
                        <ScrollView>
                            <View style={{
                                flex: 1,
                                backgroundColor: '#FFFFFF',
                                marginTop: 15,
                                ...settingApp.shadow
                            }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginLeft: 10, paddingBottom: 15, paddingTop: 15 }}>
                                    <Text style={{ fontSize: 16, color: settingApp.colorText }}> Trạng thái </Text>
                                    {
                                        isLoading ? this.renderLoad() :

                                            <View style={{ flexDirection: 'row', marginRight: 10 }}>
                                                <Text style={{ fontSize: 16, color: status.color, marginRight: 3 }}> {status.title} </Text>
                                                <Text style={{ fontSize: 16, color: checkTime && checkTime ? '#FF3B30' : '#21469B' }}>
                                                    {item.status == 2 || item.status ==
                                                        1 ? '[' + timeIn + ']' : null}</Text>
                                            </View>
                                    }

                                </View>
                            </View>

                            <View style={{
                                flex: 1,
                                backgroundColor: '#FFFFFF',
                                marginTop: 15,
                                ...settingApp.shadow
                            }}>
                                <LineItem
                                    loading={isLoading}
                                    valueTop={employee.fullName ? employee.fullName : ''}
                                    valueBot1={employee.mainPosition ? employee.mainPosition.jobtitleName : ''}
                                    valueBot2={employee.mainPosition ? employee.mainPosition.departmentName : ''}
                                />

                                <LineItem
                                    loading={isLoading}
                                    valueTop={'Ca làm việc'}
                                    valueBot1={item.shift.name}
                                />

                                <LineItem
                                    loading={isLoading}
                                    valueTop={'Giờ chấm vào'}
                                    valueBot1={timeIn}
                                    valueBot2={dateIn}
                                    checkIP={(checkInTrace==false) ? 'Chấm vào ca ngoài IP' : null}
                                />

                                {item.checkinNote ?
                                    <LineItem
                                        loading={isLoading}
                                        valueTop={'Ghi chú vào ca'}
                                        valueBot1={item.checkinNote}
                                    />
                                    : null
                                }

                                <LineItem
                                    loading={isLoading}
                                    valueTop={'Giờ kết thúc'}
                                    valueBot1={item.checkout ? timeOut : 'Chưa chấm giờ ra'}
                                    valueBot2={item.checkout ? dateOut : null}
                                    checkIP={(checkOutTrace==false) ? 'Chấm kết thúc ca ngoài IP' : null}
                                />

                                {item.checkoutNote ?
                                    <LineItem
                                        loading={isLoading}
                                        valueTop={'Ghi chú kết thúc'}
                                        valueBot1={item.checkoutNote}
                                    />
                                    : null
                                }
                            </View>

                            <View style={{ paddingBottom: 10, paddingTop: 10, justifyContent: 'center' }}>
                                <Text style={{ color: '#8E8E93', paddingLeft: 10 }}>DUYỆT CÔNG</Text>
                            </View>

                            <View style={{
                                flex: 1,
                                backgroundColor: '#FFFFFF',
                                ...settingApp.shadow
                            }}>
                                <AprItem
                                    loading={isLoading}
                                    title={item.status == 3 ? 'Duyệt vào ca' : 'Vào ca'}
                                    valueHours={valueHoursIn}
                                    valueMin={valueMinIn}
                                    time={timeIn}
                                    status={item.status}
                                    appTime={appTimeIn}
                                    setTimeState={st => this.setState({ timeIn: true })}
                                />

                                <View style={[styles.conver, { paddingRight: isLoading ? 0 : 15 }]}>
                                    <Text style={{ fontSize: 16, color: settingApp.colorText }}>Đi trễ</Text>
                                    {isLoading ? this.renderLoad() :
                                        <TouchableOpacity
                                            onPress={() => {
                                                item.status == 3 ? null :
                                                    this.setState({ isLate: !this.state.isLate })
                                            }}
                                        >
                                            <Ionicons name={isLate ? 'ios-checkmark-circle' : 'ios-checkmark-circle-outline'} size={23} color={isLate ? '#21469B' : '#8E8E93'} />
                                        </TouchableOpacity>
                                    }

                                </View>

                                <AprItem
                                    loading={isLoading}
                                    title={item.status == 3 ? 'Duyệt kết thúc ca' : 'Kết thúc ca'}
                                    valueHours={valueHoursOut}
                                    valueMin={valueMinOut}
                                    time={timeOut}
                                    status={item.status}
                                    appTime={appTimeOut}
                                    setTimeState={st => this.setState({ timeOut: true })}
                                />

                                <View style={[styles.conver, { paddingRight: isLoading ? 0 : 15 }]}>
                                    <Text style={{ fontSize: 16, color: settingApp.colorText, }}>Về sớm</Text>

                                    {isLoading ? this.renderLoad() :
                                        <TouchableOpacity
                                            onPress={() => {
                                                item.status == 3 ? null :
                                                    this.setState({ isEarly: !this.state.isEarly })
                                            }}
                                        >
                                            <Ionicons name={isEarly ? 'ios-checkmark-circle' : 'ios-checkmark-circle-outline'} size={23} color={isEarly ? '#21469B' : '#8E8E93'} />
                                        </TouchableOpacity>
                                    }

                                </View>

                                <View style={[styles.conver, { paddingRight: isLoading ? 0 : 15 }]}>
                                    <Text style={{ fontSize: 16, color: settingApp.colorText }}>Tổng giờ công duyệt</Text>
                                    {
                                        isLoading ? this.renderLoad() :
                                            <View style={{ borderRadius: 10, backgroundColor: item.approvedWorkingHours >= item.totalHour ? '#4CD964' : '#FF3B30', height:20, alignItems:'center', justifyContent:'center' }}>
                                                <Text style={{ fontSize: 14, color: '#FFFFFF', paddingLeft: 5, paddingRight: 5, borderRadius: 10 }}>
                                                    {(totalHours || totalMin) ?
                                                        totalHours + 'h' + totalMin :
                                                        (Math.floor(item.approvedWorkingHours)) + 'h' + Math.ceil((item.approvedWorkingHours - (Math.floor(item.approvedWorkingHours))) * 60)}
                                                </Text>
                                            </View>
                                    }


                                </View>

                                {item.status == 3 ? <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: 15, paddingBottom: 10, paddingTop: 10, paddingRight: 15 }}>
                                    <Text style={{ fontSize: 16, color: settingApp.colorText }}>Ngày duyệt</Text>
                                    {isLoading ? this.renderLoad() :
                                        <Text style={{ fontSize: 16, color: '#8E8E93' }}>{appDate} - {appTimeDate}</Text>
                                    }
                                </View>
                                    : null}

                                {item.approverNote ?
                                    <View>
                                        <LineItem
                                            loading={isLoading}
                                            valueTop={'Ghi chú người duyệt'}
                                            valueBot1={item.approverNote}
                                        />
                                    </View>
                                    : null}

                            </View>

                            <View style={{ height: 150, marginTop: item.status == 2 || item.status == 1 ? 0 : 15, ...settingApp.shadow }}>
                                {this.renderButton(item.status)}
                            </View>
                        </ScrollView>

                        <ModalButton
                            modalVisible={this.state.modalVisible}
                            close={() => this.setState({ modalVisible: false })}
                            data={item}
                            statusApr={this.state.statusApr}
                            timeKeeping={status => this.timeKeeping(status)}
                        />
                    </KeyboardAvoidingView>
                )
            }
            else {
                return (
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <NotFound />
                    </View>
                )
            }
        }
    }

    render() {
        const { isLoad } = this.state;
        return (
            <View style={{ flex: 1 }}>
                {this.renderHeader()}
                {this.renderContent()}
                {
                    isLoad && this.renderIsLoad()

                }
                <TimePicker
                    value={this.timeIn}
                    validate={(hour, minute) => this.validate('timeIn', hour, minute)}
                    visible={this.state.timeIn}
                    close={() => this.setState({ timeIn: false })}
                />

                <TimePicker
                    value={this.timeOut}
                    validate={(hour, minute) => this.validate('timeOut', hour, minute)}
                    visible={this.state.timeOut}
                    close={() => this.setState({ timeOut: false })}
                />

            </View>
        )
    }
}
const styles = {
    conver: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: 15,
        paddingBottom: 10,
        paddingTop: 10,
        borderBottomColor: '#C8C7CC',
        borderBottomWidth: 1,
        height:55,
        alignItems:'center'
    }
}
export default Layout;