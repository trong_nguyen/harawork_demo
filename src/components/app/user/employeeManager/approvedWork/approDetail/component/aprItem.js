import React, { Component } from 'react';
import { View, Text, TouchableOpacity, KeyboardAvoidingView, ScrollView, TextInput, ActivityIndicator, Image } from 'react-native';
import { settingApp } from '../../../../../../../public';
import { MaterialIcons, Ionicons } from '@expo/vector-icons';

class AprItem extends Component {
    constructor(props) {
        super(props);
    }
    renderLoad() {
        return (
            <View style={{
                left: 15,
                backgroundColor: 'transparent',
                paddingRight: 15,
            }}>
                <ActivityIndicator size='small' color={settingApp.color} />
            </View>
        )
    }

    render() {
        let { status, time, appTime, valueHours, valueMin, title, loading } = this.props
        time = (valueHours && valueMin) ? valueHours + ':' + valueMin : (time ? time :'Không lấy được dữ liệu')
        title = loading ? 'Đang lấy dữ liệu...' :( title ? title : 'Không lấy được dữ liệu')
        return (
            <View style={{ 
                height:55 ,
                flexDirection: 'row', 
                justifyContent: 'space-between', 
                marginLeft: 15, 
                paddingRight: 15, 
                borderBottomColor: '#C8C7CC', 
                borderBottomWidth: 1,
                alignItems:'center'
                 }}
                >
                <Text style={{ fontSize: 16, color: loading ? '#8E8E93' : settingApp.colorText }}>{title}</Text>
                {
                    loading ? this.renderLoad() :
                        <TouchableOpacity
                            activeOpacity={1}
                            style={{ flexDirection: 'row', justifyContent:'center' }}
                            onPress={() => {
                                status == 3 ? null :
                                    this.props.setTimeState(true)
                            }}
                        >
                            <Text style={{ fontSize: 16, color: '#8E8E93' }}>{status == 3 ? appTime : time}</Text>
                            {status == 3 ? null :
                                <MaterialIcons
                                    name="keyboard-arrow-right"
                                    size={20}
                                    color="#D1D1D6"
                                />
                            }
                        </TouchableOpacity>
                }

            </View>
        )
    }
}
export default AprItem;