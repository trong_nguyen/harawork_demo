import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, ActivityIndicator, Image } from 'react-native';
import { settingApp } from '../../../../../../../public';
import { MaterialIcons, Ionicons } from '@expo/vector-icons';

class LineItem extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }
    renderLoad() {
        return (
            <View style={{
                width: 50,
                height: 30,
                paddingRight: 15,
                backgroundColor: 'transparent',
            }}>
                <ActivityIndicator size='small' color={settingApp.color} />
            </View>
        )
    }

    render() {
        let { valueTop, valueTop2, valueBot1, valueBot2, checkIP, loading, noneBorder } = this.props;
        valueTop = loading ? 'Đang lấy dữ liệu...' : (valueTop ? valueTop : 'Không lấy được dữ liệu')
        valueTop2 = valueTop2 ? '- ' + valueTop2 : ''
        valueBot1 = valueBot1 ? valueBot1 : 'Không lấy được dữ liệu'
        valueBot2 = valueBot2 ? '- ' + valueBot2 : ''
        checkIP = checkIP ? checkIP : null
        let checkDirex = 'Ngày duyệt'
        return (
            <View style={{
                flex: 1,
                marginLeft: 15,
                flexDirection: valueTop === checkDirex ? 'row' : 'column',
                height: checkIP ? 75 : 55,
                paddingRight: 15,
                justifyContent:'center',
            }}>
                <Text
                    numberOfLines={1}
                    style={{ fontSize: 16, color: loading ? '#8E8E93' : settingApp.colorText }}>{valueTop} {valueTop2}</Text>
                {
                    loading ? this.renderLoad() :                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                        <View>
                            <Text
                                numberOfLines={1}
                                style={{ fontSize: 16, color: '#8E8E93', paddingRight: valueTop === checkDirex ? (loading ? 0 : 15) : 0 }}>{valueBot1} {valueBot2}</Text>
                            {checkIP ?
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <MaterialIcons name='error' size={15} color='#FF3B30' />
                                    <Text style={{ fontSize: 16, color: '#FF3B30', marginLeft: 5 }}>{checkIP}</Text>
                                </View>
                                : null}
                        </View>
                }

            </View>
        )
    }
}
export default LineItem;
