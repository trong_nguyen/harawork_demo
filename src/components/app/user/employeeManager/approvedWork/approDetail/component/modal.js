import React, { Component } from 'react';
import {Text, View, TouchableOpacity, Modal, TextInput, } from 'react-native';
import { settingApp } from '../../../../../../../public';
import { MaterialIcons, Ionicons } from '@expo/vector-icons';

class ModalButon extends Component{
    constructor(props){
        super(props);
        this.state={
          
        }
    }

    render() {
        const {  width, height } = settingApp
        const {item, statusApr , timeKeeping} = this.props;
        return (
            <Modal
                animationType='fade'
                visible={this.props.modalVisible}
                transparent={true}
                onRequestClose={() => this.props.close()}
            >
            
                <View style={{flex: 1, justifyContent:'center', alignItems:'center', backgroundColor:'rgba(4, 4, 15, 0.5)'}}>
                <View style={{ width: (width - 60), height: (height * 0.3), backgroundColor: '#FFFFFF', borderRadius:10, justifyContent:'space-between', flexDirection:'column'}}>
                    <View style={{alignItems:'center', marginTop:10}}>
                        <Text style={{color: settingApp.colorText, fontSize:18, fontWeight:'bold'}}>Thông báo </Text>
                        
                    </View>
                    <View style={{alignItems:'center', marginBottom:20, justifyContent:'center'}}>
                        <Text style={{color: settingApp.colorText, fontSize:16, width:'80%', alignItems:'center'}}
                        >Bạn có chắc chắn {statusApr ? 'muốn duyệt' : 'muốn từ chối'} phiếu chấm công này?</Text>
                    </View>
                      
                    <View style={{ height:50,flexDirection:'row', justifyContent:'space-between', borderTopColor:'#C8C7CC', borderTopWidth:0.5}}>
                        <TouchableOpacity
                            onPress={()=> this.props.close()}
                            style={{ width:'50%',alignItems:'center', justifyContent:'center', borderRightWidth:0.5, borderRightColor:'#C8C7CC'}}
                        >
                            <Text style={{color:'#21469B', fontSize:16}}> Đóng </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={()=> {
                                this.props.close()
                                this.props.timeKeeping(statusApr ? 3 : 4)
                                }}
                            style={{alignItems:'center', justifyContent:'center',  width:'50%'}}
                        >
                            <Text style={{color: statusApr ? '#4CD964' : '#FF3B30', fontSize:16 }}>{statusApr ? 'Duyệt': 'Từ chối'}</Text>
                        </TouchableOpacity>
                    </View>
                    </View>
                </View>
            </Modal>
        )
    }
}
export default ModalButon;