import React, { Component } from 'react';
import { View, Text, TouchableOpacity, KeyboardAvoidingView, ScrollView, TextInput, ActivityIndicator, Image, Alert } from 'react-native';
import { settingApp, HeaderWapper, Utils, Loading, NotFound } from '../../../../../../../public';
import { MaterialIcons, Ionicons } from '@expo/vector-icons';
import LineItem from './lineItem';
import moment from 'moment';

class RequestApr extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    aler(data, status, detail){
        Alert.alert(
            'Thông báo',
            `Bạn có chắc chắn muốn ${detail}\n yêu cầu tạo công này không?`,
            [   
                { text: 'Đóng', onPress: () => console.log('OK Pressed') },
                { text: 'Xác nhận', onPress: () => {
                    status === 1 ?
                    this.props.delete(data) : 
                    this.goCreate(data)
                 }},
            ],
            { cancelable: false }
        )
    }

    goCreate(data){
        this.props.navigation.navigate('CreateNewWorks',{
            actions: isLoad => this.setState({isLoad:isLoad}),
            data: data,
            backContent: (status => this.props.backContent(status))
        })
    }  
    
    renderButton(){
        const { data } =  this.props
        return(
            <View style={{
                width: settingApp.width,
                height: 90,
                backgroundColor: '#FFFFFF',
                justifyContent: 'space-between',
                flexDirection: 'column',
                bottom:0,
                ...settingApp.shadow
            }}>
                <View style={{
                    flexDirection: 'row', width:settingApp.width,
                    justifyContent: 'space-between', paddingBottom: 5, paddingTop:10
                }}>
                    <TouchableOpacity
                        onPress={ () => this.aler(data, 1, 'từ chối')}
                        style={{ backgroundColor: '#FF3B30', borderRadius: 5, height: 60, width: '45%', marginLeft: 10, alignItems: 'center', justifyContent: 'center' }}
                    >
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#FFFFFF' }}>Từ chối</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={ () => this.aler(data, 2, 'tạo')}
                        style={{ backgroundColor: '#4CD964', borderRadius: 5, height: 60, width: '45%', marginRight: 10, alignItems: 'center', justifyContent: 'center' }}
                    >
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#FFFFFF' }}>Đồng ý</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    render() {
        const { data } = this.props;
        const requestData = JSON.parse(data.requestData)
        const time = requestData.checkinTime;
        const isLoading= false
        return (
            <View style={{flex:1}}>
            <ScrollView>
                <View style={{
                    flex: 1,
                    backgroundColor: '#FFFFFF',
                    marginTop: 15,
                    ...settingApp.shadow
                }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginLeft: 10, paddingBottom: 15, paddingTop: 15 }}>
                        <Text style={{ fontSize: 16, color: settingApp.colorText }}> Trạng thái </Text>
                        <View style={{ flexDirection: 'row', marginRight: 10 }}>
                            <Text style={{ fontSize: 16, color: '#FF9500', marginRight: 3 }}>Chờ duyệt / </Text>
                            <Text style={{ fontSize: 16, color: '#21469B' }}>
                                [ {time} ]</Text>
                        </View>
                    </View>
                </View>

                <View style={{
                    flex: 1,
                    backgroundColor: '#FFFFFF',
                    marginTop: 15,
                    ...settingApp.shadow
                }}>
                    <LineItem
                        loading={isLoading}
                        valueTop={data.requester.fullName}
                        valueTop2={data.requester.userId}
                        valueBot1={data.requester.jobtitleName}
                        valueBot2={data.requester.departmentName}
                    />

                    <LineItem
                        loading={isLoading}
                        valueTop={'Ngày làm việc'}
                        valueBot1={Utils.formatTime(data.requestApproveDate, 'DD/MM/YYYY', true)}
                    />

                    <LineItem
                        loading={isLoading}
                        valueTop={'Ca làm việc'}
                        valueBot1={data.requestShiftName}
                    />
                    <LineItem
                        loading={isLoading}
                        valueTop={'Giờ chấm vào'}
                        valueBot1={requestData.checkinTime}
                    />
                    <LineItem
                        loading={isLoading}
                        valueTop={'Giờ chấm ra'}
                        valueBot1={requestData.checkoutTime}
                    />

                    <LineItem
                        loading={isLoading}
                        valueTop={'Ghi chú'}
                        valueBot1={data.requestNote}
                    />
                </View>

            </ScrollView>
                {this.renderButton()}
            </View>
        )
    }
}
export default RequestApr;