import React, { Component } from 'react';
import { View, Text, TouchableOpacity, FlatList, ScrollView } from 'react-native';
import { Utils, settingApp, Loading, NotFound } from '../../../../../../public';
import lodash from 'lodash';
import ApprovedList from '../worksList';
import { MaterialIcons, Ionicons } from '@expo/vector-icons';
import moment from 'moment';

class Layout extends Component {

    settupWeed(item) {
        const weekday = new Array(7)
        weekday[0] = "Chủ nhật";
        weekday[1] = "Thứ 2";
        weekday[2] = "Thứ 3";
        weekday[3] = "Thứ 4";
        weekday[4] = "Thứ 5";
        weekday[5] = "Thứ 6";
        weekday[6] = "Thứ 7";
        return weekday[item]
    }

    render() {
        const { list, listFrames, isLoading, refreshList } = this.state;
        if(isLoading) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Loading />
                </View>
            )
        }
        else {
            if (listFrames && listFrames.length > 0) {
                const date = Utils.formatTime((new Date()), 'DD', true);
                return (
                    <View style={{flex:1, backgroundColor:'#FFFFFF'}}>
                        <ScrollView
                            horizontal={true}
                            >
                            {
                                listFrames.map((item, index) => {
                                    if (item && !item.id) {
                                        let localDay  = moment.utc(item.fullDate).local().format('d')
                                        let localDate  = moment.utc(item.fullDate).local().format('L').slice(0,2)
                                        let day = this.settupWeed(localDay)
                                        return (
                                            <View
                                                key={index}
                                                style={{ maxWidth:160 ,padding:10, backgroundColor:'#FFFFFF' }}
                                            >
                                                <View style={{ flexDirection: 'column' , flex: 1}}>
                                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                        <Text style={{ fontSize: 18, color: localDate === date ? '#21469B' : settingApp.colorText, fontWeight:'bold' }}>{localDate}</Text>
                                                        <Text style={{ fontSize: 16, color: localDate === date ? '#21469B' : '#8E8E93' }}>{'/ ' + day}</Text>
                                                    </View>
                                                    {this.renderItem(item, listFrames, list, refreshList)}
                                                </View>
                                            </View>
                                        );
                                    }
                                })
                            }
                        </ScrollView>
                        </View>
                )
            }
            else {
                return (
                    <NotFound />
                )
            }
        }
    }

    goList(item, list, colleague, name, refreshList) {
        this.navigation.navigate('ApprovedList', {
            item: item, 
            data: list, 
            colleague: colleague, 
            name: name,
            actions: (refresh => this.setRefresh(refresh)),
            setRefresh:(refreshList => this.setState({refreshList:refreshList})),
            delete:(data => this.delete(data))
     })
    }
    renderItem(item, data, list, refreshList) {
        const { colleague, totalFrames } = this;
        return (
            <ScrollView
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                style={{ flexDirection: 'column', marginTop: 10 }}>
                {
                    data.map((e, i) => {
                    this.totalFrames = []
                        if (e.id && e.date === item.checkin) {
                            return (
                                <View
                                    key={i}
                                    style={{ flex:1}}
                                >
                                    <TouchableOpacity
                                        onPress={() => this.goList(item, list, colleague, e.name, refreshList)}
                                        style={{ backgroundColor: 'rgba(200, 199, 204, 0.1)', marginBottom: 10, borderRadius: 5, flexDirection: 'column', padding: 10, ...settingApp.shadow}}
                                    >   
                                        {this.renderTotal(item, list, e.name)}
                                        <Text
                                            numberOfLines={1}
                                            style={{ color: settingApp.colorText, fontSize: 18, paddingRight:10 }}
                                        > {e.name}</Text>
                                        <Text
                                            numberOfLines={1}
                                            style={{ color: settingApp.colorText, paddingLeft: 5, fontSize: 16 }}
                                        >
                                            {e.timeframes.length > 0 ? (e.timeframes[0].checkInTime) + ' - ' : '--/'}
                                            {e.timeframes.length > 0 ? (e.timeframes[0].checkOutTime) : '--/--'}
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            )
                        }
                    })
                }
            </ScrollView>
        )
    }

    renderTotal(item, list, name){
        const total =  this.checkLength(item, list, name);
        return(
            <View
                style={{
                    borderRadius: 20, 
                    backgroundColor:'#C8C7CC', 
                    alignItems:'center', 
                    justifyContent:'center',
                    height: 20, 
                    width:25,
                    position:'absolute',
                    top:3,
                    right:3
                    }}
            >
                <Text style={{color:'#FFFFFF', fontSize:14}}>{total}</Text>
            </View>
        )
    }
}
export default Layout;