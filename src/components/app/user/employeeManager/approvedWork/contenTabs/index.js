import Layout from './layout';
import { HaravanHr, HaravanIc } from '../../../../../../Haravan';
import { Utils } from '../../../../../../public';
import lodash from 'lodash';
import actions from '../../../../../../state/action';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class ContentTabs extends Layout {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            listFrames: [],
            isLoading: true,
            refreshList: true,
            totalFrames: [],
        }
        this.totalFrames= []

        this.isLoading = true
        const date = new Date()
        const currenDate = date.getDay();
        let dayStart = null;
        let dayEnd = 7;

        if (currenDate == 0) {
            dayStart = date.getDate() - 6;
            dayEnd = date.getDate()
        }
        else {
            if (currenDate == 1) {
                dayStart = date.getDate();
                dayEnd = date.getDate() + (7 - currenDate)
            }
            else {
                dayStart = date.getDate() - (currenDate - 1);
                dayEnd = date.getDate() + (7 - currenDate)
            }

        }
        this.colleague = props.app.colleague;
        this.isLoad = props.isLoad;

        this.Mon = new Date(date.getFullYear(), date.getMonth(), dayStart);
        this.Sun = new Date(date.getFullYear(), date.getMonth(), dayEnd);

        this.monDay = this.Mon;
        this.sunDay = this.Sun;

        this.idDepartmenet = '';
        this.idJod = '';
        this.idEmploy = '';

        this.page = 1;
        this.totalPage = null;
        this.currenList = [];
        this.listFrames = [];
        this.navigation = this.props.navigation;

        this.condition = props.condition;
        this.refreshList = props.aprov.refreshList;
    }

    componentDidMount() {
        this.loadData()
    }

    setRefresh(status){
        if(status === true ){
            this.props.actions.aprov.reload({status: true, list: []})
            this.setState({ refreshList: true, isLoading: true }, () =>{
                this.setRefreshData();
                this.loadData();
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.condition != this.props.condition && nextProps.condition !== null) {
            const { condition } = nextProps;
            if (condition !== null) {
                this.idDepartmenet = condition.idDepartment ? condition.idDepartment : this.idDepartmenet,
                this.idJod = condition.idJodli ? condition.idJodli : this.idJod
                this.idEmploy = condition.employID ? condition.employID : this.idEmploy
                this.monDay = condition.valueFromDate ? condition.valueFromDate : this.monDay
                this.sunDay = condition.valueTodate ? condition.valueTodate : this.sunDay
            }
            this.page = 1;
            this.totalPage = null;
            this.currenList = [];
            this.setState({ isLoading: true }, () => this.loadData())
        }
        if (nextProps.condition === null && nextProps.condition !== this.props.condition) {
            this.setRefreshData()
            this.setState({ isLoading: true }, () => this.loadData())
        }

        if (nextProps && nextProps.isLoad !== this.props.isLoad) {
            if (nextProps.isLoad == true) {
                this.setState({ isLoading: true }, () => {
                    this.setRefreshData()
                    this.loadData()
                })
            }
            else {
                this.setState({ isLoading: false })
            }
        }
        if( nextProps.aprov.refreshList && nextProps.aprov.refreshList != this.props.aprov.refreshList){
            this.setState({ isLoading: true }, () => {
                this.setRefreshData()
                this.loadData()
            })
        }
    }

    checkLength(item, list, name){
        const { totalFrames } = this;
        list.map(e=>{
                if(e.shift && e.shift.date === item.checkin && e.shift.name === name){
                    totalFrames.push(e)
                }
            else{
                if( ((Utils.formatTime(e.requestApproveDate, 'DD/MM/YYYY', true)) === item.checkin)  && (e.requestShiftName && e.requestShiftName === name)){
                    totalFrames.push(e)
                }
            }
        })
        return this.totalFrames.length;
    }

    delete(data){
        const {list} = this.state
        const index = list.findIndex(e => e.id === data.id)
        if(index > -1){
            list.splice(index , 1)
        }
        this.setState({list})
    }

    setRefreshData() {
        this.monDay = this.Mon;
        this.sunDay = this.Sun;

        this.idDepartmenet = ''
        this.idJod = '';
        this.idEmploy = '';

        this.page = 1;
        this.totalPage = null;
        this.currenList = [];
        this.listFrames = [];
    }

    async loadData() {
        const { page, totalPage, currenList, monDay, sunDay, idDepartmenet, idJod, idEmploy } = this;
        const toDate = Utils.formatTime(sunDay, 'YYYY-MM-DD', true);
        const fromDate = Utils.formatTime(monDay, 'YYYY-MM-DD', true)

        const newList = [];
        const listRequest = [];
        const result = await HaravanHr.getListFramesWorks(idEmploy, idDepartmenet, idJod, fromDate, toDate)
        const requestLeaves = await HaravanHr.getRequetsLeave(this.colleague.mainPosition.departmentId , fromDate, toDate)
        if (result && result.data && !result.error) {
            const count = result.data.totalCount
            result.data.data.map(item => {
                if (newList.length === 0) {
                    newList.push(item);
                }
                else {
                    const indexList = newList.findIndex(e => e.employeeId === item.employeeId)
                    if (indexList > -1) {
                        newList.splice(indexList, 1)
                    }
                    else {
                        newList.push(item)
                    }
                }
            })
           

            let groupDate = [];
            let date = [];
            let frames = [];
            newList.map(item => {
                if (item.timekeepingApproves && item.timekeepingApproves.length > 0) {
                    item.timekeepingApproves.map(e => {
                        frames.push(e)
                    })
                }
            })
            if(requestLeaves && requestLeaves.data && !requestLeaves.error){
                requestLeaves.data.map((e,i) => {
                    e = {...e, statusRequest:1, checkin: e.requestApproveDate, shift:e.requestShift}
                    frames.push(e)
                })
            }
            const listFill = frames.sort((a, b) => {
                return new Date(a.checkin) - new Date(b.checkin)
            })
            listFill.map(e => {
                let checkin = Utils.formatTime(e.checkin, 'DD/MM/YYYY', true)
                let status = e.statusRequest ? e.statusRequest : 0
                e.shift = { ...e.shift, date: checkin, statusRequest: status }
                if (date.length == 0) {
                    date.push({ checkin, fullDate: e.checkin, day: e.dateFomatFull }, e.shift)
                }
                else {
                    const indexDate = date.findIndex(m => m.checkin === checkin)
                    if (indexDate < 0) {
                        date.push({ checkin, fullDate: e.checkin, day: e.dateFomatFull }, e.shift)
                    } else {
                        date.push(e.shift)
                    }
                }
            });
            date.map((a, i) => {
                if (groupDate.length === 0 && date[i + 1].name) {
                    groupDate.push(a)
                }
                else {
                    if (!a.id) {
                        const index = groupDate.findIndex(n => n === a.checkin)
                        if (index < 0) {
                            groupDate.push(a)
                        }
                    }
                    else {
                        const indexDate = groupDate.findIndex(n => n.date === a.date && n.name === a.name)
                        if(indexDate < 0){
                            groupDate.push(a)
                        }
                    }
                }
            })  
            this.page = page + 1;
            this.setState({ list: listFill, listFrames: groupDate, isLoading: false, refreshList: false })
            this.props.setReload(null)
            this.props.finishLoad(false)
            this.props.actions.aprov.reload({status: false, list: this.state.list})
        }
        else {
            this.setState({ isLoading: false, refreshList: false })
            this.props.setReload(null)
            this.props.finishLoad(false)
            this.props.actions.aprov.reload({status: false, list: []})
        }
    }

    componentWillUnmount(){
        this.props.actions.aprov.reload(null)
    }
}
mapStateToProps = state => ({ ...state });
mapDispatchToProps = dispatch => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch)
    }
    return { actions: acts }
}
export default connect(mapStateToProps, mapDispatchToProps)(ContentTabs);