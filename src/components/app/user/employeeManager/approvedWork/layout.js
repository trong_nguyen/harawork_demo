import React, { Component } from 'react';
import { Text, View, TouchableOpacity, FlatList } from 'react-native';
import { ButtonCustom, settingApp, imgApp, Loading, HeaderWapper, Utils, Arrow, ScrollView} from '../../../../../public';
import HeaderIndex from '../../../../../public/components/headerIndex';
import { Ionicons, EvilIcons,Feather } from '@expo/vector-icons';
import Collapsible from 'react-native-collapsible';
import Condition from './condition';
import ContentTabs from './contenTabs';

class Layout extends Component {

    renderHeader() {
        const { isLoading } = this.state;
        return (
            <HeaderIndex
                title={
                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                    >
                        <Text style={settingApp.styleTitle}>
                            Duyệt công
                        </Text>
                    </View>
                }
                buttonRight={
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('CreateNewWorks',{
                            actions: isLoad => this.setState({isLoad:isLoad}),
                            backContent:(status => this.setState({isBack:status}))
                        })}
                        style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
                    >
                    {
                        <Feather name='plus' size={23} color="#ffffff" />
                    }
                    </TouchableOpacity>
                }
            />
        )
    }

    renderButton() {
        const { isColap } = this.state;
        return (
            <View style={{ flexDirection: 'row', paddingLeft: 10, paddingRight: 10 }}>
                <View style={{ flex: 1 }}>
                    {this.state.removeFilter && 
                    <TouchableOpacity
                        onPress={ () => {
                            this.setState({ isColap: false})
                            this.condition.refreshFillter()
                            } }
                        style={{ flexDirection: 'row', alignItems: 'center', height: 50 }}>
                        <EvilIcons name='close' color='#FF3B30' size={30} />
                        <Text style={{ color: '#FF3B30', fontSize: 16, marginLeft: 5 }}>Đặt lại bộ lọc</Text>
                    </TouchableOpacity>}
                </View>
                <View style={{ flex: 1, alignItems: 'flex-end' }}>
                    <TouchableOpacity
                        onPress={() => this.setState({ isColap: !this.state.isColap})}
                        style={{ flexDirection: 'row', alignItems: 'center', height: 50 }}>
                        <Text style={{ color: settingApp.color, fontSize: 16, marginRight: 5 }}>
                            {!isColap ? 'Bộ lọc nâng cao' : 'Thu gọn bộ lọc'}
                        </Text>
                        <View>
                            <Arrow isOpen={isColap} color={settingApp.color} />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    renderContent(){
        const {isColap} = this.state;
        return(
            <View style={{flex: 1}}>
            {this.renderButton()}
                <Collapsible collapsed={!isColap} style={{paddingBottom:1, paddingTop:1}} >
                    <Condition 
                        ref={refs => this.condition = refs}
                        setCondition={
                            condition => this.setState({ isColap: false },
                                () => this.setState({ condition }))
                        }
                        removeFilter = {this.state.removeFilter}
                        setRemoveFilter = {removeFilter => this.setState({removeFilter})}
                    />
                </Collapsible>
                <ContentTabs 
                    ref={refs => this.contentTabs = refs}
                    navigation ={ this.props.navigation}
                    condition = {this.state.condition}
                    isLoad = { this.state.isLoad }
                    setReload = {(isLoad) => this.setState({isLoad: isLoad})}
                    finishLoad = {(isLoading) => this.setState({ isLoading: isLoading})}
                />
            </View>
        )
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {this.renderHeader()}
                {this.renderContent()}
            </View>
        )
    }

}
export default Layout