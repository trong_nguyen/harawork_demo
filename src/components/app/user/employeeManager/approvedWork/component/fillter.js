import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {settingApp} from '../../../../../../public';
class Fillter extends Component {

    render() {
        const { name, value, noneBorder } = this.props;
        const { colorSperator } = settingApp;
        return (
            <View style={{
                flex: 1,
                flexDirection: 'row',
                height: 55,
                paddingRight: 15,
                borderTopColor: colorSperator,
                borderTopWidth: noneBorder ? 0 : 1
            }}>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    {name}
                </View>
                <View style={{ flex: 1 }}>
                    {value}
                </View>
            </View>
        )
    }
}

export default Fillter;