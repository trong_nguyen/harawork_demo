import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import Header from './components/header';
import Calendar from './components/calendar';
import { SvgCP, Loading } from '../../../../public';

class Layout extends Component {

    render() {
        const { selectList, isLoading } = this.state
        return (
            <View style={{ flex: 1 }}>
                <Header />
                <View>
                    <Calendar
                        ref={refs => this.calendar = refs}
                        startingDate={this.state.weekStart}
                        onWeekChanged={weekStart => {
                            this.onWeekChanged(weekStart);
                            this.setState({ weekStart });
                        }}
                        onDateSelected={date => this.onDateSelected(date)}
                        style={{ height: 125, paddingBottom: 10 }}
                        leftSelector={<SvgCP.arrowLeftCalendar />}
                        rightSelector={<SvgCP.arrowRightCalendar />}
                        iconContainer={{ flex: 0.15 }}
                        markedDates={this.state.markedDates}
                        daySelectionAnimation={{ type: 'background', highlightColor: '#21469B', }}
                    />

                    <View style={{ position: 'absolute', top: 0, right: 0, height: 42 }}>
                        <TouchableOpacity
                            onPress={() => this.initialWeek()}
                            style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', paddingRight: 10 }}>
                            <Text style={{ fontSize: 15, color: '#8E8E93', marginRight: 5 }}>Hôm nay</Text>
                            <SvgCP.calendar day={this.toDay} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flex: 1, borderTopColor: '#C8C7CC', borderTopWidth: 1, backgroundColor: '#ffffff' }}>
                    <ScrollView contentContainerStyle={{ padding: 10 }}>
                        {selectList.map((e, i) => {
                            return (
                                <View style={{ flex: 1 }} key={i}>
                                    {e}
                                </View>
                            )
                        })}
                    </ScrollView>
                    {isLoading && <Loading />}
                </View>
            </View>
        )
    }
}

export default Layout;