import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';

import Layout from './layout';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { HaravanHr } from '../../../../Haravan';
import { Utils, settingApp, imgApp, SvgCP } from '../../../../public';
import { MaterialIcons, Ionicons } from '@expo/vector-icons';

class CalendarJob extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            weekStart: moment(),
            markedDates: [],
            selectList: [],
            listIp: [],
            isLoading: true,
            checkInTrace: false
        }
        this.colleague = props.app.colleague;
        this.list = [];
        this.selectDay = moment().format('YYYY-MM-DD');

        this.dots = [{ color: '#21469B', selectedDotColor: '#ffffff' }];

        this.from = moment().subtract((7 * 4), 'days').format('YYYY-MM-DD');
        this.to = moment().add((7 * 4), 'days').format('YYYY-MM-DD');

        this.toDay = moment().date();

        this.styles = { flex: 1, borderRadius: 3, height: 50, marginBottom: 10, justifyContent: 'center', paddingLeft: 15, paddingRight: 15 }
    }

    componentDidMount() {
        this.loadIp();
        this.loadData();
    }

    async loadIp() {
        let { listIp  } = this.state
        const resDepartIP = await HaravanHr.getIpDepartment(this.colleague.mainPosition.departmentId);
        if (resDepartIP && resDepartIP.data && !resDepartIP.error) {
            if (resDepartIP.data) {
                resDepartIP.data.map(e => {
                    e.ipAddresses.map(m => {
                        listIp.push(m)
                    })
                })
                this.setState({ listIp })
            }
        }
    }

    checkIP(item) {
        const { listIp } = this.state;
        if (item.checkinTrace !== null) {
            const indexIn = listIp.findIndex(e => e === item.checkInTrace.ipAddress)
            if (indexIn < 0) {
                this.setState({ checkInTrace: true })
            }
            else {
                this.setState({ checkInTrace: false })
            }
        }
        else {
            this.setState({ checkInTrace: false })
        }
        return this.state.checkInTrace
    }

    timeRange(from, to) {
        startDate = new Date(from);
        endDate = new Date(to);
        var dates = [],
            currentDate = startDate,
            addDays = function (days) {
                var date = new Date(this.valueOf());
                date.setDate(date.getDate() + days);
                return date;
            };
        while (currentDate <= endDate) {
            dates.push(currentDate);
            currentDate = addDays.call(currentDate, 1);
        }
        return dates;
    }

    async loadData(type, from, to) {
        from = from || this.from;
        to = to || this.to;
        const timeRage = this.timeRange(from, to);
        const result = await HaravanHr.gettimeworking(from, to);
        if (result && result.data && !result.error) {
            const { data } = result;
            const list = [];
            const markedDates = [];
            for (let i = 0; i < timeRage.length; i++) {
                const day = moment(timeRage[i]).format('YYYY-MM-DD');
                let listObj = [];
                for (let item in data) {
                    const listItem = data[item];
                    switch (item) {
                        case 'employeeLeaveFulls':
                            listObj = [...listObj, ...this.employeeLeaveFulls(day, listItem)];
                            break;
                        case 'employeeShiftDistributions':
                            listObj = [...listObj, ...this.employeeShiftDistributions(day, listItem)];
                            break;
                        case 'employeeTimekeepings':
                            listObj = [...listObj, ...this.employeeTimekeepings(day, listItem)];
                            break;
                        case 'holidays':
                            listObj = [...listObj, ...this.holidays(day, listItem)];
                            break;
                        case 'requestCreateTimekeeping':
                            listObj = [...listObj, ...this.requestCreateTimekeeping(day, listItem)];
                            break;
                        default:
                            break;
                    }
                }
                if (listObj && listObj.length > 0) {
                    list.push({ day, data: listObj });
                    markedDates.push({
                        date: day,
                        dots: [{ key: day, color: '#21469B', selectedDotColor: '#ffffff' }],
                    });
                }
            }
            if (type === 'up') {
                this.list = [...list, ...this.list];
                this.setState({ markedDates: [...this.state.markedDates, ...markedDates] });
            } else if (type === 'down') {
                this.list = [...this.list, ...list];
                this.setState({ markedDates: [...markedDates, ...this.state.markedDates,] });
            } else {
                this.list = list;
                const index = list.findIndex(e => e.day === this.selectDay);
                if (index > -1) {
                    this.setState({ markedDates, selectList: [...list[index].data], isLoading: false });
                } else {
                    this.setState({ markedDates, isLoading: false });
                }
            }


        }
    }

    onWeekChanged(weekStart) {
        const { from, to } = this;
        const time = moment(weekStart).format('YYYY-MM-DD')
        const prv = moment(from).add((7 * 2), 'days').startOf('isoweek').format('YYYY-MM-DD');
        const next = moment(to).subtract((7 * 2), 'days').startOf('isoweek').format('YYYY-MM-DD');
        if (time === prv) {
            const prvTo = from;
            const prvFrom = moment(prvTo).subtract((7 * 4), 'days').format('YYYY-MM-DD');
            this.from = prvFrom;
            this.loadData('up', prvFrom, prvTo);
        } else if (time === next) {
            const nextTo = moment(to).add((7 * 4), 'days').format('YYYY-MM-DD');
            const nextForm = to
            this.to = nextTo;
            this.loadData('down', nextForm, nextTo)
        }
    }

    employeeLeaveFulls(day, listItem) {
        const result = [];
        listItem.map(e => {
            const to = moment(e.to).format('YYYY-MM-DD');
            const from = moment(e.from).format('YYYY-MM-DD');
            const formatTo = new Date(to);
            const formatFrom = new Date(from);
            const formatDay = new Date(day);
            if (day === from || day === to || (formatFrom <= formatDay && formatDay <= formatTo)) {
                const status = e.status;
                let component = <View />;
                switch (status) {
                    case 2:
                        component = (
                            <View style={[this.styles, { backgroundColor: '#4CD964' }]}>
                                <Text style={{ fontSize: 15, color: '#ffffff', marginBottom: 5 }}>{e.leaveTypeName}</Text>
                                <Text style={{ fontSize: 13, color: '#ffffff' }}>{moment(e.from).format('L HH:mm')} - {moment(e.to).format('L HH:mm')}</Text>
                            </View>
                        )
                        break;
                    case 3:
                        component = (
                            <View style={[this.styles, { backgroundColor: '#FF3B30' }]}>
                                <Text style={{ fontSize: 15, color: '#ffffff', marginBottom: 5 }}>{e.leaveTypeName}</Text>
                                <Text style={{ fontSize: 13, color: '#ffffff' }}>{moment(e.from).format('L HH:mm')} - {moment(e.to).format('L HH:mm')}</Text>
                            </View>
                        )
                        break;
                    default:
                        component = (
                            <View style={[this.styles, { backgroundColor: '#FFFFFF', borderColor: '#FF9500', borderWidth: 1 }]}>
                                <Text style={{ fontSize: 15, color: '#FF9500', marginBottom: 5 }}>{e.leaveTypeName}</Text>
                                <Text style={{ fontSize: 13, color: '#FF9500' }}>{moment(e.from).format('L HH:mm')} - {moment(e.to).format('L HH:mm')}</Text>
                            </View>
                        )
                        break;
                }
                result.push(component);
            }
        });
        return result;
    }

    employeeShiftDistributions(day, listItem) {
        const result = [];
        listItem.map(e => {
            const from = moment(e.applyDay).format('YYYY-MM-DD');
            if (day === from) {
                let component = (
                    <View style={[this.styles, { backgroundColor: '#FFFFFF', borderColor: '#007AFF', borderWidth: 1 }]}>
                        <Text style={{ fontSize: 15, color: '#007AFF', marginBottom: 5 }} numberOfLines={1}>{e.shiftName}</Text>
                        <Text style={{ fontSize: 13, color: '#007AFF' }}>{e.shift.timeframes[0].checkInTime} - {e.shift.timeframes[0].checkOutTime}</Text>
                    </View>
                )
                result.push(component);
            }
        });

        return result;
    }



    employeeTimekeepings(day, listItem) {
        const result = [];
        listItem.map(e => {
            let checkIp = this.checkIP(e)
            const { status } = e;
            let from = null;
            let component = <View />;
            switch (status) {
                case 3:
                    from = moment(e.approveIn).format('YYYY-MM-DD');
                    if (day === from) {
                        component = (
                            <View style={[this.styles, { backgroundColor: '#4CD964' }]}>
                                <Text style={{ fontSize: 15, color: '#ffffff', marginBottom: 5 }} numberOfLines={1}>{e.shiftName}</Text>
                                <Text style={{ fontSize: 13, color: '#ffffff' }}>{moment(e.approveIn).format('HH:mm')} - {moment(e.approveOut).format('HH:mm')}</Text>
                            </View>
                        )
                        result.push(component);
                    }
                    break;
                case 4:
                    from = moment(e.approveIn).format('YYYY-MM-DD');
                    if (day === from) {
                        component = (
                            <View style={[this.styles, { backgroundColor: '#FF3B30' }]}>
                                <Text style={{ fontSize: 15, color: '#ffffff', marginBottom: 5 }} numberOfLines={1}>{e.shiftName}</Text>
                                <Text style={{ fontSize: 13, color: '#ffffff' }}>{moment(e.approveIn).format('HH:mm')} - {moment(e.approveOut).format('HH:mm')}</Text>
                            </View>
                        )
                        result.push(component);
                    }
                    break;
                default:
                    from = Utils.formatTime(e.checkin, 'YYYY-MM-DD');
                    if (day === from) {
                        component = (
                            <View style={[this.styles, { backgroundColor: '#FFFFFF', borderColor: '#FF9500', borderWidth: 1, flexDirection: 'row', justifyContent: 'space-between' }]}>
                                <View style={{maxWidth: (settingApp.width * 0.75), justifyContent:'space-between' }}>
                                    <Text style={{ fontSize: 15, color: '#FF9500', paddingTop:5}} numberOfLines={1}>{e.shiftName}</Text>
                                    <Text style={{ fontSize: 13, color: '#FF9500', paddingBottom:5 }}>{Utils.formatTime(e.checkin, 'HH:mm')}{e.checkout ? `- ${Utils.formatTime(e.checkout, 'HH:mm')}` : ''}</Text>
                                </View>
                                <View style={{ maxWidth: 70, backgroundColor: 'transparent', flexDirection: 'row', alignItems:'center' }}>
                                    {checkIp ?
                                        <SvgCP.error /> : null
                                    }
                                    {e.checkinNote && e.checkinNote.length > 0 ?
                                        <SvgCP.noteCheck /> : null
                                    }
                                </View>

                            </View>
                        )
                        result.push(component);
                    }
                    break;
            }
        });

        return result;
    }

    holidays(day, listItem) {
        const result = [];
        listItem.map(e => {
            const to = moment(e.toTime).format('YYYY-MM-DD');
            const from = moment(e.fromTime).format('YYYY-MM-DD');
            const formatTo = new Date(to);
            const formatFrom = new Date(from);
            const formatDay = new Date(day);
            if (day === from || day === to || (formatFrom <= formatDay && formatDay <= formatTo)) {
                let component = (
                    <View style={[this.styles, { backgroundColor: '#71CACD' }]}>
                        <Text style={{ fontSize: 15, color: '#FFFFFF' }} numberOfLines={1}>{e.name}</Text>
                    </View>
                )
                result.push(component);
            }
        });

        return result;
    }

    requestCreateTimekeeping(day, listItem) {
        const result = [];
        listItem.map(e => {
            const from = moment(e.requestApproveDate).format('YYYY-MM-DD');
            if (day === from) {
                const requestData = JSON.parse(e.requestData);
                let component = (
                    <View style={[this.styles, { backgroundColor: '#FFFFFF', borderColor: '#FF2D55', borderWidth: 1 }]}>
                        <Text style={{ fontSize: 15, color: '#FF2D55', marginBottom: 5 }} numberOfLines={1}>{e.requestShiftName}</Text>
                        <Text style={{ fontSize: 13, color: '#FF2D55' }}>{requestData.checkinTime} - {requestData.checkoutTime}</Text>
                    </View>
                )
                result.push(component);
            }
        });

        return result;
    }

    onDateSelected(date) {
        this.selectDay = moment(date).format('YYYY-MM-DD');
        const index = this.list.findIndex(e => e.day === this.selectDay);
        if (index > -1) {
            this.setState({ selectList: [...this.list[index].data] });
        } else {
            this.setState({ selectList: [] });
        }
    }

    initialWeek() {
        this.calendar.setSelectedDate(moment());
        this.setState({ weekStart: moment() })
    }

}

mapStateToProps = state => ({ ...state });
export default connect(mapStateToProps)(CalendarJob);