import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, Text } from "react-native";

import { settingApp } from '../../../../../../../public';

class CalendarHeader extends Component {
  static propTypes = {
    calendarHeaderFormat: PropTypes.string.isRequired,
    calendarHeaderStyle: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.number
    ]),
    datesForWeek: PropTypes.array.isRequired,
    allowHeaderTextScaling: PropTypes.bool
  };

  shouldComponentUpdate(nextProps) {
    return JSON.stringify(this.props) !== JSON.stringify(nextProps);
  }

  //Function that formats the calendar header
  //It also formats the month section if the week is in between months
  formatCalendarHeader(datesForWeek, calendarHeaderFormat) {
    if (!datesForWeek || datesForWeek.length === 0) {
      return "";
    }

    let firstDay = datesForWeek[0];
    let lastDay = datesForWeek[datesForWeek.length - 1];
    let monthFormatting = "";
    //Parsing the month part of the user defined formating
    if ((calendarHeaderFormat.match(/Mo/g) || []).length > 0) {
      monthFormatting = "Mo";
    } else {
      if ((calendarHeaderFormat.match(/M/g) || []).length > 0) {
        for (
          let i = (calendarHeaderFormat.match(/M/g) || []).length;
          i > 0;
          i--
        ) {
          monthFormatting += "M";
        }
      }
    }

    if (firstDay.month() === lastDay.month()) {
      return `Tháng ${firstDay.format('M/YYYY')}`;
    } else if (firstDay.year() !== lastDay.year()) {
      return `Tháng ${firstDay.format('M/YYYY')} - Tháng ${lastDay.format('M/YYYY')}`;
    }

    return `${
      monthFormatting.length > 1 ? `Tháng ${firstDay.format('M/YYYY')}` : ""
      } ${monthFormatting.length > 1 ? "-" : ""} Tháng ${lastDay.format('M/YYYY')}`;
  }

  render() {
    const headerText = this.formatCalendarHeader(
      this.props.datesForWeek,
      this.props.calendarHeaderFormat
    );
    return (
      <View
        style={{ flex: 1, flexDirection: 'row', backgroundColor: '#ffffff', ...settingApp.shadow }}
      >
        <View style={{ flex: 0.7, justifyContent: 'center', paddingLeft: 10 }}>
          <Text
            style={{ fontSize: 15, color: '#212121', fontWeight: '500' }}
            allowFontScaling={this.props.allowHeaderTextScaling}
          >
            {headerText}
          </Text>
        </View>
        <View style={{ flex: 0.3 }}>

        </View>
      </View>
    );
  }
}

export default CalendarHeader;
