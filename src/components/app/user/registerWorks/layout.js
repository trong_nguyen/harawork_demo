import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { HeaderIndex, HeaderWapper, Utils, settingApp, Loading, NotFound } from '../../../../public';
import { Ionicons, FontAwesome } from '@expo/vector-icons';
import Content from './components/content';
import moment from 'moment';

class Layout extends Component {
    renderHeader() {
        return (
            <HeaderIndex
                title={
                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                    >
                        <Text style={settingApp.styleTitle}>
                            Đăng ký ca
                        </Text>
                    </View>
                }
            />
        )
    }

    renderContent() {
        return (
            <Content
                ref={refs => this.conten = refs}
                colleague={this.colleague}
                navigation={this.props.navigation}
            />)
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {this.renderHeader()}
                {this.renderContent()}
            </View>
        )
    }
}
export default Layout;