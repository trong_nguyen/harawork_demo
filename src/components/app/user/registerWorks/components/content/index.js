import Layout from './layout';
import { Utils, settingApp } from '../../../../../../public';
import { HaravanHr } from '../../../../../../Haravan';
import { Alert } from 'react-native';
import actions from '../../../../../../state/action';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class Content extends Layout {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            isCheck: false,
            listCheck: [],
            list:[],
            isLoading: true,
            listData: [],
            listShift: [], 
            listLeaves:[],
            message: '',
            error: false,
            changeType:[],
            type: -1,
            checkResult: false,
            isVisible: false,
            status: null,
            employeeNote: '',
            totalHour: 0,
            hourWeek: 0,
            checkFirts: true,
        }
        const date = new Date()
        const currenDate = date.getDay();
        let dayStart = null;
        let dayEnd = 7
        if (currenDate == 0) {
            dayStart = date.getDate() - 6;
            dayEnd = date.getDate()
        }
        else {
            if (currenDate == 1) {
                dayStart = date.getDate();
                dayEnd = date.getDate() + (7 - currenDate)
            }
            else {
                dayStart = date.getDate() - (currenDate - 1);
                dayEnd = date.getDate() + (7 - currenDate)
            }

        }

        this.Mon = new Date(date.getFullYear(), date.getMonth(), dayStart);
        this.Sun = new Date(date.getFullYear(), date.getMonth(), dayEnd);

        this.fromDate = Utils.formatLocalTime(this.Mon, 'YYYY-MM-DD');
        this.toDate = Utils.formatLocalTime(this.Sun, 'YYYY-MM-DD');

        this.colleague = props.app.colleague;
        this.navigation = props.navigation;

        this.registList = [];
       
        this.hourOneDay = 0;
        this.hour = 0;
        this.hourWeek = 0;
    }
    componentDidMount() {
        if(this.colleague.error && this.colleague.message){
            this.setState({ isLoading: false, message: this.colleague.message, error: true})
        }
        else{
            this.idDepart = this.props.app.colleague.mainPosition.departmentId;
            this.idJod = this.props.app.colleague.mainPosition.jobtitleId;
            this.loadData(this.idJod, this.idDepart);
        }
    }

    onRefresh(){
        this.setState({refreshing: true }, () =>{
            this.idDepart = this.props.app.colleague.mainPosition.departmentId;
            this.idJod = this.props.app.colleague.mainPosition.jobtitleId;
            this.loadData(this.idJod, this.idDepart);
        })
    }

    async getShiftWeek(fromDate, toDate){
        const {monDay, sunDay, colleague} = this;
        const { listShift, listLeaves } = this.state;   
        const result = await HaravanHr.getMeshiftdistributions(fromDate, toDate, colleague.mainPosition.departmentId, colleague.mainPosition.jobtitleId, colleague.id)
        if(result && result.data && !result.error){
            result.data.data.map(item =>{
                const leaves = item.employeeLeaves.map(m => m)
                if(item.employeeShiftDistributions){
                    this.hourOneDay = item.hourOneDay
                    item.employeeShiftDistributions.map(e =>{
                        listShift.push(e)
                    })
                    this.setState({ listShift})
                }
                this.setState({listLeaves: leaves})
            })
        }
    }

    async loadData(idJod, idDepart) {
        const { fromDate, toDate, colleague } = this;
        const resultMeShift = await HaravanHr.getMeShift(idDepart)
        if (resultMeShift && resultMeShift.data && !resultMeShift.error) {
            const length = resultMeShift.data.length;
            const fromDate = Utils.formatLocalTime(resultMeShift.data[0].date, 'YYYY-MM-DD');
            const toDate = Utils.formatLocalTime(resultMeShift.data[length - 1].date, 'YYYY-MM-DD');
            await this.getShiftWeek(fromDate, toDate)
            const newList = []
            resultMeShift.data.map(e => {
                if (e.shifts && e.shifts.length > 0)
                    newList.push(e.shifts)
            })
            this.setState({ listData: resultMeShift.data, list: newList },()=> this.setState({ isLoading:false, refreshing:false}, () => this.checkRegist()))
        }
        else{
            this.setState({ isLoading: false,  refreshing:false})
        }
    }

    checkRegist(){
        const {registList, hourWeek} = this;
        const total = []
        let hours = hourWeek
        if(registList.length > 0 ){
            registList.map(e =>{
                total.push(e.totalHour)
            })
        }
        total.map(m =>{
            hours =  hours + m
        })
        this.hourWeek =  hours
        this.registList = []
        this.setState({hourWeek: this.hourWeek,checkFirts: false }, () => this.hourWeek = this.state.hourWeek)
    }

    updateTime(time, type){
        if (type === 1) {
            const { hourWeek } = this.state;
            const hour = hourWeek - time;
            this.setState({ hourWeek: hour }, () => this.hourWeek = this.state.hourWeek)
        }
        this.registList = []
    }

    settupWeed(item) {
        const date = new Date(item.date)
        const weekday = new Array(7)
        weekday[0] = { title: 'Chủ nhật', isDay: 'isSun', isSun: true };
        weekday[1] = { title: 'Thứ 2', isDay: 'isMon', isMon: true };
        weekday[2] = { title: 'Thứ 3', isDay: 'isTue', isTue: true };
        weekday[3] = { title: 'Thứ 4', isDay: 'isWed', isWed: true };
        weekday[4] = { title: 'Thứ 5', isDay: 'isThu', isThu: true };
        weekday[5] = { title: 'Thứ 6', isDay: 'isFri', isFri: true };
        weekday[6] = { title: 'Thứ 7', isDay: 'isSat', isSat: true };
        return weekday[date.getDay()]
    }
    
    checkItem(item, totalHour){
        const { listCheck } = this.state;
        const {hour}= this;
        let total = 0;
        if (listCheck.length === 0) {
            listCheck.push(item);
            total = hour + totalHour
            this.hour = total;
        }
        else {
            const indexList = listCheck.findIndex(e =>e=== item &&  e.date === item.date && e.i === item.i && e.id ===item.id)
            if (indexList > -1) {
                listCheck.splice(indexList, 1)
                this.hour = this.hour - totalHour
            }
            else {
                listCheck.push(item)
                total = hour + totalHour
                this.hour = total;
            }
        }
        this.setState({
            listCheck: listCheck,
            changeType: [],
        })
    }

    setTime(){
        const { listCheck } = this.state;
        let hourIn = 0
        let minIn = 0
        let hourOut = 0
        let minOut = 0
        listCheck.map(item => {
            hourIn = item
        })
    }
    
    createWorks(type, note){
        const { listCheck, listShift, changeType } = this.state;
        const idDepart = this.props.app.colleague.mainPosition.departmentId;
        const idJod = this.props.app.colleague.mainPosition.jobtitleId;
        const newList = listCheck;
        const shiftID = listShift.map(m => m.id)
        this.setState({ listCheck: [], type: type})
        const { colleague } = this
        newList.map(item =>{
            changeType.push(item)
            const index = shiftID.findIndex( e => e === item.checkShift)
            if(index > -1){
                const body = {
                    ApproveTypeDistribution: 0,
                    Status: 1,
                    TypeDistribution: type,
                    EmployeeNote: note ?  note : ''
                }
                this.setState({},async() =>{
                    const result = await HaravanHr.registUpdateWorks(shiftID[index], body)
                    if(result && result.data && !result.error){
                        this.setState({ checkResult: false, employeeNote:note })
                        this.hour =0;
                        if(type === 1){
                            this.registList.push(item)
                            this.checkRegist();
                        }
                    }
                })
            }
            else{
                const data = {
                    ApplyDay: item.date,
                    ApproveTypeDistribution: 0,
                    ApproverId: colleague.id,
                    DepartmentId: colleague.mainPosition.departmentId,
                    DistributorId: colleague.id,
                    EmployeeId: colleague.id,
                    EmployeeNote: note ?  note : '',
                    IsOT: false,
                    JobTitleId: colleague.mainPosition.jobtitleId,
                    ShiftId: item.shiftId,
                    Status: 1,
                    TimeframeId: item.id,
                    TypeDistribution: type
                }
                this.setState({},async () =>{
                    const result = await HaravanHr.registCreateWorks(data)
                    if(result && result.data && !result.error){
                        this.setState({ checkResult: false, employeeNote:note })
                        this.hour = 0;
                        if(type === 1){
                            this.registList.push(item)
                            this.checkRegist();
                        }
                    }
                })    
            }
        })
    }

    alert(hourOneDay) {
        Alert.alert(
            'Thông tin giờ công',
            `Giờ công đã đăng ký: ${this.hourWeek} giờ\nGiờ công chuẩn tuần này: ${hourOneDay} giờ`,
            [
                { text: 'Đóng', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            ],
            { cancelable: false }
        )
    }

    applyItem(item, data, shifts){
        const {listShift, listLeaves} = this.state;
        const list = []
        if(listShift && listShift.length > 0){
            listShift.map(e=>{
                if(Utils.formatLocalTime(e.applyDay, 'YYYY-MM-DD') === Utils.formatLocalTime(data.date, 'YYYY-MM-DD')){
                    list.push(e)
                }
            })
            list.map(m => {
                if(Utils.formatLocalTime(m.applyDay, 'YYYY-MM-DD') === Utils.formatLocalTime(item.date, 'YYYY-MM-DD')
                && m.shiftId === shifts.id
                ){
                    item = {...item, typeDistribution: m.typeDistribution, checkShift: m.id, employeeNote: m.employeeNote}
                }
            })
        }
        return item
    }

    fillFrames(item) {
        const { listShift ,listTime, listLeaves } = this.state;
        const listFrames = []
        const dayCheck = this.settupWeed(item)
        item.shifts.map(e => {
            if (e.timeframes && e.timeframes.length > 0) {
                e.timeframes.map(m => {
                    const indexLeave = listLeaves.findIndex(a => Utils.formatLocalTime(a.from, 'YYYY-MM-DD') === Utils.formatLocalTime(item.date, 'YYYY-MM-DD'))
                    m = {...m, shiftId: e.id, date:item.date , typeDistribution: -1, checkShift: null, leaveDay: listLeaves[indexLeave], employeeNote: null, nameShifts: e.name }
                    if (dayCheck.isDay === 'isSun') {
                        const index = listFrames.findIndex(n => n.id == m.id)
                        if ((m.isSun && m.isSun == true) && index < 0) {
                            let res = this.applyItem(m, item, e)
                            listFrames.push(res)
                        }
                    }
                    else if (dayCheck.isDay === 'isMon') {
                        const index = listFrames.findIndex(n => n.id == m.id)
                        if ((m.isMon && m.isMon == true) && index < 0) {
                            let res = this.applyItem(m, item, e)
                            listFrames.push(res)
                        }
                    }
                    else if (dayCheck.isDay === 'isTue') {
                        const index = listFrames.findIndex(n => n.id == m.id)
                        if ((m.isTue && m.isTue == true) && index < 0) {
                            let res = this.applyItem(m, item, e)
                            listFrames.push(res)
                        }
                    }
                    else if (dayCheck.isDay === 'isWed') {
                        const index = listFrames.findIndex(n => n.id == m.id)
                        if ((m.isWed && m.isWed == true) && index < 0) {
                            let res = this.applyItem(m, item, e)
                            listFrames.push(res)
                        }
                    }
                    else if (dayCheck.isDay === 'isThu') {
                        const index = listFrames.findIndex(n => n.id == m.id)
                        if ((m.isThu && m.isThu == true) && index < 0) {
                            let res = this.applyItem(m, item, e)
                            listFrames.push(res)
                        }
                    }
                    else if (dayCheck.isDay === 'isFri') {
                        const index = listFrames.findIndex(n => n.id == m.id)
                        if ((m.isFri && m.isFri == true) && index < 0) {
                            let res = this.applyItem(m, item, e)
                            listFrames.push(res)
                        }
                    }
                    else if (dayCheck.isDay === 'isSat') {
                        const index = listFrames.findIndex(n => n.id == m.id)
                        if ((m.isSat && m.isSat == true) && index < 0) {
                            let res = this.applyItem(m, item, e)
                            listFrames.push(res)
                        }
                    }
                })
            }
        })
        if(this.state.checkFirts === true){
            listFrames.map(e =>{
                if(e.typeDistribution === 1){
                    this.registList.push(e)
                }
            })
        }
        return listFrames
    }
}
mapStateToProps = state => ({ ...state });
mapDispatchToProps = dispatch => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch)
    }
    return { actions: acts }
}
export default connect(mapStateToProps, mapDispatchToProps)(Content);