import React, { Component } from 'react';
import {Text, View, TouchableOpacity, Modal, TextInput, KeyboardAvoidingView } from 'react-native';
import { settingApp } from '../../../../../../../public';
import { MaterialIcons, Ionicons } from '@expo/vector-icons';

class ModalButon extends Component{
    constructor(props){
        super(props);
        this.state={
            note: ''
        }
    }
    onPress(status, note){
        this.props.close()
        this.props.createWorks(status, note)
        this.props.checkResult(true)
    }
    render() {
        const {  width, height } = settingApp;
        const {note} = this.state;
        const { status } = this.props;
        return (
            <Modal
                animationType='fade'
                visible={this.props.visible}
                transparent={true}
                onRequestClose={() => this.props.close()}
            >
            
                <KeyboardAvoidingView behavior='padding' style={{flex: 1, justifyContent:'center', alignItems:'center', backgroundColor:'rgba(4, 4, 15, 0.5)'}}>
                <View style={{ width: (width - 60), height:status == 0 ? (height * 0.35) : (height * 0.3), backgroundColor: '#FFFFFF', borderRadius:10, justifyContent:'space-between', flexDirection:'column'}}>
                    <View style={{alignItems:'center', marginTop:10}}>
                        <Text style={{color: settingApp.colorText, fontSize:18, fontWeight:'bold'}}>Thông báo </Text>
                        
                    </View>
                    <View style={{alignItems:'center', marginBottom:10, justifyContent:'center',marginLeft:15, marginRight:15,}}>
                        <Text style={{color: settingApp.colorText, fontSize:status == 0 ? 14 : 16,  alignItems:'center', textAlign:'center'}}
                        >
                        {
                            status == 0 ? 'Bạn vui lòng cho biết lý do đăng ký bận'
                            :
                            'Bạn có chắc chắn muốn đăng ký làm những ca này?'
                        }
                        </Text>
                    </View>
                    {status == 0 ?
                    <View style={{justifyContent:'center', alignItems:'center'}}>
                            <View style={{height:60, width:'80%',borderWidth:1, borderColor:'#C8C7CC', marginLeft:15, marginRight:15, borderRadius:5}}>
                                <TextInput
                                    underlineColorAndroid='transparent'
                                    placeholderTextColor='#8E8E93'
                                    multiline ={true}
                                    style={{ fontSize:16, padding:10 }}
                                    value={ this.state.note}
                                    onChangeText={(note) => this.setState({ note })}
                                    autoFocus ={true}
                                />
                            </View>
                    </View>
                            : null}
                      
                    <View style={{ height:50,flexDirection:'row', justifyContent:'space-between', borderTopColor:'#C8C7CC', borderTopWidth:0.5}}>
                        <TouchableOpacity
                            onPress={()=> this.props.close()}
                            style={{ width:'50%',alignItems:'center', justifyContent:'center', borderRightWidth:0.5, borderRightColor:'#C8C7CC'}}
                        >
                            <Text style={{color:'#21469B', fontSize:16}}> Đóng </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={()=> {
                                status == 0 && note.length == 0 ? null :
                                this.onPress(status, note)}
                                }
                            style={{alignItems:'center', justifyContent:'center',  width:'50%'}}
                        >   
                            <Text style={{color:status == 0 ? '#FF3B30' : '#4CD964', fontSize:16, opacity: status== 0 && note.length=== 0 ? 0.3 : 1 }}>Xác nhận</Text>
                        </TouchableOpacity>
                    </View>
                    </View>
                </KeyboardAvoidingView>
            </Modal>
        )
    }

    componentWillUnmount(){
        this.setState({ note: ''})
    }
}
export default ModalButon;