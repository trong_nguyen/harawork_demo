import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { settingApp, Utils, ButtonCustom } from '../../../../../../../public'
import { Ionicons, FontAwesome } from '@expo/vector-icons';
import LeaveItem from './leaveItem'

class Item extends Component {
    constructor(props) {
        super(props);
        this.state = {
            changeType: false,
            item: this.props.item,
            checkDay: false
        }

        this.isCheck = false
    }

    componentWillReceiveProps(nextProps) {
        let { item } = this.state;
        const { checkResult } = this.props
        if (nextProps && nextProps.changeType.length > 0 && checkResult == true) {
            nextProps.changeType.map(m => {
                if (m.id === item.id &&
                    Utils.formatTime(m.date, 'YYYY-MM-DD') === Utils.formatTime(item.date, 'YYYY-MM-DD')
                ) {
                    item = { ...item, typeDistribution: this.props.type }
                }
                else {
                    item = { ...item, typeDistribution: item.typeDistribution }
                }
                this.setState({ item })
            })

        }
    }

    componentDidMount(){
        const { item } = this.state;
        let curDate = Utils.formatTime((new Date()), 'DD/MM/YYYY', true);
        const dateItem = Utils.formatTime(item.date, 'DD/MM/YYYY', true)
        if( dateItem < curDate){
            this.setState({ checkDay: false})
        }
        else if(dateItem === curDate || dateItem > curDate){
            this.setState({ checkDay: true})
        }
    }

    
    renderCheck(item) {
        const { listCheck } = this.props;
        const { isCheck } = this;
        return (
            <View
                style={{
                    position: 'absolute',
                    height: '100%',
                    width: 50,
                    justifyContent: 'center',
                    //alignItems: 'flex-start',
                }}
            >
                <TouchableOpacity
                    onPress={() => {
                        this.isCheck = !this.isCheck
                        this.props.checkItem(item, item.totalHour)
                        //this.props.setTime
                    }}
                    activeOpacity={1}
                    style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'transparent',
                    }}
                >
                    <Ionicons
                        name='ios-checkmark-circle'
                        size={40}
                        color={isCheck ? '#21469B' : '#FFFFFF'}
                    />
                </TouchableOpacity>
            </View>
        )
    }

    checkColor(color) {
        const colorList = new Array(3)
        colorList[-1] = { backgroundColor: 'rgba(200, 199, 204, 0.3)', textColor: settingApp.colorText, colorLable: '#8E8E93' };
        colorList[0] = { backgroundColor: '#FF3B30', textColor: '#FFFFFF', colorLable: '#FFFFFF' };
        colorList[1] = { backgroundColor: '#4CD964', textColor: '#FFFFFF', colorLable: '#FFFFFF' };
        return colorList[color]
    }
    
    changeType(type) {
        let { item } = this.state;
        if (type.id === item.id) {
            item = { ...item, typeDistribution: -1 }
            this.setState({ item })
        }
    }

    renderFrames() {
        const { listCheck, listShift, color } = this.props;
        let { item, checkDay } = this.state;
        const changeColor = this.checkColor(item.typeDistribution);
        let status = item.typeDistribution;
        

        let checkOnPress = (listCheck.length == 0 && status != -1 && checkDay == true || item.leaveDay) ;

        return (
            <View style={{ flex: 1 }}>
                <ButtonCustom
                    onPress={() =>
                        checkOnPress ?
                            this.props.navigation.navigate('RegistFrameDetail', {
                                data: item,
                                listShift: listShift,
                                employeeNote: this.props.employeeNote,
                                changeType: (data => this.changeType(data)),
                                updateTime: ((time, type) => this.props.updateTime(time, type))
                            })
                            : null}
                    style={{
                        flex: 1, flexDirection: 'row',
                        backgroundColor: changeColor.backgroundColor,
                        borderRadius: 5,
                        height: 70,
                        justifyContent: 'center'
                    }}
                >
                    {
                        status == -1 && checkDay == true ?
                            <View style={{ width: '25%', backgroundColor: 'transparent', height: '100%' }} />
                            : null
                    }
                    <View style={{ justifyContent:'center', flexDirection: 'column', width: (status == -1 && checkDay == true)? '60%' : '90%', marginLeft: checkDay == true ? 10 : 0}}>
                        <Text numberOfLines={1}
                            style={{ color: changeColor.textColor, fontSize: 18, }}
                        >{item.nameShifts}</Text>
                        <Text numberOfLines={1}
                            style={{ color: changeColor.colorLable, fontSize: 14 }}
                        >{item.checkInTime} - {item.checkOutTime}</Text>
                    </View>
                </ButtonCustom>
                {status != -1 || item.leaveDay ? null :
                ( (checkDay == false) ? null : this.renderCheck(item) )
                    }
            </View>
        )
    }
    render() {
        const { listCheck, listShift, color } = this.props;
        let { item } = this.state
        if (listCheck.length == 0) {
            this.isCheck = false
        }
        return (
            <View style={{ flex: 1 }}>
                {item.leaveDay ?
                    <LeaveItem 
                        navigation ={ this.props.navigation}
                        item = {this.state.item}
                        listCheck = {this.props.listCheck}
                        listShift= {this.props.listShift}
                        color={this.props.color}
                    />
                    :
                    this.renderFrames()}
            </View>
        )
    }
}
export default Item;