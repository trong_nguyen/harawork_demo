import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { settingApp, Utils, ButtonCustom } from '../../../../../../../public'
import { Ionicons, FontAwesome } from '@expo/vector-icons';

class LeaveItem extends Component{
    constructor(props){
        super(props)
    }

    checkColorLeaves(color) {
        const colorList = new Array(3)
        colorList[1] = { backgroundColor: '#FFFFFF', textColor: '#FF9500', color: '#FF9500' };
        colorList[2] = { backgroundColor: '#4CD964', textColor: '#FFFFFF', color: '#4CD964' };
        colorList[3] = { backgroundColor: '#FF3B30', textColor: '#FFFFFF', color: '#FF3B30' };
        return colorList[color]
    }

    render() {
        const { listCheck, listShift, color, item } = this.props;
        let aprID = item.leaveDay.approver ? item.leaveDay.approver.userId : ''
        const leaveDay = {...item.leaveDay, approverUserId: aprID , approverJobtitleName: item.leaveDay.departmentName};
        const timeFrom = Utils.formatLocalTime(leaveDay.from, 'LT', true);
        const timeTo = Utils.formatLocalTime(leaveDay.to, 'LT', true);
        const changeColorLeaves = this.checkColorLeaves(leaveDay.status)
        return (
            <ButtonCustom
                onPress={() => this.props.navigation.navigate('DetailFormLeave', { 
                    item: leaveDay,
                    leaveType: item.leaveDay.leaveTypeName
                 })}
                style={{
                    flex: 1,
                    backgroundColor: changeColorLeaves.backgroundColor,
                    borderRadius: 5,
                    height: 70,
                    justifyContent: 'center',
                    width: '100%',
                    borderWidth: 1,
                    borderColor: changeColorLeaves.color,
                }}>
                <Text numberOfLines={1}
                    style={{ color: changeColorLeaves.textColor, fontSize: 18, paddingLeft: 10 }}
                >{leaveDay.leaveTypeCode}</Text>
                <Text numberOfLines={1}
                    style={{ color: changeColorLeaves.textColor, fontSize: 14, paddingLeft: 10 }}
                >{timeFrom} - {timeTo}</Text>
            </ButtonCustom>
        )
    }
}
export default LeaveItem;