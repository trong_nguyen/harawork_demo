import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, ActivityIndicator, RefreshControl } from 'react-native';
import { HeaderIndex, HeaderWapper, Utils, settingApp, Loading, NotFound, ButtonCustom } from '../../../../../../public'
import { Ionicons, FontAwesome } from '@expo/vector-icons';
import Item from './component/item';
import Modal from './component/modal'

class Layout extends Component {

    renderLoadResult(){
        return(
            <View style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                backgroundColor: 'transparent',
                justifyContent: 'center',
                alignItems: 'center',
            }}>
                <ActivityIndicator size='large' color={settingApp.color} />
            </View>
        )
    }

    renderTop() {
        const { listData, list, hourWeek } = this.state;
        let length = list.length;
        const dayStart = listData[0].shifts[0].timeframes[0].departmentDistributionShifts.length > 0 ? listData[0].shifts[0].timeframes[0].departmentDistributionShifts[0].fromDate :  new Date();
        const dayEnd = listData[0].shifts[0].timeframes[0].departmentDistributionShifts.length > 0 ? listData[0].shifts[0].timeframes[0].departmentDistributionShifts[0].toDate :  new Date();
        const toDate = Utils.formatTime(dayEnd, 'DD/MM') 
        const fromDate = Utils.formatTime(dayStart, 'DD/MM')
        let hourOneDay = this.hourOneDay * length
        return (
            <View
                style={{ 
                    flexDirection: 'row', 
                    justifyContent: 'space-between', 
                    alignItems: 'center',
                    backgroundColor:'#FFFFFF',
                    height:40,
                    borderBottomColor:'#8E8E93',
                    borderBottomWidth:0.5,
                    ...settingApp.shadow
                    }}
            >
                <View
                    style={{ paddingLeft: 15, backgroundColor:'transparent' }}
                >
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ fontSize: 14, color: '#8E8E93', marginRight: 3 }} >
                            Thời hạn đăng ký từ
                            </Text>
                        <Text style={{ fontSize: 16, color: settingApp.colorText, marginRight: 3, fontWeight: 'bold' }} > {fromDate} - {toDate} </Text>
                    </View>
                </View>

                <TouchableOpacity 
                onPress= { ()=> this.alert(hourOneDay)}
                    style={{ paddingRight: 15, flexDirection:'row' }}
                >   
                    <Text style={{ fontSize: 16, color: settingApp.color }}>{hourWeek}</Text>
                    <Text style={{ fontSize: 16, color: '#8E8E93' }}>/{hourOneDay}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    erro() {
        const { message } = this.state;
        return (
            <View style={{ height: 80, alignItems: 'center', flexDirection: 'row', ...settingApp.shadow, backgroundColor: '#FFFFFF', paddingLeft: 20, paddingRight: 20 }}>
                <Ionicons name='ios-warning' color='#fd7e14' size={25} />
                <View style={{ flex: 1, alignItems: 'center' }}>
                    <Text
                        numberOfLines={3}
                        style={{ fontSize: 14, color: settingApp.colorText, textAlign: 'center' }}
                    >{message}</Text>
                </View>
            </View>
        )
    }

    renderConten() {
        const { listCheck, list, listData} = this.state;
        if (listData && listData.length > 0) {
            return (
                <ScrollView 
                    refreshControl = {
                        <RefreshControl
                            refreshing= {this.state.refreshing}
                            onRefresh ={() => this.onRefresh()}
                            tintColor={settingApp.color}
                            colors={[settingApp.color]}
                    />
                    }
                    style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
                    <ScrollView
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator = {false}
                    >
                        {listData.map((item, index) => {
                            if (item.shifts && item.shifts.length > 0) {
                                const day = this.settupWeed(item)
                                const date = Utils.formatTime(item.date, 'DD')
                                return (
                                    <View
                                        key={index}
                                        style={{ width: 170, backgroundColor: '#FFFFFF', paddingTop: 10, paddingLeft:index == 0 ? 10 : 5, paddingRight:5 }}>
                                        <View
                                            style={{ paddingLeft: 10, paddingRight: 10, flexDirection: 'row', marginBottom: 10 }}>
                                            <Text style={{ fontSize: 18, fontWeight: 'bold', color: settingApp.colorText }}>{date} / </Text>
                                            <Text style={{ fontSize: 16, color: '#8E8E93' }}>{day.title}</Text>
                                        </View>
                                        {this.renderItem(item, day)}
                                    </View>
                                )
                            }
                        })}
                    </ScrollView>
                </ScrollView>
            )
        }
        else {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#FFFFFF' }}>
                    <Text
                        numberOfLines={4}
                        style={{ fontSize: 14, color: '#8E8E93', padding: 20, textAlign: 'center' }}>
                        Chưa có thiết lập ca cho khoảng thời gian này.
                    Vui lòng liên hệ quản lý của bạn để biết thêm chi tiết.
                        </Text>
                </View>
            )
        }
    }

    renderItem(item, day) {
        const frames = this.fillFrames(item)
        const { listShift, employeeNote } = this.state;
        if (frames && frames.length > 0) {
            return (
                <View
                    style={{ flexDirection: 'column', marginTop: 10 }}
                >
                    {frames.map((e, i) => {
                        return (
                            <View
                                key={i}
                                style={{ 
                                    marginBottom: 10, 
                                    flexDirection: 'column', 
                                    ...settingApp.shadow, 
                                    justifyContent: 'center' }}
                            >
                                <Item
                                    item={{...e, i}}
                                    listCheck={this.state.listCheck}
                                    //setTime={this.setTime()}
                                    updateTime ={(time, type) => this.updateTime(time, type)}
                                    navigation ={ this.navigation}
                                    listShift = { this.state.listShift }
                                    changeType = { this.state.changeType }
                                    type ={ this.state.type }
                                    checkResult =  { this.state.checkResult }
                                    employeeNote = {employeeNote}
                                    checkItem={(data, totalHour) => this.checkItem(data, totalHour)}
                                />
                            </View>
                        )
                    })}
                </View>
            )
        }
    }

    renderButton() {
        const { listCheck,  checkResult,  } = this.state;
        return (
            <View style={{ height: 120, width: '100%', bottom: 0, backgroundColor: '#FFFFFF', flexDirection: 'column', padding: 2, ...settingApp.shadow }}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text
                        style={{ fontSize: 16, color: settingApp.colorText, marginTop: 10 }}
                    > Đã chọn {listCheck.length} ca ({Math.round(this.hour)} giờ) </Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingLeft: 10, paddingRight: 10, marginTop: 10, }}>
                    <ButtonCustom
                        onPress={() => {
                            this.setState({ isVisible: true, status: 0 })
                        }}
                        style={{ backgroundColor: '#FF3B30', borderRadius: 5, height: 60, width: '48%', justifyContent: 'center', alignItems: 'center', flexDirection:'row' }}
                    >
                        <Text
                            style={{ fontSize: 18, fontWeight: 'bold', color: '#FFFFFF' }}
                        > Đăng ký bận </Text>
                    </ButtonCustom>

                    <ButtonCustom
                        onPress={() => {
                            this.setState({ isVisible: true, status: 1 })
                        }}
                        style={{ backgroundColor: '#4CD964', borderRadius: 5, height: 60, width: '48%', justifyContent: 'center', alignItems: 'center', flexDirection:'row' }}
                    >
                        <Text
                            style={{ fontSize: 18, fontWeight: 'bold', color: '#FFFFFF' }}
                        > Đăng ký làm </Text>
                    </ButtonCustom>
                </View>
            </View>
        )
    }

    render() {
        const { listCheck, isLoading, error ,listData, isLoad, checkResult } = this.state;
        if (isLoading && !error) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Loading />
                </View>
            )
        }
        else if (!isLoading && error) {
            return (
                this.erro()
            )
        }
        else {
            if (listData && listData.length > 0) {
                return (
                    <View style={{ flex: 1 }}>
                        {this.renderTop()}
                        <View style={{ flex: 1 }}>
                            {this.renderConten()}
                        </View>
                        {
                            listCheck.length > 0 ?
                                this.renderButton()
                                : null
                        }
                        {
                            checkResult ?
                                this.renderLoadResult() : null
                        }
                        <Modal 
                            visible = {this.state.isVisible}
                            close = {() => this.setState({ isVisible: false})}
                            status = { this.state.status }
                            createWorks = {(type, note) => this.createWorks(type, note)}
                            checkResult= {checkResult => this.setState({checkResult:checkResult})}
                        />
                    </View>
                )
            }
            else {
                return (
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#FFFFFF' }}>
                        <Text
                            numberOfLines={4}
                            style={{ fontSize: 14, color: '#8E8E93', padding: 20, textAlign: 'center' }}>
                            Chưa có thiết lập ca cho khoảng thời gian này.
                        Vui lòng liên hệ quản lý của bạn để biết thêm chi tiết.
                            </Text>
                    </View>
                )
            }
        }
    }
}
export default Layout;