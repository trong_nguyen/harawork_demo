import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { settingApp, Utils, HeaderWapper } from '../../../../../../public'
import { Ionicons, FontAwesome } from '@expo/vector-icons';
import moment from 'moment';

class Layout extends Component{
    renderHeader(){
        const { data } = this;
        const status= data.typeDistribution;
        
        return(
            <HeaderWapper
                style={{justifyContent:'space-between', alignItem:'center', flexDirection:'row'}}
            >
                 <TouchableOpacity
                    onPress={() => this.props.navigation.pop()}
                    style={{ width: 55, height: 43, justifyContent: 'center', alignItems: 'center' }}
                >
                    <Ionicons name='ios-arrow-back' size={23} color='#FFFFFF' />
                </TouchableOpacity>

                <Text style={{ alignItems: 'center', marginTop: 10, fontSize: 16, fontWeight: 'bold', color: '#FFFFFF' }}>
                {'Đăng ký' + (status == 0 ? ' bận' : ' làm')}
                </Text>
                <View
                    style={{width:50}}
                />
            </HeaderWapper>
        )
    }

    renderContent(){
        const { data } = this;
        const { employeeNote } = this.state
        const width = settingApp;
        const date = moment.utc(data.date).local().format('L');;
        return(
            <View style={{backgroundColor:'#FFFFFF', ...settingApp.shadow, marginTop:15}}>
                <View style={{padding:15}}>
                    <Text style={styles.textTitle}>Ngày</Text>
                    <Text style={styles.textDisrip}>{date}</Text>
                </View>
                <View style={{marginLeft:15}}>
                    <Text style={styles.textTitle}>Ca làm việc</Text>
                    <Text style={styles.textDisrip}>{data.nameShifts} [{data.checkInTime} - {data.checkOutTime}]</Text>
                </View>

                {
                    data.typeDistribution == 0 && employeeNote.length > 0?
                        <View style={{ marginLeft: 15, paddingTop:15 }}>
                            <Text style={styles.textTitle}>Lý do bận</Text>
                            <Text style={styles.textDisrip}>{employeeNote}</Text>
                        </View>
                        : null
                }

                <View style={{alignItems:'center', padding:15}}>
                <TouchableOpacity
                    onPress={() => this.alert(-1)}
                    style={{backgroundColor:'#FF3B30', height:60, width:'100%', borderRadius:5, alignItems:'center', justifyContent:'center'}}
                >
                    <Text
                        style={{ color:'#FFFFFF', fontSize:18, fontWeight:'bold'}}
                        >Huỷ đăng ký</Text>
                </TouchableOpacity>
                </View>
            </View>
        )
    }

    render(){
        return(
            <View style ={{flex: 1}}>
                {this.renderHeader()}
                {this.renderContent()}
            </View>
        )
    }
    
}
const styles={
    textTitle:{
        color:settingApp.colorText,
        fontSize:16
    },
    textDisrip:{
        color:'#8E8E93',
        fontSize:14
    }
}
export default Layout;