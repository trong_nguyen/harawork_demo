import Layout from './layout';
import {HaravanHr } from '../../../../../../Haravan';
import {Utils} from '../../../../../../public'
import { Alert } from 'react-native';

class Detail extends Layout{
    constructor(props){
        super(props);
        this.state ={
            employeeNote: ''
        }
        this.data= props.navigation.state.params.data;
        this.listShift = props.navigation.state.params.listShift;
        this.changeType = props.navigation.state.params.changeType;
        this.employeeNote= props.navigation.state.params.employeeNote;
        this.updateTime = props.navigation.state.params.updateTime;
    }

    componentDidMount(){
        if(this.data.employeeNote && this.data.employeeNote.length > 0){
            this.setState({ employeeNote: this.data.employeeNote})
        }
        else{
            this.setState({ employeeNote: this.employeeNote})
        }
    }

    updateWork(type){
        const {data, listShift} = this;
        listShift.map(item =>{
            if(Utils.formatTime(item.applyDay, 'YYYY-MM-DD') === Utils.formatTime(data.date, 'YYYY-MM-DD')
            &&( item.shiftId === data.shiftId)) {
                const body = {
                    ApproveTypeDistribution: 0,
                    Status: 1,
                    TypeDistribution: type,
                }
                this.setState({},async () => {
                    const result = await HaravanHr.registUpdateWorks(item.id, body)
                    if (result && result.data && !result.error) {
                        this.changeType(data)
                        this.updateTime(data.totalHour, data.typeDistribution)
                        this.props.navigation.pop()
                    }
                })
            }
            else{
                this.props.navigation.pop()
            }
        })
    }

    alert(type) {
        Alert.alert(
            'Thông báo',
            `Bạn có chắc chắn muốn huỷ đăng ký ca này?`,
            [
                {
                    text: 'Đóng', onPress: () => {
                        console.log('OK Pressed')
                    },
                    style: 'destructive'
                },
                {
                    text: 'Xác nhận', onPress: () => {
                        console.log('updateWork')
                        this.updateWork(type)
                    },
                }
            ],
            { cancelable: false,    
                }
           
        )
    }

}
export default Detail;