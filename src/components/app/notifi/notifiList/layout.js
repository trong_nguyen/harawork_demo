import React, { Component } from 'react';
import { ActivityIndicator, View, Text, FlatList, RefreshControl, TouchableOpacity } from 'react-native';
import { settingApp, Loading, HeaderIndex } from '../../../../public';

import { Ionicons } from '@expo/vector-icons';

import Item from './components/item';
import Filter from './components/filter';

class Layout extends Component {

    renderItem(obj) {
        const { item, index } = obj;
        return (
            <Item
                obj={{ item }}
                statusStar={(id, status) => this.statusStar(id, status)}
                navigation={this.props.navigation}
                refreshing={this.state.refreshing}
                colleague={this.colleague}
                onRefresh={() => this.refreshList()}
            />
        )
    }

    renderHeader() {
        return (
            <HeaderIndex
                title={
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <TouchableOpacity
                            activeOpacity={1}
                            onPress={() => this.setState({ isOpenFilter: true })}
                            style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={settingApp.styleTitle}>
                                {this.state.title}
                            </Text>
                            <View style={{ justifyContent: 'center', marginTop: 5, marginLeft: 5 }}>
                                <Ionicons name="md-arrow-dropdown" size={23} color="#ffffff" />
                            </View>
                        </TouchableOpacity>
                    </View>
                }
                buttonRight={
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('NotifiSearch', { role: this.role })}
                        style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Ionicons
                            name='md-search'
                            color='#ffffff'
                            size={25}
                        />
                    </TouchableOpacity>
                }
            />
        )
    }

    render() {
        const { isLoading, isLoad, onLoadMore } = this.state;
        const { width, height, statusBarHeight, color, isIPhoneX } = settingApp;
        let content = <View />
        if (!isLoad) {
            content = (
                <View style={{ flex: 1 }}>
                    <FlatList
                        data={this.state.list}
                        extraData={this.state.list}
                        keyExtractor={(item, index) => item.id ? item.id : item.header}
                        renderItem={obj => this.renderItem(obj)}
                        onEndReached={() => this.onEndReached()}
                        onEndReachedThreshold={1}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={() => this.onRefresh()}
                                tintColor={settingApp.color}
                                colors={[settingApp.color]}
                            />
                        }
                        ListFooterComponent={() => (
                            onLoadMore ?
                                <ActivityIndicator size='small' color={color} style={{ height: 50 }} />
                                :
                                <View style={{ marginBottom: isIPhoneX ? 25 : 10 }} />)
                        }
                        ListEmptyComponent={() => (
                            <View style={{ width, height: (height - (statusBarHeight + 44)), justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: 14, color: '#777' }}>Không tìm thấy dữ liệu</Text>
                            </View>)}
                    />
                </View>
            )
        }
        return (
            <View style={{ flex: 1 }}>
                {this.renderHeader()}
                <View style={{ flex: 1 }}>
                    {content}
                    {isLoading && <Loading />}
                    <Filter
                        role={this.role}
                        modalVisible={this.state.isOpenFilter}
                        close={(item) => {
                            if (item) this.changeURL(item);
                            this.setState({ isOpenFilter: false });
                        }}
                    />
                </View>
            </View >
        )
    }
}

export default Layout;