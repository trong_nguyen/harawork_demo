import Layout from './layout';
import { HaravanHr, HaravanIc } from '../../../../Haravan';

import { Toast, Utils } from '../../../../public';

import moment from 'moment';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../state/action';

class NotifiList extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            list: props.notify.listNotify,
            isLoad: !(props.notify.listNotify.length > 0),
            refreshing: false,
            isLoading: !(props.notify.listNotify.length > 0),
            isOpenFilter: false,
            onLoadMore: false,
            title: 'Đang hoặc chưa hiệu lực'
        }

        this.type = '/list/all/is_or_not_yet_activateds';
        this.initList = [];

        this.colleague = null;

        this.page = 1;
        this.page_size = 20;
        this.total_page = null;

        this.departmentListName = [];

        this.today = moment().subtract(0, 'days').format('L');
        this.yesterday = moment().subtract(1, 'days').format('L');
        this.year = moment().format('YYYY');

        const role = this.props.app.authHaravan.userInfo.role || null;
        this.role = Utils.checkRole(role);
    }

    checkData(input) {
        if (input && input.data && !input.error) {
            return input
        } else {
            return false;
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.notify.refreshListNotify) {
            this.refreshList();
        }
    }

    setInitData() {
        this.initList = [];
        this.page = 1;
        this.page_size = 20;
        this.total_page = null;

    }

    componentDidMount() {
        const { colleague } = this.props.app;
        this.colleague = colleague;
        this.setState({ refreshing: true }, () => this.loadData());
    }

    async loadData() {
        const { colleague, type, initList, page, total_page } = this;
        if (!total_page || (total_page && !isNaN(total_page) && page <= total_page)) {
            const result = await this.loadDataList(type, colleague);
            const newList = [...initList, ...result];
            this.initList = newList;

            let group = [];
            newList.map(e => {
                const department = this.departmentListName.filter(m => m.id == e.fromDepartmentId);
                group.push({ departmentName: department[0].name, ...e })
            });

            if (type === '/list/all/is_or_not_yet_activateds' && this.page === 1) {
                this.props.actions.notify.listNotify(group);
            }

            this.page = this.page + 1;

            this.setState({
                list: group
            }, () => this.setState({
                isLoading: false,
                refreshing: false,
                isLoad: false,
                onLoadMore: false
            }));
        }
    }

    async loadDataList(type, colleague) {
        let list = await HaravanIc.getListNotify(colleague, type, this.page, this.page_size);
        list = this.checkData(list);
        if (list) {
            await this.getDepartmentListName(list);
            if (!this.total_page) {
                this.total_page = Math.ceil(list.data.totalCount / this.page_size);
            }
            return list.data.data;
        } else {
            return [];
        }
    }

    async getDepartmentListName(list) {
        const { departmentListName } = this;
        if (departmentListName.length > 0) {
            let listDepartmentId = list.data.data.map(e => e.fromDepartmentId);
            listDepartmentId = listDepartmentId.filter((v, i) => listDepartmentId.indexOf(v) == i);
            listDepartmentId = listDepartmentId.filter((e, i) => {
                if (departmentListName.findIndex(m => m.id == e) === -1) {
                    return e;
                }
            });
            for (let i = 0; i < listDepartmentId.length; i++) {
                let result = await HaravanHr.getDepartmentsById(listDepartmentId[i]);
                result = this.checkData(result);
                if (result) {
                    const item = { id: listDepartmentId[i], name: result.data.name };
                    departmentListName.push(item);
                }
            }
        } else if (departmentListName.length == 0) {
            let listDepartmentId = list.data.data.map(e => e.fromDepartmentId);
            listDepartmentId = listDepartmentId.filter((v, i) => listDepartmentId.indexOf(v) == i);
            for (let i = 0; i < listDepartmentId.length; i++) {
                let result = await HaravanHr.getDepartmentsById(listDepartmentId[i]);
                result = this.checkData(result);
                if (result) {
                    const item = { id: listDepartmentId[i], name: result.data.name };
                    departmentListName.push(item);
                }
            }
        }
    }

    async statusStar(id, status) {
        const result = await HaravanIc.statusStar(id, status);
        if (!result.error) {
            Toast.show(`${status ? 'Đánh dấu' : 'Bỏ đánh dấu'} sao thành công`)
        } else if (result.error) {
            Toast.show('Lỗi')
        }
    }

    onRefresh() {
        this.setState({ refreshing: true }, () => {
            this.setInitData();
            this.loadData();
        });
    }

    refreshList() {
        this.setState({ isLoading: true }, () => {
            this.setInitData();
            this.loadData();
        });
    }

    changeURL(item) {
        if (item && (item.type !== this.type)) {
            this.type = item.type;
            this.setInitData();
            this.setState({ title: item.name, isLoading: true }, () => this.loadData());
        }
    }

    onEndReached() {
        if (this.page < this.total_page && !this.state.onLoadMore) {
            this.setState({ onLoadMore: true }, () => this.loadData());
        }
    }

}

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(mapStateToProps, mapDispatchToProps)(NotifiList);