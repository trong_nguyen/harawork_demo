import React, { Component } from 'react';
import {
    Modal,
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    Image,
    Platform
} from 'react-native';
import { settingApp, imgApp, Utils } from '../../../../../public';

class Filter extends Component {
    constructor(props) {
        super(props)

        this.state = {
            indexType: '/list/all/is_or_not_yet_activateds'
        }

        let list = [
            { icon: imgApp.headerList, name: 'Danh sách', type: '/list/mergeList' },
            { icon: imgApp.headerStar, name: 'Đánh dấu sao', type: '/list/starreds' },
            { name: 'Thông báo cần duyệt', type: '/manager/approve/waiting' },
            { name: 'Đang hoặc chưa hiệu lực', type: '/list/all/is_or_not_yet_activateds' },
            { name: 'Hết hiệu lực', type: '/list/all/expireds' },
            { name: 'Bị thu hồi', type: '/list/all/withdrawn' },
        ];

        if (!this.props.role) {
            list.splice(2, 1);
        }

        this.list = list;
    }

    changeType(item) {
        const { indexType } = this.state;
        if (indexType !== item.type) {
            this.setState({ indexType: item.type }, () => this.props.close(item));
        } else {
            this.props.close();
        }
    }

    render() {
        const { indexType } = this.state;
        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={this.props.modalVisible}
                onRequestClose={() => this.props.close()}
            >
                <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => this.props.close()}
                    style={{ flex: 1 }}>
                    <View style={styles.container} >
                        <ScrollView scrollEnabled={false}>
                            <View style={styles.cover}>
                                <View style={styles.triangle} />
                            </View>
                            <TouchableOpacity activeOpacity={1} style={styles.content}>
                                <View style={{ flex: 1 }}>
                                    {this.list.map((item, index) => {
                                        return (
                                            <View style={{ flex: 1 }} key={index}>
                                                <TouchableOpacity
                                                    onPress={() => this.changeType(item)}
                                                    style={{ flex: 1, backgroundColor: 'transparent' }}>
                                                    <View style={{
                                                        flex: 1,
                                                        height: 55,
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                        paddingLeft: 20,
                                                        backgroundColor: indexType == item.type ? 'rgba(255, 255, 255, 0.1)' : 'transparent'
                                                    }}>
                                                        <Image
                                                            source={item.icon ? item.icon : imgApp.headerDefault}
                                                            style={{ width: 20, height: 20, marginRight: 10 }}
                                                            resizeMode='contain'
                                                        />
                                                        <Text style={{ color: '#ffffff', fontSize: 16 }}>
                                                            {item.name}
                                                        </Text>
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        )
                                    })}
                                </View>
                            </TouchableOpacity>
                        </ScrollView>
                    </View>
                </TouchableOpacity>
            </Modal >
        )
    }
}

const styles = {
    container: {
        flex: 1,
        ...Platform.select({
            android: {
                marginTop: settingApp.statusBarHeight + 20,
            },
            ios: {
                marginTop: settingApp.statusBarHeight + 44,
            }
        }),
        backgroundColor: 'rgba(0,0,0,0.5)',
        paddingLeft: 20,
        paddingRight: 20
    },
    cover: {
        backgroundColor: 'transparent',
        height: 23,
        width: 23,
        overflow: "hidden",
        marginLeft: '46%',
        marginTop: -10
    },
    triangle: {
        width: 23,
        height: 23,
        backgroundColor: '#313F4E',
        transform: [{ rotate: '45deg' }],
        borderRadius: 2.5,
        marginTop: 20
    },
    content: {
        flex: 1,
        backgroundColor: '#313F4E',
        paddingTop: 5,
        paddingBottom: 5,
        borderRadius: 10
    }
}

export default Filter;