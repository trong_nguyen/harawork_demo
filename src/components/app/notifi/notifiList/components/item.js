import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { Utils, settingApp, imgApp, ButtonCustom } from '../../../../../public';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../../state/action';

class Item extends Component {
    constructor(props) {
        super(props);

        const { item } = props.obj;
        const { isReaded, isStarred } = item;
        this.state = {
            isReaded,
            isStarred
        }
    }

    componentWillReceiveProps(nextProps) {
        const { item } = this.props.obj;
        const { checkRead, checkStart } = nextProps.notify;
        if (checkStart && checkStart.id === item.id) {
            this.props.actions.notify.checkStart(null);
            this.setState({ isStarred: checkStart.status });
        }
        if (checkRead && checkRead.id === item.id) {
            this.props.actions.notify.checkRead(null);
            this.setState({ isReaded: true });
        }
    }

    statusColor(status) {
        let statusColor = null;
        switch (status) {
            case 1:
                statusColor = '#F0AD4E';
                break;
            case 2:
                statusColor = '#F68139';
                break;
            case 3:
                statusColor = '#D9534F';
                break;
            case 5:
                statusColor = '#D9534F';
                break;
            case 6:
                statusColor = '#F68139';
                break;
            case 8:
                statusColor = '#BFC7CF';
                break;
            default:
                statusColor = '#5CB85C';
                break;
        }
        return statusColor;
    }

    star(id) {
        this.setState({ isStarred: !this.state.isStarred },
            () => this.props.statusStar(id, this.state.isStarred));
    }

    changeReaded() {
        this.setState({ isReaded: true });
    }

    gotoDetail(data) {
        this.props.navigation.navigate('NotifiDetail', { item: { ...data, isStarred: this.state.isStarred } })
    }

    render() {
        const { item } = this.props.obj;
        const { name, updatedAt, id, status, departmentName } = item;
        const { isReaded, isStarred } = this.state;
        const { width } = settingApp;
        const time = Utils.formatLocalTime(updatedAt, 'L HH:mm');
        let backgroundColor = isReaded ? '#EDEFF7' : '#FFFFFF';
        let fontWeight = isReaded ? 'normal' : 'bold';
        let statusColor = this.statusColor(status);
        return (
            <View>
                <ButtonCustom
                    onPress={() => this.gotoDetail(item)}
                    style={{
                        flex: 1,
                        backgroundColor,
                        height: 70,
                        paddingLeft: 15,
                        paddingRight: 15,
                        borderBottomColor: settingApp.colorSperator,
                        borderBottomWidth: 1
                    }}>
                    <View
                        style={{
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            bottom: 0,
                            width: 5,
                            backgroundColor: statusColor
                        }}
                    />
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ color: '#0279C7', fontSize: 14, fontWeight }}>{departmentName}</Text>
                        <Text style={{ color: settingApp.colorText, fontSize: 14 }}>{time}</Text>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={{ flex: 1 }}>
                            <Text style={{ color: settingApp.colorText, fontSize: 14, fontWeight, maxWidth: (width - 80) }} numberOfLines={1}>{name}</Text>
                        </View>
                    </View>
                </ButtonCustom>
                <View style={{ position: 'absolute', bottom: 0, right: 15, height: 40, width: 50 }}>
                    <TouchableOpacity
                        onPress={() => this.star(id)}
                        activeOpacity={1}
                        style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', backgroundColor: 'transparent' }}>
                        <Image
                            source={isStarred ? imgApp.stared : imgApp.star}
                            style={{ width: 23, height: 23 }}
                            resizeMode='contain'
                        />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(mapStateToProps, mapDispatchToProps)(Item);