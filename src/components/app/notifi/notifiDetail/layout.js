import React, { Component } from 'react';
import { Alert, View, Text, ScrollView, TouchableOpacity, Image } from 'react-native';
import { ButtonCustom, Loading, settingApp, HeaderWapper, imgApp, Utils } from '../../../../public';

import Title from './components/title';
import Info from './components/info';
import Description from './components/description';
import Attachments from './components/attachments';
import History from './components/history';
import Reject from './components/reject';

import * as Animatable from 'react-native-animatable';

import { Ionicons } from '@expo/vector-icons';

class Layout extends Component {

    renderButton() {
        return (
            <View style={{
                flex: 1,
                padding: 10,
                borderTopColor: settingApp.colorSperator,
                borderTopWidth: 1
            }}>
                <ButtonCustom
                    onPress={() => this.setState({ modalVisible: true })}
                    style={{
                        flex: 1,
                        height: 40,
                        borderRadius: 5,
                        backgroundColor: '#D9534F',
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginBottom: 20
                    }}>
                    <Text style={{ color: '#ffffff', fontSize: 14, fontWeight: 'bold' }}>
                        Từ chối
                    </Text>
                </ButtonCustom>
                <ButtonCustom
                    onPress={() => {
                        Alert.alert(
                            'Xác nhận duyệt thông báo',
                            'Bạn muốn duyệt thông báo này ?',
                            [
                                { text: 'Huỷ', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                                { text: 'Đồng ý', onPress: () => this.approved() },
                            ],
                            { cancelable: false }
                        )
                    }}
                    style={{
                        flex: 1,
                        height: 40,
                        borderRadius: 5,
                        backgroundColor: '#0279C7',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                    <Text style={{ color: '#ffffff', fontSize: 14, fontWeight: 'bold' }}>
                        Duyệt
                    </Text>
                </ButtonCustom>
            </View>
        )
    }

    renderContent(data) {
        let {
            id,
            code,
            name,
            status,
            startedAt,
            endedAt,
            departmentName,
            content,
            attachments,
            typeName,
            approveds,
            tos,
            cCs,
            canPostApproved } = data;
        const checkStatus = status == 2 ? true : false;
        const checkAttachments = attachments && attachments.length > 0 ? true : false;
        return (
            <View style={{ flex: 1 }}>
                <ScrollView>
                    <Title code={code} name={name} status={status} />
                    <View style={{ flex: 1, backgroundColor: '#ffffff', ...settingApp.shadow }}>
                        <Info
                            startedAt={startedAt}
                            endedAt={endedAt}
                            nameDepartment={departmentName}
                            typeName={typeName}
                            status={status}
                            approveds={approveds}
                            tos={tos}
                            cCs={cCs}
                        />
                    </View>

                    <View style={{ flex: 1, backgroundColor: '#ffffff', ...settingApp.shadow, marginTop: 15 }}>
                        <Description content={content} />
                        {checkAttachments && <Attachments 
                            attachments={attachments} 
                            data = {this.state.data}
                            />}
                        {canPostApproved && this.renderButton()}
                    </View>

                    {checkStatus && <History
                        id={id}
                    />}
                    <View style={{ width: settingApp.width, height: checkStatus ? 70 : 20, backgroundColor: 'transparent' }} />
                </ScrollView>
                <Reject
                    modalVisible={this.state.modalVisible}
                    reject={(note) => this.reject(note)}
                    close={() => this.setState({ modalVisible: false })}
                />
            </View>

        )
    }

    renderHeader() {
        const { isStarred, data } = this.state;
        const { id } = data;
        let titleTime = '';
        if (data) {
            titleTime = Utils.formatTime(data.updatedAt, 'L');
            if (titleTime.indexOf(this.year) > -1) {
                titleTime = titleTime.replace(`/${this.year}`, '');
            }
        }
        return (
            <HeaderWapper style={{ justifyContent: 'space-between' }}>
                <TouchableOpacity
                    onPress={() => this.props.navigation.pop()}
                    style={{ backgroundColor: 'transparent', width: 55, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                    <Ionicons name='ios-arrow-back' size={23} color='#9CA7B2' style={{ marginLeft: -15 }} />
                </TouchableOpacity>
                {data && <View style={{ paddingRight: 10, flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={{ color: '#ffffff', fontSize: 14, marginRight: 10 }}>
                        {titleTime}
                    </Text>
                    <TouchableOpacity
                        onPress={() => {
                            this.setState({ isStarred: !this.state.isStarred },
                                async () => {
                                    this.statusStar(id, this.state.isStarred);
                                    this.props.actions.notify.checkStart({ id, status: this.state.isStarred });
                                }
                            );
                        }}
                        activeOpacity={1}
                    >
                        <Image
                            source={isStarred ? imgApp.stared : imgApp.star}
                            style={{ width: 23, height: 23, marginBottom: 5 }}
                            resizeMode='contain'
                        />
                    </TouchableOpacity>
                </View>}
            </HeaderWapper>
        )
    }

    render() {
        const { data } = this.state;
        return (
            <View style={{ flex: 1 }}>
                {this.renderHeader()}
                <View style={{ flex: 1 }}>
                    {this.renderContent(data)}
                    {this.state.isApproved && <Loading />}
                </View>
            </View>
        );
    }
}

export default Layout;