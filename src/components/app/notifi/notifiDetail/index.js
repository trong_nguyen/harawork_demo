import Layout from './layout';

import { Toast } from '../../../../public';

import { HaravanIc, HaravanHr } from '../../../../Haravan';

import moment from 'moment';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../state/action';

class NotifiDetail extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            data: props.navigation.state.params.item,
            isApproved: false,
            modalVisible: false,
            isStarred: props.navigation.state.params.item.isStarred,
            isReload: true,
        }

        this.colleague = props.app.colleague;
        this.year = moment().format('YYYY');
    }

    checkData(input) {
        if (input && input.data && !input.error) {
            return input
        } else {
            return false;
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps && nextProps.app && nextProps.app.reloadNotify && nextProps.app.reloadNotify==true ){
            this.setState({isLoading: true}, () =>{
                this.loadData() 
                this.props.actions.reloadNotify(null)
            })
        }
    }

    checkReload(){
        const { isReload } = this.state;
        if(isReload == true){
            this.loadData()
        }
    }

    componentDidMount() {
        this.loadData()
    }

   async loadData(){
        let { id, status } = this.state.data;
        status = (status == 2) ? true : false;
        const result = await HaravanIc.readNotify(id, this.colleague, status);
        if (result) {
            let resultIsReaded = await HaravanIc.isReaded(id, this.colleague);
            if (resultIsReaded && !resultIsReaded.error) {
                this.props.actions.notify.checkRead({ id: id, status: true });
            }
        } else {
            Toast.show('Thông báo không tồn tại');
            this.props.navigation.pop();
        }
        this.setState({isReaded:false, isReload:false})
    }

    approved() {
        const data = {
            announcement: { ...this.state.data },
            userInfo: { ...this.colleague }
        }
        this.setState({ isApproved: true }, async () => {
            let result = await HaravanIc.approved(data);
            result = this.checkData(result);
            if (result) {
                await Toast.show(`Đã duyệt thông báo ${this.state.data.code}.`);
                this.props.actions.notify.refreshListNotify(true);
                this.props.navigation.pop();
            } else {
                Toast.show('Duyệt thông báo thất bại. Vui lòng thử lại.');
            }
            this.setState({ isApproved: false });
        })
    }

    reject(note) {
        const data = {
            announcement: { ...this.state.data, reasonLog: note },
            userInfo: { ...this.colleague }
        }
        this.setState({ isApproved: true }, async () => {
            let result = await HaravanIc.reject(data);
            result = this.checkData(result);
            if (result) {
                await Toast.show(`Đã từ chối thông báo ${this.state.data.code}.`);
                this.props.actions.notify.refreshListNotify(true);
                this.props.navigation.pop();
            } else {
                Toast.show('Từ chối thông báo thất bại. Vui lòng thử lại.');
            }
            this.setState({ isApproved: false });
        })
    }

    async statusStar(id, status) {
        const result = await HaravanIc.statusStar(id, status);
        if (!result.error) {
            Toast.show(`${status ? 'Đánh dấu' : 'Bỏ đánh dấu'} thành công`)
        } else if (result.error) {
            Toast.show('Lỗi')
        }
    }

    componentWillUnmount() {
        this.props.actions.notify.checkStart(null);
        this.props.actions.notify.checkRead(null);
        this.props.actions.notify.refreshListNotify(null);
    }

}


const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(mapStateToProps, mapDispatchToProps)(NotifiDetail);