import React, { Component } from 'react';
import { View, Modal, CameraRoll, Image, TouchableOpacity, Platform, WebView, Text } from 'react-native';
import { settingApp, imgApp, Toast, PDFReader, ReviewImage, Loading } from '../../../../../public';
import * as FileSystem from 'expo-file-system';
import { Ionicons, MaterialCommunityIcons } from '@expo/vector-icons';
import { connect } from 'react-redux';

class OpenFile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dataSelected: null,
            type: null,
            isDownload: false,
            isLoad:false
        }
        this.userInfo = props.app.authHaravan.auth.access_token;

    }

    componentWillReceiveProps(nextProps) {
        if (JSON.stringify(this.state.dataSelected) !== JSON.stringify(nextProps.dataSelected)) {
            const { dataSelected } = nextProps;
            const { url, name } = dataSelected;
            let type = null;
            let isDownload = false;
            if ((name.match(/\.(jpg|jpeg|png|gif)$/) != null)) {
                type = 'image';
            } else if ((name.match(/\.(pdf)$/) != null)) {
                type = 'pdf';
            }
            if ((url.match(/\.(jpg|jpeg|png|gif|pdf|doc|docx|xls|xlsx|txt)$/) != null)) {
                //isDownload = true;
            }
            this.setState({ dataSelected: nextProps.dataSelected, type, isDownload });
        }
    }

    download() {
        const { dataSelected } = this.state;
        const { url } = dataSelected;
        const nameFile = url.slice(url.lastIndexOf('/') + 1);
        const isImage = (this.state.type == 'image');
        let msg = isImage ? 'Đã lưu ảnh' : 'Đã lưu file';
        FileSystem.downloadAsync(url, FileSystem.documentDirectory + nameFile).
            then( async ({ uri }) => {
                this.props.close();
                Toast.show(msg);
                if(isImage) {
                    await CameraRoll.saveToCameraRoll(uri);
                    await FileSystem.deleteAsync(uri, false);
                }
                //console.log('Finished downloading to ', uri);
            })
            .catch(error => {
                console.error(error);
            });
    }

    renderHeader() {
        const { width, isIPhoneX } = settingApp;
        const top = isIPhoneX ? 44 : Platform.OS === 'ios' ? 30 : 0;
        return (
            <View style={{ position: 'absolute', top, left: 0, right: 0, ...settingApp.shadow }}>
                <View style={{ width, height: 44, flexDirection: 'row', justifyContent: 'space-between' }} >
                    <TouchableOpacity
                        onPress={() => this.props.close()}
                        style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center' }}
                    >
                        <Ionicons
                            name='md-close'
                            color='#9CA7B2'
                            size={30}
                        />
                    </TouchableOpacity>
                    {/* <View style={{ flex: 1, justifyContent: 'center' }}>
                        <Text>{dataSelected.name}</Text>
                    </View> */}
                    {this.state.isDownload && <TouchableOpacity
                        onPress={() => this.download()}
                        style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center' }}
                    >
                        <Image
                            source={imgApp.download}
                            style={{ width: 20, height: 20 }}
                        />
                    </TouchableOpacity>}
                </View>
            </View>
        )
    }

    renderViewIOS(){
        const { type, dataSelected } = this.state;
        const { url } = this.props
        if(dataSelected && url){
            return(
                <WebView
                    style={{flex:1}}
                        bounces={false}
                        scrollEnabled={false} 
                        source={{ uri:url,
                            method: "GET",
                            credentials: 'include',
                            headers: {
                                'Authorization': `Bearer ${this.userInfo}`
                            }}}   
                        useWebKit={true}
                        originWhitelist={['*']}     
                            />
            )
        }
        else{
            return(
                <View />               
            )
        }
    }

    renderView() {
        const { type, dataSelected } = this.state;
        const { url } = this.props 
        let content = <View />;
        if(dataSelected && url){
            if (type == 'image') {
                content = ( 
                        <WebView
                            style={{flex:1}}
                                bounces={false}
                                scrollEnabled={false} 
                                source={{ uri:url,
                                    method: "GET",
                                    credentials: 'include',
                                    headers: {
                                        'Authorization': `Bearer ${this.userInfo}`
                                    }}}        
                            />
                )
            }
            else{
                content = ( 
                    <View style={{flex:1, backgroundColor:'#FFFFFF', justifyContent:'center', alignItems:'center'}}>
                        <MaterialCommunityIcons name='file-multiple' size={50} color={settingApp.colorDisable} />
                        <Text style={{fontSize:16, paddingTop:10, color:settingApp.colorDisable}}> Tính năng đang được xây dựng</Text>
                    </View>    
                )

            }
        }
        else{
            content =  <View />               
        }

        return content;

    }

    renderLoading(){
        return(
            <Loading/>
        )
    }

    renderContent() {
        const { isIPhoneX } = settingApp;
        const { dataSelected } = this.state;
        const marginTop = isIPhoneX ? 88 : Platform.OS === 'ios' ? 74 : 44;
        let content = Platform.OS === 'ios' ? this.renderViewIOS() : this.renderView()
        return (
            <View style={{flex:1, marginTop}}>
            {content}
                     {/* <WebView
                    style={{flex:1}}
                    style={{flex:1}}
                        bounces={false}
                        scrollEnabled={false} 
                        source={{ uri:this.props.url,
                            method: "GET",
                            credentials: 'include',
                            headers: {
                                'Authorization': `Bearer ${this.userInfo}`
                            }}}        
                            /> */}
            </View>
            
        )
    }

    render() {
        const { isLoad } = this.state
        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.props.modalVisible}
                onRequestClose={() => this.props.close()}>
                <View style={{ flex: 1, backgroundColor: '#000' }}>
                    {isLoad? <Loading/>
                    :
                        this.renderContent()}
                    {this.renderHeader()}
                </View>
            </Modal>
        )
    }
}

const mapStateToProps = (state) => ({ ...state })
export default connect(mapStateToProps)(OpenFile);