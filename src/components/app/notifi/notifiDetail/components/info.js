import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { HaravanHr } from '../../../../../Haravan';
import { settingApp, Utils } from '../../../../../public';
import Collapsible from 'react-native-collapsible';
import moment from 'moment';

import { FontAwesome } from '@expo/vector-icons';

class Info extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isCollapsed: true,
            listUserApproved: [],
            listLifeApproved: [],
            isWaitingApproved: false,
            listTos: [],
            listcCs: []
        }

        this.isApproved = props.status == 2 ? false : true;
        this.today = moment().subtract(0, 'days').format('L');
    }

    checkData(input) {
        if (input && input.data && !input.error) {
            return input
        } else {
            return false;
        }
    }

    async getDataFromList(data) {
        if (data && data.length > 0) {
            let list = [];
            for (let i = 0; i < data.length; i++) {
                let { type, numberId, departmentId } = data[i];
                numberId = numberId ? numberId : data[i].approvedBy;
                let resultData = null;
                let resultDefaul = 'Tất cả nhân viên';
                switch (type) {
                    case 1:
                        resultData = await HaravanHr.getDepartmentsById(numberId);
                        resultData = this.checkData(resultData);
                        if (resultData && resultData.data && resultData.data.name) {
                            list.push(resultData.data.name);
                        }
                        break;
                    case 2:
                        resultData = await HaravanHr.getJobtitleById(numberId);
                        resultData = this.checkData(resultData);
                        let resultDepartment = null;
                        if (resultData && resultData.data && resultData.data.name) {
                            if (departmentId) {
                                resultDepartment = await HaravanHr.getDepartmentsById(departmentId);
                                resultDepartment = this.checkData(resultDepartment);
                                if (resultDepartment) {
                                    resultDepartment = resultDepartment.data.name;
                                } else {
                                    resultDepartment = null
                                }
                            }
                            list.push(`${resultData.data.name}${resultDepartment ? ` tại ${resultDepartment}` : ` tất cả ${resultData.data.name}`}`);
                        }
                    case 3:
                        break;
                    case 4:
                        resultData = await HaravanHr.getColleague(numberId);
                        resultData = this.checkData(resultData);
                        if (resultData && resultData.data && resultData.data.fullName) {
                            list.push(resultData.data.fullName);
                        }
                        break;
                    default:
                        list.push(resultDefaul);
                        break;
                }
            }
            return list;
        }
    }

    async componentDidMount() {
        const { approveds, tos, cCs } = this.props;
        let { listUserApproved, isWaitingApproved, listTos, listcCs, listLifeApproved } = this.state;
        if (approveds && approveds.length > 0) {
            for (let i = 0; i < approveds.length; i++) {
                if (approveds[i].approvedByUser) {
                    let result = await HaravanHr.getColleague(approveds[i].approvedByUser);
                    result = this.checkData(result);
                    if (result) {
                        listUserApproved.push(result.data);
                    } else {
                        listUserApproved.push(null);
                    }
                } else {
                    isWaitingApproved = true;
                }
                let resultApproved = await this.getDataFromList([{ ...approveds[i] }]);
                listLifeApproved.push({
                    name: resultApproved,
                    isApproved: approveds[i].isApproved,
                    isRejected: approveds[i].isRejected
                })
            }
            this.setState({ listUserApproved, isWaitingApproved });
        }
        listTos = await this.getDataFromList(tos);
        listcCs = await this.getDataFromList(cCs);
        this.setState({ listTos, listcCs, listLifeApproved });
    }

    renderUserApproved() {
        const { listUserApproved, isWaitingApproved } = this.state;
        if (!isWaitingApproved) {
            return (<Text style={{ color: '#0279C7', fontSize: 14 }}>{listUserApproved.map((e, i) => `${e.fullName}${i < (listUserApproved.length - 1) ? ', ' : ''}`)}</Text>)
        } else {
            return (<Text style={{ color: '#0279C7', fontSize: 14 }}>Đang chờ duyệt</Text>)
        }
    }

    renderList(data) {
        return (<Text style={{ color: '#0279C7', fontSize: 14 }}>{data.map((e, i) => `${e}${i < (data.length - 1) ? ', ' : ''}`)}</Text>)
    }

    renderLifeApproved() {
        const { listLifeApproved } = this.state;
        return (
            <View style={{ flex: 1 }}>
                {listLifeApproved.map((item, index) => {
                    const { name, isApproved, isRejected } = item;
                    return (
                        <View style={{ flex: 1, marginTop: index == 0 ? 0 : 10 }} key={index}>
                            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                <FontAwesome
                                    name={isApproved ? 'check-circle' : isRejected ? 'minus-circle' : 'info-circle'}
                                    color={isApproved ? '#27ae60' : isRejected ? '#eb5757' : '#F0AD4E'}
                                    size={15}
                                />
                                <Text style={{ marginLeft: 5, color: settingApp.colorText, fontSize: 14, fontWeight: 'bold' }}>{`Vòng duyệt #${index + 1}`}</Text>
                                <Text style={{ marginLeft: 5, color: settingApp.colorText, fontSize: 14 }}>{isApproved ? 'Đã duyệt' : isRejected ? 'Đã từ chối' : 'Đang chờ duyệt'}</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', marginTop: 10 }}>
                                <Text style={{ marginLeft: 20, color: settingApp.colorText, fontSize: 14 }}>Chức vụ duyệt:</Text>
                                <Text style={{ color: '#0279C7', fontSize: 14, marginLeft: 5, maxWidth: (settingApp.width * 0.3) }}>{name}</Text>
                            </View>
                        </View>
                    )
                })}
            </View>
        )
    }

    checkTime(time) {
        const getTime = Utils.formatTime(time, 'L');
        if (getTime == this.today) {
            time = `Hôm nay`
        } else {
            time = `${Utils.formatTime(time, 'L')}`
        }
        return time;
    }

    renderisApproved() {
        if (this.isApproved) {
            return (
                <View style={{ flex: 1 }}>
                    <View style={{ flex: 1, flexDirection: 'row', marginTop: 15 }}>
                        <View style={{ flex: 0.45 }}>
                            <Text style={{ color: '#9CA7B2', fontSize: 12 }}>Người duyệt:</Text>
                        </View>

                        <View style={{ flex: 1 }}>
                            <Text style={{ color: '#0279C7', fontSize: 14 }}>{this.renderUserApproved()}</Text>
                        </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: 'row', marginTop: 15 }}>
                        <View style={{ flex: 0.45 }}>
                            <Text style={{ color: '#9CA7B2', fontSize: 12 }}>Vòng duyệt:</Text>
                        </View>

                        <View style={{ flex: 1 }}>
                            {this.renderLifeApproved()}
                        </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: 'row', marginTop: 15 }}>
                        <View style={{ flex: 0.45 }}>
                            <Text style={{ color: '#9CA7B2', fontSize: 12 }}>Người nhận:</Text>
                        </View>

                        <View style={{ flex: 1 }}>
                            <Text style={{ color: '#0279C7', fontSize: 14 }}>{this.renderList(this.state.listTos)}</Text>
                        </View>
                    </View>

                    {this.props.cCs.length > 0 &&
                        <View style={{ flex: 1, flexDirection: 'row', marginTop: 15 }}>
                            <View style={{ flex: 0.45 }}>
                                <Text style={{ color: '#9CA7B2', fontSize: 12 }}>CC:</Text>
                            </View>

                            <View style={{ flex: 1 }}>
                                <Text style={{ color: '#0279C7', fontSize: 14 }}>{this.renderList(this.state.listcCs)}</Text>
                            </View>
                        </View>}
                </View>
            )
        } else {
            return (
                <View style={{ flex: 1 }}>
                    <View style={{ flex: 1, flexDirection: 'row', marginTop: 15 }}>
                        <View style={{ flex: 0.45 }}>
                            <Text style={{ color: '#9CA7B2', fontSize: 12 }}>Người duyệt:</Text>
                        </View>

                        <View style={{ flex: 1 }}>
                            <Text style={{ color: '#0279C7', fontSize: 14 }}>{this.renderUserApproved()}</Text>
                        </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: 'row', marginTop: 15 }}>
                        <View style={{ flex: 0.45 }}>
                            <Text style={{ color: '#9CA7B2', fontSize: 12 }}>Vòng duyệt:</Text>
                        </View>

                        <View style={{ flex: 1 }}>
                            {this.renderLifeApproved()}
                        </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: 'row', marginTop: 15 }}>
                        <View style={{ flex: 0.45 }}>
                            <Text style={{ color: '#9CA7B2', fontSize: 12 }}>Người nhận:</Text>
                        </View>

                        <View style={{ flex: 1 }}>
                            <Text style={{ color: '#0279C7', fontSize: 14 }}>{this.renderList(this.state.listTos)}</Text>
                        </View>
                    </View>

                    {this.props.cCs.length > 0 &&
                        <View style={{ flex: 1, flexDirection: 'row', marginTop: 15 }}>
                            <View style={{ flex: 0.45 }}>
                                <Text style={{ color: '#9CA7B2', fontSize: 12 }}>CC:</Text>
                            </View>

                            <View style={{ flex: 1 }}>
                                <Text style={{ color: '#0279C7', fontSize: 14 }}>{this.renderList(this.state.listcCs)}</Text>
                            </View>
                        </View>}
                </View>
            )
        }
    }

    render() {
        let { startedAt, endedAt, nameDepartment, typeName } = this.props;
        startedAt = startedAt ? this.checkTime(startedAt) : 'khi được duyệt';
        endedAt = endedAt ? this.checkTime(endedAt) : 'vô thời hạn';
        return (
            <View style={{
                flex: 1,
                padding: 10,
                paddingBottom: 0
            }}>

                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ flex: 0.45 }}>
                        <Text style={{ color: '#9CA7B2', fontSize: 12 }}>Đơn vị ban hành:</Text>
                    </View>

                    <View style={{ flex: 1 }}>
                        <Text style={{ color: '#0279C7', fontSize: 14 }}>{nameDepartment}</Text>
                    </View>
                </View>

                <Collapsible collapsed={this.state.isCollapsed}>
                    <View style={{ flex: 1, flexDirection: 'row', marginTop: 15 }}>
                        <View style={{ flex: 0.45 }}>
                            <Text style={{ color: '#9CA7B2', fontSize: 12 }}>Loại thông báo:</Text>
                        </View>

                        <View style={{ flex: 1 }}>
                            <Text style={{ color: settingApp.colorText, fontSize: 14, fontWeight: 'bold' }}>{typeName}</Text>
                        </View>
                    </View>
                </Collapsible>

                <View style={{ flex: 1, flexDirection: 'row', marginTop: 15 }}>
                    <View style={{ flex: 0.45 }}>
                        <Text style={{ color: '#9CA7B2', fontSize: 12 }}>Hiệu lực:</Text>
                    </View>

                    <View style={{ flex: 1 }}>
                        <Text style={{ color: '#9CA7B2', fontSize: 12 }}>
                            Từ
                            <Text style={{ color: settingApp.colorText, fontSize: 14, fontWeight: 'bold' }}> {startedAt} </Text>
                            đến
                            <Text style={{ color: settingApp.colorText, fontSize: 14, fontWeight: 'bold' }}> {endedAt}</Text>
                        </Text>
                    </View>
                </View>

                <Collapsible collapsed={this.state.isCollapsed}>
                    {this.renderisApproved()}
                </Collapsible>

                <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => this.setState({ isCollapsed: !this.state.isCollapsed })}
                    style={{ height: 40, justifyContent: 'flex-end', alignItems: 'center', paddingBottom: 10 }}
                >
                    <FontAwesome
                        name={`angle-double-${this.state.isCollapsed ? 'down' : 'up'}`}
                        size={15}
                        color='#9CA7B2'
                    />
                </TouchableOpacity>

            </View>
        )
    }
}

export default Info;