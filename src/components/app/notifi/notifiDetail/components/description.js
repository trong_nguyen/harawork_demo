import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { ParseText, settingApp } from '../../../../../public';

class Description extends Component {

    render() {
        const { content } = this.props;
        return (
            <View style={{
                flex: 1,
                padding: 10,
                paddingBottom: 0
            }}>
                <Text style={{ fontSize: 16, color: settingApp.colorText, fontWeight: 'bold' }}>Nội dung</Text>
                <ParseText description={content} />
            </View>
        )
    }
}

export default Description;