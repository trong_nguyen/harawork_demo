import React, { Component } from 'react';
import { View, Text, FlatList, ImageBackground, Image, Platform, TouchableOpacity, WebView } from 'react-native';
import { imgApp, Utils, settingApp, ButtonCustom, ReviewImage, DownloadFile, Toast } from '../../../../../public';
import OpenFile from './openFile';
import Config from '../../../../../Haravan/config';
import { connect } from 'react-redux';
import * as WebBrowser  from 'expo-web-browser';

class Attachments extends Component {
    constructor(props) {
        super(props);

        this.state = {
            attachments: props.attachments,
            dataSelected: null,
            modalVisible: false,
            url: null
        }
        this.userInfo = props.app.authHaravan.auth.access_token;
    }

    async downloadFileWeb(item){
        const url = `https://ic.${Config.apiHost}/call/im_api/announcements/${this.props.data.id}/attachment?attachmentId=${item.id}&access_token=${this.userInfo}`
        await WebBrowser.openBrowserAsync(url)
  }

    async openFile(link) { 
        await WebBrowser.openBrowserAsync(link);
        //DownloadFile(url)
    }

    jsCode(){
        const htmlContent = `
            <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <style>
                body {
                    width: 50%;
                    height: 50%;
                    overflow: hidden;
                    margin: 0;
                }
        
                html {
                    width: 50%;
                    height: 50%;
                    overflow: hidden;
                }
                [contenteditable]:focus {
                    outline: 0px solid transparent;
                }
            </style>
        </html>
        `
        return htmlContent
    }

    renderItem(obj) {
        const {data } = this.props
        const { item } = obj;
        let typeUrl = Utils.checkTypeUrl(item.name)
        var url = `https://ic.${Config.apiHost}/api/announcements/${data.id}/attachment?attachmentId=${item.id}`;
        if(Platform.OS === 'ios'){
            return(
                <TouchableOpacity
                onPress={() => this.setState({url:url, dataSelected:item},() =>{
                    this.setState({modalVisible:true})
                })}
                style={{
                        width: 130,
                        height: 90,
                        marginRight: 10,
                        backgroundColor: '#EDEFF7',
                        ...settingApp.shadow,
                        borderRadius: 3
                    }}
                >
                    <WebView
                        style={{flex:1}}
                        bounces={false}
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        scrollEnabled={false} 
                        source={{ uri:url,
                            method: "GET",
                            credentials: 'include',
                            headers: {
                                'Authorization': `Bearer ${this.userInfo}`
                            }}}
                        />
                </TouchableOpacity>
            )
        }
        else{
            let content = <View />
            if (typeUrl == 'image') {
                content = (
                    <ImageBackground
                        source={imgApp.noImage}
                        style={{ flex: 1, width: undefined, height: undefined, justifyContent:'center', alignItems:'center' }}
                        resizeMode='center'
                    >
                        <WebView
                        style={{flex:1, width:120, height:80}}
                        bounces={false}
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        scrollEnabled={false} 
                        source={{ uri:url,
                            method: "GET",
                            credentials: 'include',
                            headers: {
                                'Authorization': `Bearer ${this.userInfo}`
                            }}}
                        />
                    </ImageBackground>
                );
            }  
            else{
                content = (
                    <View style={{ flex: 1, padding: 10, justifyContent: 'space-between' }}>
                        <Text style={{ color: '#474747', fontSize: 10 }}>{item.name}</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image
                                source={imgApp[`${typeUrl}`]}
                                style={{ width: 20, height: 20, marginRight: 10 }}
                                resizeMode='contain'
                            />
                            <Text style={{ color: settingApp.colorText, fontSize: 10 }}>{typeUrl}</Text>
                        </View>
                    </View>
                );
            }        
        
        return (
                <ButtonCustom
                    // onPress={() => {
                    //     this.setState({url:url, dataSelected:item},() =>{
                    //     this.setState({modalVisible:true})
                    //     })
                    // }}
                    onPress={() => 
                    {(typeUrl == 'image') ? (
                        this.setState({url:url, dataSelected:item},() =>{
                        this.setState({modalVisible:true})
                    }) ) 
                    : this.downloadFileWeb(item)
                    } }
                    style={{
                        width: 130,
                        height: 90,
                        marginRight: 10,
                        backgroundColor: '#EDEFF7',
                        ...settingApp.shadow,
                        borderRadius: 3
                    }}>
                        {content}
                </ButtonCustom>
            )}                
    }

    render() {
        return (
            <View style={{ flex: 1, padding: 10 }}>
                <Text style={{ color: settingApp.colorText, fontSize: 16, marginBottom: 20, fontWeight: 'bold' }}>
                    File đính kèm
                </Text>
                <FlatList
                    data={this.state.attachments}
                    extraData={this.state}
                    keyExtractor={(item, index) => '' + index}
                    renderItem={obj => this.renderItem(obj)}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                />
                <OpenFile
                    modalVisible={this.state.modalVisible}
                    dataSelected={this.state.dataSelected}
                    url ={this.state.url}
                    close={() => {
                        this.dataIndex = null;
                        this.setState({ modalVisible: false })
                    }}
                />
            </View>
        )
    }
}

const mapStateToProps = (state) => ({ ...state })
export default connect(mapStateToProps)(Attachments);