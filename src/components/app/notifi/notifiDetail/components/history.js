import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { HaravanIc, HaravanHr } from '../../../../../Haravan';
import { settingApp, Utils, TimeLine } from '../../../../../public';
import Collapsible from 'react-native-collapsible';

class History extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isCollapsible: true,
            refLog: []
        }
    }

    checkData(input) {
        if (input && input.data && !input.error) {
            return input
        } else {
            return false;
        }
    }

    async componentDidMount() {
        const { id } = this.props;
        let reslutLogs = await HaravanIc.logs(id);
        reslutLogs = this.checkData(reslutLogs);
        let refLog = [];
        if (reslutLogs && reslutLogs.data && reslutLogs.data.data.length > 0) {
            const { data } = reslutLogs.data;
            for (let i = 0; i < data.length; i++) {
                let item = { fullName: '', actions: null, time: null }
                let result = await HaravanHr.getColleague(data[i].createdBy);
                result = this.checkData(result);
                if (result) {
                    item.fullName = result.data.fullName;
                }
                item.actions = Utils.checkAnnouncementcheck(data[i].refAction);
                item.time = Utils.formatTime(data[i].createdAt, 'L HH:mm');
                refLog.push(item);
            }
        }

        if (refLog.length > 0) {
            this.setState({ refLog }, () => this.setState({ isCollapsible: false }));
        }
    }

    render() {
        const { refLog } = this.state;
        return (
            <View style={{ flex: 1, marginTop: 10 }}>
                <Collapsible collapsed={this.state.isCollapsible} duration={500}>
                    <View style={{ flex: 1, padding: 10 }}>
                        <Text style={{ color: settingApp.colorText, fontSize: 16, fontWeight: 'bold' }}>Lịch sử</Text>
                        <View style={{ flex: 1, marginTop: 20 }}>
                            <TimeLine
                                data={refLog}
                            />
                        </View>
                    </View>
                </Collapsible>
            </View>
        )
    }
}

export default History;