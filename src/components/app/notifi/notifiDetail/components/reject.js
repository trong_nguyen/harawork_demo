import React, { Component } from 'react';
import { View, Text, Modal, TouchableOpacity, TextInput } from 'react-native';
import { settingApp } from '../../../../../public';
import * as Animatable from 'react-native-animatable';

class Reject extends Component {
    constructor(props) {
        super(props);

        this.state = {
            note: ''
        }
    }

    async cancel() {
        //await this.slide.fadeOutUp();
        this.props.close();
    }

    async reject() {
        //await this.slide.fadeOutUp();
        this.props.reject(this.state.note);
        this.props.close();
    }

    render() {
        const { statusBarHeight, width, height, colorSperator, colorText } = settingApp;
        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={this.props.modalVisible}
                onRequestClose={() => this.props.close()}>
                <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => console.log('AAAAA')}
                    style={{
                        flex: 1,
                        backgroundColor: 'rgba(0,0,0,0.5)',
                        paddingTop: (60 + statusBarHeight),
                        alignItems: 'center'
                    }}>
                    <Animatable.View
                        animation='fadeInDown'
                        duration={500}
                        ref={refs => this.slide = refs}
                    >
                        <TouchableOpacity
                            activeOpacity={1}
                            style={{
                                width: (width - 40),
                                height: (height * 0.4),
                                backgroundColor: '#ffffff',
                                borderRadius: 3
                            }}>
                            <View style={{ flex: 1 }}>
                                <View style={{
                                    width: (width - 40),
                                    height: 44,
                                    paddingLeft: 10,
                                    justifyContent: 'center',
                                    borderBottomColor: colorSperator,
                                    borderBottomWidth: 1
                                }}>
                                    <Text
                                        style={{ color: colorText, fontSize: 14, fontWeight: 'bold' }}
                                    >
                                        YÊU CẦU NHẬP
                                    </Text>
                                </View>

                                <View style={{ flex: 1, padding: 10 }}>
                                    <Text
                                        style={{ color: colorText, fontSize: 12 }}
                                    >
                                        LÝ DO TỪ CHỐI
                                    </Text>

                                    <TouchableOpacity
                                        activeOpacity={1}
                                        onPress={() => this.note.focus()}
                                        style={{
                                            flex: 1,
                                            height: 80,
                                            borderRadius: 2,
                                            borderWidth: 1,
                                            marginTop: 10,
                                            borderColor: colorSperator,
                                        }}>
                                        <TextInput
                                            ref={refs => this.note = refs}
                                            value={this.state.note}
                                            onChangeText={note => this.setState({ note })}
                                            style={{
                                                maxHeight: (height * 0.4) - 140,
                                                padding: 5,
                                                paddingBottom: 0
                                            }}
                                            multiline={true}
                                            underlineColorAndroid='rgba(0,0,0,0)'
                                            autoCorrect={false}
                                            autoCapitalize='none'
                                        />
                                    </TouchableOpacity>
                                </View>

                                <View style={{
                                    width: (width - 40),
                                    height: 70,
                                    borderTopColor: colorSperator,
                                    borderTopWidth: 1,
                                    flexDirection: 'row',
                                    justifyContent: 'flex-end',
                                    alignItems: 'center',
                                }}>
                                    <TouchableOpacity
                                        onPress={() => this.cancel()}
                                        style={{
                                            width: (width - 40) * 0.23,
                                            height: 50,
                                            backgroundColor: '#FFFFFF',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            marginRight: 20,
                                            borderColor: colorSperator,
                                            borderRadius: 3,
                                            borderWidth: 1
                                        }}>
                                        <Text style={{ color: colorText, fontSize: 14, fontWeight: '600' }}>
                                            Huỷ
                                        </Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        onPress={() => this.reject()}
                                        style={{
                                            width: (width - 40) * 0.23,
                                            height: 50,
                                            backgroundColor: '#D9534F',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            marginRight: 10,
                                            borderColor: colorSperator,
                                            borderRadius: 3,
                                            borderWidth: 1
                                        }}>
                                        <Text style={{ color: '#ffffff', fontSize: 14, fontWeight: '600' }}>
                                            Từ chối
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </Animatable.View>
                </TouchableOpacity>
            </Modal>
        )
    }

}

export default Reject;