import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { settingApp } from '../../../../../public';

class Title extends Component {

    checkStatus(status) {
        let color = null;
        let name = null;
        switch (status) {
            case 1:
                color = '#F0AD4E';
                name = 'Nháp'
                break;
            case 2:
                color = '#F68139';
                name = 'Chờ duyệt';
                break;
            case 3:
                color = '#D9534F';
                name = 'Từ chối';
                break;
            case 5:
                color = '#D9534F';
                name = 'Đã thu hồi';
                break;
            case 6:
                color = '#F68139';
                name = 'Chưa hiệu lực';
                break;
            case 8:
                statusColor = '#BFC7CF';
                name = 'Hết hiệu lực'
                break;
            default:
                color = '#5CB85C';
                name = 'Đang hiệu lực'
                break;
        }
        return { name, color }
    };

    render() {
        let { code, name, status } = this.props;
        const { colorSperator } = settingApp;
        status = this.checkStatus(status);
        return (
            <View style={{ padding: 15 }}>
                <Text style={styles.titleText} numberOfLine={1}>
                    {code} - {name}
                </Text>

                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={[styles.titleStatus, { backgroundColor: status.color }]}>
                        <Text style={{ fontSize: 12, color: '#ffffff' }}>
                            {status.name}
                        </Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = {
    titleText: { fontSize: 16, color: settingApp.colorText, fontWeight: 'bold' },
    titleStatus: { padding: 5, paddingLeft: 20, paddingRight: 20, borderRadius: 30 >> 1, marginTop: 10 }
}

export default Title;