import React, { Component } from 'react';
import { ActivityIndicator, View, Text, TextInput, FlatList, TouchableOpacity } from 'react-native';
import { settingApp, ButtonCustom, HeaderWapper } from '../../../../public';
import Item from '../notifiList/components/item';
import * as Animatable from 'react-native-animatable';

import { Ionicons } from '@expo/vector-icons';

class Layout extends Component {

    renderItem(obj) {
        const { item } = obj;
        return (
            <View
                style={{ flex: 1 }}
            >
                <Item
                    obj={{ item }}
                    statusStar={(id, status) => this.statusStar(id, status)}
                    navigation={this.props.navigation}
                    refreshing={this.state.refreshing}
                    colleague={this.colleague}
                    onRefresh={() => this.refreshList()}
                />
            </View>
        )
    }

    renderLoading() {
        return (
            <View style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                backgroundColor: 'rgba(255,255,255,0)',
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <ActivityIndicator size='large' color={settingApp.color} />
            </View>
        )
    }

    renderHeader() {
        const { value } = this.state;
        return (
            <HeaderWapper backgroundColor='#ffffff'>
                <TouchableOpacity
                    onPress={() => {
                        if (this.back) {
                            this.back = false;
                            this.props.navigation.pop();
                        }
                    }}
                    style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                    <Ionicons name='ios-arrow-back' size={23} color='#9CA7B2' />
                </TouchableOpacity>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <TextInput
                        style={{ flex: 1 }}
                        value={value}
                        onChangeText={(value) => this.changeText(value)}
                        autoCapitalize='none'
                        underlineColorAndroid='transparent'
                        numberOfLines={1}
                        placeholder={'Tìm theo mã, tên, nội dung'}
                        ref={refs => this.input = refs}
                    />
                </View >
                {value.length > 0 &&
                    <ButtonCustom
                        onPress={() => this.changeText('')}
                        style={{
                            width: 40,
                            height: 44,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: 'transparent'
                        }}>
                        <Ionicons name='md-close' color='#9CA7B2' size={25} />
                    </ButtonCustom>}
            </HeaderWapper>
        )
    }

    render() {
        const { isLoading, isLoad } = this.state;
        const { width, height, statusBarHeight } = settingApp;
        let content = <View />
        if (!isLoad) {
            content = (
                <View style={{ flex: 1 }}>
                    {this.renderHeader()}
                    <FlatList
                        data={this.state.list}
                        extraData={this.state}
                        keyExtractor={(item, index) => '' + index}
                        renderItem={obj => this.renderItem(obj)}
                        onEndReached={() => this.loadData()}
                        onEndReachedThreshold={1}
                        ListHeaderComponent={() =>
                            this.state.list.length > 0 ?
                                <View style={{ width, height: 40, paddingLeft: 15, justifyContent: 'center' }}>
                                    <Text style={{ color: settingApp.colorText, fontSize: 16 }}>Kết quả</Text>
                                </View>
                                :
                                <View />
                        }
                        ListFooterComponent={() => (<View style={{ marginBottom: settingApp.isIPhoneX ? 25 : 10 }} />)}
                    />
                </View>
            )
        }
        return (
            <View style={{ flex: 1 }}>
                {content}
                {isLoading && this.renderLoading()}
            </View >
        )
    }
}

export default Layout;