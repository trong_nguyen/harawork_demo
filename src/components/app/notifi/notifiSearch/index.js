import Layout from './layout';
import { HaravanIc, HaravanHr } from '../../../../Haravan';
import { Toast } from '../../../../public';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../state/action';

import lodash from 'lodash';

class NotifiSearch extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            value: '',
            list: []
        }

        this.initList = [];
        this.textSearch = null;

        this.colleague = null;

        this.page_list = 1;
        this.length_list = 0;
        this.totalLength_list = null;
        this.endList = false;

        this.page_manager = 1;
        this.length_manager = 0;
        this.totalLength_manager = null;
        this.endListManager = false

        this.departmentListName = [];

        this.back = true;

        this.onChangeText = lodash.debounce(this.search, 500);
        this.role = props.navigation.state.params.role;
    }

    setInitData() {
        this.initList = [];
        this.page_list = 1;
        this.length_list = 0;
        this.totalLength_list = null;
        this.endList = false;

        this.page_manager = 1;
        this.length_manager = 0;
        this.totalLength_manager = null;
        this.endListManager = false;
    }

    componentDidMount() {
        const { colleague } = this.props.app;
        this.colleague = colleague;
        setTimeout(() => {
            if (this.input && this.input.focus) {
                this.input.focus()
            }
        }, 500);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.notify.refreshListNotify) {
            this.refreshList();
        }
    }

    checkData(input) {
        if (input && input.data && !input.error) {
            return input
        } else {
            return false;
        }
    }

    loadData(textSearch) {
        if (textSearch && textSearch.length > 0) {
            this.setState({ isLoading: true }, async () => {
                const { colleague, initList } = this;
                let newList = [...initList];
                const resultList = await this.loadDataList(colleague, textSearch);
                let resultManager = [];
                if(this.role) {
                    resultManager = await this.loadDataManager(colleague, textSearch);
                }
                newList = [...newList, ...resultList, ...resultManager];
                this.initList = newList;
                newList = newList.sort((a, b) => {
                    return new Date(b.updatedAt) - new Date(a.updatedAt);
                });
                newList = newList.map(m => {
                    const department = this.departmentListName.filter(e => e.id == m.fromDepartmentId);
                    return { departmentName: department[0].name, ...m }
                });
                this.setState({ list: newList, isLoading: false });
            })
        }
    }

    async loadDataList(colleague, textSearch) {
        let checkLoad = false;
        if (this.length_list > 0 && this.totalLength_list && (this.length_list < this.totalLength_list)) {
            this.page_list = this.page_list + 1;
            checkLoad = true;
        } else if (this.length_list === 0 && this.totalLength_list === null) {
            checkLoad = true;
        }
        if (checkLoad) {
            let list = await HaravanIc.searchList(colleague, textSearch, this.page_list);
            list = this.checkData(list);
            if (list) {
                await this.getDepartmentListName(list);
                this.totalLength_list = list.data.totalCount;
                this.length_list = this.length_list + list.data.data.length;
                return list.data.data;
            }
        } else {
            this.endList = true;
            return [];
        }
    }

    async loadDataManager(colleague, textSearch) {
        let checkLoad = false;
        if (this.length_manager > 0 && this.totalLength_manager && (this.length_manager < this.totalLength_manager)) {
            this.page_manager = this.page_manager + 1;
            checkLoad = true;
        } else if (this.length_manager === 0 && this.totalLength_manager === null) {
            checkLoad = true;
        }
        if (checkLoad) {
            let list = await HaravanIc.searchManager(colleague, textSearch, this.page_manager);
            list = this.checkData(list);
            if (list) {
                await this.getDepartmentListName(list);
                this.totalLength_manager = list.data.totalCount;
                this.length_manager = this.length_manager + list.data.data.length;
                return list.data.data;
            }
        } else {
            this.endListManager = true;
            return [];
        }
    }

    async getDepartmentListName(list) {
        const { departmentListName } = this;
        if (departmentListName.length > 0) {
            let listDepartmentId = list.data.data.map(e => e.fromDepartmentId);
            listDepartmentId = listDepartmentId.filter((v, i) => listDepartmentId.indexOf(v) == i);
            listDepartmentId = listDepartmentId.filter((e, i) => {
                if (departmentListName.findIndex(m => m.id == e) === -1) {
                    return e;
                }
            });
            for (let i = 0; i < listDepartmentId.length; i++) {
                let result = await HaravanHr.getDepartmentsById(listDepartmentId[i]);
                result = this.checkData(result);
                if (result) {
                    const item = { id: listDepartmentId[i], name: result.data.name };
                    departmentListName.push(item);
                }
            }
        } else if (departmentListName.length == 0) {
            let listDepartmentId = list.data.data.map(e => e.fromDepartmentId);
            listDepartmentId = listDepartmentId.filter((v, i) => listDepartmentId.indexOf(v) == i);
            for (let i = 0; i < listDepartmentId.length; i++) {
                let result = await HaravanHr.getDepartmentsById(listDepartmentId[i]);
                result = this.checkData(result);
                if (result) {
                    const item = { id: listDepartmentId[i], name: result.data.name };
                    departmentListName.push(item);
                }
            }
        }
    }

    async statusStar(id, status) {
        const result = await HaravanIc.statusStar(id, status);
        if (!result.error) {
            this.props.actions.notify.checkStart({ id, status });
            Toast.show(`${status ? 'Đánh dấu' : 'Bỏ đánh dấu'} thành công`);
        } else if (result.error) {
            Toast.show('Lỗi')
        }
    }

    changeText(value) {
        this.setState({ value }, this.onChangeText.bind(this));
    }

    search() {
        this.setInitData();
        this.loadData(this.state.value);
    }

    refreshList() {
        this.setInitData();
        this.loadData(this.state.value);
    }

}

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(mapStateToProps, mapDispatchToProps)(NotifiSearch);