import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { Avatar, imgApp, settingApp, Utils } from '../../../../../public'
import {MaterialCommunityIcons} from '@expo/vector-icons';

class Item extends Component{
    constructor(props){
        super(props)
        this.state={

        }
    }
    renderAvatar(item) {
        return (
            <View
                style={{
                    position: 'absolute',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <Avatar userInfo={{ name: item.fullName }} v={50} fontSize={20} />
            </View>
        )
    }
    
    renderContent(item) {
        return (
            <View style={{padding:15}}>
                    <View style={{justifyContent:'space-between'}}>
                        <Text
                            numberOfLines={1}
                            style={{
                                color: '#0279C7',
                                fontSize: 16,
                                fontWeight: 'normal',
                                paddingBottom:10
                            }}
                        >{item.fullName} </Text>
                        <Text
                            numberOfLines={1}
                            style={{
                                color: settingApp.colorText,
                                fontSize: 14,
                                fontWeight:'normal',
                            }}
                        >{item.mainPosition.jobtitleName}</Text>
                    </View>
            </View>
        )
    }
    render(){
        const { item, index } = this.props;
        let image = (item.readedAt) ? imgApp.seen : (imgApp.notSeen)
        let timeAt = (item.readedAt) ? Utils.formatTime(item.readedAt,'HH:mm', true) : '-'
        return(
                <View
                    style={{
                        borderTopWidth: index == 0 ? 0 : 1,
                        borderTopColor: '#F4F6F8',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        height:80,
                        backgroundColor:'#FFFFFF'
                    }}>
                        <View style={{ backgroundColor: 'transparent', width: 60, justifyContent: 'center', alignItems: 'center', marginLeft: 5 }}>
                            {this.renderAvatar(item)}
                        </View>

                        <View style={{ backgroundColor: 'transparent', width: (settingApp.width - 140) }}>
                            {this.renderContent(item)}
                        </View>

                        <View style={{ backgroundColor: 'transparent', width: 60, justifyContent: 'center', alignItems: 'center', marginLeft: 5 }}>
                            <MaterialCommunityIcons
                                name ='check-all'
                                size ={20}
                                color={item.readedAt ? '#5CB85C' : '#BFC7CF'}
                            />
                            <Text style={{
                                color: settingApp.colorText,
                                fontWeight: 'normal',
                                fontSize: 14,
                                paddingTop:10
                            }}>{timeAt}</Text>  
                        </View>
                </View>
        )
    }
}
export default Item;