import Layout from './layout';
import { HaravanIc } from '../../../../../../Haravan'

class SearchEmployee extends Layout{
    constructor(props){
        super(props)
        this.state = {
            listData:[],
            loadMore:false,
            isLoading:false,
            fisrt: true,
        }

        this.data= props.navigation.state.params.data;

        this.page =1;
        this.page_site=20;
        this.totalPage = null;
        this.currentList =[];
        this.textSearch = null
    }

    

    checkResult(result){
        if(result && result.data && !result.error){
            return result
        }
        else{
            return false
        }
    }

    onSearchBox(text){
        if(text !== this.textSearch){
            this.textSearch = text
            this.setState({isLoading: true, listData:[], fisrt:false}, () =>{
                this.refreshSearch()
                this.loadData()
            })
        }
    }

    refreshSearch(){
            this.page =1;
            this.totalPage = null;
            this.currentList =[]
    }

    async loadData(){
        const { page, totalPage, currentList, textSearch } = this;
        let text = textSearch ? textSearch : '';
        let newList = currentList
        if (!totalPage || (totalPage && !isNaN(totalPage) && page <= totalPage)) {
            let resultGetUser = await HaravanIc.GetUser(this.data.id, page, text);
            resultGetUser = this.checkResult(resultGetUser)
            if(resultGetUser.data){
                const totalCount = resultGetUser.data.totalCount;
                resultGetUser.data.data.map(item =>{
                    newList.push(item)
                })
                this.currentList = newList;

                this.page = (page + 1);
                this.totalPage = Math.ceil(totalCount / 20);
                this.setState({ listData:newList, loadMore:false, isLoading:false})
            }
            else{
                this.setState({loadMore:false, isLoading:false })
            }
            
        }
    }
}
export default SearchEmployee;