import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, ActivityIndicator , TextInput} from 'react-native';
import { settingApp, NotFound, Loading, HeaderWapper } from '../../../../../../public';
import Item from '../item';
import {AntDesign} from '@expo/vector-icons';
import lodash from 'lodash';

class Layout extends Component{
    onSearchBox(text) {
        this.onSearch(text)
    }

    renderHeader(){
        let onChangeCallback = lodash.debounce(this.onSearchBox, 500);
        return(
            <HeaderWapper
                    backgroundColor='#FFFFFF'
                    style={{ ...settingApp.shadow, justifyContent: 'space-between' }}
                >
                    <View
                        style={{
                            width:60,
                            backgroundColor: 'transparent',
                            flexDirection: 'row',
                            marginRight: 15,
                        }}
                    >
                        <TouchableOpacity
                            onPress={() => this.props.navigation.pop()}
                            style={{ justifyContent: 'center', paddingLeft: 5, width: 50, }}
                        >
                            <AntDesign name='left' size={20} color='#9CA7B2' />
                        </TouchableOpacity>
                    </View>
                    
                    <TextInput
                        //ref={refs => this.textInput = refs}
                        onChangeText={onChangeCallback.bind(this)}
                        style={{ flex: 1}}
                        autoCorrect={false}
                        autoFocus={true}
                        underlineColorAndroid='transparent'
                    />
                </HeaderWapper>        
        )
    }

    renderTop(){
        return(
        <View
            style={{width:settingApp.width, backgroundColor:'transparent', height:50, padding:10, ...settingApp.shadow}}
        >
            <View style={{flexDirection:'row'}}>
                <Text style={styles.textTop1}>Kết quả</Text>
            </View>
        </View>
        )
    }

    renderLoadMore(loadMore){
        return(
            <View style={{
                height: 80,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'transparent',
                borderTopWidth: 1,
                borderTopColor: 'rgba(218, 227, 234, 0.5)'
            }}>
                {
                    loadMore ?
                        <ActivityIndicator
                            style={{ height: 50 }}
                            size="small"
                            color={settingApp.color}
                        /> :
                        <View style={{ flex: 1, backgroundColor: 'transparent' }} />
                }
            </View>
        )
    }

    loadMore() {
        if (!this.state.loadMore) {
            if (this.page <= this.totalPage) {
                this.setState({ loadMore: true }, () => this.loadData());
            }
        }
    }

    renderContent(){
        const { listData, loadMore } = this.state;
        return(
            <FlatList
                data={listData}
                extraData={this.state}
                keyExtractor={(item, index) =>(index+'')}
                renderItem={({item, index}) =>
                    <Item
                        item={item}
                        index={index}
                    />
                }
                onEndReachedThreshold={1}
                onEndReached={() => this.loadMore()}
                ListFooterComponent={
                        this.renderLoadMore(loadMore)
                    }
                ListEmptyComponent={
                    <View
                        style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 100, width: settingApp.width }}
                    />
                    }
            />
        )
    }
    render(){
        const { isLoading, fisrt, listData } = this.state
        let content =  <View/>
        isLoading ? content = <Loading/> : 
        content = (listData && (listData.length > 0) )? this.renderContent() : (!fisrt) ? <NotFound/> :  <View/>;
        return(
        <View
            style={{flex:1}}
        >
            {this.renderHeader()}
            { !fisrt ? 
                this.renderTop() : <View/>
            }
            {content}
        </View>
        )
    }
}
const styles = {
    textTop1:{
        fontSize:16, 
        color:settingApp.colorText, 
    },
    textTop2:{
        fontSize:16, 
        fontWeight:'bold'
    },

}
export default Layout;
