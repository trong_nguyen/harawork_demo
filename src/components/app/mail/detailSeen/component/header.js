import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { HeaderIndex, settingApp, HeaderWapper } from '../../../../../public'
import { Ionicons, AntDesign, FontAwesome, MaterialIcons } from '@expo/vector-icons'


class Header extends Component {
    constructor(props){
        super(props)
        this.state={

        }
    }

    render(){
        return(
            <HeaderWapper
                    backgroundColor='#FFFFFF'
                    style={{ ...settingApp.shadow, justifyContent: 'space-between' }}
                >
                    <View
                        style={{
                            width:60,
                            backgroundColor: 'transparent',
                            flexDirection: 'row',
                            marginRight: 15,
                        }}
                    >
                        <TouchableOpacity
                            onPress={() => this.props.close()}
                            style={{ justifyContent: 'center', paddingLeft: 10, width: 50, }}
                        >
                            <AntDesign name='left' size={20} color='#9CA7B2' />
                        </TouchableOpacity>
                    </View>
                    
                    <View style={{
                            backgroundColor: 'transparent',
                            alignItems:'center',
                            flexDirection: 'row',
                            width:(settingApp.width*0.5)
                    }}>
                        <Text style={{fontSize:14, color:settingApp.colorText, fontWeight:'bold'}}>CHI TIẾT NGƯỜI NHẬN</Text>
                    </View>
                    <View
                        style={{
                            width: 60,
                            backgroundColor: 'transparent',
                            flexDirection: 'row',                       
                        }}
                    >
                        <TouchableOpacity
                            activeOpacity={1}
                            onPress={() => this.props.navigation.navigate('SearchEmployee',{data:this.props.data})}
                            style={{ flex: 1, backgroundColor:'transparent', alignItems: 'center', justifyContent: 'center' }}
                        >
                            <Ionicons name='ios-search' size={30} color='#9CA7B2' />
                        </TouchableOpacity>
                    </View>
                </HeaderWapper>                 
        )
    }
}
export default Header;