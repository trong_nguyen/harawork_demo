import Layout from './layout';
import { HaravanIc } from '../../../../Haravan'

class DetailSeenMail extends Layout{
    constructor(props){
        super(props)
        this.state = {
            listData:[],
            nonSeen: 0,
            seen: 0,
            totalUser: 0,
            loadMore:false,
            isLoading:true,
        }

        this.data= props.navigation.state.params.data;

        this.page =1;
        this.page_site=20;
        this.totalPage = null;
        this.currentList =[]
    }

    componentDidMount(){
        this.loadData()
    }

    checkResult(result){
        if(result && result.data && !result.error){
            return result
        }
        else{
            return false
        }
    }


    async loadData(){
        const { page, page_site, totalPage, currentList } = this;
        let { listData, seen, nonSeen, totalUser} = this.state;
        let newList = currentList
        if (!totalPage || (totalPage && !isNaN(totalPage) && page <= totalPage)) {
            let resultGetUser = await HaravanIc.GetUser(this.data.id, page);
            let resultCount = await HaravanIc.CountUser(this.data.id)
            resultCount = this.checkResult(resultCount)
            resultGetUser = this.checkResult(resultGetUser)
            if(resultCount && resultGetUser){
                if(resultCount.data.length > 0){
                    resultCount.data.map(item =>{
                        if(item.type == 1){
                            seen = item.total
                        }
                        else if(item.type == 2){
                            nonSeen = item.total
                        }
                    })
                    this.setState({seen, nonSeen})
                }
            }
            
            if(resultGetUser.data){
                const totalCount = resultGetUser.data.totalCount;
                resultGetUser.data.data.map(item =>{
                    newList.push(item)
                })
                this.currentList = newList;

                this.page = (page + 1);
                this.totalPage = Math.ceil(totalCount / 20);
                this.setState({totalUser:totalCount, listData:newList, loadMore:false, isLoading:false})
            }
            else{
                this.setState({ loadMore:false, isLoading:false }, ()=>{
                })
            }
            
        }
    }

}
export default DetailSeenMail;