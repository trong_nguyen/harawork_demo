import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, ActivityIndicator } from 'react-native';
import { settingApp, NotFound, Loading } from '../../../../public';
import Header from './component/header';
import Item from './component/item';
import {MaterialCommunityIcons} from '@expo/vector-icons';

class Layout extends Component{

    renderTop(){
        const {seen, nonSeen, totalUser} = this.state;
        return(
            <View
                style={{width:settingApp.width, backgroundColor:'transparent', height:80, padding:10}}
            >
                <View style={{flexDirection:'row'}}>
                    <Text style={styles.textTop1}>Số người nhận: </Text>
                    <Text style={[styles.textTop2,{color:settingApp.colorText}]}>
                        {totalUser}
                    </Text>
                </View>

                <View style={{flexDirection:'row',paddingTop:5}}>
                    <View style={{flexDirection:'row'}}>
                        <MaterialCommunityIcons
                                name ='check-all'
                                size ={20}
                                color='#5CB85C'
                            />  
                        <Text style={[styles.textTop1, {paddingLeft:5}]}>Đã đọc: </Text>
                        <Text style={[styles.textTop2,{color:'#5CB85C'}]}>{seen}</Text>
                    </View>
                    <View style={{flexDirection:'row',paddingLeft:30}}>
                        <MaterialCommunityIcons
                                name ='check-all'
                                size ={20}
                                color='#BFC7CF'
                            />
                        <Text style={[styles.textTop1, {paddingLeft:5}]}>Chưa đọc: </Text>
                        <Text style={[styles.textTop2,{color:'#BFC7CF'}]}>{nonSeen}</Text>
                    </View>
                </View>
            </View>
        )
    }

    renderLoadMore(loadMore){
        return(
            <View style={{
                height: 80,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'transparent',
                borderTopWidth: 1,
                borderTopColor: 'rgba(218, 227, 234, 0.5)'
            }}>
                {
                    loadMore ?
                        <ActivityIndicator
                            style={{ height: 50 }}
                            size="small"
                            color={settingApp.color}
                        /> :
                        <View style={{ flex: 1, backgroundColor: 'transparent' }} />
                }
            </View>
        )
    }

    loadMore() {
        if (!this.state.loadMore) {
            if (this.page <= this.totalPage) {
                this.setState({ loadMore: true }, () => this.loadData());
            }
        }
    }

    renderContent(){
        const { listData, loadMore } = this.state;
        return(
            <FlatList
                data={listData}
                extraData={this.state}
                keyExtractor={(item, index) => (index+'')}
                renderItem={({item, index}) =>
                    <Item
                        item={item}
                        index={index}
                    />
                }
                onEndReachedThreshold={1}
                onEndReached={() => this.loadMore()}
                ListFooterComponent={
                        this.renderLoadMore(loadMore)
                    }
                ListEmptyComponent={
                    <View
                        style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 100, width: settingApp.width }}
                    >
                        <NotFound />
                    </View>
                    }
            />
        )
    }
    render(){
        const { isLoading } = this.state
        let content =  <View/>
        isLoading ? content = <Loading/> : content = this.renderContent();
        return(
        <View
            style={{flex:1}}
        >
            <Header 
                close={() =>this.props.navigation.pop()}
                navigation = {this.props.navigation}
                data={this.data}
            />
            {this.renderTop()}
            {content}
        </View>
        )
    }
}
const styles = {
    textTop1:{
        fontSize:16, 
        color:settingApp.colorText, 
    },
    textTop2:{
        fontSize:16, 
        fontWeight:'bold'
    },

}
export default Layout;
