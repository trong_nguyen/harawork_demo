import React, { Component } from 'react';
import { View, Text, TouchableOpacity, FlatList, ActivityIndicator, RefreshControl, Image } from 'react-native';
import { settingApp, ButtonCustom, HeaderIndex, Loading, NotFound, imgApp } from '../../../../public';
import Header from './components/header';
import Item from './components/item';
import FilterMail from './components/filterMail';
import ShowStart from './components/showStart';
import { MaterialCommunityIcons} from '@expo/vector-icons'
class Layout extends Component {

    renderWriteMail() {
        const {collegue} = this
        return (
            <ButtonCustom
                // MailWrite
                // TestDetail
                onPress={() => this.props.navigation.navigate('MailWrite', {type:'write', data:[]})}
                style={{
                    position: 'absolute',
                    right: 15,
                    bottom: 15,
                    width: 60,
                    height: 60,
                    borderRadius: 60 >> 1,
                    backgroundColor: settingApp.color,
                    justifyContent: 'center',
                    alignItems: 'center',
                    overflow: 'hidden',
                }}
            >
                <MaterialCommunityIcons size={25} name='pencil' color='#FFFFFF' />
            </ButtonCustom>
        );
    }
    renderLoadMore(loadMore) {
        return (
            <View style={{
                height: 80,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'transparent',
                borderTopWidth: 1,
                borderTopColor: 'rgba(218, 227, 234, 0.5)'
            }}>
                {
                    loadMore ?
                        <ActivityIndicator
                            style={{ height: 50 }}
                            size="small"
                            color={settingApp.color}
                        /> :
                        <View style={{ flex: 1, backgroundColor: 'transparent' }} />
                }
            </View>
        )
    }

    renderList() {
        const { loadMore, oppenFilter, listMail } = this.state;
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <FlatList
                    data={this.state.listMail}
                    extraData={this.state}
                    keyExtractor={(item, index) =>  item.id ||item || (index+'')}
                    renderItem={({ item, index }) => this.renderItem(item, index)}
                    stickyHeaderIndices={this.state.stickyHeaderIndices}
                    onEndReachedThreshold={1}
                    onEndReached={() => this.loadMore()}
                    ListFooterComponent={
                        this.renderLoadMore(loadMore)
                    }
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={() => {
                                this.setState({ refreshing: true }, () =>
                                    this.refreshList())
                            }}
                            tintColor={settingApp.color}
                            colors={[settingApp.color]}
                        />}
                    ListEmptyComponent={
                        <View
                            style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 100, width: settingApp.width }}
                        >
                            <NotFound />
                        </View>
                    }

                />
            </View>
        )
    }

    loadMore() {
        if (!this.state.loadMore) {
            if (this.page <= this.totalPage) {
                this.setState({ loadMore: true }, () => this.loadData());
            }
        }
    }

    renderItem(item, index) {
        const { listCheck, listRemove, listTitle, listMail } = this.state;
        const length = listMail.length;
        if (!item.id) {
            if (listTitle.length > 0) {
                for (i = 0; i < listTitle.length; i++) {
                    if (listTitle[i] === item) {
                        this.hide = true;
                        break;
                    } else {
                        this.hide = false;
                    }
                }
            } else {
                this.hide = false;
            }
            return (
                (this.hide == true)
                    ? null
                    :
                    <View style={{
                        backgroundColor: '#E5E5E5',
                        justifyContent: 'center',
                        height: 30,
                        paddingLeft: 10,
                        width: settingApp.width,
                    }}>
                        <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{item} </Text>
                    </View>
            )
        }
        else {
            return (
                <Item
                    item={item}
                    index={index}
                    navigation={this.props.navigation}
                    statusStart={(item, status) => this.setStart(item, status)}
                    listStart={this.listStart}
                    status={this.status}
                    checkStarred={(id, status) => this.checkStarred(id, status)}
                    listLables={this.state.listLables}
                    checkAvatar={item => this.checkAvatar(item)}
                    listCheck={this.state.listCheck}
                    listAdd={this.state.listAdd}
                    listDelete={this.state.listDelete}
                    listRemove={listRemove}
                    listReverse={this.state.listReverse}
                />
            )
        }

    }

    isLoading() {
        return (
            <Loading />
        )
    }

    isLoad() {
        return (
            <View style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                backgroundColor: 'rgba(0,0,0,0.5)',
                justifyContent: 'center',
                alignItems: 'center',
            }}>
                <ActivityIndicator size='large' color={settingApp.color} />
            </View>
        )
    }

    render() {
        const { isLoading, oppenFilter, isLoad, listMail, listCheck, showStart } = this.state;
        let content = <View />
        isLoading ? content = this.isLoading() : content = this.renderList();
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
                    <Header
                        title={this.title}
                        oppenFilter={() => this.setState({ oppenFilter: !this.state.oppenFilter })}
                        closeFilter={() => this.setState({ oppenFilter: false })}
                        navigation={this.props.navigation}
                        listCheck={listCheck}
                        showLabels={() => this.showLabels()}
                        showStart={() => this.setState({ showStart: !this.state.showStart })}
                        close={() => this.closeHeader()}
                        removeMails={() => this.removeMails()}
                    />
                    {content}
                    {showStart ?
                        <ShowStart
                            bulkStarred={(status) => this.bulkStarred(status)}
                            close={() => this.setState({ showStart: false })}
                        />
                        : null}
                    {this.renderWriteMail()}
                </View>
                {isLoad && this.isLoad()}
                {
                    oppenFilter ?
                        <FilterMail
                            navigation={this.props.navigation}
                            listLables={this.state.listLables}
                            closeFilter={item => {
                                const { listLables } = this.state;
                                if (item) {
                                    listLables.unshift(item);
                                }
                                this.setState({
                                    listLables,
                                    oppenFilter: false,
                                });
                            }}
                            typeCheck={this.type}
                        /> : null
                }
            </View>
        )
    }
}
export default Layout