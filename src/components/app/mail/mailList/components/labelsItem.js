import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import { settingApp } from '../../../../../public';
import lodash from 'lodash';

class LabelsItem extends Component {

    constructor(props) {
        super(props)
        this.state = {}
    }

    checkLabels(listAdd, listDelete, listCheck, listLables, item) {
        if (listCheck.length > 0) {
     
        }
    }

    render() {
        let { listAdd, listDelete, listCheck, listLables, item } = this.props;
        let length = 0;
        const { width } = settingApp;
        
        listCheck.map(a => {
            if (a.id === item.id) {
                if (listAdd.length > 0) {
                    listAdd.map(m => {
                        listLables.map(j => {
                            if (m.id === j.id) {
                                item.listLables = [...item.listLables, j]
                            }
                        });
                    });
                }
                if (listDelete.length > 0) {
                    listDelete.map(n => {
                        const index = item.listLables.findIndex(e => e.id === n.id);
                        if (index > -1) {
                            item.listLables.splice(index, 1);
                        }
                    });
                }
            }
        });

        let list = lodash.uniq(item.listLables, item.listLables);
        if(list.length > 0){
            return (
                <View style={{ backgroundColor: 'transparent', height: 20, flexDirection: 'row', paddingTop: 5 }}>
                    {
                        list.map((e, i) => {
                            let x = (e.label.length * 8) + 13;
                            length = length + x;
                            if ((length / width) > (60 / 100)) {
                                if (((length / width) - ((length / width) - (e.label.length / width))) < (10 / 100)) {
                                    return (
                                        <View
                                            key={i}
                                            style={{
                                                width: 30,
                                                height: 20,
                                                borderRadius: 20 >> 1,
                                                backgroundColor: settingApp.colorLable,
                                                marginRight: (width * 5),
                                            }}
                                        >
                                            <Text style={{ paddingLeft: 10, paddingRight: 5, color: '#ffffff' }}>
                                                ...
                                </Text>
                                        </View>
                                    );
                                }
                                else {
                                    return null;
                                }
                            } else {
                                return (
                                    <View
                                        key={i}
                                        style={{
                                            height: 20,
                                            borderRadius: 20 >> 1,
                                            backgroundColor: settingApp.colorLable,
                                            marginRight: 3,
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}
                                    >
                                        <Text
                                            style={{ paddingLeft: 5, paddingRight: 5, color: '#ffffff' }}
                                            numberOfLines={1}
                                        >
                                            {e.label}
                                        </Text>
                                    </View>
                                );
                            }
                        })
                    }
                </View>
            )   
        }
        else{
            return null;
        }
    }
}
export default LabelsItem;