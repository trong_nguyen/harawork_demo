import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { HeaderIndex, settingApp, ButtonCustom, HeaderWapper } from '../../../../../public'
import { Ionicons, AntDesign, FontAwesome, MaterialIcons } from '@expo/vector-icons'

class Header extends Component {
    render() {
        const { listCheck, isShowLabels } = this.props
        if (listCheck && listCheck.length > 0) {
            return (
                <HeaderWapper
                    backgroundColor='#FFFFFF'
                    style={{ ...settingApp.shadow, justifyContent: 'space-between' }}
                >
                    <View
                        style={{
                            width: (settingApp.width * 0.35),
                            backgroundColor: 'transparent',
                            flexDirection: 'row',
                            marginRight: 15
                        }}
                    >
                        <TouchableOpacity
                            onPress={() =>this.props.close()}
                            style={{ justifyContent: 'center', paddingLeft: 10, width: 50, }}
                        >
                            <AntDesign name='left' size={20} color='#9CA7B2' />
                        </TouchableOpacity>

                        <View style={{ justifyContent: 'center' }}>
                            <Text style={{ color: settingApp.colorText, fontSize: 16 }}>{listCheck.length} thư được chọn </Text>
                        </View>
                    </View>

                    <View
                        style={{
                            width: (settingApp.width * 0.35),
                            backgroundColor: 'transparent',
                            flexDirection: 'row',
                            marginRight: 15
                        }}
                    >
                        <TouchableOpacity
                            onPress={() => this.props.removeMails()}
                            style={{ width: 50, justifyContent: 'center' }}
                        >
                            <Ionicons name='md-trash' size={25} color='#BFC7CF' />
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.props.showStart()}
                            style={{ width: 50, justifyContent: 'center' }}
                        >
                            <FontAwesome name='star' size={25} color='#BFC7CF' />
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.props.showLabels()}
                            style={{ width: 50, justifyContent: 'center' }}
                        >
                            <MaterialIcons name='label' size={25} color='#BFC7CF' />
                        </TouchableOpacity>
                    </View>
                </HeaderWapper>
            )
        }
        else {
            return (
                <HeaderIndex
                    title={
                        <TouchableOpacity
                            activeOpacity={1}
                            style={{ flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', backgroundColor: settingApp.color }}
                            onPress={() => {
                                this.props.isSearch ? null :
                                    this.props.oppenFilter()
                            }}
                        >
                            <Text style={[settingApp.styleTitle, { marginRight: 5, }]}>{this.props.title}</Text>
                            <Ionicons name='md-arrow-dropdown' size={20} color='#FFFFFF' />
                        </TouchableOpacity>
                    }
                    buttonRight={
                        <TouchableOpacity
                            activeOpacity={1}
                            onPress={() => this.props.navigation.navigate('SearchMail')}
                            style={{ flex: 1, backgroundColor: settingApp.color, alignItems: 'center', justifyContent: 'center' }}
                        >
                            <Ionicons name='ios-search' size={30} color='#FFFFFF' />
                        </TouchableOpacity>
                    }
                />
            )
        }

    }
}
export default Header;