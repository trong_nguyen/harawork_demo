import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, Platform, Image } from 'react-native';
import { HeaderIndex, HeaderScreen, settingApp, ButtonCustom, imgApp } from '../../../../../public';
import { AntDesign, FontAwesome, Entypo, MaterialIcons } from '@expo/vector-icons';
import Collapsible from 'react-native-collapsible';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../../state/action';

class FilterMail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            listLables: this.props.listLables,
            indexType: this.props.typeCheck,
            isOpen: false
        }

        this.list = [
            { icon: imgApp.inbox, name: 'Hộp thư đến', type: 'inbox?' },
            { icon: imgApp.headerStar, name: 'Đánh dấu sao', type: 'starred?' },
            { icon: imgApp.send, name: 'Đã gửi', type: 'sent?' },
            { icon: imgApp.draft, name: 'Nháp', type: 'draft?' },
        ];
    }

    changeType(item) {
        const { indexType } = this.state;
        if (indexType !== item.type) {
            this.setState({ indexType: item.type }, () => {
                this.props.closeFilter();
                this.props.actions.mail.typeMail({type:item.type, name: item.name});
            });
        }
    }

    render() {
        const { indexType } = this.state;
        return (
            <View
                style={{
                    position: 'absolute', width: settingApp.width, height: settingApp.height
                }}>
                <TouchableOpacity
                    onPress={() => this.props.closeFilter()}
                    activeOpacity={1}
                    style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
                >
                    <View style={styles.container}>
                        <ScrollView scrollEnabled={false}>
                            <TouchableOpacity>
                                <ScrollView style={styles.menu}>
                                    {this.list.map((item, index) => {
                                        return (
                                            <View
                                                style={{
                                                    flex: 1,
                                                    backgroundColor: indexType === item.type ? '#405467' : '#313F4E',
                                                }}
                                                key={index}>
                                                <TouchableOpacity
                                                    onPress={() => this.changeType(item)}
                                                    style={{ flex: 1, }}
                                                >
                                                    <View
                                                        style={{
                                                            flex: 1,
                                                            height: 55,
                                                            flexDirection: 'row',
                                                            alignItems: 'center',
                                                            paddingLeft: 20,
                                                            backgroundColor: indexType === item.type ? '#405467' : '#313F4E'
                                                        }}
                                                    >
                                                        <Image
                                                            source={item.icon}
                                                            style={{ width: 20, height: 20, marginRight: 10 }}
                                                            resizeMode="contain"
                                                        />
                                                        <Text style={{ color: '#ffffff', fontSize: 16 }}>
                                                            {item.name}
                                                        </Text>
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        );
                                    })}

                                    <TouchableOpacity
                                        activeOpacity={1}
                                        onPress={() =>
                                            this.setState({ isOpen: !this.state.isOpen })}
                                        style={{ flex: 1, backgroundColor: 'transparent' }}
                                    >
                                        <View
                                            style={{
                                                flex: 1,
                                                height: 55,
                                                flexDirection: 'row',
                                                alignItems: 'center',
                                                paddingLeft: 20,
                                                paddingRight: 10,
                                                backgroundColor:'#313F4E',
                                            }}
                                        >
                                            <Image
                                                source={imgApp.label}
                                                style={{ width: 20, height: 20, marginRight: 10 }}
                                                resizeMode="contain"
                                            />
                                            <Text style={{ color: '#ffffff', fontSize: 16 }}>
                                                Nhãn
                                                </Text>
                                            <View
                                                style={{
                                                    flex: 1,
                                                    justifyContent: 'center',
                                                    alignItems: 'flex-end',
                                                }}
                                            >
                                                <MaterialIcons
                                                    name={
                                                        this.state.isOpen
                                                            ? 'keyboard-arrow-down'
                                                            : 'keyboard-arrow-right'
                                                    }
                                                    color="#9CA7B2"
                                                    size={23}
                                                />
                                            </View>
                                        </View>
                                    </TouchableOpacity>

                                    <Collapsible collapsed={!this.state.isOpen}>
                                            <View>
                                                {this.state.listLables.map((e, i) => {
                                                    return (
                                                        <View
                                                            style={{
                                                                flex: 1,
                                                                height: 50,
                                                                backgroundColor:'#313F4E',
                                                            }}
                                                            key={i}
                                                        >
                                                            <TouchableOpacity
                                                                activeOpacity={1}
                                                                onPress={() => {
                                                                    this.props.actions.mail.typeMail(
                                                                        {type:`search?labelIds=${e.id}&`, name:e.label}
                                                                    );
                                                                    this.props.closeFilter();
                                                                }}
                                                                style={{
                                                                    flex: 1,
                                                                    justifyContent:'center',
                                                                    backgroundColor: indexType === this.props.typeCheck ? '#405467' : '#313F4E' ,
                                                                    paddingLeft: 50,
                                                                    backgroundColor:'#313F4E',
                                                                }}
                                                            >
                                                                <Text
                                                                    style={{ color: '#ffffff', fontSize: 16 }}
                                                                    numberOfLines={1}
                                                                >
                                                                   {e.label}
                                                                </Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                    );
                                                })}
                                                <View
                                                    style={{
                                                        flex: 1,
                                                        height: 50,
                                                        backgroundColor:'#313F4E',
                                                    }}
                                                >
                                                    <TouchableOpacity
                                                        onPress={() => {
                                                            this.setState({isOpen: false})
                                                            this.props.navigation.navigate('CreateLable',{
                                                                actions: (item => {
                                                                    const {listLables} = this.state;
                                                                    if(item){
                                                                        listLables.unshift(item)
                                                                        this.setState({listLables})
                                                                    }
                                                                }),
                                                                isOpen:(isOpen => this.setState({isOpen: isOpen}))
                                                            })
                                                        }}
                                                        style={{ flex: 1, backgroundColor:'#313F4E', }}
                                                    >
                                                        <View
                                                            style={{
                                                                flex: 1,
                                                                paddingLeft: 50,
                                                                flexDirection: 'row',
                                                                alignItems: 'center',
                                                                
                                                            }}
                                                        >
                                                            <Entypo name="plus" color="#ffffff" size={23} />
                                                            <Text
                                                                style={{
                                                                    color: '#ffffff',
                                                                    fontSize: 16,
                                                                    marginLeft: 5,
                                                                }}
                                                            >
                                                                Tạo nhãn mới
                                                            </Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </Collapsible>
                                </ScrollView>
                            </TouchableOpacity>
                        </ScrollView>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}
const styles = {
    container: {
        flex: 1,
        ...Platform.select({
            android: {
                marginTop: settingApp.statusBarHeight + 40,
            },
            ios: {
                marginTop: settingApp.statusBarHeight + 44,
            },
        }),
        backgroundColor: 'rgba(0,0,0,0.5)',
        width: settingApp.width
    },
    menu:{
        flex: 1,
        ...Platform.select({
            android: {
                maxHeight: (settingApp.height - (settingApp.statusBarHeight + 40)),
            },
            ios: {
                maxHeight: (settingApp.height - (settingApp.statusBarHeight + 44)),
            },
        }),
    },
    cover: {
        flex: 1,
        backgroundColor: 'transparent',
        height: 23,
        width: 23,
        overflow: 'hidden',
        marginLeft: '46%',
        marginTop: -10,
    },
    triangle: {
        width: 23,
        height: 23,
        backgroundColor: '#313F4E',
        transform: [{ rotate: '45deg' }],
        borderRadius: 2.5,
        marginTop: 20,
    },
    content: {
        flex: 1,
        backgroundColor: '#313F4E',
        paddingTop: 5,
        paddingBottom: 5,
        borderRadius: 10,
    },
}
const mapStateToProps = (state) => ({...state })
const mapDispatchToProps = (dispatch) =>{
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(FilterMail);