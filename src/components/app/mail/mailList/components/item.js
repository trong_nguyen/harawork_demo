import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import { settingApp, ButtonCustom, Avatar, imgApp, Utils } from '../../../../../public';
import { AntDesign, FontAwesome, Entypo } from '@expo/vector-icons';
import lodash from 'lodash';

import * as Animatable from 'react-native-animatable';
import LabelsItem from './labelsItem';

class Item extends Component {
    constructor(props) {
        super(props)
        this.state = {
            item: this.props.item,
            isCheck: false,
            isStarred: this.props.item.mailInfo.isStarred
        }
        this.checkClick = false;
        this.hide = false;
    }

    componentWillReceiveProps(nextProps) {
        const { isStarred, id } = nextProps.item.mailInfo;
        if (nextProps.listCheck.length === 0) {
            this.setState({ isCheck: false })
        }

        if (nextProps.status != null) {
            this.props.listStart.map(e => {
                if (e.id === id) {
                    this.setState({
                        isStarred: nextProps.status,
                    });
                }
            });
        } else if (nextProps.status == null && (isStarred != this.state.isStarred)) {
            this.props.listStart.map(e => {
                if (e.id === id) {
                    this.setState({
                        isStarred: isStarred,
                    });
                }
            });
        }
        else {
            this.setState({
                isStarred: this.state.isStarred,
            });
        }
        this.props.listStart = []
    }

    checkAvatar() {
        this.props.checkAvatar(this.props.item)
    }

    renderAvatar(item) {
        const { isCheck } = this.state;
        const { listCheck } = this.props;

        return (
            <TouchableOpacity
                onPress={() => this.setState({ isCheck: !this.state.isCheck }, () => {
                    this.checkAvatar()
                })
                }
                activeOpacity={1}
                style={{
                    position: 'absolute',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>{
                    isCheck ?
                        <View
                            style={{
                                width: 50,
                                height: 50,
                                borderRadius: 50,
                                backgroundColor: '#BFC7CF',
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}
                        >
                            <AntDesign name='check' size={25} color='#FFFFFF' />
                        </View>
                        :
                        <Avatar userInfo={{ name: item.createdUserName, picture: item.createdUserAvatar }} v={50} fontSize={20} />
                }
            </TouchableOpacity>
        )
    }

    start(item) {
        // this.props.setStatus(null, [])
        this.setState({ isStarred: !this.state.isStarred }, () => {
            this.props.statusStart(item, this.state.isStarred);
            this.props.checkStarred(item.id, this.state.isStarred);
        });
    }

    renderContent(item) {
        let image = (item.percentRead == 0) ? imgApp.notSeen : ((item.percentRead == 50) ? imgApp.halfSeen : (item.percentRead == 100) ? imgApp.seen : imgApp.notSeen)
        let countMessage = item.countMessage > 1;
        return (
            <View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 5 }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Text
                            numberOfLines={1}
                            style={{
                                color: settingApp.colorText,
                                fontSize: 16,
                                fontWeight: item.mailInfo.isReaded ? 'normal' : 'bold'
                            }}
                        >{item.createdUserName} </Text>
                        {
                            countMessage ?
                                <Text style={{ fontSize: 16, color: settingApp.colorText }}> ({item.countMessage}) </Text>
                                : null
                        }
                        {item.countDraftMessage > 0 ?
                            <Text style={{ fontSize: 16, color:'#CA4F40'}}>, Nháp</Text> : null
                        }
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        {item.isAttachment ?
                            <Entypo name='attachment' size={15} color='#9CA7B2' />
                            : null
                        }
                        <Image
                            style={{ width: 20, height: 20, }}
                            source={image}
                        />
                    </View>

                </View>
                <Text
                    numberOfLines={1}
                    style={{
                        color: item.mailInfo.isReaded ? '#8E8E93' : settingApp.colorText,
                        fontSize: 14,
                        fontWeight: item.mailInfo.isReaded ? 'normal' : 'bold',
                    }}
                >{item.mailInfo.subject}</Text>
                <View >
                    <LabelsItem
                        listAdd={this.props.listAdd}
                        listDelete={this.props.listDelete}
                        listCheck={this.props.listCheck}
                        listLables={this.props.listLables}
                        item={item}
                    />
                </View>

            </View>
        )
    }

    renderStart(item) {
        const { listCheck } = this.props;
        const { isStarred } = this.state;
        let checkSub = item.mailInfo.subject.length > 0 ;
        return (
            <TouchableOpacity
                onPress={() => {
                    listCheck.length == 0 ? this.start(item) : null
                }}
                activeOpacity={listCheck.length == 0 ? 0.7 : 1}
                style={{
                    position: 'absolute',
                    right: 0,
                    bottom: 0,
                    width: 50,
                    height: 50,
                    justifyContent: checkSub ? 'center' : 'flex-end',
                    alignItems: 'center'
                }}
            >
                <FontAwesome name={isStarred ? 'star' : 'star-o'} size={25} color={isStarred ? '#F0AD4E' : '#BFC7CF'} />
            </TouchableOpacity>
        )
    }

    render() {
        const { item, index, navigation, listCheck, listRemove, listReverse } = this.props
        const { isCheck } = this.state;
        const timeAt = Utils.formatLocalTime(item.updatedAt, 'HH:mm')
        let backgroundColor = 'transparent';
        if (listRemove && listRemove.length > 0) {
            listRemove.map(e => {
                if (e.id === item.id) {
                    this.hide = true;
                }
            });
        }
        if (listReverse && listReverse.length > 0) {
            listReverse.map(e => {
                if (e.id === item.id) {
                    this.hide = false;
                }
            });
        }
        return (
            this.hide ? null :
                <View
                    style={{
                        flex: 1,
                        borderTopWidth: index === 0 ? 0 : 1,
                        borderTopColor: 'rgba(218, 227, 234, 0.5)'
                    }}>
                    <TouchableOpacity
                        onPress={() => {
                            listCheck.length == 0 ?
                                navigation.navigate('MailDetail', { item: item })
                                : null
                        }}
                        activeOpacity={listCheck.length == 0 ? 0.5 : 1}
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            paddingTop: 15,
                            paddingBottom: 15,
                            backgroundColor: isCheck ? '#F4F6FC' : '#FFFFFF',
                        }}
                    >
                        <View style={{ backgroundColor: backgroundColor, width: 60, justifyContent: 'center', alignItems: 'center', marginLeft: 5 }}>
                            {this.renderAvatar(item)}
                        </View>

                        <View style={{ backgroundColor: backgroundColor, width: (settingApp.width - 140) }}>
                            {this.renderContent(item)}
                        </View>

                        <View style={{ backgroundColor: backgroundColor, width: 60, alignItems: 'center' }}>
                            <Text style={{
                                color: settingApp.colorText,
                                fontWeight: item.mailInfo.isReaded ? 'normal' : 'bold',
                                fontSize: 14
                            }}>{timeAt}</Text>
                        </View>
                        {this.renderStart(item)}
                    </TouchableOpacity>
                </View>
        )
    }
}
export default Item;