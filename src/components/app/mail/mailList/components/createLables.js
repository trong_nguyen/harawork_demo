import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput } from 'react-native';
import { HeaderIndex, HeaderScreen, settingApp, ButtonCustom, imgApp, HeaderWapper } from '../../../../../public';
import { AntDesign, FontAwesome, Entypo, MaterialIcons } from '@expo/vector-icons';
import {HaravanIc} from '../../../../../Haravan'

class CreateLables extends Component {
    constructor(props) {
        super(props)
        this.state = {
            newLabel: ''
        }
    }

    async addLable() {
        let newLabel = this.state.newLabel.trim();
        let body = {
            label: `${newLabel}`,
        }
        if (newLabel.length > 0) {
            const result = await HaravanIc.addNewLabel(body)
            let item = null;
            if (result && result.data && !result.error) {
                item = result.data;
                this.props.navigation.state.params.actions(item),
                this.props.navigation.state.params.isOpen(true)
                this.props.navigation.pop()
            } 
        }
    }

    renderHeader() {
        const { newLabel } = this.state;
        let status = newLabel.trim().length > 0;
        return (
            <HeaderWapper
                style={{ justifyContent: 'space-between' }}>
                <View style={{ backgroundColor: '#FFFFFF', flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.pop()}
                        style={{ alignItems: 'center', paddingLeft: 10 }}>
                        <AntDesign name='close' color='#9CA7B2' size={25} />
                    </TouchableOpacity>

                    <View style={{ alignItems: 'center' }}>
                        <Text style={{ color: settingApp.color, fontWeight: 'bold', fontSize: 18 }}>Tạo nhãn mới</Text>
                    </View>

                    <TouchableOpacity
                        activeOpacity={status ? 0.7 : 1}
                        onPress={() =>{
                            status ? ( 
                                this.addLable()
                                ): null }}
                        style={{ alignItems: 'center', paddingRight: 10 }}>
                        <AntDesign name='check' color={status ? settingApp.color : '#9CA7B2'} size={25} />
                    </TouchableOpacity>
                </View>
            </HeaderWapper>
        )
    }

    render() {
        const { newLabel } = this.state
        return (
            <View style={{ flex: 1, position:'absolute', backgroundColor:'red' }}>
                {this.renderHeader()}
                <View
                    style={{ height: 100, width: settingApp.width, backgroundColor: '#FFFFFF' }}
                >
                    <Text style={{ fontSize: 16, color: '#9CA7B2', paddingLeft:15, paddingTop:10 }}>NHẬP TÊN NHÃN</Text>
                    <TextInput
                        style={{ ...settingApp.shadow, height: 60, fontSize:16, paddingLeft:15 }}
                        onChangeText={(newLabel) => this.setState({ newLabel: newLabel })}
                        multiline={true}
                        autoFocus={true}
                        underlineColorAndroid='transparent'
                    />
                </View>
            </View>
        )
    }
}
export default CreateLables;