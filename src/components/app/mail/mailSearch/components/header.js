import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput } from 'react-native';
import { HeaderIndex, settingApp, ButtonCustom, HeaderWapper, HeaderScreen } from '../../../../../public'
import { Ionicons, AntDesign, FontAwesome, MaterialIcons } from '@expo/vector-icons'
import lodash from 'lodash';

class Header extends Component {

    constructor(props){
        super(props)
    }

    onSearchBox(text) {
        this.props.onSearch(text)
    }

    render() {
        const { listCheck, isShowLabels } = this.props
        let onChangeCallback = lodash.debounce(this.onSearchBox, 500);
        if (listCheck && listCheck.length > 0) {
            return (
                <HeaderWapper
                    backgroundColor='#FFFFFF'
                    style={{ ...settingApp.shadow, justifyContent: 'space-between' }}
                >
                    <View
                        style={{
                            width: (settingApp.width * 0.35),
                            backgroundColor: 'transparent',
                            flexDirection: 'row',
                            marginRight: 15,
                        }}
                    >
                        <TouchableOpacity
                            onPress={() =>this.props.close()}
                            style={{ justifyContent: 'center', paddingLeft: 10, width: 50, }}
                        >
                            <AntDesign name='left' size={20} color='#9CA7B2' />
                        </TouchableOpacity>

                        <View style={{ justifyContent: 'center' }}>
                            <Text style={{ color: settingApp.colorText, fontSize: 16 }}>{listCheck.length} </Text>
                        </View>
                    </View>

                    <View
                        style={{
                            width: (settingApp.width * 0.35),
                            backgroundColor: 'transparent',
                            flexDirection: 'row',
                            marginRight: 15
                        }}
                    >
                        <TouchableOpacity
                            onPress={() => this.props.removeMails()}
                            style={{ width: 50, justifyContent: 'center' }}
                        >
                            <Ionicons name='md-trash' size={25} color='#BFC7CF' />
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.props.showStart()}
                            style={{ width: 50, justifyContent: 'center' }}
                        >
                            <FontAwesome name='star' size={25} color='#BFC7CF' />
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.props.showLabels()}
                            style={{ width: 50, justifyContent: 'center' }}
                        >
                            <MaterialIcons name='label' size={25} color='#BFC7CF' />
                        </TouchableOpacity>
                    </View>
                </HeaderWapper>
            )
        }
        else {
            return (
                <HeaderWapper
                    style={{backgroundColor:'#FFFFFF', justifyContent:'center',}}
                >
                    <TouchableOpacity
                        onPress={() => this.props.navigation.pop()}
                        style={{width:50, backgroundColor:'transparent', justifyContent:'center', marginLeft:15 }}
                    >
                        <AntDesign name='left' size={20} color='#9CA7B2' />
                    </TouchableOpacity>

                    <TextInput
                        //ref={refs => this.textInput = refs}
                        onChangeText={onChangeCallback.bind(this)}
                        style={{ flex: 1, marginTop: 5 }}
                        autoCorrect={false}
                        autoFocus={true}
                        underlineColorAndroid='transparent'
                    />
                </HeaderWapper>
            )
        }

    }
}
export default Header;