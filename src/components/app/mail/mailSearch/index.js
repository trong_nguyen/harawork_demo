import Layout from './layout';
import { HaravanIc, HaravanHr } from '../../../../Haravan';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../state/action';
import { Utils, Toast } from '../../../../public';
import lodash from 'lodash';

class SearchMail extends Layout {
    constructor(props) {
        super(props);
        this.state = {
            showLabels: false,
            isLoading: false,
            listMail: [],
            stickyHeaderIndices: [],
            loadMore: false,
            oppenFilter: false,
            listLables: [],
            refreshing: false,
            listAdd: [],
            listDelete: [],
            listCheck: [],
            showStart: false,
            listRemove: [],
            listUndo: [],
            isError: false,
            listTitle: [],
            listReverse: [], 
            showTitle:false
        };
        this.collegue = props.app.colleague;
        this.page = 1;
        this.totalPage = null;
        this.currentList = [];
        this.listCreateBy = [];

        this.listStart = [];
        this.status = null;

        this.type = props.mail.typeMail;
        this.title = 'Hộp thư đến';
        this.hide = false;
        this.remove = false;
        this.listReverse = [];
        _thisIndex = this
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.mail.removeMail && JSON.stringify(nextProps.mail.removeMail) != JSON.stringify(this.props.mail.removeMail)) {
            const { listMail, listCheck } = this.state;
            const item = nextProps.mail.removeMail;
            const id = nextProps.mail.removeMail.id;
            listCheck.push(item)
            // const title = nextProps.mail.removeMail.subject
            let listTime = []
            listMail.map((item, index) => {
                this.state.listUndo.push(item)
                if (item.id && item.id === id) {
                    listTime.push(item)
                }
                return item;
            });
            let fillList = lodash.uniq(this.state.listUndo, this.state.listUndo);
            this.setState({ listUndo: fillList, listRemove: listTime, listReverse: [], listCheck }, () => this.removeMails())
        }

        if ( nextProps && JSON.stringify(nextProps.mail.typeMail) !== JSON.stringify(this.props.mail.typeMail)) {
            this.type = nextProps.mail.typeMail.type;
            this.title = nextProps.mail.typeMail.name;
            this.refreshData();
            this.setState({ isLoad: true }, () => {
                this.loadData(null);
            });
        }
        if (nextProps && nextProps.mail.checkStarred !== this.props.mail.checkStarred ) {
            const { listMail } = this.state;
            const id = nextProps.mail.checkStarred.id[0];
            const status = nextProps.mail.checkStarred.status;
            const newList = listMail.map(item => {
                if (item.id && item.id === id) {
                    item = {
                        ...item,
                        mailInfo: {
                            ...item.mailInfo,
                            isStarred: status,
                        },
                    };
                    this.listStart = [item];
                }
                return item;
            });
            this.setState({ listMail: newList });
        }

        if (nextProps.mail.checkLable && (nextProps.mail.checkLable !== this.props.mail.checkLable)) {
            const { listMail, listLables } = this.state;
            let id = [];
            id.push(nextProps.mail.checkLable.id)
            listMail.map(item => {
                const index = id.findIndex(m => m === item.id)
                if (index > -1) {
                    if (nextProps.mail.checkLable.labelsAdd.length > 0) {
                        this.state.listLables.map(j => {
                            const ind = nextProps.mail.checkLable.labelsAdd.findIndex(i => i.id == j.id)
                            if (ind > -1) {
                                item.listLables.push(j)
                            }
                        })
                    }
                    if (nextProps.mail.checkLable.labelsDelete.length > 0) {
                        nextProps.mail.checkLable.labelsDelete.map(j => {
                            const ind = item.listLables.findIndex(i => i.id == j.id)
                            if (ind > -1) {
                                item.listLables.splice(ind, 1);
                            }
                        })
                    }
                }
            })
            this.setState({ listMail })
        }
    }

    componentDidMount() {
        this.loadLables();
    }

    onSearch(text){
        this.setState({isLoading:true, showTitle:true}, () =>{
            this.refreshData()
            this.loadData(text)
        })
    }

    async removeMails() {
        const { listCheck, listMail } = this.state;
        this.listReverse = listCheck;
        const title = listCheck[0].subject;
        let newlistID = listCheck.map(e => e.id);
        let listTime = listCheck.map(e => e);
        for (i = 0; i < listCheck.length; i++) {
            if (listCheck[i].id) {
                const msg = `Đã xóa ${listCheck.length > 1 ? listCheck.length +' thư' : 'thư ' + title} thành công`;
                Toast.show(msg, _thisIndex.undoMail);
            }
            break;
        }
        this.removeTitle(listTime);
        this.closeHeader();
        this.setState({ listRemove: listTime })
        await setTimeout(() => (this.remove = true, this.deleted()), 4000);
    }

    async removeTitle(listTime) {
        this.listReverse = listTime;
        const { listUndo, listTitle, listMail } = this.state;
        const titleMail = lodash.uniq(listUndo, listUndo);
        const newList = listTime.sort((a, b) => {
            return new Date(b.createdAt) - new Date(a.createdAt);
        });
        const title = [];
        listMail.map(j => {
            if (!j.id) {
                title.push(j)
            }
        })
        newList.map(m => {
            const index = titleMail.findIndex(n => n.id === m.id)
            if (index > -1) {
                titleMail.splice(index, 1)
            }
        })

        this.setState({ listUndo: titleMail })
        title.map(a => {
            const ind = titleMail.findIndex(b => b === a)
            if (ind > -1) {
                if (!titleMail[ind + 1] || !titleMail[ind + 1].id) {
                    listTitle.push(titleMail[ind])
                }
            }
        })
        this.setState({ listTitle })
        if (this.state.listTitle.length == title.length) {
            this.setState({ isError: true })
        }
        else {
            this.setState({ isError: false })
        }
    }

    undoMail() {
        _thisIndex.reverse()
    }

    async reverse() {
        this.remove = false;
        this.listReverse.map(e => {
            const index = this.state.listUndo.findIndex(a => a.id === e.id)
            if (index < 0) {
                this.state.listUndo.push(e)
            }
        });
        const newList = this.state.listUndo.sort((a, b) => {
            return new Date(b.createdAt) - new Date(a.createdAt);
        });
        this.setState({
            listReverse: this.listReverse,
            listUndo: newList,
            listRemove: [],
            listTitle: [],
            listCheck: [],
            isError: false
        }, () => { this.listReverse = [] });
        this.deleted();
    }

    deleted() {
        if (this.listReverse.length > 0) {
            if (this.remove == true) {
                let newlistID = this.listReverse.map(e => e.id)
                let params = newlistID;
                HaravanIc.bulkMail(params)
                this.listReverse = [];
            }
        }
    }

    closeHeader() {
        this.setState({
            listCheck: [],
            listAdd: [],
            listDelete: []
        })
        this.listStart = [];
    }

    showLabels() {
        this.props.navigation.navigate('AddLabels', {
            listCheck: this.state.listCheck,
            data: this.state.listLables,
            updateList: (list => this.bulkLabel(list)),
        })
    }

    async bulkLabel(list) {
        const { itemCheck, labelsAdd, labelsDelete } = list;
        let mailids = this.state.listCheck.map(e => e.id)
        const labelAdd = labelsAdd.map(e => e.id);
        const labelDelete = labelsDelete.map(e => e.id)
        const params = {
            labelAdd,
            labelDelete,
            mailids
        };
        await HaravanIc.bulkLabel(params);
        this.setState({ detailLables: itemCheck, listAdd: labelsAdd, listDelete: labelsDelete }, () => {
            this.closeHeader()
        })
    }

    async bulkStarred(status) {
        const { listCheck, listMail } = this.state;
        let newList = []
        listCheck.map(e => {
            newList.push(e.id)
        })
        this.listStart = listCheck;
        this.status = status;
        const setStatus = listMail.map(e => {
            if (e.id) {
                const listID = newList.findIndex(m => m === e.id)
                if (listID > -1) {
                    e = { ...e, mailInfo: { ...e.mailInfo, isStarred: status } }
                }
            }
            return e;
        })
        this.setState({ isLoad: false, listMail: setStatus }, () => {
            this.listStart = []
            this.setState({ listCheck: [] })
        })
        await HaravanIc.statusStarMail(status, newList)
        this.status = null;
    }

    checkAvatar(item) {
        const { listCheck } = this.state;
        Toast.close();
        this.remove = true;
        if (listCheck.length === 0) {
            listCheck.push(item);
        }
        else {
            const indexList = listCheck.findIndex(e => e.id === item.id)
            if (indexList > -1) {
                listCheck.splice(indexList, 1)
            }
            else {
                listCheck.push(item)
            }
        }
        this.setState({
            listCheck: listCheck,
            listReverse: [],
            listRemove: [],
            listTitle: []
        })
    }

    refreshData() {
        this.page = 1;
        this.totalPage = null;
        this.currentList = [];
        this.listCreateBy = [];
    }

    refreshList() {
        this.refreshData();
        this.loadData(null);
    }

    async loadLables() {
        const result = await HaravanIc.getLabel(1);
        if (result && result.data && !result.error) {
            this.setState({ listLables: result.data.data });
        }
    }

    async setStart(item, status) {
        this.listStart = [item];
        this.status = null;
        const body = [item.mailInfo.id];
        const start = await HaravanIc.statusStarMail(status, body);
        if (!start.error) {
            Toast.show(`${status ? 'Đánh dấu' : 'Bỏ đánh dấu'} thành công`);
        } else {
            Toast.show('Lỗi');
        }
        this.listStart = [];
    }

    checkStarred(id, status) {
        const newList = [];
        newList.push(id);
        this.props.actions.mail.checkStarred({
            id: newList,
            status: status,
        });
    }

    async loadData(text) {
        const { totalPage, page, collegue, currentList, listCreateBy, type } = this;
        let listData = [];
        let listMail = currentList;
        let newList = [];
        if (!totalPage || (totalPage && !isNaN(totalPage) && page <= totalPage)) {
            const result = await HaravanIc.getListMail(type, page, collegue, text);
            if (result && result.data && !result.error) {
                listData = result.data.data;
                const totalCount = result.data.totalCount;
                listData = listData.sort((a, b) => {
                    return new Date(b.createdAt) - new Date(a.createdAt);
                });

                for (let i = 0; i < listData.length; i++) {
                    let item = {};
                    let listLables = listData[i].mailInfo.internalLabel != null && listData[i].mailInfo.internalLabel.length > 0 ? listData[i].mailInfo.internalLabel : []
                    if (listCreateBy.length == 0) {
                        const resultCreate = await HaravanHr.getColleague(listData[i].from.id);
                        if (resultCreate && resultCreate.data && !resultCreate.error) {
                            item = {
                                ...listData[i],
                                createdUserName: resultCreate.data.fullName,
                                createdUserAvatar: resultCreate.data.photo,
                                listLables: listLables,
                            };
                            newList.push(item);
                            listCreateBy.push({
                                key: listData[i].from.id,
                                data: { ...resultCreate.data },
                            });
                        }
                    } else {
                        const index = listCreateBy.findIndex(m => m.key === listData[i].from.id);
                        if (index < 0) {
                            const resultCreate = await HaravanHr.getColleague(
                                listData[i].from.id
                            );
                            if (resultCreate && resultCreate.data && !resultCreate.error) {
                                item = {
                                    ...listData[i],
                                    createdUserName: resultCreate.data.fullName,
                                    createdUserAvatar: resultCreate.data.photo,
                                    listLables: listLables,
                                };
                                newList.push(item);
                                listCreateBy.push({
                                    key: listData[i].from.id,
                                    data: { ...resultCreate.data },
                                });
                            }
                        } else {
                            item = {
                                ...listData[i],
                                createdUserName: listCreateBy[index].data.fullName,
                                createdUserAvatar: listCreateBy[index].data.photo,
                                listLables: listLables,
                            };
                            newList.push(item);
                        }
                    }
                }

                newList.map((e, i) => {
                    const date = Utils.formatLocalTime(e.createdAt, 'DD/MM/YYYY', true);
                    if (listMail.length == 0) {
                        listMail.push(date, e);
                    } else {
                        const index = listMail.findIndex(
                            m => m === Utils.formatLocalTime(e.createdAt, 'DD/MM/YYYY', true)
                        );
                        if (index < 0) {
                            listMail.push(date, e);
                        } else {
                            listMail.push(e);
                        }
                    }
                });

                // listMail = [ ...currentList, ...listMail]
                this.currentList = listMail;
                const arr = [];
                listMail.map((e, i) => {
                    if (!e.id) {
                        arr.push(i);
                    }
                });
                listMail.map(m => {
                    this.state.listUndo.push(m)
                })
                this.page = page + 1;
                this.totalPage = Math.ceil(totalCount / 20);
                this.setState({
                    isLoading: false,
                    listMail: listMail,
                    stickyHeaderIndices: arr,
                    loadMore: false,
                    isLoad: false,
                    refreshing: false,
                });
            }
        } else {
            this.setState({
                isLoading: false,
                listMail: listMail,
                stickyHeaderIndices: arr,
                loadMore: false,
                isLoad: false,
                refreshing: false,
            });
        }
    }

    componentWillUnmount() {
        this.props.actions.mail.typeMail('inbox?');
    }
}
const mapStateToProps = state => ({ ...state });
const mapDispatchToProps = dispatch => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};
export default connect(mapStateToProps, mapDispatchToProps)(SearchMail);
