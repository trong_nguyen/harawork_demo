import Layout from './layout';
import lodash from 'lodash';

class AddLabels extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            idLabel: [],
            itemCheck:[]
        }
        this.initial = [];
        this.data = props.navigation.state.params.data

        this.labelsAdd = [];
        this.labelsDelete = [];
    }

    componentDidMount(){
        if(this.props.navigation.state.params.initial){
            this.initial = [...this.props.navigation.state.params.initial];
            this.setState({
                itemCheck:this.props.navigation.state.params.initial
            })
        }
        else{
            const { itemCheck } = this.state;
            let listCheck = this.props.navigation.state.params.listCheck;
            listCheck.map(item =>{
               
                if(item.listLables && item.listLables.length > 0 ){
                    item.listLables.map(e =>{
                        const index = itemCheck.findIndex(m => m.id === e.id)
                        if(index < 0){
                            this.initial.push(e)
                            itemCheck.push(e)
                        }
                    })
                }
            })
            this.setState({itemCheck:itemCheck}, () =>{
                this.initial = this.initial
            })
        }
    }

    checkItem(item) {
        let itemCheck = [...this.state.itemCheck];
        const index = itemCheck.findIndex(e => e.id === item.id);
        if (index > -1) {
            itemCheck.splice(index, 1);
        } else {
            itemCheck.push(item);
        }
        this.setState({ itemCheck:itemCheck },() =>{
            this.getData(item);
        })
    }

    getData(item) {
        const { initial } = this;
        const indexInitial = initial.findIndex(e => e.id === item.id);
        const indexLabelAdd = this.labelsAdd.findIndex(i => i.id === item.id);
        const indexLabelDelete = this.labelsDelete.findIndex(j => j.id === item.id);
        if (indexInitial > -1) {
            if (indexLabelDelete > -1) {
                this.labelsDelete.splice(indexLabelDelete, 1);
            } else {
                this.labelsDelete.push(item);
            }
        } else {
            if (indexLabelAdd > -1) {
                this.labelsAdd.splice(indexLabelAdd, 1);
            } else {
                this.labelsAdd.push(item);
            }
        }
    }

    submit() {
        const { itemCheck } = this.state;
        this.props.navigation.state.params.updateList({
            itemCheck,
            labelsAdd: this.labelsAdd,
            labelsDelete: this.labelsDelete
        });
        this.labelsAdd = [];
        this.labelsDelete = [];
        this.props.navigation.pop()
    }

    close() {
        this.labelsAdd = [];
        this.labelsDelete = [];
        this.setState({ itemCheck: this.initial }, () =>{
            this.props.navigation.pop()
        });
    }
}

export default AddLabels;