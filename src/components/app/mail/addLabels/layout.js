import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { ButtonCustom, HeaderWapper, settingApp } from '../../../../public';

import { MaterialIcons } from '@expo/vector-icons';

import * as Animatable from 'react-native-animatable';

class Layout extends Component {
    renderHeader() {
        return (
            <HeaderWapper backgroundColor='#ffffff' style={{ justifyContent: 'space-between' }}>
                <TouchableOpacity
                    onPress={() => this.close()}
                    style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                    <MaterialIcons name='close' size={30} color='#BFC7CF' />
                </TouchableOpacity>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: settingApp.colorText, fontSize: 18 }}>
                        Gán nhãn là
                    </Text>
                </View>

                <TouchableOpacity
                    onPress={() => this.submit()}
                    style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                    <MaterialIcons name='check' size={30} color='#BFC7CF' />
                </TouchableOpacity>
            </HeaderWapper>
        )
    }

    renderContent() {
        const { data } = this;
        const { itemCheck } = this.state;
        return (
            <ScrollView>
                {
                    data && data.length > 0 ?
                        data.map((e, i) => {
                            let isCheck = itemCheck.findIndex(m => m.id === e.id);
                            isCheck = (isCheck > -1);
                            return (
                                <View key={i}>
                                    <ButtonCustom
                                        onPress={() => this.checkItem(e)}
                                        style={{ backgroundColor: 'transparent' }}>
                                        <View
                                            style={{
                                                height: 55,
                                                backgroundColor: '#ffffff',
                                                alignItems: 'center',
                                                flexDirection: 'row',
                                                borderBottomWidth: 1,
                                                paddingLeft: 10,
                                                paddingRight: 10,
                                                borderBottomColor: settingApp.colorSperator
                                            }}>
                                            <View style={{ width: 50 }}>
                                                <MaterialIcons name='label' size={30} color='#BFC7CF' />
                                            </View>

                                            <View style={{ flex: 1, justifyContent: 'center', marginRight: 20 }}>
                                                <Text style={{ color: settingApp.colorText, fontSize: 14 }} numberOfLines={1}>
                                                    {e.label}
                                                </Text>
                                            </View>

                                            <View style={{
                                                width: 23,
                                                height: 23,
                                                borderRadius: 3,
                                                borderColor: settingApp.colorSperator,
                                                borderWidth: 1,
                                                overflow: 'hidden'
                                            }}>
                                                {isCheck && <Animatable.View
                                                    animation='bounceIn'
                                                    duration={350}
                                                    style={{
                                                        flex: 1,
                                                        justifyContent: 'center',
                                                        alignItems: 'center',
                                                        backgroundColor: settingApp.color
                                                    }}>
                                                    <MaterialIcons name='check' color='#ffffff' size={20} />
                                                </Animatable.View>}
                                            </View>
                                        </View>
                                    </ButtonCustom>
                                </View>
                            )
                        })
                        : <View />
                }
            </ScrollView>
        )
    }

    render() {
        return (
            <View style={{ backgroundColor: '#FFFFFF', flex:1 }}>
                {this.renderHeader()}
                {this.renderContent()}
            </View>
        )
    }
}
export default Layout;