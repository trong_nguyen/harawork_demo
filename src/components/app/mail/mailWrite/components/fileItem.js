import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, Platform } from 'react-native';
import { imgApp, settingApp, Utils } from '../../../../../public';

class FileItem extends Component {

    render() {
        const { fileName, type, uri } = this.props.item;
        const typeUrl = Utils.checkTypeUrl(fileName);
        if (type === 'image') {
            return (
                <TouchableOpacity
                    onPress={() => this.props.control(uri)}
                    activeOpacity={1}
                    style={{
                        flex: 1,
                        width: 150,
                        marginRight: 10,
                        backgroundColor: '#EDEFF7',
                        borderRadius: 3,
                        ...styles.shadow
                    }}>
                    <Image
                        source={{ uri }}
                        style={{ flex: 1, width: undefined, height: undefined, borderRadius: 3 }}
                        resizeMode='cover'
                    />
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity
                    onPress={() => this.props.control(uri)}
                    activeOpacity={1}
                    style={{
                        flex: 1,
                        width: 150,
                        marginLeft: 10,
                        marginRight: 10,
                        padding: 10,
                        borderRadius: 3,
                        backgroundColor: '#EDEFF7',
                        justifyContent: 'space-between',
                        ...styles.shadow
                    }}>
                    <Text style={{ color: '#474747', fontSize: 10 }}>{fileName}</Text>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image
                            source={imgApp[`${typeUrl}`]}
                            style={{ width: 20, height: 20, marginRight: 10 }}
                            resizeMode='contain'
                        />
                        <Text style={{ color: settingApp.colorText, fontSize: 10 }}>{typeUrl}</Text>
                    </View>
                </TouchableOpacity>
            )
        }
    }
}

const styles = {
    shadow: {
        ...Platform.select({
            ios: {
                shadowOffset: { width: 2, height: 2 },
                shadowColor: 'black',
                shadowOpacity: 0.3,
            },
            android: {
                elevation: 1
            }
        })
    }
}

export default FileItem;