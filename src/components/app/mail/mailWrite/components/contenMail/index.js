import React, { Component } from 'react';
import { View, WebView, Text } from 'react-native';
import { settingApp, Utils } from '../../../../../../public';
import * as _ from 'lodash';
class ContentMail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            height: (settingApp.height)
        }
        this.webView = null;
        this.toFisrt = true
        this.contentHTML = null
    }


    setCodeJS(){
        const {description, collegue, type, listTo, title, draft} = this.props;
        let date = Utils.formatTime((new Date), 'HH:mm', true)
        let user = collegue.fullName;
        let data = (type === 'Edit') ? 
            `<div id="content" contenteditable='true' oninput="onInput()">
                <p><br></p>
                ${description}
            </div>
            ` 
            :
            (type === 'Chuyển tiếp' ? 
            `
            <div id="content" contenteditable='true' oninput="onInput()">
            <p><br></p>
            <p>---------- Nội dung chuyển tiếp ----------<p>
            <p>Từ: ${user}<p>
            <p>Tiêu đề: ${title}<p>
            <p>Thời gian: Hôm nay ${date}<p>
            <p><br></p>
            ${description}
            </div>
            `
            :
            `
            <div id="content" contenteditable='true' oninput="onInput()">
            <p><br></p>
            <p>.....................<p>
            <p>Hôm nay ${date} - ${user}<p>
            <p><br></p>
            ${description}
            </div>
            ` ) 
            ;
        //return data
        let htmlContent = `
        <!DOCTYPE html>
        <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="default-style" content="ie=edge">
                <style>
                body {
                    width: 100%;
                    height: auto;
                    margin: 0;
                    font-size: "16px";
                    font-family: 'Arial';
                    color: black;
                }
        
                html {
                    width: 100%;
                    height: auto;
                }
        
                div{
                    width: 100%;
                    height: 'auto';
                    font-size: "16px";
                    font-family: 'Arial';
                    color: black;
                }
        
                [contenteditable]:focus {
                    outline: 0px solid transparent;
                }
                </style>
            
                <script>
                    function onInput() {
                        var height = document.getElementById('content').offsetHeight;
                        var html = document.getElementById('content').innerHTML;
                        const data = JSON.stringify({ html, height });
                        window.postMessage(data, '*');
                    }
                </script>
            </head>
            ${data}
        </html>
            `
        return htmlContent
    }

    async componentDidMount(){
        this.contentHTML = await this.setCodeJS()
    }

    onMessage(e) {
        let { data } = e.nativeEvent;
        data = JSON.parse(data);
        this.props.getHTML(data.html);
        // this.setState({ height:( data.height > (settingApp.height)) ? (height + 20) : (settingApp.height) });
      }

    render() {
        const { height } = this.state;
        const { type} = this.props;
        // const source = (type === 'write') ? : 
        return (
            <View 
            style={{ flex: 1, padding: 10, backgroundColor:'transparent'}}>
                <Text
                    style={{ color: '#9CA7B2', fontSize: 14, paddingBottom:10 }}
                >NỘI DUNG</Text>

                
                {(type === 'write') ?
                    <WebView
                        ref={( ref ) => this.webView = ref}
                        style={{ flex: 1,backgroundColor:'transparent', maxHeight:'100%', minHeight:settingApp.height, padding:10 }}
                        source={require('./index.html')}
                        originWhitelist={['*']}
                        scrollEnabled={false}
                        javaScriptEnabled={true}
                        // injectedJavaScript={jsCode}
                        automaticallyAdjustContentInsets={true}
                        onMessage={data => this.onMessage(data)}
                        domStorageEnabled={true}
                        useWebKit={true}
                        //scalesPageToFit={true}
                    />:
                    <WebView
                    ref={( ref ) => this.webView = ref}
                    style={{ flex: 1,backgroundColor:'transparent', maxHeight:'100%', minHeight:settingApp.height, padding:10 }}
                    source={{html:this.contentHTML}}
                    originWhitelist={['*']}
                    scrollEnabled={false}
                    javaScriptEnabled={true}
                    // injectedJavaScript={jsCode}
                    automaticallyAdjustContentInsets={true}
                    onMessage={data => this.onMessage(data)}
                    domStorageEnabled={true}
                    useWebKit={true}
                    //scalesPageToFit={true}
                />
                }
                
            </View>
        )
    }
}

export default ContentMail;