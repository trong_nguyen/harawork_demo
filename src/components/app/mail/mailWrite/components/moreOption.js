import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Modal, Image, TextInput, ScrollView, Platform, Alert } from 'react-native';
import { settingApp } from '../../../../../public';

class MoreOption extends Component {
    constructor(props) {
        super(props);
    }

    render(){
        return (
            <Modal
                visible ={this.props.visible}
                animationType='none'
                transparent={true}
                onRequestClose={() => this.props.close()}
            >
            <View style={{flex: 1, width:settingApp.width, position:'absolute', height:settingApp.height, backgroundColor:'transparent' }}>
                <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => this.props.close()}
                    style={{ backgroundColor: 'transparent', flex: 1 }}>
                    <TouchableOpacity
                        activeOpacity={1}
                        onPress={() => this.props.close()}
                        style={styles.container}>
                        <View style={styles.triangle} />
                        <View style={styles.cover}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.props.cancel();
                                    this.props.close();
                                }}
                                style={styles.inten}>
                                <Text style={styles.content}>Bỏ nháp</Text>
                            </TouchableOpacity>
                        </View>
                    </TouchableOpacity>
                </TouchableOpacity>
            </View>
        </Modal>
        )
    }
}
const styles = {
    container: {
        flex: 1,
        ...Platform.select({
            android: {
                marginTop: settingApp.statusBarHeight + 20,
            },
            ios: {
                marginTop: settingApp.statusBarHeight + 44,
            },
        }),
        backgroundColor: 'rgba(0,0,0,0.5)',
        paddingLeft: 20,
        paddingRight: 20,
    },
    inten: {
        height: 50,
        width: 180,
        backgroundColor: '#FFFFFF',
    },
    cover: {
        overflow: 'hidden',
        width: 180,
        marginLeft: 150,
        backgroundColor: '#FFFFFF',
        marginTop: -15,
    },
    triangle: {
        width: 15,
        height: 20,
        backgroundColor: '#FFFFFF',
        transform: [{ rotate: '45deg' }],
        marginLeft: 190,
        marginTop: 5,
        borderRadius: 2.5,
    },
    content: {
        flex: 1,
        color: settingApp.colorText,
        fontSize: 16,
        height: 23,
        paddingTop: 15,
        paddingLeft: 10,
        paddingBottom: 5,
    },
};
export default MoreOption;