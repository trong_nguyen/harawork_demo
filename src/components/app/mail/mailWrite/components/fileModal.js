import React, { Component } from 'react';
import { Animated, View, Text, Modal, TouchableOpacity } from 'react-native';
import { settingApp } from '../../../../../public';

class FileModal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            bottom: new Animated.Value(-120)
        }
    }

    show() {
        Animated.timing(
            this.state.bottom,
            {
                toValue: 0,
                duration: 150
            }
        ).start();
    }

    close() {
        Animated.timing(
            this.state.bottom,
            {
                toValue: -120,
                duration: 150
            }
        ).start(() => this.props.close());
    }

    render() {
        const { width } = settingApp;
        return (
            <Modal
                animationType="none"
                transparent={true}
                visible={this.props.visible}
                onShow={() => this.show()}
                onRequestClose={() => this.close()}>
                <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.5)' }}>
                    <TouchableOpacity
                        activeOpacity={1}
                        onPress={() => this.close()}
                        style={{ flex: 1 }}
                    />
                    <Animated.View
                        style={{
                            position: 'absolute',
                            left: 0,
                            right: 0,
                            bottom: this.state.bottom,
                            width,
                            height: 110, backgroundColor: '#ffffff'
                        }}>
                        <TouchableOpacity
                            onPress={async () => {
                                await this.props.remove();
                                this.close();
                            }}
                            style={styles.textContainer}>
                            <Text style={styles.text}>Xóa</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.close()}
                            style={styles.textContainer}>
                            <Text style={styles.text}>Hủy</Text>
                        </TouchableOpacity>
                    </Animated.View>
                </View>
            </Modal>
        )
    }
}

const styles = {
    textContainer: {
        flex: 1, justifyContent: 'center', paddingLeft: 15
    },
    text: { fontSize: 16, color: settingApp.colorText }
}

export default FileModal;