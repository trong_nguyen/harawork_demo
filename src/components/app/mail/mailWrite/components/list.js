import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { settingApp, imgApp } from '../../../../../public';

class List extends Component {

    render() {
        const { list } = this.props;
        return (
            <View style={{ flexDirection: 'row', flexWrap: 'wrap', alignItems: 'flex-start', marginTop: 10 }}>
                {list.map((e, i) => {
                    return (
                        <TouchableOpacity
                            style={{ marginRight: 10, marginBottom: 10, backgroundColor: 'transparent'}} key={i}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ color: '#0279C7', fontSize: 14 }}>
                                        {e.fullName} {(i == (list.length -1)) ? '' :','}
                                    </Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    )
                })}
            </View>
        )
    }
}
export default List;