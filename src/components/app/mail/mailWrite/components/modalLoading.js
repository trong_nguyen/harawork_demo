import React, { Component } from 'react';
import { Animated, View, Text, Modal, TouchableOpacity } from 'react-native';
import { settingApp, Loading } from '../../../../../public';

class ModalLoad extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Modal
                animationType="none"
                transparent={true}
                visible={this.props.visible}
                onRequestClose={() => this.close()}>
                <View style={{ flex: 1, backgroundColor:'rgba(0,0,0,0.5)'}}>
                    <Loading/>
                </View>
            </Modal>
        )
    }
}

const styles = {
    textContainer: {
        flex: 1, justifyContent: 'center', paddingLeft: 15
    },
    text: { fontSize: 16, color: settingApp.colorText }
}

export default ModalLoad;