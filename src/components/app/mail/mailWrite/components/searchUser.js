import React, { Component } from 'react';
import {
    View,
    Modal,
    TouchableOpacity,
    Text,
    TextInput,
    ScrollView,
    Image,
    KeyboardAvoidingView,
    StyleSheet,
    Platform
} from 'react-native';
import { HeaderWapper, settingApp, imgApp, SvgCP } from '../../../../../public';
import { Ionicons } from '@expo/vector-icons';
import lodash from 'lodash';
import HistoryTabs from '../../tab/historyTab';
import TabList from '../../../public/tree';

class SearchUser extends Component {
    constructor(props) {
        super(props);

        this.state = {
            listDefaul: props.navigation.state.params.list,
            list: [],
            listRemove:[],
            text: '',
            disable: true,
            tab: 1,
            ishowFind:false
        }
        this.collegue = props.navigation.state.params.dataUser;
        this.userInfo = props.navigation.state.params.dataAuth;
        this.title = props.navigation.state.params.title;
        this.getList = props.navigation.state.params.getList;
    }

    componentDidMount() {
        const { listDefaul, list } = this.state;
        if(listDefaul && listDefaul.length > 0){
            listDefaul.map(item =>{
                list.push(item)
            })
        }
        this.setState({list})
        setTimeout(() => {
            if (this.textInput && this.textInput.focus) {
                this.textInput.focus()
            }
        }, 500);
    }

    listUser(item) {
        const { list, listRemove } = this.state;
        if (list.length == 0) {
            const indexRemove = listRemove.findIndex(m => m.fullName === item.fullName)
                list.push(item);
                if(indexRemove > -1){
                    listRemove.splice(indexRemove, 1)
                }
            this.setState({listRemove})
        }
        else {
            const index = list.findIndex(e => e.fullName === item.fullName)
            if (index < 0) {
                const indexRemove = listRemove.findIndex(m => m.fullName === item.fullName)
                list.push(item);
                if(indexRemove > -1){
                    listRemove.splice(indexRemove, 1)
                }
                this.setState({listRemove})
            }
            else{
                list.splice(index, 1)
            }
        }
        this.setState({list})
    }

    onSearchBox(text) {
        if(text.length != 0){
            this.setState({ishowFind:true})
            this.HistoryTabs.onSearch(text)
        }
        else{
            this.setState({ishowFind:false})
        }
        
    }

    removeItem(item){
        const { list, listRemove } = this.state;
        const index = list.findIndex(e => (e.id == item.id) && (e.name === item.name) )
        if(index > -1){
            listRemove.push(item)
            list.splice(index, 1)
        }
        this.setState({list, listRemove})
    }

    setList(){
        this.getList(this.state.list)
        this.props.navigation.pop()
    }

    close(){
        this.getList(this.state.listDefaul)
        this.props.navigation.pop()
    }

    renderHeader() {
        const { disable, list } = this.state;
        let checkList = list.length >0 ;
        const marginTop = Platform.OS === 'android' ? -20 : 0;
        return (
            <HeaderWapper backgroundColor='#ffffff' style={{ justifyContent: 'space-between' }}>
                <TouchableOpacity
                    onPress={() => this.close()}
                    style={styles.headerBack}>
                    <Ionicons name='ios-close' size={30} color='#9CA7B2' style={styles.headerIcon} />
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => this.setList()}
                    style={[styles.headerBack, { marginRight: -10 }]}
                >
                    <Ionicons
                        name='md-checkmark'
                        size={20}
                        color={checkList ? settingApp.color : '#9CA7B2' }
                        style={styles.headerIcon}
                    />
                </TouchableOpacity>
            </HeaderWapper>
        )
    }

    scrollToEnd(width, height){
        if(height >= (settingApp.height*0.4)){
            this.ScrollView.scrollToEnd({animated: true})
        }
    }

    renderContent() {
        const { list } = this.state;
        const { height } = settingApp;
        let onChangeCallback = lodash.debounce(this.onSearchBox, 500);
        return (
            <View style={styles.title}>
                <Text style={styles.textTitle}>
                    {this.title}
                </Text>
                <ScrollView 
                ref={ref => this.ScrollView = ref}
                onContentSizeChange={(width, height)=>{        
                    this.ScrollView.scrollToEnd({animated: true});
                }}
                style={{ minHeight:50  }}> 
                <View style={{flexDirection: 'row', flexWrap: 'wrap', alignItems: 'flex-start', minHeight:50 }}>
                    {list.map((e, i) => {
                        return (
                            <View
                                key={i}
                                style={{ marginRight: 10, marginBottom: 10, backgroundColor: 'rgba(33, 70, 155, 0.08)', padding: 10, height:40 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        {e.icon ? e.icon : <SvgCP.job/>}
                                    </View>
                                    <View style={{ justifyContent: 'center', alignItems: 'center', marginLeft:10 }}>
                                        <Text style={{ color: '#21469B', fontSize: 14 }}>
                                        {!e.type == 4 ?
                                            e.departmentName || e.departmentName ? 
                                          ((e.jobtitleName ? e.jobtitleName : e.fullName) + ' tại ' + e.departmentName) : ( e.fullName)
                                            : ( e.fullName)
                                        }
                                        </Text>
                                    </View>
                                    <TouchableOpacity style={{
                                        width: 40,
                                        justifyContent: 'center',
                                        alignItems: 'flex-end',
                                        paddingRight: 10,
                                    }}
                                    onPress={() => this.removeItem(e)}
                                    >
                                        <Ionicons name='ios-close' size={23} color='#9CA7B2' />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        )
                    })}
                </View>
                </ScrollView>
                <View style={{paddingTop:10, height:30, width:settingApp.width, backgroundColor:'transparent'}}>
                    <TextInput
                        ref={refs => this.textInput = refs}
                        onChangeText={onChangeCallback.bind(this)}
                        style={{ flex: 1, height: 30}}
                        autoCorrect={false}
                        underlineColorAndroid='transparent'
                    />
                </View>
            </View>
        )
    }

    render() {
        const { tab, ishowFind } = this.state;
        const { OS } = Platform;
        if (OS === 'android') {
            return (
                <KeyboardAvoidingView style={{ flex: 1 }} behavior='padding'>
                        {this.renderHeader()}
                            {this.renderContent()}
                            {(ishowFind == false)? 
                                <TabList 
                                    screenProps ={{
                                        listUser: (item) => this.listUser(item),
                                        list: this.state.list,
                                        collegue: this.collegue,
                                        userInfo:this.userInfo,
                                        listRemove:this.state.listRemove 
                                    }}
                                />
                            :
                            <HistoryTabs
                                    ref={refs => this.HistoryTabs = refs}
                                    listUser={(item) => this.listUser(item)}
                                    list={this.state.list}
                                    collegue={this.collegue}
                                    userInfo = {this.userInfo}
                                    listRemove ={ this.state.listRemove }
                                />
                            }
                </KeyboardAvoidingView>
            )
        } else {
            return (
                <KeyboardAvoidingView style={{ flex: 1 }} behavior='padding'>
                    {this.renderHeader()}
                        {this.renderContent()}
                        <View style={{ borderBottomColor: settingApp.colorSperator, borderBottomWidth: 1, flex:1}}>
                        {(ishowFind == false)? 
                            <TabList 
                                screenProps ={{
                                    listUser: (item) => this.listUser(item),
                                    list: this.state.list,
                                    collegue: this.collegue,
                                    userInfo:this.userInfo,
                                    listRemove:this.state.listRemove 
                                 }}
                            />
                            :
                            <HistoryTabs
                                    ref={refs => this.HistoryTabs = refs}
                                    listUser={(item) => this.listUser(item)}
                                    list={this.state.list}
                                    collegue={this.collegue}
                                    userInfo = {this.userInfo}
                                    listRemove ={ this.state.listRemove }
                                />
                            }
                        </View>
                </KeyboardAvoidingView>
            )
        }
    }

}

const styles = StyleSheet.create({
    contatner: { flex: 1 },
    headerBack: { backgroundColor: 'transparent', width: 55, height: 44, justifyContent: 'center', alignItems: 'center' },
    headerIcon: { marginLeft: -15 },
    title: {
        padding: 10,
        backgroundColor: '#FFFFFF',
        minHeight:80,
        maxHeight:(settingApp.height*0.4),
        marginBottom:5,
    },
    textTitle: { color: '#9CA7B2', fontSize: 14 },
    cover: {
        backgroundColor: 'transparent',
        height: 23,
        width: 23,
        overflow: "hidden",
        marginLeft: '46%',
        marginTop: -10
    },
    triangle1: {
        width: 0,
        height: 0,
        borderLeftWidth: 4,
        borderRightWidth: 4,
        borderBottomWidth: 8,
        borderStyle: 'solid',
        backgroundColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: '#DADADA',
        marginLeft: 7
    },
    triangle2: {
        width: 0,
        height: 0,
        borderLeftWidth: 3,
        borderRightWidth: 3,
        borderBottomWidth: 6,
        borderStyle: 'solid',
        backgroundColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: '#ffffff',
        position: 'absolute',
        top: 3,
        left: 8
    }
});

export default SearchUser;