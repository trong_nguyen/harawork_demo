import React, { Component } from 'react';
import {
    Animated,
    View,
    Text,
    TouchableOpacity,
    Modal,
    CameraRoll,
    FlatList,
    Image,
    Platform,
} from 'react-native';
import { settingApp, Utils, imgApp, HeaderWapper } from '../../../../../public';
import { Entypo, Ionicons } from '@expo/vector-icons';
import * as Permissions from 'expo-permissions';
import * as FileSystem from 'expo-file-system'
import * as ImagePicker from 'expo-image-picker'

class Attachment extends Component {
    constructor(props) {
        super(props);


        const bottom = -(settingApp.height * 0.5 + 10);

        this.state = {
            photos: [],
            file: [],
            selectedFile: [],
            bottom: new Animated.Value(bottom)
        }

        this.intialFile = [];
    }

    async componentDidMount() {
        try {
            const granted = await Permission.request(
                Permission.PERMISSIONS.READ_EXTERNAL_STORAGE,
              {
                'title': 'Access Storage',
                'message': 'Access Storage for the pictures'
              }
            )
            if (granted === Permission.RESULTS.GRANTED) {
              console.log("You can use read from the storage")
            } else {
              console.log("Storage permission denied")
            }
          } catch (err) {
            //console.warn(err)
          }
          
        this.loadData();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.isVisible) {
            this.open();
        }
    }

    async loadData() {
        // const result = await CameraRoll.getPhotos({ first: 9999, assetType: 'Photos', });
        // let file = await FileSystem.readDirectoryAsync(FileSystem.documentDirectory);
        // file = this.checkFile(file);
        // const photos = result.edges;
        // this.setState({ photos, file });
        if(Platform.OS==='ios'){
            const result = await CameraRoll.getPhotos({ first: 9999, assetType: 'Photos', groupTypes: "All"});
            const photos = result.edges;
            this.setState({ photos });
        }
        else{
            const result = await CameraRoll.getPhotos({ first: 9999, assetType: 'Photos'});
            const photos = result.edges;
            this.setState({ photos });
        }
    }

    checkFile(list) {
        const file = list.filter(url => {
            if ((url.match(/\.(pdf|doc|docx|xls|xlsx|txt)$/) != null)) {
                return url;
            }
        })
        return file;
    }

    open() {
        Animated.timing(
            this.state.bottom,
            {
                toValue: 0,
                duration: 350
            }
        ).start();
    }

    close(status) {
        Animated.timing(
            this.state.bottom,
            {
                toValue: -(settingApp.height * 0.5 + 10),
                duration: 350
            }
        ).start(() => {
            if (!status) {
                this.setState({ selectedFile: [...this.intialFile] })
            }
            this.props.close()
        });
    }

    selectFile(file) {
        const { selectedFile } = this.state;
        const index = selectedFile.findIndex(e => e.uri === file.uri);
        if (index > -1) {
            selectedFile.splice(index, 1);
        } else {
            selectedFile.push(file);
        }
        this.setState({ selectedFile });
    }

    async askPermissionsAsync(){
        const camera = await Permissions.askAsync(Permissions.CAMERA);
        const cameraRoll = await Permissions.askAsync(Permissions.CAMERA_ROLL);

        this.setState({
        hasCameraPermission: camera.status === 'granted',
        hasCameraRollPermission: cameraRoll.status === 'granted'
        });
    };

    async takePhotos() {
        const option = {
            allowsEditing: true,
            aspect: [1, 1],
            base64: false,
        }
        const resutl = await ImagePicker.launchCameraAsync(option);
        if (resutl && !resutl.cancelled) {
            await CameraRoll.saveToCameraRoll(resutl.uri);
            this.loadData();
        }
    }

    async useCameraHandler(){
        await this.askPermissionsAsync();
        let result = await ImagePicker.launchCameraAsync({
        allowsEditing: true,
        aspect: [4, 4],
        base64: false,
        });
        if (result && !result.cancelled) {
            await CameraRoll.saveToCameraRoll(result.uri);
            this.loadData();
        }
        //this.insertImage(result.uri);
    };

    renderHeadeList() {
        return (
            <TouchableOpacity
                onPress={() => this.useCameraHandler()}
                style={{
                    flex: 1,
                    width: 150,
                    marginRight: 10,
                    backgroundColor: '#EDEFF7',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginLeft: 10,
                    ...styles.shadow
                }}>
                <Entypo
                    name='camera'
                    size={50}
                    color='#ACB4BC'
                />
            </TouchableOpacity>
        )
    }

    renderPhotos(obj) {
        const { item, index } = obj;
        const { uri } = item.node.image;
        const isCheck = (this.state.selectedFile.findIndex(e => e.uri === uri) > -1);
        let fileName = item.node.image.uri;
        fileName = `${fileName.substr(fileName.lastIndexOf('/') + 1)}.jpeg`;
        return (
            <TouchableOpacity
                key={index}
                activeOpacity={1}
                onPress={() => this.selectFile({
                    fileName,
                    type: 'image',
                    uri,
                })}
                style={{
                    flex: 1,
                    width: 150,
                    marginRight: 10,
                    backgroundColor: '#EDEFF7',
                    borderRadius: 3,
                    ...styles.shadow
                }}>
                <Image
                    source={{ uri }}
                    style={{ flex: 1, width: undefined, height: undefined, borderRadius: 3 }}
                    resizeMode='cover'
                />
                <View style={{ position: 'absolute', top: 5, left: 5 }}>
                    {isCheck ?
                        <Ionicons name='ios-checkmark-circle' color={settingApp.color} size={23} /> :
                        <Entypo name='circle' color='#ffffff' size={20} />
                    }
                </View>
            </TouchableOpacity>
        )
    }

    renderFile(file) {
        const typeUrl = Utils.checkTypeUrl(file);
        const uri = `${FileSystem.documentDirectory}/${file}`;
        const isCheck = (this.state.selectedFile.findIndex(e => e.uri === uri) > -1);
        return (
            <TouchableOpacity
                activeOpacity={1}
                onPress={() => this.selectFile({
                    fileName: file,
                    type: 'file',
                    uri,
                    
                })}
                style={{
                    flex: 1,
                    width: 150,
                    marginLeft: 10,
                    marginRight: 10,
                    padding: 10,
                    borderRadius: 3,
                    backgroundColor: '#EDEFF7',
                    justifyContent: 'space-between',
                    ...styles.shadow
                }}>
                <Text style={{ color: '#474747', fontSize: 10 }}>{file}</Text>

                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Image
                        source={imgApp[`${typeUrl}`]}
                        style={{ width: 20, height: 20, marginRight: 10 }}
                        resizeMode='contain'
                    />
                    <Text style={{ color: settingApp.colorText, fontSize: 10 }}>{typeUrl}</Text>
                </View>

                <View style={{ position: 'absolute', top: 5, left: 5 }}>
                    {isCheck ?
                        <Ionicons name='ios-checkmark-circle' color={settingApp.color} size={23} /> :
                        <Entypo name='circle' color='#ffffff' size={20} />
                    }
                </View>
            </TouchableOpacity>
        )
    }

    async getFile() {
        const { selectedFile } = this.state;
        this.intialFile = [...selectedFile];
        await this.props.selectFile(selectedFile);
        this.close(true);
    }

    checkData() {
        const { selectedFile } = this.state;
        const { intialFile } = this;
        if (JSON.stringify(selectedFile) !== JSON.stringify(intialFile)) {
            return true;
        }
        return false;
    }

    renderHeader() {
        const disable = !this.checkData();
        return (
            <HeaderWapper
                isModal={true}
                backgroundColor='#ffffff' style={{ justifyContent: 'space-between' }}>
                <TouchableOpacity
                    onPress={() => this.close()}
                    style={{ backgroundColor: 'transparent', width: 55, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                    <Ionicons name='ios-close' size={30} color='#9CA7B2' style={{ marginLeft: -15 }} />
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => this.getFile()}
                    style={[{ backgroundColor: 'transparent', width: 55, height: 44, justifyContent: 'center', alignItems: 'center', marginRight: -10 }]}
                    disabled={disable}
                >
                    <Ionicons
                        name='md-checkmark'
                        size={20}
                        color={disable ? '#9CA7B2' : settingApp.color}
                        style={{ marginLeft: -15 }}
                    />
                </TouchableOpacity>
            </HeaderWapper>
        )
    }

    render() {
        const { height } = settingApp;
        const { bottom } = this.state;
        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={this.props.isVisible}
                onRequestClose={() => this.props.close()}>
                {this.renderHeader()}
                <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => this.close()}
                    style={{
                        flex: 1,
                        backgroundColor: 'transparent'
                    }}

                />
                <Animated.View
                    style={{
                        position: 'absolute',
                        left: 0,
                        right: 0,
                        bottom,
                        height: (height * 0.5),
                        backgroundColor: '#ffffff',
                        ...styles.shadow
                    }}
                >
                    <View style={{
                        flex: 1,
                        borderBottomWidth: 1,
                        borderBottomColor: settingApp.colorSperator,
                        paddingTop: 10,
                        paddingBottom: 10
                    }}>
                        <Text style={{ color: '#9CA7B2', fontSize: 14, marginLeft: 10 }}>
                            THƯ VIỆN ẢNH
                    </Text>
                        <FlatList
                            style={{ paddingTop: 10, paddingBottom: 10 }}
                            data={this.state.photos}
                            extraData={this.state}
                            keyExtractor={(item, index) => '' + item.node.timestamp}
                            ListHeaderComponent={this.renderHeadeList()}
                            renderItem={obj => this.renderPhotos(obj)}
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                        />

                    </View>

                    <View style={{
                        flex: 1,
                        paddingTop: 10,
                        paddingBottom: 10
                    }}>
                        <Text style={{ color: '#9CA7B2', fontSize: 14, marginLeft: 10 }}>
                            TỆP ĐÍNH KÈM GẦN ĐÂY
                        </Text>
                        <FlatList
                            style={{ paddingTop: 10, paddingBottom: 10 }}
                            data={this.state.file}
                            extraData={this.state}
                            keyExtractor={(item, index) => '' + index+''+item.id}
                            renderItem={({ item }) => this.renderFile(item)}
                            ListEmptyComponent={(
                                <View style={{
                                    flex: 1,
                                    backgroundColor: '#ffffff',
                                    width: settingApp.width,
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}>
                                    <Text style={{ color: '#9CA7B2', fontSize: 14, marginLeft: 10 }}>
                                        Không tìm thấy tệp
                                    </Text>
                                </View>
                            )}
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>
                </Animated.View>
            </Modal>
        )
    }
}

const styles = {
    shadow: {
        ...Platform.select({
            ios: {
                shadowOffset: { width: 2, height: 2 },
                shadowColor: 'black',
                shadowOpacity: 0.3,
            },
            android: {
                elevation: 3
            }
        })
    }
}

export default Attachment;