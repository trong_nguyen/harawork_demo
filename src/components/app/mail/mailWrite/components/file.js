import React, { Component } from 'react';
import { View, FlatList } from 'react-native';
import FileItem from './fileItem';
import FileModal from './fileModal';
import { settingApp } from '../../../../../public';

class File extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showControl: false,
            uriSelect: null
        }
    }

    control(item) {
        this.setState({
            showControl: true,
            uriSelect: item
        })
    }

    close() {
        this.setState({
            showControl: false,
            uriSelect: null
        });
    }

    renderItem(item) {
        return (
            <FileItem
                item={item}
                control={(item) => this.control(item)}
            />
        )
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <FlatList
                    data={this.props.data}
                    extraData={this.props}
                    keyExtractor={(item, index) => item.uri}
                    ListHeaderComponent={<View style={{ marginLeft: 10 }} />}
                    renderItem={({ item }) => this.renderItem(item)}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                />
                <FileModal
                    visible={this.state.showControl}
                    remove={() => this.props.remove(this.state.uriSelect)}
                    close={() => this.close()}
                />
            </View>
        )
    }
}

export default File;