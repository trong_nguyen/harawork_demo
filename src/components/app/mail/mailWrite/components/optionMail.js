import React, { Component } from 'react';
import { Animated, View, Text, Modal, TouchableOpacity, Platform } from 'react-native';
import { settingApp } from '../../../../../public';

class OptionMail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            bottom: new Animated.Value(120)
        }

        this.list = [
            { name: 'Trả lời', },
            { name: 'Trả lời tất cả'},
            { name: 'Chuyển tiếp'},
        ];
    }

    show() {
        Animated.timing(
            this.state.bottom,
            {
                toValue: 0,
                duration: 150
            }
        ).start();
    }

    close() {
        Animated.timing(
            this.state.bottom,
            {
                toValue: 120,
                duration: 150
            }
        ).start(() => this.props.close());
    }

    render() {
        const { width } = settingApp;
        return (
            <Modal
                animationType="none"
                transparent={true}
                visible={this.props.visible}
                onShow={() => this.show()}
                onRequestClose={() => this.close()}>
                <View 
                onPress={() => this.close()}
                style={styles.container}>
                    <TouchableOpacity
                        activeOpacity={1}
                        onPress={() => this.close()}
                        style={{ flex: 1 }}
                    />
                    <Animated.View
                        style={{
                            position: 'absolute',
                            width:200,
                            backgroundColor: '#ffffff',
                            marginLeft:(settingApp.width*0.2),
                            ...settingApp.shadow
                        }}>{
                            this.list.map((item, index) =>{
                                return(
                                    <TouchableOpacity
                                        key={index}
                                        onPress={() => {
                                            //await this.props.remove();
                                            this.props.setTitle(item.name)
                                            this.close();
                                        }}
                                        style={styles.textContainer}>
                                        <Text style={styles.text}>{item.name}</Text>
                                    </TouchableOpacity>
                                )
                            })
                        }
                    </Animated.View>
                </View>
            </Modal>
        )
    }
}

const styles = {
    textContainer: {
        flex: 1, justifyContent: 'center', paddingLeft: 15,  borderTopColor:'#DAE3EA', borderTopWidth: 1 , height:50
    },
    text: { fontSize: 16, color:'#0279C7', },

    container: {
            flex: 1,
            ...Platform.select({
                android: {
                    marginTop: settingApp.statusBarHeight + 20,
                },
                ios: {
                    marginTop: settingApp.statusBarHeight + 44,
                },
            }),
            backgroundColor: 'transparent',
            width: settingApp.width
        },
    menu:{
            flex: 1,
            ...Platform.select({
                android: {
                    maxHeight: (settingApp.height - (settingApp.statusBarHeight + 40)),
                },
                ios: {
                    maxHeight: (settingApp.height - (settingApp.statusBarHeight + 44)),
                },
            }),
        },
}

export default OptionMail;