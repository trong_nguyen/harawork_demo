import Layout from './layout';
import React, { Component } from 'react'
import { findNodeHandle , Alert, AsyncStorage} from 'react-native';
import actions from '../../../../state/action';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { HaravanIc, HaravanHr } from '../../../../Haravan';
import { Toast, SvgCP, keyStore, Utils } from '../../../../public';

class MailWrite extends Layout {
    constructor(props) {
        super(props)
        this.state={
            moreOption:false,
            showCC: false,
            isLoad:false,
            title: '',
            disable: true,
            listTo:[],
            listCC:[],
            content:'',
            image:null,
            file: [],
            listAttach:[],
            modalVisibleAttachment:false,
            isShow: false,
            titleHeader: this.props.navigation.state.params.type,
        }
        this.collegue = this.props.app.colleague;
        this.userInfo = this.props.app.authHaravan.userInfo;
        this.dataRep = props.navigation.state.params.data;
        this.checkReply = props.navigation.state.params.checkReply;
        this.isSent = false;

        this.dataContact = props.navigation.state.params.dataContact

        this.item = props.navigation.state.params.item
    }

    componentDidMount(){
        if(this.dataRep){
            this.loadReply()
        }

        if(this.dataContact){
            let newListTo = []
            const { dataContact } = this
            let item = {...dataContact, typeMail:4, subject:dataContact.jobtitleName}
            newListTo.push(item)
            this.setState({listTo:newListTo})
        }
    }

    showError(mess){
        Alert.alert(
            'Thông báo',
            `${mess}`,
            [
                { text: 'Đồng ý', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false }
        )
    }

    async loadReply(){
        const { dataRep } = this;
        const { titleHeader, listTo, listCC } = this.state;
        let date = Utils.formatTime((new Date), 'HH:mm', true)
        let user = this.collegue.fullName
        let contentRep = `<p>.....................<p>
        <p>Hôm nay ${date} - ${user}<p>
        <p><br></p>
        ${dataRep.content}`
        let contentDrat = `<p><br></p>
        ${dataRep.content}`

        if(titleHeader !== 'write'){
            if(titleHeader === 'Trả lời'){
                const result = await HaravanHr.getColleague(dataRep.from.id)
                let infoMe = (result && result.data && result.data.haraId == this.collegue.haraId); 
                if(result && result.data && !result.error){
                    const data ={...result.data, fullName:((infoMe==true )? 'Tôi': result.data.fullName), name: result.data.fullName ,type: 4, typeMail:4, id: result.data.haraId, icon:<SvgCP.me/>, iconCheck:<SvgCP.meClick/>}
                    listTo.push(data)
                }
                this.setState({ listTo, title:dataRep.subject, content:contentRep, disable:false })
            }
            else if((titleHeader === 'Trả lời tất cả') || (titleHeader === 'Edit')){
                if(dataRep.from){
                    const result = await HaravanHr.getColleague(dataRep.from.id)
                    let infoMe = (result && result.data && result.data.haraId == this.collegue.haraId); 
                    if(result && result.data && !result.error){
                        const data ={...result.data, fullName:((infoMe==true )? 'Tôi': result.data.fullName), name: result.data.fullName  ,type: 4, typeMail:4, id: result.data.haraId, icon:<SvgCP.me/>, iconCheck:<SvgCP.meClick/>}
                        listTo.push(data)
                    }
                }

                if( dataRep.tos && dataRep.tos.length > 0 ){
                    dataRep.tos.map(async (e) =>{
                        if(e.id != this.collegue.haraId){
                            if(e.type == 1){
                                const result = await HaravanHr.getDepartmentsById(e.id)
                                if(result && result.data && !result.error){
                                    data = {...result.data, fullName: result.data.name, type: 1, typeMail:1, icon:<SvgCP.iconTree/>, iconCheck:<SvgCP.iconTreeClick/>}
                                    listTo.push(data)
                                }
                            }
                            else if(e.type == 2){
                                const result = await HaravanHr.getJobtitleById(e.id)
                                if(result && result.data && !result.error ){
                                    data = {...result.data, fullName: result.data.name, type: 2, typeMail:2, icon:<SvgCP.job/>, iconCheck:<SvgCP.jobClick/>}
                                    listTo.push(data)
                                }
                            }
                            // else if(e.type == 4){
                            //     const result = await HaravanHr.getColleague(e.id)
                            //     let infoMe = (result && result.data && result.data.haraId == this.collegue.haraId); 
                            //     if(result && result.data && !result.error && (result.data.haraId != this.collegue.haraId)){
                            //         data = {...result.data, fullName:(infoMe==true )? 'Tôi': result.fullName , type: 4, typeMail:4, id: result.data.haraId, icon:<SvgCP.me/>, iconCheck:<SvgCP.meClick/>}
                            //         listTo.push(result.data)
                            //     }
                            // }
                            else if(e.type == 5){
                                data = {...this.userInfo, subject: 'Tất cả nhân viên', id: 'all', type: 5, fullName:this.userInfo.orgname, typeMail:5, icon:<SvgCP.homeOrg/>, iconCheck:<SvgCP.homeOrg/>}
                                listTo.push(data)
                            }
                            this.setState({listTo})
                        }
                    })
                }
                
                if( dataRep.cCs && dataRep.cCs.length > 0 ){
                    dataRep.cCs.map(async (e) =>{
                        if(e.type == 1){
                            const result = await HaravanHr.getDepartmentsById(e.id)
                            if(result && result.data && !result.error){
                                data = {...result.data, fullName: result.data.name, type: 1, typeMail:1, icon:<SvgCP.iconTree/>, iconCheck:<SvgCP.iconTreeClick/>}
                                listCC.push(data)
                            }
                        }
                        else if(e.type == 2){
                            const result = await HaravanHr.getJobtitleById(e.id)
                            if(result && result.data && !result.error ){
                                data = {...result.data, fullName: result.data.name, type: 2, typeMail:2, icon:<SvgCP.job/>, iconCheck:<SvgCP.jobClick/>}
                                listCC.push(data)
                            }
                        }
                        else if(e.type == 4){
                            const result = await HaravanHr.getColleague(e.id)
                            if(result && result.data && !result.error && (result.data.haraId != this.collegue.haraId)){
                                data = {...result.data, type: 4, typeMail:4, id: result.data.haraId, icon:<SvgCP.me/>, iconCheck:<SvgCP.meClick/>}
                                listCC.push(result.data)
                            }
                        }
                        else if(e.type == 5){
                            data = {...this.userInfo, subject: 'Tất cả nhân viên', id: 'all', type: 5, fullName:this.userInfo.orgname, typeMail:5, icon:<SvgCP.homeOrg/>, iconCheck:<SvgCP.homeOrg/>}
                            listCC.push(data)
                        }
                        this.setState({listCC, showCC: true})
                    })
                }
                this.setState({ listTo, listCC, title:dataRep.subject, content:(dataRep.isDraft == true ? contentDrat : contentRep), disable:false  })
            }
        }
    }

    checkListTo(){
        const { listTo } = this.state;
        let newList = []
        listTo.map(item => {
            let newItem =  {type:item.typeMail}
            switch (item.typeMail) {
                case 1:
                    newItem =  {...newItem, content:item.fullName, id:item.id, name:item.name}
                    newList.push(newItem)
                    break;
                case 2:
                    newItem = {...newItem, content:item.fullName, id:item.id, name:item.name}
                    newList.push(newItem)
                    break;
                case 4:
                    newItem = {...newItem, content:item.subject, id:item.haraId, name:item.fullName}
                    newList.push(newItem)
                    break;
                case 5:
                    newItem={...newItem, content:item.subject, id:this.userInfo.orgid, name:item.fullName}
                    newList.push(newItem)
                    break;
                default: null
                    break;
            }
            
        })
        return newList
        //return list
    }

    checkListCC(){
        const { listCC } = this.state;
        let newList = []       
        listCC.map(item => {
            //console.log('item', item);
            let newItem =  {type:item.typeMail}
            switch (item.typeMail) {
                case 1:
                    newItem =  {...newItem, content:item.fullName, id:item.id, name:item.name}
                    newList.push(newItem)
                    break;
                case 2:
                    newItem = {...newItem, content:item.fullName, id:item.id, name:item.name}
                    newList.push(newItem)
                    break;
                case 4:
                    newItem = {...newItem, content:item.subject, id:item.haraId, name:item.fullName}
                    newList.push(newItem)
                    break;
                case 5:
                    newItem={...newItem, content:item.subject, id:this.userInfo.orgid, name:item.fullName}
                    newList.push(newItem)
                    break;
                default: null
                    break;
            }
            
        })
        return newList
    }

    checkButton(){
        const { title, content } = this.state;
        const listSentTo = this.checkListTo();
        if( (listSentTo.length == 0) || (title === '') || (content === '')){
            this.setState({disable:true})
        }
        else{
            this.setState({disable:false})
        }
    }

    async sentMail(draft){
        const { title, content, listAttach, file } = this.state;
        let listSentTo = await this.checkListTo();
        let listCC = await this.checkListCC();
        let newList = []
        listAttach.map(item =>{
            item={
                name:item.name,
                url:item.file.url
            }
            newList.push(item)
        })
        if( (listSentTo.length == 0) || (title === '') || (content === '')){
            this.setState({disable:true})
        }
        else {
            this.isSent = true
            if((listAttach.length == file.length)){
                const body = {
                    message:[
                        {
                            attachments: newList ? newList : [],
                            from:{id: this.collegue.haraId, type:4},
                            content:content,
                            tos: listSentTo,
                            subject: title,
                            cCs:listCC,
                            isDraft: draft
                        }
                    ],
                    userInfo: this.collegue
                }
                if(this.state.titleHeader == 'write'){
                    const data = { 
                        message:body.message, 
                        mail: { subject: title },
                        userInfo: this.collegue,
                    }
                    const  result = await HaravanIc.sentMailDraft(data, draft)
                    if(result && result.data && !result.error){
                        if(draft == false){
                            Toast.show(`Gửi thư thành công`)
                            this.saveHistory(draft)
                        }
                        else{
                            Toast.show(`Đã lưu thư nháp`)
                            this.saveHistory(draft)
                        }
                    }
                    else{
                        if(result.message && (result.message.length> 0)){
                            Toast.show(`${result.message}`)
                        }
                        else{
                            Toast.show('Lỗi')
                        }
                    }
                }
                else{
                    if(draft == false){
                        const data =  {
                                message: {
                                    attachments: newList ? newList : [],
                                    from:{ id: this.collegue.haraId, type:4 },
                                    content:content,
                                    tos: listSentTo,
                                    subject: title,
                                    cCs:listCC,
                                    isDraft: draft,
                                    mailId: this.dataRep.mailId
                                },
                                userInfo: this.collegue
                            }
                            const result = await this.CallAPI(data, draft) 
                            if(result && result.data && !result.error ){
                                Toast.show(`Gửi thư thành công`)
                                this.props.actions.mail.deletedDraft({item:this.item, status:0})
                                this.saveHistory(draft)
                            }
                            else{
                                if(result.message && (result.message.length> 0)){
                                    Toast.show(`${result.message}`)
                                }
                                else{
                                    Toast.show('Lỗi')
                                }
                            }
                        }
                        else{
                            const data =  {
                                message: {
                                    attachments: newList ? newList : [],
                                    from:{ id: this.collegue.haraId, type:4 },
                                    content:content,
                                    tos: listSentTo,
                                    subject: title,
                                    cCs:listCC,
                                    isDraft: draft,
                                    mailId: this.dataRep.mailId
                                },
                                userInfo: this.collegue
                            }
                            const result = await this.CallAPI(data, draft) 
                            if(result && result.data && !result.error ){
                                Toast.show(`Đã lưu thư nháp`)
                                this.saveHistory(draft)
                            }
                            else{
                                if(result.message && (result.message.length> 0)){
                                    Toast.show(`${result.message}`)
                                }
                                else{
                                    Toast.show('Lỗi')
                                }
                            }
                    }
                   
                }
                this.setState({isLoad:false})
            }
            else{
                this.setState({isLoad:true}, () =>{
                    this.checkUrl()
                })
            }
        }
    }

    async CallAPI(data, draft){
        let result = null
        if((this.dataRep.isDraft && (this.dataRep.isDraft == true)) ){
            const body =  {
                message: {
                    ...data.message,
                    id: this.dataRep.id,
                },
                userInfo: data.userInfo
            }
            result =  await HaravanIc.PutreplyMailDraft(body, draft)
        }
        else{
            result =  await HaravanIc.PostreplyMailDraft(data, draft)
        }
        return result;
    }

    async checkUrl(){
        const url = await this.getUrl()
        const {file} = this.state;
        if(url.length == file.length){
            this.setState({listAttach:url}, () =>{
                this.sentMail(false)
            })
        }
    }

    saveHistory(draft) {
        const { listTo, listCC} = this.state
        let listHistory = [...listTo, ...listCC];
        listHistory = listHistory.filter(e => e.id !== 'all' && e.id !== this.collegue.haraId);
        if (listHistory && listHistory.length > 0) {
            listHistory = listHistory.slice(0, 10);
            AsyncStorage.setItem(keyStore.historyFromAndCC, JSON.stringify(listHistory));
        }
        this.props.actions.mail.loadDetail(true)
        if(draft == false){
            this.props.navigation.pop();
        }
    }

    getHTML(data) {
        this.setState({content: data}, () =>{
            this.checkButton()
        })
        // let scrollResponder = this.scrollMail.getScrollResponder();
        // scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
        //     findNodeHandle(this.textMail),
        //     110,
        //     true
        // );
    }

    listImage(image){
        if(image){
            this.setState({image:image})
        }
    }

    async getUrl(){
        const { file } = this.state;
        let listUrl = []
        let input = new FormData();
        for (let i = 0; i < file.length; i++) {
            input.append("files",{
                uri: file[i].uri,
                type: `image/${file[i].fileName.substr(file[i].fileName.lastIndexOf('.') + 1)}`,
                name: file[i].fileName,
            });
        }
        const response = await HaravanIc.postImage(input)
        if(response){
            response.map(e =>{
                listUrl.push(e)
            })
        }
        if(listUrl.length == file.length){
            this.setState({ listAttach: listUrl})
        }
        return listUrl
    }

    async remove(uri) {
        const { file } = this.state;
        const index = file.findIndex(e => e.uri === uri);
        file.splice(index, 1);
        await this.setState({ file });
    }

    cancel(){
        this.isSent = true
        const {titleHeader} = this.state;
        if(titleHeader === 'Edit'){
            this.props.actions.mail.removeDraft(true)
            this.props.navigation.pop()
        }
        else{
            this.props.navigation.pop()
        }
    }

    componentWillUnmount(){
        const { isSent } = this;
        if(isSent == false ){
           this.sentMail(true)
        }
    }

}
const mapStateToProps = state => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(MailWrite);