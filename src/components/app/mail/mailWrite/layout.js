import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity, TextInput, KeyboardAvoidingView, Image } from 'react-native';
import { HeaderWapper, settingApp, Editor, Loading } from '../../../../public';
import { Ionicons, Entypo, Feather, AntDesign } from '@expo/vector-icons';
import ContentMail from './components/contenMail';
import List from './components/list';
import Attachment from './components/attachment';
import File from './components/file';
import OptionMail from './components/optionMail';
import ModalLoading from './components/modalLoading';
import MoreOption from './components/moreOption';

class Layout extends Component {

    renderHeader() {
        return (
            <HeaderWapper backgroundColor='#ffffff' style={{ justifyContent: 'space-between' }} >
                <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.pop();
                    }}
                    style={{ backgroundColor: 'transparent', width: 55, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                    <Ionicons name='ios-arrow-back' size={23} color='#9CA7B2' style={{ marginLeft: -15 }} />
                </TouchableOpacity>

                { ((this.state.titleHeader != 'write') && (this.state.titleHeader != 'Edit')) ?
                    <TouchableOpacity
                        onPress ={() => this.setState({isShow: !this.state.isShow})}
                        style={{flexDirection:'row', justifyContent:'center', alignItems:'center'}}
                    >
                        <Text
                            style={{ fontSize:16, fontWeight:'bold', color:'#0279C7', marginRight:5}}
                        >{this.state.titleHeader}</Text>
                        <AntDesign name='caretdown' color='#0279C7' size={15}/>
                    </TouchableOpacity>
                    : null
                }

                <View style={{ flexDirection: 'row' }}> 
                    <TouchableOpacity
                        onPress={() => this.setState({ moreOption: true })}
                        style={{ backgroundColor: 'transparent', width: 55, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                        <Feather name='more-horizontal' size={23} color='#BFC7CF' />
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.setState({ modalVisibleAttachment: true })}
                        style={{ backgroundColor: 'transparent', width: 55, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                        <Feather name='paperclip' size={23} color='#9CA7B2' />
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress = {() => {
                            !this.state.disable ?
                            this.setState({isLoad:true},()=>{
                                //this.checkListTo();
                                this.sentMail(false)
                                }) : null
                            }}
                        style={{ backgroundColor: 'transparent', width: 55, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                        <Entypo
                            name='paper-plane'
                            size={23} color={settingApp.color}
                            style={{ opacity: this.state.disable ? 0.5 : 1 }}
                        />
                    </TouchableOpacity>
                </View>
            </HeaderWapper>
        )
    }

    renderCC(){
        return(
            <TouchableOpacity
            onPress={() => this.setState({showCC:true})}
             style={{
                 position:'absolute', 
                 width:50, height:44, 
                 backgroundColor:'transparent',
                 flexDirection:'row',
                 right:10,
                 justifyContent:'center',
                 alignItems:'center'
                 }}>
                <Text style={{fontSize:14, fontWeight:'bold', color:settingApp.color, flexDirection:'row'}}>
                    CC
                </Text>
            </TouchableOpacity>
        )
    }

    renderAddress() {
        const { listTo, listCC, showCC } = this.state;
        return (
            <View>
                <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => this.props.navigation.navigate('SearchUserMail', { 
                        dataUser: this.collegue, 
                        dataAuth: this.userInfo, 
                        list: this.state.listTo,
                        title: 'GỬI ĐẾN',
                        getList:(listTo) => this.setState({listTo:listTo}, () =>{
                            this.checkListTo()
                        })
                        })}
                    style={{
                        paddingTop: 10,
                        paddingBottom: 5,
                        paddingLeft: 10,
                        paddingRight: 10,
                        minHeight: 44,
                        backgroundColor: '#ffffff',
                        borderBottomWidth: 1,
                        borderBottomColor: settingApp.colorSperator,
                    }}>
                    <Text style={{ color: '#9CA7B2', fontSize: 14 }}>
                        GỬI ĐẾN:
                    </Text>
                    <List list={listTo} /> 
                    { !showCC ? this.renderCC() : null}
                </TouchableOpacity>
                
                {showCC ? <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => this.props.navigation.navigate('SearchUserMail', { 
                        dataUser: this.collegue, 
                        dataAuth: this.userInfo, 
                        list: this.state.listCC,
                        title: 'CC',
                        getList:(listCC) => this.setState({listCC:listCC},() =>{
                            this.checkListCC()
                        })
                        })}
                    style={{
                        paddingTop: 10,
                        paddingBottom: 5,
                        paddingLeft: 10,
                        paddingRight: 10,
                        minHeight: 44,
                        backgroundColor: '#ffffff',
                        borderBottomWidth: 1,
                        borderBottomColor: settingApp.colorSperator
                    }}>
                    <Text style={{ color: '#9CA7B2', fontSize: 14 }}>
                        CC:
                    </Text>
                    <List list={listCC} />
                </TouchableOpacity> : <View/>}
            </View>
        )
    }

    contentTitleMail() {
        return (
            <View style={{
                backgroundColor: '#ffffff'
            }}>
                <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => this.textTitle.focus()}
                    style={{
                        padding: 10,
                        paddingBottom: 0,
                        backgroundColor: '#ffffff',
                        borderBottomWidth: 1,
                        borderBottomColor: settingApp.colorSperator

                    }}>
                    <Text style={{ color: '#9CA7B2', fontSize: 14 }}>
                        TIÊU ĐỀ:
                    </Text>
                    <TextInput
                        ref={refs => this.textTitle = refs}
                        value={this.state.title}
                        onChangeText={title => this.setState({ title }, () =>{
                            this.checkButton()
                        })}
                        style={{ minHeight: 30, paddingBottom: 5 }}
                        multiline={true}
                        selectionColor={settingApp.color}
                        underlineColorAndroid='transparent'
                    />
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        const { image,isShow, isLoad,moreOption  } = this.state;
        return (
            <KeyboardAvoidingView style={{ flex: 1, backgroundColor: '#ffffff' }} behavior='padding'>
            {
                isLoad && 
                <ModalLoading
                    visible={isLoad}
                    close={() =>{this.setState({isLoad:false})}}                
                />
            }
                {this.renderHeader()}
                {
                    moreOption &&
                    <MoreOption
                    visible = {this.state.moreOption}
                    close ={() => this.setState({moreOption:false})}
                    cancel = {() => this.cancel()}
                />
                }

                <OptionMail 
                    visible={this.state.isShow}
                    close={() => this.setState({isShow:false})}
                    setTitle={(title) => this.setState({titleHeader:title, listTo:[], listCC:[], showCC:false}, () =>{
                        this.loadReply()
                    })}
                />
                <ScrollView
                    style={{flex:1}}
                    ref={refs => this.scrollMail = refs}
                    contentContainerStyle={{ paddingBottom: 10 }}
                >   
                    {this.renderAddress()}
                    {this.contentTitleMail()}
                    <View 
                        style={{ backgroundColor:'#FFFFFF', minHeight:(settingApp.height * 0.4)}}
                    >
                        <ContentMail
                            ref={refs => this.textMail = refs}
                            getHTML={(data) => this.getHTML(data)}
                            type ={ this.state.titleHeader }
                            description={this.dataRep.content}
                            collegue={this.collegue}
                            listTo={this.state.listTo}
                            title = {this.dataRep.subject}
                            draft = {this.dataRep.isDraft}
                        />
                    </View>

                    <View style={{width:settingApp.width, height:100}}>
                        <File
                            data={this.state.file}
                            remove={async (uri) => await this.remove(uri)}
                        />
                    </View>

                    <View style={{width:settingApp.width, height:120}}>
                        <Attachment 
                            isVisible={this.state.modalVisibleAttachment}
                            close={() => this.setState({ modalVisibleAttachment: false })}
                            selectFile={async (file) => await this.setState({ file }, () =>{
                                this.getUrl()
                            })}
                        />
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        )
    }
}

export default Layout;