import Layout from './layout';
import { HaravanIc, HaravanHr } from '../../../../Haravan';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../state/action';
import { Utils, Toast } from '../../../../public';

class MailDetail extends Layout {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            listDetail: [],
            listLabels: [],
            detailLables: [],
            showLables: false,
            listReverse:[],
            dataRep: [],
            modalOption: false,
            index: 0,
            listRemove: [],
            listTrue:[],
            listFalse:[],
            // isStarred: props.navigation.state.params.item.mailInfo.isStarred,
            isReload:true,
            dataMail: null,
            isStarred:false
        }
        this.listCreateBy = [];
        this.item = props.navigation.state.params.item;
        this.idStart = [];
        this.remove = false;

        this.listReverse = []

        this.colleague = props.app.colleague;
        _thisMessage = this
    }

    componentWillReceiveProps(nextProps){
        //console.log('nextProps', nextProps);
        if(nextProps && ((nextProps.mail.loadDetail == true ))){
            this.setState({isLoading: true}, () =>{
                this.loadData()
            })
        }
        if(nextProps && ((nextProps.mail.removeDraft == true))){
            const index = (this.state.listDetail.length - 1);
            this.setState({isLoading:false},() =>{
                this.props.actions.mail.removeDraft(null)
                this.loadData()
            })
            this.removeMassage(index)
        }
        if(nextProps && nextProps.app && nextProps.app.reloadNotify && (nextProps.app.reloadNotify==true) ){
            this.item = nextProps.navigation.state.params.item;
            this.setState({isLoading: true}, () =>{
                this.loadData() 
                this.props.actions.reloadNotify(null)
            })
           
        }
    }

    checkReload(){
        const { isReload } = this.state;
        if(isReload == true){
            this.setState({isLoading: true}, () =>{
                this.loadData() 
            })
        }
    }

    componentDidMount() {
        this.loadData()
        this.loadLables()
    }

    editMail(index){
        this.props.navigation.navigate('MailWrite', {
            type:'Edit',
            data: this.state.dataRep[index],
            item: this.state.dataMail
        })
    }

    async removeMail() {
        if(this.item.notify){
            let newlistID = [this.item.mailInfo.id]
            let params = newlistID;
           let rest = await HaravanIc.bulkMail(params)    
           if(rest && !rest.error){
               this.props.navigation.pop()
               Toast.show('Xóa thư thành công')
           }
        }
        else{
            const body = []
            body.push(this.state.dataMail.id)
        // const data = this.item;
            this.props.actions.mail.removeMail(this.state.dataMail)
            this.props.navigation.pop();
        }
    }

    showLables() {
        this.props.navigation.navigate('AddLabels', {
            initial:this.state.detailLables ,
            data:this.state.listLabels ,
            updateList: (list => this.bulkLabel(list)),
        })
    }

    async setStart(status) {
        const { listDetail } = this.state;
        const body = this.idStart;
        const start = await HaravanIc.statusStarMail(status, body);
        if (!start.error) {
            Toast.show(`${status ? 'Đánh dấu' : 'Bỏ đánh dấu'} sao thành công`)
            this.setState({ isStarred: status }, () => {
                if(status == true){
                    this.startMail(true, (listDetail.length -1))
                }
                else {
                    let newList =[]
                    listDetail.map(item =>{
                        item = {...item, isStarred:false}
                        newList.push(item)
                    })
                    this.setState({listDetail:newList})
                }
                setTimeout(() => (Toast.close()), 4000)
            })
        } else {
            Toast.show('Lỗi')
        }
    }
    checkStarred(status) {
        this.props.actions.mail.checkStarred({ id: this.idStart, status: status })
    }

    checkReply(data){
        if(data.from.id === this.colleague.haraId){
            return true
        }
        else{
            return false
        }
    }

    removeMassage(draf){
        const { index, listDetail, listRemove } = this.state;
        let idnexDelete = draf ? draf : index;
        const item = listDetail[idnexDelete];
        listRemove.push(item)
        this.listReverse.push(item)
        this.setState({listRemove})
        if(this.state.listRemove.length == listDetail.length){
            this.removeMail()
            this.deleted(false)
        } else{
            if(draf){
                const msg = `Bỏ nháp thành công`;
                Toast.show(msg);
                this.remove = true;
                this.deleted(true,draf)
            }
            else{
                const msg = `Xóa thư thành công`;
                Toast.show(msg, _thisMessage.undoMail);
                setTimeout(() => (this.remove = true, this.deleted(false)), 4000);
            }
        }
    }

    undoMail(){
        _thisMessage.reverse()
    }

    reverse(){
        const { listRemove, listReverse} = this.state;
        this.listReverse.map(item =>{
            listReverse.push(item)
            const index = listRemove.findIndex(e => e.id === item.id)
            if(index > -1){
                listRemove.splice(index, 1)
            }
            this.setState({listReverse, listRemove}, () => this.listReverse = [])
        })
    }

    async deleted(status,draf){
        const { listReverse, listDetail, index } = this.state;
        if(listReverse.length > 0){
            this.remove = false;
        }
        else{
            if(this.remove == true){
                const number = draf ? draf : index
                const result = await HaravanIc.deletaMessage(status, listDetail[number].id, listDetail[number].mailId)
                if(result && !result.error && (status == true) && draf){
                    this.props.actions.mail.deletedDraft({item:this.state.dataMail, status:-1})
                }
                this.setState({ listReverse:[] },  () => this.listReverse = [])
            }
        }
    }

    async startMail(status, index){
        const { listDetail, isStarred, listFalse, listTrue } = this.state;
        const result  = await HaravanIc.putStartMessage(listDetail[index].id, status)
        if(result && !result.error){
            Toast.show(`${status ? 'Đánh dấu' : 'Bỏ đánh dấu'} sao thành công`)
            listDetail.map(item =>{
                if((item.id === listDetail[index].id)){
                    listDetail[index] = { ...listDetail[index], isStarred:status}
                }
            })
            this.setState({listDetail}, () =>{
                if(status == true){
                    this.state.listDetail.map(item =>{
                        if(item.isStarred == true){
                            const indexTrue = listTrue.findIndex(e => e.id ===item.id)
                            if(indexTrue < 0){
                                listTrue.push(item)  
                            }
                            const indexFales= listFalse.findIndex(e => e.id === item.id)
                            listFalse.splice(listFalse[indexFales], 1)
                        }
                        else{
                            listFalse.push(item)
                        }
                    })
                }
                else{
                    const indexList = listTrue.findIndex(m => (m.id === listDetail[index].id) && (listDetail[index].isStarred == false))
                    if(indexList > -1){
                        listFalse.push(listTrue[indexList])
                        listTrue.splice(listTrue[indexList], 1)
                    }
                }
                this.setState({ listFalse, listTrue}, () =>{
                    if((listTrue.length > 0)){
                        this.setState({isStarred:true},() =>{
                            this.checkStarred(true)
                        })
                    }
                    else if(listTrue.length == 0 ){
                        this.setState({isStarred:false}, () =>{
                            this.checkStarred(false)
                        })
                    }
                })
            })
        }
        else{
            Toast.show('Có lỗi vui lòng thử lại sau')
        }
    }

    checkStartDefaul(listDetail){
        const { listFalse, listTrue } = this.state;
        if(listDetail && listDetail.length > 0 ){
            this.state.listDetail.map(item =>{
                if(item.isStarred == true){
                    const indexTrue = listTrue.findIndex(e => e.id ===item.id)
                    if(indexTrue < 0){
                        listTrue.push(item)  
                    }
                    const indexFales= listFalse.findIndex(e => e.id === item.id)
                    listFalse.splice(listFalse[indexFales], 1)
                }
                else{
                    listFalse.push(item)
                }
            })
            this.setState({listFalse, listTrue})
        }
    }
    
    async loadLables() {
        const { listLabels } = this.state;
        const result = await HaravanIc.getLabel(1);
        if (result && result.data && !result.error) {
            this.setState({ listLabels: result.data.data })
        }
    }

    async loadData() {
        const { item, listCreateBy } = this;
        const { listFalse, listTrue} = this.state;
        let newList = [];
        let listData = [];
        const result = await HaravanIc.getMailDetail(item.mailInfo.id)
        if (result && result.data && !result.error) {
            listData = result.data.message;
            this.idStart.push(result.data.mail.id)
            for (let i = 0; i < listData.length; i++) {
                let item = {}
                if (listCreateBy.length == 0) {
                    const resultCreate = await HaravanHr.getColleague(listData[i].from.id);
                    if (resultCreate && resultCreate.data && !resultCreate.error) {
                        item = {
                            ...listData[i],
                            createdUserName: resultCreate.data.fullName,
                            createdUserAvatar: resultCreate.data.photo,
                        }
                        newList.push(item)
                        listCreateBy.push({ key: listData[i].from.id, data: resultCreate.data })
                    }
                }
                else {
                    const index = listCreateBy.findIndex(m => m.key === listData[i].from.id)
                    if (index < 0) {
                        const resultCreate = await HaravanHr.getColleague(listData[i].from.id);
                        if (resultCreate && resultCreate.data && !resultCreate.error) {
                            item = {
                                ...listData[i],
                                createdUserName: resultCreate.data.fullName,
                                createdUserAvatar: resultCreate.data.photo,
                            }
                            newList.push(item)
                            listCreateBy.push({ key: listData[i].from.id, data: resultCreate.data })
                        }
                    }
                    else {
                        item = {
                            ...listData[i],
                            createdUserName: listCreateBy[index].data.fullName,
                            createdUserAvatar: listCreateBy[index].data.photo,
                        }
                        newList.push(item)
                    }
                }
            }
            const checkPercentRead = this.checkPercentRead(result.data)
            this.props.actions.mail.readMail({item, checkPercentRead})
            this.props.actions.mail.loadDetail(null)
            this.setState({ listDetail: newList, dataRep:result.data.message , isReload:false, dataMail:result.data}, () => {
                this.checkStartDefaul(this.state.listDetail)
                this.setState({ isLoading: false },() =>{
                    this.checkLables(result)
                })
            })
        }
        else {
            this.setState({ isLoading: false, isReload:false })
        }
    }

    checkPercentRead(data){
        const index = (data.message.length - 1)
        let newList = []
        data.message[index].tos.map(item =>{
            if(item.percentRead != 100 ){
                newList.push(item)
            }
        })
        return newList;
    }

    checkLables(result) {
        const { detailLables } = this.state;
        const data = result.data
        if (data.mail.internalLabel && data.mail.internalLabel.length > 0) {
            data.mail.internalLabel.map(e => {
                if(detailLables.length == 0){
                    detailLables.push(e)
                }
                else{
                    const index = detailLables.findIndex(m => m.id === e.id)
                    if(index < 0 ){
                        detailLables.push(e)
                    }
                }
            })
            this.setState({ detailLables })
        }
    }

    async bulkLabel(list) {
        const { itemCheck, labelsAdd, labelsDelete } = list;
        const labelAdd = labelsAdd.map(e => e.id);
        const labelDelete = labelsDelete.map(e => e.id)
        this.setState({ detailLables: itemCheck });
        let mailids = this.idStart
        const params = {
            labelAdd,
            labelDelete,
            mailids
        };
        this.props.actions.mail.checkLable({ labelsAdd: labelsAdd, labelsDelete: labelsDelete, id: this.idStart[0] })
        await HaravanIc.bulkLabel(params);
    }

    componentWillUnmount(){
        this.props.actions.mail.removeMail(null)
    }
}
const mapStateToProps = (state) => ({ ...state })
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(MailDetail);