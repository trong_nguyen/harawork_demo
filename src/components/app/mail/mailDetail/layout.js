import React, { Component } from 'react';
import { View, Text, TouchableOpacity, FlatList, ActivityIndicator, ScrollView } from 'react-native';
import { settingApp } from '../../../../public';
import Header from './components/header';
import TopMailDetail from './components/topMailDetail';
import Item from './components/item';
import { Entypo, MaterialIcons } from '@expo/vector-icons'
import ModalOption from './components/modalOption'

class Layout extends Component {

    renderConten() {
        const { listDetail, isLoading } = this.state;
        return (
            <FlatList
                data={listDetail}
                extraData={this.state}
                keyExtractor={(item, index) => (index + '')}
                renderItem={({ item, index }) => this.renderItem(item, index)}
                ListFooterComponent={!isLoading && (listDetail[listDetail.length -1].isDraft == true ) ? null :
                <View
                    style={{width:settingApp.width, height:80, backgroundColor:'#FFFFFF', justifyContent:'space-between', alignItems:'center', borderTopWidth:0.5, borderTopColor:'#DAE3EA'}}
                >
                    {(!isLoading && this.state.dataMail) ? this.renderReplay() : <View/>}
                </View>
                }
            />
        )
    }

    renderItem(item, index) {
        const { isLoading } = this.state;
        return (
            <Item
                item={item}
                index={index}
                listDetail={this.state.listDetail}
                isLoading={isLoading}
                data={this.state.dataMail}
                colleague = {this.colleague}
                listRemove = {this.state.listRemove}
                listReverse={this.state.listReverse}
                modalOption={(index) => this.setState({ modalOption:!this.state.modalOption, index:index, listReverse:[] })}
                editMail={(index) => this.editMail(index)}
            />
        )
    }

    loading() {
        return (
            <View style={{
                backgroundColor: 'rgba(255,255,255,0)',
                justifyContent: 'center',
                alignItems: 'center',
                position: 'absolute',
                width: settingApp.width,
                height: (settingApp.height - 50)
            }}>
                <ActivityIndicator size='large' color={settingApp.color} />
            </View>
        )
    }

    renderReplay(){
        const index = this.state.dataRep.length - 1;
        const checkReply = this.checkReply(this.state.dataRep[0])
        return(
            <View style={{flex:1, flexDirection:'row', justifyContent:'space-between', width:settingApp.width, alignItems:'center'}}>
                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('MailWrite', {
                        type:'Trả lời',
                        data:this.state.dataRep[index],
                        checkReply: checkReply,
                        item:this.state.dataMail
                        })}
                    style={{width:(settingApp.width * 0.33), alignItems:'center', }}
                >
                    <Entypo name='reply' size={25} color='#9CA7B2'/>
                    <Text style={{fontSize:16, color:'#9CA7B2'}}>Trả lời</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('MailWrite', {
                        type:'Trả lời tất cả',
                        data: this.state.dataRep[index],
                        checkReply:checkReply,
                        item:this.state.dataMail
                        })}
                    style={{width:(settingApp.width * 0.33), alignItems:'center', }}
                >
                    <Entypo name='reply-all' size={25} color='#9CA7B2'/>
                    <Text style={{fontSize:16, color:'#9CA7B2'}}>Trả lời tất cả</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('MailWrite', {
                        type:'Chuyển tiếp',
                        data:this.state.dataRep[index],
                        checkReply: checkReply,
                        item:this.state.dataMail
                        })}
                    style={{width:(settingApp.width * 0.33), alignItems:'center', }}
                >
                    <MaterialIcons name='forward' size={25} color='#9CA7B2'/>
                    <Text style={{fontSize:16, color:'#9CA7B2'}}>Chuyển tiếp</Text>
                </TouchableOpacity>
            </View> 
        )
    }


    render() {
        const { isLoading, showLables, listDetail, index, dataMail } = this.state;
        let content = <View />
        isLoading ? content = this.loading() : content = this.renderConten()
        return (
            <View
                style={{flex:1}}
            >
                        <Header
                            isStarred={this.state.isStarred}
                            navigation={this.props.navigation}
                            statusStart={(status) => this.setStart(status)}
                            checkStarred={(status) => this.checkStarred(status)}
                            showLables={() => this.showLables()}
                            removeMail={() => this.removeMail()}
                        />
                <View style={{ flex: 1 }}>
                    {dataMail && <TopMailDetail
                        item={this.state.dataMail}
                        detailLables={this.state.detailLables}
                    />}
                    {content}
                </View>
                
                {!isLoading && 
                    <ModalOption 
                        visible = {this.state.modalOption}
                        close={() => this.setState({modalOption: false})}
                        index ={ this.state.index }
                        dataRep = { this.state.dataRep[this.state.index] }
                        //checkStarred={(status) => this.checkStarred(status)}
                        //statusStart={(status) => this.setStart(status)}
                        statusStart={(status, index) => this.startMail(status, index)}
                        navigation={this.props.navigation.navigate}
                        removeMassage={() => this.removeMassage()}
                        isStarred={ listDetail[this.state.index].isStarred}
                    />
                }
                
            </View>
        )
    }
}
export default Layout;