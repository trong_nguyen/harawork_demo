import React, { Component } from 'react';
import { Animated, View, Text, Modal, TouchableOpacity, Platform } from 'react-native';
import { settingApp } from '../../../../../public';

class OptionMail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            bottom: new Animated.Value(-120),
            isStarred: this.props.isStarred,
        }

        this.list = [
            { name: 'Trả lời', },
            { name: 'Trả lời tất cả'},
            { name: 'Chuyển tiếp'},
            { name: this.state.isStarred ? 'Bỏ đánh dấu sao' : 'Đánh dấu sao' },
            { name: 'Chi tiết người nhận'},
            { name: 'Xoá'},
        ];
    }

    componentWillReceiveProps(nextProp){
        if(nextProp && (nextProp.isStarred != this.props.isStarred)){
            this.setState({ isStarred: nextProp.isStarred })
            this.list = [
                { name: 'Trả lời', },
                { name: 'Trả lời tất cả'},
                { name: 'Chuyển tiếp'},
                { name: nextProp.isStarred ? 'Bỏ đánh dấu sao' : 'Đánh dấu sao' },
                { name: 'Chi tiết người nhận'},
                { name: 'Xoá'},
            ];
        }
    }

    startMail() {
        // this.props.setStatus(null, [])
        this.setState({ isStarred: !this.state.isStarred }, () => {
            this.props.statusStart(this.state.isStarred, this.props.index);
            //this.props.checkStarred(this.state.isStarred);
        });
    }

    show() {
        Animated.timing(
            this.state.bottom,
            {
                toValue: 0,
                duration: 150
            }
        ).start();
    }

    close() {
        Animated.timing(
            this.state.bottom,
            {
                toValue: 120,
                duration: 150
            }
        ).start(() => this.props.close());
    }

    reply(type){
        if(type === 'Trả lời'){
            this.props.navigation('MailWrite', {
                type:type,
                data:this.props.dataRep,
                // checkReply: checkReply
                })
        }
        else if(type === 'Trả lời tất cả'){
            this.props.navigation('MailWrite', {
                type:type,
                data:this.props.dataRep,
                // checkReply: checkReply
                })
        }
        else if(type === 'Chuyển tiếp'){
            this.props.navigation('MailWrite', {
                type:type,
                data:this.props.dataRep,
                // checkReply: checkReply
                })          
        }
        else if(type === 'Đánh dấu sao' || type === 'Bỏ đánh dấu sao' ){
            this.startMail()
        }
        else if(type === 'Chi tiết người nhận'){
            //this.props.removeMassage()
            this.props.navigation('DetailSeenMail',{data:this.props.dataRep})
        }
        else if(type === 'Xoá'){
            this.props.removeMassage()
        }
    }

    render() {
        const { width } = settingApp;
        const indexList = this.list.length - 1;
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.props.visible}
                onShow={() => this.show()}
                onRequestClose={() => this.close()}>
                <View 
                onPress={() => this.close()}
                style={styles.container}>
                    <TouchableOpacity
                        activeOpacity={1}
                        onPress={() => this.close()}
                        style={{ flex: 1 }}
                    />
                    <Animated.View
                        style={{
                            position: 'absolute',
                            width,
                            backgroundColor: '#ffffff',
                            bottom:0,
                            ...settingApp.shadow
                        }}>{
                            this.list.map(  (item, index) =>{
                                return(
                                    <TouchableOpacity
                                        key={index}
                                        onPress={() => {
                                            this.reply(item.name);
                                            this.close();
                                        }}
                                        style={styles.textContainer}>
                                        <Text style={{fontSize: 16, color:(indexList == index)  ? '#D9534F' : '#0279C7'}}>{item.name}</Text>
                                    </TouchableOpacity>
                                )
                            })
                        }
                    </Animated.View>
                </View>
            </Modal>
        )
    }
}

const styles = {
    textContainer: {
        flex: 1, justifyContent: 'center', paddingLeft: 15,  borderTopColor:'#DAE3EA', borderTopWidth: 1 , height:50
    },
    text: { fontSize: 16, color:'#0279C7' },
    container: {
            flex: 1,
            ...Platform.select({
                android: {
                    marginTop: settingApp.statusBarHeight + 20,
                },
                ios: {
                    marginTop: settingApp.statusBarHeight + 44,
                },
            }),
            backgroundColor: 'rgba(0,0,0,0.5)',
            width: settingApp.width
        },
    menu:{
            flex: 1,
            ...Platform.select({
                android: {
                    maxHeight: (settingApp.height - (settingApp.statusBarHeight + 40)),
                },
                ios: {
                    maxHeight: (settingApp.height - (settingApp.statusBarHeight + 44)),
                },
            }),
        },
}

export default OptionMail;