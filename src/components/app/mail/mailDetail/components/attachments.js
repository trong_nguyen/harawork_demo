import React, { Component } from 'react';
import { View, Text, FlatList, WebView, Image, TouchableOpacity, Platform, ImageBackground,  } from 'react-native';
import { imgApp, Utils, settingApp, ButtonCustom , ReviewImage, DownloadFile, Toast} from '../../../../../public';
import OpenFile from './openFile';
import { connect } from 'react-redux';
import Config from '../../../../../Haravan/config';
import * as WebBrowser  from 'expo-web-browser';

class Attachments extends Component {
    constructor(props) {
        super(props);

        this.state = {
            attachments: props.attachments,
            dataSelected: null,
            modalVisible: false,
            url:null
        }
        this.userInfo = props.app.authHaravan.auth.access_token;
    }

    async downloadFileWeb(item){
        const url = `https://ic.${Config.apiHost}/call/im_api/attachment/${item.id}?isView=${false}&access_token=${this.userInfo}`;
        await WebBrowser.openBrowserAsync(url)
    } 

    openFile(item) {
        // DownloadFile(item.id, this.userInfo, false)
        Toast.show('Tính năng đang được xây dựng')
    }

    renderItem(obj) {
        const { item } = obj;
        
        let typeUrl = Utils.checkTypeUrl(item.name)
        var url = `https://ic.${Config.apiHost}/call/im_api/attachment/${item.id}?isview=true&access_token=${this.userInfo}`;
        if(Platform.OS === 'ios'){
            return(
                <TouchableOpacity
                onPress={() => this.setState({url:url, dataSelected:item},() =>{
                    this.setState({modalVisible:true})
                })}
                style={{
                        width: 130,
                        height: 90,
                        marginRight: 10,
                        backgroundColor: '#EDEFF7',
                        ...settingApp.shadow,
                        borderRadius: 3
                    }}
                >
                    <WebView
                        style={{flex:1}}
                        bounces={false}
                        scrollEnabled={false} 
                        source={{uri: url}}>
                    </WebView>
                </TouchableOpacity>
            )
        }
        else{
            let content = <View />
            if (typeUrl == 'image') {
                content = (
                    <ImageBackground
                        source={imgApp.noImage}
                        style={{ flex: 1, width: undefined, height: undefined }}
                        resizeMode='center'
                    >
                        <ReviewImage 
                            id={item.id}
                        />
                    </ImageBackground>
                );
            }  
            else{
                content = (
                    <View style={{ flex: 1, padding: 10, justifyContent: 'space-between' }}>
                        <Text style={{ color: '#474747', fontSize: 10 }}>{item.name}</Text>

                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image
                                source={imgApp[`${typeUrl}`]}
                                style={{ width: 20, height: 20, marginRight: 10 }}
                                resizeMode='contain'
                            />
                            <Text style={{ color: settingApp.colorText, fontSize: 10 }}>{typeUrl}</Text>
                        </View>
                    </View>
                );
            }
            return (
                <ButtonCustom
                    onPress={() => 
                    {(typeUrl == 'image') ? (
                        this.setState({url:url, dataSelected:item},() =>{
                        this.setState({modalVisible:true})
                    }) ) 
                    : this.downloadFileWeb(item)
                    } }
                    style={{
                        width: 130,
                        height: 90,
                        marginRight: 10,
                        backgroundColor: '#EDEFF7',
                        ...settingApp.shadow,
                        borderRadius: 3
                    }}>
                    {content}
                </ButtonCustom>
            )                
        }
    }

    render() {
        const { url } = this.state;
        if(this.state.attachments.length > 0){
            return (
                <View style={{ flex: 1, padding: 10,backgroundColor:'#FFFFFF' }}>
                    <Text style={{ color:'#9CA7B2', fontSize: 16, marginBottom: 20}}>
                        FILE ĐÍNH KÈM
                    </Text>
                    <FlatList
                        data={this.state.attachments}
                        extraData={this.state}
                        keyExtractor={(item, index) => '' + index}
                        renderItem={obj => this.renderItem(obj)}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                    />
                    {
                    url && <OpenFile
                        modalVisible={this.state.modalVisible}
                        dataSelected={this.state.dataSelected}
                        url ={this.state.url}
                        close={() => {
                            this.dataIndex = null;
                            this.setState({ modalVisible: false })
                        }}
                    />
                    }
                </View>
            )
        }
        else{
            return null
        }
        
    }
}

const mapStateToProps = (state) => ({ ...state })
export default connect(mapStateToProps)(Attachments);