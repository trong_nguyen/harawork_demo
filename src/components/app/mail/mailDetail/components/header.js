import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Platform, Image } from 'react-native';
import { HeaderIndex, HeaderScreen, settingApp, ButtonCustom, HeaderWapper, imgApp } from '../../../../../public'
import { Ionicons, AntDesign, FontAwesome, MaterialIcons } from '@expo/vector-icons'

class Header extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isStarred: this.props.isStarred,
            showLables:this.props.showLables,
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps && (nextProps.isStarred !== this.state.isStarred)){
            this.setState({isStarred: nextProps.isStarred})
        }
    }

    start() {
        // this.props.setStatus(null, [])
        this.setState({ isStarred: !this.state.isStarred }, () => {
            this.props.statusStart(this.state.isStarred);
            this.props.checkStarred(this.state.isStarred);
        });
    }

    render() {
        const { isStarred } = this.state
        return (
            <HeaderWapper
                backgroundColor= '#FFFFFF'
                style={{...settingApp.shadow, justifyContent: 'space-between'}}
            >
                <TouchableOpacity
                    onPress={() => this.props.navigation.pop()}
                    style={{ justifyContent: 'center', paddingLeft: 10, width: 50, }}
                >
                    <AntDesign name='left' size={20} color='#9CA7B2' />
                </TouchableOpacity>

                <View
                    style={{
                        width: (settingApp.width*0.35),
                        backgroundColor:'transparent',
                        flexDirection:'row',
                        marginRight:15
                    }}
                >
                     <TouchableOpacity
                        onPress={() =>this.props.removeMail()}
                        style={{width:50, justifyContent:'center'}}
                    >
                        <Ionicons name='md-trash' size={25} color='#BFC7CF' />
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.start()}
                        style={{width:50, justifyContent:'center'}}
                    >
                        <FontAwesome name='star'  size={25} color={isStarred ? '#F0AD4E' : '#BFC7CF'}/>
                    </TouchableOpacity>
                    
                   
                    <TouchableOpacity
                        onPress={() => this.props.showLables()}
                        style={{width:50, justifyContent:'center'}}
                    >
                        <MaterialIcons name='label' size={25} color= '#BFC7CF' />
                    </TouchableOpacity>
                </View>
            </HeaderWapper>
        )
    }
}
export default Header;