import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Platform, Image } from 'react-native';
import { HeaderIndex, HeaderScreen, settingApp, ButtonCustom, HeaderWapper, imgApp } from '../../../../../public'
import { Ionicons, AntDesign, FontAwesome, MaterialIcons } from '@expo/vector-icons'

class TopMailDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderLables(detailLables) {
        if (detailLables !== null || detailLables.length > 0) {
            return (
                detailLables.map((e, i) => {
                    return (
                        <View
                            key={i}
                            style={{
                                height: 20,
                                borderRadius: 20 >> 1,
                                backgroundColor: settingApp.colorLable,
                                marginRight: 3,
                                marginBottom:3,
                                justifyContent:'center',
                                alignItems:'center'
                            }}
                        >
                            <Text
                                style={{ paddingLeft: 5, paddingRight: 5, color: '#ffffff' }}
                            >
                                {e.label}
                            </Text>
                        </View>
                    );
                })
            )
        }
        else {
            return (null)
        }
    }

    render() {
        const { item, detailLables } = this.props;
        let listLables = [];
        if (detailLables == null) {
            listLables = []
        } else {
            listLables = detailLables
        }
        return (
            <View style={{ 
                width: settingApp.width, 
                backgroundColor: '#FFFFFF' ,
                borderTopColor:'#DAE3EA',
                borderTopWidth:1,
                padding: 10, 
                paddingBottom: 0,
                minHeight:60,
                borderBottomWidth:1,
                borderBottomColor:'#DAE3EA'
                }}>
                {item && item.mail && item.mail.subject &&
                <Text
                    style={{ fontSize: 18, color: settingApp.colorText, flexWrap: 'wrap', marginBottom:5
                    }}
                >{item.mail.subject} 
                </Text>
                }
                {(listLables && listLables.length > 0) ?
                        <View style={{flexDirection: 'row', flexWrap: 'wrap', alignItems: 'flex-start', marginTop: 5, marginBottom:10 }}>
                            {
                                this.renderLables(detailLables)
                            }
                        </View>
                        : null
                }
            </View>
        )
    }
}
export default TopMailDetail;