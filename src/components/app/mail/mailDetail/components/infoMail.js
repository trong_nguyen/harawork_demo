import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image, ActivityIndicator } from 'react-native';
import { HeaderIndex, HeaderScreen, settingApp, ButtonCustom, HeaderWapper, imgApp, Avatar, Utils, ParseText, Loading } from '../../../../../public';
import Collapsible from 'react-native-collapsible';
import { HaravanIc, HaravanHr } from '../../../../../Haravan'

class InfoMail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dataUser: [],
            titleButon: false,
            isCollap: false,
            dataCC: [],
            loadTo: true,
            loadCC: true
        }
    }

    async componentDidMount() {
        let { dataUser, dataCC } = this.state;
        let { tos, cCs } = this.props.item;
        dataUser = await this.getUserInfo(tos)
        dataCC = await this.getUserInfo(cCs)
        this.setState({ dataUser, dataCC }, () => {
            this.setState({ loadTo: false, loadCC: false })
        })
    }

    async getUserInfo(data) {
        const { colleague } = this.props;
        if (data && data.length > 0) {
            let list = [];
            for (let i = 0; i < data.length; i++) {
                const { type } = data[i];
                let userInfo = {};
                let resultData = null;
                let resultDefaul = 'Tất cả nhân viên';
                switch (type) {
                    case 1:
                        resultData = await HaravanHr.getDepartmentsById(data[i].id);
                        if (resultData && resultData.data && resultData.data.name) {
                            userInfo = { ...data[i], fullName: resultData.data.name }
                            list.push(userInfo);
                        }
                        break;
                    case 2:
                        resultData = await HaravanHr.getJobtitleById(data[i].id);
                        let resultDepartment = null;
                        if (resultData && resultData.data && resultData.data.name) {
                            if (data[i].parentId) {
                                resultDepartment = await HaravanHr.getDepartmentsById(data[i].parentId);
                                if (resultDepartment && resultDepartment.data && !resultDepartment.error) {
                                    resultDepartment = resultDepartment.data.name;
                                } else {
                                    resultDepartment = null
                                }
                            }
                            userInfo = { ...data[i], fullName: `${resultDepartment ? `${resultData.data.name} tại ${resultDepartment}` : `Tất cả ${resultData.data.name}`}` }
                            list.push(userInfo);
                        }
                        break;
                    case 4:
                        if (colleague.haraId == data[i].id) {
                            userInfo = { ...data[i], fullName: 'Tôi' }
                        } else {
                            resultData = await HaravanHr.getColleague(data[i].id);
                            if (resultData && resultData.data && resultData.data.fullName) {
                                userInfo = { ...data[i], fullName: resultData.data.fullName }
                            }
                        }
                        list.push(userInfo);
                        break;
                    default:
                        userInfo = { ...data[i], fullName: resultDefaul }
                        list.push(userInfo);
                        break;
                }
            }
            return list
        }
    }

    renderAvatar(item) {
        return (
            <Avatar userInfo={{ name: item.createdUserName, picture: item.createdUserAvatar }} v={50} fontSize={20} />
        )
    }

    renderName(item, index, length) {
        return (
            <View
                key={index}
                style={{ flexDirection: 'row', alignItems:'center' }}>
                <Image
                    style={{ width: 15, height: 15 }}
                    source={(item.percentRead == 0) ? imgApp.notSeen : (item.percentRead == 50 ? imgApp.halfSeen : imgApp.seen)} />
                <Text
                    numberOfLines={1}
                    style={{ fontSize: 14, color: '#0279C7', paddingLeft: 3 }}
                >{item.fullName}{(index == length - 1) ? '' : ', '}</Text>
            </View>
        )
    }

    renderUser(item) {
        const { dataUser, titleButon, loadCC, loadTo } = this.state;
        const { isCollap } = this.props
        let mailDraft = item.isDraft == true;
        const time = Utils.formatTime(item.updatedAt, 'HH:mm');
        let title = titleButon ? 'Ẩn chi tiết' : 'Xem chi tiết';
        return (
            <View
                style={{ marginLeft: 5}}
            >
                <View style ={{ flexDirection:'row'}}>
                    <Text style={{ paddingBottom: 10, color: settingApp.colorText, fontSize: 14 }}>{item.createdUserName}</Text>
                    <Text style={{fontSize:14, color:'#CA4F40'}}>{mailDraft ? ', Nháp' : ''}</Text>
                </View>
                {isCollap ?
                    <View
                        style={{ flexDirection: 'row' }}
                    >
                        <Text style={{ color: '#9CA7B2', fontSize: 14 }}>Đến: </Text>
                        {loadTo ? this.renderLoad(1) :
                            <Text 
                            numberOfLines={1}
                            style={{ paddingBottom: 10, maxWidth:(settingApp.width - 150) }}>
                            {
                                dataUser && dataUser.length > 0 ?
                                dataUser.map((e, i) => {
                                    return (
                                        <Text
                                            key={i}
                                            numberOfLines={1}
                                            style={{ fontSize: 14, color: '#9CA7B2', maxWidth:(settingApp.width - 150)}}
                                        >{e.fullName}{(i == dataUser.length - 1) ? '' : ', '}</Text>
                                    )
                                }) : null
                            }
                            </Text>
                        }
                    </View>
                    : null
                }

                {isCollap ?
                    <View style={{ flexDirection: 'row' }}>
                        <Text
                            style={{ color: '#9CA7B2', fontSize: 14 }}
                        >{time}</Text>

                        <TouchableOpacity
                            onPress={() => this.setState({
                                titleButon: !this.state.titleButon,
                                isCollap: !this.state.isCollap
                            })}
                            style={{ marginLeft: 5, height: 20, backgroundColor: 'transparent' }}
                        >
                            <Text
                                style={{ color: '#0279C7', fontSize: 14 }}
                            >{title}</Text>
                        </TouchableOpacity>
                    </View>
                    : null}
                {
                    !isCollap ?
                        <Text>
                            {item.subject}
                        </Text>
                        : null
                }

            </View>
        )
    }

    renderLoad(status) {
        return (
            <View style={{
                backgroundColor: 'transparent',
                justifyContent: 'center',
                alignItems: 'center',
            }}>
                <ActivityIndicator size='small' color={status && status == 1 ? '#9CA7B2' : settingApp.color} />
            </View>
        )
    }

    renderInfo(item) {
        const { dataUser, dataCC, loadTo, loadCC } = this.state;
        return (
            <TouchableOpacity
                onPress={() => this.setState({ isCollap: !this.state.isCollap, titleButon: !this.state.titleButon, })}
                style={{ width: settingApp.width, backgroundColor: '#FFFFFF' }}>
                <View style={{ flexDirection: 'row', padding: 10 }}>
                    <Text style={{ fontSize: 14, color: '#9CA7B2', width: 40 }}>TỪ: </Text>
                    <Text style={{ color: '#0279C7', fontSize: 14, paddingLeft: 15 }}>{item.createdUserName}</Text>
                </View>

                <View style={{ flexDirection: 'row', padding: 10 }}>
                    <Text style={{ fontSize: 14, color: '#9CA7B2', width: 40 }}>ĐẾN: </Text>
                    {loadTo ? this.renderLoad() :
                        <View
                            style={{ flexDirection: 'row', flexWrap: 'wrap', maxWidth: (settingApp.width - 60) }}
                        >
                            {dataUser && dataUser.length > 0 ?
                                (dataUser.map((e, i) => {
                                    return (
                                        this.renderName(e, i, dataUser.length)
                                    )
                                })) : null
                            }
                        </View>}
                </View>

                {dataCC && dataCC.length > 0 ?
                    (loadCC ? this.renderLoad() :
                        <View style={{ flexDirection: 'row', padding: 10, maxWidth: (settingApp.width - 60) }}>
                            <Text style={{ fontSize: 14, color: '#9CA7B2', width: 40 }}>CC: </Text>
                            <View
                                style={{ flexDirection: 'row', flexWrap: 'wrap' }}
                            >{
                                dataCC && dataCC.length > 0 ?
                                dataCC.map((e, i) => {
                                    return (
                                        this.renderName(e, i, dataCC.length)
                                    )
                                }) : null
                            }
                                
                            </View>
                        </View>)
                    : null}
            </TouchableOpacity>
        )
    }

    render() {
        const { isCollap, item, data } = this.props;
        return (
            <View style={{ flex: 1, }}>
                <View style={{ width: settingApp.width, backgroundColor: '#FFFFFF', flexDirection: 'row', paddingBottom: 10, paddingTop: 10 }}>
                    <View
                        style={{ height: 60, width: 60, justifyContent: 'center', alignItems: 'center' }}
                    >
                        {this.renderAvatar(item)}
                    </View>

                    <View
                        style={{ width: (settingApp.width - 80) }}
                    >
                        {this.renderUser(item)}
                    </View>
                    <View>
                    </View>
                </View>

                {isCollap ?
                    <Collapsible collapsed={!this.state.isCollap} style={{ width: settingApp.width }}>
                        <View
                            style={{ width: settingApp.width }}
                        >
                            {this.renderInfo(item)}
                        </View>
                    </Collapsible>
                    : null}
            </View>
        )
    }

    checkCollap() {
    }
}
export default InfoMail;