import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { HeaderIndex, HeaderScreen, settingApp, ButtonCustom, HeaderWapper, imgApp, Loading, ParseText } from '../../../../../public';
import Collapsible from 'react-native-collapsible';
import InfoMail from './infoMail';
import ContentMail from './contentMail';
import{ Feather, MaterialCommunityIcons } from '@expo/vector-icons'
import Attachments from './attachments';

class Item extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isCollap: false,
            isShow:true,
            listAttac:[],
            isAttachments:false
        }

        this.listDetail = this.props.listDetail;
        this.hide = false;
    }
    componentDidMount() {
        this.checkIndex()
        this.attachments()
    }

    attachments(){
        const {item} = this.props;
        if(item.attachments && item.attachments.length > 0){
            this.setState({listAttac: item.attachments}, () =>{
                this.setState({ isAttachments: true})
            })
        }
        else{
            this.setState({ isAttachments: false})
        }
    }

    checkIndex() {
        const { listDetail, item, index } = this.props;
        const length = listDetail.length
        if (item.id === listDetail[length - 1].id) {
            this.setState({ isCollap: true, isShow:false })
        }
        else {
            this.setState({ isCollap: false, isShow:false  })
        }
    }

    optionMore(index, item){
        return(
            <TouchableOpacity
                onPress={() => {
                    (item.isDraft == true) ? 
                    this.props.editMail(index)
                    :
                    this.props.modalOption(index)}}
                style={{position:'absolute', width:100, height:80, right:-50, top:10}}
            >
                {
                    (item.isDraft == true) ? 
                    <MaterialCommunityIcons name='pencil' size={20} color='#BFC7CF'/>
                    :
                    <Feather name='more-horizontal' size={20} color='#BFC7CF' />
                }
            </TouchableOpacity>
        )
    }

    render() {
        const { isLoading, item, data, index, listRemove, listReverse } = this.props;
        const { isCollap, isShow, listAttac, isAttachments } = this.state;
        if (listRemove && listRemove.length > 0) {
            listRemove.map(e => {
                if (e.id === item.id) {
                    this.hide = true;
                }
            });
        }
        if (listReverse && listReverse.length > 0) {
            listReverse.map(e => {
                if (e.id === item.id) {
                    this.hide = false;
                }
            });
        }
        return (
            this.hide ?  null :
            <View
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderTopWidth: index == 0 ? 0 : 1,
                    borderTopColor: '#DAE3EA',
                    ...settingApp.shadow,
                }}
            >
                <TouchableOpacity
                    onPress={() => this.setState({ isCollap: !this.state.isCollap, isAttachmrnts: !this.state.isAttachments })}
                    style={{ width: settingApp.width, }}
                >
                    <InfoMail
                        item={item}
                        isCollap={isCollap}
                        data={data}
                        colleague={this.props.colleague}
                    />
                </TouchableOpacity>
               {isShow ? null :
                <View style={{ flex: 1, width:settingApp.width}}>
                    {this.state.isCollap &&
                        <ContentMail
                            isCollap={isCollap}
                            item={item}
                            data={data}
                            isShow={isShow}
                        />}
                </View>
                }
                {
                    isCollap ?
                    (listAttac.length > 0 ? 
                        <View style={{width:settingApp.width }}> 
                        <Attachments 
                            attachments = { this.state.listAttac }
                        />
                    </View> : null )
                    : null
                }

                { isCollap ?  this.optionMore(index, item) : <View />}
            </View>
        )
    }
}
export default Item