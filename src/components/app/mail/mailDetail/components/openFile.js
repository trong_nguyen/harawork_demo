import React, { Component } from 'react';
import { View, Modal, CameraRoll, Image, TouchableOpacity, Platform , WebView, Text } from 'react-native';
import { settingApp, imgApp, Toast, ReviewImage, DownloadFile, Loading } from '../../../../../public';
import Config from '../../../../../Haravan/config';
import { Ionicons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import * as WebBrowser  from 'expo-web-browser';
import * as FileSystem from 'expo-file-system'

class OpenFile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dataSelected: null,
            type: null,
            isDownload: false,
        }
        this.userInfo = props.app.authHaravan.auth.access_token;
    }

    componentWillReceiveProps(nextProps) {
        if (JSON.stringify(this.state.dataSelected) !== JSON.stringify(nextProps.dataSelected)) {
            const { dataSelected } = nextProps;
            const { url, id, name } = dataSelected;
            let type = null;
            let isDownload = false;
            if ((name.match(/\.(jpg|jpeg|png|gif)$/) != null)) {
                type = 'image';
                isDownload = true;
            } else if ((name.match(/\.(pdf)$/) != null)) {
                type = 'pdf';
            }
            if ((name.match(/\.(jpg|jpeg|png|gif|doc|docx|xls|xlsx|txt)$/) != null)) {
                //isDownload = true;
            }
            this.setState({ dataSelected: nextProps.dataSelected, type, isDownload });
        }
    }

    async download() {
        const { dataSelected } = this.state;
        DownloadFile(dataSelected.id, this.userInfo, false)
    }

    renderHeader() {
        const { width, isIPhoneX } = settingApp;
        const top = isIPhoneX ? 44 : Platform.OS === 'ios' ? 30 : 0;
        return (
            <View style={{ position: 'absolute', top, left: 0, right: 0, ...settingApp.shadow }}>
                <View style={{ width, height: 44, flexDirection: 'row', justifyContent: 'space-between' }} >
                    <TouchableOpacity
                        onPress={() => this.props.close()}
                        style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center' }}
                    >
                        <Ionicons
                            name='md-close'
                            color='#9CA7B2'
                            size={30}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    renderViewIOS(){
        const { type, dataSelected } = this.state;
        if(dataSelected){
            const url = `https://ic.${Config.apiHost}/call/im_api/attachment/${dataSelected.id}?isview=true&access_token=${this.userInfo}`; 
            return(
                <WebView
                    style={{ flex:1 }} 
                    bounces={false}
                    scrollEnabled={true} 
                    source={{uri: url}}
                    useWebKit={true}
                    domStorageEnabled={true}
                    originWhitelist={['*']}
                    >
                </WebView>               
            )
        }
        else{
            return(
                <View />               
            )
        }
    }
    
    renderView() {
        const { type, dataSelected } = this.state;
        let content = <View />;
        if(dataSelected){
        const url = `http://ic.${Config.apiHost}${dataSelected.url}?isView=true&access_token=${this.userInfo}`; 
            if (type == 'image') {
                content = ( 
                    <ReviewImage
                        id={this.state.dataSelected.id}
                    />
                )
            } 
            else if( type =='pdf') {
                content = (
                    <View style={{flex:1}}>
                        {/* <PDFReader
                            source={{ uri: url}}
                        /> */}
                    </View>
                )
            }
            
        }
        else{
            content =  <View />               
        }
        return content;
    }

    renderContent() {
        const { isIPhoneX } = settingApp;
        const marginTop = isIPhoneX ? 88 : Platform.OS === 'ios' ? 74 : 44;
        let content = Platform.OS === 'ios' ? this.renderViewIOS() : this.renderView()
        return (
            <View style={{flex:1, marginTop}}>
                {content}
            </View>
        )
    }

    render() {
        const { url } = this.props
        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.props.modalVisible}
                onRequestClose={() => this.props.close()}
                >
                <View style={{ flex: 1, backgroundColor: '#FFFF' }}>
                    {this.renderContent()}
                    {this.renderHeader()}
                </View>
            </Modal>
        )
    }
}

const mapStateToProps = (state) => ({ ...state })
export default connect(mapStateToProps)(OpenFile)