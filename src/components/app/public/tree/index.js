import React, { Component } from 'react';
import { createMaterialTopTabNavigator } from 'react-navigation';
import  {settingApp}  from '../../../../public';
import Tab1 from './screenTab/tab1'
import Tab2 from './screenTab/tab2'
import Tab3 from './screenTab/tab3'
import {Entypo} from '@expo/vector-icons'

const TabListView = createMaterialTopTabNavigator({
    First: {
        screen:(props) => <Tab1 
            listUser={(item) => props.screenProps.listUser(item)}
            list={props.screenProps.list}
            collegue={props.screenProps.collegue}
            userInfo = {props.screenProps.userInfo}
            listRemove ={ props.screenProps.listRemove }
            {...props}
        />,
        navigationOptions:(props) => {
                return {
                    tabBarIcon:({tintColor}) =>(

                        <Entypo name='back-in-time' color={tintColor} size= {15}/>
                    ),
                    tabBarLabel: `Gần đây`
            }
        }
    },
    Second: {
        screen:(props) => <Tab2 
            listUser={(item) => props.screenProps.listUser(item)}
            list={props.screenProps.list}
            collegue={props.screenProps.collegue}
            userInfo = {props.screenProps.userInfo}
            listRemove ={ props.screenProps.listRemove }
            {...props}
        />,
        navigationOptions:(props) => {
                return {
                    tabBarIcon:({tintColor}) =>(
                        <Entypo name='flow-tree' color={tintColor} size= {15}/>
                    ),
                    tabBarLabel: `Theo cây công ty`
            }
        }
    },
    Third: {
        screen:(props) => <Tab3
            listUser={(item) => props.screenProps.listUser(item)}
            list={props.screenProps.list}
            collegue={props.screenProps.collegue}
            userInfo = {props.screenProps.userInfo}
            listRemove ={ props.screenProps.listRemove }
            {...props}
        />,
        navigationOptions:(props) => {
                return {
                    tabBarIcon:({tintColor}) =>(

                        <Entypo name='v-card' color={tintColor} size= {15}/>
                    ),
                    tabBarLabel: `Theo chức danh`
            }
        }
    },
}, 
{   
    tabBarOptions: {
        activeTintColor: settingApp.color,
        inactiveTintColor: settingApp.colorDisable,
        showIcon:true,
        labelStyle: {
            marginTop: 1,
            fontSize: 12,
            width: (((settingApp.width)) / 3)
        },
        tabStyle: {
            height:54,
            backgroundColor:'transparent',
            width: (((settingApp.width)) / 3),
            
        },
        style: {
            borderBottomColor:'#ffffff',
            borderBottomWidth:3,
            width: ((settingApp.width)),
            height: 54,
            backgroundColor: '#ffffff',
            ...settingApp.shadow
        },
        indicatorStyle: {
            borderBottomColor:settingApp.color,
            borderBottomWidth:4,

            borderColor: '#ffffff',
            // borderWidth: 44
        },
        upperCaseLabel: false,
    },
    lazy: false,
    swipeEnabled: true,
}
)
export default TabListView;