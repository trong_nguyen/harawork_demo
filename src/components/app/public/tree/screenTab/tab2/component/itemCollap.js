import React, { Component } from 'react';
import { View, Text, TouchableOpacity, FlatList} from 'react-native';
import { HaravanHr } from '../../../../../../../Haravan'
import Item from './item';
import {  SvgCP } from '../../../../../../../public'


class ItemCollap extends Component {
    constructor(props) {
        super(props)
        this.state = {
            listData:[],
            isLoading:true
        }
        this.listUser = this.props.listUser
    }

    async componentDidMount(){
        this.loadData()
    }

    async loadData(){
        const { item } = this.props;
        const { listData } = this.state;
        if(item.type == 1){
            const result = await HaravanHr.getCompanysTructure(item.id, true);
            if(result && result.data && !result.error){
                result.data.map(e =>{
                    const icon = (e.type == 3) ? <SvgCP.job/> : <SvgCP.iconTree/>;
                    const iconCheck = (e.type == 3) ? <SvgCP.jobClick/> : <SvgCP.iconTreeClick/>
                    let keyID = e.keyID ? e.keyID : (e.type + '' + e.id);
                    e = {...e, departmentId: item.id, icon: icon, iconCheck: iconCheck, typeMail: e.type == 3 ? 2 : 1, departmentName:item.name, keyID}
                    if(e.type && (e.type == 3) && (e.count > 0)){
                        const index = listData.findIndex(m => m.id === e.id)
                        if(index < 0){
                            listData.push(e)
                        }
                    }
                    else if(e.type && (e.type == 1)){
                        const index = listData.findIndex(m => m.id === e.id)
                        if(index < 0){
                            listData.push(e)
                        }
                    }
                })
            }
        }
        else if(item.type == 3){
            let body =[{ 
                departmentId: item.departmentId, 
                jobtitleId: item.id
            }]
            const result = await HaravanHr.getListCompanystructure(1,body)
            if(result && result.data && !result.error){
                result.data.data.map(m =>{
                    let keyID = m.keyID ? m.keyID : (m.userId + '' + m.haraId + '' + m.mainPosition.departmentId);
                    const icon = <SvgCP.me/>;
                    const iconCheck = <SvgCP.meClick/>;
                    m = {...m, name: m.fullName, icon: icon, iconCheck: iconCheck, typeMail: 4, keyID}
                    const indexList = listData.findIndex(e => e.keyID == m.keyID)
                    if(indexList < 0 ){
                        listData.push(m)
                    }
                })
            }
        }
        this.setState({listData, isLoading:false})
    }

    renderCollap(obj){
        const { item, index} = obj
            return (
                            <Item
                                item = {item}
                                listData={this.state.listData}
                                listUser = {(item) => this.listUser(item)}
                                list = { this.props.list}
                                listRemove = { this.props.listRemove}
                            />
                                )
    }
    render() {
        const {  isLoading } = this.state;
            return (
                <View style={{marginLeft:20, flex:1}}>
                    <FlatList
                        data={this.state.listData}
                        extraData={this.state}
                        keyExtractor={(item, index) => (item.keyID + '')}
                        renderItem={(obj) => this.renderCollap(obj) }
                    />
                </View>
            )
    }
}
export default ItemCollap;