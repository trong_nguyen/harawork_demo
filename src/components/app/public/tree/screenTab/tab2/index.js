import Layout from './layout';
import React, { Component } from 'react'
import { HaravanHr } from '../../../../../../Haravan';
import { SvgCP } from '../../../../../../public'

class Tab2 extends Layout {
    constructor(props) {
        super(props);
        this.state = {
            isLoading:true,
            listData: [],
            listCheck: [],
            listJob: [],
            itemInfo: {},
            listItem:[]
        }

        this.userInfo = props.userInfo;
        this.list = props.list;
        this.listRemove =props.listRemove;
        this.listTotal = []
    }

    componentWillReceiveProps(nextProps) {
        this.loadData();
        this.filterList();
    }

    filterList() {
        const { listCheck } = this.state;
        this.list.map(e => {
            if (listCheck.length == 0) {
                listCheck.push(e)
            } else {
                const index = listCheck.findIndex(m => m.id === e.id)
                if (index < 0) {
                    listCheck.push(e)
                } else {
                    listCheck.splice(index, 1)
                }
            }
        })
        this.setState({ listCheck })
    }

    async componentDidMount() {
        this.loadData()
        this.filterList()
    }

    listUser(item) {
        let data = {
            ...item,
            fullName: item.fullName ? item.fullName : item.name
        }
        const {
            listCheck
        } = this.state;
        this.props.listUser(data)
        if (listCheck.length == 0) {
            listCheck.push(data)
        } else {
            const index = listCheck.findIndex(m => m.id === data.id)
            if (index < 0) {
                listCheck.push(data)
            } else {
                listCheck.splice(index, 1)
            }
        }
        this.setState({
            listCheck
        })
    }




    async loadData() {
        const { listData } = this.state;
        const result = await HaravanHr.getCompanysTructure(null, true);
        if (result && result.data && !result.error) {
            result.data.map(item => {
                let keyID = item.type + '' + item.id;
                item = { ...item, typeMail: 1, icon:<SvgCP.iconTree/>, iconCheck:<SvgCP.iconTreeClick/>, departmentId: item.id, keyID}
                if(listData.length == 0){
                    listData.push(item)
                }
                else{
                    const index = listData.findIndex(m => (m.id === item.id)&& (m.name === item.name))
                    if(index < 0){
                        listData.push(item)
                    }
                }
            })
            this.setState({
                isLoading:false,
                listData
            }, () => {
                this.setState({
                    itemInfo: { 
                        keyID:(this.userInfo.orgname + '' +this.userInfo.email),
                        fullName: this.userInfo.orgname, 
                        subject: 'Tất cả nhân viên', id: 'all', 
                        name: this.userInfo.orgname, 
                        type: 5
                    }
                })
            })
        }
    }
}
export default Tab2;