import React, { Component } from 'react';
import { View, TouchableOpacity, Text, FlatList, ScrollView, Image } from 'react-native';
import Item from './component/item';
import { imgApp, SvgCP, settingApp, Loading } from '../../../../../../public';
import { AntDesign } from '@expo/vector-icons';

class Layout extends Component {

    rednerContent() {
        const { listData } = this.state;
            return (
                <FlatList
                    style={{flex:1}}
                    data={this.state.listData }
                    extraData={this}
                    keyExtractor={(item, index) => (item.id+'')}
                    renderItem={({ item, index }) =>
                        <Item
                            item={item}
                            listData={this.state.listData}
                            listUser={item => this.listUser(item)}
                            list = {this.list}
                            listRemove = { this.listRemove}
                            listTotal = {this.listTotal}
                        />
                    }
                />
            )
        }
    
    renderView(){
        return(
            <View style={{flex:1, alignItems: 'center',justifyContent: 'center', backgroundColor: '#FFFFFF', }}>
                <Text style={{fontSize:16, color:settingApp.colorText}}>
                    Tính năng đang được xây dựng
                </Text>
            </View>
        )
    }

    render() {
        const { userInfo } = this;
        const { itemInfo , isLoading, listItem} = this.state;
        if(isLoading == true){
            return(
                <Loading/>
            )
        }
        else{
            return (
                <ScrollView style={{flex:1, backgroundColor: '#FFFFFF', paddingBottom:10 }}>
                    <TouchableOpacity
                        //onPress={() => this.listUser(itemInfo)}
                        style={{ flexWrap: 'wrap', alignItems: 'center', paddingLeft: 10, flexDirection: 'row', paddingTop: 10 }}>
                        <AntDesign name='right' size={15} color='#9CA7B2' />
                        <View style={{ marginLeft: 10, backgroundColor: 'rgba(33, 70, 155, 0.08)', padding: 10, flexDirection: 'row' }}>
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <SvgCP.homeOrg />
                            </View>
                            <View style={{ justifyContent: 'center', alignItems: 'center', paddingLeft:10 }}>
                                <Text style={{ color: '#21469B', fontSize: 14 }}>
                                    { itemInfo ? itemInfo.fullName : 'loading...'}
                                </Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    { this.rednerContent() }
                </ScrollView>
            )
        }
    }
}
export default Layout;