import Layout from './layout';
import React, { Component } from 'react'
import {
    HaravanHr
} from '../../../../../../Haravan';
import { SvgCP } from '../../../../../../public'

class Tab3 extends Layout {
    constructor(props) {
        super(props);
        this.state = {
            listCheck: [],
            listJob: [],
            itemInfo: {},
        }

        this.userInfo = this.props.userInfo;
        this.list = this.props.list;
        this.listRemove = this.props.listRemove
    }

    componentWillReceiveProps(nextProps) {
        this.loadJob();
        this.filterList()
    }

    filterList() {
        const { listCheck } = this.state;
        this.list.map(e => {
            if (listCheck.length == 0) {
                listCheck.push(e)
            } else {
                const index = listCheck.findIndex(m => m.id === e.id)
                if (index < 0) {
                    listCheck.push(e)
                } else {
                    listCheck.splice(index, 1)
                }
            }
        })
        this.setState({ listCheck })
    }

    async componentDidMount() {
        this.loadJob()
        this.filterList()
    }

    listUser(item) {
        let data = {
            ...item,
            fullName: item.fullName ? item.fullName : item.name
        }
        const {
            listCheck
        } = this.state;
        this.props.listUser(data)
        if (listCheck.length == 0) {
            listCheck.push(data)
        } else {
            const index = listCheck.findIndex(m => m.id === data.id)
            if (index < 0) {
                listCheck.push(data)
            } else {
                listCheck.splice(index, 1)
            }
        }
        this.setState({
            listCheck
        })
    }

    async loadJob() {
        let newList = []
        const resultJob = await HaravanHr.getJobTructure();
        if (resultJob && resultJob.data && !resultJob.error) {
            resultJob.data.data.map(item =>{
                item = { ...item, fullName: ('Tất cả ' + item.name), typeMail: 2, icon:<SvgCP.job/>, iconCheck:<SvgCP.jobClick/>}
                if(newList.length == 0 ){
                    newList.push(item)
                }
                else {
                    const index = newList.findIndex(m =>(m.id === item.id))
                    if(index < 0){
                        newList.push(item)
                    }
                }
            })
            this.setState({
                listJob:newList
            }, () => {
                this.setState({
                    fisrtInfo: null
                })
            })
        }
    }
}
export default Tab3;