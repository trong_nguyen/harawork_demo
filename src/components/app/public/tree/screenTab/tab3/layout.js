import React, { Component } from 'react';
import { View, Text, FlatList, ScrollView} from 'react-native';
import Item from './component/item';
import {settingApp, Loading } from '../../../../../../public';

class Layout extends Component {

    rednerContent() {
        const { listData } = this.state;
            return (
                <FlatList
                    style={{flex:1}}
                    data={this.state.listJob}
                    extraData={this.state}
                    keyExtractor={(item, index) => (item.id+'')}
                    renderItem={({ item, index }) =>
                        <Item
                            item={item}
                            listJob={this.state.listJob}
                            listUser={item => this.listUser(item)}
                            list = { this.list}
                            listRemove = { this.listRemove}
                        />
                    }
                />
            )
        }
    renderView(){
            return(
                <View style={{flex:1, alignItems: 'center',justifyContent: 'center', backgroundColor: '#FFFFFF', }}>
                    <Text style={{fontSize:16, color:settingApp.colorText}}>
                        Tính năng đang được xây dựng
                    </Text>
                </View>
            )
        }

    render() {
        const { isLoading} = this.state;
        if(isLoading == true){
            return(
                <Loading/>
            )
        }
        else{
            return (
                <ScrollView style={{flex:1, backgroundColor: '#FFFFFF', paddingBottom:10 }}>
                    { this.rednerContent() }
                </ScrollView>
            )
        }
    }
}
export default Layout;