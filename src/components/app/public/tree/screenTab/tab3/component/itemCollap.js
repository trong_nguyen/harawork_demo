import React, { Component } from 'react';
import { View, FlatList} from 'react-native';
import { HaravanHr } from '../../../../../../../Haravan'
import Item from './item';
import { SvgCP } from '../../../../../../../public'


class ItemCollap extends Component {
    constructor(props) {
        super(props)
        this.state = {
            listData:[]
        }
        this.listUser = this.props.listUser
    }

    async componentDidMount(){
        const{listData  } = this.state;
        const { item, list } = this.props
        if(item.departments){
            item.departments.map(e =>{
                const index = listData.findIndex(m => m.departmentId == e.departmentId)
                if(index < 0){
                    e = { ...e, name: e.departmentName, typeMail: 1 , fullName: e.departmentName}
                    listData.push(e)
                }
            })
            this.setState({ listData })
        }
        else{
            if(!item.type){
                let page = 1
                let data = {departmentId: item.departmentId, jobtitleId: item.jobtitleId}
                const body = [];
                body.push(data)
                const resultEmploy = await HaravanHr.getListCompanystructure(page,body)
                if(resultEmploy && resultEmploy.data && !resultEmploy.error){
                    const icon = <SvgCP.me/>;
                    const iconCheck = <SvgCP.meClick/>;
                    resultEmploy.data.data.map(e =>{
                        e = {...e, name: e.fullName , type:3 , typeMail: 4, departmentName:item.name,icon: icon, iconCheck: iconCheck }
                        listData.push(e)
                    })
                    this.setState({listData})
                }
            }
        }
            
    }

    async loadData(){
        const { item } = this.props;
        const { listData } = this.state;
        if(item.type == 1){
            const result = await HaravanHr.getCompanysTructure(item.id, true);
            if(result && result.data && !result.error){
                result.data.map(e =>{
                    const icon = (e.type == 3) ? <SvgCP.job/> : <SvgCP.iconTree/>;
                    const iconCheck = (e.type == 3) ? <SvgCP.jobClick/> : <SvgCP.iconTreeClick/>
                    e = {...e, departmentId: item.id, icon: icon, iconCheck: iconCheck, typeMail: e.type == 3 ? 2 : 1, departmentName:item.name}
                    if(e.type && (e.type == 3) && (e.count > 0)){
                        const index = listData.findIndex(m => m.id === e.id)
                        if(index < 0){
                            listData.push(e)
                        }
                    }
                    else if(e.type && (e.type == 1)){
                        const index = listData.findIndex(m => m.id === e.id)
                        if(index < 0){
                            listData.push(e)
                        }
                    }
                })
            }
        }
        else if(item.type == 3){
            let body =[{ 
                departmentId: item.departmentId, 
                jobtitleId: item.id
            }]
            const result = await HaravanHr.getListCompanystructure(1,body)
            if(result && result.data && !result.error){
                result.data.data.map(m =>{
                    const icon = <SvgCP.me/>;
                    const iconCheck = <SvgCP.meClick/>;
                    m = {...m, name: m.fullName, icon: icon, iconCheck: iconCheck, typeMail: 4}
                    listData.push(m)
                })
            }
        }
        this.setState({listData})
    }

    render() {
        return (
            <View style={{marginLeft:20, flex:1}}>
            <FlatList
                data={this.state.listData}
                extraData={this.state}
                keyExtractor={(item, index) => (index + '')}
                renderItem={({ item, index }) =>
                    <Item
                        item={item}
                        listUser = {(item) => this.listUser(item)}
                        list = { this.props.list}
                        listRemove = { this.props.listRemove}
                    />
                }
            />
            </View>
        )
    }
}
export default ItemCollap;