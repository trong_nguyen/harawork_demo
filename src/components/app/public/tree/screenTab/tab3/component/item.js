import React, { Component } from 'react';
import { Text, View,  TouchableOpacity, } from 'react-native';
import { SvgCP } from '../../../../../../../public'
import { AntDesign } from '@expo/vector-icons';
import ItemCollap from './itemCollap'

class Item extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpen: false,
            isCollap: true,
            isCheck: false
        }
    }

    componentWillReceiveProps(nextProps){
        const { item } = this.props
        if(nextProps && nextProps.listRemove ){
            nextProps.listRemove.map(e =>{
                if(e.departmentId){
                    if((e.departmentId == item.departmentId) && (e.name === item.name)){
                        this.setState({isCheck: false})
                    }
                   
                }
                else if((e.id == item.id) && (e.name === item.name)){
                    this.setState({isCheck: false})
                }
            })
        }
    }

    componentDidMount(){
        const { item, list } = this.props;
        list.map(e =>{
            if(e.departmentId && !e.departmentParentId){
                if((e.departmentId == item.departmentId) && ((e.name == item.jobtitleName) 
                || (e.name == item.name)
                )
                ){
                    this.setState({isCheck: true})
                }
                else if((e.departmentId == item.id) && (e.jobtitleName == item.name)){
                    this.setState({isCheck: true})
                }
                else if((e.departmentId == item.departmentId) && (e.name == item.name)){
                    this.setState({isCheck: true})
                }
            }
            else if((e.id == item.id) && (e.name == item.name)){
                this.setState({isCheck: true})
            }
            else if((e.departmentParentId == item.departmentId) && (e.name == item.name)){
                this.setState({isCheck: true})
            }
        })
    }

    render() {
        const { item } = this.props;
        const { isCheck } = this.state;
        return (
            <View style={{ flex: 1, borderLeftColor: '#DAE3EA', borderLeftWidth: 1 }}>
                <View
                    style={{
                        flex: 1,
                        flexWrap: 'wrap',
                        alignItems: 'center',
                        flexDirection: 'row',
                        paddingTop: 5
                    }}>
                    <View
                        style={{ backgroundColor: '#DAE3EA', width: 20, height: 1 }}
                    />
                    <TouchableOpacity
                        activeOpacity={item.type ==1 ? 0.5 : 1}
                        style={{width:30, height:30, alignItems:'center',justifyContent:'center'}}
                        onPress={() =>{
                            this.setState({ isOpen: !this.state.isOpen })
                            }}
                    >
                        <AntDesign name={this.state.isOpen ? 'down' :'right'} size={15} color='#9CA7B2' />
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => {
                            this.setState({isCheck: !this.state.isCheck})
                            this.props.listUser(item)}}
                        style={{ backgroundColor: isCheck  ? '#21469B' :'rgba(33, 70, 155, 0.08)', padding: 10, flexDirection: 'row', height:44 }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        {
                           ( item.icon && item.iconCheck) ? ( isCheck ? item.iconCheck : item.icon) : ( isCheck ? <SvgCP.iconTreeClick/> :<SvgCP.iconTree/>)
                        }
                        </View>
                        <View style={{ justifyContent: 'center', alignItems: 'center', paddingLeft:10 }}>
                            <Text style={{ color: isCheck ? '#FFFFFF' :'#21469B', fontSize: 14 }}>
                                {item.name}
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>

                {this.state.isOpen ?
                    <ItemCollap
                        item={item}
                        listUser = {(item) => this.props.listUser(item)}
                        list = { this.props.list}
                        listRemove = { this.props.listRemove}
                    />
                    : <View />
                }
            </View>
        )
    }
}
export default Item;