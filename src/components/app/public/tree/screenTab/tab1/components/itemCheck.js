import React, { Component } from 'react';
import { View, Text, TouchableOpacity} from 'react-native';
import {  settingApp,  } from '../../../../../../../public';

class ItemCheck extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isCheck: false,
        }
    }

    componentWillReceiveProps(nextProps){
        const { item, list } = this.props
        if(nextProps && nextProps.listRemove && nextProps.listRemove.length > 0){
            nextProps.listRemove.map(e =>{
                if((e.id === item.id) && (e.fullName === item.fullName)){
                    this.setState({isCheck:false})
                }
            })
        }

        if(list && list.length > 0){
            list.map(e => {
                if(e.haraId && item.haraId && (e.haraId == item.haraId)){
                    this.setState({isCheck:true})
                }
                else if(e.departmentUnitId && item.departmentUnitId && (e.departmentUnitId == item.departmentUnitId)){
                    this.setState({isCheck:true})
                }
                else if((e.typeMail == 2) && e.id && item.id && ( e.id  == item.id)){
                    this.setState({isCheck:true})
                }
            })
        }
    }

    componentDidMount(){
        const { list, item } = this.props
        if(list && list.length > 0){
            list.map(e =>{
                if((e.id === item.id) && (e.fullName === item.fullName)){
                    this.setState({isCheck:true})
                }
            })
        }
    }

    render() {
        const { item } = this.props;
        const {isCheck} = this.state;
        return (
            <TouchableOpacity
                onPress={() => {
                    this.setState({isCheck:!this.state.isCheck})
                    this.props.checkItem(item)}}
                style={{  marginRight: 10, marginBottom: 10, backgroundColor:(isCheck) ?'#21469B' : '#E9EDF5', padding: 10, height:60 }} >
            <View style={{flexDirection: 'row' }}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    {isCheck ? item.iconCheck : item.icon}
                </View>
                <View style={{ justifyContent: 'center', paddingLeft:10}}>
                    <Text style={{ color: (isCheck) ? '#FFFFFF' : '#21469B', fontSize: 14 }}>
                        {item.fullName}
                    </Text>
                    <Text style={{ color: (isCheck) ? '#FFFFFF' : settingApp.colorText, fontSize: 12 }}>{item.subject}</Text>
                </View>
                {
                    item.total ?
                    <View
                    style={{marginLeft:5 ,height:25, width:35, borderRadius:15, backgroundColor:'#9CA7B2', justifyContent:'center', alignItems:'center'}}
                    > 
                        <Text style={{fontSize:14, color:'#FFFFFF'}}>{item.total}</Text>
                    </View>
                    : null
                }
            </View>
            </TouchableOpacity>
        )
    }
}
export default ItemCheck;