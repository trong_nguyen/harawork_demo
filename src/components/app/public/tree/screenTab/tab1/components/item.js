import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { HeaderWapper, settingApp, imgApp, SvgCP } from '../../../../../../../public';
import ItemCheck from './itemCheck'

class Item extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isCheck: false,
        }
    }

    checkItem(e){
        this.props.listUser(e);
    }


    renderContent(data) {
        return (
            <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap', alignItems: 'flex-start' }}>
                {
                    data.list && data.list.length > 0 ?
                        (
                            data.list.map((e, i) => {
                                return (
                                    <View
                                        key={i}
                                    >
                                    <ItemCheck 
                                        item ={e}
                                        checkItem={(e => this.checkItem(e))}
                                        list={this.props.list}
                                        listRemove={this.props.listRemove}
                                    />
                                    </View>
                                )
                            })
                        )
                        : null
                }
            </View>
        )
    }

    render() {
        const { item } = this.props
        if (item.list.length > 0) {
            return (
                <View style={{
                    minHeight: 80, padding: 10, backgroundColor: '#FFFFFF'
                }}>
                    <Text
                        style={{ color: '#9CA7B2', fontSize: 14 }}
                    >{item.title}</Text>
                    {this.renderContent(item)}
                </View>
            )
        }
        else {
            return null;
        }
    }
}
export default Item;