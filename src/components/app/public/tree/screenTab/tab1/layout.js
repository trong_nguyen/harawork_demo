import React, { Component } from 'react';
import { View, TouchableOpacity, Text, FlatList, ScrollView, ActivityIndicator } from 'react-native';
import Item from './components/item';
import { Loading, settingApp } from '../../../../../../public'

class Layout extends Component {
    renderLoad() {
        return (
            <View style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                backgroundColor: 'rgba(0,0,0,0.3)',
                justifyContent: 'center',
                alignItems: 'center',
            }}>
                <ActivityIndicator size='large' color={settingApp.color} />
            </View>
        )
    }

    renderList() {
        return (
            <FlatList
                data={this.state.data}
                extraData={this.state}
                keyExtractor={(item, index) => (index+'')}
                renderItem={({item, index}) => 
                    <Item 
                        item={item}
                        listUser={item => this.listUser(item)}
                        list = {this.list }
                        listRemove = { this.listRemove}
                    />
                }
            />
        )
    }

    render() {
        const { isLoad } = this.state; 
        return (
            <View style={{ flex: 1}}>
                {this.renderList()}
                {isLoad ? this.renderLoad() : null}
            </View>
        )
    }
}
export default Layout;