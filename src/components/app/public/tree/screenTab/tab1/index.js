import Layout from './layout';
import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import { HaravanHr } from '../../../../../../Haravan';
import { imgApp, SvgCP, keyStore } from '../../../../../../public'

class Tab1 extends Layout {
    constructor(props) {
        super(props);
        this.state = {
            data: [
                { id: 1, title: 'THÔNG DỤNG', list: [] },
                { id: 2, title: 'ĐƠN VỊ', list: [] },
                { id: 3, title: 'CHỨC DANH', list: [] },
                { id: 4, title: 'CÁ NHÂN', list: [] }
            ],
            listCheck: [],
            isLoad: false,
            listRemove:[]
        }
        this.collegue = props.collegue;
        this.userInfo = props.userInfo;
        this.list = props.list;
        this.listRemove = props.listRemove;
    }

    componentWillReceiveProps(nextProps){
        const { listCheck } = this.state;
        if(nextProps.listRemove && nextProps.listRemove.length > 0 ){
            nextProps.listRemove.map(item =>{
                const index = listCheck.findIndex(e => (e.id == item.id) && (e.fullName === item.fullName))
                if(index > -1){
                    listCheck.splice(index , 1)
                }
            })
            this.setState({listCheck})
        }
        if(nextProps.list && nextProps.list.length > 0){
            const {listCheck} = this.state;
            nextProps.list.map(item =>{
                if (listCheck.length == 0) {
                    listCheck.push(item)
                }
                else {
                    const index = listCheck.findIndex(e => e.id === item.id)
                    if (index < 0) {
                        listCheck.push(item)
                    }
                    else {
                        listCheck.splice(index, 1)
                    }
                }
            })
            this.setState({listCheck})
        }
    }

    componentDidMount() {
        const { listCheck } = this.state;
        const { list }  = this.props;
        if(list && list.length > 0){
            this.list.map(e => {
                if (listCheck.length == 0) {
                    listCheck.push(e)
                }
                else {
                    const index = listCheck.findIndex(m => m.id === e.id)
                    if (index < 0) {
                        listCheck.push(e)
                    }
                    else {
                        listCheck.splice(index, 1)
                    }
                }
            })
            this.setState({ listCheck })
        }
        this.loadData()
    }
    
    refreshData() {
        const { data } = this.state
        data[1].list = [];
        data[2].list = [];
        data[3].list = [];
        this.setState({ data })
    }

    async onSearch(text) {
        this.setState({ isLoad: true })
        const { data } = this.state;
        const resultDepart = await HaravanHr.searchDepartments(text)
        const resultJobli = await HaravanHr.searchJobtitle(text)
        const resultUser = await HaravanHr.searchColleague(text)
        if (resultDepart && resultDepart.data && !resultDepart.error) {
            data[1].list = []
            resultDepart.data.data.map(e => {
                e = { ...e, fullName: e.name, subject: e.code, key: 'departments', typeMail:1, icon:<SvgCP.iconTree/>, iconCheck:<SvgCP.iconTreeClick/> }
                data[1].list.push(e)
            })
        }
        if (resultJobli && resultJobli.data && !resultJobli.error) {
            resultJobli.data.data.map(e => {
                data[2].list = [];
                if (e.departments.length > 2) {
                    const total = { ...e, fullName: e.name, subject: ('Tất cả ' + e.name), total: (e.departments.length), typeMail:2, icon:<SvgCP.job/>, iconCheck:<SvgCP.jobClick/> }
                    const index = data[2].list.findIndex(a => (a.id === e.id) && (a.jobtitleLevelId === e.jobtitleLevelId))
                    if (index < 0) {
                        data[2].list.push(total)
                    }
                }
                e.departments.map(m => {
                    m = { ...m, fullName: m.jobtitleName, subject: m.departmentName, key: 'jobtitle', typeMail:2, icon:<SvgCP.job/>, iconCheck:<SvgCP.jobClick/> }
                    const index = data[2].list.findIndex(a => (a.id === m.id) && (a.jobtitleId === m.jobtitleId))
                    if (index < 0) {
                        data[2].list.push(m)
                    }
                })
            })
        }
        if (resultUser && resultUser.data && !resultUser.error) {
            data[3].list = [];
            resultUser.data.data.map(e => {
                e = { ...e, subject: (e.departmentName + '-' + e.jobtitleName), key: 'employee', typeMail:4, icon:<SvgCP.me/>, iconCheck:<SvgCP.meClick/> }
                const index = data[3].list.findIndex(a => a.id === e.haraId)
                if (index < 0) {
                    data[3].list.push(e)
                }
            })
        }
        this.setState({ data, isLoad: false  },() =>{
            this.setState({ isLoad: false})
        })
    }

    listUser(item) {
        this.props.listUser(item)
    }

    async loadData() {
        const { data } = this.state
        
        this.collegue = { ...this.collegue, subject: 'Tài khoản của bạn', fullName: 'Tôi', id: this.collegue.haraId, typeMail:4, icon: <SvgCP.me/>, iconCheck:<SvgCP.meClick/> }
        let dataAut = { fullName: this.userInfo.orgname, subject: 'Tất cả nhân viên', id: 'all', type: 5, typeMail:5, icon:<SvgCP.iconTree/>, iconCheck:<SvgCP.iconTreeClick/> }
        data[0].list.push(this.collegue, dataAut)
        let historyFromAndCC = await AsyncStorage.getItem(keyStore.historyFromAndCC);
        historyFromAndCC = JSON.parse(historyFromAndCC)
        if(historyFromAndCC && historyFromAndCC.length > 0){
            historyFromAndCC.map(item => {
                if(item.typeMail == 1){
                    item = {...item,  icon: <SvgCP.iconTree/>, iconCheck:<SvgCP.iconTreeClick/>, subject:item.fullName}
                    data[1].list.push(item)
                }
                else if(item.typeMail == 2){
                    item = {...item,  icon: <SvgCP.job/>, iconCheck:<SvgCP.jobClick/>, subject:item.fullName}
                    data[2].list.push(item)
                }
                else if(item.typeMail == 4){
                    item = {...item,  icon: <SvgCP.me/>, iconCheck:<SvgCP.meClick/>, subject:item.fullName}
                    data[3].list.push(item)
                }
            })
        }
        this.setState({ data })
    }

}
export default Tab1;