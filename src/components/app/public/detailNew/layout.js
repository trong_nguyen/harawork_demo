import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import { HeaderScreen, ParseText, settingApp, Utils, AsyncImage } from '../../../../public';

class Layout extends Component {

    render() {
        const {  pinHome, groupName, name, createdAt, fullName, } = this.data;
        const { data } = this.state;
        return (
            <View style={{ flex: 1 }}>
                <HeaderScreen navigation={this.props.navigation} />
                <ScrollView style={{ backgroundColor: '#ffffff' }}>
                    <View style={{ flex: 1, height: settingApp.imageFullSize }}>
                        <AsyncImage
                            source={{ uri: pinHome.imageUrl }}
                            style={{ flex: 1, width: undefined, height: undefined }}
                            resizeMode='contain'
                        />
                    </View>
                    <View style={{ padding: 10, paddingTop: 20 }}>
                        <Text style={{ fontSize: 12, color: '#F68139', fontWeight: 'bold' }}>{groupName}</Text>
                        <Text style={{ fontSize: 16, color: '#212121', fontWeight: 'bold', marginTop: 5 }}>{name}</Text>
                        <Text style={{ marginTop: 5, fontSize: 12, color: settingApp.colorText }}>
                            <Text style={{ color: '#0279C7' }}>{fullName ? fullName : 'Lấy thông tin thất bại'} </Text>• {Utils.formatLocalTime(createdAt, 'DD/MM/YYYY hh:mm a')}
                        </Text>
                        {
                            data && data.thread ? 
                            <View style={{ flex: 1, marginTop: 20 }}>
                                <ParseText description={data.thread.content} />
                            </View>
                        : <View />
                        }
                        
                    </View>
                </ScrollView>
            </View>
        )
    }
}

export default Layout;