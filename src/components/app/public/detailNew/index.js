import Layout from './layout';
import { HaravanIc } from '../../../../Haravan';
import { connect } from 'react-redux';

class DetailNew extends Layout {
    constructor(props) {
        super(props);
        this.state={
            data: []
        }
        this.data = props.navigation.state.params.data;
        this.colleague = props.app.colleague;
    }

    async componentDidMount(){
        const result = await HaravanIc.threadDetail(this.data.id, this.colleague)
        if(result && result.data && !result.error){
            this.setState({ data: result.data})
        }
    }

}

const mapStateToProps = (state) => ({ ...state })
export default connect(mapStateToProps)(DetailNew);