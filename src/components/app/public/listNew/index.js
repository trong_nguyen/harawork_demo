import Layout from './layout';
import { HaravanIc, HaravanHr } from '../../../../Haravan';

class ListNew extends Layout {
    constructor(props) {
        super(props);

        const { data, page, pageSize, totalPage } = props.navigation.state.params;
        this.state = {
            list: data || []
        }

        this.page = page;
        this.pageSize = pageSize;
        this.totalPage = totalPage;
    }

    async loadData() {
        if (!this.totalPage || (this.totalPage >= 1 && (this.page <= this.totalPage))) {
            const { page, pageSize } = this;
            const { list } = this.state;
            const result = await HaravanIc.list_pinhome(page, pageSize);
            if (result && result.data && !result.error) {
                let data = [...list];
                for (let create of result.data.data) {
                    let createInfo = await HaravanHr.getColleague(create.createdBy);
                    if (createInfo && createInfo.data && !createInfo.error) {
                        data.push({ ...create, fullName: createInfo.data.fullName });
                    } else {
                        data.push({ ...create, fullName: null });
                    }
                }
                const { totalCount } = result.data;
                const countPage = Math.ceil(totalCount / pageSize);
                this.totalPage = countPage;
                this.page = page + 1;
                this.setState({ list: data });
            }
        }
    }

}

export default ListNew;