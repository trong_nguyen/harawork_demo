import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';
import { settingApp, ButtonCustom, Utils, HeaderScreen, AsyncImage } from '../../../../public';

class Layout extends Component {

    render() {
        return (
            <View style={{ flex: 1 }}>
                <HeaderScreen title='Bản tin' navigation={this.props.navigation} />
                <FlatList
                    data={this.state.list}
                    extraData={this.state}
                    keyExtractor={(item, index) => `${item.id}`}
                    style={{ padding: 10 }}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    contentContainerStyle={{ alignItems: 'center' }}
                    onEndReached={() => this.loadData()}
                    onEndReachedThreshold={1}
                    ListFooterComponent={<View style={{ flex: 1, height: 20 }}/>}
                    renderItem={({ item }) => {
                        return (
                            <ButtonCustom
                                onPress={() => this.props.navigation.navigate('DetailNew', { data: item })}
                                style={{
                                    width: (settingApp.width * 0.9),
                                    height: 390,
                                    ...settingApp.shadow,
                                    backgroundColor: '#ffffff',
                                    marginBottom: 10,
                                    marginTop: 10,
                                    borderRadius: 3
                                }}>
                                <View style={{ flex: 1 }}>
                                    <AsyncImage
                                        source={{ uri: item.pinHome.imageUrl }}
                                        style={{ flex: 1, width: undefined, height: undefined }}
                                        resizeMode='contain'
                                    />
                                </View>
                                <View style={{ flex: 1, padding: 10 }}>
                                    <Text style={{ fontSize: 12, color: '#F68139', fontWeight: 'bold' }}>{item.groupName}</Text>
                                    <Text style={{ fontSize: 16, color: '#212121', fontWeight: 'bold', marginTop: 5 }}>{item.name}</Text>
                                    <Text style={{ marginTop: 5, fontSize: 12, color: settingApp.colorText }}>
                                        <Text style={{ color: '#0279C7' }}>{item.fullName ? item.fullName : 'Lấy thông tin thất bại'} </Text>• {Utils.formatLocalTime(item.createdAt, 'DD/MM/YYYY hh:mm a')}
                                    </Text>
                                    <View style={{ flex: 1, marginTop: 10, flexWrap: "wrap", overflow: 'hidden' }}>
                                        <Text style={{ fontSize: 15, color: '#212121' }}>
                                            {item.pinHome.shortDescription}
                                        </Text>
                                    </View>
                                </View>
                            </ButtonCustom>
                        )
                    }}
                />
            </View>
        )
    }
}

export default Layout;