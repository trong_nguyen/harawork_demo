import { HaravanHr } from '../../../../Haravan';

export function GetBody(collegue) {
    let newList = [];
    let listID =[];
    let body= {};
    if(collegue &&  collegue.actingPositions && (collegue.actingPositions.length > 0)){
        collegue.actingPositions.map(item => {
            let content = {departmentId:item.departmentId, jobtitleName:item.jobtitleName, departmentName:item.departmentName, jobtitleId:item.jobtitleId}
            newList.push(content)
        })
    }

    if(collegue && collegue.mainPosition){
        let content = { 
            departmentId: collegue.mainPosition.departmentId, 
            departmentName: collegue.mainPosition.departmentName,
            jobtitleName: collegue.mainPosition.jobtitleName,
            jobtitleId: collegue.mainPosition.jobtitleId
        }
        newList.push(content)
    }

    if(newList && (newList.length > 0)){
        newList.map(async (item) =>{
            listID.push(item.departmentId)
            let result = await HaravanHr.getDepartmantID(item.departmentId)
            if(result && result.data && !result.error){
                if(result.data && result.data.paths && (result.data.paths.length > 0)){
                    result.data.paths.map(e =>{
                        listID.push(e.id)
                    })
                }
            }
        })
    }
    body={
        departmentIds:listID,
        positionInfos:newList,
    }
    return body
}