import React, { Component } from 'react';
import { View, Platform } from 'react-native';
import { settingApp, lottie } from '../../../../public';
import Lottie from 'lottie-react-native'

class ItemLoad extends Component {

    shouldComponentUpdate() {
        return false;
    }

    componentDidMount(){
        this.animation.play();
    }

    resetAnimation = () => {
        this.animation.reset();
        this.animation.play();
      };

    render() {
        const margin = Platform.OS === 'android' ? 10 : 5;
        return (
            <View
                style={{
                    width: (settingApp.width * 0.8),
                    height: 260,
                    backgroundColor: '#ffffff',
                    marginBottom: 10,
                    marginTop: 10,
                    marginRight: 15,
                    borderRadius: 3,
                    ...settingApp.shadow,
                }}>
                <Lottie
                    ref={animation => this.animation = animation}
                    style={{ width: ((settingApp.width * 0.7) - 20), height: 230, marginLeft: margin, marginTop: margin }}
                    source={lottie.listNewItemLoad}
                    resizeMode='cover'
                />
            </View>
        )
    }
}

export default ItemLoad;