import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native';
import { HeaderNewStyles, settingApp, SvgCP } from '../../../../public'
import { Ionicons } from '@expo/vector-icons'
import * as Animatable from 'react-native-animatable';

class Header extends Component{
    constructor(props){
        super(props)
        this.state ={
            isShow: false
        }
        this.isShow = this.props.isShow
    }

    render(){
        const { isShow, title } = this.props;
        return(
            <HeaderNewStyles 
                title={
                    (isShow && isShow == true)?
                        <Animatable.View
                            duration={500}
                            animation='fadeIn'
                            style={{ flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}
                        >
                          <Text style={{fontSize:20, color:(settingApp.colorText), fontWeight:'bold'}}> {title ? title : '' }</Text>
                        </Animatable.View>
                        : <View />
                    }
                buttonRight={
                        <TouchableOpacity
                            onPress={() => this.props.isSeacrh()}
                            activeOpacity={1}
                            style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
                        >
                            <SvgCP.iconSeach />
                        </TouchableOpacity>
                    }
            />
        )
    }
}
export default Header;