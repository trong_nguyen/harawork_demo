import React, { Component } from 'react';
import { View, Text, FlatList , Animated ,ScrollView , TouchableOpacity} from 'react-native';
import { settingApp, SvgCP , HeaderScreenNews} from '../../../../public'
import Header from '../components/header';
import ListHeader from './components/listHeader';
import ListContent from './components/listContent';

class Layout extends Component{

renderHeader(){
    const {isShow} = this.state
    let titleHeader = isShow ? 'Bảng tin' :''
    return(
        <HeaderScreenNews 
            navigation={this.props.navigation}
            title={titleHeader}
        />
    )
}


render(){
    return(
        <View style={{flex:1}}>
            {/* <Header 
                isShow = {this.state.isShow}
                title= 'Bảng tin'
                isSeacrh={() => this.isSeacrh()}
            /> */}
            {this.renderHeader()}
                <View style={{ flex: 1 , backgroundColor:'#FFFFFF'}}>
                    <ScrollView
                        scrollEventThrottle={1}
                        onScroll ={ Animated.event([
                            {nativeEvent:{ contentOffset: { y : this.state.scrollY }}}
                        ])}
                    >
                        <View style={{width:(settingApp.width), height:60, backgroundColor:'#FFFFFF', flexDirection:'row', alignItems:'center', justifyContent:'flex-start', paddingLeft:10}}>
                            <SvgCP.iconNews />
                            <Text style={{color:settingApp.colorText, fontSize:36, paddingLeft:10, fontWeight:'bold'}}>Bảng tin</Text>
                        </View>
                        
                        <View style={{paddingBottom:10, paddingTop:10}}>
                            <ListContent 
                                navigation = { this.props.navigation}
                            />
                        </View>
                        <View style={{ flex: 1, height: 20 }}/>
                    </ScrollView>
                </View>
        </View>
    )
}
}
export default Layout;