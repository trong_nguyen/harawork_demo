import Layout from './layout';
import { Animated } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../state/action';

class News extends Layout{
    constructor(props){
        super(props)
        this.state={
           scrollY: new Animated.Value(0),
           isShow: false,
        }

        this.collegue = props.app.colleague;
        this.isShow = true
    }

    componentDidMount(){
        this.state.scrollY.addListener(({value}) =>{
            if(value > 45 && (this.isShow !== this.state.isShow)){
                this.setState({isShow:true})
            }else if(value < 45&& (this.state.isShow === this.isShow)){
                this.setState({isShow:false})
            }
        })
    }
    isSeacrh(){}


}
const mapStateToProps = (state) => ({ ...state })
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(News);