import React, { Component } from 'react';
import { settingApp } from '../../../../../../public';
import { View, Text, FlatList } from 'react-native';
import Item from './component/item';
import ItemLoad from '../../../components/itemLoad'

class Layout extends Component{

    render(){
        const { listHeader, listLoad , isLoading} = this.state;
        if(!isLoading){
            return(
                <View style={{ flex:1}}>
                    <FlatList 
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        horizontal={true}
                        data={listHeader}
                        extraData={this.state}
                        keyExtractor={(item, index) =>(index+'')}
                        renderItem={(obj) => this.renderItem(obj)}
                        onEndReachedThreshold={1}
                        onEndReached={() => this.loadMore()}
                    />
                </View>
            )
        }
        else{
            return(
                <View style={{ flex:1}}>
                    <FlatList 
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        horizontal={true}
                        data={listLoad}
                        extraData={this.state}
                        keyExtractor={(item, index) =>(index+'')}
                        renderItem={(obj) => this.renderItemLoad(obj)}
                        onEndReachedThreshold={1}
                        onEndReached={() => this.loadMore()}
                    />
                </View>
            )
        }
    }

    renderItemLoad(){
        return(
            <ItemLoad />
        )
    }

    renderItem(obj){
        const { isLoading} = this.state
        const { item, index } = obj
            return(
                <Item
                    item ={item}
                    navigation ={ this.props.navigation }
                />
            )
    }
}
export default Layout;