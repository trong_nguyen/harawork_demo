import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { settingApp, AsyncImage } from '../../../../../../../public'
import moment from 'moment';

class Item extends Component{
    constructor(props){
        super(props)
        this.state={

        }
    }

    formatTime(time){
        let newtime = null
        let timeCheck = moment(time).fromNow();
        if(timeCheck.indexOf('trước') > -1){
            newtime=  timeCheck.replace('trước', '').trim()
        }
        else if(timeCheck.indexOf('ago') > -1){
            newtime= timeCheck.replace('ago', '').trim()
        }
        else{
            newtime = timeCheck
        }
        return newtime
    }

    render(){
        const { item } = this.props;
        let time = this.formatTime(item.createdAt)
        return(
            <TouchableOpacity 
                onPress={() => this.props.navigation.navigate('DetailComment' ,{data:item})}
                style={{width:(settingApp.width *0.8), height:300, ...settingApp.shadow, margin:10, borderRadius:5, backgroundColor:'#FFFFFF'}}>
                <View style={{ width:(settingApp.width*0.8), height:220 }}>
                    <AsyncImage
                        source={{ uri: item.pinHome.imageUrl }}
                        style={{ flex: 1, width: undefined, height: undefined }}
                        resizeMode='contain'
                    />
                </View>
                <View style={{ padding:10, overflow: 'hidden'  }}>
                    <Text
                        numberOfLines={3}
                        style={{fontSize:14, color:settingApp.colorText, fontWeight:'800' }}>{item.pinHome.shortDescription}</Text> 
                </View>
            </TouchableOpacity>
        )
    }
}
export default Item;