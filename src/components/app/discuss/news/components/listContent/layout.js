import React, { Component } from 'react';
import { settingApp } from '../../../../../../public';
import { View, Text, FlatList, Platform } from 'react-native';
import Item from './component/item';
import ItemLoad from './component/itemLoad'

class Layout extends Component{

    render(){
        const { listContent, isLoading, listLoad } = this.state;
        if (!isLoading){
            return(
                <View style={{ flex:1}}>
                    <FlatList 
                        data={listContent}
                        extraData={this.state}
                        keyExtractor={(item, index) =>(index+'')}
                        renderItem={(obj) => this.renderItem(obj)}
                        onEndReachedThreshold={1}
                        scrollEventThrottle={16}
                        onEndReached={() => this.loadMore()}
                    />
                </View>
            )
        }
        else{
            return(
                <View style={{ flex:1}}>
                    <ItemLoad />
                </View>
            )
        }
        
    }

    renderItem(obj){
        const { item, index } = obj
        return(
            <Item
                item ={item}
                index={index}
                navigation ={ this.props.navigation }
            />
        )
    }

    renderItemLoad(obj){
        const { item, index } = obj
        return(
            <ItemLoad
                item ={item}
                index={index}
                navigation ={ this.props.navigation }
            />
        )
    }
}
export default Layout;