import React, { Component } from 'react';
import Layout from './layout'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../../../state/action';
import { HaravanIc, HaravanHr } from '../../../../../../Haravan';

class ListContent extends Layout {
    constructor(props){
        super(props)
        this.state={
            listLoad: [
                { id: 0, value: null }, 
                { id: 1, value: null }, 
                { id: 2, value: null }, 
                { id: 3, value: null }, 
                { id: 4, value: null }, 
                { id: 5, value: null }
            ],
            listContent: [],
            loadMore:false,
            isLoading:true,
        }

        this.collegue = props.app.colleague;
        this.page = 1;
        this.totalPage = null;
        this.currentList = [];
        this.listCreateBy = [];

        this.checkLoadMore = false    
    }

    componentDidMount(){
        this.loadData()
    }

    checkResult(input){
        if(input && input.data && !input.error){
            return input
        }
        else {
            return null
        }
    }

    loadMore(){
        if(!this.state.loadMore){
            if(this.page <= this.totalPage){
                //this.initialNumberRender = this.initialNumberRender + 20
                this.setState({loadMore:true}, () =>{
                    this.checkLoadMore =  false
                    this.loadData()
                })
            }
        }
    }

    async loadData(){
        const { currentList, page, totalPage } = this
        let newList = currentList;
        if (!totalPage || (totalPage && !isNaN(totalPage) && page <= totalPage)) {
            let result = await HaravanIc.GetListNews(page)
            result = this.checkResult(result)
            if( result ){
                let data = result.data.data
                for(let i = 0; i < data.length; i++){
                    const index = data.findIndex(e => e.createBy == this.collegue.haraId)
                    if(index > -1){
                        let user = {...data[i], fullName:this.collegue.fullName}
                        newList.push(user)
                    }
                    else{
                        let resulUser = await HaravanHr.getColleague(data[i].createdBy)
                        resulUser = this.checkResult(resulUser)
                        if(resulUser){
                            let user = {...data[i], fullName:resulUser.data.fullName}
                            newList.push(user)
                        }
                        else{
                            newList.push(data[i])
                        }
                    }
                }
                this.currentList = newList
                this.setState({listContent: newList }, () =>{
                    this.setState({isLoading: false, loadMore:false})
                    isCheck = false;
                })
            }
            else{
                this.setState({isLoading: false})
            }
            
            let count = result.data.totalCount
            this.page = page + 1;
            this.totalPage = Math.ceil(count / 20);
        }
        else {
            this.setState({
                isLoading: false,
                loadMore:false,
            });
        }
        
    }

}
const mapStateToProps = (state) => ({ ...state })
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(ListContent);