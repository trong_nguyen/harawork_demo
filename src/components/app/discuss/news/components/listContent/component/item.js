import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native';
import { settingApp, AsyncImage, imgApp } from '../../../../../../../public'
import moment from 'moment';
import { Entypo } from '@expo/vector-icons'

class Item extends Component{
    constructor(props){
        super(props)
        this.state={

        }
    }

    formatTime(time){
        let newtime = null
        let timeCheck = moment(time).fromNow();
        if(timeCheck.indexOf('trước') > -1){
            newtime=  timeCheck.replace('trước', '').trim()
        }
        else if(timeCheck.indexOf('ago') > -1){
            newtime= timeCheck.replace('ago', '').trim()
        }
        else{
            newtime = timeCheck
        }
        
        return newtime
    }
    
    renderItemTop(item){
        let time = this.formatTime(item.createdAt)
        return(
            <TouchableOpacity 
                onPress={() => this.props.navigation.navigate('DetailComment' ,{data:item})}
                style={{width:settingApp.width,  minHeight:310, justifyContent:'center', alignItems:'center', backgroundColor:'transparent'}}
                >
                <View style={styles.itemTop}>
                    <View style={{ width:(settingApp.width*0.9), height:220 ,overflow:'hidden', borderTopLeftRadius:8, borderTopRightRadius:8}}>
                        <AsyncImage
                            source={{ uri: item.banner ? item.banner : imgApp.noImage }}
                            style={{ flex: 1, width:(settingApp.width*0.9), height:220}}
                            //resizeMode='contain'
                        />
                    </View>
                    <View style={{ padding:10, overflow: 'hidden'  }}>
                        <Text 
                            numberOfLines={3}
                            style={styles.textDescirf}>{item.description}</Text>
                        {/* <Text
                            numberOfLines={3}
                            style={[styles.textDescirf,{width:(settingApp.width - 30),}]}>{item.name}</Text> 
                        <View style={{ flexDirection:'row', 
                                width:(settingApp.width - 20), 
                                paddingBottom:10}}> 
                            <Text style={{color:(settingApp.color), fontSize:14, flex:3/5}}>{item.fullName}</Text> 
                            <Text style={{flex:2/5, fontSize:14, color:settingApp.colorDisable}}>{time}</Text> 
                        </View> */}
                    </View>
                </View>
            </TouchableOpacity>

        )
    }

    renderItem(item){
        let time = this.formatTime(item.createdAt)
        return(
            <TouchableOpacity 
            onPress={() => this.props.navigation.navigate('DetailComment', {data:item})}
            style={styles.itemContent}>
                <View style={styles.coverIMG}>
                    <AsyncImage
                        source={{ uri:  item.banner ? item.banner : imgApp.noImage }}
                        style={{flex: 1,
                                width:(settingApp.width*0.3), 
                                borderRadius:8,
                                height:(settingApp.width*0.3), 
                                }}
                    />
                </View>
                <View style={{marginLeft:15}}>
                    <Text style={styles.textNameGroups}>{item.groupName} </Text>
                    <Text 
                        numberOfLines={2}
                        style={[styles.textDescirf,{width:(settingApp.width - 150)}]}>{item.name} </Text>
                    
                    <Text 
                        numberOfLines={2}
                        style={{width:(settingApp.width - 150), fontSize:14, color:settingApp.colorText, paddingBottom:5}}>{item.description} </Text>
                    
                    <View style={styles.coverUser}> 
                        <Text 
                            numberOfLines={1}
                            style={{color:(settingApp.colorDisable), fontSize:14, paddingRight:5,maxWidth:(settingApp.width*0.3)}}>{item.fullName}</Text> 
                        <Entypo name='dot-single' color={settingApp.colorDisable} size={15} />
                        <Text style={{ fontSize:14, color:settingApp.colorDisable, paddingLeft:5}}>{time}</Text> 
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    render(){
        const { item, index } = this.props;
        let content = <View />
        let time = this.formatTime(item.createdAt)
        content = index == 0 ? this.renderItemTop(item) : this.renderItem(item);
        return(
            <View>
                {content}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    itemTop:{
        width:(settingApp.width * 0.9), 
        minHeight:300, 
        ...settingApp.shadow, 
        borderRadius:8, 
        backgroundColor:'#FFFFFF', 
        justifyContent:'center', 
        alignItems:'center', 
        marginBottom:40
    },
    itemContent:{
        width:(settingApp.width-20), 
        minHeight:(settingApp.width*0.3), 
        flexDirection:'row',
        marginBottom:20
    },
    textDescirf:{
        color:(settingApp.colorText), 
        fontSize:16, 
        fontWeight:'bold', 
        paddingBottom:5
    },
    textNameGroups:{
        color:(settingApp.colorDisable), 
        fontSize:12, 
        fontWeight:'bold', 
        paddingBottom:5
    },
    coverIMG:{
        width:(settingApp.width*0.3), 
        height:(settingApp.width*0.3), 
        marginLeft:15,
        borderRadius:8
    },
    coverUser:{
        flexDirection:'row', 
        width:(settingApp.width - 90), 
        paddingBottom:10
    }
})
export default Item;