import React, { Component } from 'react';
import Layout from './layout'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../../../state/action';
import { HaravanIc, HaravanHr } from '../../../../../../Haravan';
import * as Utils from '../../../components/Utils'

class ListHeader extends Layout {
    constructor(props){
        super(props)
        this.state={
            listHeader: [],
            loadMore:false,
            isLoading:true,
            listLoad: [{ id: 0, value: null }, { id: 1, value: null }]
        }

        this.collegue = props.app.colleague;
        this.page = 1;
        this.totalPage = null;
        this.currentList = [];
        this.listCreateBy = [];
    }

    componentDidMount(){
        this.loadData()
    }

    checkDelete(value){
        let { listHeader } = this.state
        if(value.status == true){
            const index = listHeader.findIndex(m => (m.id == value.data.thread.id))
            if(index > -1){
                listHeader.splice(index, 1)
                this.setState({listHeader})
            }
        }
    }

    checkResult(input){
        if(input && input.data && !input.error){
            return input
        }
        else {
            return null
        }
    }

    loadMore(){
        if(!this.state.loadMore){
            if(this.page <= this.totalPage){
                this.setState({loadMore:true}, () =>{
                    this.loadData()
                })
            }
        }
    }

    async loadData(){
        let { listHeader} = this.state
        let newList = listHeader ;
        let checkBody = await Utils.GetBody(this.collegue)
        let result = await HaravanIc.GetListInteresting(checkBody)
        result = this.checkResult(result)
        if( result ){
            let data = result.data
            for(let i = 0; i < data.length; i++){
                if(data[i].createBy == this.collegue.haraId){
                    let user = {...data[i], fullName:this.collegue.fullName}
                    newList.push(user)
                }
                else{
                    let resulUser = await HaravanHr.getColleague(data[i].createdBy)
                    resulUser = this.checkResult(resulUser)
                    if(resulUser){
                        let user = {...data[i], fullName:resulUser.data.fullName}
                        newList.push(user)
                    }
                    else{
                        newList.push(data[i])
                    }
                }
            }
            this.setState({listHeader: newList }, () =>{
                this.setState({isLoading: false, loadMore:false})
            })
        }
        else{
            this.setState({isLoading: false})
        }
    }

}
const mapStateToProps = (state) => ({ ...state })
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(ListHeader);