import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native';
import { settingApp, AsyncImage, ButtonCustom, imgApp, SvgCP } from '../../../../../../../public'
import moment from 'moment';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../../../../state/action';
import image from '../../../../../../../public/image';

class Item extends Component{
    constructor(props){
        super(props)
        this.state={
            imgUrl:null
        }
    }

    componentDidMount(){
        const { item } = this.props
        if(item.banner){
            this.setState({
                imgUrl:item.banner
            })
        }
        else{
            this.setState({ imgUrl:null })
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps && nextProps.discuss && nextProps.discuss.changeImage){
            let {url, data} = nextProps.discuss.changeImage
            if(data && (data.thread.id == this.props.item.id)){
                this.setState({imgUrl:url})
            }
        }
    }

    formatTime(time){
        let newtime = null
        let timeCheck = moment(time).fromNow();
        if(timeCheck.indexOf('trước') > -1){
            newtime=  timeCheck.replace('trước', '').trim()
        }
        else if(timeCheck.indexOf('ago') > -1){
            newtime= timeCheck.replace('ago', '').trim()
        }
        else{
            newtime = timeCheck
        }
        return newtime
    }

    render(){
        const { item } = this.props;
        let time = this.formatTime(item.createdAt)
        const { imgUrl } = this.state;
        return(
            <ButtonCustom 
                onPress={() => this.props.navigation.navigate('DetailComment' ,{
                    data:item,
                    delete:(value) => this.props.delete(value)
                    })}
                style={{width:(settingApp.width *0.8), height:300, ...settingApp.shadow, margin:10, borderRadius:8, backgroundColor:'#FFFFFF'}}>
              
                <View style={{ flex: 1 ,width:(settingApp.width*0.8), height:180,borderTopLeftRadius:8, borderTopRightRadius:8, overflow:'hidden'}}>
                    <AsyncImage
                        source={{ uri: imgUrl ? imgUrl : '' }}
                        style={{ flex: 1, borderTopLeftRadius:8, borderTopRightRadius:8, width:(settingApp.width*0.8), height:180,}}
                        //resizeMode='contain'
                    /> 
                </View>
                <View style={{paddingBottom:15, paddingRight:15, paddingLeft:15, height:110, justifyContent:'space-between'}}>
                    <View style={{ padding:5, minHeight:30}}>
                        <Text
                            numberOfLines={2}
                            style={{fontSize:14, color:settingApp.colorText, fontWeight:'800'}}>{item.name}</Text> 
                        <Text
                            numberOfLines={1}
                            style={{fontSize:14, color:settingApp.blueSky, paddingTop:4 }}>{item.fullName}</Text> 
                    </View>

                    <View style={{ padding:5, overflow: 'hidden', flexDirection:'row',  height:20,  alignItems:'center'}}>
                        <View style={styles.viewActions}>
                            <SvgCP.iconComment />
                            <Text style={[styles.text, {paddingLeft:5,color:settingApp.colorText,}]}>{item.countComment}</Text>
                        </View>
                        <View style={[styles.viewActions,{marginLeft:15}]}>
                            <SvgCP.iconAcions />
                            <Text style={[styles.text, {paddingLeft:5,color:settingApp.colorText,}]}>{item.countReaction}</Text>
                        </View>
                        <View style={[styles.viewActions,{marginLeft:15}]}>
                            <Text style={[styles.text, {paddingLeft:5,color:settingApp.colorDisable,}]}>{time}</Text>
                        </View>
                    </View>
                </View>
            </ButtonCustom>
        )
    }
}

const styles =StyleSheet.create({
    text:{
        fontSize:12,
        paddingLeft:2
    },
    viewActions:{
        height:20, 
        flexDirection:'row', 
        justifyContent:'center', 
        alignItems:'center',
        
    }
})
const mapStateToProps = (state) => ({ ...state })
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(Item);