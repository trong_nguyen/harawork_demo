import React, { Component } from 'react';
import { View, ScrollView, Text, TouchableOpacity } from 'react-native';
import  { settingApp } from '../../../../../public'
import ListContent from './listContent'

class TabContent extends Component{
    constructor(props){
        super(props)
        this.state={
            page:1,
        }
    }

    renderButton(value, title){
        let check = value == this.state.page
        return(
            <TouchableOpacity
                onPress={() => this.setState({page:value})}
                style={{
                    width:120, 
                    height:40, 
                    backgroundColor: check ? settingApp.blueSky :'#FFFFFF',
                    borderRadius:20,
                    justifyContent:'center',
                    alignItems:'center'
                    }}
                >
                <Text style={{color:check? '#FFFFFF': settingApp.colorDisable, fontSize:14, fontWeight:'bold'}}>{title}</Text>
            </TouchableOpacity>
        )
    }

    renderTab(){
        return(
            <View style={{
                width:settingApp.width, 
                height:44, 
                backgroundColor:'#FFFFFF', 
                flexDirection:'row',
                justifyContent:'center'
                }}>
                {this.renderButton(1, 'Quan tâm')}
                {this.renderButton(2, 'Khám phá')}
            </View>
        )
    }

    render(){
        let { page } = this.state
        return(
            <View style={{flex:1}}>
                {this.renderTab()}
                {page== 1 && <ListContent 
                        navigation={this.props.mainNavigation}
                        type='cares'
                        count = {1}
                    />}

                {page == 2 &&  <ListContent
                        navigation={this.props.mainNavigation}
                        type='discovery'
                        count = {2}
                    />}
            </View>
        )
    }
}
export default TabContent;