import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { settingApp, AsyncImage, Utils } from '../../../../../../../public'
import moment from 'moment';
import { MaterialCommunityIcons, Entypo } from '@expo/vector-icons'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../../../../state/action';

class Item extends Component{
    constructor(props){
        super(props)
        this.state={
            isRead: false
        }
    }

    componentDidMount(){
        const { item } = this.props
        if(item){
            this.setState({ isRead:item.isReaded })
        }
    }

    checkRead(value, type){
        const { item } = this.props
        if(value && value.thread && (value.thread.id === item.id)){
            this.setState({isRead:type})
        }
    }

    formatTime(time){
        let newtime = null
        let timeCheck = moment(time).fromNow();
        if(timeCheck.indexOf('trước') > -1){
            newtime=  timeCheck.replace('trước', '').trim()
        }
        else if(timeCheck.indexOf('ago') > -1){
            newtime= timeCheck.replace('ago', '').trim()
        }
        else{
            newtime = timeCheck
        }
        return newtime
    }

    checkTime(time, value){
        let newTime = null
        if(time === 'một tháng'){
            newTime = Utils.formatTime(value, 'DD/MM/YYYY', true)
        }
        else{
            newTime = time
        }
        return newTime
    }

    render(){
        const { item, index } = this.props;
        const { isRead } = this.state
        let time = this.formatTime(item.createdAt)
        let checkTime = this.checkTime(time, item.createdAt)
        return(
            <TouchableOpacity 
            onPress={() => this.props.navigation.navigate('DetailComment', {
                data:item,
                actions:(value, type) => this.checkRead(value, type),
                delete:(value) => this.props.delete(value)
                })}
            style={{width:(settingApp.width), minHeight:(settingApp.width*0.20), flexDirection:'row', marginBottom:16}}>
                <View style={{ flex:1 , paddingTop:16}}>
                    <Text style={{color:(settingApp.colorDisable), fontSize:12, fontWeight:'bold', paddingBottom:5, maxWidth:(settingApp.width - 20),}}>{item.groupName}  </Text>
                    <View style={{ width:(settingApp.width - 50), alignItems:'center', flexDirection:'row'}}>
                        <Text 
                            numberOfLines={2}
                            style={{color:(settingApp.colorText), fontSize:16, maxWidth:(settingApp.width - 60), fontWeight:'bold', paddingBottom:5, paddingRight:5}}>{item.name + ' '} 
                            {(!isRead) && <MaterialCommunityIcons name='circle'  size={10} color="#FC625D" /> }
                        </Text>
                        
                    </View>
                    <View style={{flexDirection:'row', width:(settingApp.width - 90), paddingBottom:6}}> 
                        <Text style={{color:(settingApp.colorDisable), fontSize:14, paddingRight:5 }}>{item.fullName}</Text> 
                        <Entypo name='dot-single' color={settingApp.colorDisable} size={15} />
                        <Text style={{fontSize:14, color:settingApp.colorDisable, paddingLeft:5}}>{checkTime}</Text> 
                    </View>
                </View> 
            </TouchableOpacity>
        )
    }
}
export default Item;