import React, { Component } from 'react';
import { View, Platform } from 'react-native';
import { settingApp, lottie } from '../../../../../../../public';
import Lottie from 'lottie-react-native'

class ItemLoad extends Component {

    shouldComponentUpdate() {
        return false;
    }

    componentDidMount(){
        this.animation.play();
    }

    resetAnimation = () => {
        this.animation.reset();
        this.animation.play();
      };

    render() {
        const { index } = this.props
        const margin = Platform.OS === 'android' ? 10 : 5;
        return (
            <View
                style={{
                    width: settingApp.width,
                    height:settingApp.height,
                    backgroundColor: '#ffffff',
                    marginBottom: 10,
                    marginTop: 10,
                    marginRight: 15,
                    borderRadius: 3,
                }}>
                <Lottie
                    ref={animation => this.animation = animation}
                    style={{ 
                        flex:1
                     }}
                    source={lottie.listItemLoadFrame}
                    resizeMode='contain'
                />
            </View>
        )
    }
}

export default ItemLoad;