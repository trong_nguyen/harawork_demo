import React, { Component } from 'react';
import Layout from './layout'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../../../state/action';
import { HaravanIc, HaravanHr } from '../../../../../../Haravan';
import * as Utils from '../../../components/Utils'

class ListContent extends Layout {
    constructor(props){
        super(props)
        this.state={
            listLoad: [
                { id: 0, value: null }, 
                { id: 1, value: null }, 
                { id: 2, value: null }, 
                { id: 3, value: null }, 
                { id: 4, value: null }, 
                { id: 5, value: null }
            ],
            listContent: [],
            loadMore:false,
            isLoading:true,
            visible:false,
            isNews:false,
            isCount:0,
            typeList:1,
            title:'Tất cả'
        }
        this.type = props.type
        this.count = props.count

        this.collegue = props.app.colleague;
        this.page = 1;
        this.totalPage = null;
        this.currentList = [];
        this.listCreateBy = [];
        this.navigation = props.navigation
        this.checkLoadMore = false    
    }

    componentDidMount(){
        this.loadData()
        this.loadCount()
    }

    checkDelete(value){
        let { listContent } = this.state
        if(value.status == true){
            const index = listContent.findIndex(m => (m.id == value.data.thread.id))
            if(index > -1){
                listContent.splice(index, 1)
                this.setState({listContent})
            }
        }
    }

    checkResult(input){
        if(input && input.data && !input.error){
            return input
        }
        else {
            return null
        }
    }

    reloadList(value, type, title){
        if(type != this.state.typeList){
            this.setState({loadData:[], isLoading:true, typeList:type, isNews:value, title:title},() =>{
                this.page = 1;
                this.totalPage = null;
                this.currentList = [];
                this.listCreateBy = [];
                this.loadData()
            })
        }
    }

    loadMore(){
        if(this.page <= this.totalPage){
            this.setState({loadMore:true}, () =>{
                this.checkLoadMore =  false
                this.loadData()
            })
        }
    }

    async loadData(){
        const { currentList, page, totalPage, type } = this
        const { isNews } = this.state
        let body = await Utils.GetBody(this.collegue)
        let newList = currentList;
        if (!totalPage || (totalPage && !isNaN(totalPage) && page <= totalPage)) {
            let result = await HaravanIc.GetListTopicCare(type, isNews, body, page)
            result = this.checkResult(result)
            if( result ){
                let data = result.data.data
                for(let i = 0; i < data.length; i++){
                    //const index = data.findIndex(e => e.createdBy == this.collegue.haraId)
                    if(data[i].createdBy === this.collegue.haraId){
                        let user = {...data[i], fullName:this.collegue.fullName}
                        newList.push(user)
                    }
                    else{
                        let resulUser = await HaravanHr.getColleague(data[i].createdBy)
                        resulUser = this.checkResult(resulUser)
                        if(resulUser){
                            let user = {...data[i], fullName:resulUser.data.fullName}
                            newList.push(user)
                        }
                        else{
                            newList.push(data[i])
                        }
                    }
                }
                this.currentList = newList
                let count = result.data.totalCount
                this.page = page + 1;
                this.totalPage = Math.ceil(count / 20);
                this.setState({listContent: newList }, () =>{
                    this.setState({isLoading: false, loadMore:false})
                    isCheck = false;
                })
            }
            else{
                this.setState({isLoading: false})
            }
        }
        else {
            this.setState({
                isLoading: false,
                loadMore:false,
            });
        }
        
    }

    async loadCount(){
        const { collegue, count } = this
        let body = await Utils.GetBody(collegue)
        let result = await HaravanIc.GetListCount(count, body)
        result = this.checkResult(result)
        if(result){
            this.setState({isCount:result.data.totalCount})
        }
        else{
            this.setState({isCount:0})
        }
    }
}
const mapStateToProps = (state) => ({ ...state })
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(ListContent);