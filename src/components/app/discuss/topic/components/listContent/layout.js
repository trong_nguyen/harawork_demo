import React, { Component } from 'react';
import { settingApp, Loading, iconGroups} from '../../../../../../public';
import { View, Text, FlatList, Platform, ActivityIndicator, TouchableOpacity } from 'react-native';
import Item from './component/item';
import Modal from '../modalOption';
import { AntDesign } from '@expo/vector-icons'

class Layout extends Component{

    renderLoadMore(){
        const {loadMore}= this.state;
        return(
            <View style={{flex:1, height:50, backgroundColor:'#FFFFFF'}}>
                {
                   (!loadMore )?
                    <TouchableOpacity 
                        onPress={() => this.setState({loadMore:true}, () =>{
                            this.loadMore()
                        })}
                        style={{flex:1, height:50, backgroundColor:'#FFFFFF', justifyContent:'center', alignItems:'center'}}>
                        <Text style={{fontSize:14, color:settingApp.blueSky}}>Xem thêm</Text>
                    </TouchableOpacity>
                    :
                    <ActivityIndicator size='small' color={settingApp.color}/>
                }
            </View>
        )
    }

    renderOption(){
        const { title } = this.state
        return(
            <TouchableOpacity 
                onPress ={() => this.setState({visible:true})}
                style={{flex:1, height:30, justifyContent:'center', alignItems:'center', flexDirection:'row'}}>
                <Text style={{fontSize:14, color:settingApp.blueSky, paddingRight:5}}>{title}</Text>
                <AntDesign size={10} color={settingApp.blueSky} name='down' />
            </TouchableOpacity>
        )
    }
    renderContent(){
        const { listContent} = this.state;
        if(listContent.length > 0){
            return(
                <FlatList 
                    data={listContent}
                    extraData={this.state}
                    keyExtractor={(item, index) =>(index+'')}
                    renderItem={(obj) => this.renderItem(obj)}
                    onEndReachedThreshold={1}
                    scrollEventThrottle={16}
                    //onEndReached={() => this.loadMore()}
                    ListFooterComponent={(this.page < this.totalPage) &&this.renderLoadMore()}
                />
            )
        }
        else{
            return(
                <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                    <iconGroups.ArtWorks/>
                    <Text style={{ textAlign:'center',fontSize:18, color:settingApp.colorText, fontWeight:'600', marginTop:10}}
                    >{`Rất tiếc, hiện chưa có \n chủ đề nào`}</Text>
                </View>
            )
        }
    }

    renderLoading(){
        return(
            <View style={{flex:1, marginTop:54}}>
                <Loading/>
            </View>
        )
    }

    render(){
        let content = <View/>
        const { isLoading, visible } = this.state;
        content = !isLoading ? this.renderContent() : this.renderLoading()
        return(
            <View style={{ flex:1, padding:10}}>
                {!isLoading && this.renderOption()}
                {content}
                <Modal 
                    visible={visible}
                    close={() => this.setState({visible:false})}
                    setIsNews= {(value, type, title) => this.reloadList(value, type, title)}
                    isCount = {this.state.isCount}
                />
            </View>
        )
    }

    renderItem(obj){
        const { item, index } = obj
        return(
            <Item
                item ={item}
                index={index}
                navigation ={ this.navigation }
                delete={value => this.checkDelete(value)}
            />
        )
    }
}
export default Layout;