import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Animated, Modal, Alert, Platform } from 'react-native';
import * as Animatable from 'react-native-animatable';
import {  settingApp } from '../../../../../public'

class ModalOption extends Component{
    constructor(props){
        super(props)
        const bottom = -(settingApp.height * 0.5 + 10);
        this.state={
            isOpen: false,
            bottom: new Animated.Value(bottom),
            item: null
        }
    }

    componentWillReceiveProps(nextProps) {
        // if(nextProps.item){
        //     this.setState({item: nextProps.item})
        // }
    }
    componentDidMount(){
        this.open()
    }

    open() {
        Animated.timing(
            this.state.bottom,
            {   delay:500,
                toValue: 0,
                duration: 250
            }
        ).start();
    }

    close() {
        Animated.timing(
            this.state.bottom,
            {
                toValue: -(settingApp.height * 0.5 + 10),
                duration: 120
            }
        ).start(() => {
            this.props.close()
        });
    }

    checkType(type, title){
        if(type == 2){
            this.props.setIsNews(true, type, title)
            this.props.close()
        }
        else{
            this.props.setIsNews(false, type, title)
            this.props.close()
        }
    }

    renderButton(title, type){
        const { isCount } = this.props;
        let count =  (isCount > 99) ? '99+' : isCount;
        return(
            <TouchableOpacity
                activeOpacity={0.8}    
                onPress={() => {
                    this.checkType(type, title)
                }}
                style={[Platform.OS == 'ios'? styles.lineIos : styles.lineAndroid, {
                        borderBottomWidth: type != 2 ? 1 : 0 ,
                        borderBottomColor:settingApp.colorSperator,
                        flexDirection:'row'
                    }]} 
            >
            <Text style={{fontSize:17, color:settingApp.blueSky }}>{title}</Text>
            {type ==2 && 
                <View style={{borderRadius:15, height:25,minWidth:25, backgroundColor:'#FC625D', justifyContent:'center', alignItems:'center', marginLeft:5}}>
                    <Text style={{fontSize:12, color:'#FFFFFF', padding:5}}>{count}</Text>
                </View>
            }
        </TouchableOpacity>
        )
    }

    renderLine(){
        return(
            <View                 
                style={[styles.viewLine, {
                    width: Platform.OS=='ios' ? (settingApp.width*0.95) : (settingApp.width),
                    borderRadius: Platform.OS=='ios' ? 10 : 0,
                    } ]}
            >
                {this.renderButton('Tất cả', 1)}
                {this.renderButton('Mới cập nhật', 2)}
            </View>
        )
    }

    renderCancel(){
        return(
            <TouchableOpacity 
                activeOpacity={0.8}
                onPress={() => {
                    this.Animatable.props.animation="fadeInDownBig"
                    this.props.close()
                    }}
                style={[Platform.OS =='ios' ? styles.buttonIos : styles.buttonAndroid,{
                    borderRadius:Platform.OS =='ios' ? 10 : 0, 
                }]}
                 >
                <Text  style={{fontSize:16, color:settingApp.blueSky, fontWeight:'600'}}>Hủy</Text>
            </TouchableOpacity>
        )
    }

    render(){
        const { bottom } = this.state;
        const { height } = settingApp;
        let { item } = this.state
        return(
            <Modal
                ref={refs => this.Modal = refs}
                animationType="fade"
                transparent={true}
                visible={this.props.visible}
                onRequestClose={() => this.close()}>
            
            <TouchableOpacity
                onPress={() => this.props.close()}
                activeOpacity={1}
                style={{
                    flex: 1,
                    backgroundColor: 'rgba(0,0,0,0.5)',
                }}
            />
            <TouchableOpacity 
                onPress={() => this.props.close()}
                activeOpacity={1}
                style={{flex:1, backgroundColor:'rgba(0,0,0,0.5)', justifyContent:'flex-end'}}>
                <Animatable.View
                    ref={refs => this.Animatable = refs}
                    animation="fadeInUpBig"
                    delay={120}
                    duration={120}
                    style={{
                        width:settingApp.width,
                        height: (height*0.5),
                        backgroundColor: 'transparent',
                        justifyContent:'flex-end',
                        alignItems:'center',
                        ...settingApp.shadow
                    }}
                >   
                    {this.renderLine()}
                    {this.renderCancel()}
                </Animatable.View>
            </TouchableOpacity>
        </Modal>
        )
    }
}
const styles ={
    lineAndroid:{
        width:(settingApp.width),  
        backgroundColor:'#FFFFFF', 
        height:54,
        ...settingApp.shadow, 
        justifyContent:'center', 
        alignItems:'center'
    },
    buttonAndroid:{
        height:54,
        width:(settingApp.width), 
        justifyContent:'center', 
        alignItems:'center',
        borderRadius:10,
        backgroundColor:'#FFFFFF', 
        marginTop:10
    },
    lineIos:{
        width:(settingApp.width*0.92),  
        height:54,
        borderRadius:10,
        backgroundColor:'#FFFFFF', 
        justifyContent:'center', 
        alignItems:'center'
    },
    buttonIos:{
        height:54,
        width:(settingApp.width*0.95), 
        justifyContent:'center', 
        alignItems:'center',
        borderRadius:10,
        marginTop:10,
        marginBottom: 10,
        backgroundColor:"#FFFFFF"
    },
    viewLine:{
        width:(settingApp.width*0.95),  
        backgroundColor:'#FFFFFF', 
        ...settingApp.shadow, 
        justifyContent:'center', 
        alignItems:'center'
    }
}
export default ModalOption;