import Layout from './layout';
import { Animated } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../state/action';
import { HaravanIc, HaravanHr } from '../../../../Haravan';
import * as Utils from '../components/Utils'

class News extends Layout{
    constructor(props){
        super(props)
        this.state={
           scrollY: new Animated.Value(0),
           isShow: false,
           isSeacrh:false,
           listContent:[],
           loadMore:false,
           isLoad:false,
           text:'',
           isFirst:true,
           totalTopic:0
        }

        this.collegue = props.app.colleague;
        this.isShow = true

        this.page = 1;
        this.totalPage = null;
        this.currentList = [];

        this.body = null
    }

    async componentDidMount(){
        this.state.scrollY.addListener(({value}) =>{
            if(value > 45 && (this.isShow !== this.state.isShow)){
                this.setState({isShow:true})
            }else if(value < 45&& (this.state.isShow === this.isShow)){
                this.setState({isShow:false})
            }
        })
        this.body = await Utils.GetBody(this.collegue)
    }

    cancelSearch(){
        this.setState({
            isSeacrh:false, 
            isFirst:true,
            listContent:[]
        })
    }

    loadMore(){
        if(!this.state.loadMore){
            if(this.page <= this.totalPage){
                this.setState({loadMore:true}, () =>{
                    this.loadData(this.state.text)
                })
            }
        }
    }

    checkResult(input){
        if(input && input.data && !input.error){
            return input
        }
        else {
            return null
        }
    }

    async loadData(text){
        let { page, totalPage, currentList, collegue } = this
        let newList = currentList
        if (!totalPage || (totalPage && !isNaN(totalPage) && page <= totalPage)){
            const result = await HaravanIc.SearchTopic(collegue ,text, page)
            if(result && result.data && !result.error){
                let data = result.data.data
                let totalCount = result.data.totalCount
                if(data.length>0){
                    for(let i = 0; i < data.length; i++){
                        const index = data.findIndex(e => e.createdBy == collegue.haraId)
                        if(index > -1){
                            let user = {...data[i], fullName:collegue.fullName}
                            newList.push(user)
                        }
                        else{
                            let resulUser = await HaravanHr.getColleague(data[i].createdBy)
                            resulUser = this.checkResult(resulUser)
                            if(resulUser){
                                let user = {...data[i], fullName:resulUser.data.fullName}
                                newList.push(user)
                            }
                            else{
                                newList.push(data[i])
                            }
                        }
                    }
                    this.page = page + 1
                    this.totalPage = Math.ceil(totalCount/20)
                    this.currentList = newList
                    this.setState({listContent: newList }, () =>{
                        this.setState({isLoading: false, loadMore:false, text:text, isFirst:false, totalTopic:totalCount, isLoad:false})
                    })
                }
                else{
                    this.setState({
                        listContent:[],
                        isLoading: false, 
                        loadMore:false, 
                        text:text,
                        isFirst:false,
                        isLoad:false
                    })
                }
                    
            }
        }
        else {
            this.setState({
                isLoading: false,
                loadMore:false,
            });
        }
    }

    onSearchBox(text){
        if(text.length != 0){
            this.setState({isLoad: true, listContent:[]},() =>{
                this.reloadList(text)
            })
        }
        else{
            this.setState({isLoad: true, listContent:[], isFirst:true})
        }
    }

    reloadList(text){
        this.setState({
            text:text,
            listContent:[],
        },() =>{
            this.page = 1;
            this.totalPage = null;
            this.currentList = [];
            this.loadData(text)
        })
    }

}
const mapStateToProps = (state) => ({ ...state })
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(News);