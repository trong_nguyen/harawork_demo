import React, { Component } from 'react';
import { View, Text, FlatList ,ScrollView, TextInput, TouchableOpacity, Animated, ActivityIndicator } from 'react-native';
import { settingApp, SvgCP, iconGroups, Loading } from '../../../../public'
import Header from '../components/header';
import ListHeader from './components/listHeader';
import TabContent from './components/tabContent';
import lodash from 'lodash';
import { EvilIcons } from '@expo/vector-icons';
import * as Animatable from 'react-native-animatable';
import Item from './components/listContent/component/item'

class Layout extends Component{

renderSearch(){
    let onChangeCallback = lodash.debounce(this.onSearchBox, 500)
    return(
        <View
            style={{
                flexDirection:'row', 
                height:settingApp.isIPhoneX? (84 +settingApp.statusBarHeight) : (64 +settingApp.statusBarHeight), 
                width:settingApp.width,
                padding:15,
                backgroundColor:'#FFFFFF', 
                justifyContent:'space-between', 
                alignItems:'center',
                ...settingApp.shadow
                }}
            >
            <Animatable.View 
                animation='fadeInRight'
                duration={120}
                style={{
                    width:settingApp.width - 64,
                    height:35,
                    paddingLeft:10,
                    paddingRight:10,
                    marginTop:10,
                    borderRadius:10,
                    backgroundColor:'rgba(142, 142, 147, 0.12)',
                    flexDirection:'row',
                    alignItems:'center',
                }}>
                <EvilIcons name="search" size={25} color={settingApp.colorDisable} />
                <TextInput
                    ref = {refs => this.TextInput = refs}
                    style={{
                        height: 30,
                        width:settingApp.width -95,
                        fontSize:14}}
                    placeholder='Tìm kiếm'
                    autoFocus={true}
                    onChangeText={onChangeCallback.bind(this)}
                    multiline={false}
                />
            </Animatable.View>
            
            <TouchableOpacity
                onPress={() => this.cancelSearch()}
                style={{height:44, justifyContent:'center', alignItems:'center', marginTop:10}}
                >
                <Text style={{fontSize:17, color:settingApp.blueSky}}>Hủy</Text>
            </TouchableOpacity>
        </View>
    )
}

renderContent(){
    return(
        <ScrollView
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            scrollEventThrottle={1}
            onScroll ={ Animated.event([
                {nativeEvent:{ contentOffset: { y : this.state.scrollY }}}
            ])}
            style={{flex:1}}
        >
            <View style={{width:(settingApp.width -30), height:60, backgroundColor:'#FFFFFF', flexDirection:'row', alignItems:'center', justifyContent:'flex-start',paddingLeft:5, top:-5}}>
                <SvgCP.iconTopic />
                <Text style={{color:settingApp.colorText, fontSize:36, paddingLeft:5, fontWeight:'bold'}}>Chủ đề</Text>
            </View>

            <View style={{paddingBottom:10, paddingTop:10}}>
                <Text style={{fontSize:20, color:settingApp.blackText, fontWeight:'bold', paddingLeft:10}}>Sôi động trong tuần</Text>
                <ListHeader 
                    navigation = { this.props.navigation}
                />
            </View>
            <TabContent
                mainNavigation ={this.props.navigation}
            />
            <View style={{ flex: 1, height: 20 }}/>
        </ScrollView>
    )
}

    render(){
        const { isSeacrh } = this.state
        return(
            <View style={{flex:1}}>
                {!isSeacrh ? <Header 
                    isShow = {this.state.isShow}
                    title= 'Chủ đề'
                    isSeacrh={() => this.setState({isSeacrh:true})}
                />:  this.renderSearch()}
                <View style={{ flex: 1 , backgroundColor:'#FFFFFF', padding:5}}>
                    {!isSeacrh ? this.renderContent() : 
                        this.renderList()
                        }
                </View>
            </View>
        )
    }
    
    

    renderLoadMore(){
        const {loadMore}= this.state;
        return(
            <View style={{flex:1, height:50, backgroundColor:'#FFFFFF'}}>
                {loadMore && <ActivityIndicator size='small' color={settingApp.color}/>}
            </View>
        )
    }
   
    renderList(){
        const { listContent, text, isLoad, isFirst, totalTopic } = this.state;
        if(listContent.length > 0){
            return(
                <View style={{paddingBottom:10, paddingLeft:10}}>
                {listContent.length > 0 &&
                    <Text style={{color:settingApp.colorDisable, fontSize:14, padding:10, fontWeight:'500'}}>{`Có ${totalTopic} kết quả tìm được`}</Text>
                }
                <FlatList 
                    data={listContent}
                    extraData={this.state}
                    keyExtractor={(item, index) =>(index+'')}
                    renderItem={(obj) => this.renderItem(obj)}
                    onEndReachedThreshold={1}
                    scrollEventThrottle={16}
                    onEndReached={() => this.loadMore()}
                    ListFooterComponent={this.renderLoadMore()}
                />
                </View>
            )
        }
        else{
            return(
                <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                   {!isLoad ?
                       <View style={{flex:1, alignItems:'center'}}>
                        {!isFirst && 
                        <View style={{alignItems:'center'}}>
                            <iconGroups.ArtWorks/>
                            <Text style={{ textAlign:'center',fontSize:18, color:settingApp.colorText, fontWeight:'600', marginTop:10}}
                            >{`Không tìm thấy "${text}"trong \n Danh sách chủ đề`}</Text>
                            <Text style={{ textAlign:'center',fontSize:14, color:settingApp.colorText,  marginTop:10}}
                            >{`Ðề xuất: Hãy thử những từ khóa khác. `}</Text>
                        </View>
                        }
                    </View>
                     : <Loading />
                    }
                </View>
            )
        }
    }

    renderItem(obj){
        const { item, index } = obj
        return(
            <Item
                item ={item}
                navigation ={ this.props.navigation }
            />
        )
    }
}
export default Layout;