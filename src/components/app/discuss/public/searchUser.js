import React, { Component } from 'react';
import { View, TouchableOpacity, Text, TextInput, ScrollView, FlatList, Platform } from 'react-native';
import {HeaderScreenNews, settingApp, iconGroup, SvgCP, Tree} from '../../../../public';
import { HaravanHr } from '../../../../Haravan'
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import lodash from 'lodash';
import Item from './itemUser';
import actions from '../../../../state/action';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class SearchUser extends Component {
    constructor(props){
        super(props)
        this.state={
            value:'',
            ishowFind: false,
            isLoading:false,
            isLoad:false,
            listEmploy: props.navigation.state.params.listEmploy,
            listRemove:[],
            list:[],
            isTree:false
        }
        this.actions = props.navigation.state.params.actions;
        this.colleague = props.app.colleague;
    }

    componentWillReceiveProps(nextProps){
        if(nextProps && nextProps.publicActions  && nextProps.publicActions.itemUser ){
            const item = nextProps.publicActions.itemUser
            let { list, listEmploy } = this.state;
            const index = listEmploy.findIndex(e => ((e.typeIC == item.typeIC) && (e.id == item.id) && (e.subName === item.subName)))
            if(index < 0){
                listEmploy.push(item)
            }
            else{
                listEmploy.splice(index, 1)
            }
            this.setState({listEmploy}, () => {
                this.props.actions.publicActions.itemUser(null)
            })
        }
    }

    setListEmploy(){
        this.actions(this.state.listEmploy)
        this.setState({list:[], listRemove:[]})
        this.TextInput.clear()
        this.props.navigation.pop()
    }

    removeItem(item){
        const { listEmploy, listRemove, list } = this.state;
        const index = listEmploy.findIndex(e => ((e.subName === item.subName) && (e.typeIC == item.typeIC) && ( e.id == item.id)) )
        if(index > -1){
            listRemove.push(item)
            listEmploy.splice(index, 1)
            this.setState({listEmploy, listRemove},()=>{
                this.props.actions.publicActions.removeItemUser(item)
            })
        }
       
    }

    setDefaul(){
        this.setState({
            listEmploy: this.props.navigation.state.params.listEmploy,
            list:[],
            listRemove:[]
        },() =>{
            this.TextInput.clear()
            this.props.navigation.pop()
        })
    }

    async onSearch(text) {
        this.setState({ isLoad: true, isTree:false, list:[] })
        const { listEmploy, list } = this.state;
        const resultDepart = await HaravanHr.searchDepartments(text)
        const resultJobli = await HaravanHr.searchJobtitle(text)
        const resultUser = await HaravanHr.searchColleague(text);
        let newList = this.state.list
        if (resultDepart && resultDepart.data && !resultDepart.error) {
            resultDepart.data.data.map(e => {
                e = { ...e, fullName: e.name, subName: e.name, typeIC:1, code:e.code }
                newList.push(e)
            })
        }
        if (resultJobli && resultJobli.data && !resultJobli.error) {
            resultJobli.data.data.map(e => {
                if (e.departments.length > 0) {
                    const total = { ...e, fullName: e.name, subName: ('Tất cả ' + e.name), total: (e.departments.length)}
                    const index = newList.findIndex(a => (a.id === e.id) && (a.jobtitleLevelId === e.jobtitleLevelId))
                    if (index < 0) {
                        newList.push(total)
                    }
                }
                e.departments.map(m => {
                    m = { ...m, fullName: m.jobtitleName, subName: m.departmentName, typeIC:2}
                    const index = newList.findIndex(a => (a.id === m.id) && (a.jobtitleId === m.jobtitleId))
                    if (index < 0) {
                        newList.push(m)
                    }
                })
            })
        }
        if (resultUser && resultUser.data && !resultUser.error) {
            resultUser.data.data.map(e => {
                let checkMe =  e.haraId == this.colleague.haraId
                e = { ...e, subName: (e.departmentName + '-' + e.jobtitleName), typeIC:4, fullName:checkMe ? 'Tôi' : e.fullName}
                const index = newList.findIndex(a => ((a.haraId === e.haraId)))
                if (index < 0) {
                    newList.push(e)
                }
            })
        }
        this.setState({ list:newList, isLoad: false  })
    }

    listEmploy(item) {
        const { listRemove, listEmploy } = this.state;
        if (listEmploy.length == 0) {
            const indexRemove = listRemove.findIndex(m => ((m.typeIC == item.typeIC) && (m.id == item.id) && (m.subName === item.subName)))
            listEmploy.push(item);
                if(indexRemove > -1){
                    listRemove.splice(indexRemove, 1)
                }
            this.setState({listRemove})
        }
        else {
            const index = listEmploy.findIndex(e => ((e.typeIC == item.typeIC) && (e.id === item.id) && (e.subName === item.subName)))
            if (index < 0) {
                const indexRemove = listRemove.findIndex(m =>( (m.typeIC == item.typeIC) && (m.id === item.id) && (m.subName === item.subName) ))
                listEmploy.push(item);
                if(indexRemove > -1){
                    listRemove.splice(indexRemove, 1)
                }
                this.setState({listRemove})
            }
            else{
                listEmploy.splice(index, 1)
            }
        }
        this.setState({listEmploy, listRemove})
    }
    onSearchBox(text) {
        if(text.length != 0){
            this.setState({ishowFind:true})
            this.onSearch(text)
        }
        else{
            this.setState({ishowFind:false})
        }
        
    }

    renderHeader(){
        return(
            <HeaderScreenNews 
                buttonLeft={
                    <TouchableOpacity
                        onPress={() => this.setDefaul()}
                        style={{
                            width:50, height:44, justifyContent:'center', alignItems:'center'
                        }}
                    >
                        <MaterialIcons  name='close' size={25} color={settingApp.colorDisable} />
                    </TouchableOpacity>
                }
                title ='Thêm thành viên'
                buttonRight = {
                    <TouchableOpacity
                        onPress={() =>  this.setListEmploy()}
                        style={{
                            width:50, height:44, justifyContent:'center', alignItems:'center'
                        }}
                    >
                        <Text style={{fontSize:17, color:settingApp.blueSky}}>Lưu</Text>
                    </TouchableOpacity>
                }
            />
        )
    }

    renderTopUser(){
        let { listEmploy, list } = this.state;
        return(
            <ScrollView 
                ref={ref => this.ScrollView = ref}
                onContentSizeChange={(width, height)=>{        
                    this.ScrollView.scrollToEnd({animated: true});
                }}
                style={{ minHeight:50  }}> 
                <View style={{flexDirection: 'row', flexWrap: 'wrap', alignItems: 'flex-start', minHeight:20 }}>
                    {listEmploy.map((e, i) => {
                        return (
                            <View
                                key={`${e.id}${e.typeIC}${e.subName}`}
                                style={{flexDirection:'row', minHeight:20, backgroundColor:'rgba(33, 70, 155, 0.08)', padding:5, justifyContent:'center', alignItems:'center', margin:5}}
                            >
                                <Text style={{fontSize:12, color:settingApp.blueSky}}
                                    >{e.typeIC == 4 ? e.fullName : e.subName ? e.subName : e.fullName} </Text>
                                <TouchableOpacity style={{
                                            width: 40,
                                            justifyContent: 'center',
                                            alignItems: 'flex-end',
                                            paddingRight: 10,
                                            justifyContent:'center'
                                        }}
                                        onPress={() => this.removeItem(e)}
                                        >
                                            <Ionicons name='ios-close' size={23} color='#9CA7B2' />
                                    </TouchableOpacity>
                            </View>
                           
                        )
                    })}
                </View>
            </ScrollView>
        )
    }

    renderContent(){
        return(
            <FlatList
                data={this.state.list}
                extraData={this.state}
                keyExtractor={(item, index) => (index+'')}
                renderItem={({item, index}) => 
                    <Item 
                        item={item}
                        checkList={item => this.listEmploy(item)}
                        list = { this.state.list }
                        listEmploy={this.state.listEmploy}
                        listRemove = {this.listRemove}
                    />
                }
            />
        )
    }

    renderTree(){
        if(Platform.OS === 'android'){
            return(
                <View style={{width:settingApp.width}}>
                    <Tree
                        screenProps={{
                            listEmploy: this.state.listEmploy
                        }}
                    />
                </View>
            )
        }
        else{
            return(
                <View style={{marginBottom:80}}>
                    <Tree
                        screenProps={{
                            listEmploy: this.state.listEmploy
                        }}
                    />
                </View>
            ) 
        }
    }


    render(){
        let { listEmploy , isTree} = this.state
        let onChangeCallback = lodash.debounce(this.onSearchBox, 500);
        let content = <View/>
        content = isTree ? this.renderTree(): this.renderContent()
        return(
            <View style={{ width:settingApp.width, backgroundColor:'#FFFFFF',height:"100%"}}> 
                {this.renderHeader()}
                <ScrollView 
                    style={{ 
                        padding: 15, height:"100%", width:settingApp.width,
                        }}>
                    <Text style={{color:settingApp.colorDisable, fontSize:12}}>Thành viên</Text>
                    {this.renderTopUser()}
                    <View style ={{flexDirection:'row', height:50, alignItems:'center',  justifyContent:'center'}}>
                        <TextInput
                            ref = {refs => this.TextInput = refs}
                            style={{
                                minHeight: 40,
                                width:(settingApp.width *0.8),
                                fontSize:15,
                                paddingBottom:5
                                }}
                            placeholder=''
                            onChangeText={onChangeCallback.bind(this)}
                            multiline={true}
                        />

                        <TouchableOpacity
                            onPress={() => this.setState({isTree:!this.state.isTree})}
                            style={{width:44, height:44, justifyContent:'center', alignItems:'center'}}
                        >
                            <SvgCP.button_Tree check={this.state.isTree}/>
                        </TouchableOpacity>
                    </View>
                    <View
                        style={{
                            width:(settingApp.width -30),
                            height:1,
                            backgroundColor:settingApp.blueSky,
                            marginBottom:5,
                            marginTop:5
                        }}
                    />
                    <Text style={{color:settingApp.colorDisable, fontSize:12}}>{`* Hệ thống sẽ thêm các thành viên thuộc đối tượng được chọn ở thời điểm tạo nhóm`}</Text>
                    {content}
                </ScrollView>
            </View>
        )
    }
}
const mapStateToProps = state => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(SearchUser);