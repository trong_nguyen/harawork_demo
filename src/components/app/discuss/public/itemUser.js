import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { HeaderWapper, settingApp, imgApp, SvgCP , AvatarCustom} from '../../../../public';
import { Feather } from '@expo/vector-icons';
import actions from '../../../../state/action';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class ItemUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isCheck: false,
        }
    }

    checkItem(e){
        this.props.checkList(e);
    }

    componentWillReceiveProps(nextProps){
        const { item } = this.props
        if(nextProps && nextProps.listEmploy){
            let listEmploy = nextProps.listEmploy;
           const index = listEmploy.findIndex(e => ((e.typeIC == item.typeIC) && (e.id == item.id)))
           if(index > -1){
               this.setState({isCheck:true})
           }
           else{
            this.setState({isCheck:false})
           }
        }
    }

    componentDidMount(){
        const { item, listEmploy } = this.props 
        const index = listEmploy.findIndex(e => ((e.typeIC == item.typeIC) && (e.id == item.id)))
        if(index > -1){
            this.setState({isCheck:true})
        }
    }

    checkList(item){
        let res = false
        let index = this.props.listEmploy.findIndex(m => (m.id === item.id) && (m.typeIC === item.typeIC))
        if(index > -1){
            res = true
        }
        else{
            res = false
        }
        return res
    }

    renderContent() {
        let { item } = this.props;
        const { isCheck } = this.state
        return (
            <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent:'space-between',alignItems:'center' }}>
                 <View style={{ flexDirection:'row',justifyContent: 'center', alignItems: 'center', height:50,}}>
                    {item.typeIC == 4 ? <AvatarCustom userInfo={{ name:item.fullName, picture: item.photo? item.photo : ""}} v={30}  fontSize={12}/>
                        : item.typeIC == 1 ? <SvgCP.icon_Depart/>
                            : item.typeIC == 2 ? <SvgCP.icon_Jobtile/> :<SvgCP.icon_Contrus/>
                    }
                    <View>
                        <Text style={{fontSize:14, color:settingApp.colorText, width:(settingApp.width*0.6), paddingLeft:5}}>{item.fullName}</Text>
                        <Text style={{fontSize:14, color:settingApp.colorDisable, width:(settingApp.width*0.6), paddingLeft:5}}
                            >{item.code ? item.code : item.subName}</Text>
                    </View>

                </View>
                <Feather name ='check' color={isCheck ? settingApp.blueSky : '#FFFFFF'} size={25}/>
            </View>
        )
    }

    render() {
        const { item } = this.props
        return (
            <TouchableOpacity 
            onPress={() => this.checkItem(item)}
            style={{
                minHeight: 50, backgroundColor: '#FFFFFF', paddingBottom:5, paddingTop:5
            }}>
                {this.renderContent()}
            </TouchableOpacity>
        )
    }
}
const mapStateToProps = state => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(ItemUser);