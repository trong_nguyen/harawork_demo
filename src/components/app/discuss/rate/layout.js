import React, { Component } from 'react';
import {
    View, Text, FlatList, Image , Animated ,
    ScrollView, TouchableOpacity, StyleSheet,
    Platform
    } from 'react-native';
import { HeaderNewStyles, settingApp, SvgCP, Loading, iconGroups, AvatarCustom , Utils} from '../../../../public'
import * as Animatable from 'react-native-animatable';
import ItemTopLoad from './component/itemTopLoading'
import ListLoading from './component/listLoading';

const widthCover = (settingApp.width - 48) / 2
class Layout extends Component{
    renderHeader(){
        const {isShow} = this.state
        return(
            <HeaderNewStyles
                title={
                    (isShow && isShow == true)?
                        <Animatable.View
                            duration={500}
                            animation='fadeIn'
                            style={styles.header}
                        >
                          <Text style={{fontSize:20, color:(settingApp.colorText), fontWeight:'bold'}}>Xếp hạng</Text>
                        </Animatable.View>
                        : <View />
                }
            />
        )
    }

    renderTitle(){
        return(
            <View style={styles.coverTitleHead}>
                <SvgCP.iconRate />
                <Text style={styles.textTitle}>Xếp hạng</Text>
            </View>
        )
    }

    renderListTop(listUser){
        return(
            <FlatList 
                data={listUser}
                extraData={this.state}
                keyExtractor={(item,index) =>  ''+index}
                renderItem={(obj) => this.renderTopMember(obj)}
                style={{flex:1}}
                scrollEnabled={false}
            />
        )
    }

    renderTopMember(item, type){
        let imageUser =  Utils.getLinkImage(item.haraId)
        return(
            <View
                style={[styles.menberTop]}
            >
                <View style={styles.sticker}>
                    {(type ==0) ? <iconGroups.topOne/> : <iconGroups.topTwo/>}
                </View>
                <View style={styles.coverUser}>
                    <AvatarCustom userInfo={{ name: item.fullName, picture: imageUser }} v={60} fontSize={20}/>
                </View>
                <Text style={styles.nameUser}>{item.fullName}</Text>
                <View
                    style={styles.lineBottom}
                >
                    <Text style={[styles.text,{ paddingRight:3 }]}>{item.point}</Text>
                    <iconGroups.iconPoint/>
                </View>
            </View>
        )
    }

    renderItem(obj){
        const { item, index } = obj;
        let imageUser = Utils.getLinkImage(item.haraId)
        let number = (index + 1)
        if((index != 0 && index != 1) && (index <10)){
            return(
                <View style={styles.itemUser}>
                    <View style={styles.coverInforUser}>
                        <View style={{height:50, minWidth:20, justifyContent:'center', alignItems:'center'}}>
                            <Text style={[styles.text]}>{number}</Text>
                        </View>
                        <AvatarCustom userInfo={{ name: item.fullName, picture: imageUser }} v={30} fontSize={15}/>
                        <Text style={[styles.text,{paddingLeft:5, width:settingApp.width*0.5}]}>{item.fullName}</Text>
                    </View>
                    <View style={[styles.coverInforUser,{marginRight:5}]}>
                        <Text style={[styles.text,{paddingRight:3}]}>{item.point}</Text>
                        <View style={{height:50, minWidth:20, justifyContent:'center', alignItems:'center'}}>
                            <iconGroups.iconPoint/>
                        </View>
                    </View>
                </View>
            )
        }
        else{
            return <View/>
        }
        
    }

    renderListUser(){
        const { listUser } = this.state
        return(
            <FlatList 
                data={listUser}
                extraData={this.state}
                keyExtractor={(item,index) =>  ''+index}
                renderItem={(obj) => this.renderItem(obj)}
                initialNumToRender={8}
                windowSize={8}
            />
        )
    }

    render(){
        const { listUser, infoMe, isLoading, totalUser} = this.state
        return(
            <View style={{flex:1, backgroundColor:'#FFFFFF'}}>
                {this.renderHeader()}
                <ScrollView
                    scrollEnabled={true}
                    scrollEventThrottle={1}
                    onScroll ={ Animated.event([
                        {nativeEvent:{ contentOffset: { y : this.state.scrollY }}}
                    ])}
                    style={{flex:1}}
                >    
                    {this.renderTitle()}
                    <View style={{flex:1, padding:15}}>
                        {/* Top comment a month */}
                        { listUser && (listUser.length > 0) && !isLoading ?
                            <View style={styles.coverTopMember}>
                                { listUser[0] && this.renderTopMember(listUser[0], 0)}
                                { listUser[1] &&  this.renderTopMember(listUser[1], 1)}
                            </View>
                            :
                            <ItemTopLoad />
                        }

                        {/* Rank of you */}
                    { infoMe && <View style={styles.infoMe}>
                            <Text style={[styles.text,{paddingRight:3}]}>{`Bạn có ${infoMe.point}`}</Text>
                            <iconGroups.iconPoint />
                            <Text style={[styles.text,{paddingLeft:3}]}> đứng thứ </Text>
                            <Text style={[styles.text,{fontWeight:'600'}]}>{`${infoMe.position}/${totalUser}`}</Text>
                        </View>}

                        {listUser && (listUser.length > 0) && !isLoading ?
                            this.renderListUser()
                            : <ListLoading/>
                            }
                        <View style={{width:settingApp.width, height:60}}/>
                    </View>
                </ScrollView>
                {/* {isLoading && <Loading style={{backgroundColor:'#FFFFFF'}}/>} */}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    text:{
        fontSize:16, 
        color:settingApp.colorText
    },
    header:{
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center', 
        flexDirection: 'row' 
    },
    coverTitleHead:{
        width:(settingApp.width), 
        height:60, 
        backgroundColor:'#FFFFFF', 
        flexDirection:'row', 
        alignItems:'center', 
        justifyContent:'flex-start', 
        paddingLeft:10
    },
    textTitle:{
        color:settingApp.colorText, 
        fontSize:30, 
        paddingLeft:10, 
        fontWeight:'bold'
    },
    coverTopMember:{
        width:settingApp.width -32,
        flexDirection:'row',
        justifyContent:'space-between',
        marginTop:15
    },
    menberTop:{
        width:widthCover,
        height:(settingApp.width - 14) / 2,
        backgroundColor:'#FFFFFF',
        borderRadius:5,
        margin:(Platform.OS === 'android') ? 2 : 0,
        ...settingApp.shadow,
        alignItems:'center', 
        justifyContent:'space-between'
    },
    sticker:{
        left:15, 
        top:0, 
        zIndex:100, 
        position:'absolute'
    },
    nameUser:{
        textAlign:'center', 
        paddingTop:10,
        paddingLeft:15,
        paddingRight:15, 
        fontSize:16, 
        color:settingApp.colorText,
        lineHeight:20 
    },
    coverUser:{
        alignItems:'center', 
        top:16, 
        minHeight:52, 
        width:(settingApp.width - 45) / 2,
    },
    lineBottom:{
        backgroundColor:'#F4F8FF', 
        height:28, 
        width:widthCover, 
        bottom:0,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center'
    },
    infoMe:{
        height:44,
        //width:settingApp.width -30,
        borderBottomColor:settingApp.colorSperator,
        borderTopColor:settingApp.colorSperator,
        borderTopWidth:1,
        borderBottomWidth:1,
        alignItems:'center',
        justifyContent:'center',
        marginTop:24,
        marginBottom: 16,
        marginLeft:43,
        marginRight:43,
        flexDirection:'row'
    },
    itemUser:{
        width:(settingApp.width - 30), 
        minHeight:50, 
        flexDirection:'row', 
        alignItems:'center', 
        justifyContent:'space-between'
    },
    coverInforUser:{
        flexDirection:'row', 
        alignItems:'center',
    }
})
export default Layout;