import Layout from './layout';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../state/action';
import { Animated } from 'react-native';
import { HaravanIc, HaravanHr } from '../../../../Haravan';

class Rate extends Layout{
    constructor(props){
        super(props)
        this.state={
           scrollY: new Animated.Value(0),
           isShow: false,
           listUser:[],
           isSeacrh:false,
           infoMe: null,
           isLoading:true,
           topUser:[],
           totalUser:0
        }
        this.collegue = props.app.colleague;
    }

    async componentDidMount(){
        this.state.scrollY.addListener(({value}) =>{
            if(value > 45){
                this.setState({isShow:true})
            }else if(value <45){
                this.setState({isShow:false})
            }
        })
        await this.loadData()
        await this.rankUser()
    }

    async loadData(){
        const { collegue } = this;
        let { infoMe, listUser } = this.state
        const result = await HaravanIc.topUserComment()
        const resulTotal = await HaravanHr.getTotalEmploy()
        if(result && result.data && (result.data.length > 0) && !result.error){
            let { data } = result
            for(let i = 0; i < data.length; i++){
                if(data[i].list && (data[i].list.length > 0)){
                    const { list } = data[i]
                    for(let a = 0; a< list.length; a++){
                        let item = {...list[a]}
                        if(list[a].haraId === collegue.haraId){
                            item = {...item, fullName:collegue.fullName}
                            listUser.push(item)
                        }
                        else{
                            const result = await HaravanHr.getColleague(list[a].haraId)
                            if(result && result.data && !result.error){
                                item = { ...item, fullName:result.data.fullName}
                                listUser.push(item)
                            }
                            else{
                                item = { ...item, fullName:'--'}
                                listUser.push(item)
                            }
                        }
                        this.setState({listUser})
                    }
                }
                else if(data[i].userId){
                    const user =  data[i]
                    let info = {...user}
                    if(user.haraId == collegue.haraId){
                        info = { ...info, fullName:collegue.fullName }
                        this.setState({infoMe:info})
                    }
                    else{
                        const result = await HaravanHr.getColleague(user.userId)
                        if(result && result.data && !result.error){
                            info = { ...info, fullName:result.data.fullName }
                            this.setState({infoMe:info})
                        }
                        else{
                            info = { ...info, fullName:'--' }
                            this.setState({infoMe:info})
                        }
                    }
                }
            }
            this.setState({isLoading:false})
        }
        else{
            this.setState({isLoading:false})
        }

        if(resulTotal && resulTotal.data && !resulTotal.error ){
            this.setState({totalUser:resulTotal.data.totalCount})
        }
    }

    rankUser(){
        let { listUser, topUser } = this.state
    }
}
const mapStateToProps = (state) => ({ ...state })
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(Rate);