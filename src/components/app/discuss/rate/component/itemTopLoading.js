import React, { Component } from 'react'
import { View , StyleSheet, Platform} from 'react-native';
import { settingApp, iconGroups} from '../../../../../public';

const widthCover = (settingApp.width - 48) / 2
class ItemTopLoad extends Component{
    renderTopMember(type){
        return(
            <View
                style={[styles.menberTop]}
            >
                <View style={styles.sticker}>
                    {(type ==0) ? <iconGroups.topOne/> : <iconGroups.topTwo/>}
                </View>
                <View style={styles.coverUser}>
                    <View style={styles.imageLoad}/>
                </View>
                <View style={{justifyContent:'center', alignItems:'center'}}>
                    <View
                        style={{width:80, height:20, backgroundColor:settingApp.colorSperator, borderRadius:5, marginBottom:5}}
                    />
                    <View
                        style={{width:50, height:20, backgroundColor:settingApp.colorSperator, borderRadius:5,}}
                    />
                </View>
                <View
                    style={styles.lineBottom}
                >
                     <View
                        style={{width:30, height:20, backgroundColor:settingApp.colorSperator, borderRadius:10}}
                    />
                    <iconGroups.iconPoint/>
                </View>
            </View>
        )
    }

    render(){
        return(
            <View style={styles.coverView}>
                {this.renderTopMember(0)}
                {this.renderTopMember(1)}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    coverView:{
        width:settingApp.width -32,
        flexDirection:'row',
        justifyContent:'space-between',
        marginTop:15
    },
    imageLoad:{
        width:60,
        height:60,
        borderRadius:30,
        backgroundColor:settingApp.colorSperator,
    },
    menberTop:{
        width:widthCover,
        height:(settingApp.width - 14) / 2,
        backgroundColor:'#FFFFFF',
        borderRadius:5,
        margin:(Platform.OS === 'android') ? 2 : 0,
        ...settingApp.shadow,
        alignItems:'center', 
        justifyContent:'space-between'
    },
    sticker:{
        left:15, 
        top:0, 
        zIndex:100, 
        position:'absolute'
    },
    coverUser:{
        alignItems:'center', 
        top:16, 
        minHeight:52, 
        width:(settingApp.width - 45) / 2,
    },
    lineBottom:{
        backgroundColor:'#F4F8FF', 
        height:28, 
        width:widthCover, 
        bottom:0,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center'
    },
})
export default ItemTopLoad;