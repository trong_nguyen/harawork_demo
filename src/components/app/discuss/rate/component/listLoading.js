import React, { Component } from 'react'
import { View , StyleSheet, Platform, FlatList, Text} from 'react-native';
import { settingApp, iconGroups} from '../../../../../public';

class ListLoading extends Component{
    constructor(props){
        super(props)
        this.state={
            list:[
                {value:'', stt:3},
                {value:'', stt:4},
                {value:'', stt:5},
                {value:'', stt:6},
                {value:'', stt:7},
                {value:'', stt:8},
                {value:'', stt:9},
                {value:'', stt:10},
            ]
        }
    }

    rederItem(obj){
        const { item, index } = obj
        return(
            <View style={styles.itemUser}>
                <View style={styles.coverInforUser}>
                    <View style={{height:50, width:20, justifyContent:'center', alignItems:'center'}}>
                        <Text>{item.stt}</Text>
                    </View>
                    <View style={styles.imageLoad}/>
                    <View style={styles.name}/>
                </View>
                <View style={[styles.coverInforUser,{paddingRight:5}]}>
                    <View style={styles.point}/>
                    <iconGroups.iconPoint/>
                </View>
            </View>
        )
    }

    render(){
        return(
            <View style={styles.coverView}>
                <FlatList
                    data={this.state.list}
                    extraData={this.state}
                    keyExtractor={(item,index) => ''+index}
                    renderItem={(obj) => this.rederItem(obj)}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:'#FFFFFF'
    },
    coverView:{
        width:settingApp.width -32,
        flexDirection:'row',
        justifyContent:'space-between',
        marginTop:15
    },
    itemUser:{
        width:(settingApp.width - 30), 
        height:50, 
        flexDirection:'row', 
        alignItems:'center', 
        justifyContent:'space-between',
    },
    imageLoad:{
        width:30,
        height:30,
        borderRadius:15,
        backgroundColor:settingApp.colorSperator,
        marginLeft:5
    },
    name:{
        width:settingApp.width * 0.5,
        height:20,
        borderRadius:5,
        backgroundColor:settingApp.colorSperator,
        marginLeft:5
    },
    point:{
        width:40,
        height:20,
        borderRadius:5,
        backgroundColor:settingApp.colorSperator,
        paddingRight:5
    },
    coverInforUser:{
        flexDirection:'row', 
        alignItems:'center'
    }
})
export default ListLoading;