import React, { Component } from 'react';
import { View, Text, FlatList, TextInput , Animated ,ScrollView, TouchableOpacity, ActivityIndicator} from 'react-native';
import {  settingApp, SvgCP, iconGroups, Loading } from '../../../../public'
import * as Animatable from 'react-native-animatable';
import Header from '../components/header';
import ListHeader from './components/listHeader';
import TabContent from './components/content/tabContent'
import { EvilIcons } from '@expo/vector-icons';
import Item from './components/content/screenTab/components/item';
import Modal from './components/modalOption'
import lodash from 'lodash';

class Layout extends Component{

    renderTitle(){
        return(
            <View style={styles.viewTitle}>
                <SvgCP.iconGroup />
                <Text style={styles.textTitle}>Nhóm</Text>
            </View>
        )
    }

    renderListContent(){
        return(
            <TabContent.TabContentAdmin
                screenProps ={{
                    collegue: this.collegue,
                    mainNavigation:this.props.navigation,
                    onEndReached: this.state.onEndReached
                    }}
            />
        )
    }

    renderSearch(){
        let onChangeCallback = lodash.debounce(this.onSearchBox, 500)
        return(
            <View style={[styles.searchBox,{...settingApp.shadow}]}>
                <Animatable.View 
                    animation='fadeInRight'
                    duration={120}
                    style={styles.animatable}>
                    <EvilIcons name="search" size={25} color={settingApp.colorDisable} />
                    <TextInput
                        ref = {refs => this.TextInput = refs}
                        style={{
                            height: 30,
                            width:settingApp.width -95,
                            fontSize:14}}
                        placeholder='Tìm kiếm'
                        autoFocus={true}
                        onChangeText={onChangeCallback.bind(this)}
                        multiline={false}
                    />
                </Animatable.View>
                <TouchableOpacity
                    onPress={() => this.cancelSearch() }
                    style={{height:44, justifyContent:'center', alignItems:'center', marginTop:10}}
                    >
                    <Text style={{fontSize:17, color:settingApp.blueSky}}>Hủy</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderItem(obj){
        const { item, index } = obj
        return(
            <Item
                item={item}
                index={index}
                navigation ={ this.props.navigation }
                colleague = {this.colleague} 
                isShow={(show, item) => this.setState({isShow:show, item:item})}
                itemChange={this.state.item}
            />
        )
    }

    renderList(){
        const { listContent, text, isLoad, isFirst, totalTopic, isShow } = this.state;
        if(listContent.length > 0){
            return(
                <View style={{paddingBottom:10, padding:15}}>
                {listContent.length > 0 &&
                    <Text style={styles.titleSearch}>{`Có ${listContent.length} kết quả tìm được`}</Text>
                }
                <FlatList 
                    data={listContent}
                    extraData={this.state}
                    style={{marginBottom:100}}
                    keyExtractor={(item, index) =>(index+'')}
                    renderItem={(obj) => this.renderItem(obj)}
                    onEndReachedThreshold={1}
                    scrollEventThrottle={16}
                    onEndReached={() => this.loadMore()}
                    ListFooterComponent={this.renderLoadMore()}
                />
                <Modal 
                    visible = {isShow}
                    item = {this.state.item}   
                    isShow={isShow}
                    close={() => this.setState({isShow:false})}
                    checkOption={(item, type) => this.checkOption(item, type)}
                />
                </View>
            )
        }
        else{
            return(
                <View style={styles.notFound}>
                   {!isLoad ?
                       <View style={styles.notFound}>
                        {!isFirst &&<View style={{alignItems:'center'}}>
                            <iconGroups.ArtWorks/>
                            <Text style={{ textAlign:'center',fontSize:18, color:settingApp.colorText, fontWeight:'600', marginTop:10}}
                            >{`Không tìm thấy "${text}"trong \n trong danh sách nhóm`}</Text>
                            <Text style={{ textAlign:'center',fontSize:14, color:settingApp.colorText,  marginTop:10}}
                            >{`Ðề xuất: Hãy thử những từ khóa khác. `}</Text>
                        </View>}
                    </View>
                     : <Loading />
                    }
                </View>
            )
        }
    }

    renderLoadMore(){
        const {loadMore}= this.state;
        return(
            <View style={{flex:1, height:50, backgroundColor:'#FFFFFF'}}>
                {loadMore && <ActivityIndicator size='small' color={settingApp.color}/>}
            </View>
        )
    }

    render(){
        const { isSeacrh } = this.state
        return(
            <View style={{flex:1, backgroundColor:'#FFFFFF'}}>
               {!isSeacrh ? <Header 
                    isShow = {this.state.isShow}
                    title= 'Nhóm'
                    isSeacrh={() => this.setState({isSeacrh:true})}
                />:  this.renderSearch()}
                {!isSeacrh ? <ScrollView
                        scrollEnabled={true}
                        scrollEventThrottle={1}
                        onScroll ={ Animated.event([
                            {nativeEvent:{ contentOffset: { y : this.state.scrollY }}}
                        ])}
                        >     
                        {this.renderTitle()}
                        {/* <View style={styles.viewTextHotGroup}>
                            <Text style={styles.textHotGroups}>Nhóm nổi bật</Text>
                        </View> */}
                        <View style={{paddingBottom:10, paddingTop:5}}>
                            <ListHeader 
                                navigation = { this.props.navigation}
                            />
                        </View>
                        <View>
                            <TabContent 
                                navigation = { this.props.navigation}
                            />
                        </View>
                    </ScrollView>
                    : this.renderList()
                }
                {!isSeacrh && <TouchableOpacity 
                    onPress={() => this.props.navigation.navigate('CreateGroup',{data:null})}
                    style={styles.addGroup}>
                    <iconGroups.AddGroups />
                </TouchableOpacity>}
            </View>
        )
    }
}
const styles = {
    animatable:{
        width:settingApp.width - 64,
        height:35,
        paddingLeft:10,
        paddingRight:10,
        marginTop:10,
        borderRadius:10,
        backgroundColor:'rgba(142, 142, 147, 0.12)',
        flexDirection:'row',
        alignItems:'center',
    },
    searchBox:{
        flexDirection:'row', 
        height:settingApp.isIPhoneX? (84 +settingApp.statusBarHeight) : (64 +settingApp.statusBarHeight), 
        width:settingApp.width,
        padding:15,
        backgroundColor:'#FFFFFF', 
        justifyContent:'space-between', 
        alignItems:'center',
    },
    textHotGroups:{
        color:settingApp.colorText, fontSize:20, paddingLeft:10, fontWeight:'bold'
    },
    viewTextHotGroup:{
        width:(settingApp.width-30), 
        height:30, 
        backgroundColor:'#FFFFFF', 
        flexDirection:'row', 
        alignItems:'center', 
        justifyContent:'flex-start', 
        paddingLeft:10
    },
    notFound:{
        flex:1, 
        alignItems:'center'
    },
    mess:{
        textAlign:'center',
        fontSize:18, 
        color:settingApp.colorText,
        fontWeight:'600', marginTop:10
    },
    viewTitle:{
        width:(settingApp.width-30), 
        height:60, 
        backgroundColor:'#FFFFFF', 
        flexDirection:'row', 
        alignItems:'center', 
        justifyContent:'flex-start', 
        paddingLeft:10
    },
    textTitle:{
        color:settingApp.colorText, 
        fontSize:36, 
        paddingLeft:10, 
        fontWeight:'bold'
    },
    addGroup:{
        position:'absolute', 
        width:80, 
        height:80, 
        right:0, 
        bottom:0
    },
    titleSearch:{
        color:settingApp.colorDisable, 
        fontSize:14, 
        padding:10, 
        fontWeight:'500'
    }
}
export default Layout;