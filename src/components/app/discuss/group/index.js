import Layout from './layout';
import { Animated } from 'react-native';
import { connect } from 'react-redux';
import actions from '../../../../state/action';
import { Alert } from 'react-native';
import { bindActionCreators } from 'redux';
import { HaravanIc, HaravanHr } from '../../../../Haravan';
import {  Toast } from '../../../../public';
import * as Utils from '../components/Utils'

class Group extends Layout{
    constructor(props){
        super(props)
        this.state={
            scrollY: new Animated.Value(0),
            isShow: false,
            isSeacrh:false,
            isLoad:false,
            listContent:[],
            loadMore:false,
            text:'',
            isFirst:true,
            totalTopic:0,
            item:null
        }

        this.colleague= props.app.colleague;
        this.page=1;
        this.totalPage = null;
        this.currentList = [];
        this.body = null
    }

    async componentDidMount(){
        this.state.scrollY.addListener(({value}) =>{
            if(value > 45){
                this.setState({isShow:true})
            }else if(value <45){
                this.setState({isShow:false})
            }
        })
        this.body = await Utils.GetBody(this.colleague)
    }

    cancelSearch(){
        this.setState({
            isSeacrh:false, 
            isFirst:true,
            listContent:[]
        })
    }

    async removeGroup(value){
        const { type, note } = value
        let { item }  = this.state
        const { body } = this
        const result =  await HaravanIc.RemoveGroup(item.id, note, body)
        if(result && !result.error){
            Toast.show('Đã xóa nhóm thành công')
            this.props.actions.discuss.refreshList(true)
        }
        else{
            Alert.alert(
                'Thông báo',
                `Xóa nhóm thất bại. \n Lý do: Lỗi hệ thống. \n Đề xuất: Thử lại lần nữa `,
                [
                    { text: 'Xác nhận', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: true }
            )
        }
    }

    async joinGroup(){
        const { colleague } = this ;
        const {item} = this.state;
        const { body } = this
        const result = await HaravanIc.JoinGroup(item.id, false, body)
        if(result && !result.error){
            this.setState({item:{...item, isJoined:false}},() =>{
                Toast.show('Đã rời khỏi nhóm thành công')
                this.props.actions.discuss.changeItem(this.state.item)
            })
        }
        else{
            Alert.alert(
                'Thông báo',
                `Rời nhóm thất bại. \n Lý do: Lỗi hệ thống. \n Đề xuất: Thử lại lần nữa `,
                [
                    { text: 'Xác nhận', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: false }
            )
        }
    }
    async pinGroup(item, type){
        if(type == 1){
            const result = await HaravanIc.PinGroup( item.id , true)
            if(result && !result.error){
                this.setState({item:{...item, isPinned:true}},() =>{
                // Toast.show('Ghim nhóm thành công')
                })
            }
            else{
                // Toast.show('Ghim nhóm thất bại')
            }
        }
        else{
            const result = await HaravanIc.PinGroup( item.id,false)
            if(result && !result.error){
                this.setState({item:{...item, isPinned:false}},() =>{
                    Toast.show('Bỏ ghim nhóm thành công')
                })
            }
            else{
                Toast.show('Bỏ ghim nhóm thất bại')
            }
        }
    }

    async checkOption(item, type){
        switch(type){
            case 1:
                this.pinGroup(item, 1)
                break;
            case 2:
                this.joinGroup()
                break;
            case 3:
                this.props.navigation.navigate('RemoveGroup',{
                    actions:(value) => this.removeGroup(value)
                })
                break;
            case 4:
                this.pinGroup(item, 4)
                break;
            default:null
                break;
        }
    }

    onSearchBox(text){
        if(text.length != 0){
            this.setState({isLoad: true, listContent:[],isFirst:false},() =>{
                this.reloadList(text)
            })
        }
        else{
            this.setState({isLoad: true, listContent:[], isFirst:true})
        }
    }

    reloadList(text){
        this.setState({
            text:text,
            listContent:[],
        },() =>{
            this.page = 1;
            this.totalPage = null;
            this.currentList = [];
            this.loadData(text)
        })
    }

    loadMore(){
        if(!this.state.loadMore){
            if(this.page <= this.totalPage){
                this.setState({loadMore:true}, () =>{
                    this.loadData(this.state.text)
                })
            }
        }
    }

    async loadData(text){
        let { page, totalPage, currentList, colleague } = this
        let newList = currentList   
        const { body } = this
        if (!totalPage || (totalPage && !isNaN(totalPage) && page <= totalPage)){
            const result = await HaravanIc.SearchGroups(text, body, page)
            if(result && result.data && !result.error){
                let totalCount = result.data.totalCount
                newList = [...newList,...result.data.data]
                this.currentList = newList
                this.page =  page + 1
                this.totalPage = Math.ceil(totalCount/10)
                this.setState({listContent:newList, totalTopic:totalCount, loadMore:false,}, () =>{
                    this.setState({isLoading:false, reLoad:false,isLoadMore:false, isLoad:false})
                })
            }
        }
        else {
            this.setState({
                isLoading: false,
                loadMore:false,
                isLoad:false
            });
        }
    }

}
const mapStateToProps = (state) => ({ ...state })
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(Group);