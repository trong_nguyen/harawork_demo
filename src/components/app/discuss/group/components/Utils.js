import React, { Component } from 'react';
import {  iconGroups } from '../../../../../public'

export function checkCreateThread(typeThread){
    let create = null;
    if(typeThread == 1){
        create = 'Quản trị viên'
    }
    else{
        create = 'Cho phép mọi thành viên được tạo chủ đề '
    }
    return create
}

export function checkContentType(type, size){
    let avt = null;
    switch (type) {
        case 1:
            avt = <iconGroups.Document size={size}/>
            break;
        case 2:
            avt = <iconGroups.Groups size={size}/>
            break;
        case 3:
            avt = <iconGroups.Infomation size={size}/>
            break;
        case 4:
            avt = <iconGroups.Sports size={size}/>
            break;
        default:
            avt = <iconGroups.Speaker size={size}/>
            break;
    }   
    return avt;
}

export function checkType(item){
    let type = null;
    if(item){
        if(item == 1){
            type = 'NHÓM MỞ'
        }
        else{
            type = 'NHÓM KÍN'
        }
    }
    else{
        type= 'Không lấy được dữ liệu'
    }
    return type
}

export function checkTypeGoup(type){
    let title = null;
    switch (type) {
        case 0:
            title = 'Chọn loại'
            break;
        case 1:
            title = 'Danh sách - Công việc'
            break;
        case 2:
            title = 'Thảo luận chung'
            break;
        case 3:
            title = 'Sở thích -  Thể thao'
            break;
        case 4:
            title = 'Tin tức - Hướng dẫn'
            break;
        default:
            title = 'Nhóm mặt định'
            break;
    }   
    return title;
}


export function checkName(name, size){
    let avt = null;
    switch (name) {
        case "sport":
            avt = <iconGroups.Sports size={size}/>
            break;
        case "document":
            avt = <iconGroups.Document size={size}/>
            break;
        case "communicate":
            avt = <iconGroups.Comunicate size={size}/>
            break;   
        case "tag":
            avt = <iconGroups.Tag size={size}/>
            break;
        case "group":
            avt = <iconGroups.Groups size={size}/>
            break;
        case "customer-service":
            avt = <iconGroups.Customer_Service size={size}/>
            break;
        case "house":
            avt = <iconGroups.House size={size}/>
            break;
        case "beach":
            avt = <iconGroups.Beach size={size}/>
            break;
        case "teacher":
            avt = <iconGroups.Teacher size={size}/>
            break;
        case "building":
            avt = <iconGroups.Building size={size}/>
            break;
        case "money":
            avt = <iconGroups.Money size={size}/>
            break;
        case "books":
            avt = <iconGroups.Book size={size}/>
            break;
        case "information":
            avt = <iconGroups.Infomation size={size}/>
            break;
        case "cup":
            avt = <iconGroups.Cup size={size}/>
            break;
        case "none":
            avt = <iconGroups.CreateAvatar size={size}/>
            break;         
        default:
            avt = <iconGroups.Speaker size={size}/>
            break;
    }
    return avt
}