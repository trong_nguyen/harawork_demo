import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Animated, Modal, Alert, Platform } from 'react-native';
import * as Animatable from 'react-native-animatable';
import {  settingApp } from '../../../../../public'

class ModalOption extends Component{
    constructor(props){
        super(props)
        const bottom = -(settingApp.height * 0.5 + 10);
        this.state={
            isOpen: false,
            bottom: new Animated.Value(bottom),
            item: null
        }
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.item){
            this.setState({item: nextProps.item})
        }
    }
    componentDidMount(){
        this.open()
    }

    open() {
        Animated.timing(
            this.state.bottom,
            {   delay:500,
                toValue: 0,
                duration: 250
            }
        ).start();
    }

    close() {
        Animated.timing(
            this.state.bottom,
            {
                toValue: -(settingApp.height * 0.5 + 10),
                duration: 120
            }
        ).start(() => {
            this.props.close()
        });
    }

    checkType(type){
        if(type== 2){
            Alert.alert(
                'Thông báo',
                `Bạn có chắc chắn muốn rời khỏi nhóm này và không còn nhận được các thông báo về chủ đề trong nhóm nữa?`,
                [
                    { text: 'Hủy', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                    { text: 'Xác nhận', onPress: () =>{ 
                        this.props.checkOption(this.state.item, type)
                        this.props.close(type)    
                    }},
                ],
                { cancelable: false }
            )
        }
        else{
            this.props.checkOption(this.state.item, type)
            this.props.close(type)
        }
    }

    renderButton(title, type){
        return(
            <TouchableOpacity
                activeOpacity={0.8}    
                onPress={() => {
                    this.checkType(type)
                }}
                style={[Platform.OS == 'ios'? styles.lineIos : styles.lineAndroid, {
                        borderBottomWidth: type != 3 ? 1 : 0 ,
                        borderBottomColor: settingApp.colorSperator
                    }]} 
            >
            <Text style={{fontSize:18, color:(type == 3) ? '#FF3B30' : settingApp.blueSky}}>{title}</Text>
        </TouchableOpacity>
        )
    }

    renderLine(item){
        return(
            <View                 
                style={[styles.viewLine, {
                    width: Platform.OS=='ios' ? (settingApp.width*0.95) : (settingApp.width),
                    borderRadius: Platform.OS=='ios' ? 10 : 0,
                    } ]}
            >
                {this.renderButton((!item.isPinned==true) ? 'Ghim nhóm vào menu' : 'Bỏ ghim nhóm vào menu', (!item.isPinned) ? 1:4)}
                {(item.isJoined && (item.contentType !=5 || item.canLeftGroup)) && this.renderButton('Rời khỏi nhóm', 2)}
                {(item.isJoined && (item.isAdminForum || item.isAdminGroup)) && this.renderButton('Xóa nhóm', 3)}
            </View>
        )
    }

    renderCancel(){
        return(
            <TouchableOpacity 
                activeOpacity={0.8}
                onPress={() => {
                    this.Animatable.props.animation="fadeInDownBig"
                    this.props.close()
                    }}
                style={[Platform.OS =='ios' ? styles.buttonIos : styles.buttonAndroid,{
                    borderRadius:Platform.OS =='ios' ? 10 : 0, 
                }]}
                 >
                <Text  style={{fontSize:18, color:settingApp.blueSky, fontWeight:'600'}}>Hủy</Text>
            </TouchableOpacity>
        )
    }

    render(){
        const { bottom } = this.state;
        const { height } = settingApp;
        let { item } = this.state
        return(
            <Modal
                ref={refs => this.Modal = refs}
                animationType="fade"
                transparent={true}
                visible={this.props.visible}
                onRequestClose={() => this.close()}>
            
            <TouchableOpacity
                onPress={() => this.props.close()}
                activeOpacity={1}
                style={{
                    flex: 1,
                    backgroundColor: 'rgba(0,0,0,0.5)',
                }}
            />
            <View style={{flex:1, backgroundColor:'rgba(0,0,0,0.5)', justifyContent:'flex-end'}}>
                <Animatable.View
                    ref={refs => this.Animatable = refs}
                    animation="fadeInUpBig"
                    delay={120}
                    duration={120}
                    style={{
                        width:settingApp.width,
                        height: (height*0.5),
                        backgroundColor: 'transparent',
                        justifyContent:'flex-end',
                        alignItems:'center',
                        ...settingApp.shadow
                    }}
                >
                    {item && this.renderLine(item)}

                    {this.renderCancel()}
                </Animatable.View>
            </View>
        </Modal>
        )
    }
}
const styles ={
    lineAndroid:{
        width:(settingApp.width),  
        backgroundColor:'#FFFFFF', 
        height:54,
        ...settingApp.shadow, 
        justifyContent:'center', 
        alignItems:'center'
    },
    buttonAndroid:{
        height:54,
        width:(settingApp.width), 
        justifyContent:'center', 
        alignItems:'center',
        backgroundColor:'#FFFFFF', 
        marginTop:10
    },
    lineIos:{
        width:(settingApp.width*0.95),  
        height:54,
        borderRadius:10,
        backgroundColor:'#FFFFFF', 
        justifyContent:'center', 
        alignItems:'center'
    },
    buttonIos:{
        height:54,
        width:(settingApp.width*0.95), 
        justifyContent:'center', 
        alignItems:'center',
        borderRadius:10,
        marginTop:10,
        marginBottom: 10,
        backgroundColor:"#FFFFFF"
    },
    viewLine:{
        width:(settingApp.width*0.95),  
        borderRadius:Platform.OS ==='ios' ? 10 : 0,
        backgroundColor:'#FFFFFF', 
        ...settingApp.shadow, 
        justifyContent:'center', 
        alignItems:'center'
    }
}
export default ModalOption;