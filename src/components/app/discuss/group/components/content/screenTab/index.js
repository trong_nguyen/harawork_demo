import Layout from './layout';
import { HaravanIc } from '../../../../../../../Haravan'
import { Toast, ToastMess } from '../../../../../../../public';
import { connect } from 'react-redux';
import actions from '../../../../../../../state/action';
import { Alert } from 'react-native';
import { bindActionCreators } from 'redux';

class ListGroups extends Layout{
    constructor(props){
        super(props)
        this.state={
            listData:[],
            isLoading:true,
            isShow:false,
            item: null,
            refreshing:false,
            isLoadMore:false
        }
        this.url = props.url;
        this.navigation = props.navigation;
        this.colleague= props.colleague;
        this.page=1;
        this.totalPage = null;
        this.currentList = []
    }

    componentDidMount(){
        this.loadData()
    }

    componentWillReceiveProps(nextProps){
        if(nextProps && nextProps.discuss && nextProps.discuss.refreshList && (nextProps.discuss.refreshList==true)){
            this.props.actions.discuss.refreshList(null)
            this.refreshList()
        }
    }

    deleteItem(item){
        let { listData } = this.state
        const index = listData.findIndex(e => (e.id === item.id))
        if(this.url === 'list_groups'){
            if(index > -1){
                listData.splice(index, 1)
            }
            this.setState({listData},() => {
                Toast.show('Đã rời khỏi nhóm thành công')
                this.props.actions.discuss.changeItem(this.state.item)
            })
        }
        else{
            if(index > -1){
                Toast.show('Đã rời khỏi nhóm thành công')
                this.props.actions.discuss.changeItem(this.state.item)
            }
        }
    }

    refreshList(){
        this.page=1;
        this.totalPage = null;
        this.currentList = []
        this.url = this.props.url;
        this.setState({item:null, isShow:false},() =>{
            this.loadData()
        })
    }

    async joinGroup(){
        const { collegue } = this ;
        const {item} = this.state
        const result = await HaravanIc.JoinGroup(item.id, false, collegue)
        if(result && !result.error){
            this.setState({item:{...item, isJoined:false}, refreshing:true},() =>{
                this.deleteItem(item)
            })
        }
        else{
            Alert.alert(
                'Thông báo',
                `Rời nhóm thất bại. \n Lý do: Lỗi hệ thống. \n Đề xuất: Thử lại lần nữa `,
                [
                    { text: 'Xác nhận', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: false }
            )
        }
    }
    async pinGroup(item, type){
        if(type == 1){
            const result = await HaravanIc.PinGroup( item.id , true)
            if(result && !result.error){
                this.setState({item:{...item, isPinned:true}},() =>{
                    Toast.show('Ghim nhóm thành công')
                })
            }
            else{
               Toast.show('Ghim nhóm thất bại')
            }
        }
        else{
            const result = await HaravanIc.PinGroup( item.id,false)
            if(result && !result.error){
                this.setState({item:{...item, isPinned:false}},() =>{
                    Toast.show('Bỏ ghim nhóm thành công')
                })
            }
            else{
                Toast.show('Bỏ ghim nhóm thất bại')
            }
        }
    }

    async checkOption(item, type){
        switch(type){
            case 1:
                this.pinGroup(item, 1)
                break;
            case 2:
                this.joinGroup()
                break;
            case 3:
                this.navigation.navigate('RemoveGroup',{
                    actions:(value) => this.removeGroup(value)
                })
                break;
            case 4:
                this.pinGroup(item, 4)
                break;
            default:null
                break;
        }
    }

    async removeGroup(value){
        const { type, note } = value
        let { item }  = this.state
        const result =  await HaravanIc.RemoveGroup(item.id, note, this.colleague)
        if(result && !result.error){
            //.show('Đã xóa nhóm thành công')
            this.props.actions.discuss.refreshList(true)
        }
        else{
            Alert.alert(
                'Thông báo',
                `Xóa nhóm thất bại. \n Lý do: Lỗi hệ thống. \n Đề xuất: Thử lại lần nữa `,
                [
                    { text: 'Xác nhận', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: true }
            )
        }
    }

    loadMore(){        
        let { page, totalPage} = this;
        if(page <= totalPage){
            this.setState({isLoadMore:true},() =>{
                this.loadData()
            })
        }
    }

    async loadData(){
        let { collegue, url, currentList, page, totalPage} = this;
        let newList = currentList
        if(!totalPage || (totalPage && !isNaN(totalPage) && (page < totalPage))){
            const result = await HaravanIc.GetListGroup(url, collegue, page)
            if(result && result.data && !result.error){
                let totalCount = result.data.totalCount
                newList = [...newList,...result.data.data]
                this.currentList = newList
                this.page =  page + 1
                this.totalPage = Math.ceil(totalCount/10)
                this.setState({listData:newList, }, () =>{
                    this.setState({isLoading:false, reLoad:false,refreshing:false, isLoadMore:false})
                })
            }
        }
        else{
            this.setState({
                isLoading:false, 
                eLoad:false, 
                refreshing:false, 
                isLoadMore:false})
        }
    }
}
const mapStateToProps = (state) => ({ ...state })
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(ListGroups);