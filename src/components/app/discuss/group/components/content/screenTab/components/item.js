import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Alert, ActivityIndicator } from 'react-native';
import { settingApp, SvgCP, Toast, Loading } from '../../../../../../../../public';
import { HaravanIc } from '../../../../../../../../Haravan';
import * as UtilGroups from '../../../Utils';
import { Feather } from '@expo/vector-icons'
import { connect } from 'react-redux';
import actions from '../../../../../../../../state/action';
import { bindActionCreators } from 'redux';

class Item extends Component {
    constructor(props){
        super(props)
        this.state={
            isJoined: this.props.item.isJoined,
            isLoad:false,
            item: this.props.item
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps && nextProps.discuss && nextProps.discuss.changeItem &&
            (nextProps.discuss.changeItem.id === this.state.item.id) &&
            (JSON.stringify(nextProps.discuss.changeItem) !== JSON.stringify(this.state.item)) )
            {   
                this.setState({
                    item:nextProps.discuss.changeItem,
                    isJoined:nextProps.discuss.changeItem.isJoined,
                    isPinned:nextProps.discuss.changeItem.isPinned
                },()=>{
                    this.props.actions.discuss.changeItem(null)
             })
        }

        if(nextProps && nextProps.itemChange && ((nextProps.itemChange.id === this.state.item.id))){
            this.setState({
                item:nextProps.itemChange
            })
        }
    }   

    async joinGroup(){
        const { colleague, item } = this.props;
        const result = await HaravanIc.JoinGroup(item.id, true, colleague)
        if(result && !result.error){
            this.setState({isJoined: true,isLoad:false, item:{...this.props.item, isJoined:true} },() =>{
                Toast.show('Tham gia nhóm thành công')
                this.props.actions.discuss.changeItem(this.state.item)
            }
           )
        }
        else{
            Alert.alert(
                'Thông báo',
                `Tham gia nhóm thất bại. \n Lý do: Lỗi hệ thống. \n Đề xuất: Thử lại lần nữa `,
                [
                    { text: 'Xác nhận', onPress: () =>(this.setState({isLoad:false},() => console.log('OK Pressed') )  )},
                ],
                { cancelable: false }
            )
        }
    }

    checkAvatar(){
        const { item } = this.state
        let avatar = null
        let size = 50
        if(item.avatar){
            avatar = UtilGroups.checkName(item.avatar, size)
        }
        else{
            avatar = UtilGroups.checkContentType(item.contentType, size)
        }
        return avatar
    }

    renderOption(){
        return(
            <TouchableOpacity
                onPress={() =>{
                    this.props.isShow(true, this.state.item)
                }}
                style={{
                    width:60, 
                    height:60,
                    position:'absolute', 
                    justifyContent:'center', 
                    alignItems:'center',
                    right:-10
                    }}>
                    <Feather name='more-horizontal' size={20} color={settingApp.colorDisable} />
            </TouchableOpacity>
        )
    }

    renderJoin(){
        return(
            <TouchableOpacity 
                onPress={() =>{this.setState({isLoad:true},() => this.joinGroup()) }}
                style={{
                    position:'absolute', 
                    justifyContent:'center', 
                    alignItems:'center',
                    top:15 ,
                    right:0,
                    backgroundColor:'#FFFFFF',
                    
                    }}>
                    <View style={{
                        height:25,
                        justifyContent:'center', 
                        alignItems:'center',
                        borderWidth:1,
                        borderRadius:5,
                        borderColor:settingApp.blueSky
                        }}>
                        <Text style={{color:settingApp.blueSky, fontSize:12, padding:3}}>Tham gia</Text>
                    </View>
            </TouchableOpacity>
        )
    }

    loadDing(){
        return(
            <View style={{
                position: 'absolute',
                top: 0,
                right: 0,
                bottom: 0,
                backgroundColor: 'rgba(255,255,255,0)',
                justifyContent: 'center',
                alignItems: 'center',
            }}>
                <ActivityIndicator size='small' color={settingApp.color} />
            </View>
            )
    }

    renderContent(){
        const { item } = this.state;
        let title = UtilGroups.checkTypeGoup(item.contentType)
        return(
            <View style={{justifyContent:'space-between', paddingTop:5, paddingBottom:5, width:(settingApp.width*0.62)}}>
                <Text  
                    numberOfLines={2}
                    style={{ color:settingApp.colorText, fontSize:18, width:(settingApp.width*0.6), fontWeight:'600'}}
                >{item.name}</Text>
                <View style={{flexDirection:'row', height:30}}>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <SvgCP.iconDocument/>
                        <Text style={styles.text}>{title}</Text>
                    </View>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Text style={styles.text}> | </Text>
                        <SvgCP.icon_Pe0ple/>
                        <Text style={styles.text}>{item.countMember}</Text>
                    </View>
                </View>
            </View>
        )
    }

    checkType(item){
        let { type } = this.state;
        if(item && item.type){
            if(item.type == 1){
                type = 'NHÓM MỞ'
            }
            else{
                type = 'NHÓM KÍN'
            }
        }
        else{
            type= 'Không lấy được dữ liệu'
        }
        this.setState({type})
    }

    render(){
        const { index  } = this.props;
        let { isJoined, isLoad, item } = this.state
        let avatar = this.checkAvatar();
        if(item){
            return(
                <TouchableOpacity 
                    onPress={() => this.props.navigation.navigate('DetailGroup', {data:{...item, isJoined:this.state.isJoined}})}
                    style={{
                        minHeight:80 , 
                        backgroundColor:'#FFFFFF',
                        borderTopWidth:index == 0 ? 0 :1,
                        borderTopColor:index == 0 ?  '#FFFFFF' : settingApp.colorSperator,
                        flexDirection:'row',
                        //marginRight:10
                        }}> 
                    <View style={{paddingTop:10, marginTop:5, paddingRight:5}}>
                        <View style={{width:55, height:55}}>
                            {avatar}
                            <View style={{position:'absolute', bottom:0, left:0}}>
                                {(item.type == 1) ? <SvgCP.icon_UnBlock/> : <SvgCP.icon_Block/>}
                            </View>
                        </View>
                    </View>
                    <View style={{ alignItems:'center', paddingTop:10, paddingBottom:10}}>
                        {item && this.renderContent()}
                    </View>
                    { (!isLoad) ? ((isJoined == true) ? this.renderOption() :  this.renderJoin()) : this.loadDing()}
                </TouchableOpacity>
            )
        }
        else{
            return <View />
        }
        
    }
}

const styles ={
    text:{
        color:settingApp.colorDisable,
        fontSize:14,
        paddingLeft: 5,
    }
}
const mapStateToProps = (state) => ({ ...state })
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(Item);