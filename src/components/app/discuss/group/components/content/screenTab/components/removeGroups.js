import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Alert , ScrollView, KeyboardAvoidingView} from 'react-native';
import { settingApp, SvgCP, Toast, Loading, HeaderScreenNews } from '../../../../../../../../public';
import { MaterialIcons } from '@expo/vector-icons';

class RemoveGroups extends Component{
    constructor(props){
        super(props)
        this.state={
            disabled:true,
            types:0,
            setNote:false,
            note:''
        }
        this.actions = props.navigation.state.params.actions
    }

    checkOption(option, title){
        if(option == 3 || option==4){
            this.setState({
                setNote:true,
                disabled:(this.state.note.trim().length == 0) ? true : false
            })
        }
        else{
            this.setState({disabled:false, note:title})
        }
    }

    accept(){
        const { types, note} = this.state
        if((types == 3) && (note.trim().length == 0)){
            Alert.alert(
                'Thông báo',
                `Vui lòng nhập rõ lý do`,
                [
                    { text: 'Xác nhận', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: false }
            )
        }
        else{
            let value= { 
                    type:this.state.types,
                    note:this.state.note
                }
            this.actions(value)
            this.props.navigation.pop()
        }
    }

    renderHeader(){
        return(
            <HeaderScreenNews 
                buttonLeft={
                    <TouchableOpacity
                        onPress={() => this.props.navigation.pop()}
                        style={{
                            width:50, height:44, justifyContent:'center', alignItems:'center'
                        }}
                    >
                        <MaterialIcons  name='close' size={25} color={settingApp.colorDisable} />
                    </TouchableOpacity>
                }
                title= {'Thông báo'}
                buttonRight = {
                    <TouchableOpacity
                        disabled={this.state.disabled}
                        onPress={() => this.accept()}
                        style={{
                            height:44, 
                            justifyContent:'center',
                            alignItems:'center', 
                            opacity:(this.state.disabled ? 0.3 : 1),
                            marginRight:10
                        }}
                    >
                        <Text style={{fontSize:14, color:'#FC625D'}}>Xác nhận</Text>
                    </TouchableOpacity>
                }
                navigation={this.props.navigation}
            />
        )
    }

    renderOption(title, option){
        let { types } = this.state
       return(
        <TouchableOpacity 
            onPress={() => this.setState({types:option},() =>{
                this.checkOption(option, title)
            })}
            style={styles.button}> 
                <View style={{flexDirection:'row', width:30, height:44}}>
                    <MaterialIcons name={types == option ? 'radio-button-checked' : 'radio-button-unchecked'} size={20} color={types == option ? settingApp.blueSky : settingApp.colorSperator} />
                </View>
                <View style={{height:44, paddingLeft:5}}>
                    <Text style={styles.text}>{title}</Text>
                </View>
        </TouchableOpacity>
       )
    }

    renderNote(){
        let { note } = this.state;
        return(
            <TextInput
                autoCorrect={false}
                placeholder='Lý do xóa nhóm'
                style={{ 
                    padding: 10, 
                    borderColor: settingApp.blueSky, 
                    borderWidth: 1, 
                    borderRadius: 5,
                    fontSize:14,
                    minHeight:44,
                    justifyContent:'center'
                }}
                multiline={true}
                value={this.state.note}
                onChangeText={(note) => this.setState({note},() => this.checkOption(4))}
                underlineColorAndroid='transparent'
            />
        )
    }

    render(){
        let { setNote, types } = this.state
        return(
            <View style={{flex:1, backgroundColor:'#FFFFFF'}}>
                {this.renderHeader()}
                <KeyboardAvoidingView behavior="padding" style={{flex:1}}>
                    <ScrollView style={{flex:1, padding:15, marginBottom:20}}>
                        <Text style={styles.text}
                        >Nhóm và các chủ đề trong nhóm sẽ bị xóa hoàn toàn trên hệ thống và không thể khôi phục.</Text>
                        <Text
                            style={[styles.text,{paddingTop:10, paddingBottom:10}]}
                        >Bạn có chắc chắn muốn xoá nhóm này?</Text>
                        <Text
                            style={[styles.text,{paddingTop:10, paddingBottom:10}]}
                        >Vui lòng chọn lý do xóa:</Text>

                        {this.renderOption('Đã lâu không có tương tác', 1)}
                        {this.renderOption('Không phù hợp quy định công ty', 2)}
                        {this.renderOption('Lý do khác', 3)}

                        {(setNote && (types == 3)) && this.renderNote()}
                    </ScrollView>
                </KeyboardAvoidingView>
            </View>
        )
    }
}

const styles = {
    text:{
        color:settingApp.colorText,
        fontSize:14,
    },
    button:{
        width:(settingApp.width-30), 
        height:50, 
        flexDirection:'row',
        alignItems:'center'
    },
}
export default RemoveGroups;