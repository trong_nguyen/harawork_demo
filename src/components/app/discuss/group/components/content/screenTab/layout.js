import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, ScrollView, Alert, RefreshControl, ActivityIndicator } from 'react-native';
import { settingApp, iconGroups, Toast, Loading, ToastMess } from '../../../../../../../public';
import Item from './components/item';
import Modal from '../../modalOption';
import { AntDesign } from '@expo/vector-icons';

class Layout extends Component{
    endList(){
        let { isLoadMore  } = this.state
        let { totalPage, page } = this
        let show = page < totalPage;
        return(
            <View
                style={{
                    backgroundColor:'#FFFFFF',
                    width:settingApp.width-30,
                    height:60,
                    justifyContent:'center',
                    alignItems:'center',
                }}
            >
               {show && ((isLoadMore == true) ? this.renderMore() :
                    <View/>
                ) }
                
            </View>
        )
    }

    renderMore(){
        return(
            <View style={{
                height:44, 
                width:50,
                justifyContent: 'center',
                alignItems: 'center',
            }}>
                <ActivityIndicator size='small' color={settingApp.color} />
            </View>
        )
    }

    render(){
        let { listData } = this.state;
        let { isShow, isLoading } = this.state;
        if((listData && listData.length > 0) && !isLoading ){ 
            return(
                <View style={{width:(settingApp.width-30), minHeight:settingApp.width, backgroundColor:'#FFFFFFF'}}>
                    <FlatList 
                        data={this.state.listData}
                        style={{width:(settingApp.width-30), backgroundColor:'#FFFFFF', minHeight:settingApp.width}}
                        extraData={this.state}
                        keyExtractor={(item, index) => `${item.id}`}
                        onEndReachedThreshold={1}
                        onEndReached={() => this.loadMore()}
                        renderItem={({item, index}) => 
                            <Item 
                                item={item}
                                index={index}
                                navigation ={ this.navigation }
                                colleague = {this.colleague} 
                                isShow={(show, item) => this.setState({isShow:show, item:item})}
                                itemChange={this.state.item}
                            />
                        }
                        ListFooterComponent={this.endList()}
                    />
                    <Modal 
                        visible = {isShow}
                        item = {this.state.item}   
                        isShow={isShow}
                        close={() => this.setState({isShow:false})}
                        checkOption={(item, type) => this.checkOption(item, type)}
                    />
                </View>
            )
        }
        else if((listData && listData.length ==0) && !isLoading ){
            return(
                <View style={{width:(settingApp.width-30) ,height:settingApp.width, justifyContent:'center', alignItems:'center', paddingRight:10}}>
                    <iconGroups.ArtWorks/>
                    <Text style={{ textAlign:'center',fontSize:18, color:settingApp.colorText, fontWeight:'600', marginTop:10}}>{`Rất tiếc, hiện chưa có \n nhóm nào`}</Text>
                    {/* <TouchableOpacity
                        style={{
                            width:settingApp.width*0.8,
                            height:50,
                            backgroundColor:settingApp.blueSky,
                            borderRadius:5,
                            ...settingApp.shadow,
                            justifyContent:'center', 
                            alignItems:'center',
                            marginTop:10
                        }}
                    >
                        <Text style={{fontSize:16, color:'#FFFFFF', fontWeight:'600'}}>+ Thêm nhóm mới</Text>
                    </TouchableOpacity> */}
                </View>
            )
        }
        else{
            return(
                <View style={{width:settingApp.width-30, height:60}}>
                    <Loading />
                </View>
            )
        }
        
    }
}
export default Layout;