import React, { Component } from 'react';
import { View, TouchableOpacity, Text, ScrollView } from 'react-native';
import { settingApp, } from '../../../../../../../public';
import ListGroups from '../screenTab'

class Layout extends Component{

    renderTab(value, title){
        let { tab } = this.state
        let check = tab == value
        return(
            <TouchableOpacity
                onPress={() => this.setState({tab:value},() => this.checkScroll(value))}
                style={{
                    width:(settingApp.width*0.35), 
                    height:40, 
                    backgroundColor: check ? settingApp.blueSky : "#FFFFFF",
                    justifyContent:'center',
                    alignItems:'center',
                    borderRadius:20,
                    }}
            >
                <Text 
                    style={{fontSize:14, color:check? '#FFFFFF': settingApp.colorDisable, fontWeight:"600"}}
                >{title}</Text>
            </TouchableOpacity>
        )
    }

    render(){
        let {tab} = this.state;
        return(
            <View style={{flex:1, padding:15}}>
                <ScrollView
                    ref={refs => this.ScrollView = refs}
                    style={{height:50, backgroundColor:'#FFFFFF',}}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                >
                    {this.renderTab(0, 'Của tôi')}
                    {this.renderTab(1, 'Khám phá')}
                    {this.renderTab(2, 'Quản lý')}
                    {(this.userInfo.role && (this.userInfo.role === "admin")) && this.renderTab(3, 'Admin')}
                </ScrollView>
                <View style={{width:(settingApp.width-30), minWidth:settingApp.width}}>
                    {tab == 0  && <ListGroups 
                            navigation={this.props.navigation}
                            url="list_groups"
                            colleague={this.colleague}
                        />
                    }
                    {tab == 1  && <ListGroups 
                            navigation={this.props.navigation}
                            url="list_discoveries"
                            colleague={this.colleague}
                        />
                    }
                    {tab == 2  && <ListGroups 
                            navigation={this.props.navigation}
                            url="list_manager"
                            colleague={this.colleague}
                        />
                    }
                    {(tab == 3)  &&  <ListGroups 
                            navigation={this.props.navigation}
                            url="list_admin"
                            colleague={this.colleague}
                        />
                    }
                </View>
            </View>
        )
    }
}
export default Layout;