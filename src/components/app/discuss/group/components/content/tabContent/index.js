import Layout from './layout';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../../../../state/action';

class TabContent extends Layout{
    constructor(props){
        super(props)
        this.state={
            tab:0
        }
        this.colleague= props.app.colleague;
        this.userInfo = props.app.authHaravan.userInfo
    }

    checkScroll(value){
        if(value == 2){
            this.ScrollView.scrollToEnd()
        }
        else if( value == 1){
            this.ScrollView.scrollTo({x:0, y:0})
        }
    }
}
const mapStateToProps = (state) => ({ ...state })
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(TabContent);