import Layout from './layout';
import { HaravanIc, HaravanHr } from '../../../../../../Haravan'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../../../state/action';
import * as Utils from '../../../components/Utils'

class ListHeader extends Layout{
    constructor(props){
        super(props)
        this.state={
            listHeader: [],
            loadMore:false,
            isLoading:true,
            listLoad: [{ id: 0, value: null }, { id: 1, value: null }]
        }
        this.collegue = props.app.colleague;
    }

    componentDidMount(){
        this.loadData()
    }

    async loadData(){
        let body = await Utils.GetBody(this.collegue)
        const { listHeader, isLoading } = this.state;
        let result = await HaravanIc.GetListFeatured(body);
        if(result && result.data && !result.error){
            this.setState({listHeader:result.data}, () =>{
                this.setState({isLoading:false})
            })
        }
        else{
            this.setState({isLoading:false})
        }
    }

}
const mapStateToProps = (state) => ({ ...state })
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(ListHeader);