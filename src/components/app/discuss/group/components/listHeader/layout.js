import React, { Component } from 'react';
import { View, TouchableOpacity, FlatList, StyleSheet, Text } from 'react-native';
import Item from './component/item';
import ItemLoad from './component/itemLoad'
import { settingApp } from '../../../../../../public';

class Layout extends Component{
    render(){
        const { listHeader, listLoad , isLoading} = this.state;
        if(!isLoading && (listHeader.length > 0)){
            return(
                <View style={{ flex:1}}>
                    <View style={styles.viewTextHotGroup}>
                        <Text style={styles.textHotGroups}>Nhóm nổi bật</Text>
                    </View>
                    <FlatList 
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        horizontal={true}
                        data={listHeader}
                        extraData={this.state}
                        keyExtractor={(item, index) =>(index+'')}
                        renderItem={(obj) => this.renderItem(obj)}
                        onEndReachedThreshold={1}
                    />
                </View>
            )
        }
        else if(!isLoading && (listHeader.length == 0)){
            return(
                <View/>
            )
        }
        else{
            return(
                <View style={{ flex:1}}>
                    <View style={styles.viewTextHotGroup}>
                        <Text style={styles.textHotGroups}>Nhóm nổi bật</Text>
                    </View>
                    <FlatList 
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        horizontal={true}
                        data={listLoad}
                        extraData={this.state}
                        keyExtractor={(item, index) =>(index+'')}
                        renderItem={(obj) => this.renderItemLoad(obj)}
                        onEndReachedThreshold={1}
                    />
                </View>
            )
        }
    }

    renderItemLoad(){
        return(
            <ItemLoad />
        )
    }

    renderItem(obj){
        const { isLoading} = this.state
        const { item, index } = obj
            return(
                <Item
                    item ={item}
                    navigation ={ this.props.navigation }
                />
            )
    }
}

const styles = StyleSheet.create({
    textHotGroups:{
        color:settingApp.colorText, fontSize:20, paddingLeft:10, fontWeight:'bold'
    },
    viewTextHotGroup:{
        width:(settingApp.width-30), 
        height:30, 
        backgroundColor:'#FFFFFF', 
        flexDirection:'row', 
        alignItems:'center', 
        justifyContent:'flex-start', 
        paddingLeft:10
    },
})
export default Layout;