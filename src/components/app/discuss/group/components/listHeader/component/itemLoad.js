import React, { Component } from 'react';
import { View, Platform } from 'react-native';
import { settingApp, lottie } from '../../../../../../../public';
import Lottie from 'lottie-react-native'

class ItemLoad extends Component {

    shouldComponentUpdate() {
        return false;
    }

    render() {
        const margin = Platform.OS === 'android' ? 10 : 5;
        return (
            <View
                style={{
                    width: (settingApp.width * 0.6),
                    height: 220,
                    backgroundColor: '#ffffff',
                    marginBottom: 10,
                    marginTop: 10,
                    marginLeft:10,
                    borderRadius: 3,
                    ...settingApp.shadow,
                }}>
                <Lottie
                    ref={animation => this.animation = animation}
                    style={{ width: (settingApp.width * 0.6), height: 200, marginLeft: margin, marginTop: margin }}
                    source={lottie.listNewItemLoad}
                    resizeMode='cover'
                />
            </View>
        )
    }
}

export default ItemLoad;