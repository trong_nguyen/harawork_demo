import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { settingApp, AsyncImage, iconGroups } from '../../../../../../../public';
import * as Utils from '../../Utils';
import { Entypo } from '@expo/vector-icons'

class Item extends Component{
    constructor(props){
        super(props)
        this.state={
            type: '',
            avatar:null
        }
    }

    componentDidMount(){
        const { item } = this.props;
        // this.checkAvatar(item)
        this.checkType(item)
    }

    checkType(item){
        let { type } = this.state;
        if(item && item.type){
            if(item.type == 1){
                type = 'NHÓM MỞ'
            }
            else{
                type = 'NHÓM KÍN'
            }
        }
        else{
            type= 'Không lấy được dữ liệu'
        }
        this.setState({type})
    }

    checkAvatar(item){
        let avatar = null
        let size = 70
        if(item.avatar){
            avatar = Utils.checkName(item.avatar, size)
        }
        else{
            avatar = Utils.checkContentType(item.contentType, size)
        }
        return avatar
    }

    

    render(){
        const { item } = this.props;
        const { type } = this.state;
        let avatar = this.checkAvatar(item);
        return(
            <TouchableOpacity 
                onPress={() => this.props.navigation.navigate('DetailGroup', {data:item})}
                style={{width:(settingApp.width *0.6), height:210, ...settingApp.shadow, margin:10, borderRadius:5, backgroundColor:'#FFFFFF', padding:16}}>
                <View>{avatar}</View>
                <View style={{paddingTop:16}}>
                    <View style={{height:50,marginBottom:25}}>
                        <Text style={{fontSize:14, color:settingApp.blueSky }}>{type}</Text>
                        <Text
                            numberOfLines={2}
                            style={{fontSize:18, color:settingApp.colorText, fontWeight:'bold'}}
                        >{item.name ?  item.name :  'Đang lấy được dữ liệu...'}{item.description ?  ' - '+item.description : ''}</Text>
                    </View>
                
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Text style={{ fontSize:14, color:settingApp.colorDisable, fontWeight:'bold'}}
                        >{item.countMember ? item.countMember : 0}</Text>
                        <Text style={{ fontSize:14, color:settingApp.colorDisable}}> thành viên</Text>
                        <Entypo name='dot-single' color={settingApp.colorDisable} size={15} />
                        <Text style={{ fontSize:14, color:settingApp.colorDisable , fontWeight:'bold'}}
                            >{item.totalThread ? item.totalThread : 0} </Text>
                        <Text style={{ fontSize:14, color:settingApp.colorDisable}}>chủ đề</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}
export default Item;