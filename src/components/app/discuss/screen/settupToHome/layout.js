import React, { Component } from 'react'
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, Switch, ActivityIndicator, Image, TextInput } from 'react-native';
import { HeaderScreenNews, settingApp, SvgCP } from '../../../../../public';
import AttachMent from './component/attachment'

class Layout extends Component{

    renderHeader(){
        return(
            <HeaderScreenNews
                navigation={this.props.navigation}
                title={'Cấu hình bảng tin'}
                buttonRight={
                    <TouchableOpacity
                        onPress={() => this.setState({isPosting:true},() =>{
                            this.isPin()
                        })}
                        disabled={this.state.disabled}
                        style={styles.buttonHeader}
                    >
                        <Text style={{fontSize:14, color:settingApp.blueSky, opacity:(this.state.disabled ? 0.4 : 1)}}>Lưu</Text>
                    </TouchableOpacity>
                }
            />
        )
    }

    renderLoading(){
        return (
            <View style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                backgroundColor: 'rgba(0,0,0,0.5)',
                justifyContent: 'center',
                alignItems: 'center',
            }}>
                <ActivityIndicator size='large' color={settingApp.color} />
            </View>
        )  
    }

    renderButton(){
        return(
            <View style={{flex:1, width:(settingApp.width -30), height:44, flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
                <Text style={{fontSize:14, color:settingApp.blackText}}>Đưa chủ đề ra trang bảng tin</Text>
                <Switch
                    onValueChange={() => this.setState({isHome:!this.state.isHome})}
                    trackColor={{false:settingApp.colorSperator, true:'#22C993'}}
                    value={this.state.isHome}
                    ios_backgroundColor={settingApp.colorSperator}
                />
            </View>
        )
    }

    getImage(){
        const {uriIMG} = this.state
        return(
            <View style={{ width:(settingApp.width -30), justifyContent:'center',}}>
                <Text style={{fontSize:14, color:settingApp.blackText, marginBottom:5}}>Hình đại diện</Text>
                {uriIMG ? 
                    <TouchableOpacity
                        activeOpacity={1}
                        onPress={() => this.setState({visible:true })}
                        style={styles.addImage}>
                        <Image
                            source={{uri:`${uriIMG}`}}  
                            style={[styles.addImage,{resizeMode:'contain'}]}                     
                        />
                    </TouchableOpacity>
                    :
                    <TouchableOpacity
                        activeOpacity={1}
                        onPress={() => this.setState({visible:true})}
                        style={styles.addImage}
                        > 
                        <SvgCP.addImage />
                        <Text style={{fontSize:14, color:settingApp.colorDisable}}>Thêm hình đại diện</Text>
                    </TouchableOpacity>
                }
            </View>
        )
    }

    renderDescrip(){
        let { value } = this.state
        let total = (150-value.length)
        let mess =  total < 150 ? 'Còn lại' : 'Tối đa'
        return(
            <View style={{flex:1, width:(settingApp.width -30), marginTop:20}}>
                <View style={{flexDirection:'row', alignItems:'center'}}>
                    <Text style={{fontSize:14, color:settingApp.blackText}}>Mô tả ngắn</Text>
                    <Text style={{fontSize:14, color:settingApp.colorDisable}}>{` (${mess} ${total} ký tự)`}</Text>
                </View>
                <TextInput 
                    value={this.state.value}
                    onChangeText={(value) => this.setState({value:value},() => this.checkDisable())}
                    placeholder='Thêm mô tả ngắn'
                    maxLength={150}
                    multiline={true}
                />
            </View>   
        )
    }

    render(){
        let { value,isPosting } = this.state
        return(
            <View style={{flex:1, backgroundColor:'#FFFFFF'}}>
                {this.renderHeader()}
                <ScrollView
                    style={{flex:1, padding:15}}
                >
                    {this.renderButton()}
                    {this.getImage()}
                    {this.renderDescrip()}
                    
                </ScrollView>
                <AttachMent 
                    isVisible={this.state.visible}
                    close={() => this.setState({ visible: false })}
                    selectFile={async (file) => await this.setState({ file },() => this.getUri())}
                    />
                {isPosting && this.renderLoading()}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    addImage:{
        width:settingApp.width-30, 
        height:(settingApp.width*0.4), 
        backgroundColor:'#F8F8F8', 
        alignItems:'center',
        justifyContent:'center',
        marginTop:10,
    },
    buttonHeader:{
        width:50, 
        height:44, 
        justifyContent:'center', 
        alignItems:'center'
    },
    main: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        paddingBottom: 1,
        alignItems: 'stretch',
    },
    toolbarButton: {
        fontSize: 20,
        width: 28,
        height: 28,
        textAlign: 'center'
    },
    italicButton: {
        fontStyle: 'italic'
    },
    boldButton: {
        fontWeight: 'bold'
    },
    underlineButton: {
        textDecorationLine: 'underline'
    },
    lineThroughButton: {
        textDecorationLine: 'line-through'
    },
})
export default Layout;