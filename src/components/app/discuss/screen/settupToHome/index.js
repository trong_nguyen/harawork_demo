import Layout from './layout';
import { HaravanGraphQL, HaravanIc } from '../../../../../Haravan';
import { Alert } from 'react-native';

class SettupToHome extends Layout{
    constructor(props){
        super(props)
        this.state={
            disabled:true,
            isHome:false,
            uriIMG:null,
            visible:false,
            file:[],
            value:'',
            isLoadIMG:false,
        }

        this.data = props.navigation.state.params.data;
        this.actions = props.navigation.state.params.actions
    }

    componentDidMount(){
        this.setDefaul()
    }

    setDefaul(){
        const { data } = this
        if(data && data.thread && data.thread){
            this.setState({ uriIMG:data.banner })
        }
    }

    async isPin(){
        let { value, uriIMG , isHome} = this.state
        let listUri = await this.getUrl();
        let body={
            imageUrl:listUri[0].file.url,
            isActive: isHome,
            shortDescription: value,
        }
        const result = await HaravanIc.SetPinHome(this.data.id, body)
        if(result && !result.error){
            this.actions(isHome)
            this.props.navigation.pop()
        }
        else{
            Alert.alert(
                'Thông báo',
                `Cấu hình ra trang bảng tin thất bại. \n Lý do: Lỗi hệ thống. \n Đề xuất: Thử lại lần nữa `,
                [
                    { text: 'Đồng ý', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: true }
            )
        }
    }

    checkDisable(){
        let { value, uriIMG } = this.state
        if(value && (value.trim().length > 0) && uriIMG){
            this.setState({ disabled: false })
        }
    }

    getUri(){
        const { file } = this.state;
        let input = new FormData();
        if(file && file.length > 0 ){
            for (let i = 0; i < file.length; i++) {
                input.append("files",{
                    uri: file[i].uri,
                    type: `image/${file[i].fileName.substr(file[i].fileName.lastIndexOf('.') + 1)}`,
                    name: file[i].fileName,
                });
                this.setState({uriIMG:file[i].uri},() =>{
                    this.checkDisable()
                })
            }
        }
    }

    async getUrl(){
        const { file } = this.state;
        let listUrl = []
        let input = new FormData();
        if(file && file.length > 0 ){
            for (let i = 0; i < file.length; i++) {
                input.append("files",{
                    uri: file[i].uri,
                    type: `image/${file[i].fileName.substr(file[i].fileName.lastIndexOf('.') + 1)}`,
                    name: file[i].fileName,
                });
                this.setState({uriIMG: file[i].uri})
            }
            const response = await HaravanIc.postImage(input)
            if(response){
                response.map(e =>{
                    listUrl.push(e)
                })
                this.setState({ isLoadIMG: false })
            }
            else{
                this.setState({ isLoadIMG:false})
            }
        }
        else{
            listUrl = []
            this.setState({isLoadIMG:false})
        }
        return listUrl
    }
}
export default SettupToHome;