import Layout from './layout';
import { Alert, Keyboard, Animated } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../../state/action';
import { HaravanGraphQL, HaravanIc, HaravanHr } from '../../../../../Haravan';
import { settingApp } from '../../../../../public';

class CreateThread extends Layout{
    constructor(props){
        super(props)
        this.state ={
            file:null,
            visible:false,
            title:'',
            value:null,
            uriIMG:null,
            listUrl:[],
            disabled: true,
            isLoadIMG: false,
            creating:false,
            visibleContent:false,
            editor:false
        }
        this.editor = null;
        this.data = props.navigation.state.params.data;
        this.colleague = props.app.colleague;
        this.actions = props.navigation.state.params.actions;
        
    }

    componentDidMount(){
        this.setDefaul()
    }

    setDefaul(){
        const { data } = this
        if(data){
            this.setState({value:data.content, title:data.name, uriIMG:data.banner, editor:true})
        }
        else{
            this.setState({editor:false})
        }
    }
    
    checkThread(){
        const { editor } = this.state
        if(editor ==true ){
            this.editThread()
        }
        else{
            this.saveThreard()
        }
    }

    async editThread(){
        const {  value, title }  = this.state
        const{ data } = this;
        let listUri = await this.getUrl()
        let body={
            attachments: [],
            content: value,
            groupId: data.groupId ? data.groupId :  data.group.id,
            name: title,
        }
        if((listUri && listUri.length > 0)){
            body ={
                ...body,
                banner:listUri[0].file.url,
            }
            this.props.actions.discuss.changeImage({url:listUri[0].file.url, data:data})
        }
        else{
            body = body
        }
        let id = data.id ?  data.id : data.thread.id
        const result = await HaravanIc.EditThread(body, id)
        if(result && !result.error){
            this.setState({creating:false})
            this.actions(true)
            this.props.navigation.pop()
        }
        else{
            Alert.alert(
                'Thông báo',
                `Tạo chủ đề thât bại. \nLý do: Lỗi hệ thống. \nĐề xuất: Thử lại lần nữa.`,
                [
                    { text: 'Xác nhận', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: true }
            )
        }
    }

    async saveThreard(){
        const {  value, title }  = this.state
        const{ colleague, data } = this;
        let listUri = await this.getUrl()
        let body = {
            attachments: [],
            content: value,
            groupId:data.id,
            name:title,
            positionInfos:[{...colleague}]
        }
        if(listUri && listUri.length > 0){
            body ={
                ...body,
                banner:listUri[0].file.url,
            }
        }
        else{
            body = body
        }
        const result = await HaravanIc.CreateThread(body)
        if(result && !result.error){
            this.setState({creating:false})
            this.actions(true)
            this.props.navigation.pop()
        }
        else{
            Alert.alert(
                'Thông báo',
                `Tạo chủ đề thât bại. \nLý do: Lỗi hệ thống. \nĐề xuất: Thử lại lần nữa.`,
                [
                    { text: 'Xác nhận', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: true }
            )
        }
    }

    checkDisable(){
        let { title } = this.state
        let checkHtml = (this.state.value && this.state.value.length > 0)
        if(checkHtml && (title.trim().length > 0)){
            this.setState({ disabled: false })
        }
        else if((JSON.stringify(this.state.uriIMG) !== JSON.stringify(this.data.banner))
        && (checkHtml && (title.trim().length > 0))
        ){
            this.setState({ disabled: false })
        }
    }

    async getUrl(){
        const { file } = this.state;
        let listUrl = []
        let input = new FormData();
        if(file && file.length > 0 ){
            for (let i = 0; i < file.length; i++) {
                input.append("files",{
                    uri: file[i].uri,
                    type: `image/${file[i].fileName.substr(file[i].fileName.lastIndexOf('.') + 1)}`,
                    name: file[i].fileName,
                });
                this.setState({uriIMG: file[i].uri})
            }
            const response = await HaravanIc.postImage(input)
            if(response){
                response.map(e =>{
                    listUrl.push(e)
                })
                this.setState({ isLoadIMG: false })
            }
            else{
                this.setState({ isLoadIMG:false})
            }
        }
        else{
            listUrl = []
            this.setState({isLoadIMG:false})
        }
        return listUrl
    }

    getUri(){
        const { file } = this.state;
        let input = new FormData();
        if(file && file.length > 0 ){
            for (let i = 0; i < file.length; i++) {
                input.append("files",{
                    uri: file[i].uri,
                    type: `image/${file[i].fileName.substr(file[i].fileName.lastIndexOf('.') + 1)}`,
                    name: file[i].fileName,
                });
                this.setState({uriIMG:file[i].uri},() =>{
                    this.checkDisable()
                })
            }
        }
    }

    getHTML(html){
        this.setState({value:html},() => this.checkDisable())
    }

    componentWillUnmount(){
        this.props.actions.discuss.changeImage(null)
    }
}
const mapStateToProps = (state) => ({ ...state })
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(CreateThread);