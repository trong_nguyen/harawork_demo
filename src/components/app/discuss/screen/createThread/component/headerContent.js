import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StatusBar, Platform  } from 'react-native';
import { settingApp } from '../../../../../../public';
import { Ionicons } from '@expo/vector-icons';

class HeaderContent extends Component{
    constructor(props){
        super(props)
    }

    buttonLeft(){
        return(
            <TouchableOpacity
                onPress={() => {
                    this.props.navigation.pop()}}
                style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                    <Ionicons name='ios-arrow-back' size={23} color='#000000' />
            </TouchableOpacity>
        )
    }

    render() {
        let { title, buttonRight, buttonLeft, navigation } = this.props;
        let { width, statusBarHeight } = settingApp;
        title = title || '';
        buttonRight = buttonRight || <View style={{ width: 50, height: 44 }} />;
        buttonLeft = buttonLeft || this.buttonLeft();
        return (
            <View style={{
                backgroundColor:'#FFFFFF',
            }}>
                <StatusBar barStyle={Platform.OS === 'ios' ? 'dark-content' : 'default'}/>
                <View style={{ width, height:Platform.OS === 'ios' ?  statusBarHeight : 0, backgroundColor:'#FFFFFF' }} />
                <View style={{ width, height: 44, flexDirection: 'row', alignItems:'center', justifyContent:'space-between'}} >
                        <View style={{ flex: 1, backgroundColor: 'transparent', flexDirection: 'row', justifyContent:'space-between', alignItems:'center' }}>
                            {buttonLeft}
                            <View style={{justifyContent:'center', alignItems:'center', width:settingApp.width*0.65}}>
                                <Text 
                                    numberOfLines={1}
                                    style={{ fontSize: 16, color: '#000000', fontWeight: 'bold'}}>
                                        {title}
                                </Text>
                            </View>
                            {buttonRight}
                        </View>
                </View>
            </View>
        )
    }

}   
export default HeaderContent;