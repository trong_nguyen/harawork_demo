import Layout from './layout';
import { Animated, Keyboard } from 'react-native';

class ProductDescription extends Layout {

    constructor(props) {
        super(props);

        this.state = {
            headerChange: false,
            isbold: false,
            isitalic: false,
            isunderline: false,

            indexJustify: 0,
            expandJustify: false,

            indexInsert: null,
            expandInsert: false,

            bottom: new Animated.Value(0),
            keyShow:false
        };

        this.listFont = ['bold', 'italic', 'underline'];
        this.listJustify = ['justifyLeft', 'justifyCenter', 'justifyRight'];
        this.listInsert = ['insertUnorderedList', 'insertOrderedList'];


        String.prototype.replaceAll = function (search, replacement) {
            var target = this;
            return target.replace(new RegExp(search, 'g'), replacement);
        };

        this.html = props.data;
    }

    componentDidMount() {
        this.keyboardDidShowListener = Keyboard.addListener(
            'keyboardWillShow',
            this._keyboardShow.bind(this),
        );
        this.keyboardDidHideListener = Keyboard.addListener(
            'keyboardWillHide',
            this._keyboardHide.bind(this),
        );
    }

    _keyboardShow(e) {
        const keyBoardHeight = e.endCoordinates.height;
        this.setState({keyShow:true}, () =>{
            Animated.timing(
                this.state.bottom,
                {
                    toValue: keyBoardHeight,
                    duration: 350,
                    delay: 150
                },
            ).start();
        })
        
    }

    _keyboardHide() {
        this.setState({keyShow:false} , () =>{
            Animated.timing(
                this.state.bottom,
                {
                    toValue: 0,
                    duration: 0
                },
            ).start();
        })
        
    }

    saveContent() {
        if(!this.html && this.props.data){
            this.props.getHTML(this.props.data);
            this.props.close()
        }
        else{
            this.props.getHTML(this.html);
            this.props.close()
        }
    }

    execCommand(value) {
        const status = this.state[`is${value}`];
        this.setState({ [`is${value}`]: !status, headerChange: true });
        this.webview.injectJavaScript(`document.execCommand('${value}', false, '')`);
    }

    onMessage(event) {
        const { data } = event.nativeEvent;
        if (data.indexOf('_onkeydown') > -1) {
            const html = data.replace('_onkeydown', '');
            this.html = html;
            if (!this.state.headerChange) {
                this.setState({ headerChange: true });
            }
        }
        if (data.indexOf('_status') > -1) {
            let status = data.split(' ');
            status = JSON.parse(status[1]);
            const { isbold, isitalic, isunderline, indexJustify, indexInsert } = status;
            this.setState({ isbold, isitalic, isunderline, indexJustify, indexInsert });
        }

        if(data && (data.indexOf('_onkeydown') < 0) && (data.indexOf('_status') <0)){
            this.html = data;
        }
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
        this.setState({keyShow:false})
    }
}

export default ProductDescription;