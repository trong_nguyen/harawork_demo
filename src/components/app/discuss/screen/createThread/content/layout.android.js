import React, { Component } from 'react';
import {
    View,
    WebView,
    TouchableOpacity,
    KeyboardAvoidingView,
    Modal,
    Text,
    StyleSheet
} from 'react-native';
import { FontAwesome, MaterialIcons } from '@expo/vector-icons';
import { settingApp } from '../../../../../../public';
import HeaderContent from '../component/headerContent';
import * as _ from 'lodash';

const WEBVIEW_REF = 'registration';
const sourceHTML = require('./index.html');

class Layout extends Component {
    renderHeader(){
        return(
            <HeaderContent 
                title={'Nội dung'}  
                buttonLeft={
                    <TouchableOpacity
                        onPress={() => this.props.close()}
                        style={[styles.button,{paddingLeft:15}]}
                        >
                        <Text style={styles.text}>Hủy</Text>
                    </TouchableOpacity>
                }
                buttonRight={
                    <TouchableOpacity 
                        onPress={() => this.saveContent()}
                        style={styles.button}>
                        <Text style={styles.text}>Lưu</Text>
                    </TouchableOpacity>
                }
            />
        )
    }

    renderButton() {
        let {
            indexJustify, expandJustify,
            indexInsert, expandInsert
        } = this.state;
        return (
            <View
                style={{
                    position: 'absolute',
                    right: 0,
                    left: 0,
                    bottom: 0,
                    height: 50,
                    backgroundColor: '#ffffff',
                    ...settingApp.shadow
                }}>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    {this.listFont.map((e, i) => (
                        <TouchableOpacity
                            key={i}
                            onPress={() => this.execCommand(e)}
                            style={{
                                width: 40,
                                height: 40,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 3,
                                marginRight: 5,
                                backgroundColor: this.state[`is${e}`] ? settingApp.blueSky : 'transparent'
                            }}>
                            <FontAwesome name={e} color={this.state[`is${e}`] ? '#ffffff' : '#000'} size={15} />
                        </TouchableOpacity>
                    ))}

                    <View style={{ width: 1, height: 40, backgroundColor: settingApp.colorSperator, marginRight: 5 }} />

                    {this.listJustify.map((e, i) => {
                        let name = 'format-align-left';
                        if (e === 'justifyCenter') name = 'format-align-center';
                        if (e === 'justifyRight') name = 'format-align-right';
                        const isActive = indexJustify === i;
                        return (
                            <TouchableOpacity
                                key={i}
                                onPress={() => {
                                    if (!expandJustify) {
                                        this.setState({ expandJustify: true, expandInsert: false })
                                    } else {
                                        this.setState({ indexJustify: i });
                                        this.execCommand(e);
                                    }
                                }}
                                style={{
                                    width: !expandJustify && !isActive ? 0 : 40,
                                    height: 40,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    borderRadius: 3,
                                    marginRight: !expandJustify && !isActive ? 0 : 5,
                                    backgroundColor: !!(expandJustify && isActive) ? settingApp.blueSky : 'transparent'
                                }}>
                                <MaterialIcons name={name} color={!!(expandJustify && isActive) ? '#ffffff' : '#000'} size={15} />
                            </TouchableOpacity>
                        )
                    })}

                    <View style={{ width: 1, height: 40, backgroundColor: settingApp.colorSperator, marginRight: 5 }} />

                    {this.listInsert.map((e, i) => {
                        let name = 'format-list-bulleted';
                        if (e === 'insertOrderedList') name = 'format-list-numbered';
                        const isActive = indexInsert === i;
                        return (
                            <TouchableOpacity
                                key={i}
                                onPress={() => {
                                    if (!expandInsert) {
                                        this.setState({ expandInsert: true, expandJustify: false });
                                    } else {
                                        this.setState({ indexInsert: i });
                                        this.execCommand(e);
                                    }
                                }}
                                style={{
                                    width: expandInsert ? 40 : isActive ? 40 : !!(i === 0 && !indexInsert) ? 40 : 0,
                                    height: 40,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    borderRadius: 3,
                                    marginRight: expandInsert ? 5 : isActive ? 5 : !!(i === 0 && !indexInsert) ? 5 : 0,
                                    backgroundColor: isActive && expandInsert ? settingApp.colorItemActive : 'transparent'
                                }}>
                                <MaterialIcons name={name} color={isActive && expandInsert ? '#ffffff' : '#000'} size={15} />
                            </TouchableOpacity>
                        )
                    })}

                </View>
            </View>
        )
    }

    render() {
        let html = this.props.data ? this.props.data : '<b/>';
        //html = html.replaceAll('min-width: 500px', '');
        let content = _.unescape(typeof (html) == 'string' ? html : '')
        const jsCode = `
            (function(){
                setTimeout(function() {
                var tagMain  = document.querySelectorAll('#main');
                if(tagMain && tagMain.length > 0) {
                    tagMain[0].innerHTML = '${content}';
                }
              }, 0);
            })();
        `;
        return (
            <KeyboardAvoidingView style={{ flex: 1 }} behavior='padding'>
                  <Modal 
                    visible={this.props.visible}
                    transparent={false}
                    animationType='slide'
                    onRequestClose={() => this.props.close()}
                    style={{ flex: 1 }} >
                        {this.renderHeader()}
                        <WebView
                            ref={refs => this.webview = refs}
                            originWhitelist={['*']}
                            style={{marginBottom:50, overflow:'hidden'}}
                            source={sourceHTML}
                            javaScriptEnabled={true}
                            injectedJavaScript={jsCode}
                            onMessage={
                                event => this.onMessage(event)
                                // (event) => console.log(event.nativeEvent.data)
                            }
                            domStorageEnabled={true}
                            startInLoadingState={true}
                            scalesPageToFit={true}
                            automaticallyAdjustContentInsets={true}
                        />
                        {this.renderButton()}
                    </Modal>
            </KeyboardAvoidingView>
        );
    }
}
const styles = StyleSheet.create({
    button:{
        width:50, 
        height:44, 
        backgroundColor:'#FFFFFF',
        justifyContent:'center',
    },
    text:{
        fontSize:14, 
        color:settingApp.blueSky
    }
})
export default Layout;