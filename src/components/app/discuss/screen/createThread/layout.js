import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, TextInput, StyleSheet, KeyboardAvoidingView, ActivityIndicator, Image, Animated } from 'react-native';
import { HeaderScreenNews, settingApp, SvgCP, Loading, HTMLView, ParseText } from '../../../../../public';
import {AntDesign, MaterialCommunityIcons} from '@expo/vector-icons';
import AttachMent from './component/attachment';
import Content from './content'

class Layout extends Component{
    renderHeader(){
        return(
            <HeaderScreenNews
                title={'Tạo chủ đề'}
                buttonLeft={
                    <TouchableOpacity
                        onPress={() => this.props.navigation.pop()}
                        style={styles.buttonHeader}
                    >
                        <AntDesign name='close' size={25} color={settingApp.colorDisable} />
                    </TouchableOpacity>
                }
                buttonRight={
                    <TouchableOpacity
                        onPress={() => this.setState({ isLoadIMG:true, creating:true },() =>{
                            this.checkThread()
                            }) }
                        style={styles.buttonHeader}
                        disabled={this.state.disabled}
                    >
                        <Text style={{fontSize:17, color:settingApp.blueSky, opacity:(this.state.disabled ? 0.4 : 1)}}>Lưu</Text>
                    </TouchableOpacity>
                }
            />
        )
    }

    addImage(){
        const { uriIMG } = this.state
        return(
            <View style={styles.addImage}>
             {uriIMG ? 
                <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => this.setState({visible:true })}
                    style={styles.addImage}>
                    <Image
                        source={{uri:`${uriIMG}`}}  
                        style={[styles.addImage,{resizeMode:'contain'}]}                     
                    />
                </TouchableOpacity>
                :
                <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => this.setState({visible:true})}
                    style={styles.addImage}
                    > 
                    <SvgCP.addImage />
                    <Text style={{fontSize:14, color:settingApp.colorDisable}}>Chọn hình ảnh đại diện</Text>
                </TouchableOpacity>
                }
            </View>
        )
    }

    addTitle(){
        return(
            <View style={{width:settingApp.width, padding:15}}>
                <TextInput
                    value={this.state.title}
                    multiline={true}
                    onChangeText={(title) => this.setState({title},() => this.checkDisable())}
                    placeholder={'Tiêu đề'}
                    style={{fontSize:20, color:settingApp.colorText, fontWeight:'600'}}
                />
            </View>
        )
    }

    renderLoading() {
        return (
            <View style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                backgroundColor: 'rgba(0,0,0,0.5)',
                justifyContent: 'center',
                alignItems: 'center',
            }}>
                <ActivityIndicator size='large' color={settingApp.color} />
            </View>
        )
    }

    renderContent(){
        const { value } = this.state
        if(value && (value.trim().length > 0)){
            return(
                <TouchableOpacity
                    onPress={() => this.setState({visibleContent:true})}
                    activeOpacity={1}
                    style={{flex:1, padding:15}}>
                        <ParseText 
                            description={this.state.value}
                        />
                </TouchableOpacity>
            )
        }
        else{
            return(
                <TouchableOpacity
                    onPress={() => this.setState({visibleContent:true})}
                    style={{justifyContent:'center', backgroundColor:'#FFFFFF', width:settingApp.width, height:60, padding:15}}
                    >
                    <Text style={{fontSize:14, color:settingApp.colorDisable}}>Nhập nội dung...</Text>
                </TouchableOpacity>
            )
        }
        
    }

    render(){
        const { creating, height} = this.state;
        return(
            <View
                style={{flex:1, backgroundColor:'#FFFFFF'}}>
                {this.renderHeader()}
                <ScrollView 
                    keyboardDismissMode={'on-drag'}
                    style={{flex:1}}>
                    {this.addImage()}
                    {this.addTitle()}
                    {this.renderContent()}
                    <Content
                        visible={this.state.visibleContent}
                        close={() => this.setState({visibleContent:false})}
                        getHTML = {(html) => this.getHTML(html)}
                        data={this.state.value}
                    />
                </ScrollView>
                <AttachMent 
                    isVisible={this.state.visible}
                    close={() => this.setState({ visible: false })}
                    selectFile={async (file) => await this.setState({ file },() => this.getUri())}
                    />
                {creating && this.renderLoading()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    addImage:{
        width:settingApp.width, 
        height:(settingApp.width*0.5), 
        backgroundColor:'#DADADA', 
        justifyContent:'center', 
        alignItems:'center'
    },
    buttonHeader:{
        width:50, 
        height:44, 
        justifyContent:'center', 
        alignItems:'center'
    },
    main: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        paddingBottom: 1,
        alignItems: 'stretch',
    },
    toolbarButton: {
        fontSize: 20,
        width: 28,
        height: 28,
        textAlign: 'center'
    },
    italicButton: {
        fontStyle: 'italic'
    },
    boldButton: {
        fontWeight: 'bold'
    },
    underlineButton: {
        textDecorationLine: 'underline'
    },
    lineThroughButton: {
        textDecorationLine: 'line-through'
    },
})
export default Layout;