import React, { Component } from 'react';
import { View, Text, ScrollView, FlatList, TouchableOpacity, WebView, Platform, ActivityIndicator, StyleSheet, KeyboardAvoidingView, Image, Dimensions } from 'react-native';
import { settingApp, HeaderScreenNews, AsyncImage, ParseText, Utils,AvatarCustom, SvgCP } from '../../../../../public'
import { Feather, Ionicons, FontAwesome, MaterialIcons} from '@expo/vector-icons'
import Item from './component/item';
import * as Animatable from 'react-native-animatable';
import { Popover, PopoverController } from 'react-native-modal-popover';
import CNText from './writeComment/CNText';
import GetImage from './getImage';
import Modal from './component/modal';
import Attachments from './component/attachments'; 
import ImageView from './component/ImageView';
import VotePoll from './component/votePoll';
import ModalEdit from './component/modalEdit';

class Layout extends Component{
    buttonAction(){
        return(
            <View style={{flexDirection:'row', width:(settingApp.width), height:50}}>
                <View
                    style={{
                        height:50,
                        width:150,
                        backgroundColor:'#FFFFFF',
                        borderRadius:15,
                        flexDirection:'row',
                        ...settingApp.shadow
                        }}>
                        <Animatable.View
                            duration = {200}
                            delay ={200}
                            animation='bounceIn'
                            style={{width:50, height:50, alignItems:'center', justifyContent:'center'}}
                        >
                            <TouchableOpacity
                                onPress={() => this.checkOption(1)}
                                style={{alignItems:'center', justifyContent:'center'}}
                            > 
                                <SvgCP.isHeart/>
                            </TouchableOpacity>
                        </Animatable.View>
                        
                        <Animatable.View
                            duration = {250}
                            delay ={250}
                            animation='bounceIn'
                            style={{width:50, height:50, alignItems:'center', justifyContent:'center'}}
                        >
                            <TouchableOpacity
                                onPress={() => this.checkOption(3)}
                                style={{alignItems:'center', justifyContent:'center'}}
                            > 
                                <SvgCP.isClap/>
                            </TouchableOpacity>
                        </Animatable.View>
                        <Animatable.View
                            duration = {300}
                            delay ={300}
                            animation='bounceIn'
                            style={{width:50, height:50, alignItems:'center', justifyContent:'center'}}
                        >
                            <TouchableOpacity
                                onPress={() => this.checkOption(2)}
                                style={{alignItems:'center', justifyContent:'center'}}
                            > 
                                <SvgCP.isDisLike/>
                            </TouchableOpacity>
                        </Animatable.View>
                </View>
            </View>
        )
    }

    optionActions(list){
        let checkAction = this.checkAction(list)
        return(
            <PopoverController>
                {({ openPopover, closePopover, popoverVisible, setPopoverAnchor, popoverAnchorRect }) => (
                    <React.Fragment>
                        <TouchableOpacity 
                            onPress={() => this.checkNoneLongPress()}
                            ref={setPopoverAnchor} 
                            onLongPress={openPopover}
                            style={styles.buttonAction}
                        >
                            <View style={{top:-5, justifyContent:'center', alignItems:'center'}}>
                                {checkAction.image}
                            </View> 
                            <Text style={[styles.textAction, {color:checkAction.color}]}>{checkAction.title}</Text>
                        </TouchableOpacity> 
                    <Popover 
                        ref = {ref => this.Popover = ref}
                        contentStyle={styles.content}
                        arrowStyle={styles.arrow}
                        backgroundStyle={styles.background}
                        visible={popoverVisible}
                        onClose={closePopover}
                        fromRect={popoverAnchorRect}
                        supportedOrientations={['portrait', 'landscape']}
                    >   
                        {this.buttonAction()}
                    </Popover>
                    </React.Fragment>
                )}
            </PopoverController>
        )
    }
    renderHeader(){
        const { isStarred, data, isCreate } = this.state 
        return(
            <HeaderScreenNews
                navigation = {this.props.navigation}
                buttonLeft={
                    <TouchableOpacity
                            onPress={() => {
                                this.setState({autoFocus:false},()=>{
                                    (this.actions && this.actions(this.state.data, this.state.isRead))
                                    this.props.navigation.pop()
                                })
                                }}
                            style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                            <Ionicons name='ios-arrow-back' size={23} color='#000000' />
                    </TouchableOpacity>
                }
                buttonRight={
                    <View style={{flexDirection:'row', paddingRight:15}}>
                        <TouchableOpacity 
                            onPress={() => this.setState({isStarred:!this.state.isStarred},() =>{
                                this.isStarred()
                            })}
                            style={{justifyContent:'center', alignItems:'center', width:40, height:44, backgroundColor:'transparent'}}>
                            <MaterialIcons name={isStarred ?'bookmark' :'bookmark-border'} color={isStarred ? settingApp.blueSky: settingApp.colorButon} size={25}/>
                        </TouchableOpacity>
                        {((isCreate==true) || (data && (data.isAdminForum==true))) &&
                            <TouchableOpacity 
                                onPress={() => this.setState({visibleOption:true})}
                                style={{justifyContent:'center', alignItems:'center', width:40, height:44, backgroundColor:'transparent'}}>
                                <Feather name='more-horizontal' color={settingApp.colorButon} size={25}/>
                            </TouchableOpacity> 
                        }
                    </View>
                }
            />
        )
    }

    renderTypeActions(listCheck){
        let index1 = listCheck.findIndex(m => m.type == 1);
        let index2 = listCheck.findIndex(m => m.type == 2)
        let index3 = listCheck.findIndex(m => m.type == 3)
        let pading1 = ((index2 > -1) && (index3 >-1)) ? 50 : (index2 > -1 && index3 <0) ? 30 : 10;
        let pading2 = (index3 > -1) ? 30 : 10;
        const { data } = this.state
        return(
            <View style={{bottom:8}}>
                { (index1 > -1) ?
                    <View style={{position:'absolute', right:pading1, zIndex:90}}>
                        <SvgCP.isHeart/>
                    </View>
                    :
                    <View/>
                }
                { (index3 > -1)  ? 
                    <View style={{position:'absolute', right:pading2, zIndex:80}}>
                        <SvgCP.isClap/>
                    </View>
                    : <View />
                }
                { (index2 > -1)  ? 
                    <View style={{position:'absolute', right:10, zIndex:70}}>
                        <SvgCP.isDisLike/>
                    </View>
                    :<View/>
                }
            </View>
        )
    }
    
    renderAction(list){
        const { data, listAction } = this.state
        return(
            <View style={[styles.coverACtion]}>
                <View style={{ flexDirection:'row', alignItems:'center', marginLeft:10}}>
                    { this.optionActions(list) }
                </View>
                {(listAction.length > 0)?
                    <View style={[styles.buttonAction, {paddingTop:15}]}>
                        {this.renderTypeActions(listAction)}
                        <Text style={styles.textType}>{listAction.length}</Text>
                    </View>
                : <View/>
                }
                
            </View>
        )
    }

    renderPosting(){
        const {  fullName, photo } = this.colleague;
        const {file, isLoadIMG, value, data, isPosting } = this.state;
        let source = this.getSource(file)
        if(isPosting == true){
        return(
            <View style={{ paddingBottom:10 }}>
                <View style={{flexDirection:'row', alignItems:'center'}}> 
                    <FontAwesome name='circle' size={10} color='#FDBD41' />
                    <Text style={{fontSize:12, color:settingApp.colorDisable, paddingLeft:5}}>Đang đăng...</Text>
                </View>

                <View style={{flexDirection:'row', alignItems:'center'}}>
                    {data ?
                        <AvatarCustom userInfo={{ name: fullName, picture: photo }} v={30} fontSize={12}/>
                        : <View/>
                    }
                    <Text style={{ marginTop: 5, fontSize: 14, color: settingApp.blackText, paddingLeft:10, fontWeight:'bold' }}>
                        <Text style={{ color: settingApp.blackText }}>{fullName ? fullName : 'Lấy thông tin thất bại'} </Text>
                    </Text>
                </View>
                <View style={{paddingLeft:5, backgroundColor:'#FFFFFF', justifyContent:'center', width:(settingApp.width*0.85)}}>
                    <ParseText description={value} />
                </View>
                {(file && file.length > 0) &&
                    <View 
                    style={{paddingTop:5,paddingLeft:5, backgroundColor:'#FFFFFF', paddingBottom:10}}>
                        <Image 
                            source={{uri: `${source}`}}
                            style={{resizeMode:'contain', width:120, height:120}}
                        />
                    </View>
                }
            </View>
        )}
    }

    renderNewsComment(){
        const {listComment, showNews} = this.state;
        if(showNews == true){
            return(
                <TouchableOpacity
                    onPress={() => this.getNewCommnet()}
                    style={{
                        position:'absolute',
                        bottom:80,
                        left:((settingApp.width/2) - 75),
                        right:((settingApp.width/2) - 75),
                        backgroundColor:settingApp.blueSky,
                        width:150,
                        height:40,
                        borderRadius:22,
                        zIndex:9999,
                        justifyContent:'center',
                        alignItems:'center'
                    }}
                >
                    <Text style={{fontSize: 16, color:'#FFFFFF'}}>1 bình luận mới</Text>
                </TouchableOpacity>
            )  
        }
    }

    renderComment(){
        const {listComment, isLoadComment, isPosting} = this.state;
        let aestTime = new Date().getTime();
        if(!isLoadComment && (listComment.length > 0)){
            return(
                <View style={{flex:1, paddingLeft:10}}>
                <FlatList 
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    data={listComment}
                    extraData={this.state}
                    keyExtractor={(item, index) =>(item.id+''+this.timeLet)}
                    renderItem={({item, index}) => <Item 
                        item ={ item }
                        index = { index}
                        colleague = { this.colleague }
                        token ={ this.data.id }
                        navigation = { this.props.navigation }
                        autoFocus={''}
                        editComment={(value) => this.setState({editComment:value.editComment, itemEdit:value.item})}
                    />}
                    ListFooterComponent={this.renderPosting()}
                />
                </View>
            )
        }

        else if(!isLoadComment && (listComment.length== 0)){
            return(
                <View style={{flex:1, justifyContent:'center', alignItems:'center', borderTopColor:settingApp.colorSperator, borderTopWidth:1, marginBottom:20}}> 
                    <View style={{flex:1, paddingRight:10}}>
                        <Text style={{ fontSize:14, color:settingApp.colorDisable, paddingTop:10}}>Chưa có bình luận</Text>
                    </View>
                </View>
            )
        }
        else if(!isLoadComment && isPosting && (listComment.length == 0)){
            return(
                <View> 
                    {this.renderPosting()}
                </View>
            )
        }
        else{
            return(
                <View style={{flex:1, justifyContent:'center', alignItems:'center', paddingRight:10}}>
                    <ActivityIndicator size='small' color={settingApp.colorDisable} />
                </View>
            )
        }
    }

    loadMore(){
        return(
            <View style={{
                backgroundColor: 'transparent',
                justifyContent: 'center',
                alignItems: 'center',
            }}>
                <ActivityIndicator size='small' color={settingApp.color} />
            </View>
        )
    }

    renderImage(){
        const { file, isPosting } = this.setState
        let source =this.getSource(file)
        return(
            <View
                style={{
                    width:120, 
                    height:90,
                    backgroundColor:'#FFFFFF',
                    padding:10,
                    marginBottom:5,
                }}
            >
                <View style={{flex:1}}>
                    {!isPosting && <TouchableOpacity
                        onPress={() => this.setState({file: []}, () => this.checkDisable())}
                        style={{
                            position:'absolute',top:-10, right:0, width:30, height:30, zIndex:999, justifyContent:'center', alignItems:'center'}}
                            >
                        <Ionicons name="ios-close-circle" size={20} color={settingApp.colorDisable} />
                    </TouchableOpacity>}
                    <Image 
                        source={{uri:`${source}`}}
                        style={{flex:1, width:undefined, height:undefined, resizeMode:'contain'}} />
                </View>
            </View>
        )
    }

    renderLoadMore(){
        const { loadMore, listComment, totalCount } = this.state;
        if((listComment.length < totalCount)){
            return(
                <TouchableOpacity
                    onPress={() =>{
                        this.setState({loadMore:true},() =>{
                            this.loadComment()
                        })
                        }}
                    style={{height:44, width:(settingApp.width), backgroundColor:'#FFFFFF', padding:10, flexDirection:'row', paddingLeft:15}}
                >
                    <Text style={{fontSize:14, color:settingApp.blueSky}}>Xem các bình luận trước</Text>
                    <View style={{paddingLeft:10, justifyContent:'center', alignItems:'center'}}> 
                        { loadMore && this.loadMore()}
                    </View>
                </TouchableOpacity>
            )
        }
        else{
            return <View />
        }
    }

    render(){
        const {  groupName, createdAt } = this.data;
        const { data, totalCount, file, isPosting, imgUrl } = this.state;
        let total =  totalCount > 0 ? ('('+totalCount +')') : '(0)'
        // let source  = this.data && this.data.createdBy ? Utils.getLinkImage(this.data.createdBy):''
        let name = (data && data.fullName) ? data.fullName : ''
        return(
            <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
                {this.renderNewsComment()}
                <Modal
                    visible={this.state.visibleOption}
                    close={() => this.setState({visibleOption:false})}
                    item ={this.state.data}
                    collegue = {this.colleague}
                    checkOption={(value) => this.checkOptionThread(value)}
                />
                {this.renderHeader()}
                <ScrollView 
                    ref={ref => this.ScrollView = ref}
                    style={{ backgroundColor: '#ffffff'}}>
                    {imgUrl ?
                        <TouchableOpacity 
                            onPress={() => this.setState({showImage:true})}
                            style={{ flex: 1, height: settingApp.imageFullSize }}>
                            <AsyncImage
                                source={{ uri: `${imgUrl}` }}
                                style={{ flex: 1, width: undefined, height: undefined }}
                            />   
                        </TouchableOpacity> : <View />
                        }
                    <View style={{ padding: 10, paddingTop: 20, paddingLeft:15 }}>
                        <Text style={{ fontSize: 14, color: settingApp.colorDisable, fontWeight: 'bold' }}>{groupName ? groupName: ' -- '}</Text>
                        <Text style={{ fontSize: 22, color: '#212121', fontWeight: 'bold', marginTop: 5, lineHeight:26 }}>{(data && data.name) ?  data.name :  ' -- '}</Text>
                        
                        <View style={{flexDirection:'row', alignItems:'center', marginTop:20}}>
                            {data ?
                                <AvatarCustom userInfo={{ name: name , picture:'' }} v={30} fontSize={12}/>
                                : <View/>
                            }
                            <View style={{color: settingApp.blackText, paddingLeft:8, flexDirection:'row', justifyContent:'center'}}>
                                <Text style={{ color: settingApp.blackText,fontWeight:'bold' , maxWidth:(settingApp.width*0.5) }}>
                                    {name} </Text> 
                                {data && <Text style={{color:settingApp.colorDisable, fontSize:14, marginLeft:12}}>{Utils.formatLocalTime(createdAt, 'DD/MM/YYYY hh:mm')}</Text>}
                            </View>
                        </View>
                        { data ? 
                            <View style={{ flex: 1, paddingRight:5 }}>
                                 <ParseText 
                                    style={{width:(settingApp.width -30)}}
                                    description={data.content}/>
                            </View>
                        : <View />
                        }
                        {data && data.poll && <VotePoll
                                data={data.poll}
                                navigation={this.props.navigation}
                                dataId={this.data.id}
                                body={this.body}
                             />}

                        {data && data.attachments && (data.attachments.length > 0) ? 
                        <View style={{width:settingApp.width }}> 
                            <Attachments 
                                attachments = { data.attachments }
                            />
                        </View> : <View /> }
                    </View>
                    <View style={{width:(settingApp.width-30), height:1, backgroundColor:settingApp.colorSperator, marginLeft:15, marginRight:15}}/>
                    {data  ? this.renderAction(data.action) : <View/>}
                    <Text style={{fontSize:18, fontWeight:'bold', padding:10,paddingLeft:15, color:(settingApp.blackText) }}>{'Bình luận'} {total}</Text>
                    {totalCount > 10 ?
                        this.renderLoadMore() :
                        <View/>
                    }
                    <View style={{width:(settingApp.width), paddingLeft:5, paddingRight:20}}>
                        {this.renderComment()}
                    </View>
                    
                </ScrollView>
                <KeyboardAvoidingView behavior='padding'
                    style={{
                        borderTopColor:(settingApp.colorSperator), 
                        borderTopWidth:1,
                    }}
                >
                    {(file.length > 0) && !isPosting && this.renderImage()} 

                    <View style={{
                        backgroundColor:'#FFFFFF', 
                        width:(settingApp.width), 
                        minHeight: 44, 
                        flexDirection:'row',
                        alignItems:'flex-end'
                        }}>
                        <View style={{
                            marginLeft:10, marginRight:10, width:(settingApp.width-100), minHeight:50,
                            justifyContent:'center'
                            }}>
                           <CNText 
                                ref={refs => this.CNText = refs}
                                focus={this.state.autoFocus}
                                setAutoFocus={() => this.setState({autoFocus:false})}
                                setContent={(value) => this.setContent(value)}
                                clear={this.state.clear}
                                checkDisable={(value) => this.checkDisable(value)}
                           />
                            {/* <TextComment 
                                ref={refs => this.TextComment = refs}
                                focus={this.state.autoFocus}
                                setAutoFocus={() => this.setState({autoFocus:false})}
                                setContent={(value) => this.setContent(value)}
                                clear={this.state.clear}
                                checkDisable={(value) => this.checkDisable(value)}
                            /> */}

                        </View>
                        <View style={{width:80, flexDirection:'row', height:44, bottom:5}}> 
                            <TouchableOpacity
                                onPress={() => this.setState({modalVisibleAttachment:true})}
                                style={styles.buttonAddIMG}
                            >
                                <SvgCP.iconAddImage/>
                            </TouchableOpacity>

                            <TouchableOpacity
                                disabled={this.state.disabled}
                                onPress={() => 
                                    this.setState({isPosting:true, disabled:true},() =>{
                                        this.getContent()
                                    })
                                    }
                                style={[styles.buttonSent,{opacity:((this.state.disabled) ? 0.4 : 1)}] }
                            >
                                <Ionicons name='md-send' color={settingApp.blueSky} size={22} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </KeyboardAvoidingView>
                <GetImage 
                    isVisible={this.state.modalVisibleAttachment}
                    close={() => this.setState({ modalVisibleAttachment: false })}
                    selectFile={async (file) => await this.setState({ file , disabled:false})}
                />

                <ImageView 
                    visible = { this.state.showImage}
                    url = { imgUrl }
                    close = {() => this.setState({showImage:false})}
                />
                <ModalEdit
                    visible = { this.state.editComment}
                    close = {() => this.setState({editComment:false})}
                    item={this.state.itemEdit}
                    checkOption={(type) => this.checkOptionEdit(type)}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    coverACtion:{
        backgroundColor:'#FFFFFF', 
        justifyContent:'space-between', 
        flexDirection:'row', 
        paddingLeft:10
    },
    buttonAction:{
        height:40, 
        justifyContent:'center', 
        flexDirection:'row',
        paddingTop:10,
    },
    textAction:{
        fontSize:14, 
        fontWeight:'600', 
        paddingLeft:5, 
        paddingTop:3
    },
    textType:{
        fontSize:14, 
        color:(settingApp.colorText), 
        paddingTop:3,
        marginRight:20,
        bottom:7
    },
    app: {
        ...StyleSheet.absoluteFillObject,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'transparent',
      },
      content: {
        backgroundColor: 'transparent',
        top: Platform.OS === 'ios' ?  10 : -10,
        left:10,
        width:150,
        borderRadius: 15,
      },
      arrow: {
        borderTopColor: 'transparent',
      },
      background: {
        backgroundColor: 'transparent'
      },
      buttonSent:{
        width:40, 
        height:44, 
        backgroundColor:'transparent', 
        justifyContent:'center', 
        alignItems:'center', 
        paddingRight:15,
        paddingTop:3,
        
      },
      buttonAddIMG:{
        width:40, 
        height:44, 
        backgroundColor:'transparent', 
        justifyContent:'center', 
        alignItems:'center', 
        paddingRight:12,
      }
})
export default Layout;