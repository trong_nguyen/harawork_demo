import React, { Component } from 'react';
import { 
    View, 
    TextInput, 
    Text, 
    TouchableOpacity, 
    KeyboardAvoidingView, 
    StyleSheet, 
    Keyboard,
    ScrollView,
    FlatList
} from 'react-native';
import { settingApp, AvatarCustom } from '../../../../../../public';
import { HaravanHr } from '../../../../../../Haravan';
import lodash from 'lodash';
import { AntDesign } from '@expo/vector-icons';

class TextComment extends Component{
    constructor(props){
        super(props)
        this.state={
            value:'',
            showTag:false,
            listData:[],
            listTag:[],
            keyEnter:false,
            showingUser:false
        }
        this.listContent = [];
        this.currentValue = '';
        this.valueChange = '';
        this.valueSlice = '';
        this.listValue = [];
        this.currentText = ''
    }

    componentDidMount(){
        this.keyboardDidHideListener = Keyboard.addListener(
            'keyboardWillHide',
            this._keyboardHide.bind(this),
        );
    }

    _keyboardHide(){
        //this.setState({showTag:false })      
    }

    async CallApi(){
        const { value, showTag } = this.state
        if(showTag == true){
            let newValue = value.substr(value.lastIndexOf('@'))
                if(newValue.indexOf(' ') < 0){
                    newValue = newValue.replace('@','')
                    const result = await HaravanHr.getTotalEmploy(newValue)
                    if(result && result.data && !result.error){
                        this.setState({ listData:result.data.data })
                    }
                }
        }
    }

    changeText(text){
        this.valueSlice = text
        //this.setState({ value:text })
        setTimeout(() => {
            this.convertValue()
        }, 500);
            
    }

    convertValue(){
        const { value } = this.state
        this.props.checkDisable(value)
        if(value=== ''){
            this.currentValue = ''
            this.listContent = []
            this.setState({listTag:[]})
        }
        else{
            this.CallApi()
        }
    }

    setContent(){
        this.handleContent()
        // this.props.setContent(this.listContent)
        // this.TextInput.clear()
        
        // this.setState({value:''},() =>{
        //     this.convertValue()
        // })
    }

    handleContent(){
        let { listContent, currentValue, valueSlice, listValue } = this;
        let { value } = this.state
        // console.log('value', valueSlice);
        // console.log('value currentValue', currentValue);
        // console.log('value state', this.state.value);
        // console.log('value listContent', listContent);
        const { listTag, keyEnter } = this.state
        let newContent = ''
        let listString = value.split(' ')
        console.log('value listString 22222', listValue);
        for(let i = 0; i < listContent.length; i++){
            listValue.map((item, index) => {
                if(index == i){
                    let indexTag= listTag.findIndex(m => m.name === item)
                    if(indexTag < 0){
                        item = item.trim()
                        listContent[i] = `${item}`
                    }
                }
                // let valueTest = listContent[i].substr(listContent[i].indexOf(item))
            })
        }

        if(listContent.length == 0 ){
            if(listTag.length == 0){
                newContent = this.state.value;
                let newValue = keyEnter ? `${newContent}</br>` : `${newContent}`
                listContent.push(newValue)
            }
            else{
                listTag.map(itemTag =>{
                    newContent = this.state.value.split(itemTag.name).join('')
                    let newValue  = keyEnter ? `${newContent}</br>` : `${newContent}`
                    listContent.push(newValue)
                })
            }
            // let indexContent = listContent.findIndex(e => e == newContent)
            // if(indexContent > -1){
            //     listContent.splice(indexContent, 1)
            // }
        }
        else{
            if(this.state.value.indexOf(currentValue) > -1){
                if(listTag.length == 0){
                    newContent = this.state.value.replace(currentValue,'')
                    let newValue  = keyEnter ? `${newContent}</br>` : `${newContent}`
                    listContent.push(newValue)
                }
                else{
                    listTag.map(itemTag =>{
                        newContent = this.state.value.replace(currentValue,'').trim()
                        let valueKey  = keyEnter ? `${newContent}</br>` : `${newContent}`
                        let index  = listContent.findIndex(m => m === newContent)
                        if(index > -1){
                            let newValue = valueKey.replace(itemTag.name,'')
                            listContent.splice(index, 1)
                            listContent.push(newValue)
                        }
                        else{
                            let newValue = valueKey.replace(itemTag.name,'')
                            listContent.push(newValue)
                        }
                        let indexContent = listContent.findIndex(e => e == newContent)
                        if(indexContent > -1){
                            listContent.splice(indexContent, 1)
                        }
                    })
                }
            }
            // else{
            //     newContent = ''
            //     listContent.push(newContent)
            // }
        }

        this.currentValue = this.state.value;
        console.log('value currentValue', this.listContent);
        this.setState({keyEnter:false})
    }

    
    getUser(item){
        let { listTag, value } = this.state
        let link = `<a data-id=\"${item.haraId}\" target=\"_blank\" href=\"https://hr.hara.vn/colleague/profile/${item.id}\">${item.fullName}</a>`
        let name = item.fullName.split(' ').join('')
        name = name.toUpperCase()
        let namePush = `@${name}`;
        let body = {
            name:namePush,
            data:item
        }
        listTag.push(body)
        let valueChange = value.substr(value.lastIndexOf('@')+1)
        value = value.replace(valueChange,"")
        let newValue = value+ name+ ' ' 
        this.setState({value:newValue, showTag:false, listTag, showingUser:false},() => {
                this.setNewValue(newValue)
                this.handleContent()
                this.listContent.push(link)
                this.TextInput.focus()
            }
        )
    }

    checkNameTag(){
        let { value , listTag, showingUser, showTag} = this.state;
        let  {listContent} = this
        if((showingUser == false)){
            if(value.lastIndexOf('@')  > -1){
                let newValue =  value.substr(value.lastIndexOf('@'))
                let valueSlice = newValue.slice(newValue[0], newValue.length -1)
                const index = listTag.findIndex(item => item.name === newValue)
                let valueNew = `<p id='noneTag'>${valueSlice}</p>`
                this.listContent.push(valueNew)
                if(index > -1){
                    let data = listTag[index].data;
                    let link = `<a data-id=\"${data.haraId}\" target=\"_blank\" href=\"https://hr.hara.vn/colleague/profile/${data.id}\">${data.fullName}</a>`
                    const indexLink = listContent.findIndex(e => e === link)
                    if(indexLink > -1){
                        listContent.splice(indexLink, 1)
                    }
                    listTag.splice(listTag[index], 1)
                    this.setState({listTag:listTag})
                }
                const indexContent = this.listContent.findIndex(m => m === newValue)
                if(indexContent > -1){
                    this.listContent.splice(indexContent, 1)
                }
            }
        }
    }

    handleKeyPress(e){
        if(e.nativeEvent.key === 'Enter'){
            this.setState({keyEnter:true, showingUser:false },() =>{
                this.setNewValue()
                this.handleContent()
            })
        }
        else if(e.nativeEvent.key === '@'){
            this.setState({showTag:true},() =>{
                this.CallApi()
            })
        }
        else if(e.nativeEvent.key === " "){
            this.setNewValue()
            this.setState({showTag:false, showingUser:false})
        }
        else if( e.nativeEvent.key === 'Backspace'){
            this.setState({showTag:false, showingUser:true },() =>{
                this.checkNameTag()
            })
        }
    }

    setNewValue(){
        let { value } = this.state
        let { listValue, valueSlice} = this;
        let newValue = ''
        if(listValue.length ==0 ){
            newValue = value.trim()
            listValue.push(newValue)
        }
        else{
            newValue = value.replace(this.currentText,'').trim()
            listValue.push(newValue)
        }
       
        this.currentText = value
    }

    onChange(event){
       // console.log('event', event.nativeEvent);
    }

    renderItem(obj){
        const { item, index } = obj
        return(
            <TouchableOpacity 
                onPress={() => this.getUser(item)}
                style={styles.coverItem}>
                <AvatarCustom userInfo={{ name: item.fullName, picture: item.photo }} v={30} fontSize={12}/>
                <Text style={styles.nameUser}>{item.fullName}</Text>
            </TouchableOpacity>
        )
    }
    
    buttonClose(){
        return(
            <TouchableOpacity 
                onPress={() => this.setState({showTag:false})}
                style={styles.buttonClose}>
                <AntDesign name='closecircle' size={30} color={'#3F3F3F'} />
            </TouchableOpacity>
        )
    }

    render(){
        const { showTag, listData } = this.state;
        return(
            <KeyboardAvoidingView 
                behavior="padding" 
                enabled
                keyboardVerticalOffset={0}
                style={styles.main}
            >
                {showTag && this.buttonClose()}
                {(showTag == true) && 
                    <View style={styles.coverTag}>
                        <FlatList 
                            style={{flex:1}}
                            data={listData}
                            extraData={this.state}
                            keyExtractor={(item,index) => ''+index}
                            renderItem={(obj) => this.renderItem(obj)}
                        />
                    </View>
                }
                
                <View style={styles.coverTextInput}>
                    <TextInput
                        ref={refs => this.TextInput = refs}
                        style={styles.textInput}
                        value={this.state.value}
                        onChangeText={(value) => this.setState({value:value},()=> this.changeText(value))}
                        onChange={(event) => this.onChange(event)}
                        placeholder={'Viết bình luận'}
                        multiline={true}
                        editable={true}
                        scrollEnabled={true}
                        onKeyPress={(event) => this.handleKeyPress(event)}
                    />
                </View>
            </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    main:{
        backgroundColor:'#fff',
        flexDirection: 'column', 
        justifyContent:'center',
    },
    textInput:{
        width:(settingApp.width -150), 
        minHeight:40, 
        maxHeight:(settingApp.height*0.25)
    },
    coverTag:{
        width:settingApp.width - 30,
        height:settingApp.height * 0.25,
        backgroundColor:'#FFFFFF',
        marginLeft:5,
        borderBottomColor:settingApp.colorSperator,
        borderBottomWidth:1
    },
    coverItem:{
        width:settingApp.width - 35,
        height:44,
        alignItems:'center',
        paddingLeft:5,
        flexDirection:'row',
        backgroundColor:'#FFFFFF'
    },
    coverTextInput:{
        minHeight:50, 
        maxHeight:(settingApp.height*0.25), 
        paddingTop:5,  
        justifyContent:'center',
    },
    nameUser:{
        fontSize:16, 
        color:settingApp.colorText,
        marginLeft:5
    },
    buttonClose:{
        width:60,
        height:60,
        backgroundColor:'transparent',
        justifyContent:'center',
        alignItems:'center',
        left:-10
    }
})
export default TextComment;