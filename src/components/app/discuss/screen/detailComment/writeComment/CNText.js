import React, { Component } from 'react';
import { View, StyleSheet, Keyboard
, TouchableOpacity, Text
, KeyboardAvoidingView ,Platform, } from 'react-native';
 
import  CNText , { CNToolbar, getInitialObject , getDefaultStyles } from '../../../../../../public/CNText';
import { settingApp, Utils } from '../../../../../../public'

const defaultStyles = getDefaultStyles();
 
class CNTextEditor extends Component {
 
    constructor(props) {
        super(props);
        
        this.state = {
            selectedTag : 'body',
            selectedStyles : [],
            value: [getInitialObject()],
            height:44,
            focus:true,
            showTag:false
        };

        this.editor = null;
        this.currentLengh = 0
    }

 
    onStyleKeyPress = (toolType) => {
        this.editor.applyToolbar(toolType);
    }
 
    onSelectedTagChanged = (tag) => {
        this.setState({
            selectedTag: tag
        })
    }
 
    onSelectedStyleChanged = (styles) => { 
        this.setState({
            selectedStyles: styles,
        })
    }
 
    onValueChanged = (value) => {
        this.currentLengh
        if(this.currentLengh < value[0].content.length){
            this.currentLengh = value[0].content.length
            this.setState({height:((value[0].content.length * 20))})
        }
        else if(this.currentLengh > value[0].content.length){
            this.currentLengh = value[0].content.length
            this.setState({height:(this.state.height - 20)})
        }
        else{
            this.setState({height:44})
        }
        //this.checkName(value)
        this.setState({
                value: value,
            },() => 
            this.props.checkDisable(value)
        );
    }

    checkName(value){
        let listText = value[0].content;
        for(let i =0; i < listText.length; i++){
            if(listText[i].text.indexOf('@') > -1){
                this.setState({showTag:true})
            }
            else{
                this.setState({showTag:false})
            }
        }
    }

    setContent(){
        this.props.setContent(this.state.value)
    }

    componentWillReceiveProps(nextProps){
        if(nextProps && nextProps.focus && (nextProps.focus == true)){
            this.setState({focus: true})
        }
        if(nextProps && nextProps.clear && (nextProps.clear == true)){
            this.setState({ value: [getInitialObject()],})
        }
    }

    componentDidMount(){
        this.editor.onConnectToPrevClicked()
        
        this.keyboardDidShowListener = Keyboard.addListener(
            'keyboardWillShow',
            this._keyboardShow.bind(this),
        );
        this.keyboardDidHideListener = Keyboard.addListener(
            'keyboardWillHide',
            this._keyboardHide.bind(this),
        );
    }

    _keyboardHide() {
        this.setState({showTag:false, focus:false })        
    }

    _keyboardShow(e) {
        //console.log('_keyboardShow', e);
    }

 
    render() {
        let { height, focus, showTag } = this.state;
        return (
            <KeyboardAvoidingView 
            behavior="padding" 
            enabled
            keyboardVerticalOffset={0}
            style={{
                backgroundColor:'#fff',
                flexDirection: 'column', 
                minHeight:44, 
                justifyContent:'center'
            }}
            >
               {showTag && 
                        <View style={styles.coverShowtag}>

                        </View>
                    }  
                <TouchableOpacity onPress={() => {
                    this.setState({focus:false, showTag:false},() =>{
                        Keyboard.dismiss
                        this.props.setAutoFocus(false)
                    })
                }} >    
                    <View style={{minHeight:50, maxHeight:(settingApp.height*0.25), paddingTop:5,  justifyContent:'center'}}>
                        <CNText              
                            ref={input => this.editor = input}
                            onSelectedTagChanged={this.onSelectedTagChanged}
                            onSelectedStyleChanged={this.onSelectedStyleChanged}
                            value={this.state.value}
                            style={{ 
                                    backgroundColor : '#fff', 
                                    ...this.props.style, 
                                    minHeight:44, 
                                    width:(settingApp.width*0.7),
                                    paddingTop:Platform.OS ==='ios' ? 7 : 0,
                                    }}
                            styleList={defaultStyles}
                            onValueChanged={this.onValueChanged}
                            autoFocus={false}
                            focus={this.props.focus}
                            placeholder={'Viết bình luận'}
                            maxHeight={settingApp.height*0.25}
                        />                        
                    </View>
                </TouchableOpacity>
        </KeyboardAvoidingView>
        );
    }

    componentWillUnmount(){
        this.setState({showTag:false})
    }
 
}
 
var styles = StyleSheet.create({
    main: {
        alignItems: 'center',
    },
    toolbarButton: {
        fontSize: 20,
        width: 28,
        height: 28,
        textAlign: 'center'
    },
    italicButton: {
        fontStyle: 'italic'
    },
    boldButton: {
        fontWeight: 'bold'
    },
    underlineButton: {
        textDecorationLine: 'underline'
    },
    lineThroughButton: {
        textDecorationLine: 'line-through'
    },
    coverShowtag:{
        width:settingApp.width -30,
        height:settingApp.height * 0.2,
        backgroundColor:'blue',
    }
});
 
export default CNTextEditor;