import React, { Component } from 'react';
import { View, WebView, Text, ScrollView, Platform } from 'react-native';
import { settingApp, Utils } from '../../../../../../public'

class Comment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            height: 44
        }
        this.webView = null;
    }

    
    onMessage(e) {
        let { data } = e.nativeEvent;
        data = JSON.parse(data);
        // this.props.getHTML(data.html);
        // this.props.setHeight(data.height)
        this.setState({ height:(data.height + 20) });
      }

    render() {
        let {focus} = this.props;
        const jsCode = `
            var html = document.getElementById('content')
            if(html.length == 0){
                window.postMessage(data);
            }
            
        `
        const { height } = this.state;
        const { type} = this.props;
        if(Platform.OS === 'ios'){
            return (
                <View 
                    style={{backgroundColor:'transparent',height, minHeight:44, maxHeight:(settingApp.height*0.25), paddingTop:5}}>
                    <ScrollView
                    ref={ref => this.ScrollView = ref}
                    onContentSizeChange={(width, height)=>{        
                        this.ScrollView.scrollToEnd({animated: true});
                    }}
                    style={{ backgroundColor:'transparent', paddingTop:10}}>
                        <WebView
                            ref={( ref ) => this.webView = ref}
                            style={{ height, backgroundColor:'transparent'}}
                            source={require('./writeComment.html')}
                            scrollEnabled={false}
                            javaScriptEnabled={true}
                            injectedJavaScript={jsCode}
                            automaticallyAdjustContentInsets={true}
                            onMessage={data => this.onMessage(data)}
                            domStorageEnabled={true}
                            useWebKit={true}
                        />
                    </ScrollView>
                </View>
            )
        }
        else{
            return (
                <View 
                    style={{backgroundColor:'transparent',height, minHeight:44, maxHeight:(settingApp.height*0.25), paddingTop:5}}>
                        <WebView
                            ref={( ref ) => this.webView = ref}
                            style={{ height, backgroundColor:'transparent', maxHeight:(settingApp.height*0.25)}}
                            source={require('./writeComment.html')}
                            scrollEnabled={false}
                            javaScriptEnabled={true}
                            injectedJavaScript={jsCode}
                            automaticallyAdjustContentInsets={true}
                            onMessage={data => this.onMessage(data)}
                            domStorageEnabled={true}
                            useWebKit={true}
                        />
                </View>
            )
        }
    }
}

export default Comment;