import React, { Component } from 'react';
import {View, Text, TouchableOpacity, StyleSheet, Platform , Image, ScrollView, FlatList,KeyboardAvoidingView, ActivityIndicator,StatusBar} from 'react-native';
import { settingApp, HeaderScreenNews, ParseText ,AvatarCustom, SvgCP, Utils} from '../../../../../../public';
import { FontAwesome, Ionicons} from '@expo/vector-icons';
import ImageView from '../component/ImageView';
import { Popover, PopoverController } from 'react-native-modal-popover';
import * as Animatable from 'react-native-animatable';
import ItemReplies from '../component/itemReplies';
import CNText from '../writeComment/CNText';
import GetImage from '../getImage';
import HTML from 'react-native-render-html';
import TextComment from '../writeComment/textComment';
import EditComment from '../component/modalEdit'

class Layout extends Component {
    renderHeader(){
        return (
            <View style={{
                backgroundColor:'#FFFFFF',
            }}>
                <StatusBar barStyle={Platform.OS === 'ios' ? 'dark-content' : 'default'}/>
                <View style={{ width:settingApp.width, height: settingApp.statusBarHeight, backgroundColor:'#FFFFFF'}} />
                <View style={{ width:settingApp.width, height: 44, flexDirection: 'row', alignItems:'center', justifyContent:'space-between'}} >
                        <View style={{ flex: 1, backgroundColor: 'transparent', flexDirection: 'row', justifyContent:'space-between', alignItems:'center' }}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({autoFocus:false},()=>{
                                        this.actionsRemove({dataRemove:this.state.data, list:this.state.listRemove})
                                        this.props.navigation.pop()
                                    })
                                    }}
                                style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                                    <Ionicons name='ios-arrow-back' size={23} color='#000000' />
                            </TouchableOpacity>
                            <View 
                                animation='fadeIn'
                                style={{justifyContent:'center', alignItems:'center', width:settingApp.width*0.6}}>
                                <Text 
                                    numberOfLines={1}
                                    style={{ fontSize: 17, color: '#000000', fontWeight: 'bold'}}>
                                    Trả lời
                                </Text>
                            </View>
                            <View style={{ width: 50, height: 44 }} />
                        </View>
                </View>
            </View>
        )
    }

    checkAction(){
        const { data } = this.state
        let { like_users, dislike_users, praise_users } = data
        const { colleague } = this
        let actionUser = null
        const indexLike =  like_users.findIndex(m => m.id == colleague.haraId)
        const indexPraise =  praise_users.findIndex(m => m.id == colleague.haraId)
        const indexDislike=  dislike_users.findIndex(m => m.id == colleague.haraId)
        if(indexLike > -1){
            actionUser = {title:'Thích', color:(settingApp.color), image: <SvgCP.isHeart />, type:1}
        }
        else if( indexPraise > -1){
            actionUser = {title:'Tán dương', color:(settingApp.color), image: <SvgCP.isClap />, type:2}
        }
        else if( indexDislike > -1){
            actionUser = {title:'Không thích', color:(settingApp.color), image: <SvgCP.isDisLike />, type:3}
        }
        else{
            actionUser = {title:'Thích', color:(settingApp.colorDisable), image: <SvgCP.noneHeart />, type:-1}
        }
        return actionUser
    }

    buttonAction(checkAction){
        return(
            <View style={{flexDirection:'row', width:(settingApp.width), height:50, alignItems:'center'}}>
                <View
                    style={{
                        height:50,
                        width:150,
                        backgroundColor:'#FFFFFF',
                        borderRadius:15,
                        flexDirection:'row',
                        ...settingApp.shadow
                        }}>
                        <Animatable.View
                            duration = {200}
                            delay ={200}
                            animation='bounceIn'
                            style={{width:50, height:50, alignItems:'center', justifyContent:'center'}}
                        >
                            <TouchableOpacity
                                onPress={() => this.setState({autoFocus:false},() =>{
                                    this.setOption(1)
                                })}
                                style={{alignItems:'center', justifyContent:'center'}}
                            > 
                                <SvgCP.isHeart/>
                            </TouchableOpacity>
                        </Animatable.View>
                                
                        <Animatable.View
                            duration = {250}
                            delay ={250}
                            animation='bounceIn'
                            style={{width:50, height:50, alignItems:'center', justifyContent:'center'}}
                         >
                            <TouchableOpacity
                                onPress={() => this.setState({autoFocus:false},() =>{
                                    this.setOption(2)
                                })}
                                style={{alignItems:'center', justifyContent:'center'}}
                                > 
                                    <SvgCP.isClap/>
                            </TouchableOpacity>
                        </Animatable.View>
                        
                        <Animatable.View
                            duration = {300}
                            delay ={300}
                            animation='bounceIn'
                            style={{width:50, height:50, alignItems:'center', justifyContent:'center'}}
                        >
                            <TouchableOpacity
                                onPress={() => 
                                this.setState({autoFocus:false},() =>{
                                    this.setOption(3)
                                })
                                }
                                style={{alignItems:'center', justifyContent:'center'}}
                            > 
                                <SvgCP.isDisLike/>
                            </TouchableOpacity>
                        </Animatable.View>
                </View>
            </View>
        )
    }

    optionActions(){
        let checkAction =  this.checkAction()
        return(
            <PopoverController>
                {({ openPopover, closePopover, popoverVisible, setPopoverAnchor, popoverAnchorRect }) => (
                    <React.Fragment>
                        <TouchableOpacity 
                            onPress={() => 
                                this.setState({autoFocus:false}, () =>{
                                    this.checkNoneLongPress()
                                })
                            }
                            ref={setPopoverAnchor} 
                            onLongPress={openPopover}
                            style={styles.buttonAction}
                        >
                            <Text style={[styles.textAction, {color:checkAction.color, bottom:10}]}>{checkAction.title}</Text>
                        </TouchableOpacity> 
                    <Popover 
                        ref = {ref => this.Popover = ref}
                        contentStyle={styles.content}
                        arrowStyle={styles.arrow}
                        backgroundStyle={styles.background}
                        visible={popoverVisible}
                        onClose={closePopover}
                        fromRect={popoverAnchorRect}
                        supportedOrientations={['portrait', 'landscape']}
                    >   
                        {this.buttonAction(checkAction)}
                    </Popover>
                    </React.Fragment>
                )}
            </PopoverController>
        )
    }

    renderAction(){
        const { data , totalCount} = this.state;
        let  {like_users, dislike_users, praise_users} = data;
        let check =  (like_users.length > 0 )&& (dislike_users.length > 0) && (praise_users.length > 0)
        return(
            <View style={{flexDirection:'row', bottom:10}}>
                { (like_users.length > 0) ?
                    <View style={{zIndex:90, right:(check ? 0 : -10 )}}>
                        <SvgCP.isHeart/>
                    </View>
                    :
                    <View/>
                }
                { (praise_users.length > 0)  ? 
                    <View style={{ zIndex:80, right:(check ? 5 : 0)}}>
                        <SvgCP.isClap/>
                    </View>
                    : <View />
                }
                { (dislike_users.length > 0)  ? 
                    <View style={{zIndex:70, right:( check ? 10 : (praise_users.length > 0)  ? 10 : 0)}}>
                        <SvgCP.isDisLike/>
                    </View>
                    :<View/>
                }
            </View>
        )
    }

    renderPosting(){
        const {fullName, photo} = this.colleague
        const { created_By } = this.data;
        const {file, value, isPosting } = this.state;
        let source =this.getURI(file)
        if(isPosting == true){
        return(
            <View style={{ paddingBottom:10, borderTopWidth:1, borderTopColor:settingApp.colorSperator, paddingTop:10 }}>
                <View style={{flexDirection:'row', alignItems:'center'}}> 
                    <FontAwesome name='circle' size={10} color='#FDBD41' />
                    <Text style={{fontSize:12, color:settingApp.colorDisable, paddingLeft:5}}>Đang đăng...</Text>
                </View>
                <View style={{flexDirection:'row', alignItems:'center'}}>
                    <AvatarCustom userInfo={{ name: fullName, picture: photo }} v={30} fontSize={12}/>
                    <Text style={{ marginTop: 5, fontSize: 14, color: settingApp.colorText, paddingLeft:10, fontWeight:'bold' }}>
                        <Text style={{ color: settingApp.colorText }}>{ fullName ?  fullName : 'Lấy thông tin thất bại'} </Text>
                    </Text>
                </View>
                <View style={{paddingLeft:5, backgroundColor:'#FFFFFF', justifyContent:'center', width:(settingApp.width*0.85)}}>
                    <ParseText description={value} />
                </View>
                {file && (file.length > 0) ?
                    <View 
                    style={{paddingTop:5,paddingLeft:5, backgroundColor:'#FFFFFF', paddingBottom:10}}>
                        <Image 
                            source={{uri: `${source}`}}
                            style={{resizeMode:'contain', width:120, height:120}}
                        />
                    </View> : <View />
                }
            </View>
        )}
        
    }

    renderImage(){
        const { file } = this.setState
        let source = this.getURI(file)
        return(
            <View
                style={{
                    width:120, 
                    height:90,
                    backgroundColor:'#FFFFFF',
                    padding:10,
                    marginBottom:5,
                }}
            >
                <View style={{flex:1}}>
                    <TouchableOpacity
                        onPress={() => this.setState({file: []}, () => this.checkDisable())}
                        style={{
                            position:'absolute',top:-10, right:0, width:30, height:30, zIndex:999, justifyContent:'center', alignItems:'center'}}
                            >
                        <Ionicons name="ios-close-circle" size={20} color={settingApp.colorDisable} />
                    </TouchableOpacity>
                    <Image 
                        source={{uri:`${source}`}}
                        style={{flex:1, width:undefined, height:undefined, resizeMode:'contain'}} />
                </View>
            </View>
        )
    }
    loadMore(){
        return(
            <View style={{
                backgroundColor: 'transparent',
                justifyContent: 'center',
                alignItems: 'center',
            }}>
                <ActivityIndicator size='small' color={settingApp.color} />
            </View>
        )
    }

    renderLoadMore(){
        const { loadMore, listComment , totalCountPage} = this.state;
        if((listComment.length < totalCountPage)){
            return(
                <TouchableOpacity
                    onPress={() =>{
                        this.setState({loadMore:true},() =>{
                            this.loadComment()
                        })
                        }}
                    style={{height:44, width:(settingApp.width), backgroundColor:'#FFFFFF', padding:10, flexDirection:'row'}}
                >
                    <Text style={{fontSize:14, color:'#2962FF'}}>Xem các bình luận trước</Text>
                    <View style={{paddingLeft:10, justifyContent:'center', alignItems:'center'}}> 
                        { loadMore && this.loadMore()}
                    </View>
                </TouchableOpacity>
            )
        }
        else{
            return <View />
        }
    }
    
    renderComment(){
        const {listComment, isLoadComment, isPosting} = this.state;
        if(!isLoadComment && (listComment.length > 0)){
            return(
                <View style={{flex:1, paddingLeft:10}}>
                <FlatList 
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    data={listComment}
                    extraData={this.state}
                    keyExtractor={(item, index) =>(item.id+'')}
                    renderItem={({item, index}) => <ItemReplies 
                        item ={ item }
                        index = { index}
                        colleague = { this.colleague }
                        token ={ this.token }
                        navigation = { this.props.navigation }
                        showComment={true}
                        autoFocus={(value) => this.setState({autoFocus:value})}
                        editComment={(value) => this.setState({itemEdit:value.item, editComment:value.type})}
                    />}
                    ListFooterComponent={this.renderPosting()}
                />
                </View>
            )
        }
        else if(!isLoadComment && (listComment.length == 0)){
            return(
                <View style={{flex:1, justifyContent:'center', alignItems:'center'}}> 
                    <Text style={{ fontSize:14, color:settingApp.colorDisable, paddingTop:10}}>Chưa có bình luận</Text>
                </View>
            )
        }
        else{
            return(
                <View style={{flex:1, justifyContent:'center', alignItems:'center', paddingRight:10}}>
                    <ActivityIndicator size='small' color={settingApp.colorDisable} />
                </View>
            )
        }
        
    }

    render(){
        let {  content, created_At, created_By,} = this.data;
        let { totalCount, data, file, isPosting, totalCountPage } = this.state;
        let time = this.formatTime(created_At)
        let source = this.getSource()
        let imageUser = Utils.getLinkImage(created_By.id)
        content = content ?  content : '<b/>'
        return(
            <View style={{flex:1 , backgroundColor:'#FFFFFF'}}>
                {this.renderHeader()}
                {((data && data.attachments.length > 0) && (source)) &&
                <ImageView 
                    visible = { this.state.showImage}
                    url = { source }
                    close = {() => this.setState({showImage:false, autoFocus:false})}
                />}
                <ScrollView 
                ref={ref => this.ScrollView = ref}
                style={{paddingLeft:10, padding:5}}>
                    <View style={{ padding: 10, paddingTop: 20 }}>
                        <View style={{flexDirection:'row', alignItems:'center'}}>
                            <AvatarCustom userInfo={{ name:created_By ? created_By.name :'--', picture: imageUser }} v={30} fontSize={12}/>
                            <Text style={{ fontSize: 14, color: settingApp.colorText, paddingLeft:10, fontWeight:'bold' }}>
                                <Text style={{ color: settingApp.colorText }}>{ created_By.name ?  created_By.name : 'Lấy thông tin thất bại'} </Text>
                            </Text>
                        </View>
                            
                        <View style={{ flex: 1, marginTop: 20 }}>
                            {/* <ParseText description={content} /> */}
                            <HTML 
                                baseFontStyle={{fontSize:16, lineHeight:20}} 
                                html={content} />
                        </View>
                    </View>
                       
                    {((data && data.attachments.length > 0) && (source)) &&
                        <TouchableOpacity 
                            onPress={() => this.setState({showImage:true})}
                            style={{paddingTop:5,paddingLeft:10, backgroundColor:'#FFFFFF', paddingBottom:10}}>
                            <Image 
                                source={{uri: `${source}`}}
                                style={{resizeMode:'contain', width:120, height:120}}
                            />
                        </TouchableOpacity>
                    }
                    <View style={{flexDirection:'row', alignItems:'center',backgroundColor:'#FFFFFF', top:-5 , justifyContent:'space-between', paddingRight:25}}>
                        <View style={{flexDirection:'row', alignItems:'center' ,width:(settingApp.width*0.65)}}>
                            <View style={{height:40, padding:5, paddingRight:5, bottom:2, }}>
                                <Text style={{fontSize:14, color:(settingApp.colorDisable),paddingLeft:3}}>{time}</Text>
                            </View>
                            {data ? this.optionActions() : <View/>}
                            <View style={{bottom:2}}>
                            <TouchableOpacity
                                onPress={() => this.setState({autoFocus:true})}
                                style={{height:40, padding:5}}
                            >
                                <Text style={{fontSize:14, color:(settingApp.colorDisable), fontWeight:'600', marginLeft:10 }}>Trả lời</Text>
                            </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{flexDirection:'row', paddingLeft:10, alignItems:'center', justifyContent:'center'}}> 
                            {data ?  this.renderAction(): <View/>}
                            {(totalCount > 0) ?  <Text style={{ color:settingApp.colorText, fontSize:14, top:-8, marginLeft:5 }}>{totalCount}</Text> : null}
                        </View>
                    </View>
                    {totalCountPage > 10 ?
                        this.renderLoadMore() :
                        <View/>
                    }
                    <View style={{width:(settingApp.width), paddingLeft:20, paddingRight:20}}>
                        {this.renderComment()}
                    </View>

                </ScrollView>
                <KeyboardAvoidingView behavior='padding'
                    style={{
                        borderTopColor:(settingApp.colorSperator), 
                        borderTopWidth:1,
                    }}
                >  
                     {(file.length > 0) && !isPosting && this.renderImage()} 
                    <View style={{
                        backgroundColor:'#FFFFFF', 
                        width:(settingApp.width), 
                        minHeight:44, 
                        flexDirection:'row',
                        alignItems:'flex-end'
                        }}>
                        <View style={{
                            marginLeft:10, marginRight:10, width:(settingApp.width-100), minHeight:44}}>
                            <CNText 
                                ref={refs => this.CNText = refs}
                                focus={this.state.autoFocus}
                                setAutoFocus={() => this.setState({autoFocus:false})}
                                setContent={(value) => this.setContent(value)}
                                clear = {this.state.clear}
                                checkDisable={(value) => this.checkDisable(value)}
                           />
                        </View>
                        <View style={{width:80, flexDirection:'row', height:44, bottom:5}}> 
                            <TouchableOpacity
                                onPress={() => this.setState({modalVisibleAttachment:true})}
                                style={{width:40, height:44, backgroundColor:'transparent', justifyContent:'center', alignItems:'center', paddingRight:12,}}
                            >
                                <SvgCP.iconAddImage />
                            </TouchableOpacity>

                            <TouchableOpacity
                                disabled={this.state.disabled}
                                onPress={() => (
                                    this.setState({isPosting:true,disabled:true, autoFocus:false},() =>{
                                            this.getContent()
                                        })
                                    )}
                                style={{
                                    opacity:(this.state.disabled ? 0.4 :1),
                                    width:40, height:44, backgroundColor:'transparent', justifyContent:'center', alignItems:'center',  paddingRight:15}}
                            >
                                <Ionicons name='md-send' color={settingApp.color} size={20} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </KeyboardAvoidingView>

                <GetImage 
                    isVisible={this.state.modalVisibleAttachment}
                    close={() => this.setState({ modalVisibleAttachment: false })}
                    selectFile={async (file) => await this.setState({ file,disabled:false })}
                />

                <EditComment 
                    visible = { this.state.editComment}
                    close = {() => this.setState({editComment:false})}
                    item={this.state.itemEdit}
                    checkOption={(type) => this.checkOptionEdit(type)}
                />

            </View>
        )
    }
}
const styles ={
    app: {
        ...StyleSheet.absoluteFillObject,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'transparent',
      },
      content: {
        backgroundColor: 'transparent',
        top: Platform.OS === 'ios' ?    10 : -10,
        left:50,
        width:150,
        borderRadius: 15,
      },
      arrow: {
        backgroundColor: 'transparent',
        borderTopColor: 'transparent',
      },
      background: {
        backgroundColor: 'transparent'
      },
      textAction:{
        fontSize:14, 
        fontWeight:'600', 
        paddingLeft:5, 
        paddingTop:3
    },
    buttonAction:{
        height:40, 
        justifyContent:'center', 
        flexDirection:'row',
        paddingTop:10,
    },
}
export default Layout;