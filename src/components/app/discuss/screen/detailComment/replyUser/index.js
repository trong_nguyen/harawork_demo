import React, { Component } from 'react';
import Layout from './layout';
import moment from 'moment';
import { settingApp, SvgCP, imgApp } from '../../../../../../public';
import { HaravanGraphQL, HaravanIc } from '../../../../../../Haravan';
import Configs from '../../../../../../Haravan/config';
import {connect} from 'react-redux';
import actions from '../../../../../../state/action';
import { bindActionCreators } from 'redux';

class ReplyUser extends Layout{
    constructor(props){
        super(props)
        this.state={
            isLoadComment:true,
            loadMore:false,
            showImage:false,
            data: null,
            listComment:[],
            totalCount:0,
            value: null,
            listAttach:[],
            modalVisibleAttachment:false,
            file: [],
            isPosting:false,
            totalCountPage: 0,
            autoFocus:false,
            clear:false,
            disabled:true,
            itemEdit:null,
            editComment:false,
            listRemove:[]
        }

        this.data = props.navigation.state.params.data;
        this.token = props.navigation.state.params.token;
        this.colleague = props.app.colleague;
        this.actionComment = props.discuss.checkAtions;
        this.dataReply = props.navigation.state.params.dataReply;
        this.reply = props.navigation.state.params.reply;
        this.actionsRemove = props.navigation.state.params.actions
        this.currentList = [];
        this.page = 1;
        this.totalPage = null;
    }

    componentWillReceiveProps(nextProps){
        if(nextProps && (nextProps.discuss.checkAtions)){
            let { info , value} = nextProps.discuss.checkAtions
            let {data} = this.state;
            if( (info.id == data.id)){
                this.setState({data:info},() =>{
                    this.setTotalCount()
                })
            }
        }
    }

    componentDidMount(){
        const { data } = this;
        if(data){
            let { like_users, dislike_users, praise_users } = data
            let totalCount =  like_users.length + dislike_users.length + praise_users.length;
            this.setState({data:data , totalCount:totalCount},()=>{
                this.loadComment()
            })
        }
        if(this.reply){
            this.setState({autoFocus:this.reply})
        }
    }

   async checkOptionEdit(type){
        let { listComment, itemEdit, listRemove } = this.state;
        const { token } = this;
        if(type == 1){
            let params ={
                token: token,
                id: itemEdit.id
            }
            const result = await HaravanGraphQL.deleteComment(params)
            if(result && result.data && result.data.deleteComment){
                let index = listComment.findIndex(m => m.id === itemEdit.id)
                if(index > -1){
                    listComment.splice(index, 1)
                    listRemove.push(itemEdit)
                    this.setState({listComment})
                }
            }
            else{
                Toast.show('Có lỗi hệ thống, vui lòng thử lại sau')
            }
        }
    }

    setContent(value){
        let textInput = '';
        if(value){
            if( value[0].content.length > 1){
                for(let i = 0; i < value[0].content.length; i++){
                    if(i < (value[0].content.length - 1)){
                        textInput = textInput + value[0].content[i].text.trim() + '</br>'
                    }
                    else{
                        textInput = textInput + value[0].content[i].text.trim()
                    }
                }
            }
            else{
                textInput = textInput + value[0].content[0].text.trim()
            }
        }
        let newValue =  textInput.replace(/["/\\]/g, '\\$&')
        this.setState({value:`${textInput}`,  clear:true },() => this.checkComment(newValue))
    }

    getContent(){
        this.CNText.setContent()
        //this.TextComment.setContent()
    }

    setHeight(height){
        this.setState({height:height})
    }

    getHTML(html){
        this.setState({value:html})
    }

    async getUrl(){
        const { file } = this.state;
        let listUrl = []
        let input = new FormData();
        if(file && file.length > 0 ){
            for (let i = 0; i < file.length; i++) {
                input.append("files",{
                    uri: file[i].uri,
                    type: `image/${file[i].fileName.substr(file[i].fileName.lastIndexOf('.') + 1)}`,
                    name: file[i].fileName,
                });
            }
            const response = await HaravanIc.postImage(input)
            if(response){
                response.map(e =>{
                    listUrl.push(e)
                })
                this.setState({isLoadIMG: false})
            }
            else{
                this.setState({isLoadIMG:false})
            }
        }
        else{
            listUrl = []
        }
        return listUrl
    }

    async checkComment(textInput){
        this.ScrollView.scrollToEnd({animated: true});
        const { file } = this.state;
        let getUrl = await this.getUrl()
        let attachments = null
        if(file && (file.length > 0) && getUrl){
            for (let i = 0; i < getUrl.length; i++) {
                attachments = {name:getUrl[i].name, url:getUrl[i].file.url, size: getUrl[i].file.size}
            }
            this.addCommentNew(attachments, textInput)
        }
        else{
            this.addCommentNew(attachments, textInput)
        }
    }

    async addCommentNew(attachments, textInput){
        const { listComment, file,value } = this.state;
        this.ScrollView.scrollToEnd({animated: true});
        let content = `${textInput}`
        let body = {
            token: this.token,
            parentId:this.data.id,
            ref_Url: `https://ic.${Configs.apiHost}/threads/detail/${this.token}`, 
            content: content, 
            moduleName: "IC", 
            tagged_Ids: `tagged_Ids:[]`, 
            attachments: (file && file.length > 0) ? attachments : null
        }
        let result = await HaravanGraphQL.addNewReplyComment(body)
        if(result && result.data ){
            const aestTime = new Date().getTime();
            let itemNews = {
                action_count: 0,
                action_users: [],
                attachments:(file && file.length > 0) ? [attachments] : [],
                content: `${value}`,
                like_count: 0,
                like_users: [],
                praise_count: 0,
                praise_users: [],
                dislike_count: 0,
                dislike_users: [],
                id: result.data.replyComment.id,
                created_By: {id: this.colleague.haraId, name: this.colleague.fullName, avartar: this.colleague.photo},
                created_At: aestTime,
                totalCount:0,
            }
            listComment.push(itemNews)
            this.setState({listComment, isPosting:false, file:[], clear:true, autoFocus:false},() =>{
                this.ScrollView.scrollToEnd({animated: true});
                this.addReplay(itemNews)
            })
        }
    }

    addReplay(itemNews){
        let { data } = this.state;
        if(data.replies){
            data = {...data, replies:{
                data:[...data.replies.data, itemNews],
                totalCount:(data.replies.totalCount +1)
            }}
            this.props.actions.discuss.addReply(data)
            this.setState({ clear:false })
        }
    }

    async potsAction(value){
        const { token } = this
        const { data } = this.state;
        let params = null
        if(value == 1){
           params = `likeComment(token:"${token}", id:"${data.id}")` 
        }
        else if (value == 2){
            params = `praiseComment(token:"${token}", id:"${data.id}")` 
        }
        else if (value == 3){
            params = `dislikeComment(token:"${token}", id:"${data.id}")` 
        } else{
            params = `removeActionComment(token:"${token}", id:"${data.id}")` 
        }
        let resss = await HaravanGraphQL.actionComment(params)
    }

    checkDisable(value){
        let textInput = '';
        if(value){
            if(value && value[0].content.length > 1){
                for(let i = 0; i < value[0].content.length; i++){
                    if(i < (value[0].content.length -1)){
                        textInput = textInput + value[0].content[i].text.trim() + '</br>'
                    }
                    else{
                        textInput = textInput + value[0].content[i].text.trim()
                    }
                }
            }
            else{
                textInput = textInput + value[0].content[0].text.trim()
            }
        }

        if((this.state.file.length > 0) || (textInput.length > 0)){
            this.setState({disabled:false})
        }
        else{
            this.setState({disabled:true})
        }
    }

    checkNoneLongPress(){
        let { data} = this.state;
        const { colleague } = this
        let value = null
        let { like_users, dislike_users, praise_users } = data;
        const indexLike =  like_users.findIndex(m => m.id == colleague.haraId)
        const indexPraise =  praise_users.findIndex(m => m.id == colleague.haraId)
        const indexDislike=  dislike_users.findIndex(m => m.id == colleague.haraId)
        let infoUser = {name:colleague.fullName, id:colleague.haraId, email:colleague.email, avartar:colleague.photo}
        if((indexLike <0) && (indexPraise < 0) && (indexDislike < 0)){
            data = {...data, like_users:[...like_users, infoUser]}
            value= 1
        }
        else if(indexLike > -1){
            like_users.splice(indexLike, 1)
            data={...data, like_users:like_users}
            value = -1
        } 
        else if(indexPraise > -1){
            praise_users.splice(indexPraise, 1)
            data={...data, praise_users:praise_users}
            value = -1
        } 
        else if(indexDislike > -1){
            dislike_users.splice(indexDislike, 1)
            data={...data, dislike_users:dislike_users}
            value = -1
        }
        this.setState({data}, () => {
            this.props.actions.discuss.checkAtions({value:value, info:data})
            this.potsAction(value)
            this.setTotalCount()})
    }

    checkAction(){
        const { data } = this.state
        let { like_users, dislike_users, praise_users } = data
        const { colleague } = this
        let actionUser = null
        const indexLike =  like_users.findIndex(m => m.id == colleague.haraId)
        const indexPraise =  praise_users.findIndex(m => m.id == colleague.haraId)
        const indexDislike=  dislike_users.findIndex(m => m.id == colleague.haraId)
        if(indexLike > -1){
            actionUser = {title:'Thích', color:(settingApp.color), image: <SvgCP.isHeart />, type:1}
        }
        else if( indexPraise > -1){
            actionUser = {title:'Tán dương', color:(settingApp.color), image: <SvgCP.isClap />, type:2}
        }
        else if( indexDislike > -1){
            actionUser = {title:'Không thích', color:(settingApp.color), image: <SvgCP.isDisLike />, type:3}
        }
        else{
            actionUser = {title:'Thích', color:(settingApp.colorDisable), image: <SvgCP.noneHeart />, type:-1}
        }
        return actionUser
    }

    setTotalCount(){
        let { data } = this.state;
        let { like_users, dislike_users, praise_users } = data;
        let totalCount =  like_users.length + dislike_users.length + praise_users.length;
        this.setState({totalCount:totalCount})
    }

    checkInfo(index1, index2, index3){
        let check = null
        if((index1 < 0 && (index2 >-1) && (index3 > -1))
        || (index1 < 0 && (index2 < 0) && (index3 > -1) )
        || (index1 < 0 && (index2 > -1) && (index3 < 0) )
        || (index1 < 0 && (index2 < 0) && (index3 < 0) )
        ){
            check = true
        } 
        else{
            check = false
        }
        return check
    }

    setOption(value){
        let { data } = this.state;
        const { colleague } = this
        let { like_users, dislike_users, praise_users } = data;
        const indexLike =  like_users.findIndex(m => m.id == colleague.haraId)
        const indexPraise =  praise_users.findIndex(m => m.id == colleague.haraId)
        const indexDislike=  dislike_users.findIndex(m => m.id == colleague.haraId)
        let infoUser = {name:colleague.fullName, id:colleague.haraId, email:colleague.email, avartar:colleague.photo}
        let acitons = null
        if(value == 1){
            let check = this.checkInfo(indexLike, indexPraise, indexDislike)
            if(check == true){
                if(indexDislike > -1){
                    dislike_users.splice(indexDislike, 1)
                }
                if(indexPraise > -1){
                    praise_users.splice(indexPraise, 1)
                }
                data = {...data, 
                    like_users:[...data.like_users, infoUser ], 
                    like_count:(data.like_count + 1),
                    dislike_users:dislike_users,
                    praise_users:praise_users,
                }
                acitons = 1
            } else {
                like_users.splice(indexLike, 1)
                data = {...data, like_users:like_users, like_count:(data.like_count - 1)}
                acitons = -1
            }
        }
        else if(value == 2){
            let check = this.checkInfo(indexPraise, indexLike, indexDislike)
            if(check == true){
                if(indexLike > -1){
                    like_users.splice(indexLike, 1)
                }
                if(indexDislike > -1){
                    dislike_users.splice(indexDislike, 1)
                }
                data = {...data, 
                    praise_users:[...data.praise_users, infoUser], 
                    praise_count:(data.praise_count + 1),
                    like_users:like_users,
                    dislike_users:dislike_users
                }
                acitons = 2
            }else{
                praise_users.splice(indexPraise, 1)
                data = {...data, praise_users:praise_users, praise_count:(data.praise_count - 1)}
                acitons = -1
            }
        }
        else if(value == 3) {
            let check =  this.checkInfo(indexDislike, indexLike, indexPraise)
            if(check == true){
                if(indexLike > -1){
                    like_users.splice(indexLike, 1)
                }
                if(indexPraise > -1){
                    praise_users.splice(indexPraise, 1)
                }
                data = {...data, 
                    dislike_users:[...data.dislike_users, infoUser], 
                    dislike_count:(data.dislike_count + 1),
                    like_users:like_users,
                    praise_users:praise_users
                }
                acitons = 3
            }else{
                dislike_users.splice(indexDislike, 1)
                data = {...data, dislike_users:dislike_users, dislike_count:(data.dislike_count - 1)}
                acitons = -1
            }
        }
        this.setState({data},() => {
            this.props.actions.discuss.checkAtions({value:value, info:this.state.data})
            this.checkAction()
            this.setTotalCount()
            this.potsAction(acitons)
            this.Popover.props.onClose()
        })
    }

    formatTime(time){
        let newtime = null
        let timeCheck = moment(time).fromNow();
        if(timeCheck.indexOf('trước') > -1){
            newtime=  timeCheck.replace('trước', '').trim()
        }
        else if(timeCheck.indexOf('ago') > -1){
            newtime= timeCheck.replace('ago', '').trim()
        }
        return newtime
    }

    getURI(){
        let { file } = this.state;
        let uri = null
        if(file && (file.length > 0)){
            file.map(item =>{
                uri = item.uri
            })
        }
        return uri
    }

    getSource(){
        const { data } = this;
        let url = null
        if(data && data.attachments && (data.attachments.length > 0)){
            data.attachments.map(item =>{
                url = (item && item.url) ? item.url : imgApp.no_Image
            })
        }else{
            url = imgApp.no_Image
        }
        return url
    }

    loadAgain(){
        let { totalPage, page } = this
        if(page < 3){
            this.loadComment()
        }
    }
    
    async loadComment(){
        let { currentList, totalPage, page, colleague, token } = this
        let listMore = []
        let newList = currentList
        if (!totalPage || (totalPage && !isNaN(totalPage) && (page - 1) <= totalPage)) {
            const resultComment = await HaravanGraphQL.getRepliesComment(token, this.data.id, page);
            if(resultComment && resultComment.data && resultComment.data.replied_comments ){
                const totalCount = resultComment.data.replied_comments.totalCount
                resultComment.data.replied_comments.data.map((item) =>{
                    if(page > 1){
                        listMore.push(item)
                    }
                    else{
                        newList.push(item)
                    }
                })
                newList = [...listMore, ...newList]
                this.totalPage = Math.ceil(totalCount / 10);
                this.currentList =  newList
                this.page = page + 1;
                this.setState({ 
                    listComment:newList, 
                    isLoadComment:false, 
                    loadMore:false,
                    totalCountPage: totalCount
                })
                    this.loadAgain()
            }
            else{
                this.setState({ listComment:[], isLoadComment:false, loadMore:false })

            }
        }
        else{
            this.setState({ isLoadComment:false, loadMore:false})
        }
    }

    componentWillUnmount(){
        this.setState({autoFocus:false})
    }
        
}
const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};
export default connect(mapStateToProps, mapDispatchToProps)(ReplyUser);