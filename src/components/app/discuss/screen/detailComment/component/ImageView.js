import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Modal , Platform} from 'react-native';
import { settingApp } from '../../../../../../public';
import { Ionicons } from '@expo/vector-icons';

class ImageView extends Component{
    constructor(props){
        super(props)
    }
    render(){
        const { isIPhoneX, width, statusBarHeight } = settingApp
        const top = isIPhoneX ? 44 : 0;
        return(
            <Modal
                visible={ this.props.visible}
                animationType='slide'
                transparent={true}
                onRequestClose={() => this.props.close()}
            >
                <View style={{flex:1, backgroundColor:'#000000', marginTop:Platform.OS === 'ios' ? (statusBarHeight) : 0}}>
                    <View style={{ width, height: 44, flexDirection: 'row',alignItems:'center', backgroundColor:'#000000' }} >
                            <TouchableOpacity
                                onPress={() => this.props.close()}
                                style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center',margin:10 }}
                            >
                                <Ionicons
                                    name='md-close'
                                    color='#9CA7B2'
                                    size={30}
                                />
                            </TouchableOpacity>
                    </View>

                <View style={{
                    width:(settingApp.width),
                    height:(settingApp.height -(44 + statusBarHeight))
                }}>
                    <Image 
                        style={{
                            resizeMode:'contain',
                            width:(settingApp.width),
                            height:(settingApp.height -(44 + statusBarHeight))
                            }}
                        source={{uri:`${this.props.url}`}}
                    />
                </View>

                </View>
            </Modal>
        )
    }
}
export default ImageView;