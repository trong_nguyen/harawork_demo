import React, { Component } from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { settingApp } from '../../../../../../public';
import { MaterialIcons } from '@expo/vector-icons'

class ItemCheckMuti extends Component{
    constructor(props){
        super(props)
        this.state={
            checkVote:false,
            list:[],
            meVote:true
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps && nextProps.listVote){
            this.setState({list:nextProps.listVote})
        }

        if(nextProps && nextProps.meVote){
            this.setState({meVote:nextProps.meVote})
        }
        else{
            this.setState({meVote: false})
        }
    }

    componentDidMount(){
        const { listVote, meVote } = this.props
        if(listVote && (listVote.length > 0)){
            this.setState({list:listVote})
        }
        
        if(meVote){
            this.setState({meVote:meVote})
        }
    }

    render(){
        const { item } = this.props
        const { list, meVote } = this.state;
        let checkVote = list.findIndex(e => e.id === item.id)
        return(
            <TouchableOpacity
                disabled={meVote} 
                onPress={() => this.props.getLitsVote(item)}
                style={[styles.lineItem,{opacity:meVote ? 0.5: 1}]}>
                <View style={styles.circle}>
                    <MaterialIcons name={(checkVote > -1) ? 'check-box' : 'check-box-outline-blank'} size={20} color={(checkVote > -1) ? settingApp.blueSky : settingApp.colorDisable} />
                </View>
                <Text style={styles.textItem}>{item.name}</Text>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    lineItem:{
        width:settingApp.width - 30,
        minHeight:50,
        flexDirection:'row',
        alignItems:'center'
    },
    circle:{
        minHeight:50,
        width:30,
        justifyContent:'center',
        alignItems:'center'
    },
    textItem:{
        fontSize:16, 
        color:settingApp.colorText
    },
})
export default ItemCheckMuti;