import React, { Component } from 'react';
import { View, Modal, CameraRoll, Image, TouchableOpacity, Platform , WebView, Text } from 'react-native';
import { settingApp, imgApp, Toast, ReviewImage, DownloadFile, Loading } from '../../../../../../public';
import Config from '../../../../../../Haravan/config';
import { Ionicons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import * as WebBrowser  from 'expo-web-browser';
import * as FileSystem from 'expo-file-system'
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';
import {MaterialCommunityIcons} from '@expo/vector-icons'

class OpenFile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dataSelected: null,
            type: null,
            isDownload: false,
        }
        this.userInfo = props.app.authHaravan.auth.access_token;
    }

    componentWillReceiveProps(nextProps) {
        if (JSON.stringify(this.state.dataSelected) !== JSON.stringify(nextProps.dataSelected)) {
            const { dataSelected } = nextProps;
            const { url, id, name } = dataSelected;
            let type = null;
            let isDownload = false;
            if ((name.match(/\.(jpg|jpeg|png|gif)$/) != null)) {
                type = 'image';
                isDownload = true;
            } else if ((name.match(/\.(pdf)$/) != null)) {
                type = 'pdf';
            }
            if ((name.match(/\.(jpg|jpeg|png|gif|doc|docx|xls|xlsx|txt)$/) != null)) {
                //isDownload = true;
            }
            this.setState({ dataSelected: nextProps.dataSelected, type, isDownload });
        }
    }

    async downloadFile(dataSelected) {
        const url = `https://ic.${Config.apiHost}${dataSelected.url}?isview=false&access_token=${this.userInfo}`; 
        await FileSystem.downloadAsync( url,
            `${FileSystem.documentDirectory}${dataSelected.name}`,
          )
            .then((res) => {
              console.log('Finished ');
            })
            .catch(error => {
              console.error(error);
            });
    }

    async downloadFileWeb(dataSelected){
        const url = `https://ic.${Config.apiHost}${dataSelected.url}?isview=true&access_token=${this.userInfo}`; 
        const fileUri =  `${FileSystem.documentDirectory}${dataSelected.name}`
        let downloadObject = FileSystem.createDownloadResumable(
            url,
            fileUri
          );
          let response = await downloadObject.downloadAsync();
    }

    renderHeader() {
        const { width, isIPhoneX } = settingApp;
        const top = isIPhoneX ? 44 : Platform.OS === 'ios' ? 30 : 0;
        return (
            <View style={{ position: 'absolute', top, left: 0, right: 0, ...settingApp.shadow }}>
                <View style={{ width, height: 44, flexDirection: 'row', justifyContent: 'space-between' }} >
                    <TouchableOpacity
                        onPress={() => this.props.close()}
                        style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center' }}
                    >
                        <Ionicons
                            name='md-close'
                            color='#9CA7B2'
                            size={30}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    renderViewIOS(){
        const { type, dataSelected } = this.state;
        if(dataSelected){
            const url = `https://ic.${Config.apiHost}/call/im_api/attachment/${dataSelected.id}?isview=true&access_token=${this.userInfo}`; 
            return(
                <WebView
                    style={{ flex:1 }} 
                    bounces={false}
                    scrollEnabled={false} 
                    source={{uri: url}}>
                </WebView>               
            )
        }
        else{
            return(
                <View />               
            )
        }
    }
    
    renderView() {
        const { type, dataSelected } = this.state;
        let content = <View />;
        if(dataSelected){
        //const url = `http://ic.${Config.apiHost}${dataSelected.url}?isView=true&access_token=${this.userInfo}`; 
            if (type == 'image') {
                content = ( 
                    <ReviewImage
                        id={this.state.dataSelected.id}
                    />
                )
            } 
            else {
                content = (
                    <View style={{flex:1, backgroundColor:'#FFFFFF', justifyContent:'center', alignItems:'center'}}>
                        <TouchableOpacity
                            onPress={() => this.downloadFile(dataSelected)}
                            >
                            <MaterialCommunityIcons name='file-download-outline' size={50} color={settingApp.colorDisable} />
                            <Text style={{fontSize:16, paddingTop:10, color:settingApp.colorDisable}}>Tải xuống</Text>
                        </TouchableOpacity>

                        <Text style={{fontSize:16, paddingTop:20, color:settingApp.colorDisable}}>Không có bản xem trước</Text>
                </View>    
                )
            }
            
        }
        else{
            content =  <View/>
            // <WebView
            //                 style={{ flex:1 }} 
            //                 bounces={false}
            //                 scrollEnabled={false} 
            //                 source={{uri: url}}
            //                 domStorageEnabled={true}
            //                 javaScriptEnabled={true}
            //             />
        }
        return content;
    }

    renderContent() {
        const { isIPhoneX } = settingApp;
        const marginTop = isIPhoneX ? 88 : Platform.OS === 'ios' ? 74 : 44;
        let content = Platform.OS === 'ios' ? this.renderViewIOS() : this.renderView()
        return (
            <View style={{flex:1, marginTop}}>
                {content}
            </View>
        )
    }

    render() {
        const { url } = this.props
        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.props.modalVisible}
                onRequestClose={() => this.props.close()}
                >
                <View style={{ flex: 1, backgroundColor: '#FFFF' }}>
                    {this.renderContent()}
                    {this.renderHeader()}
                </View>
            </Modal>
        )
    }
}

const mapStateToProps = (state) => ({ ...state })
export default connect(mapStateToProps)(OpenFile)