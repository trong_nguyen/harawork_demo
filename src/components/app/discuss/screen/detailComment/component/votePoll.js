import React, { Component } from 'react'
import { View, Text, TouchableOpacity, StyleSheet, ActivityIndicator } from 'react-native';
import { settingApp, Utils, Toast, SvgCP } from '../../../../../../public';
import { FlatList } from 'react-native-gesture-handler';
import { MaterialIcons, Feather } from '@expo/vector-icons'
import moment from 'moment';
import ItemCheckMuti from './itemCheckMuti'
import { HaravanIc } from '../../../../../../Haravan';
import { connect } from 'react-redux'

class VotePoll extends Component{
    constructor(props){
        super(props)
        this.state={
            dataVote:null,
            listVote:[],
            itemVote:'',
            disable:true,
            remove:false,
            isVoting:false,
            countVote:0,
            meVote:true
        }
        this.colleague = props.app.colleague
    }

    async componentDidMount(){
        const { data } = this.props;
        if(data){
            this.setState({dataVote:data})
        }
        await this.checkCount(data)
        await this.findMe(data)
        if(data.isExpried == true){
            this.setState({meVote:true})
        }
    }

    async loadData(){
        let { dataId, body  } = this.props
        const result = await HaravanIc.getThreadDetail(dataId, body)
        if(result && result.data && !result.error){
            this.setState({dataVote:result.data.poll },() =>{
                this.checkCount(this.state.dataVote)
            })
        }
    }

    findMe(data){
        const { colleague } = this
        let list = data.options;
        let newList = []
        for(let i = 0; i < list.length; i++){
            let newData= {...list[i]}
            if(list[i].votes && (list[i].votes.length > 0)){
                let index = list[i].votes.findIndex(m => m === colleague.haraId)
                if(index > -1){
                    newList.push(newData)
                }
            }
        }
        if(newList.length > 0){
            if((data.isMultiChoice == true)){
                this.setState({meVote:true, listVote:newList})
            }
            else{
                this.setState({meVote:true, itemVote:newList[0]})
            }
        }
        else{
            this.setState({meVote:false})
        }
    }

    async votePoll(){
        const { data } = this.props;
        const { listVote, itemVote } = this.state
        let list = []
        if(data.isMultiChoice == true){
            listVote.map(item =>{
                list.push(item.id)
            })
        }
        else{
            list.push(itemVote.id)
        }
        const result = await HaravanIc.votePoll(data.id, list);
        if(result && !result.error){
            this.setState({remove:true, disable:true, isVoting:false, meVote:true},() =>{
                //Toast.show('Bình chọn thành công')
            })
        }
        else{
            this.setState({remove:true, disable:true, isVoting:false},() =>{
            })
        }
    }

    async removePoll(){
        const { data } = this.props;
        const { listVote, itemVote } = this.state
        const result = await HaravanIc.removePoll(data.id);
        if(result && !result.error){
            await this.loadData()
            this.setState({remove:false, disable:true, meVote:false, listVote:[], listMeVote:[], itemVote:''})
        }
        else{
            this.setState({remove:false, disable:false, },() =>{
            })
        }
    }

    checkCount(data){
        let count = 0;
        let list = data.options
        for(let i =0 ; i <list.length; i++){
            count = count + (list[i].votes.length)
        }
        this.setState({countVote:count})
    }

    checkDisable(){
        const { data } = this.props;
        const { listVote, itemVote } = this.state;
        let disable = null
        if(data.isMultiChoice == true){
            disable = (listVote.length ==0)
        }
        else{
            disable = (itemVote.length == 0)
        }
        this.setState({disable:disable})
    }

    voteItemOnly(item){
        this.setState({listVote:[], itemVote:item},() =>{
            this.checkDisable()
        })
    }

    voteItemMuti(item){
        let { listVote } = this.state
        let newList = listVote
        let index = newList.findIndex(m => m.id === item.id);
        if(index < 0){
            newList.push(item)
        }
        else{
            newList.splice(index, 1)
        }
        this.setState({listVote: newList},()=> this.checkDisable())
    }

    checkVote(item){
        let { listVote } = this.state
        let vote = null
        listVote.map(e =>{
            if(e.id === item.id){
                vote = true;
                return vote
            }
            else{
                vote = false;
                return vote
            }
        })
    }

    renderTitle(){
        const { data } = this.props;
        return(
            <View style={styles.coverTitle}>
                <Text style={styles.textTitle}>{data.title}</Text>
            </View>
        )
    }

    renderItem(obj){
        const { item, index} = obj;
        let { itemVote , meVote} = this.state
        let checkVote = (item.id === itemVote.id)
        return(
            <TouchableOpacity
                disabled={meVote}
                onPress={() => this.voteItemOnly(item)}
                style={[styles.lineItem,{opacity:meVote ? 0.5: 1}]}>
                <View style={styles.circle}>
                    <MaterialIcons name={checkVote ? 'radio-button-checked' : 'radio-button-unchecked'} size={20} color={checkVote ? settingApp.blueSky : settingApp.colorDisable} />
                </View>
                <Text style={styles.textItem}>{item.name}</Text>
            </TouchableOpacity>
        )
    }

    renderItemMuti(obj){
        const { item, index} = obj;
        let { itemVote, listVote, meVote } = this.state
        return(
           <ItemCheckMuti
                meVote= { meVote } 
                item = { item }
                listVote = { listVote }
                getLitsVote = {(data) => this.voteItemMuti(data)}
           />
        )
    }

    renderListOption(){
        const { dataVote } = this.state
        return(
            <FlatList
                data={dataVote.options}
                extraData={this.state}
                keyExtractor={(item, index) => item.id ? item.id+'' : index+''}
                renderItem={(obj) => dataVote.isMultiChoice ? this.renderItemMuti(obj) : this.renderItem(obj)}
            />
        )
    }

    renderButton(){
        const { disable, isVoting } = this.state
        return(
            <TouchableOpacity 
                onPress={() => 
                    this.setState({isVoting:true},() =>{
                        this.votePoll()
                    })
                }
                disabled={disable}
                style={[styles.buttonAccept,{opacity:disable ? 0.4 :1}]}>
                <View style={styles.iconButton}>
                    {!isVoting && <Feather name='check' size={20} color={settingApp.colorText} />}
                    {isVoting && this.loadVoting()}
                </View>
                <Text style={styles.textButton}>Hoàn tất</Text>
            </TouchableOpacity>
        )
    }

    renderRemoveVote(){
        return(
            <TouchableOpacity 
                onPress={() => this.removePoll()}
                style={styles.buttonRemove}>
                <View style={styles.iconRemove}>
                    <SvgCP.iconReset/>
                </View>
                <Text style={styles.textRemove}>Bình chọn lại</Text>
            </TouchableOpacity>
        )
    }

    renderLine(date, timeEnd){
        const { data } = this.props
        if(data && data.isExpried && (data.isExpried ==true)){
            return(
                <View>
                    {this.renderEndVote(date, timeEnd)}
                </View>
            )
        }
        else{
            return(
                <Text>
                    <Text style={styles.dateDisable}>{`* Cuộc thăm dò ý kiến sẽ kết thúc vào `}</Text>
                    <Text style={styles.dateWeigth}>{timeEnd}</Text>  
                    <Text style={styles.dateDisable}> ngày </Text>
                    <Text style={styles.dateWeigth}>{date}</Text>
                </Text> 
            )
        }
    }
    
    renderEndVote(date, timeEnd){
        const { countVote  } = this.state;
        return(
            <Text>
                <Text style={styles.dateDisable}>{`Cuộc thăm dò ý kiến đã có `}</Text>
                <Text style={styles.dateWeigth}>{countVote}</Text>
                <Text style={styles.dateDisable}> bình chọn và đã kết thúc vào </Text>
                <Text style={styles.dateWeigth}>{timeEnd}</Text>
                <Text style={styles.dateDisable}> ngày </Text>
                <Text style={styles.dateWeigth}>{date}</Text>
            </Text>
        )
    }


    renderSeenTotal(){
        const { countVote } = this.state
        return(
            <TouchableOpacity 
                onPress={() => this.props.navigation.navigate('DetailVotePoll',{
                    data:this.props.data,
                    dataId:this.props.dataId,
                    body:this.props.body,
                    meVote:this.state.meVote,
                    actions: () => this.removePoll()
                })}
                style={styles.buttonSeen}>
                <Text style={styles.textSeen}>Xem kết quả bình chọn</Text>
            </TouchableOpacity>
        )
    }
    
    render(){
        const { data } = this.props;
        const { meVote , dataVote} = this.state
        let date = data && data.endDate && Utils.formatTime(data.endDate, 'DD/MM/YYYY', true)
        let timeEnd =data && data.endDate && moment(data.endDate).format('HH:mm')
        return(
            <View style={styles.main}>
                {this.renderTitle()}
                {dataVote && this.renderListOption()}
                {!data.isExpried && !meVote && this.renderButton()}
                {meVote && !data.isExpried && this.renderRemoveVote()}
                {date && timeEnd && this.renderLine(date, timeEnd)}
                {this.renderSeenTotal()}
            </View>
        )
    }

    loadVoting(){
        return(
            <View style={styles.iconButton}>
                <ActivityIndicator size={'small'} color={settingApp.colorText} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main:{
        flex:1, width:(settingApp.width -30), 
        minHeight:20,
        borderTopColor:settingApp.colorSperator,
        borderTopWidth:1,
        paddingTop:10,
        paddingBottom:10
    },
    coverTitle:{
        width:settingApp.width -30,
        minHeight:60,
        paddingTop:10,
        paddingBottom:5
    },
    textTitle:{
        fontSize:18,
        fontWeight:'600',
        color:settingApp.colorText
    },
    lineItem:{
        width:settingApp.width - 30,
        minHeight:50,
        flexDirection:'row',
        alignItems:'center'
    },
    circle:{
        minHeight:50,
        width:30,
        justifyContent:'center',
        alignItems:'center'
    },
    textItem:{
        fontSize:16, 
        color:settingApp.colorText
    },
    coverDate:{
        width:settingApp.width - 30,
        minHeight:20,
        alignItems:'center',
        flexDirection:'row',
        flexWrap:'wrap'
    },
    dateDisable:{
        fontSize:14,
        color:settingApp.colorDisable
    },
    dateWeigth:{
        fontSize:14,
        color:settingApp.colorText,
        fontWeight:'600'
    },
    buttonAccept:{
        width:120,
        height:50,
        justifyContent:'flex-start',
        alignItems:'center',
        flexDirection:'row',
        backgroundColor:'#EEEEEE',
        borderRadius:4,
        marginLeft:10,
        marginBottom:15,
        marginTop:10
    },
    iconButton:{
        height:50,
        width:30,
        justifyContent:'center',
        alignItems:'center', 
        paddingLeft:10
    },
    textButton:{
        fontSize:16,
        color:settingApp.colorText,
        fontWeight:'600',
        paddingLeft:5
    },
    buttonSeen:{
        width:settingApp.width -30,
        height:40,
        justifyContent:'center'
    },
    textSeen:{
        fontSize:14,
        color:settingApp.blueSky,
        textDecorationLine:'underline'
    },
    buttonRemove:{
        width:120,
        height:40,
        flexDirection:'row',
        justifyContent:'flex-start',
        alignItems:'center'
    },
    textRemove:{
        color:settingApp.blueSky,
        fontSize:14,
        paddingLeft:5
    },
    iconRemove:{
        height:50,
        width:20,
        justifyContent:'center',
        alignItems:'center', 
        paddingLeft:5
    }
})
const mapStateToProps = (state) => ({ ...state })
export default connect(mapStateToProps)(VotePoll);