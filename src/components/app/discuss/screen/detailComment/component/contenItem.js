import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Platform , Image} from 'react-native';
import { ParseText, settingApp, Utils, AvatarCustom, SvgCP, imgApp, AsyncImage } from '../../../../../../public';
import { Popover, PopoverController } from 'react-native-modal-popover';
import * as Animatable from 'react-native-animatable';
import { HaravanGraphQL } from '../../../../../../Haravan';
import ImageView from './ImageView';
import {connect} from 'react-redux';
import actions from '../../../../../../state/action';
import { bindActionCreators } from 'redux';
import { Feather } from '@expo/vector-icons';
import HTML from 'react-native-render-html';

class ContentItem extends Component{
    constructor(props){
        super(props)
        this.state = {
            data: null,
            actions_user:-1,
            totalCount:0,
            showImage:false,
            userInfoReply:null,
        }
        this.colleague = props.colleague
        this.actionComment = props.discuss.checkAtions;
    }

    componentWillReceiveProps(nextProps){
        if(nextProps && (nextProps.discuss.checkAtions)){
            let { info , value} = nextProps.discuss.checkAtions
            let {data} = this.state;
            if( (info.id == data.id)){
                this.setState({data:info},() =>{
                    this.checkAction()
                })
            }
        }
        if(nextProps && (nextProps.discuss.addReply) && (nextProps.discuss.addReply.id === this.state.data.id)){
            let {userInfoReply} = this.state
            const { colleague } = this;
            const { addReply } = nextProps.discuss
            userInfoReply = {name: colleague.fullName, photo: colleague.photo, id:colleague.haraId}
            let content = {...addReply, replies:{...addReply.replies, totalCount:(addReply.replies.totalCount)}}
            this.setState({data:content, userInfoReply},() =>{
                this.props.actions.discuss.addReply(null)
            })
        }
    }

    componentDidMount(){
        const { data, level } = this.props;
        if(data){
            let { like_users, dislike_users, praise_users } = data
            let totalCount =  like_users.length + dislike_users.length + praise_users.length;
            this.setState({data:{...data, level:level}, totalCount:totalCount, })
        }
    }

    checkRemove(dataCheck){
        let { dataRemove, list } = dataCheck;
        let { data, totalCount } = this.state;
        let listReplie = data.replies.data
        if(list && list.length >0){
            list.map(item =>{
                let index = listReplie.findIndex(m => m.id === item.id)
                if(index > -1){
                    listReplie.splice(index, 1)
                }
            })
        }
        let total = (data.replies.totalCount*1) - list.length
        let newData = {...data, replies:{data:listReplie, totalCount:total}}
        this.setState({data:newData})
    }

    checkNoneLongPress(){
        let { data} = this.state;
        const { colleague } = this
        let value = null
        let { like_users, dislike_users, praise_users } = data;
        const indexLike =  like_users.findIndex(m => m.id == colleague.haraId)
        const indexPraise =  praise_users.findIndex(m => m.id == colleague.haraId)
        const indexDislike=  dislike_users.findIndex(m => m.id == colleague.haraId)
        let infoUser = {name:colleague.fullName, id:colleague.haraId, email:colleague.email, avartar:colleague.photo}
        if((indexLike <0) && (indexPraise < 0) && (indexDislike < 0)){
            data = {...data, like_users:[...like_users, infoUser]}
            value = 1
        }
        else if(indexLike > -1){
            like_users.splice(indexLike, 1)
            data={...data, like_users:like_users}
            value = -1
        } 
        else if(indexPraise > -1){
            praise_users.splice(indexPraise, 1)
            data={...data, praise_users:praise_users}
            value = -1
        } 
        else if(indexDislike > -1){
            dislike_users.splice(indexDislike, 1)
            data={...data, dislike_users:dislike_users}
            value = -1
        }
        this.setState({data}, () => {
            this.props.actions.discuss.checkAtions({value:value, info:data})
            this.potsAction(value)
            this.setTotalCount()})
    }

    async potsAction(value){
        const { token } = this.props;
        const { data } = this.state;
        let params = null
        if(value == 1){
           params = `likeComment(token:"${token}", id:"${data.id}")` 
        }
        else if (value == 2){
            params = `praiseComment(token:"${token}", id:"${data.id}")` 
        }
        else if (value == 3){
            params = `dislikeComment(token:"${token}", id:"${data.id}")` 
        } else{
            params = `removeActionComment(token:"${token}", id:"${data.id}")` 
        }
       await HaravanGraphQL.actionComment(params)
    }

    checkAction(){
        const { data } = this.state
        let { like_users, dislike_users, praise_users } = data
        const { colleague } = this
        let actionUser = null
        const indexLike =  like_users.findIndex(m => m.id == colleague.haraId)
        const indexPraise =  praise_users.findIndex(m => m.id == colleague.haraId)
        const indexDislike=  dislike_users.findIndex(m => m.id == colleague.haraId)
        if(indexLike > -1){
            actionUser = {title:'Thích', color:(settingApp.color), image: <SvgCP.isHeart />, type:1}
        }
        else if( indexPraise > -1){
            actionUser = {title:'Tán dương', color:(settingApp.color), image: <SvgCP.isClap />, type:2}
        }
        else if( indexDislike > -1){
            actionUser = {title:'Không thích', color:(settingApp.color), image: <SvgCP.isDisLike />, type:3}
        }
        else{
            actionUser = {title:'Thích', color:(settingApp.colorDisable), image: <SvgCP.noneHeart />, type:-1}
        }
        return actionUser
    }

    checkInfo(index1, index2, index3){
        let check = null
        if((index1 < 0 && (index2 >-1) && (index3 > -1))
        || (index1 < 0 && (index2 < 0) && (index3 > -1) )
        || (index1 < 0 && (index2 > -1) && (index3 < 0) )
        || (index1 < 0 && (index2 < 0) && (index3 < 0) )
        ){
            check = true
        } 
        else{
            check = false
        }
        return check
    }

    setTotalCount(){
        let { data } = this.state;
        let { like_users, dislike_users, praise_users } = data;
        let totalCount =  like_users.length + dislike_users.length + praise_users.length;
        this.setState({totalCount:totalCount})
    }

    setOption(value){
        let { data } = this.state;
        const { colleague } = this
        let { like_users, dislike_users, praise_users } = data;
        const indexLike =  like_users.findIndex(m => m.id == colleague.haraId)
        const indexPraise =  praise_users.findIndex(m => m.id == colleague.haraId)
        const indexDislike=  dislike_users.findIndex(m => m.id == colleague.haraId)
        let infoUser = {name:colleague.fullName, id:colleague.haraId, email:colleague.email, avartar:colleague.photo}
        let acitons = null
        if(value == 1){
            let check = this.checkInfo(indexLike, indexPraise, indexDislike)
            if(check == true){
                if(indexDislike > -1){
                    dislike_users.splice(indexDislike, 1)
                }
                if(indexPraise > -1){
                    praise_users.splice(indexPraise, 1)
                }
                data = {...data, 
                    like_users:[...data.like_users, infoUser ], 
                    like_count:(data.like_count + 1),
                    dislike_users:dislike_users,
                    praise_users:praise_users,
                }
                acitons = 1
            } else {
                like_users.splice(indexLike, 1)
                data = {...data, like_users:like_users, like_count:(data.like_count - 1)}
                acitons = -1
            }
        }
        else if(value == 2){
            let check = this.checkInfo(indexPraise, indexLike, indexDislike)
            if(check == true){
                if(indexLike > -1){
                    like_users.splice(indexLike, 1)
                }
                if(indexDislike > -1){
                    dislike_users.splice(indexDislike, 1)
                }
                data = {...data, 
                    praise_users:[...data.praise_users, infoUser], 
                    praise_count:(data.praise_count + 1),
                    like_users:like_users,
                    dislike_users:dislike_users
                }
                acitons = 2
            }else{
                praise_users.splice(indexPraise, 1)
                data = {...data, praise_users:praise_users, praise_count:(data.praise_count - 1)}
                acitons = -1
            }
        }
        else if(value == 3) {
            let check =  this.checkInfo(indexDislike, indexLike, indexPraise)
            if(check == true){
                if(indexLike > -1){
                    like_users.splice(indexLike, 1)
                }
                if(indexPraise > -1){
                    praise_users.splice(indexPraise, 1)
                }
                data = {...data, 
                    dislike_users:[...data.dislike_users, infoUser], 
                    dislike_count:(data.dislike_count + 1),
                    like_users:like_users,
                    praise_users:praise_users
                }
                acitons = 3
            }else{
                dislike_users.splice(indexDislike, 1)
                data = {...data, dislike_users:dislike_users, dislike_count:(data.dislike_count - 1)}
                acitons = -1
            }
        }
        this.setState({data},() => {
            this.props.actions.discuss.checkAtions({value:value, info:data})
            this.checkAction()
            this.setTotalCount()
            this.potsAction(acitons)
            this.Popover.props.onClose()
        })
    }

    buttonAction(checkAction){
        return(
            <View style={{flexDirection:'row', width:(settingApp.width), height:50, alignItems:'center'}}>
                <View
                    style={{
                        height:50,
                        width:150,
                        backgroundColor:'#FFFFFF',
                        borderRadius:15,
                        flexDirection:'row',
                        ...settingApp.shadow
                        }}>
                        <Animatable.View
                            duration = {200}
                            delay ={200}
                            animation='bounceIn'
                            style={{width:50, height:50, alignItems:'center', justifyContent:'center'}}
                        >
                            <TouchableOpacity
                                onPress={() => {this.setOption(1)}}
                                style={{alignItems:'center', justifyContent:'center'}}
                            > 
                                <SvgCP.isHeart/>
                            </TouchableOpacity>
                        </Animatable.View>
                                
                        <Animatable.View
                            duration = {250}
                            delay ={250}
                            animation='bounceIn'
                            style={{width:50, height:50, alignItems:'center', justifyContent:'center'}}
                         >
                            <TouchableOpacity
                                onPress={() => this.setOption(2)}
                                style={{alignItems:'center', justifyContent:'center'}}
                                > 
                                    <SvgCP.isClap/>
                            </TouchableOpacity>
                        </Animatable.View>
                        
                        <Animatable.View
                            duration = {300}
                            delay ={300}
                            animation='bounceIn'
                            style={{width:50, height:50, alignItems:'center', justifyContent:'center'}}
                        >
                            <TouchableOpacity
                                onPress={() => this.setOption(3)}
                                style={{alignItems:'center', justifyContent:'center'}}
                            > 
                                <SvgCP.isDisLike/>
                            </TouchableOpacity>
                        </Animatable.View>
                </View>
            </View>
        )
    }

    optionActions(){
        let checkAction =  this.checkAction()
        return(
            <PopoverController>
                {({ openPopover, closePopover, popoverVisible, setPopoverAnchor, popoverAnchorRect }) => (
                    <React.Fragment>
                        <TouchableOpacity 
                            onPress={() => this.checkNoneLongPress()}
                            ref={setPopoverAnchor} 
                            onLongPress={openPopover}
                            style={styles.buttonAction}
                        >
                            <Text style={[styles.textAction, {color:checkAction.color, bottom:8}]}>{checkAction.title}</Text>
                        </TouchableOpacity> 
                    <Popover 
                        ref = {ref => this.Popover = ref}
                        contentStyle={styles.content}
                        arrowStyle={styles.arrow}
                        backgroundStyle={styles.background}
                        visible={popoverVisible}
                        onClose={closePopover}
                        fromRect={popoverAnchorRect}
                        supportedOrientations={['portrait', 'landscape']}
                    >   
                        {this.buttonAction(checkAction)}
                    </Popover>
                    </React.Fragment>
                )}
            </PopoverController>
        )
    }

    renderAction(){
        const { data , totalCount} = this.state;
        let  {like_users, dislike_users, praise_users} = data;
        let check =  (like_users.length > 0 )&& (dislike_users.length > 0) && (praise_users.length > 0)
        return(
            <View style={{flexDirection:'row', bottom:10}}>
                { (like_users.length > 0) ?
                    <View style={{zIndex:90, right:(check ? 0 : -5 )}}>
                        <SvgCP.isHeart/>
                    </View>
                    :
                    <View/>
                }
                { (praise_users.length > 0)  ? 
                    <View style={{ zIndex:80, right:(check ? 5 : 0)}}>
                        <SvgCP.isClap/>
                    </View>
                    : <View />
                }
                { (dislike_users.length > 0)  ? 
                    <View style={{zIndex:70, right:( check ? 10 : (praise_users.length > 0)  ? 5 : 0)}}>
                        <SvgCP.isDisLike/>
                    </View>
                    :<View/>
                }
            </View>
        )
    }

    getSource(){
        const { data } = this.state;
        let url = null
        if(data && data.attachments && (data.attachments.length > 0)){
            data.attachments.map(item =>{
                if(item){
                    url = (item && item.url) ? item.url : imgApp.no_Image
                }
                else{
                    url = imgApp.no_Image
                }
            })
        }
        else{
            url = imgApp.no_Image
        }
        return url
    }

    buttonReplies(){
        const {parentData, level} = this.props
        return(
            <TouchableOpacity
                onPress={() => {
                    level == 2 ?
                        this.props.autoFocus() 
                        :
                        this.props.navigation.navigate('ReplyUser', {
                            data:((level ==1) ? this.state.data : parentData), 
                            token:this.props.token,
                            reply:true,
                            actions:(dataRemove) => this.checkRemove(dataRemove)
                    })
                    }
                }
                style={{height:40, padding:5}}
            >
                <Text style={{fontSize:14, color:(settingApp.colorDisable), fontWeight:'600', marginLeft:10}}>Trả lời</Text>
            </TouchableOpacity>
        )
    }

    render(){
        let { name, photo, time, content, level } = this.props;
        const { data, totalCount } = this.state
        content = content ?  content : '<b/>'
        let source = this.getSource()
        let imageUser = Utils.getLinkImage(this.props.data.created_By.id)
        return(
            <View style={{ flex: 1}}>
                {((data && data.attachments.length > 0) && (source)) &&
                    <ImageView 
                        visible = { this.state.showImage}
                        url = { source }
                        close = {() => this.setState({showImage:false})}
                    />
                }
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <AvatarCustom userInfo={{ name: name, picture: imageUser }} v={30} fontSize={12}/>
                        <Text style={{
                            fontSize:14, 
                            color:(settingApp.colorText), 
                            paddingLeft:10,
                            fontWeight:'bold'
                            }}>{name}</Text>
                    </View>
                    <View style={{paddingLeft:5, paddingTop:5, backgroundColor:'#FFFFFF', justifyContent:'center', width:(settingApp.width*0.85),}}>
                        <HTML  
                            html={content}
                            baseFontStyle={{fontSize:16, lineHeight:20}}                                
                            />
                    </View>
                    {((data && data.attachments.length > 0) && (source)) &&
                        <TouchableOpacity 
                            onPress={() => this.setState({showImage:true})}
                            style={{paddingTop:5,paddingLeft:5, backgroundColor:'#FFFFFF', paddingBottom:10, width:120, height:120}}>
                            {/* <Image 
                                source={{uri: `${source}`}}
                                style={{resizeMode:'contain', width:120, height:120}}
                            /> */}
                            <AsyncImage
                                source={{ uri: source ? source : imgApp.no_Image }}
                                style={{ width: 120, height: 120, backgroundColor:source ? 'transparent' : settingApp.colorSperator }}
                                resizeMode='contain'
                            />
                        </TouchableOpacity>
                    }
                    <View style={{flexDirection:'row', alignItems:'center',backgroundColor:'#FFFFFF', width:((level == 1) ? (settingApp.width-40) : (settingApp.width-70)),height:44, marginTop:10, justifyContent:'space-between'}}>
                        <View style={{flexDirection:'row', alignItems:'center', width:(settingApp.width*0.6)}}>
                            <View style={{height:40, padding:5, bottom:0, marginRight:5}}>
                                <Text style={{fontSize:14, color:(settingApp.colorDisable)}}>{time}</Text>
                            </View>
                            {data ? this.optionActions() : <View/>}
                            <View style={{bottom:0,}}>
                                {this.buttonReplies()}
                            </View>
                        </View>

                        <View style={{flexDirection:'row', paddingLeft:10, alignItems:'center', justifyContent:'center'}}> 
                            {data ?  this.renderAction(): <View/>}
                            {(totalCount > 0) ?  <Text style={{ color:settingApp.colorText, fontSize:14, top:-8, marginLeft:5 }}>{totalCount}</Text> : null}
                        </View>
                    </View>
                    {this.renderItemReplay()}
            </View>
        )
    }

    renderItemReplay(){
        const {userReplies } = this.props;
        const { data, userInfoReply } = this.state;
        if(data && data.replies && data.replies.data && (data.replies.data.length > 0) && (userReplies || userInfoReply)){
            return(
                <TouchableOpacity
                    onPress={() =>
                        this.props.navigation.navigate('ReplyUser', {
                                data: data, 
                                token:this.props.token,
                                reply:false,
                                actions:(dataRemove) => this.checkRemove(dataRemove)
                            })
                        }
                    style={{
                        height:40, width:(settingApp.width), backgroundColor:'#FFFFFF', alignItems:'center', flexDirection:'row',
                        paddingLeft:40,
                        top:-10
                        }}
                >
                    <Feather name='corner-down-right' color={settingApp.blueSky} size={15} />                    
                    <Text
                        style={{ color:(settingApp.blueSky), fontSize:16, paddingLeft:5, textAlign:'center'}}
                        >{`Xem ${data.replies.totalCount} trả lời`}</Text>
                </TouchableOpacity>
            )
        }
        else{
            return  <View />
        }
    }

}
const styles ={
    app: {
        ...StyleSheet.absoluteFillObject,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'transparent',
      },
      content: {
        backgroundColor: 'transparent',
        top: Platform.OS === 'ios' ?    10 : -10,
        left:50,
        width:150,
        borderRadius: 15,
      },
      arrow: {
        backgroundColor: 'transparent',
        borderTopColor: 'transparent',
      },
      background: {
        backgroundColor: 'transparent'
      },
      textAction:{
        fontSize:14, 
        fontWeight:'600', 
        paddingLeft:5, 
        paddingTop:3
    },
    buttonAction:{
        height:40, 
        justifyContent:'center', 
        flexDirection:'row',
        paddingTop:10,
    },
}
const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};
export default connect(mapStateToProps, mapDispatchToProps)(ContentItem);