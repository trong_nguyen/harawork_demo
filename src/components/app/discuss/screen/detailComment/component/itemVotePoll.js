import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { AvatarCustom, settingApp } from '../../../../../../public';
import { ProgressBar } from 'react-native-paper';
import { HaravanHr } from '../../../../../../Haravan';

class ItemVotePoll extends Component{
    constructor(props){
        super(props)
        this.state={
            listUser:[]
        }
    }

    async componentDidMount(){
        const { item, value } = this.props;
        let { listUser } = this.state
        if(item && item.votes){
            for(let i = 0; i < item.votes.length; i++){
                let result = await HaravanHr.getColleague(item.votes[i])
                if(result && result.data && !result.error){
                    listUser.push(result.data)
                }
            }
            this.setState({listUser})
        }
    }

    renderProgress(){
        return(
            <View style={styles.coverPro}>
               <ProgressBar 
                    progress={0.2} color={'#30C894'} 
                />
            </View>
        )
    }

    renderItemUser(value, length, userInfo){
        let count = (length - 4)
        return(
            <View style={[styles.iconUser,{backgroundColor:'#B2DAFF'}]}>
               {value == 4 && <Text style={styles.textUSer}>{`+${count}`}</Text>}
               {value != 4 && userInfo &&
                    <AvatarCustom userInfo={{ name:userInfo.fullName , picture: userInfo.photo }} v={25} fontSize={12} />
               }
            </View>
        )
    }

    render(){
        const { item, value } = this.props;
        const { listUser } = this.state
        if(item){
            return(
                <TouchableOpacity 
                    onPress={() =>{
                        this.props.navigation.navigate('DetailUserVote', {data:item})
                    }}
                    style={styles.viewBar}>
                    <Text style={styles.titleItem}>{item.name}</Text>
                    <View style={styles.coverPro}>
                        <ProgressBar 
                            progress={value} color={'#30C894'}
                        />
                    </View>
                    <View style={styles.coverItem}>
                        <Text style={styles.textWeigth}>{`${item.votes.length} bình chọn`}</Text>
                        {listUser[0] && this.renderItemUser(0, item.votes.length, listUser[0])}
                        {listUser[1] && this.renderItemUser(1, item.votes.length, listUser[1])}
                        {listUser[2] && this.renderItemUser(2, item.votes.length, listUser[2])}
                        {listUser[3] && this.renderItemUser(3, item.votes.length, listUser[3])}
                        {listUser[4] && this.renderItemUser(4, item.votes.length, listUser[4])}
                    </View>
                </TouchableOpacity>
            )
        }
    }

}
const styles = StyleSheet.create({
    coverPro:{
        width:settingApp.width-30, 
        height:5, 
        borderRadius:6, 
    },
    viewBar:{
        width:settingApp.width,
        minHeight:60,
        justifyContent:'center',
        padding:15,
        marginTop:10
    },
    titleItem:{
        fontSize:16,
        color:settingApp.colorText,
        paddingBottom:5,
        paddingTop:5
    },
    titleVote:{
        fontWeight:'bold',
        fontSize:18,
        color:settingApp.colorText,
        width:settingApp.width - 30,
        marginLeft:15
    },
    coverItem:{
        width:settingApp.width -30, 
        minHeight:40, 
        flexDirection:'row', 
        marginTop:10, 
        alignItems:'center'
    },
    iconUser:{
        width:25, 
        height:25, 
        borderRadius:12.5, 
        marginLeft:5, 
        justifyContent:'center', 
        alignItems:'center'
    },
    textUSer:{
        fontSize:14,
        color:settingApp.blueSky
    },
    textInfo:{
        width:settingApp.width -30,
        paddingLeft:15,
        marginBottom:10
    },
    textWeigth:{
        fontSize:14, 
        fontWeight:'500',
        color:settingApp.colorText
    },
})
export default ItemVotePoll