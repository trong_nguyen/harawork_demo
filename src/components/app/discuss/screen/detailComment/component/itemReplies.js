import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { ParseText, settingApp, Utils, AvatarCustom } from '../../../../../../public';
import moment from 'moment';
import ContentItem from './contenItem';
import { HaravanHr } from '../../../../../../Haravan';

class ItemReplies  extends Component{
    constructor(props){
        super(props)
        this.state={
            data:null,
            index: 0
        }
    }

    async componentDidMount(){
        const { item , colleague, index} = this.props;
        let { data } = this.state
        if(item){
            if(item.created_By.id == colleague.haraId){
                data = { ...item, created_By:{...item.created_By, photo: colleague.photo}}
            }
            else{
                let resultUser = await HaravanHr.getColleague(item.created_By.id)
                if(resultUser && resultUser.data && !resultUser.error){
                    data ={...item , created_By: { ...item.created_By, photo:resultUser.data.photo}}
                }
                else{
                    data = {...item , created_By: { ...item.created_By, photo:""}}
                }
            }
            this.setState({data, index: index})
        }
    }

    formatTime(time){
        let newtime = null
        let timeCheck = moment(time).fromNow();
        if(timeCheck.indexOf('trước') > -1){
            newtime=  timeCheck.replace('trước', '').trim()
        }
        else if(timeCheck.indexOf('ago') > -1){
            newtime= timeCheck.replace('ago', '').trim()
        }
        return newtime
    }

    buttonReplies(){
        return(
            <TouchableOpacity
                onPress={() => this.props.autoFocus(true)}
                style={{height:40, padding:5}}
            >
                <Text style={{fontSize:14, color:(settingApp.colorDisable), fontWeight:'600'}}>Trả lời</Text>
            </TouchableOpacity>
        )
    }

    render(){
        const { data, index } = this.state;
        if(data){
            let time = this.formatTime(data.created_At)
            return(
                <TouchableOpacity 
                    onLongPress={() => this.props.editComment({item:data,editComment:true})}
                    style={{ width:(settingApp.width), borderTopWidth: 1, borderTopColor: settingApp.colorSperator, paddingTop:10}}>
                    <ContentItem
                        data = {data} 
                        name= {data.created_By.name}
                        photo= { data.created_By.photo }
                        content = { data.content }
                        time = { time}
                        level = {2}
                        colleague = { this.props.colleague }
                        token ={this.props.token}
                        parentData={this.props.parentData}
                        navigation={this.props.navigation}
                        userReplies = { null }
                        autoFocus={() => this.props.autoFocus(true)}
                    />
                </TouchableOpacity>
            )
        }
        else{
             return ( <View />)
        }
    }
}
export default ItemReplies;