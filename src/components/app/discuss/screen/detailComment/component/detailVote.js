import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, FlatList } from 'react-native';
import { settingApp, HeaderScreenNews, Utils, Loading, SvgCP, AvatarCustom } from '../../../../../../public';
import { ProgressBar } from 'react-native-paper';
import { HaravanIc, HaravanHr } from '../../../../../../Haravan';
import ItemVotePoll from './itemVotePoll'

class DetailVote extends Component{
    constructor(props){
        super(props)
        this.state={
           dataPoll:[],
           isLoading:true,
           totalVote:0
        }  
        this.data= props.navigation.state.params.data;
        this.dataId = props.navigation.state.params.dataId;
        this.body = props.navigation.state.params.body;
        this.meVote = props.navigation.state.params.meVote;
        this.actions = props.navigation.state.params.actions;
    }

    componentDidMount(){
        this.loadData()
    }

    async loadData(){
        const { dataId, body } = this;
        const result = await HaravanIc.getThreadDetail(dataId, body)
        if(result && result.data && !result.error){
            this.setState({dataPoll:result.data.poll, isLoading:false},() =>{
                this.checkCount(this.state.dataPoll)
            })
        }
    }

    async getUserInfo(id){
        let data = null
        let result = await HaravanHr.getColleague(id)
        if(result && result.data && !result.error){
            data= result.data
        }
        else{
            data = null
        }
        return data
    }

    checkCount(data){
        let count = 0;
        let list = data.options
        for(let i =0 ; i < list.length; i++){
            count = count + (list[i].votes.length)
        }
        this.setState({totalVote:count})
    }

    getColor(value){
        let color = '';
        switch (value) {
            case 0:
                color = '#0084FF'
                break;
            case 1:
                color = '#4CA9FF'
                break;
            case 2:
                color = '#66B5FF'
                break;
            case 3:
                color = '#80C1FF'
                break;
            default: color = '#B2DAFF'
                break;
        }
        return color
    }

    renderHeader(){
        return(
            <HeaderScreenNews
                navigation={this.props.navigation}
            />
        )
    }

    renderProgress(value){
        return(
            <View style={styles.coverPro}>
                <ProgressBar 
                    progress={value} color={'#30C894'} 
                />
            </View>
        )
    }

    renderItemUser(value, length, id){
        let userInfo = this.getUserInfo(id)
        let backgroundColor = this.getColor(value)
        let count = (length - 4)
        return(
            <View style={[styles.iconUser,{backgroundColor:backgroundColor}]}>
               {value == 4 && <Text style={styles.textUSer}>{`+${count}`}</Text>}
               {value != 4 && userInfo &&
                    <AvatarCustom userInfo={{ name:userInfo.fullName , picture: userInfo.photo }} v={25} fontSize={12} />
               }
            </View>
        )
    }

    renderItem(obj){
        const { item, index } = obj;
        let value = ((item.votes.length > 0) &&(this.state.totalVote > 0))? (item.votes.length / this.state.totalVote) : 0
        return(
            <ItemVotePoll
                item= {item}
                value={value}
                navigation= {this.props.navigation}
            />            
            // <TouchableOpacity 
            //     onPress={() =>{
            //         this.props.navigation.navigate('DetailUserVote', {data:item})
            //     }}
            //     style={styles.viewBar}>
            //     <Text style={styles.titleItem}>{item.name}</Text>
            //     {this.renderProgress(value)}
            //     <View style={styles.coverItem}>
            //         <Text style={styles.textWeigth}>{`${item.votes.length} bình chọn`}</Text>
            //         {item.votes[0] && this.renderItemUser(0, item.votes.length, item.votes[0])}
            //         {item.votes[1] && this.renderItemUser(1, item.votes.length, item.votes[1])}
            //         {item.votes[2] && this.renderItemUser(2, item.votes.length, item.votes[2])}
            //         {item.votes[3] && this.renderItemUser(3, item.votes.length, item.votes[3])}
            //         {item.votes[4] && this.renderItemUser(4, item.votes.length, item.votes[4])}
            //     </View>
            // </TouchableOpacity>
        )
    }

    renderText(){
        const { dataPoll, totalVote } = this.state;
        let endDate =(dataPoll.length> 0) && dataPoll.endDate && Utils.formatTime(dataPoll.endDate, 'DD/MM/YYYY', true)
        return(
            <View style={styles.coverEnd}>
                <Text style={styles.textInfo}>
                    <Text style={styles.textInfoDis}>Cuộc thăm dò ý kiến đã có </Text>
                    <Text style={styles.textWeigth}>{totalVote}</Text>
                    <Text style={styles.textInfoDis}> bình chọn </Text>
                    {endDate &&<Text style={styles.textInfoDis}>và kết thúc vào </Text>}
                    {endDate && <Text style={styles.textWeigth}>{endDate}</Text>}
                </Text>

                {this.meVote && ! dataPoll.isExpried ? <TouchableOpacity
                    onPress={() => {
                        this.actions()
                        this.props.navigation.pop()
                    }}
                    style={styles.buttonRemove}
                    >
                    <View style={styles.iconRemove}>
                        <SvgCP.iconReset/>
                    </View>
                    <Text style={styles.textRemove}>Bình chọn lại</Text>
                </TouchableOpacity>:
                <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.pop()
                    }}
                    style={styles.buttonRemove}
                    >
                    <View style={styles.iconRemove}>
                        <SvgCP.iconReset/>
                    </View>
                    <Text style={styles.textRemove}>Trở lại màn hình bình chọn</Text>
                </TouchableOpacity>
                }
            </View>
        )
    }

    render(){
        const { dataPoll, isLoading } = this.state
        return(
            <View style={{flex:1, backgroundColor:'#FFFFFF'}}>
                {this.renderHeader()}
                <Text style={styles.titleVote}>{this.data.title}</Text>
                {(dataPoll.options) && <FlatList 
                    data ={dataPoll.options}
                    extraData={this.state}
                    keyExtractor={(item, index) => ''+index}
                    renderItem={(obj) => this.renderItem(obj)}
                    ListFooterComponent={() => this.renderText()}
                />}
                {isLoading && <Loading/>}
            </View>
        )
    }

}

const styles = StyleSheet.create({
    coverPro:{
        width:settingApp.width-30, 
        height:5, 
        borderRadius:6, 
    },
    viewBar:{
        width:settingApp.width,
        minHeight:60,
        justifyContent:'center',
        padding:15,
        marginTop:10
    },
    titleItem:{
        fontSize:16,
        color:settingApp.colorText,
        paddingBottom:5,
        paddingTop:5
    },
    titleVote:{
        fontWeight:'bold',
        fontSize:18,
        color:settingApp.colorText,
        width:settingApp.width - 30,
        marginLeft:15
    },
    textWeigth:{
        fontSize:14, 
        fontWeight:'500',
        color:settingApp.colorText
    },
    coverItem:{
        width:settingApp.width -30, 
        minHeight:40, 
        flexDirection:'row', 
        marginTop:10, 
        alignItems:'center'
    },
    iconUser:{
        width:25, 
        height:25, 
        borderRadius:12.5, 
        marginLeft:5, 
        justifyContent:'center', 
        alignItems:'center'
    },
    textUSer:{
        fontSize:14,
        color:settingApp.blueSky
    },
    textInfo:{
        width:settingApp.width -30,
        paddingLeft:15,
        marginBottom:10
    },
    textInfoDis:{
        fontSize:14,
        color:settingApp.colorDisable
    },
    coverEnd:{
        width:settingApp.width,
        minHeight:40,
        justifyContent:'center',
        marginBottom:20
    },
    buttonRemove:{
        width:200,
        height:40,
        flexDirection:'row',
        justifyContent:'flex-start',
        alignItems:'center',
        marginLeft:5
    },
    textRemove:{
        color:settingApp.blueSky,
        fontSize:14,
        paddingLeft:5
    },
    iconRemove:{
        height:50,
        width:20,
        justifyContent:'center',
        alignItems:'center', 
        paddingLeft:5
    }
})
export default DetailVote;