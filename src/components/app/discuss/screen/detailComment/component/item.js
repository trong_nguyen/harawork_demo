import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { settingApp, SvgCP, AvatarCustom } from '../../../../../../public';
import moment from 'moment';
import ContentItem from './contenItem';
import { HaravanHr } from '../../../../../../Haravan';
import { Feather } from '@expo/vector-icons';
import {connect} from 'react-redux';
import actions from '../../../../../../state/action';
import { bindActionCreators } from 'redux';

class Item extends Component{
    constructor(props){
        super(props)
        this.state={
            listReplies:[],
            showReplies: false,
            userReplies: null,
            isLoadComment: false,
            loadMore: false,
            showMoreReplies: false,
            data: null
        }
        this.page = 1;
        this.currentList =[];
        this.totalPage = null;
    }

    async componentDidMount(){
        const { item, colleague } = this.props;
        let { data } = this.state;
        if(item){
            if(item.created_By.id == colleague.haraId){
                data = { ...item, created_By:{...item.created_By, photo: colleague.photo}}
            }
            else{
                let resultUser = await HaravanHr.getColleague(item.created_By.id)
                if(resultUser && resultUser.data && !resultUser.error){
                    data ={...item , created_By: { ...item.created_By, photo:resultUser.data.photo}}
                }
                else{
                    data = {...item , created_By: { ...item.created_By, photo:""}}
                }
            }
            this.setState({data})
        }

        if(item && item.replies){
            let data = item.replies.data[item.replies.data.length -1]
            let userReplies = null;
            if(colleague.haraId == (data.created_By.id)){
                userReplies = {name: colleague.fullName, photo: colleague.photo, id:colleague.haraId}
            }
            else{
                let resultUser = await HaravanHr.getColleague(data.created_By.id)
                if(resultUser && resultUser.data && !resultUser.error){
                    userReplies ={name:resultUser.data.fullName , photo: resultUser.data.photo, id:resultUser.data.haraId}
                }
                else{
                    userReplies ={name: '' , photo: "", id:''}
                }
            }
            this.setState({userReplies:userReplies})
        }
    }

    formatTime(time){
        let newtime = null
        let timeCheck = moment(time).fromNow();
        if(timeCheck.indexOf('trước') > -1){
            newtime=  timeCheck.replace('trước', '').trim()
        }
        else if(timeCheck.indexOf('ago') > -1){
            newtime= timeCheck.replace('ago', '').trim()
        }
        return newtime
    }

    getTime(time){
        let newtime = this.formatTime(time)
        let setTime= null
        if(newtime.indexOf('một') > -1){
            setTime=  newtime.replace('một', '1').trim()
        }
        else if(newtime.indexOf('one') > -1){
            setTime= newtime.replace('one', '1').trim()
        }
        else{
            setTime = newtime
        }
        return setTime
    }

    renderTypeActions(item){
        let  {like_users, dislike_users, praise_users} = item;
        let pading1 = ((praise_users.length > 0) && (dislike_users.length > 0)) ? 40 : (praise_users.length > 0 && dislike_users.length <1) ? 20 : 5;
        let pading2 = (dislike_users.length > 0) ? 30 : 10;
        return(
            <View style={{flexDirection:'row'}}>
                { (like_users.length > 0) ?
                    <View style={{ right:pading1, zIndex:90}}>
                        <SvgCP.isHeart/>
                    </View>
                    :
                    <View/>
                }
                { (praise_users.length > 0)  ? 
                    <View style={{ right:pading2, zIndex:80}}>
                        <SvgCP.isClap/>
                    </View>
                    : <View />
                }
                { (dislike_users.length > 0)  ? 
                    <View style={{ right:10, zIndex:70}}>
                        <SvgCP.isDisLike/>
                    </View>
                    :<View/>
                }
            </View>
        )
    }

    render(){
        const { item, index  } = this.props;
        const { data, userReplies } = this.state;
        let time = this.getTime(item.created_At)
        if(data){
            return(
                <TouchableOpacity 
                    onLongPress={() => this.props.editComment({editComment:true, item:item})}
                    style={{ width:(settingApp.width), borderTopWidth:((index > 0) ? 1 : 0), borderTopColor:((index > 0) ? settingApp.colorSperator : 'transparent'), paddingTop:10}}>
                    <ContentItem
                        data = {data} 
                        name= {data.created_By.name}
                        photo= { data.created_By.photo }
                        content = { data.content }
                        time = { time}
                        level = {1}
                        colleague = { this.props.colleague }
                        token ={this.props.token}
                        navigation ={this.props.navigation}
                        parentData ={null}
                        userReplies = { userReplies }
                        //autoFocus={() => this.props.autoFocus(true)}
                    />
                 </TouchableOpacity>
            )
        }
        else{
            return <View />
        }
            
    }
}
const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};
export default connect(mapStateToProps, mapDispatchToProps)(Item);