import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Animated, Modal, Alert, Platform } from 'react-native';
import * as Animatable from 'react-native-animatable';
import {  settingApp } from '../../../../../../public'

class ModalOption extends Component{
    constructor(props){
        super(props)
        const bottom = -(settingApp.height * 0.5 + 10);
        this.state={
            isOpen: false,
            bottom: new Animated.Value(bottom),
            item: null
        }
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.item){
            this.setState({item: nextProps.item})
        }
    }
    componentDidMount(){
        this.open()
    }

    open() {
        Animated.timing(
            this.state.bottom,
            {   
                toValue: 0,
                duration: 120
            }
        ).start();
    }

    close() {
        Animated.timing(
            this.state.bottom,
            {
                toValue: -(settingApp.height * 0.5 + 10),
                duration: 120
            }
        ).start(() => {
            this.props.close()
        });
    }

    checkType(type){
        if(type== 9 ){
            Alert.alert(
                'Thông báo',
                `Bạn có chắc chắn muốn rời khỏi nhóm này và không còn nhận được các thông báo về chủ đề trong nhóm nữa?`,
                [
                    { text: 'Hủy', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                    { text: 'Xác nhận', onPress: () =>{ 
                        this.props.checkOption(type)
                        this.props.close(type)    
                    }},
                ],
                { cancelable: false }
            )
        }
        else{
            this.props.checkOption(type)
            this.props.close()
        }
    }

    renderButton(title, type){
        return(
            <TouchableOpacity
                activeOpacity={0.8}    
                onPress={() => {
                    this.checkType(type)
                }}
                style={[Platform.OS == 'ios'? styles.lineIos : styles.lineAndroid, {
                        borderTopWidth: (type == 3 || type== 4) ? 1 : 0 ,
                        borderTopColor:settingApp.colorSperator
                    }]} 
            >
            <Text style={{fontSize:17, color:(type == 3) ? '#FF3B30' : settingApp.blueSky}}>{title}</Text>
        </TouchableOpacity>
        )
    }

    renderLine(item){
        const { collegue } = this.props
        let createBy =  collegue.haraId === item.createdBy
        return(
            <View                 
                style={[styles.viewLine, {
                    width: Platform.OS=='ios' ? (settingApp.width*0.95) : (settingApp.width),
                    borderRadius: Platform.OS=='ios' ? 10 : 0,
                    } ]}
                >
                    {createBy && this.renderButton('Chỉnh sửa chủ đề', 1)}
                    {/* {this.renderButton('Ghim chủ đề lên đầu nhóm', 2)} */}
                    {this.renderButton('Cấu hình ra trang bảng tin', 4)}
                    {((createBy || (item.isAdminForum ==true))) 
                        && this.renderButton('Xóa chủ đề', 3)}
               </View>
        )
    }

    renderCancel(){
        return(
            <TouchableOpacity 
                activeOpacity={0.8}
                onPress={() => {
                    this.Animatable.props.animation="fadeInDownBig"
                    this.props.close()
                    }}
                style={[Platform.OS =='ios' ? styles.buttonIos : styles.buttonAndroid,{
                    borderRadius:Platform.OS =='ios' ? 10 : 0, 
                }]}
                 >
                <Text  style={{fontSize:17, color:settingApp.blueSky, fontWeight:'600'}}>Hủy</Text>
            </TouchableOpacity>
        )
    }

    render(){
        const { height } = settingApp;
        let { item } = this.state
        return(
            <Modal
                ref={refs => this.Modal = refs}
                animationType="fade"
                transparent={true}
                visible={this.props.visible}
                onRequestClose={() => this.props.close()}>
            
            <TouchableOpacity
                onPress={() => this.props.close()}
                activeOpacity={1}
                style={{
                    flex: 1,
                    backgroundColor: 'rgba(0,0,0,0.5)',
                }}
            />
            <View style={{flex:1, backgroundColor:'rgba(0,0,0,0.5)', justifyContent:'flex-end'}}>
                <Animatable.View
                    ref={refs => this.Animatable = refs}
                    animation="fadeInUpBig"
                    delay={120}
                    duration={120}
                    style={{
                        width:settingApp.width,
                        height: (height*0.5),
                        backgroundColor: 'transparent',
                        justifyContent:'flex-end',
                        alignItems:'center',
                        ...settingApp.shadow
                    }}
                >
                   {item && this.renderLine(item)}
                   {item && this.renderCancel()}
                </Animatable.View>
            </View>
        </Modal>
        )
    }
}
const styles ={
    lineAndroid:{
        width:(settingApp.width),  
        backgroundColor:'#FFFFFF', 
        height:54,
        ...settingApp.shadow, 
        justifyContent:'center', 
        alignItems:'center'
    },
    buttonAndroid:{
        height:54,
        width:(settingApp.width), 
        justifyContent:'center', 
        alignItems:'center',
        borderRadius:10,
        backgroundColor:'#FFFFFF', 
        marginTop:10
    },
    lineIos:{
        width:(settingApp.width*0.95),  
        height:54,
        borderRadius:10,
        backgroundColor:'#FFFFFF', 
        justifyContent:'center', 
        alignItems:'center'
    },
    buttonIos:{
        height:54,
        width:(settingApp.width*0.95), 
        justifyContent:'center', 
        alignItems:'center',
        borderRadius:10,
        marginTop:10,
        marginBottom: 10,
        backgroundColor:"#FFFFFF"
    },
    viewLine:{
        width:(settingApp.width*0.95),  
        backgroundColor:'#FFFFFF', 
        ...settingApp.shadow, 
        justifyContent:'center', 
        alignItems:'center'
    }
}
export default ModalOption;