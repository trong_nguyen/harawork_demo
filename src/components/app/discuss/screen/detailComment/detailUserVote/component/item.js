import React, { Component } from 'react';
import { View, Text, StyleSheet, ActivityIndicator, TouchableOpacity } from 'react-native';
import { HaravanHr } from '../../../../../../../Haravan';
import { settingApp, AvatarCustom, ModalUserInfo } from '../../../../../../../public';

class Item extends Component{
    constructor(props){
        super(props)
        this.state={
            data:null,
            isLoading:true,
            visibleModal:false
        }
    }

    componentDidMount(){
        this.loadData()
    }

    async loadData(){
        const { item } = this.props;
        const result = await HaravanHr.getColleague(item)
        if(result && result.data && !result.error){
            this.setState({data:result.data, isLoading:false})
        }
        else{
            this.setState({isLoading: false})
        }
    }

    render(){
        const { isLoading, data } = this.state
        return(
            <View style={styles.main}>
                {isLoading && this.loading()}
                {data && <TouchableOpacity 
                    onPress={() => this.setState({visibleModal:true})}
                    style={styles.coverLine}>
                    <AvatarCustom userInfo={{ name:data.fullName, picture: data.photo? data.photo : ""}} v={30}  fontSize={12}/>
                    <Text style={styles.nameUser}>{data.fullName}</Text>
                </TouchableOpacity>}

                {this.state.visibleModal && <ModalUserInfo
                    visibleModal={this.state.visibleModal}
                    onClose={() => this.setState({ visibleModal: false })}
                    item={data}
                    imageUser={data.photo}
                />}
            </View>
        )
    }

    loading(){
        return(
            <ActivityIndicator color={settingApp.colorDisable} size={'small'} style={{alignItems:'flex-start'}} />
        )
    }
}

const styles = StyleSheet.create({
    main:{
        minHeight:50,
        justifyContent:'center',
        paddingLeft:15
    },
    coverLine:{
        flexDirection:'row',
        alignItems:'center',
        minHeight:50,
        width:(settingApp.width -30)
    },
    nameUser:{
        fontSize:16,
        color:settingApp.colorText,
        marginLeft:5
    }
})
export default Item;