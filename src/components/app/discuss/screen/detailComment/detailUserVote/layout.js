import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, FlatList } from 'react-native';
import { HeaderScreenNews, settingApp } from '../../../../../../public';
import Item from './component/item';

class Layout extends Component{
    renderHeader(){
        return(
            <HeaderScreenNews 
                title ='Kết quả bình chọn'
                navigation = {this.props.navigation}
            />
        )
    }

    renderHeaderList(){
        const { data } = this;
        return(
            <View style={styles.coverTop}>
                <View style={styles.coverHeaderList}>
                    <Text style={styles.textHeader}>{data.name}</Text>
                </View>
                <View style={styles.coverTotalVote}>
                    <Text>
                        <Text style={styles.textDisable}>Có </Text>
                        <Text sty={styles.textWeigth}>{data.votes.length}</Text>
                        <Text style={styles.textDisable}> bình chọn</Text>
                    </Text>
                </View>
            </View>
        )
    }

    renderItem(obj){
        const { item, index } = obj;
        return(
            <Item
                item={item}
            />
        )
    }

    render(){
        const { votes } = this.data
        return(
            <View style={{flex:1, backgroundColor:'#FFFFFF'}}>
                {this.renderHeader()}
                <FlatList 
                    data= {votes}
                    extraData={this.state}
                    keyExtractor={(item, index) => ''+index}
                    renderItem={(obj) => this.renderItem(obj)}
                    ListHeaderComponent={() => this.renderHeaderList()}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    coverTop:{
        width:settingApp.width,
        minHeight:100, 
    },
    coverHeaderList:{
        width:settingApp.width,
        minHeight:60, 
        justifyContent:'center',
        backgroundColor:'#FAFAFA',
        paddingLeft:15
    },
    coverTotalVote:{
        width:settingApp.width,
        minHeight:40, 
        justifyContent:'center',
        backgroundColor:'#FFFFFF',
        paddingLeft:15
    },
    textHeader:{
        fontSize:16, 
        fontWeight:'600',
        color:settingApp.colorText
    },
    textWeigth:{
        fontWeight:'bold',
        color:settingApp.colorText,
        fontSize:16,
    },
    textDisable:{
        color:settingApp.colorDisable,
        fontSize:16,
    }
})
export default Layout;