import React, { Component } from 'react';
import Layout from './layout';
import { HaravanGraphQL, HaravanIc, HaravanHr } from '../../../../../Haravan';
import {connect} from 'react-redux';
import actions from '../../../../../state/action';
import { bindActionCreators } from 'redux';
import { settingApp, SvgCP, Toast } from '../../../../../public';
import Configs from '../../../../../Haravan/config';
import { Alert } from 'react-native';
import * as Utils from '../../components/Utils'

class DetailComment extends Layout{
    constructor(props){
        super(props)
        this.state = {
            isLoading:true,
            data: null,
            listComment:[],
            isLoadComment: true,
            totalCount: 0,
            getComment: false,
            listAction:[],
            showOption:false,
            loadMore:false,
            value: null,
            listAttach:[],
            modalVisibleAttachment:false,
            file: [],
            isLoadIMG:false,
            isPosting: false,
            autoFocus: false,
            content: null,
            clear:false,
            visibleOption:false,
            imgUrl:null,
            isStarred:false,
            isCreate:false,
            disabled:true,
            showNews:false,
            showImage:false,
            editComment:false,
            itemEdit:null
        }
        this.isMount = true

        _this = this
        this.timeLet = new Date().getTime();
        this.colleague = props.app.colleague;
        this.data = props.navigation.state.params.data;
        this.actions = props.navigation.state.params.actions;
        this.delete = props.navigation.state.params.delete;
        this.page = 1;
        this.reply_page = 1;
        this.currentList = [];
        this.totalPage = null;
        this.body = null;
    }

    async componentWillReceiveProps(nextProps){
        if(nextProps && nextProps.discuss && nextProps.discuss.changeImage){
            let {url, data} = nextProps.discuss.changeImage
            if(data && (data.id == this.state.data.id)){
                this.setState({imgUrl:url})
            }
        }

        if(nextProps && nextProps.discuss && nextProps.discuss.newComment){
            let {newComment} = nextProps.discuss;
            let { listComment  } = this.state
            if(newComment.id && (newComment.id === this.data.id) && (listComment.length > 0)){
            let params = { 
                id: this.data.id, 
                preview:10,  
                page:1, 
                limit:10, 
                reply_review:10, 
                reply_page:1, 
                reply_limit:10
            }
                const resultComment = await HaravanGraphQL.getComment(params);
                if(resultComment && resultComment.data && resultComment.data.comments ){
                   const {data} = resultComment.data.comments
                    for(let i =0 ; i < data.length; i++){
                        const index = listComment.findIndex(m => m.id === data[i].id)
                        if(index < 0){
                            listComment.push(data[i])
                            break ;
                        }
                    }
                }
                this.setState({showNews:true},() =>{
                    setTimeout(() => {
                        this.setState({showNews:false},() =>{
                            this.props.actions.discuss.newComment(null)
                        })
                    }, 4000);
                })
            }
        }

        if(nextProps && nextProps.app && nextProps.app.reloadNotify && (nextProps.app.reloadNotify==true) && 
        ( (nextProps && nextProps.discuss && this.data &&  (nextProps.discuss.newComment.id !== this.data.id) ))
        ){
            this.data = nextProps.navigation.state.params.data;
            this.page = 1;
            this.reply_page = 1;
            this.currentList = [];
            this.totalPage = null;
            this.setState({isLoading: true}, () =>{
                this.loadData() 
                this.loadBanner()
                //this.checkImage(this.data)
                this.loadComment()
                this.props.actions.reloadNotify(null)
            })
        }
    }

    getNewCommnet(){
        this.setState({
            showNews:false
        },() =>{
            this.ScrollView.scrollToEnd({animated: true});
        })
    }
    
    async componentDidMount(){
        this.body = await Utils.GetBody(this.colleague)
        this.loadData()
        this.loadBanner()
        // this.checkImage(this.data)
        this.loadComment()
        
    }

    async checkOptionEdit(type){
        let { itemEdit, data, listComment } = this.state;
        if(type == 1){
            let params = {
                token:data.id,
                id:itemEdit.id
            }
            let result =await HaravanGraphQL.deleteComment(params);
            if(result && result.data && result.data.deleteComment){
                let index = listComment.findIndex(m => m.id === itemEdit.id)
                if(index > -1){
                    listComment.splice(index, 1)
                    this.setState({listComment})
                }
            }
            else{
                Toast.show('Có lỗi hệ thống, vui lòng thử lại sau')
            }
        }
    }

    checkDisable(value){
        let textInput = '';
        if(value){
            if( value[0].content.length > 1){
                for(let i = 0; i < value[0].content.length; i++){
                    if(i < (value[0].content.length -1)){
                        textInput = textInput + value[0].content[i].text.trim() + '</br>'
                    }
                    else{
                        textInput = textInput + value[0].content[i].text.trim()
                    }
                }
            }
            else{
                textInput = textInput + value[0].content[0].text.trim()
            }
        }
        else{
            this.setState({disabled:true})
        }

        if((this.state.file.length > 0)|| (textInput && textInput.length > 0)){
            this.setState({disabled:false})
        }
        else{
            this.setState({disabled:true})
        }
    }

    checkAtions(value){
        if(value == true){
            this.loadData()
        }
    }

    async isStarred(){
        const { isStarred } = this.state
        let body={
            isStarred:isStarred
        }
        let mess = isStarred ? 'Lưu' : 'Đã bỏ lưu'
        const result = await HaravanIc.isStarredThread(this.state.data.id, isStarred, body)
        if(result && !result.error){
            //Toast.show(`${mess} chủ đề thành công`)
        }
        else{
            Alert.alert(
                'Thông báo',
                `${mess} chủ đề thất bại. \nLý do: Lỗi hệ thống \nĐề xuất: Thử lại lần nữa`,
                [
                    { text: 'Đồng ý', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: true }
            )
        }
    }

    async deleteThread(){
        const { data } = this.state
        const result = await HaravanIc.DeleteThread(data.id)
        if(result && !result.error){
            this.delete && this.delete({status:true, data:data})
            this.props.navigation.pop()
        }
        else{
            Alert.alert(
                'Thông báo',
                `Xóa chủ đề thất bại. \nLý do: Lỗi hệ thống \nĐề xuất: Thử lại lần nữa`,
                [
                    { text: 'Đồng ý', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: true }
            )
        }
    }

    checkPin(value){
        if(value === true){
            //Toast.show('Cấu hình ra trang bảng tin thành công')
        }
    }

    checkOptionThread(value){
        switch (value) {
            case 1:
                let data = {...this.state.data, banner:this.state.imgUrl}
                this.props.navigation.navigate('CreateThread', { 
                    data:data,
                    actions:value => this.checkAtions(value)
                })
                break;
            
            case 3:
                this.deleteThread()
                break;
            case 4:
                let body = {...this.state.data, banner:this.state.imgUrl}
                this.props.navigation.navigate('SettupToHome', { 
                    data:body,
                    actions:value => this.checkPin(value)
                })
                break;
            default: null
                break;
        }
    }

    checkImage(data){
        if(data && data.banner){
            this.setState({imgUrl:data.banner})
        }
        else{
            this.setState({imgUrl:null})
        }
    }

    setHeight(height){
        let { file } = this.state;
        if(file && file.length > 0){
            this.setState({height:(height + 100)})
        }
        else{
            this.setState({height:height})
        }
    }
    getSource(){
        const { file } = this.state;
        let uri = null
        if(file && (file.length >0)){
            file.map(item =>{
                uri = item.uri
            })
        }
        return uri
    }

    setContent(value){
        let textInput = '';
        if(value){
            if( value[0].content.length > 1){
                for(let i = 0; i < value[0].content.length; i++){
                    if(i < (value[0].content.length - 1)){
                        textInput = textInput + value[0].content[i].text.trim() + '</br>'
                    }
                    else{
                        textInput = textInput + value[0].content[i].text.trim()
                    }
                }
            }
            else{
                textInput = textInput + value[0].content[0].text.trim()
            }
        }
        let newValue =  textInput.replace(/["/\\]/g, '\\$&')
        this.setState({value:`${textInput}`,  clear:true },() => this.checkComment(newValue))
    }

    getContent(){
        this.CNText.setContent()
        //this.TextComment.setContent()
    }

    async getUrl(){
        const { file } = this.state;
        let listUrl = []
        let input = new FormData();
        if(file && file.length > 0 ){
            for (let i = 0; i < file.length; i++) {
                input.append("files",{
                    uri: file[i].uri,
                    type: `image/${file[i].fileName.substr(file[i].fileName.lastIndexOf('.') + 1)}`,
                    name: file[i].fileName,
                });
            }
            const response = await HaravanIc.postImage(input)
            if(response){
                response.map(e =>{
                    listUrl.push(e)
                })
                this.setState({isLoadIMG: false})
            }
            else{
                this.setState({isLoadIMG: false})
            }
        }
        else{
            listUrl = []
        }
        
        return listUrl
    }

    getHTML(html){
        this.setState({value:html})
    }

    async checkComment(textInput){
        this.ScrollView.scrollToEnd({animated: true});
        const { file } = this.state;
        let getUrl = await this.getUrl()
        
        let attachments = null
        if(file && (file.length > 0) && getUrl){
            for (let i = 0; i < getUrl.length; i++) {
                attachments = {name:getUrl[i].name, url:getUrl[i].file.url, size: getUrl[i].file.size}
            }
            this.addCommentNew(attachments, textInput)
        }
        else{
            this.addCommentNew(attachments, textInput)
        }
    }

    async addCommentNew(attachments, textInput){
        const {  listComment, file , value} = this.state;
        let content = `${textInput}`
        let body = {
            token: this.data.id,
            ref_Url: `https://ic.${Configs.apiHost}/threads/detail/${this.data.id}`, 
            content: content, 
            moduleName: "IC", 
            tagged_Ids: `tagged_Ids:[]`, 
            attachments: (file && file.length > 0) ? attachments: null
        }
        let result = await HaravanGraphQL.addCommentNews(body)
        if(result && result.data ){
            const aestTime = new Date().getTime();
            let itemNews = {
                action_count: 0,
                action_users: [],
                attachments: (file && file.length > 0) ? [attachments] : [],
                content: `${value}`,
                like_count: 0,
                like_users: [],
                praise_count: 0,
                praise_users: [],
                dislike_count: 0,
                dislike_users: [],
                replies:{data: [],totalCount: 0},
                id: result.data.createComment.id,
                created_By: {id: this.colleague.haraId, name: this.colleague.fullName, avartar: this.colleague.photo},
                created_At: aestTime,
                totalCount:0,
                isRead:false
            }
            listComment.push(itemNews)
            let body ={
                sendToUser: this.colleague.haraId,
                type: 1,
                userInfo: this.colleague
            }
            this.setState({ listComment, totalCount: (this.state.totalCount +1), clear:true },async () =>{
                this.ScrollView.scrollToEnd({animated: true});
                let result = await HaravanIc.updatrTheard(this.data.id, body)
                if(result){
                    this.setState({ file:[], isPosting:false, clear:false })
                }
                
            })
        }
    }

    async checkNoneLongPress(){
        const { listAction } = this.state;
        const { colleague } = this;
        let type = null
        let action = false
        let item = { createdBy:colleague.haraId, type:1 }
        if(listAction.length == 0 ){
            type = 1
            listAction.push(item)
        }
        else{
            const index =  listAction.findIndex(m => m.createdBy == colleague.haraId)
            if(index > -1){
                type = null
                action = true
                listAction.splice(index, 1)
            }
            else{
                type = 1
                listAction.push(item)
            }
        }
        let data ={
            id: this.data.id,
            type: type,
            action:action
        }
        await HaravanIc.PutActions(data)
        this.setState({listAction}, () =>{
            this.checkAction(listAction)
        })
    }

    async checkOption(value){
        const { colleague } = this
        const { listAction } = this.state;
        let type = null;
        let action = false;
        let item = { createdBy:colleague.haraId, type:value}
        if(listAction.length > 0 ){
            const index = listAction.findIndex(m => m.createdBy == colleague.haraId)
            if(index > -1){
                if(value == listAction[index].type){
                    type = null
                    action = true
                    listAction.splice(index, 1)
                }
                else {
                    type = value
                    action = false
                    listAction[index] = { ...listAction[index], type:value}
                }
            }
            else{
                type = value
                action = false
                listAction.push(item)
            }
        }
        else{
            type = value
            action = false
            listAction.push(item)
        }
        let data ={
            id: this.data.id,
            type: type,
            action: action
        }
        await HaravanIc.PutActions(data)
        this.setState({listAction, showOption:false}, () =>{
            this.Popover.props.onClose()
            this.checkAction(listAction)
        })
    }

    checkAction(list){
        let checkAction = null;
        const { colleague } = this;
        if(list && list.length > 0){
            const index = list.findIndex(m => m.createdBy == colleague.haraId)
            if(index > -1){
                if(list[index].type == 1){
                    checkAction = { title:'Thích', color:(settingApp.color), image: <SvgCP.isHeart />, type:list[index].type}
                }
                else if(list[index].type == 2){
                    checkAction = { title:'Không thích', color:(settingApp.color), image: <SvgCP.isDisLike />,type:list[index].type}
                }
                else{
                    checkAction = { title:'Tán dương', color:(settingApp.color), image: <SvgCP.isClap />, type:list[index].type}
                }
            }
            else{
                checkAction = {title:'Thích', color:(settingApp.colorDisable), image: <SvgCP.noneHeart />, type:-1}
            }
        }
        else{
            checkAction = {title:'Thích', color:(settingApp.colorDisable), image: <SvgCP.noneHeart />, type:-1}
        }
        return checkAction
    }

async loadData(){
        const { colleague, body} = this
        const result = await HaravanIc.threadDetail(this.data.id, body);
        if(result && result.data && !result.error){
            let item = {...result.data}
            if(colleague.haraId == result.data.createdBy){
                item ={...item, imageInfo:colleague.photo, fullName:colleague.fullName}
                this.setState({isCreate:true})
            }
            else{
                let resultUser = await HaravanHr.getColleague(result.data.createdBy)
                if(resultUser && resultUser.data && !resultUser.error){
                    item ={...item, imageInfo: resultUser.data.photo, fullName:resultUser.data.fullName}
                }
                else{
                    item ={...item, imageInfo: ""}
                }
                this.setState({isCreate:false})
            }
            this.setState({ data:item, isLoading:false, listAction:item.action, isRead:true, isStarred:item.isStarred})
        }
        else{
            this.setState({isLoading:false})
        }
    }
    
    reLoadComment(){
        this.loadComment()
    }

    async loadComment(){
        let { currentList, totalPage, page, colleague, reply_page } = this
        let params = { 
            id: this.data.id, 
            preview:10,  
            page:this.page, 
            limit:10, 
            reply_review:10, 
            reply_page:reply_page, 
            reply_limit:10
        }
        let listMore = []
        let newList = currentList
        const resultComment = await HaravanGraphQL.getComment(params);
        if (!totalPage || (totalPage && !isNaN(totalPage) && page <= totalPage)) {
            if(resultComment && resultComment.data && resultComment.data.comments ){
                const totalCount = resultComment.data.comments.totalCount
                resultComment.data.comments.data.map(async (item) =>{
                    if(page > 1){
                        listMore.push(item)
                    }
                    else{
                        newList.push(item)
                    }
                })
                newList = [...listMore, ...newList]
                this.totalPage = Math.ceil(totalCount / 10);
                this.currentList =  newList
                this.page = page + 1;
                this.setState({ listComment:newList, isLoadComment:false, totalCount:totalCount, getComment:false,loadMore:false })
            }
            else{
                this.setState({listComment:[], isLoadComment:false, loadMore:false})
            }
        }
        else{
            this.setState({ isLoadComment:false, loadMore:false})
        }
    }
    async loadBanner(){
        const {body} = this
        const result = await HaravanIc.getThreadDetail(this.data.id, body);
        if(result && result.data && !result.error){
            if(result.data.banner){
                this.setState({
                    imgUrl: result.data.banner
                })
            }
            else{
                this.setState({imgUrl:null})
            }
        }
        else{
            this.setState({imgUrl:null})
        }
    }

    componentWillUnmount(){
        this.isMount = false
        this.setState({autoFocus:false})
        this.props.actions.discuss.newComment(null)
        // this.props.actions.discuss.checkAtions(null)
    }
}
const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};
export default connect(mapStateToProps, mapDispatchToProps)(DetailComment);