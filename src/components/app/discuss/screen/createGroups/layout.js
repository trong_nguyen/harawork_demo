import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, TextInput } from 'react-native';
import { HeaderScreenNews, settingApp, iconGroups } from '../../../../../public'
import {MaterialIcons, AntDesign} from '@expo/vector-icons';
import * as UtilsGroup from '../../group/components/Utils'

class Layout extends Component{

    renderHeader(){
        const { titleHeader } = this.state
        return(
            <HeaderScreenNews 
                buttonLeft={
                    <TouchableOpacity
                        onPress={() => this.props.navigation.pop()}
                        style={{
                            width:50, height:44, justifyContent:'center', alignItems:'center'
                        }}
                    >
                        <MaterialIcons  name='close' size={25} color={settingApp.colorDisable} />
                    </TouchableOpacity>
                }
                title ={titleHeader}
                buttonRight = {
                    <TouchableOpacity
                        disabled={this.state.disabled}
                        onPress={() => {
                            this.data ? this.editGroups() : this.createGroup()}}
                        style={{
                            width:50, height:44, justifyContent:'center', alignItems:'center', opacity:(this.state.disabled ? 0.3 :1)
                        }}
                    >
                        <Text style={{fontSize:17, color:settingApp.blueSky}}>Lưu</Text>
                    </TouchableOpacity>
                }
            />
        )
    }

    renderInputName(){
        let { nameGroups } = this.state;
        return(
            <View style={{ minHeight: 80, justifyContent:'center'}}>
                <Text style={styles.textTitle}>Tên nhóm <Text style={styles.require}>*</Text> </Text>
                <TextInput
                    style={{
                        height: 40,
                        paddingTop:5,
                        }}
                    placeholder='Nhập tên nhóm'
                    onChangeText={(nameGroups) => this.setState({nameGroups},()=>{
                        this.checkDisable()
                    })}
                    value={this.state.nameGroups}
                />
            </View>
        )
    }

    renderLine(title, place, type){
        let placeholder =  this.checkPlaceholder(place, type)
        return(
            <TouchableOpacity 
                onPress={() => this.goPage(type)}
                style={{ 
                    minHeight: 80,
                    borderTopColor:settingApp.colorSperator,
                    borderTopWidth:1,
                    flexDirection:'row',
                    alignItems:'center',
                    paddingTop:10,
                    paddingBottom:10
                }}>
                <View style={{width:(settingApp.width*0.8), justifyContent:'center'}}>
                    <Text style={styles.textTitle}>{title}<Text style={styles.require}>*</Text> </Text>
                    <Text style={styles.textPlace}>{placeholder}</Text>
                </View>
                <View style={{width:50, height:80, justifyContent:'center', alignItems:'center'}}>
                    <AntDesign  name='right' size={20} color={settingApp.colorDisable} />
                </View>
            </TouchableOpacity>
        )
    }

    renderAvatar(title, placeholder){
        let { nameAvatar } = this.state
        let avatar = UtilsGroup.checkName(nameAvatar,50)
        return(
            <TouchableOpacity 
            onPress={() => this.props.navigation.navigate('ChoiceAvatar',{
                nameAvatar:this.state.nameAvatar,
                actions:(value) => this.setState({nameAvatar:value})
            })}
            style={{ 
                    height: 100,
                    borderTopColor:settingApp.colorSperator,
                    borderTopWidth:1,
                    flexDirection:'row',
                    alignItems:'center'
                }}>
                <View style={{width:(settingApp.width*0.8), justifyContent:'center'}}>
                    <Text style={[styles.textTitle,{paddingTop:5}]}>{title}</Text>
                    <View style={{ height:52,  alignItems:'center', flexDirection:'row'}}>{avatar}<Text 
                            style={{color:(nameAvatar !== 'none') ? settingApp.blueSky :settingApp.colorDisable, 
                            fontSize:16,paddingLeft:10}}
                            >{(nameAvatar !== 'none') ?'Thay đổi ảnh đại diện' : placeholder}</Text>
                        </View>
                </View>
                <View style={{width:50, height:80, justifyContent:'center', alignItems:'center'}}>
                    <AntDesign  name='right' size={20} color={settingApp.colorDisable} />
                </View>
            </TouchableOpacity>
        )
    }

    renderListEmploy(title, place, type){
        let placeholder = this.checkPlaceholder(place, type)
        let {listEmploy} = this.state;
        return(
            <TouchableOpacity 
                onPress={() => this.goPage(type)}
                style={{ 
                    minHeight: 80,
                    borderTopColor:settingApp.colorSperator,
                    borderTopWidth:1,
                    flexDirection:'row',
                    alignItems:'center',
                    paddingBottom:10,
                    paddingTop:10
                }}>
                <View style={{width:(settingApp.width*0.8), justifyContent:'center', flexWrap:'wrap', padding:5}}>
                    <Text style={styles.textTitle}>{title}{(type !=3) && <Text style={styles.require}>*</Text>} </Text>
                        {(listEmploy.length > 0 ) ? listEmploy.map((item, index) =>{
                            return(
                                <Text 
                                    key={index}
                                    style={{fontSize:14, color:settingApp.blueSky}}
                                    >{item.typeIC == 4 ? item.fullName : item.subName ? item.subName : item.fullName}{(index +1) ? ', ' : ''}
                                    </Text>
                                )
                            })
                            :
                        <Text style={styles.textPlace}>{placeholder}</Text>
                        }
                </View>
                <View style={{width:50, height:80, justifyContent:'center', alignItems:'center'}}>
                    <AntDesign  name='right' size={20} color={settingApp.colorDisable} />
                </View>
            </TouchableOpacity>
        )
    }
   
    render(){
        const { data } = this
        return(
            <View style={{flex:1, backgroundColor:'#FFFFFF'}}>
                {this.renderHeader()}
                <ScrollView style={{padding:15}}>
                    {this.renderInputName()}
                    {this.renderLine('Loại ', 'Chọn loại', 1)}
                    {this.renderAvatar('Ảnh đại diện', 'Chọn ảnh đại diện')}
                    {this.renderLine('Mô tả ', 'Thêm mô tả', 2)}
                    {!data && this.renderListEmploy('Thành viên', 'Thêm thành viên', 3)}
                    {this.renderLine('Quyền riêng tư ', 'Chọn quyền', 4)}
                    {this.renderLine('Quyền tạo chủ đề ', 'Chọn quyền', 5)}
                    <View style={{flex:1, height:50, backgroundColor:'#FFFFFF'}} />
                </ScrollView>
            </View>
        )
    }
}

const styles={
    require:{
        color:'#FF3B30', 
        fontSize:14,
        paddingLeft:5
    },
    textTitle:{
        color:settingApp.colorText, 
        fontSize:16,
        fontWeight:'600',
        paddingBottom:10,
    },
    textPlace:{
        color:settingApp.colorDisable, 
        fontSize:16
    }
}

export default Layout;  