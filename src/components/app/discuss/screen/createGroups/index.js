import Layout from './layout'
import * as UtilsGroup from '../../group/components/Utils';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../../state/action';
import { HaravanIc, HaravanHr } from '../../../../../Haravan';
import { View, Text, Alert, Platform} from 'react-native';
import { Toast } from '../../../../../public';

class CreateGroup extends Layout{
        constructor(props){
            super(props)
            this.state={
                nameGroups:'',
                nameAvatar:'none',
                typeGroup: 0,
                typePermissGroup: 0,
                typeThread: 0,
                content:'',
                listEmploy:[],
                disabled: true,
                isLoading:false,
                titleHeader:'',
                adminIC: false
            }
            this.collegue = props.app.colleague;
            this.userInfo =  props.app.authHaravan.userInfo
            this.data = props.navigation.state.params.data;
            this.setValue = props.navigation.state.params.setValue
    }

    componentDidMount(){
        let { listEmploy } = this.state
        if(this.data){
           this.setDefaulData(this.data)
        }
        else{
            let item = {...this.collegue,
                fullName:'Tôi',
                typeIC:4,
                subName: (this.collegue.mainPosition.departmentName + '-' + this.collegue.mainPosition.jobtitleName)
            }
            listEmploy.push(item)
            this.setState({
                titleHeader:'Tạo nhóm mới',
                listEmploy
            },() => {
            })
        }
       this.checkAdmin()
    }

    componentWillReceiveProps(nextProps){
        let { listEmploy } = this.state
        if(nextProps && nextProps.publicActions && nextProps.publicActions.removeItemUser ){
            let removeItemUser = nextProps.publicActions.removeItemUser;
            let index = listEmploy.findIndex(m => ((m.typeIC == removeItemUser.typeIC) && (m.id == removeItemUser.haraId)))
            if(index > 0){
                listEmploy.splice(index, 1)
                this.setState({listEmploy},() =>{
                    this.props.actions.publicActions.removeItemUser(null)
                })
            }
        }
    }

    async checkAdmin(){
        const result = await HaravanHr.checkPermissionUser();
        if (result && result.data && !result.error) {
           if (result.data.length && result.data.length > 0) {
               this.setState({ adminIC: true });
           }
           else{
               this.setState({ isManager: false });
           }
       }
    }

    setDefaulData(data){
            this.setState({
                titleHeader:'Chỉnh sửa thông tin',
                nameGroups:data.name,
                nameAvatar:data.avatar,
                typeGroup:data.contentType,
                typeThread:data.createThreadType,
                typePermissGroup:data.type,
                content:data.description,
                disabled:false,
            })
    }

    showAlert(mess){
        Alert.alert(
            'Thông báo',
            `${mess}`,
            [
                { text: 'Xác nhận', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: true }
        )
    }

    checkList(){
        const { listEmploy } = this.state;
        const list = []
        listEmploy.map(item => {
                let itemMail = {name:item.fullName, type:item.typeIC, }
                switch (item.typeIC) {
                    case 1:
                        itemMail = {...itemMail, icon:"department",  id:item.id, count:item.count, }
                        break;
                    case 2:
                        itemMail = {...itemMail, icon: "job", parentId:item.departmentId, id:item.id, content:item.subName, count:item.count}
                        break;
                    case 4:
                        itemMail = {...itemMail, icon: "user", id:item.haraId }
                        break;
                    default:itemMail = {...itemMail, icon:"company", content: "Tất cả nhân viên", id:item.orgid}
                        break;
                }
                list.push(itemMail)
            
        })
        return list
    }

    async editGroups(){
        const { data } = this
        let { nameAvatar, nameGroups,typeGroup, typePermissGroup, typeThread, listEmploy, content } = this.state
        let timeUpdate = new Date().getTime()
        let body={
            avatar: nameAvatar,
            canLeftGroup: data.canLeftGroup,
            contentType: typeGroup,
            createThreadType: typeThread,
            description: content,
            name: nameGroups,
            type: typePermissGroup,
            updatedAt: timeUpdate,
        }
        const result = await HaravanIc.EditGroups(data.id, body)
        if(result && !result.error){
            this.setValue(true, data.id)
            this.props.navigation.pop()
        }
        else{
            let mess = 'Lưu chỉnh sửa thất bại. Lý do: lỗi hệ thống. \n Ðề xuất: Hãy thử Lưu lại lần nữa.'
            this.showAlert(mess)
        }
    }

    async createGroup(){
        let { nameAvatar, nameGroups,typeGroup, typePermissGroup, typeThread, listEmploy, content } = this.state
        let list = this.checkList()
        let body ={
            avatar:nameAvatar,
            contentType:typeGroup,
            createThreadType:typeThread,
            description:content,
            members:list,
            name:nameGroups,
            type:typePermissGroup,
            userInfo:this.collegue
        }
        let result = await HaravanIc.CreateGroup(body)
        if(result && result.data && !result.error){
            let body ={...result.data, isJoined:true}
            this.props.navigation.navigate('DetailGroup', 
                {
                    data:body,
                    actionsBack:isBack => this.isBack(isBack)
                })
        }
        else{
            let mess = 'Tạo nhóm thất bại. Lý do: lỗi hệ thống. \n Ðề xuất: Hãy thử Lưu lại lần nữa.'
            this.showAlert(mess)
        }
    }

    checkDisable(){
        let { nameAvatar, nameGroups,typeGroup, typePermissGroup, typeThread, listEmploy ,} = this.state
        if(this.data){
            this.setState({disabled:false})
        }
        else if((nameAvatar == 'none') || (nameGroups.trim().length ==0) ||
            (typeGroup == 0) || (typePermissGroup == 0) || (typeThread == 0) || (listEmploy.length == 0)
        ){
            this.setState({disabled:true})
        }
        else{
            this.setState({disabled:false})
        }
    }

    goPage(type){
        switch (type) {
            case 1:
                this.props.navigation.navigate('TypeGroup', {
                    typeGroup: this.state.typeGroup,
                    userInfo:this.userInfo,
                    actions:(type, name) => this.setState({typeGroup:type, nameAvatar:name},()=>{
                        this.checkDisable()
                    })
                })
                break;
            case 2:
                this.props.navigation.navigate('Description', {
                    content: this.state.content,
                    actions:(value) => this.setState({content:value},()=>{
                        this.checkDisable()
                    })
                })
                break;
            case 3:
                Platform.OS === 'android' ?
                this.props.navigation.navigate('SearchAndroid', {
                    listEmploy: this.state.listEmploy,
                    actions:(list) => this.setState({listEmploy:list},()=>{
                        this.checkDisable()
                    })
                })
                :
                this.props.navigation.navigate('SearchUserGroup', {
                    listEmploy: this.state.listEmploy,
                    actions:(list) => this.setState({listEmploy:list},()=>{
                        this.checkDisable()
                    })
                })
                break;
            case 4:
                this.props.navigation.navigate('PermissionGroups', {
                    page:1,
                    type: this.state.typePermissGroup,
                    actions:(type) => this.setState({typePermissGroup:type},()=>{
                        this.checkDisable()
                    })
                })
                break;
            
            case 5:
                this.props.navigation.navigate('PermissionGroups', {
                    page:2,
                    type: this.state.typeThread,
                    actions:(type) => this.setState({typeThread:type},()=>{
                        this.checkDisable()
                    })
                })
                break;
            default: null
                break;
        }
    }

    checkPlaceholder(place, type){
        let value = null
        switch (type) {
            case 1:
                value = UtilsGroup.checkTypeGoup(this.state.typeGroup)             
                break;
            case 2:
                value = this.state.content.trim().length >0 ? this.state.content : place          
            break;
            case 4:
                value =  this.state.typePermissGroup == 0? 'Chọn quyền' : UtilsGroup.checkType(this.state.typePermissGroup)             
            break;
            case 5:
                value = this.state.typeThread == 0? 'Chọn quyền' : UtilsGroup.checkCreateThread(this.state.typeThread)             
            break;
            default:
                value = place
                break;
        }
        return value
    }

    isBack(isBack){
        if(isBack && (isBack== true)){
            this.props.navigation.pop()
        }
    }
}
const mapStateToProps = (state) => ({ ...state })
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(CreateGroup);