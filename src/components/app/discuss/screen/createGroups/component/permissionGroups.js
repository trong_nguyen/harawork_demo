import React, { Component } from 'react';
import { View, TouchableOpacity, Text, ScrollView } from 'react-native';
import * as UtilsGroup from '../../../group/components/Utils'
import {HeaderScreenNews, settingApp, iconGroups} from '../../../../../../public'
import { MaterialIcons, Ionicons  } from '@expo/vector-icons'

class PermissionGroups extends Component{
    constructor(props){
        super(props)
        this.state={
            typeCheck: props.navigation.state.params.type
        }
        this.page = props.navigation.state.params.page
        this.type = props.navigation.state.params.actions
    }

    renderHeader(){
        let { page } = this;
        return(
            <HeaderScreenNews 
                buttonLeft={
                    <TouchableOpacity
                        onPress={() => this.props.navigation.pop()}
                        style={{
                            width:50, height:44, justifyContent:'center', alignItems:'center'
                        }}
                    >
                        <MaterialIcons  name='close' size={25} color={settingApp.colorDisable} />
                    </TouchableOpacity>
                }
                title ={page == 1 ?  'Quyền riêng tư' : 'Quyền tạo chủ đề'}
            />
        )
    }


    renderGroup(){
        const { typeCheck} = this.state;
        return(
            <View style={{flex:1, padding:15}}>
                <TouchableOpacity 
                    onPress={() => this.setState({typeCheck:1},() =>{
                        this.type(1)
                        this.props.navigation.pop()
                    })}
                    style={styles.button}> 
                    <View style={{flexDirection:'row', width:30, height:80}}>
                        <MaterialIcons name={typeCheck == 1 ? 'radio-button-checked' : 'radio-button-unchecked'} size={20} color={typeCheck == 1 ? settingApp.blueSky : settingApp.colorSperator} />
                    </View>
                    <View style={{minHeight:80, width:(settingApp.width*0.8)}}>
                        <Text style={styles.title}>Nhóm mở</Text>
                        <Text style={{fontSize:14, color:settingApp.colorText}}
                        >Bất kỳ ai trong công ty cũng có thể xem nhóm, thành viên của nhóm và bài viết của họ.</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity 
                    onPress={() => this.setState({typeCheck:2},() =>{
                        this.type(2)
                        this.props.navigation.pop()
                    })}
                    style={styles.button}> 
                    <View style={{flexDirection:'row', width:30, height:80}}>
                        <MaterialIcons name={typeCheck == 2 ? 'radio-button-checked' : 'radio-button-unchecked'} size={20} color={typeCheck == 2 ? settingApp.blueSky : settingApp.colorSperator} />
                    </View>
                    <View style={{minHeight:80, width:(settingApp.width*0.8)}}>
                        <Text style={styles.title}>Nhóm kín</Text>
                        <Text style={{fontSize:14, color:settingApp.colorText}}
                        >Những người không phải thành viên sẽ không thấy được nhóm</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    renderThread(){
        const { typeCheck} = this.state;
        return(
            <View style={{flex:1, padding:15}}>
                 <TouchableOpacity 
                    onPress={() => this.setState({typeCheck:1},() =>{
                        this.type(1)
                        this.props.navigation.pop()
                    })}
                    style={styles.button}> 
                    <View style={{flexDirection:'row', width:30, height:80}}>
                        <MaterialIcons name={typeCheck == 1 ? 'radio-button-checked' : 'radio-button-unchecked'} size={20} color={typeCheck == 1 ? settingApp.blueSky : settingApp.colorSperator} />
                    </View>
                    <View>
                        <Text style={styles.title}>Quản trị viên</Text>
                        <Text style={{fontSize:14, color:settingApp.colorText}}
                        >Chỉ quản trị viên mới có thể tạo chủ đề mới</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => this.setState({typeCheck:2},() =>{
                        this.type(2)
                        this.props.navigation.pop()
                    })}
                    style={styles.button}
                > 
                    <View style={{flexDirection:'row', width:30, height:80}}>
                        <MaterialIcons name={typeCheck == 2 ? 'radio-button-checked' : 'radio-button-unchecked'} size={20} color={typeCheck == 2 ? settingApp.blueSky : settingApp.colorSperator} />
                    </View>
                    <View style={{height:80}}>
                        <Text style={styles.title}>Mọi thành viên</Text>
                        <Text style={{fontSize:14, color:settingApp.colorText}}
                        >Mọi thành viên trong nhóm đều có thể tạo chủ đề mới.</Text>
                    </View>
                </TouchableOpacity>
               
            </View>
        )
    }

    render(){
        let { page } = this;
        let content = <View />
        content = page == 1 ? this.renderGroup() : this.renderThread()
        return(
            <View style={{flex:1, backgroundColor:'#FFFFFF'}}>
                {this.renderHeader()}
                {content}
            </View>
        )
    }
}

const styles = {
    button:{
        width:(settingApp.width-30), 
        height:80, 
        flexDirection:'row'
    },
    title:{
        fontSize:16, 
        color:settingApp.colorText
    }
}
export default PermissionGroups
