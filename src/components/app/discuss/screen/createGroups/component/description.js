import React, { Component } from 'react';
import { View, TouchableOpacity, Text, TextInput } from 'react-native';
import * as UtilsGroup from '../../../group/components/Utils'
import {HeaderScreenNews, settingApp, iconGroups} from '../../../../../../public'
import { MaterialIcons, Ionicons  } from '@expo/vector-icons'

class Description extends Component {
    constructor(props){
        super(props)
        this.state={
            content: props.navigation.state.params.content
        }
        this.actions = props.navigation.state.params.actions
    }

    checkType(type){
        let check = null;
        if(type== this.typeGroup){
            check = true
        }
        else {
            check=false
        }
        return check
    }

    renderHeader(){
        let {content} = this.state
        return(
            <HeaderScreenNews 
                buttonLeft={
                    <TouchableOpacity
                        onPress={() => this.props.navigation.pop()}
                        style={{
                            width:50, height:44, justifyContent:'center', alignItems:'center'
                        }}
                    >
                        <MaterialIcons  name='close' size={25} color={settingApp.colorDisable} />
                    </TouchableOpacity>
                }
                title ='Mô tả'
                buttonRight = {
                    <TouchableOpacity
                        onPress={() => {
                            this.actions(content)
                            this.TextInput.clear()
                            this.props.navigation.pop()}}
                        style={{
                            width:50, height:44, justifyContent:'center', alignItems:'center'
                        }}
                    >
                        <Text style={{fontSize:17, color:settingApp.blueSky}}>Lưu</Text>
                    </TouchableOpacity>
                }
            />
        )
    }

    render(){
        let { content } = this.state
        let total = (200-content.length)
        let mess =  total < 200 ? 'Còn lại' : 'Tối đa'
        return(
            <View style={{flex: 1, backgroundColor:'#FFFFFF'}}> 
                {this.renderHeader()}
                <View style={{ minHeight: 80, justifyContent:'center', padding:15}}>
                    <TextInput
                        ref = {refs => this.TextInput = refs}
                        style={{
                            minHeight: 40,
                            fontSize:15,
                            paddingBottom:5
                            }}
                        placeholder='Mô tả nhóm'
                        onChangeText={(content) => this.setState({content})}
                        value={this.state.content}
                        maxLength={200}
                        multiline={true}
                    />
                    <View
                        style={{
                            width:(settingApp.width -30),
                            height:1,
                            backgroundColor:settingApp.blueSky,
                            marginBottom:5,
                            marginTop:5
                        }}
                    />
                    <Text style={{color:settingApp.colorDisable, fontSize:12}}>{`${mess} ${total} ký tự`}</Text>
                </View>
            </View>
        )
    }
}

const styles = {
    note:{
        color:settingApp.colorDisable, 
        fontSize:14,
        paddingTop:5 ,
    }
}
export default Description;