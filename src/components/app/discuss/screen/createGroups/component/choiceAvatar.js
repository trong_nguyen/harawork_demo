import React, { Component } from 'react';
import { View, TouchableOpacity, Text, ScrollView } from 'react-native';
import * as UtilsGroup from '../../../group/components/Utils'
import {HeaderScreenNews, settingApp, iconGroups} from '../../../../../../public'
import { MaterialIcons, Ionicons  } from '@expo/vector-icons'

class ChoiceAvatar extends Component{
    constructor(props){
        super(props)
        this.state={
            listFirst:[{name:'sport'}, {name:'document'} , {name:'communicate'}, {name:'tag'}, {name:'group'}],
            listSecon:[{name:'customer-service'}, {name:'house'} , {name:'beach'}, {name:'teacher'}, {name:'building'}],
            listThree:[{name:'money'}, {name:'books'} , {name:'information'}, {name:'cup'}, {name:'speaker'}]
        }

        this.choiceAvatar = props.navigation.state.params.actions;
        this.nameAvatar = props.navigation.state.params.nameAvatar
    }

    checkName(name){
        let isCheck =null;
        if(name === this.nameAvatar){
            isCheck = true
        }
        else{
            isCheck = false
        }
        return isCheck
    }

    renderHeader(){
        return(
            <HeaderScreenNews 
                buttonLeft={
                    <TouchableOpacity
                        onPress={() => this.props.navigation.pop()}
                        style={{
                            width:50, height:44, justifyContent:'center', alignItems:'center'
                        }}
                    >
                        <MaterialIcons  name='close' size={25} color={settingApp.colorDisable} />
                    </TouchableOpacity>
                }
                title ='Ảnh đại diện'
            />
        )
    }

    checked(){
        return(
            <View
                style={{
                    width:54, 
                    height:54, 
                    borderRadius:27, 
                    borderWidth:3, 
                    borderColor:settingApp.blueSky,
                    top:0,
                    bottom:0,
                    left:0,
                    right:0,
                    position:'absolute'
                    }}
            >
                <View
                    style={{
                        width:48 ,
                        height:48, 
                        borderRadius:24, 
                        borderWidth:2, 
                        borderColor:'#ffffff',
                        }}
                    />
                <View
                    style={{justifyContent:'center', alignItems:'center',top:-5, right:-5, zIndex:10, position:'absolute',width:20, height:20,borderRadius:10, backgroundColor:'#FFFFFF'}}
                >
                    <Ionicons  name='md-checkmark-circle' size={20} color={settingApp.blueSky}/>
                </View>
            </View>

        )
    }

    renderLine(list){
        return(
            <View style={{justifyContent:'center', alignItems:'center', marginBottom:15}}>
                <View style={{height:60, justifyContent:'space-between', flexDirection:'row', width:(settingApp.width -30)}}>
                    {list.map((item, index) =>{
                         let isCheck = this.checkName(item.name)
                        let avatar = UtilsGroup.checkName(item.name, 50)
                        return(
                            <TouchableOpacity
                                onPress={() => {
                                    this.choiceAvatar(item.name)
                                    this.props.navigation.pop()
                                    }}
                                key={index}
                                style={{width:55, height:55, justifyContent:'center', alignItems:'center'}}
                            >
                                {avatar}
                                {isCheck && this.checked()}
                            </TouchableOpacity>
                        )
                    })}
                </View>
            </View>

        )
    }

    render(){
        return(
            <View style={{flex:1, backgroundColor:'#FFFFFF'}}>
                {this.renderHeader()}
                <Text style={{ padding:15, color:settingApp.colorText, fontSize:16}}>Chọn ảnh đại diện</Text>
                {this.renderLine(this.state.listFirst)}
                {this.renderLine(this.state.listSecon)}
                {this.renderLine(this.state.listThree)}
            </View>
        )
    }
}
export default ChoiceAvatar;