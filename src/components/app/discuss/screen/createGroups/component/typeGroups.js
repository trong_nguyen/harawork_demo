import React, { Component } from 'react';
import { View, TouchableOpacity, Text, ScrollView } from 'react-native';
import * as UtilsGroup from '../../../group/components/Utils'
import {HeaderScreenNews, settingApp, iconGroups} from '../../../../../../public'
import { MaterialIcons, Ionicons  } from '@expo/vector-icons'

class TypeGroups extends Component {
    constructor(props){
        super(props)
        this.state={
            listContent:[{type:1, name:'document'},{type:2, name:'group'},{type:3, name:'sport'},{type:4, name:'information'}]
        }

        this.typeGroup = props.navigation.state.params.typeGroup
        this.actions = props.navigation.state.params.actions
        this.userInfo = props.navigation.state.params.userInfo
    }

    componentDidMount(){
        let { listContent } = this.state
        if(this.userInfo.role === "admin"){
            listContent.push({type:5, name:'speaker'})
            this.setState({listContent})
        }
        else{
            this.setState({listContent})
        }
    }

    checkType(type){
        let check = null;
        if(type== this.typeGroup){
            check = true
        }
        else {
            check=false
        }
        return check
    }

    renderHeader(){
        return(
            <HeaderScreenNews 
                buttonLeft={
                    <TouchableOpacity
                        onPress={() => this.props.navigation.pop()}
                        style={{
                            width:50, height:44, justifyContent:'center', alignItems:'center'
                        }}
                    >
                        <MaterialIcons  name='close' size={25} color={settingApp.colorDisable} />
                    </TouchableOpacity>
                }
                title ='Loại'
            />
        )
    }

    renderContent(item, index){
        let typeName = UtilsGroup.checkTypeGoup(item.type)
        let isCheck = this.checkType(item.type)
        return(
            <View 
                key={index}
                style={{
                    width:(settingApp.width -30),
                    height:60, 
                    borderBottomColor:settingApp.colorSperator, 
                    borderBottomWidth:1,
                    alignItems:'center'
                    }}>
                    <TouchableOpacity 
                        onPress={() => {
                                this.actions(item.type, item.name)
                                this.props.navigation.pop()
                            }}
                        style={{ flex:1, flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
                        <Text style={{color:settingApp.colorText, fontSize:16, width:(settingApp.width -80)}}>{typeName}</Text>
                        <View style={{width:40, height:40, justifyContent:'center', alignItems:'center'}}>
                            {isCheck==true ? <Ionicons  name='md-checkmark' size={30} color={settingApp.blueSky} /> : <View/>}
                        </View>
                        
                    </TouchableOpacity>
            </View>
        )
    }

    renderNote(){
        return(
            <View style={{width:(settingApp.width -30), padding:15 }}>
                <Text style={[styles.note]}>* Lưu ý về loại Mặc định:</Text>
                <Text style={[styles.note,{paddingLeft:20}]}>+ Thành viên trong nhóm tự cập nhật theo cây sơ đồ công ty</Text>
                <Text style={[styles.note,{paddingLeft:20}]}>+ Phù hợp cho việc tạo các nhóm tin tức, chức năng của công ty</Text>
            </View>
        )
    }
    
    render(){
        const { listContent } = this.state;
        return(
            <View style={{flex:1, backgroundColor:'#FFFFFF'}}>
                {this.renderHeader()}
                <View style={{padding:15}}>
                    {listContent.map((item, index) =>{
                        return(
                            this.renderContent(item, index)
                        )
                    })
                    }
                </View>
                {this.adminIC && this.renderNote()}
            </View>
        )
    }
}

const styles = {
    note:{
        color:settingApp.colorDisable, 
        fontSize:14,
        paddingTop:5 ,
    }
}
export default TypeGroups;