import React, { Component } from 'react';
import { View, Text, FlatList, ScrollView, TouchableOpacity, ActivityIndicator } from 'react-native';
import { HeaderScreenNews, settingApp, Loading } from '../../../../../public';
import TabGroup from './component/tabGroup'
import { Feather, MaterialCommunityIcons,Ionicons } from '@expo/vector-icons';
import Modal from '../../group/components/modalOption'

class Layout extends Component{

    renderHeader(){
        this.actionsBack
        return(
            <HeaderScreenNews 
                buttonLeft={
                    <TouchableOpacity
                        onPress={() => {
                            this.actionsBack && this.actionsBack(true)
                            this.props.navigation.pop()}}
                        style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                            <Ionicons name='ios-arrow-back' size={23} color='#000000' />
                    </TouchableOpacity>}
                title= {this.data.name}
                buttonRight={
                    <TouchableOpacity 
                        onPress={() => this.setState({isShow:true})}
                        style={{width:50, height:44, justifyContent:'center', alignItems:'center'}}>
                        <Feather name='more-horizontal' size={25} color={settingApp.colorButon} />     
                    </TouchableOpacity>
                }
                navigation={this.props.navigation}
            />
        )
    }

    renderJoin(){
        const { isJoining } = this.state
        return(
            <View
                style={{
                    position:'absolute',
                    backgroundColor:'transparent',
                    height:50,
                    width:(settingApp.width),
                    bottom:20,
                    justifyContent:'center',
                    alignItems:'center',
                    left:0, 
                    right:0
                }}
            >
                {!isJoining ? <TouchableOpacity
                    onPress={() =>  this.setState({isJoining:true},() =>{
                        this.joinGroup(true)
                    })}
                    style={{
                        flexDirection:'row', 
                        width:(settingApp.width*0.5), 
                        height:44, 
                        backgroundColor:'#FFFFFF', 
                        justifyContent:'center', 
                        alignItems:'center',
                        ...settingApp.shadow,
                        borderRadius:20,
                        padding:10,
                        }}
                >
                    <MaterialCommunityIcons name='plus-circle' size={20} color={settingApp.blueSky}/>
                    <Text style={{fontSize:16, fontWeight:'600', color:settingApp.blueSky, paddingLeft:10}}>Tham gia nhóm</Text>
                </TouchableOpacity>
                : <ActivityIndicator size='small' color={settingApp.color}/>
                }
            </View>
        )
    }

    renderLoading() {
        return (
            <View style={{
                position: 'absolute',
                backgroundColor: 'rgba(0,0,0,0.5)',
                justifyContent: 'center',
                alignItems: 'center',
                width:settingApp.width,
                height:settingApp.height,
            }}>
                <ActivityIndicator size='large' color={settingApp.color} />
            </View>
        )
    }

    render(){
        const { isShow, data, isLoading } = this.state;
        if(data){
            return(
                <View style={{flex:1, backgroundColor:'#FFFFFF'}}>
                    {this.renderHeader()}
                    <View style={{flex:1,  padding:15}}>
                        <TabGroup 
                                screenProps={{
                                data: this.state.data,
                                mainNavigation: this.props.navigation
                            }}
                        />
                       
                    </View>
                    {(data && data.isJoined == true) ? <View/> : this.renderJoin()}
                    <Modal 
                        visible = {isShow}
                        item = {data}   
                        isShow={isShow}
                        close={() => this.setState({isShow:false})}
                        checkOption={(item, type) => this.checkOption(item, type)}
                    />
                     { isLoading && this.renderLoading()}
                </View>
            )
        }
        else{
            return <View />
        }
        
    }
}
export default Layout;