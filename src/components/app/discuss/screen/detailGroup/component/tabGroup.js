import React, { Component } from 'react';
import { createMaterialTopTabNavigator } from 'react-navigation';
import { settingApp, SvgCP } from '../../../../../../public';
import ListTopic from '../screenGroups/listToppic';
import InfoGroup from '../screenGroups/infoGroup';
import ListEmploy from '../screenGroups/listEmploy';

const TabGruops = createMaterialTopTabNavigator(
    {
        TopicTab:{
            screen: (props) => <ListTopic
                    data= {props.screenProps.data}
                    mainNavigation={props.screenProps.mainNavigation}
                    {...props}
                />,
            navigationOptions:{
                tabBarLabel: 'Chủ đề',
            }
        },
        InfoTab: {
            screen: (props) => <InfoGroup
                    data= {props.screenProps.data}
                    mainNavigation={props.screenProps.mainNavigation}
                    {...props}
                />,
            navigationOptions: {
                tabBarLabel: 'Thông tin',
            }
        },
        EmployTab: {
            screen: (props) => <ListEmploy
                    data= {props.screenProps.data}
                    mainNavigation={props.screenProps.mainNavigation}
                    {...props}
                />,
            navigationOptions: {
                tabBarLabel: 'Thành viên',
            }
        },
    }, {
        animationEnabled:true,
        tabBarPosition:'top',
        swipeEnabled: false,
        tabBarOptions: {
            activeTintColor: '#FFFFFF',
            inactiveTintColor: settingApp.colorDisable,
            activeBackgroundColor:settingApp.blueSky,
            tabStyle:{
                height:40,
                alignItems:'center',
                justifyContent:'center',
            },
            labelStyle: {
                fontSize: 14,
                fontWeight:'bold',
                height:25,
                paddingTop:5,
                width: ((settingApp.width-42)/3),
            },
            style: {
                elevation: 0,   
                shadowOpacity: 0,
                shadowOffset: {
                        height: 0,
                        width:0
                    },
                shadowRadius: 0,
                height:40,
                backgroundColor: '#FFFFFF',
            },
            indicatorStyle: {
                backgroundColor:settingApp.blueSky,
                borderRadius:20,
                height:35,
            },
            scrollEnabled:false,
            upperCaseLabel:false
        }
    }
);

export default TabGruops;