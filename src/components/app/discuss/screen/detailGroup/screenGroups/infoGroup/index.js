import Layout from './layout';
import { HaravanIc } from '../../../../../../../Haravan';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../../../../state/action';
import * as Utils from '../../../../components/Utils'

class InfoGroup extends Layout{
    constructor(props){
        super(props)
        this.state={
            data:null
        }
        this.collegue = props.app.colleague;

        this.data = props.data;
        this.navigation = props.mainNavigation;
        this.body = null;
        _this = this
    }

    async componentDidMount(){
        if(this.data){
            this.setState({data: this.data})
        }
        this.body = await Utils.GetBody(this.collegue)
    }

    componentWillReceiveProps(nextProps){
        if(nextProps && nextProps.data &&
            (JSON.stringify(nextProps.data) !== JSON.stringify(this.data))
            ){
                this.setState({data:nextProps.data})
            }
    }

    editGroups(){
        this.navigation.navigate('CreateGroup',{
            data: this.state.data,
            setValue:(reload, id) => this.checkDetail(reload, id)
        })
    }

    async checkDetail(reload, id){
        if(reload){
            const result = await HaravanIc.GetDetailGroups(id, this.body)
            if(result && result.data && !result.error){
                this.setState({data:result.data},() =>{
                    this.props.actions.discuss.changeItem({data:result.data})
                })
            }
           
        }
        
    }
}
const mapStateToProps = (state) => ({ ...state })
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(InfoGroup);