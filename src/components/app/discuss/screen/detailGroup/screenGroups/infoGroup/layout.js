import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { settingApp, Loading } from '../../../../../../../public';
import * as UtilGroups from '../../../../group/components/Utils';
import {MaterialCommunityIcons} from '@expo/vector-icons'

class Layout extends Component{

    renderLine(title, description){
        return(
            <View style={{minHeight:60, width:(settingApp.width -50), justifyContent:'center'}}>
                <Text style={{fontSize:14, color:settingApp.colorDisable, paddingBottom:5, fontWeight:'600'}}>{title}</Text>
                <Text style={{fontSize:14, color:settingApp.colorText}}>{description}</Text>
            </View>
        )
    }

    renderButton(){
        return(
            <View style={{flex:1, justifyContent:'center', alignItems:'center',marginTop:20, marginBottom:20,}}>
                <TouchableOpacity 
                    onPress={() => this.editGroups()}
                    style={{
                        width:(settingApp.width*0.8),
                        height:50, 
                        backgroundColor:settingApp.blueSky, 
                        borderRadius:5,
                        flexDirection:'row',  
                        alignItems:'center',
                        justifyContent:'center',
                    }}>
                        <MaterialCommunityIcons name='pencil' size={20} color='#FFFFFF'/>
                        <Text style={{fontSize:18, fontWeight:'600', color:'#ffffff', paddingLeft:10}}>Chỉnh sửa</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render(){
        const { data } = this.state
        let permission =data && UtilGroups.checkType(data.type)
        let typeGroup = data && UtilGroups.checkTypeGoup(data.contentType)
        let createTheard =data &&  UtilGroups.checkCreateThread(data.createThreadType)
        if(data){
            return(
                <ScrollView 
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    style={{flex:1, marginBottom:40}}>
                    {this.renderLine('Nhóm', data.name)}
                    {this.renderLine('Số thành viên', `${data.countMember} thành viên`)}
                    {this.renderLine('Quyền riêng tư', permission)}
                    {this.renderLine('Loại', typeGroup)}
                    {this.renderLine('Mô tả', data.description ? data.description : 'Không có mô tả')}
                    {this.renderLine('Quyền tạo chủ đề', createTheard)}
                    {(data.isAdminGroup || data.isAdminForum) && data.isJoined && this.renderButton()}
                    <View
                        style={{flex:1, height:40, backgroundColor:'#FFFFFF'}}
                    />
                </ScrollView>
            )
        }
        else{
            return <Loading/>
        }
    }
}
export default Layout;