import React, { Component } from 'react';
import { View, Text, FlatList, ScrollView, TouchableOpacity , TextInput, ActivityIndicator, RefreshControl, Keyboard} from 'react-native';
import { settingApp, SvgCP, iconGroups, Loading} from '../../../../../../../public';
import Item from './component/item';
import lodash from 'lodash';
import { EvilIcons } from '@expo/vector-icons'

class Layout extends Component{

    renderFooter(){
        return(
            <View
                style={{
                    width:(settingApp.width-30),
                    height:(settingApp.statusBarHeight + 90),
                    }}
            />
        )
    }
    renderLoadmore(){
        return(
            <View
                style={{
                    width:(settingApp.width-30),
                    height:(settingApp.statusBarHeight + 90),
                    }}
            >
                <ActivityIndicator size="small"  color={settingApp.color}/>
            </View>
        )
    }

    renderContent(create){
        let { isLoad, isLoading , listData, data, loadMore, isSearch, text} = this.state;
        if((listData.length > 0) && !isLoading){
            return(
                <View style={{flex:1}}>
                    {isLoad && <ActivityIndicator size='small' color={settingApp.color} />}
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={this.state.listData}
                        extraData={this.state}
                        keyExtractor={(item, index) => item.id ? item.id+'' :  ""+index}
                        onEndReachedThreshold={1}
                        onEndReached={() => this.loadMore()}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={() => this.setState({refreshing:true},()=> {this.refreshList()})}
                                tintColor={settingApp.color}
                                colors={[settingApp.color]}
                            />
                        }
                        renderItem={({item, index}) => 
                            <Item 
                                item={item}
                                index={index}
                                navigation ={ this.navigation}
                                collegue= {this.collegue}
                                delete={value => this.checkDelete(value)}
                            />
                        }
                        ListFooterComponent={loadMore ?  this.renderLoadmore() : this.renderFooter()}
                        ListHeaderComponent={isSearch && 
                        <Text style={{fontSize:14, paddingTop:5, color:settingApp.colorDisable}}>{`Có ${listData.length} kết quả tìm được`}</Text>}
                    />
                </View>
            )
        }
        else if((listData.length == 0) && !isLoading){
            let mess = isSearch ?  `Không tìm thấy "${text}"trong \n Danh sách chủ đề \n Ðề xuất: Hãy thử những từ khóa khác. `:
                `Rất tiếc, hiện chưa có \n chủ đề nào`
            return(
                <TouchableOpacity 
                    activeOpacity={1}
                    onPress={() => Keyboard.dismiss()}
                    style={{flex:1, alignItems:'center', marginTop:30}}>
                    <iconGroups.ArtWorks/>
                    <Text style={{ textAlign:'center',fontSize:16, color:settingApp.colorText, fontWeight:'600', marginTop:10}}
                        >{mess}</Text>
                    {(data && (data.isJoined == true)) && create && !isSearch
                        && <TouchableOpacity
                        onPress={() => this.navigation.navigate('CreateThread', 
                            {data:data,
                            actions: (value) => this.checkCreateThread(value)
                            })}
                        style={{
                            width:settingApp.width*0.8,
                            height:50,
                            backgroundColor:settingApp.blueSky,
                            borderRadius:5,
                            ...settingApp.shadow,
                            justifyContent:'center', 
                            alignItems:'center',
                            marginTop:10
                        }}
                    >
                        <Text style={{fontSize:16, color:'#FFFFFF', fontWeight:'600'}}>+ Thêm chủ đề mới</Text>
                    </TouchableOpacity>}
                </TouchableOpacity>
            )
        }
        else{
            return <Loading/>
        }
        
    }

   

    renderSearch(){
        let onChangeCallback = lodash.debounce(this.onSearchBox, 500);
        return(
            <View style={{
                width:settingApp.width -30,
                height:35,
                paddingLeft:10,
                paddingRight:10,
                marginTop:10,
                paddingBottom:5,
                borderRadius:10,
                backgroundColor:'rgba(142, 142, 147, 0.12);',
                flexDirection:'row',
                alignItems:'center'
            }}>
                <EvilIcons name="search" size={25} color={settingApp.colorDisable} />
                <TextInput
                    ref = {refs => this.TextInput = refs}
                    style={{
                        height: 30,
                        width:settingApp.width -120,
                        fontSize:15,}}
                    placeholder='Tìm kiếm'
                    onChangeText={onChangeCallback.bind(this)}
                    multiline={false}
                />
            </View>
        )
    }

    render(){
        const {data} = this.state
        let create = this.checkCreate(data)
        return(
            <View style={{flex:1}}>
                {( data && data.isJoined == true ) && (this.state.listData.length > 0) 
                  && create &&
                 <TouchableOpacity
                    onPress={() => this.navigation.navigate('CreateThread',{
                        data:data,
                        actions: (value) => this.checkCreateThread(value)})}
                    style={{
                        position:'absolute',
                        right:10,
                        bottom:settingApp.statusBarHeight,
                        zIndex:9999
                    }}>
                    <SvgCP.icon_AddThread/>
                </TouchableOpacity>
                }
                {this.renderSearch()}
                {this.renderContent(create)}
            </View>
        )
    }
}
export default Layout;