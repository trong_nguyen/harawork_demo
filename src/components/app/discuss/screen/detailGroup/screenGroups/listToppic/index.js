import Layout from './layout';
import { HaravanIc, HaravanHr } from '../../../../../../../Haravan';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../../../../state/action';
import * as Utils from '../../../../components/Utils'

class ListTopic extends Layout{
    constructor(props){
        super(props)
        this.state={
            isLoading:true,
            listData:[],
            isLoad:false,
            data:null,
            loadMore:false,
            refreshing:false,
            isSearch:false,
            text:''
        }
        this.collegue = props.app.colleague;
        this.url = 'list_all'
        this.data = props.data;
        this.navigation = props.mainNavigation;
        this.page =1;
        this.totalPage = null;
        this.currentList = [];
        this.body = null
    }

    

    async componentDidMount(){
        if(this.data){
            this.setState({data:this.data},()=>{
                this.loadData()
            })  
        }
        this.body = await Utils.GetBody(this.collegue)
    }

    componentWillReceiveProps(nextProps){
        if(nextProps && nextProps.data &&
            (JSON.stringify(nextProps.data) !== JSON.stringify(this.data))
            ){
                this.setState({data:nextProps.data})
            }
    }

    async refreshList(text){
        this.page =1;
        this.totalPage = null;
        this.currentList = []
        await this.loadData(text)
    }

    onSearchBox(text) {
        if(text.length != 0){
            this.url = 'list_search'
            this.setState({isLoad: true, listData:[], isSearch:true, text:text},() =>{
                this.refreshList(text)
            })
        }
        else{
            this.url = 'list_all'
            this.setState({ listData:[], isSearch:false, isLoading: true },() =>{
                this.refreshList(text)
            })
        }
    }

    checkCreateThread(actions){
        if(actions && (actions ==true)){
            this.setState({refreshing:true},()=>{
                this.refreshList('')
            })
        }
    }

    checkDelete(value){
        let { listData } = this.state
        if(value.status == true){
            const index = listData.findIndex(m => (m.id == value.data.thread.id))
            if(index > -1){
                listData.splice(index, 1)
                this.setState({listData})
            }
        }
    }
    checkCreate(data){
        let create = null
        if(data && data.createThreadType == 2){
            create = true;
        }
        else{
            if(data && (data.isAdminGroup || data.isAdminForum)){
                create =true
            }
            else{
                create =false
            }
        }
        return create
    }

    loadMore(){
        const { totalPage, page } = this
        if(page <= totalPage){
            this.setState({loadMore:true},() =>{
                this.loadData()
            })
        }
    }
    

    async loadData(text){
        let { totalPage, page, currentList, url, collegue  } = this
        let value = text ? text : '';
        let newList = currentList
        if(!totalPage || (totalPage && !isNaN(totalPage) && (page < totalPage))){
            const result = await HaravanIc.GetListThread(this.data.id, url, page, value);
            if(result && result.data && !result.error){
                newList = [...newList, ...result.data.data]
                const totalCaount = result.data.totalCount
                this.currentList = newList
                this.page = page + 1
                this.totalPage = Math.ceil(totalCaount/20)
                this.setState({listData:newList, isLoading:false, isLoad:false, loadMore:false, refreshing:false})
            }
            else{
                this.setState({ isLoading:false, isLoad:false, loadMore:false, refreshing:false})
            }
        }
        else{
            this.setState({ isLoading:false, isLoad:false, loadMore:false, refreshing:false})
        }
    }
}
const mapStateToProps = (state) => ({ ...state })
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(ListTopic);