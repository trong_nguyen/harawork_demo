import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native';
import { HaravanHr } from '../../../../../../../../Haravan';
import { settingApp, AvatarCustom, SvgCP } from '../../../../../../../../public';
import moment from 'moment';

class Item extends Component{
    constructor(props){
        super(props)
        this.state={
            userCreate:null
        }
    }

    async componentDidMount(){
        const { item, collegue} = this.props;
        let { userCreate } = this.state;
        if(item.createdBy == collegue.haraId){
            userCreate= {...item, photo: collegue.photo, fullName:collegue.fullName,}
        }
        else{
            let result = await HaravanHr.getColleague(item.createdBy)
            if(result && result.data && !result.error){
                userCreate= {...item, photo: result.data.photo, fullName:result.data.fullName,}
            }
            else{
                userCreate = {...item}
            }
        }
        this.setState({userCreate})
    }

    formatTime(time){
        let newtime = null
        let timeCheck = moment(time).fromNow();
        if(timeCheck.indexOf('trước') > -1){
            newtime=  timeCheck.replace('trước', '').trim()
        }
        else if(timeCheck.indexOf('ago') > -1){
            newtime= timeCheck.replace('ago', '').trim()
        }
        return newtime
    }

    render(){
        const  { userCreate } = this.state;
        const { item, index } = this.props;
        let time = this.formatTime(item.createdAt)
        return(
            <TouchableOpacity 
                    onPress={() => {userCreate && this.props.navigation.navigate('DetailComment', {
                            data:this.state.userCreate,
                            delete:(value) => this.props.delete(value)
                            })}}
                style={{
                    minHeight:80,
                    paddingTop:10, 
                    paddingBottom:10,
                    borderTopColor:settingApp.colorSperator,
                    borderTopWidth:(index == 0 )? 0 :  1,
                    justifyContent:'center'
                    }}>
                <Text
                    numberOfLines={2}
                    style={{width:(settingApp.width - 60), fontSize:18, fontWeight:'600', color:settingApp.colorText}}
                    >{item.name}</Text>
                <View
                    style={{flexDirection:'row', justifyContent:'space-between'}}
                    >
                    <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                        <AvatarCustom userInfo={{ 
                            name: userCreate ? userCreate.fullName : 'NaN', 
                            picture:  (userCreate && userCreate.photo) ? userCreate.photo : '' }} v={30} fontSize={12}/>
                    
                        <Text numberOfLines={1}
                            style={{width:(settingApp.width*0.35), fontSize:14, color:settingApp.blueSky, paddingLeft:5}}
                            >{userCreate ? userCreate.fullName : 'Đang lấy được dữ liệu'}</Text> 
                    </View>
                    

                    <View style={{flexDirection:'row', paddingLeft:5, justifyContent:'center', alignItems:'center'}}>
                        <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                            <SvgCP.iconComment/>
                            <Text style={{fontSize:14, color:settingApp.colorDisable, paddingLeft:5}}
                            >{item.countComment}</Text>
                        </View>

                        <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center', paddingLeft:15}}>
                            <SvgCP.icon_Eye/>
                            <Text style={{fontSize:14, color:settingApp.colorDisable, paddingLeft:5}}
                            >{item.countView}</Text>
                        </View>

                        <Text style={{fontSize:14, color:settingApp.colorDisable, paddingLeft:15, paddingRight:10}}
                        >{time}</Text>
                    </View>
                    
                </View>
            </TouchableOpacity>
        )
    }
}
export default Item;