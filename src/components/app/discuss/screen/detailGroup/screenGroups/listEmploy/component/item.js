import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { HaravanHr } from '../../../../../../../../Haravan';
import { settingApp, AvatarCustom, SvgCP } from '../../../../../../../../public';
import {Feather} from '@expo/vector-icons'

class Item extends Component{
    constructor(props){
        super(props)
        this.state={
            data:null
        }
    }

    componentDidMount(){
        const { item } = this.props
        if(item){
            this.setState({data:item})
        }
    }

    componentWillReceiveProps(nextProps){
        let { data } = this.state;
        if(nextProps && nextProps.itemChange && nextProps.itemChange.numberId && (nextProps.itemChange.numberId == data.numberId)){
            this.setState({data: nextProps.itemChange})
        }
    }

    checkName(id, name){
        const { collegue } = this.props;
        let nameUser = null
        if(id === collegue.userId){
            nameUser = 'Tôi'
        }
        else{
            nameUser = name
        }
        return nameUser
    }

    render(){
        const { item, index, parentData } = this.props;
        const { data } = this.state;
        let name = this.checkName(item.userId, item.fullName)
        return(
            <View style={{
                minHeight:80, 
                flexDirection:'row',
                borderTopColor:settingApp.colorSperator,
                borderTopWidth:index == 0 ? 0 : 1,
                alignItems:'center'
                }}>
                <View style={{width:44, height:44, justifyContent:'center', alignItems:'center'}}>
                <AvatarCustom userInfo={{ 
                    name: item.fullName, 
                    picture: '' }} v={40} fontSize={14}/>
                </View>
                <View>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Text
                            style={{fontSize:16, color:settingApp.colorText, fontWeight:'600', padding:5}}
                        >{name}</Text>
                        {data && data.role == 1 &&
                            <SvgCP.icon_Admin/>
                        }
                    </View>
                    {item.userId && 
                        <Text style={{fontSize:14, color:settingApp.colorText, fontWeight:'600', padding:5}}>{item.userId}</Text>
                    }
                </View>
                {data && parentData.isJoined && (parentData.isAdminGroup|| parentData.isAdminForum) &&
                    <TouchableOpacity
                        onPress={() => this.props.showModal(data)}
                        style={{
                            position:'absolute',
                            height:50,
                            width:50,
                            right:0
                        }}
                    >
                        <Feather name='more-horizontal' size={20} color={settingApp.colorDisable} />
                    </TouchableOpacity>
                }
                
            </View>
        )
    }
}
export default Item;