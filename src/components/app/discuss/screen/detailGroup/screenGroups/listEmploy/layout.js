import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, ActivityIndicator, TextInput, Keyboard } from 'react-native';
import Item from './component/item';
import { settingApp, SvgCP, NotFound, Loading , iconGroups} from '../../../../../../../public';
import lodash from 'lodash';
import { EvilIcons } from '@expo/vector-icons';
import Modal from '../component/modal'

class Layout extends Component{

    renderFooter(){
        return(
            <View
                style={{
                    width:(settingApp.width-30),
                    height:(settingApp.statusBarHeight +90)
                    }}
            />
        )
    }

    renderLoadMore(){
        return(
            <View
                style={{
                    width:(settingApp.width-30),
                    height:(settingApp.statusBarHeight +90),
                    justifyContent:'center',
                    alignItems:'center'
                    }}
            >
                <ActivityIndicator size="small" color={settingApp.color} />
            </View>
        )
    }
    renderSearch(){
        let onChangeCallback = lodash.debounce(this.onSearchBox, 500);
        return(
            <View style={{
                width:settingApp.width -30,
                height:35,
                paddingLeft:10,
                paddingRight:10,
                borderRadius:10,
                marginTop:10,
                paddingBottom:5,
                backgroundColor:'rgba(142, 142, 147, 0.12);',
                flexDirection:'row',
                alignItems:'center'
            }}>
                <EvilIcons name="search" size={25} color={settingApp.colorDisable} />
                <TextInput
                    ref = {refs => this.TextInput = refs}
                    style={{
                        height: 30,
                        width:settingApp.width -120,
                        fontSize:15,}}
                    placeholder='Tìm kiếm'
                    onChangeText={onChangeCallback.bind(this)}
                    multiline={false}
                />
            </View>
        )
    }

    renderNotFound(){
        const { isSearch, listData, isLoading, text } = this.state;
        if(isSearch && !isLoading && (listData.length==0)){
            return(
                <TouchableOpacity 
                    activeOpacity={1}
                    onPress={() => Keyboard.dismiss()}
                    style={{flex:1, alignItems:'center', marginTop:30}}>
                    <iconGroups.ArtWorks/>
                    <Text style={{ textAlign:'center',fontSize:16, color:settingApp.colorText, fontWeight:'600', marginTop:10}}
                        >{`Không tìm thấy "${text}" trong \n Danh sách thành viên \n Ðề xuất: Hãy thử những từ khóa khác. `}</Text>
                </TouchableOpacity>
            )
        }
        else{
            return(
                <TouchableOpacity 
                    activeOpacity={1}
                    onPress={() => Keyboard.dismiss()}
                    style={{flex:1, alignItems:'center', marginTop:30}}>
                    <iconGroups.ArtWorks/>
                    <Text style={{ textAlign:'center',fontSize:16, color:settingApp.colorText, fontWeight:'600', marginTop:10}}
                        >{`Chưa có thành viên`}</Text>
                </TouchableOpacity>
            )
        }
    }

    renderContent(){
        const {isLoad, loadMore, listData, isSearch} = this.state
        return(
            <View style={{flex:1}}>
                {isLoad && <Loading />}
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={this.state.listData}
                    extraData={this.state}
                    style={{flex:1}}
                    keyExtractor={(item, index) => item.id+''}
                    onEndReachedThreshold={1}
                    onEndReached={() => this.loadMore()}
                    renderItem={({item, index}) => 
                        <Item 
                            item={item}
                            index={index}
                            navigation ={ this.navigation}
                            collegue= {this.collegue}
                            itemChange={this.state.item}
                            parentData = {this.data}
                            showModal={(value) => this.setState({item:value, showModal:true})}
                        />
                    }
                    ListFooterComponent={loadMore ? this.renderLoadMore():this.renderFooter()}
                    ListHeaderComponent={isSearch && 
                        <Text style={{fontSize:14, paddingTop:5, color:settingApp.colorDisable}}>{`Có ${listData.length} kết quả tìm được`}</Text>}
                />
            </View>
            
        )
    }

    render(){
        const {listData, isLoading, showModal, data} = this.state;
        let content = <View/>
        content = isLoading ? <Loading /> : (listData.length > 0 ? this.renderContent() : this.renderNotFound())
        return(
            <View style={{flex:1}}>
                {data && data.isJoined == true ? <TouchableOpacity 
                    onPress={() => this.goSearchUser()}
                    style={{
                        position:'absolute',
                        right:10,
                        bottom:settingApp.statusBarHeight,
                        zIndex:9999
                    }}>
                    <SvgCP.icon_AddMember/>
                </TouchableOpacity>
                : <View/>
                }
                {this.renderSearch()}
                {content}
                <Modal 
                    visible = {showModal}
                    item = {this.state.item}   
                    isShow={showModal}
                    close={() => this.setState({showModal:false})}
                    checkOption={(item, type) => this.checkOption(item, type)}
                    collegue={this.collegue}
                    dataAdmin = {this.data}
                />
            </View>
        )
    }
}
export default Layout;