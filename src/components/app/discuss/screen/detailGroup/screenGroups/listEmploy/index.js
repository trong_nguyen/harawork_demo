import Layout from './layout';
import { HaravanIc, HaravanHr } from '../../../../../../../Haravan';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../../../../state/action';
import { Toast } from '../../../../../../../public';
import * as Utils from '../../../../components/Utils'

class ListEmploy extends Layout{
    constructor(props){
        super(props)
        this.state={
            isLoading:true,
            listData:[],
            isLoad:false,
            showModal:false,
            item:null,
            data:null,
            loadMore:false,
            text:'',
            isSearch:false
        }
        this.collegue = props.app.colleague;
        this.data = props.data;
        this.navigation = props.mainNavigation;
        this.page =1;
        this.totalPage = null;
        this.currentList = [];
        this.body = null
    }

    async componentDidMount(){
        if(this.data){
            this.setState({data:this.data},()=>{
                this.loadData()
            })  
        }
        this.body = await Utils.GetBody(this.collegue)
    }

    componentWillReceiveProps(nextProps){
        if(nextProps && nextProps.data &&
            (JSON.stringify(nextProps.data) !== JSON.stringify(this.data))
            ){
                this.setState({data:nextProps.data})
            }
        if(nextProps && nextProps.discuss && nextProps.discuss.joinGroups && (nextProps.discuss.joinGroups==true)){
            this.refreshList()
        }
    }

    goSearchUser(){
        this.navigation.navigate('SearchUserGroup', {
            listEmploy: [],
            actions:(listEmploy) => this.setState({isLoading:true},()=>{
                this.addUser(listEmploy)
            })
        })
    }

    async addUser(listEmploy){
        let list = this.checkList(listEmploy)
        let result = await HaravanIc.addMember(this.data.id, list)
        if(result && !result.error){
           await this.refreshList()
        }
        else{
            let mess = 'Thêm thành viên thất bại. Lý do: lỗi hệ thống. \n Ðề xuất: Hãy thử Lưu lại lần nữa.'
            this.showAlert(mess)
        }
    }

    checkList(listEmploy){
        const list = []
        listEmploy.map(item => {
            let itemMail = {name:item.fullName, type:item.typeIC, }
            switch (item.typeIC) {
                case 1:
                    itemMail = {...itemMail, icon:"department",  id:item.id, count:item.count, }
                    break;
                case 2:
                    itemMail = {...itemMail, icon: "job", parentId:item.departmentId, id:item.id, content:item.subName, count:item.count}
                    break;
                case 4:
                    itemMail = {...itemMail, icon: "user", id:item.haraId }
                    break;
                default:itemMail = {...itemMail, icon:"company", content: "Tất cả nhân viên", id:item.orgid}
                    break;
            }
            list.push(itemMail)
        
        })
        return list
    }

    async refreshList(text){
        this.page =1;
        this.totalPage = null;
        this.currentList = []
        await this.loadData(text)
    }

    onSearchBox(text) {
        if(text.length != 0){
            this.setState({isLoad: true, isSearch:true, listData:[], text:text},() =>{
                this.refreshList(text)
            })
        }
        else{
            this.setState({item:null, isLoad: true, isSearch:false, listData:[], isLoading:false},() =>{
                this.refreshList(text)
            })
            
        }
    }

    showAler(mess){
        Alert.alert(
            'Thông báo',
            `${mess}`,
            [
                { text: 'Xác nhận', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: true }
        )
    }

    async joinGroup(){
        const { collegue, body } = this ;
        const {item} = this.state
        const result = await HaravanIc.JoinGroup(item.groupId, false, body)
        if(result && !result.error){
           // Toast.show('Đã rời khỏi nhóm thành công')
            this.props.actions.discuss.refreshList(true)
            this.props.navigation.pop()
        }
        else{
            this.showAler(`Rời nhóm thất bại. \n Lý do: Lỗi hệ thống. \n Đề xuất: Thử lại lần nữa `)
        }
    }

    async removeUser(item){
        let { listData } = this.state;
        const result  = await HaravanIc.RemoveUser(item.groupId, item.id, this.body)
        if(result && !result.error){
            let index = listData.findIndex(e => e.numberId == item.numberId)
            if(index > -1){
                listData.splice(index, 1)
            }
            this.setState({listData},() =>{
              //  Toast.show('Đã xóa thành viên khỏi nhóm')
            })
        }
        else{
            this.showAler(`Xóa thành viên thất bại. \n Lý do: Lỗi hệ thống. \n Đề xuất: Thử lại lần nữa `)
        }
    }

    async roleMember(item, value, role){
        const result = await HaravanIc.RoleMember(item.groupId, item.id, value, this.body)
        if(result && !result.error){
            this.setState({item:{...item, role:role}})
            let mess = role == 1 ? 'Đã chỉ định làm quản trị viên' : 'Đã hủy tư cách quản trị viên'
            // Toast.show(`${mess}`)
        }
        else{
            this.showAler(`Chỉ định thất bại. \n Lý do: Lỗi hệ thống. \n Đề xuất: Thử lại lần nữa `)
        }
    }

    checkOption(item, type){
        switch (type) {
            case 1:
                this.joinGroup()
                break;
            case 2:
                this.removeUser(item)
                break;
            case 3:
                this.roleMember(item, true, 1)
                break;
            case 4:
                this.roleMember(item, false, 2)
                break;
            default:null
                break;
        }
    }

    loadMore(){
        const {totalPage, page } = this
        if(page <= totalPage ){
            this.setState({loadMore:true},() =>{
                this.loadData()
            })
        }
    }

    async loadData(text){
        let { totalPage, page, currentList, url, collegue  } = this
        let value = text ? text : ''
        let newList = currentList
        if(!totalPage || (totalPage && !isNaN(totalPage) && (page < totalPage))){
            const result = await HaravanIc.GetListMember(this.data.id, page, value);
            if(result && result.data && !result.error){
                newList = [...newList, ...result.data.data]
                const totalCaount = result.data.totalCount
                this.currentList = newList
                this.page = page + 1
                this.totalPage = Math.ceil(totalCaount/20)
                this.setState({listData:newList, isLoading:false, isLoad:false, loadMore:false})
            }
            else{
                this.setState({ isLoading:false, isLoad:false, loadMore:false})
            }
        } 
        else{
            this.setState({ isLoading:false, isLoad:false, loadMore:false})
        }
        this.props.actions.discuss.joinGroups(null)
    }

}
const mapStateToProps = (state) => ({ ...state })
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(ListEmploy);