import Layout from './layout';
import { Toast } from '../../../../../public';
import { HaravanIc } from '../../../../../Haravan';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../../state/action';
import { Alert } from 'react-native';
import * as Utils from '../../components/Utils'

class DetailGroups extends Layout{
    constructor(props){
        super(props)
        this.state={
            isShow:false,
            data:null,
            isLoading:false,
            isJoining:false
        }

        this.data = props.navigation.state.params.data;
        this.actionsBack = props.navigation.state.params.actionsBack ? props.navigation.state.params.actionsBack : null
        this.colleague = props.app.colleague;
        this.body = null
    }
    
    async componentDidMount(){
        this.body = await Utils.GetBody(this.colleague)
        if(this.data){
            const result = await HaravanIc.GetDetailGroups(this.data.id, this.body)
            if(result && result.data && !result.error){
                this.setState({data:result.data})
            }
        }
    }

    async componentWillReceiveProps(nextProps){
        if(nextProps && nextProps.discuss && nextProps.discuss.changeItem &&
            (nextProps.discuss.changeItem.id === this.data.id) &&
            (JSON.stringify(nextProps.discuss.changeItem) !== JSON.stringify(this.state.data) )){
            this.setState({data:nextProps.discuss.changeItem}, () =>{
                this.props.actions.discuss.changeItem(null)
            })
        }
        if(nextProps && nextProps.app && nextProps.app.reloadNotify && (nextProps.app.reloadNotify==true)){
            this.data = nextProps.navigation.state.params.data;
            const result = await HaravanIc.GetDetailGroups(this.data.id, this.body)
            if(result && result.data && !result.error){
                this.setState({data:result.data})
            }
        }
    }

    showAlert(mess){
        Alert.alert(
            'Thông báo',
            `${mess}`,
            [
                { text: 'Xác nhận', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: true }
        )
    }

    async checkOption(item, type){
        switch(type){
            case 1:
                this.pinGroup(item, 1)
                break;
            case 2:
                this.joinGroup(false)
                break;
            case 3:
                this.props.navigation.navigate('RemoveGroup',{
                    actions:(value) => this.removeGroup(value)
                })
                break;
            case 4:
                this.pinGroup(item, 4)
                break;
            default:null
                break;
        }
    }

    async joinGroup(value){
        const { colleague, body } = this;
        const { data } = this.state
        const result = await HaravanIc.JoinGroup(data.id, value, colleague)
        if(result && !result.error){
            this.setState({data:{...data, isJoined:value}, refreshing:true, isJoining:false},() =>{
                let mess = value ? 'Tham gia nhóm thành công' : 'Đã rời khỏi nhóm thành công'
                this.props.actions.discuss.changeItem(this.state.data)
                value && this.props.actions.discuss.joinGroups(true)
                !value && this.props.navigation.pop()
            })
        }
        else{
            let messResult = result && result.message ? result.message : 'Lỗi hệ thống'
            let mess = value ? `Tham gia nhóm thất bại. \n Lý do: ${messResult}.`
                :`Rời nhóm thất bại. \n Lý do: Lỗi hệ thống. \n Đề xuất: Thử lại lần nữa` 
            this.setState({isJoining:false},() =>{
                this.showAlert(mess)
            })
        }
    }

    async pinGroup(item, type){
        let {data} = this.state
        if(type == 1){
            const result = await HaravanIc.PinGroup( data.id , true)
            if(result && !result.error){
                this.setState({data:{...data, isPinned:true}},() =>{
                //Toast.show('Ghim nhóm thành công')
                })
            }
            else{
                this.showAlert('Ghim nhóm thất bại')
            }
        }
        else{
            const result = await HaravanIc.PinGroup( data.id, false)
            if(result && !result.error){
                this.setState({data:{...data, isPinned:false}},() =>{
                    //Toast.show('Bỏ ghim nhóm thành công')
                })
            }
            else{
                this.showAlert('Bỏ ghim nhóm thất bại')
            }
        }
    }

    async removeGroup(value){
        const { body } = this
        const { type, note } = value
        let { data }  = this.state
        const result =  await HaravanIc.RemoveGroup(data.id, note, body)
        if(result && !result.error){
            this.setState({isLoading:true},()=> {
                // Toast.show('Đã xóa nhóm thành công')
                this.props.navigation.pop()
                this.props.actions.discuss.refreshList(true)
            })
            
        }
        else{
            Alert.alert(
                'Thông báo',
                `Xóa nhóm thất bại. \n Lý do: Lỗi hệ thống. \n Đề xuất: Thử lại lần nữa `,
                [
                    { text: 'Xác nhận', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: true }
            )
        }
    }

    componentWillUnmount(){
        this.props.actions.discuss.refreshList(null)
        this.props.actions.discuss.changeItem(null)
    }
}
const mapStateToProps = (state) => ({ ...state })
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(DetailGroups);