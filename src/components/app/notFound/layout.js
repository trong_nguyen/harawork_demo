import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { HeaderWapper } from '../../../public';
import { Ionicons } from '@expo/vector-icons';

class Layout extends Component {

    renderHeader() {
        return (
            <HeaderWapper backgroundColor='#ffffff'>
                <TouchableOpacity
                    onPress={() => {
                        if (this.back) {
                            this.back = false;
                            this.props.navigation.pop();
                        }
                    }}
                    style={{ backgroundColor: 'transparent', width: 55, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                    <Ionicons name='ios-arrow-back' size={23} color='#9CA7B2' style={{ marginLeft: -15 }} />
                </TouchableOpacity>
            </HeaderWapper>
        )
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {this.renderHeader()}
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: '#bdc3c7', fontSize: 40, fontWeight: 'bold' }}>
                        404
                    </Text>
                    <Text style={{ color: '#bdc3c7', fontSize: 20 }}>
                        Trang hiện tại chưa được hỗ trợ
                    </Text>
                </View>
            </View>
        )
    }
}

export default Layout;