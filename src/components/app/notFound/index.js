import Layout from './layout';

class NotFound extends Layout {
    constructor(props) {
        super(props);

        this.back = true;
    }


}

export default NotFound;