import Layout from './layout';
import { Keyboard } from 'react-native';
import { HaraBPM } from '../../../../Haravan';

class Comment extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            data: props.navigation.state.params.detail
        }

        _this = this;

    }

    componentDidMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    keyboardDidShow() {
        _this.scrollView.scrollToEnd();
    }

    keyboardDidHide() {

    }

    async comment(value) {
        const { formId } = this.state.data;
        const resultComment = await HaraBPM.postComment(formId, value);
        if (resultComment && resultComment.data && !resultComment.error) {
            this.setState({ data: resultComment.data },
                () => setTimeout(() => this.scrollView.scrollToEnd(), 150));
            this.props.navigation.state.params.actions();
        }
    }

}

export default Comment;