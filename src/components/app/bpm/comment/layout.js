import React, { Component } from 'react';
import { View, Text, KeyboardAvoidingView, ScrollView } from 'react-native';

import Header from './components/header';
import Item from './components/item';
import Input from './components/input';

import { settingApp } from '../../../../public';

class Layout extends Component {

    renderContent() {
        const { logs } = this.state.data;
        return (
            <View style={{ flex: 1 }}>
                <ScrollView contentContainerStyle={{ paddingBottom: 10 }} ref={refs => this.scrollView = refs}>
                    <View style={{ backgroundColor: '#ffffff', padding: 10, ...settingApp.shadow }}>
                        {logs.map((e, i) => {
                            return (
                                <View key={i} style={{ flex: 1, marginBottom: 10 }}>
                                    <Item data={e}/>
                                </View>
                            )
                        })}    
                    </View>
                </ScrollView>
                <Input comment={value => this.comment(value)}/>
            </View>
        )
    }

    render() {
        return (
            <KeyboardAvoidingView behavior='padding' style={{ flex: 1 }}>
                <Header navigation={this.props.navigation} />
                <View style={{ flex: 1 }}>
                    {this.renderContent()}
                </View>
            </KeyboardAvoidingView>
        )
    }
}

export default Layout;