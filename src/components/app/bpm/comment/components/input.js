import React, { Component } from 'react';
import { View, Text, TextInput, Image, TouchableOpacity } from 'react-native';
import { settingApp, imgApp } from '../../../../../public';

class Input extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: ''
        }
    }

    sendMail() {
        const { value } = this.state;
        if (value.length > 0) {
            this.props.comment(value);
            this.setState({ value: '' });
        }
    }

    render() {
        const { colorSperator, color, width } = settingApp;
        return (
            <View style={{
                width,
                flexDirection: 'row',
                height: 50,
                borderColor: colorSperator,
                borderWidth: 1,
                backgroundColor: '#ffffff',
                ...settingApp.shadow
            }}>
                <TextInput
                    autoCorrect={false}
                    value={this.state.value}
                    onChangeText={value => this.setState({ value })}
                    selectionColor={color}
                    placeholder='Nội dung bình luận (dành cho mọi người)'
                    style={{ flex: 1, paddingLeft: 10, paddingRight: 10 }}
                    underlineColorAndroid='transparent'
                    returnKeyType='send'
                    onSubmitEditing={() => this.sendMail()}
                />
                <TouchableOpacity
                    onPress={() => this.sendMail()}
                    style={{ width: 50, height: 48, justifyContent: 'center', alignItems: 'center', }}>
                    <Image
                        source={imgApp.plane}
                        style={{ width: 20, height: 15 }}
                        resizeMode='stretch'
                    />
                </TouchableOpacity>
            </View>
        )
    }
}

export default Input;