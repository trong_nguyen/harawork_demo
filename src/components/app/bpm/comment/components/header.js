import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { HeaderWapper } from '../../../../../public';
import { Ionicons } from '@expo/vector-icons';

class Header extends Component {

    render() {
        return (
            <HeaderWapper style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                <TouchableOpacity
                    onPress={() => this.props.navigation.pop()}
                    style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                    <Ionicons name='ios-arrow-back' size={23} color='#ffffff' />
                </TouchableOpacity>
                <Text style={{ fontSize: 16, color: '#ffffff', fontWeight: 'bold' }}>
                    Bình luận
                </Text>
                <View style={{ width: 40, height: 44 }} />
            </HeaderWapper>
        )
    }
}

export default Header;