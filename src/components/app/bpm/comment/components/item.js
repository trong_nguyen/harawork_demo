import React, { Component } from 'react';
import { ActivityIndicator, View, Text } from 'react-native';
import { settingApp, Utils } from '../../../../../public';
import { HaravanHr } from '../../../../../Haravan';

class Item extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoadEnd: false
        }

        this.detail = null;
    }

    async componentDidMount() {
        let detail = <View />;
        const { createdBy, type, resend } = this.props.data;
        const { color } = settingApp;
        const userCreate = await HaravanHr.getColleague(createdBy);
        if (userCreate && userCreate.data && !userCreate.error) {
            const { fullName, mainPosition } = userCreate.data;
            switch (type) {
                case 'approved':
                    detail = (<Text style={{ fontSize: 14 }}>
                        <Text style={{ color }}>{fullName} - {mainPosition.jobtitleName} </Text>
                        <Text style={{ color: '#5CB85C', fontWeight: 'bold' }}>đã duyệt</Text>
                    </Text>);
                    break;
                case 'completed':
                    detail = (<Text style={{ fontSize: 14 }}>
                        <Text style={{ color }}>{fullName} - {mainPosition.jobtitleName} </Text>
                        <Text style={{ color: '#F0AD4E', fontWeight: 'bold' }}>đã hoàn tất</Text>
                    </Text>);
                    break;
                case 'rejected':
                    detail = (<Text style={{ fontSize: 14 }}>
                        <Text style={{ color }}>{fullName} - {mainPosition.jobtitleName} </Text>
                        <Text style={{ color: '#D9534F', fontWeight: 'bold' }}>đã từ chối</Text>
                    </Text>);
                    break;
                case 'comment':
                    detail = (<Text style={{ fontSize: 14 }}>
                        <Text style={{ color }}>{fullName} - {mainPosition.jobtitleName} </Text>
                        <Text style={{ color: settingApp.colorText, fontWeight: 'bold' }}>đã bình luận</Text>
                    </Text>);
                    break;
                case 'resend':
                    detail = (<Text style={{ fontSize: 14 }}>
                        <Text style={{ color }}>{fullName} - {mainPosition.jobtitleName} </Text>
                        <Text style={{ color: settingApp.colorText, fontWeight: 'bold' }}>đã gửi lại </Text>
                        <Text>Vòng # {resend.actionName}</Text>
                    </Text>);
                    break;
                default:
                    detail = (<Text style={{ fontSize: 14 }}>
                        <Text style={{ color }}>{fullName} - {mainPosition.jobtitleName} </Text>
                        <Text style={{ color: settingApp.colorText, fontWeight: 'bold' }}>đã tạo phiếu yêu cầu</Text>
                    </Text>);
                    break;
            }

            this.detail = detail;
            this.setState({ isLoadEnd: true })
        }
    }

    colorStatus(type) {
        let color = '';
        switch (type) {
            case 'approved':
                color = '#5CB85C';
                break;
            case 'completed':
                color = '#F0AD4E';
                break;
            case 'rejected':
                color = '#D9534F';
                break;
            case 'comment':
                color = '#DAE3EA';
                break;
            case 'resend':
                color = '#0279C7';
                break;
            default:
                color = '#DAE3EA';
                break;
        }
        return color;
    }

    render() {
        const { comment, createdAt, type } = this.props.data
        const isComment = !!(comment && comment.content && comment.content.length > 0);
        const time = Utils.formatTime(createdAt, 'L, hh:mm A');
        const color = this.colorStatus(type);
        return (
            <View style={{ flex: 1, flexDirection: 'row', paddingRight: 20 }}>
                <View style={{ marginRight: 15, alignItems: 'center' }}>
                    <View style={{ width: 12, height: 12, borderRadius: 12, backgroundColor: color, marginTop: 3 }} />
                    <View style={{ flex: 1, width: 1, marginTop: 10, backgroundColor: '#DAE3EA' }} />
                </View>
                <View style={{ flex: 1, paddingBottom: 10 }}>
                    <View>
                        <Text style={{ fontSize: 12, color: '#9CA7B2' }}>{time}</Text>
                    </View>
                    <View style={{ marginTop: 5 }}>
                        {this.state.isLoadEnd ? this.detail : <ActivityIndicator size='small' color={settingApp.color} style={{ height: 30 }} />}
                    </View>
                    {isComment && <View>
                        <View style={styles.cover}>
                            <View style={styles.triangle} />
                        </View>
                        <View style={{ flex: 1, backgroundColor: '#DADADA', borderRadius: 3, padding: 10 }}>
                            <Text>{comment.content}</Text>
                        </View>
                    </View>}
                </View>
            </View>
        )
    }
}

const styles = {
    cover: {
        backgroundColor: 'transparent',
        height: 23,
        width: 23,
        overflow: "hidden",
        marginLeft: 10,
        marginTop: -10
    },
    triangle: {
        width: 23,
        height: 23,
        backgroundColor: '#DADADA',
        transform: [{ rotate: '45deg' }],
        borderRadius: 2.5,
        marginTop: 20
    },
}

export default Item;