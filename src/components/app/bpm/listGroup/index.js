import Layout from './layout';
import { HaraBPM } from '../../../../Haravan';

class ListGroup extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            group: [],
            isLoading: true,
            onLoadMore: false,
            refreshing: true,
            idGroup: null
        }

        this.page = 1;
        this.totalPage = null;

        this.api = props.api;
        this.keyStore = props.keyStore;
        this.colleague = props.propsApp.app.colleague;

        this.departmentListName = [];
        this.prvList = [];
        
    }

    componentDidMount() {
        this.loadGroup();
    }

    async loadGroup() {
        const { api } = this;
        const group = await HaraBPM.getGroup(api);
        if (group && group.data && !group.error) {
            const { data } = group;
            this.setState({ group: data, isLoading: false });
        }
    }

}

export default ListGroup;