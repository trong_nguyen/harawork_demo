import React, { Component } from 'react';
import { Animated, View, Text } from 'react-native';

import { settingApp, Loading, Utils } from '../../../../../public';

import { HaraBPM, HaravanHr } from '../../../../../Haravan';

import Item from '../../components/item';

import moment from 'moment';

class List extends Component {
    constructor(props) {
        super(props);

        this.state = {
            list: [],
            end: false,
            isLoading: true,
            isComplete: false
        }

        this.id = props.id;
        this.api = props.propsGroup.api;

        this.page = 1;
        this.totalPage = null;

        this.departmentListName = [];
        this.prvList = [];

        this.end = false;
        this.setting = null;

        this.isMount = true;
    }

    async componentDidMount() {
        const setting = await HaraBPM.getSetting();
        if (setting && setting.data && !setting.error) {
            this.setting = setting.data[0];
        }
        while (!this.state.end) {
            if (this.isMount) {
                await this.loadData();
            } else {
                break;
            }
        }
        const value = this.prvList.length * 84;
        if (this.isMount) {
            await this.props.heightGroup(value > 0 ? value : 84);
            await this.setState({
                list: this.prvList,
                isLoading: false,
                isComplete: true
            });
        }
    }

    async loadData() {
        const { id, api, page, totalPage, departmentListName, prvList } = this;
        const isValidate = !!((!totalPage && totalPage !== 0) || (totalPage && totalPage > 0 && page <= totalPage));
        if (isValidate) {
            const reulst = await HaraBPM.getDetailGroup(id, api, page);
            if (reulst && reulst.data && !reulst.error) {
                const { data, totalCount } = reulst.data;
                let newList = [];
                for (const item of data) {
                    let createUserName = '';
                    const { createdBy } = item;
                    const itemIndex = departmentListName.findIndex(e => e.key === createdBy);
                    if (itemIndex > -1) {
                        createUserName = departmentListName[itemIndex].fullName;
                    } else {
                        createUserName = await this.getdepartmentListName(createdBy);
                    }

                    const info = this.checkInfoStatus(item);

                    newList.push({ ...item, createUserName, info });
                }

                newList = [...prvList, ...newList];
                this.prvList = newList;

                if (!totalPage) {
                    this.totalPage = Math.ceil(totalCount / 20);
                }

                this.page = this.page + 1;
            }
        } else {
            if (this.isMount) {
                this.setState({
                    end: true,
                    isLoading: false
                })
            }
        }
    }

    checkInfoStatus(item) {
        const { approveds, status, createdAt } = item;
        let search = approveds.map(e => e.actions);
        let info = {};
        let index = -1;
        switch (status) {
            case 2:
                index = search.findIndex(e => e[0].isWaiting === true);
                if (index > -1) {
                    if (search[index][0].type === 1) {
                        info.color = '#0279C7';
                        info.borderLeftColor = '#5CB85C';
                    } else {
                        info.color = '#F0AD4E';
                        info.borderLeftColor = '#F0AD4E';
                    }
                    info.name = search[index][0].name;
                    let time = createdAt;
                    if (search[index][0].approved && search[index][0].approved.approvedAt) {
                        time = search[index][0].approved.approvedAt;
                    }
                    const slaTime = this.setting && this.setting.slaTime ? this.setting.slaTime : 0;
                    time = moment(time).add(slaTime, 'hours');
                    info.time = `ETA: ${Utils.formatTime(time, 'L hh:mm A')}`;
                }
                break;
            case 3:
                index = search.findIndex(e => e[0].isLastValid === true && e[0].isValidCondition === true);
                if (index > -1) {
                    info.color = '#5CB85C';
                    info.name = search[index][0].name;
                    info.borderLeftColor = '#5CB85C';
                    info.time = Utils.formatTime(createdAt, 'L hh:mm A');
                } else {
                    info.color = '#9CA7B2';
                    info.name = 'Tạo yêu cầu';
                    info.borderLeftColor = '#9CA7B2';
                    info.time = Utils.formatTime(createdAt, 'L hh:mm A');
                }
                break;
            case 4:
                index = search.findIndex(e => e[0].isReject === true);
                if (index > -1) {
                    info.color = '#D9534F';
                    info.name = search[index][0].name;
                    info.borderLeftColor = '#D9534F';
                    info.time = Utils.formatTime(createdAt, 'L hh:mm A');
                }
                break;
            default:
                info.color = '#9CA7B2';
                info.name = 'Tạo yêu cầu';
                info.borderLeftColor = '#9CA7B2';
                info.time = Utils.formatTime(createdAt, 'L hh:mm A');
                break;
        }

        return info;
    }

    async getdepartmentListName(id) {
        let { departmentListName } = this;
        const result = await HaravanHr.getColleague(id);
        if (result && result.data && !result.error) {
            const { data } = result;
            departmentListName.push({ key: id, fullName: data.fullName });
            this.departmentListName = departmentListName;
            return data.fullName;
        } else {
            return '';
        }
    }

    renderListEmty() {
        return (
            <View style={{ flex: 1, height: 110, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: settingApp.colorText, fontSize: 14 }}>Không tìm thấy dữ liệu</Text>
            </View>
        )
    }

    render() {
        const { list, isLoading, end, isComplete } = this.state;
        if (isLoading || !isComplete) {
            return (
                <View style={{ flex: 1, height: 84 }}>
                    <Loading />
                </View>
            )
        } else {
            if (list.length === 0 && end) {
                return this.renderListEmty();
            } else if (isComplete) {
                return (
                    <View style={{ flex: 1 }}>
                        {this.state.list.map((item, index) => {
                            return (
                                <View style={{ flex: 1 }} key={index}>
                                    <Item obj={{ item, index }} navigation={this.props.propsGroup.propsApp.navigation} />
                                </View>
                            )
                        })}
                    </View>
                )
            }
        }
    }

    componentWillUnmount() {
        this.isMount = false;
    }
}

export default List;