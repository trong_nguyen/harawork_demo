import React, { Component } from 'react';
import { Animated, View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { settingApp } from '../../../../../public';
import { MaterialIcons } from '@expo/vector-icons';
import Collapsible from 'react-native-collapsible';
import List from './list';

class Group extends Component {
    constructor(props) {
        super(props);

        this.state = {
            height: 84,
            rotateAnimation: new Animated.Value(0),
            isOpen: false
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.idGroup !== this.props.obj.item.id && this.state.isOpen) {
            this.setState({ isOpen: false });
            Animated.timing(
                this.state.rotateAnimation,
                {
                    toValue: 0,
                    duration: 150,
                    useNativeDriver: true
                }
            ).start();
        }
    }

    openGroup(id) {
        this.setState({ isOpen: !this.state.isOpen });
        Animated.timing(
            this.state.rotateAnimation,
            {
                toValue: this.state.isOpen ? 0 : 1,
                duration: 150,
                useNativeDriver: true
            }
        ).start();
        this.props.checkShowGroup(id);
    }


    render() {
        const { item, index } = this.props.obj;
        const borderTopWidth = index === 0 ? 0 : 1;
        const rotateProp = this.state.rotateAnimation.interpolate({
            inputRange: [0, 1],
            outputRange: ["0deg", "90deg"]
        });
        return (
            <View>
                <TouchableOpacity
                    onPress={() => this.openGroup(item.id)}
                    style={{
                        flex: 1,
                        backgroundColor: '#DAE3EA',
                        padding: 20,
                        borderTopColor: '#ffffff',
                        borderTopWidth,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        ...settingApp.shadow
                    }}>
                    <Text
                        style={{
                            color: settingApp.colorText,
                            fontSize: 14,
                            fontWeight: 'bold',
                            maxWidth: (settingApp.width * 0.7)
                        }}
                        numberOfLines={1}
                    >
                        {item.title}
                    </Text>
                    <Animated.View style={{
                        transform: [
                            {
                                rotate: rotateProp
                            }
                        ]
                    }}>
                        <MaterialIcons name='keyboard-arrow-right' color='#9CA7B2' size={23} />
                    </Animated.View>
                </TouchableOpacity>
                <Collapsible collapsed={!this.state.isOpen} style={{ height: this.state.height }}>
                    <ScrollView scrollEnabled={false}>
                        <List
                            id={item.formBuilderId}
                            heightGroup={async height => await this.setState({ height })}
                            propsGroup={this.props.propsGroup}
                        />
                    </ScrollView>
                </Collapsible>
            </View>
        )
    }
}

export default Group;