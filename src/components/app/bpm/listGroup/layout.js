import React, { Component } from 'react';
import { View, Text, ScrollView, Image, TouchableOpacity } from 'react-native';
import { Loading, settingApp, imgApp } from '../../../../public';
import Group from './components/group'

class Layout extends Component {

    renderContent() {
        const { group } = this.state;
        const { color } = settingApp;
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1, height: 55, justifyContent: 'center', alignItems: 'flex-end', paddingRight: 10 }}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('List')}
                        style={{ flexDirection: 'row' }}
                    >
                        <Image
                            source={imgApp.row}
                            style={{ width: 20, height: 20, marginRight: 10 }}
                            resizeMode='stretch'
                        />
                        <Text style={{ color: color, fontSize: 14 }}>
                            Hiển thị từng dòng
                        </Text>
                    </TouchableOpacity>
                </View>
                {group.map((item, index) => (
                    <View key={item.id}>
                        <Group
                            obj={{ item, index }}
                            idGroup={this.state.idGroup}
                            propsGroup={this.props}
                            checkShowGroup={idGroup => this.setState({ idGroup })}
                        />
                    </View>
                ))}
            </View>
        )
    }

    render() {
        const { isLoading } = this.state;
        if (isLoading) {
            return (
                <View style={{ flex: 1 }}>
                    <Loading />
                </View>
            )
        }
        return (
            <ScrollView>
                {this.renderContent()}
            </ScrollView>
        )
    }
}

export default Layout;