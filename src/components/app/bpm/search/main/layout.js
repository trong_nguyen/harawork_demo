import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Header from '../components/header';
import BPMTabSearch from '../../../../../route/components/bpm/tab';

class Layout extends Component {

    render() {
        const listURL = []
        return (
            <View style={{ flex: 1 }}>
                <Header navigation={this.props.navigation} />
                <BPMTabSearch screenProps={{ maninNavigation: this.props.navigation }}/>
            </View>
        )
    }
}

export default Layout;