import Layout from './layout';
import { HaravanHr } from '../../../../../Haravan';

class TabSearch extends Layout {
    constructor(props) {
        super(props);

        const dataStatus = [
            { title: 'Tất cả', value: '/search' },
            { title: 'Chờ xét duyệt', value: '/search?status=2' },
            { title: 'Đã xét duyệt', value: '/search?status=3' },
            { title: 'Từ chối', value: '/search?status=4' },
        ]

        this.state = {
            indexUnit: null,
            indexStatus: null,
            isLoadEnd: false
        }
        
        this.dataUnit = [];
        this.dataStatus = dataStatus;

        const date = new Date();
        this.formDate = date;
        this.toDate = date;

        this.loadEnd = false;
    }

    async componentDidMount() {
        let page = 0;
        while (!this.loadEnd) {
            page = page + 1;
            await this.loadData(page);
        }
        let dataUnit = this.dataUnit.sort((a, b) => a.id - b.id);
        dataUnit = dataUnit.map(e => ({ title: e.name, value: { ...e } }));
        this.dataUnit = dataUnit;
        this.setState({ isLoadEnd: true });
    }

    async loadData(page) {
        const result = await HaravanHr.getAllDepartments(page);
        if (result && result.data && !result.error) {
            const { data, totalCount } = result.data;
            dataUnit = [...this.dataUnit, ...data];
            if (dataUnit.length >= totalCount) {
                this.loadEnd = true;
            }
            this.dataUnit = dataUnit;
        }
    }

    validate(type, value) {
        if (type === 'fromDate') {
            if (!!(value <= this.toDate)) {
                this.formDate = value;
                return { accpect: true, value: null };
            } else {
                return { accpect: false, value: this.toDate };
            }
        } else {
            if (!!(value >= this.formDate)) {
                this.toDate = value;
                return { accpect: true, value: null };
            } else {
                return { accpect: false, value: this.formDate };
            }
        }
    }

}

export default TabSearch;