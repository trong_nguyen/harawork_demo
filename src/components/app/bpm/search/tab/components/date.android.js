import React, { Component } from 'react';
import { View, Text, DatePickerAndroid, TouchableOpacity, Image } from 'react-native';
import { imgApp, settingApp, Utils } from '../../../../../../public';

class DatePicker extends Component {
    constructor(props) {
        super(props);

        this.state = {
            date: Utils.formatTime(new Date(), 'DD/MM/YYYY', true),
            chosenDate: new Date()
        }

    }

    submit(valueDate) {
        const result = this.props.validate(valueDate);
        const { accpect, value } = result;
        if (accpect) {
            this.setState({
                date: Utils.formatTime(valueDate, 'DD/MM/YYYY', true),
                chosenDate: valueDate
            });
        } else {
            this.setState({
                date: Utils.formatTime(value, 'DD/MM/YYYY', true),
                chosenDate: value
            });
        }
    }

    async openCalendar() {
        try {
            const { action, year, month, day } = await DatePickerAndroid.open({
                date: new Date(this.state.chosenDate)
            });
            if (action !== DatePickerAndroid.dismissedAction) {
                const valueDate = new Date(`${month + 1}/${day}/${year}`);
                this.submit(valueDate);
            }
        } catch ({ code, message }) {
            console.warn('Cannot open date picker', message);
            this.props.close();
        }
    }


    render() {
        const { title } = this.props;
        const { date } = this.state;
        const { colorText, colorSperator } = settingApp;
        return (
            <View>
                <TouchableOpacity
                    onPress={() => this.openCalendar()}
                    activeOpacity={1}>
                    <Text style={{ color: colorText, fontSize: 12, fontWeight: 'bold' }}>{title}</Text>
                    <View style={{ flexDirection: 'row', marginTop: 10, height: 50, alignItems: 'center', justifyContent: 'space-between', paddingLeft: 10, paddingRight: 10, borderColor: colorSperator, borderWidth: 1, borderRadius: 3 }}>
                        <Text style={{ color: colorText, fontSize: 16 }}>{date}</Text>
                        <Image
                            source={imgApp.calendar}
                            style={{ width: 20, height: 20 }}
                            resizeMode='stretch'
                        />
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

export default DatePicker;