import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, DatePickerIOS } from 'react-native';
import { settingApp, imgApp, Utils } from '../../../../../../public';

import Modal from "react-native-modal";

class DatePicker extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isVisible: false,
            date: Utils.formatTime(props.value, 'DD/MM/YYYY', true),
            chosenDate: props.value
        }

        this.date = props.value;
    }

    setDate(newDate) {
        this.date = newDate;
    }

    submit() {
        const result = this.props.validate(this.date);
        const { accpect, value } = result;
        if (accpect) {
            this.setState({
                isVisible: false,
                date: Utils.formatTime(this.date, 'DD/MM/YYYY', true),
                chosenDate: this.date
            });
        } else {
            this.date = value;
            this.setState({
                isVisible: false,
                date: Utils.formatTime(this.date, 'DD/MM/YYYY', true),
                chosenDate: this.date
            });
        }
    }

    cancel() {
        this.date = this.state.chosenDate;
        this.setState({ isVisible: false })
    }

    render() {
        const { title } = this.props;
        const { color, colorText, colorSperator, width } = settingApp;
        const { date, isVisible, chosenDate } = this.state;
        return (
            <View>
                <TouchableOpacity
                    onPress={() => this.setState({ isVisible: true })}
                    activeOpacity={1}>
                    <Text style={{ color: colorText, fontSize: 12, fontWeight: 'bold' }}>{title}</Text>
                    <View style={{ flexDirection: 'row', marginTop: 10, height: 50, alignItems: 'center', justifyContent: 'space-between', paddingLeft: 10, paddingRight: 10, borderColor: colorSperator, borderWidth: 1, borderRadius: 3 }}>
                        <Text style={{ color: colorText, fontSize: 16 }}>{date}</Text>
                        <Image
                            source={imgApp.calendar}
                            style={{ width: 20, height: 20 }}
                            resizeMode='stretch'
                        />
                    </View>
                </TouchableOpacity>

                <Modal isVisible={isVisible} style={{ flex: 1 }}>
                    <TouchableOpacity
                        onPress={() => this.cancel()}
                        activeOpacity={1}
                        style={{ flex: 1, marginLeft: -20, marginTop: -20, marginRight: -20, backgroundColor: 'transparent' }}
                    />
                    <View style={{ width, marginLeft: -20, marginBottom: -20, backgroundColor: '#ffffff' }}>
                        <View style={{
                            width,
                            height: 40,
                            alignItems: 'flex-end',
                            backgroundColor: '#ecf0f1',
                            borderTopColor: colorSperator,
                            borderTopWidth: 1,
                            borderBottomColor: colorSperator,
                            borderBottomWidth: 1,
                            paddingRight: 10
                        }}>
                            <TouchableOpacity
                                onPress={() => this.submit()}
                                style={{ width: 50, height: 40, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color, fontSize: 14, fontWeight: 'bold' }}>XONG</Text>
                            </TouchableOpacity>
                        </View>
                        <DatePickerIOS
                            date={chosenDate}
                            mode='date'
                            onDateChange={newDate => this.setDate(newDate)}
                        />
                    </View>
                </Modal>
            </View>
        )
    }
}

export default DatePicker;