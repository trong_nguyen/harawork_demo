import React, { Component } from 'react';
import { ActivityIndicator, View, Text, ScrollView, TextInput, TouchableOpacity } from 'react-native';
import { settingApp } from '../../../../../public';
import { Ionicons } from '@expo/vector-icons';

import DatePicker from './components/date';

class Layout extends Component {

    renderSelect() {
        const { indexUnit, indexStatus, isLoadEnd } = this.state;
        let unitSelect = 'Vui lòng chọn';
        if (indexUnit && indexUnit.length > 0) {
            unitSelect = indexUnit.map(e => this.dataUnit[e].title);
            unitSelect = unitSelect.reduce((a, b) => a + ', ' + b);
        }
        const statusSelect = (indexStatus !== null && indexStatus) ? this.dataStatus[indexStatus].title : 'Vui lòng chọn';
        const colorName = '#212121';
        const colorValue = '#8E8E93';
        const { colorSperator, color } = settingApp;
        return (
            <View style={{ flex: 1 }}>
                <View style={{ height: 40, flexDirection: 'row', borderBottomColor: colorSperator, borderBottomWidth: 1 }}>
                    <View style={{ flex: 1, justifyContent: 'center', marginRight: 5 }}>
                        <Text style={{ fontSize: 14, color: colorName }}>TRẠNG THÁI</Text>
                    </View>
                    <TouchableOpacity
                        onPress={
                            () => this.props.maniNavigation.navigate(
                                'BPMSearchSelectItem',
                                {
                                    titleHeader: 'Trạng thái',
                                    data: this.dataStatus,
                                    selectIndex: indexStatus,
                                    select: indexStatus => this.setState({ indexStatus })
                                })
                        }
                        style={{ flex: 1, flexDirection: 'row', alignItems: 'center', marginLeft: 5 }}>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 10 }}>
                            <Text style={{ fontSize: 14, color: colorValue }} numberOfLines={1}>{statusSelect}</Text>
                        </View>
                        <Ionicons name='ios-arrow-forward' color={colorSperator} size={23} />
                    </TouchableOpacity>
                </View>

                <View style={{ height: 40, flexDirection: 'row', marginTop: 10, borderBottomColor: colorSperator, borderBottomWidth: 1 }}>
                    <View style={{ flex: 1, justifyContent: 'center', marginRight: 5 }}>
                        <Text style={{ fontSize: 14, color: colorName }}>ĐƠN VỊ</Text>
                    </View>
                    {// BPMSearchSelectMutiItem
                        isLoadEnd ?
                            <TouchableOpacity
                                onPress={
                                    () => this.props.maniNavigation.navigate(
                                        'BPMSearchSelectMutiItem',
                                        {
                                            titleHeader: 'Đơn vị',
                                            data: this.dataUnit,
                                            selectIndex: indexUnit,
                                            select: indexUnit => this.setState({ indexUnit })
                                        })
                                }
                                style={{ flex: 1, flexDirection: 'row', alignItems: 'center', marginLeft: 5 }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 10 }}>
                                    <Text style={{ fontSize: 14, color: colorValue }} numberOfLines={1}>{unitSelect}</Text>
                                </View>
                                <Ionicons name='ios-arrow-forward' color={colorSperator} size={23} />
                            </TouchableOpacity>
                            :
                            <ActivityIndicator size='small' color={color} style={{ flex: 1, marginLeft: 5 }} />
                    }
                </View>
            </View>
        )
    }

    render() {
        const { formDate, toDate } = this;
        const { colorSperator, color } = settingApp;
        return (
            <View style={{ flex: 1 }}>
                <ScrollView contentContainerStyle={{ ...settingApp.shadow }}>
                    <View style={{ flex: 1, backgroundColor: '#ffffff', padding: 10 }}>
                        <View style={{ flex: 1, height: 40 }}>
                            <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Ionicons name='md-close' color='#FF3B30' size={30} />
                                <Text style={{ fontSize: 16, color: '#FF3B30', marginLeft: 16 }}>Đặt lại bộ lọc</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ height: 50 }}>
                            <TextInput
                                autoCorrect={false}
                                selectionColor={color}
                                autoCapitalize='none'
                                placeholder='Từ khóa'
                                style={{ flex: 1, padding: 10, borderColor: colorSperator, borderWidth: 1, borderRadius: 3 }}
                                underlineColorAndroid='transparent'
                            />
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', marginTop: 10 }}>
                            <View style={{ flex: 1, marginRight: 5 }}>
                                <DatePicker
                                    title='TỪ NGÀY'
                                    value={formDate}
                                    validate={value => this.validate('fromDate', value)}
                                />
                            </View>
                            <View style={{ flex: 1, marginLeft: 5 }}>
                                <DatePicker
                                    title='ĐẾN NGÀY'
                                    value={toDate}
                                    validate={value => this.validate('toDate', value)}
                                />
                            </View>
                        </View>
                        <View style={{ marginTop: 10 }}>
                            {this.renderSelect()}
                        </View>
                        <TouchableOpacity
                            style={{ flex: 1, height: 55, backgroundColor: color, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}
                        >
                            <Text style={{ fontSize: 16, color: '#ffffff' }}>Áp dụng</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

export default Layout;