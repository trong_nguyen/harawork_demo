import Layout from './layout';

class SelectItem extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            selectIndex: props.navigation.state.params.selectIndex
        }

        this.headerTitle = props.navigation.state.params.headerTitle;
        this.data = props.navigation.state.params.data
    }

    selected(index) {
        if(index !== null && !isNaN(index)){
            this.props.navigation.state.params.select(index);
            this.props.navigation.pop();
        }
    }
    
}

export default SelectItem;