import React, { Component } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import { HeaderWapper, settingApp } from '../../../../../public';
import { Ionicons } from '@expo/vector-icons';

class Layout extends Component {

    renderHeader() {
        const { navigation } = this.props;
        return (
            <HeaderWapper style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                <TouchableOpacity
                    onPress={() => navigation.pop()}
                    style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                    <Ionicons name='ios-arrow-back' size={23} color='#ffffff' />
                </TouchableOpacity>
                <Text style={{ fontSize: 16, color: '#ffffff', fontWeight: 'bold' }}>
                    {navigation.state.params.titleHeader}
                </Text>
                <View style={{ width: 50, height: 44, backgroundColor: 'transparent' }} />
            </HeaderWapper>
        )
    }

    renderItem(obj) {
        const { item, index } = obj;
        const { colorSperator, color } = settingApp;
        const checkIndex = !!(this.state.selectIndex === index);
        return (
            <TouchableOpacity
                onPress={() => this.selected(index)}
                style={{ flex: 1, height: 50, backgroundColor: '#ffffff', paddingLeft: 15 }}>
                <View style={{ flex: 1, paddingTop: 5, paddingBottom: 5, borderTopColor: colorSperator, borderTopWidth: index === 0 ? 0 : 1 }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 0.1, borderRightColor: colorSperator, borderRightWidth: 1, justifyContent: 'center' }}>
                            {checkIndex && <Ionicons name='ios-checkmark' size={30} color={color} />}
                        </View>
                        <View style={{ flex: 1, justifyContent: 'center', paddingLeft: 15 }}>
                            <Text style={{ color: '#212121', fontSize: 16 }}>{item.title}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    renderContent() {
        return (
            <FlatList
                style={{ flex: 1, paddingTop: 10, ...settingApp.shadow }}
                data={this.data}
                extraData={this.state}
                keyExtractor={(item, index) => `${index}`}
                renderItem={obj => this.renderItem(obj)}
            />
        )
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {this.renderHeader()}
                <View style={{ flex: 1 }}>
                    {this.renderContent()}
                </View>
            </View>
        )
    }
}

export default Layout;