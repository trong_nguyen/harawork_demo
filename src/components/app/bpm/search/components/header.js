import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { HeaderWapper } from '../../../../../public';
import { Ionicons} from '@expo/vector-icons';

class Header extends PureComponent {
    render() {
        const { navigation } = this.props;
        return (
            <HeaderWapper style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                <TouchableOpacity
                    onPress={() => navigation.pop()}
                    style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                    <Ionicons name='ios-arrow-back' size={23} color='#ffffff' />
                </TouchableOpacity>
                <Text style={{ fontSize: 16, color: '#ffffff', fontWeight: 'bold' }}>
                    Tim kiếm
                </Text>
                <View style={{ width: 50, height: 44, backgroundColor: 'transparent' }} />
            </HeaderWapper>
        )
    }
}

export default Header;