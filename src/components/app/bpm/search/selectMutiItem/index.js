import Layout from './layout';

class SelectMutiItem extends Layout {
    constructor(props) {
        super(props);

        const { selectIndex } = props.navigation.state.params;
        this.state = {
            selectIndex: selectIndex ? [...selectIndex] : [],
            isSubmit: false
        }

        this.initialSelectItem = selectIndex ? [...selectIndex] : [];
        this.headerTitle = props.navigation.state.params.headerTitle;
        this.data = props.navigation.state.params.data
    }

    selected(index) {
        const { selectIndex } = this.state;
        const checkIndex = selectIndex.indexOf(index);
        if (checkIndex > -1) {
            selectIndex.splice(checkIndex, 1);
        } else {
            selectIndex.push(index);
        }

        this.check(selectIndex);
        this.setState({ selectIndex });
    }

    check(selectIndex) {
        let { initialSelectItem } = this;
        selectIndex = selectIndex.sort((a,b) => a - b);
        initialSelectItem = initialSelectItem.sort((a,b) => a - b);
        if (JSON.stringify(initialSelectItem) != JSON.stringify(selectIndex)) {
            this.setState({ isSubmit: true });
        } else {
            this.setState({ isSubmit: false });
        }
    }

    submit() {
        this.props.navigation.state.params.select(this.state.selectIndex);
        this.props.navigation.pop();
    }

}

export default SelectMutiItem;