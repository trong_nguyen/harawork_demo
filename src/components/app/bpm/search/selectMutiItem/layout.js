import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { settingApp, HeaderWapper } from '../../../../../public';
import { Ionicons } from '@expo/vector-icons';

class Layout extends Component {

    renderHeader() {
        const { navigation } = this.props;
        return (
            <HeaderWapper style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                <TouchableOpacity
                    onPress={() => navigation.pop()}
                    style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                    <Ionicons name='ios-arrow-back' size={23} color='#ffffff' />
                </TouchableOpacity>
                <Text style={{ fontSize: 16, color: '#ffffff', fontWeight: 'bold' }}>
                    {navigation.state.params.titleHeader}
                </Text>
                <View style={{ width: 50, height: 44, backgroundColor: 'transparent' }}>
                   {this.state.isSubmit && <TouchableOpacity
                        onPress={() => this.submit()}
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: 'transparent'
                        }}>
                        <Ionicons name='ios-checkmark' color='#ffffff' size={40}/>
                    </TouchableOpacity>}
                </View>
            </HeaderWapper>
        )
    }

    renderItem(obj) {
        const { item, index } = obj;
        const { colorSperator, color } = settingApp;
        const { selectIndex } = this.state;
        const checkIndex = (selectIndex.indexOf(index) > -1);
        return (
            <TouchableOpacity
                key={index}
                onPress={() => this.selected(index)}
                style={{ flex: 1, height: 50, backgroundColor: '#ffffff', paddingLeft: 15 }}>
                <View style={{ flex: 1, paddingTop: 5, paddingBottom: 5, borderTopColor: colorSperator, borderTopWidth: index === 0 ? 0 : 1 }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 0.1, borderRightColor: colorSperator, borderRightWidth: 1, justifyContent: 'center' }}>
                            {checkIndex && <Ionicons name='ios-checkmark' size={30} color={color} />}
                        </View>
                        <View style={{ flex: 1, justifyContent: 'center', paddingLeft: 15 }}>
                            <Text style={{ color: '#212121', fontSize: 16 }}>{item.title}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    renderContent() {
        const { data } = this;
        return (
            <ScrollView style={{ paddingTop: 15 }}>
                <View style={{ flex: 1, ...settingApp.shadow }}>
                    {data.map((item, index) => this.renderItem({ item, index }))}
                </View>
                <View style={{ flex: 1, height: 35, backgroundColor: 'transparent' }} />
            </ScrollView>
        )
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {this.renderHeader()}
                <View style={{ flex: 1 }}>
                    {this.renderContent()}
                </View>
            </View>
        )
    }
}

export default Layout;