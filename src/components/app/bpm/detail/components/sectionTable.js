import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { settingApp } from '../../../../../public';

class SectionTable extends Component {

    render() {
        const { data } = this.props;
        if (data && data.hasOwnProperty('length') && data.length > 0) {
            const lastRow = (data.length - 1);
            let isShowTotal = false;
            data[lastRow].map(e => {
                if ((typeof (e.value) === 'string') && e.value.hasOwnProperty('length') && e.value.length > 0) {
                    isShowTotal = true;
                }
            })
            return (
                <View style={{ flex: 1 }}>
                    {data.map((item, index) => {
                        if ((index < lastRow) || ((index === lastRow) && isShowTotal)) {
                            return (
                                <View style={{ flex: 1 }} key={index}>
                                    <View style={{
                                        padding: 10,
                                        borderBottomColor: settingApp.colorSperator,
                                        borderBottomWidth: 1,
                                        borderTopColor: settingApp.colorSperator,
                                        borderTopWidth: index > 0 ? 1 : 0
                                    }}>
                                        <Text style={{ fontSize: 14, color: settingApp.colorText, fontWeight: 'bold' }}>
                                            {index === lastRow ? `Tổng cộng` : `Dòng ${(index + 1)}`}
                                        </Text>
                                    </View>
                                    <View style={{ padding: 10 }}>
                                        {
                                            item.map((e, i) => {
                                                let { title } = e;
                                                title = title && title.hasOwnProperty('length') ? title.indexOf('table_section') > -1 ? title.substr(title.indexOf('-') + 2) : '' : '';
                                                return (
                                                    <View style={{ flex: 1, marginTop: (i === 0) ? 0 : 10 }} key={i}>
                                                        <Text style={{ fontSize: 12, color: '#9CA7B2', fontWeight: '500', lineHeight: 22 }}>{title}</Text>
                                                        <Text style={{ fontSize: 14, color: '#474747' }}>{e.value}</Text>
                                                    </View>
                                                )
                                            })
                                        }
                                    </View>
                                </View>
                            )
                        }
                        return <View key={index}/>;
                    })}
                </View>
            )
        }
        return <View />
    }
}

const styles = {
    header: {
        flex: 1,
        flexDirection: 'row',
        height: 50,
        paddingLeft: 10,
        paddingRight: 10,
        alignItems: 'center',
        borderRadius: 3
    }
}

export default SectionTable;