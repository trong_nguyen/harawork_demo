import React, { Component } from 'react';
import { ActivityIndicator, View, Text } from 'react-native';

import { HaraBPM, HaravanHr } from '../../../../../Haravan';

import { settingApp, Utils } from '../../../../../public';

import { MaterialIcons, Ionicons } from '@expo/vector-icons';

import moment from 'moment';

class StatusForm extends Component {
    constructor(props) {
        super(props);

        const infoStaus = this.getInfoStatus(props.detail);

        this.state = {
            isLoadingInfo: true,
            infoStaus,
            infoUser: '',
            time: '',
        }
    }

    checkData(input) {
        if (input && input.data && !input.error) {
            return true
        }
        return false;
    }

    async componentDidMount() {
        const { isETA, userId } = this.state.infoStaus;
        const setting = await HaraBPM.getSetting();
        const userInfo = await HaravanHr.getColleague(userId);
        if (this.checkData(setting) && this.checkData(userInfo)) {
            const infoUser = `${userInfo.data.fullName} - ${userInfo.data.mainPosition.jobtitleName}`;
            let time = this.props.detail.createdAt;
            if (isETA) {
                const { slaTime } = setting.data[0];
                time = moment(time).add(slaTime, 'hours');
                time = `ETA: ${Utils.formatTime(time, 'L hh:mm A')}`;
            } else {
                time = Utils.formatTime(time, 'L hh:mm A');
            }

            this.setState({
                infoUser,
                time,
                isLoadingInfo: false
            })
        }
    }

    getInfoStatus(detail) {
        const { approveds, status, createdBy } = detail;
        const { color } = settingApp;
        let search = approveds.map(e => e.actions);
        let info = {
            isETA: false
        };
        let index = -1;
        switch (status) {
            case 2:
                index = search.findIndex(e => e[0].isWaiting === true);
                if (index > -1 && search[index][0].type < 3) {
                    info.acceptBy = search[index][0].approved.approvedName;
                } else {
                    info.acceptBy = 'Hệ thống'
                }
                if (index > -1 && search[index][0].approved && search[index][0].approved.approvedBy) {
                    info.userId = search[index][0].approved.approvedBy;
                } else {
                    info.userId = createdBy
                }
                if (index > -1) {
                    if (search[index][0].type === 1) {
                        info.borderLeftColor = '#5CB85C';
                    } else {
                        info.borderLeftColor = '#F0AD4E';
                    }
                    info.userId = search[index][0].approved.approvedBy;
                    info.component = (
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <MaterialIcons name='loop' size={23} color={color} />
                            <Text style={{ fontSize: 14, color: color, fontWeight: 'bold', marginLeft: 10 }}>
                                Đang chờ xét duyệt
                            </Text>
                        </View>
                    );
                    info.isETA = true;
                }
                break;
            case 3:
                index = search.findIndex(e => e[0].isLastValid === true && e[0].isValidCondition === true);
                if (index > -1 && search[index][0].type < 3) {
                    info.acceptBy = search[index][0].approved.approvedName;
                } else {
                    info.acceptBy = 'Hệ thống'
                }
                if (index > -1 && search[index][0].approved && search[index][0].approved.approvedBy) {
                    info.userId = search[index][0].approved.approvedBy;
                } else {
                    info.userId = createdBy
                }
                if (index > -1) {
                    info.borderLeftColor = '#5CB85C';
                    info.component = (
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Ionicons name='ios-checkmark-circle' size={23} color='#5CB85C' />
                            <Text style={{ fontSize: 14, color: '#5CB85C', fontWeight: 'bold', marginLeft: 10 }}>
                                Đã duyệt
                            </Text>
                        </View>
                    );
                } else {
                    info.borderLeftColor = '#9CA7B2';
                    info.component = (
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{ fontSize: 14, color: '#9CA7B2', fontWeight: 'bold' }}>
                                Tạo yêu cầu
                            </Text>
                        </View>
                    );
                }
                break;
            case 4:
                index = search.findIndex(e => e[0].isReject === true);
                if (index > -1 && search[index][0].type < 3) {
                    info.acceptBy = search[index][0].approved.approvedName;
                } else {
                    info.acceptBy = 'Hệ thống'
                }
                if (index > -1 && search[index][0].approved && search[index][0].approved.approvedBy) {
                    info.userId = search[index][0].approved.approvedBy;
                } else {
                    info.userId = createdBy
                }
                if (index > -1) {
                    info.borderLeftColor = '#D9534F';
                    info.component = (
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Ionicons name='md-remove-circle' size={23} color='#D9534F' />
                            <Text style={{ fontSize: 14, color: '#D9534F', fontWeight: 'bold', marginLeft: 10 }}>
                                Đã từ chối
                            </Text>
                        </View>
                    );
                }
                break;
            default:
                info.acceptBy = 'Hệ thống';
                info.userId = createdBy;
                info.borderLeftColor = '#9CA7B2';
                info.component = (
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ fontSize: 14, color: '#9CA7B2', fontWeight: 'bold' }}>
                            Tạo yêu cầu
                        </Text>
                    </View>
                );
                break;
        }

        return info;
    }

    renderInfo() {
        const { color, colorText } = settingApp;
        const { isLoadingInfo, infoUser, time, infoStaus } = this.state;
        let content = <ActivityIndicator size='small' color={color} style={{ height: 29 }} />
        if (!isLoadingInfo) {
            content = (
                <View>
                    <Text style={{ fontSize: 12, color: color }}>
                        <Text style={{ color: colorText }}>bởi </Text>
                        {infoUser}
                    </Text>
                    <Text style={{ fontSize: 12, color: colorText }}>
                        Vòng# {infoStaus.acceptBy} xét duyệt ({time})
                    </Text>
                </View>
            )
        }

        return (
            <View style={{ marginTop: 10 }}>
                {content}
            </View>
        )
    }

    rendeDescription() {
        const { shortDescription } = this.props.detail;
        const { colorText } = settingApp;
        if (shortDescription && shortDescription.length > 0) {
            return (
                <View style={{ marginTop: 20 }}>
                    <Text style={{ fontSize: 14, color: '#9CA7B2' }}>MÔ TẢ NGẮN</Text>
                    <Text style={{ fontSize: 12, color: colorText, marginTop: 10 }}>
                        {shortDescription}
                    </Text>
                </View>
            )
        } else {
            return <View />
        }
    }

    render() {
        const { infoStaus } = this.state;
        const { borderLeftColor, component } = infoStaus;
        return (
            <View style={{
                padding: 20,
                backgroundColor: '#ffffff',
                borderLeftColor,
                borderLeftWidth: 5,
                ...settingApp.shadow
            }}>
                <Text style={{ fontSize: 14, color: '#9CA7B2' }}>TRẠNG THÁI</Text>
                <View style={{ marginTop: 10 }}>
                    {component}
                    {this.renderInfo()}
                </View>
                {this.rendeDescription()}
            </View>
        )
    }
}

export default StatusForm;