import React, { Component } from 'react';
import { ActivityIndicator, View, Text, TouchableOpacity } from 'react-native';
import { Arrow, settingApp, Utils, Error } from '../../../../../public';
import { HaravanHr } from '../../../../../Haravan';

import Collapsible from 'react-native-collapsible';

class InfoUserCreate extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isCollapsed: false,
            list: [
                { title: 'HỌ TÊN', detail: null, key: 'fullName' },
                { title: 'MÃ NHÂN VIÊN', detail: null, key: 'userId' },
                { title: 'CHỨC DANH', detail: null, key: 'jobtitleName' },
                { title: 'PHÒNG BAN', detail: null, key: 'departmentName' },
                { title: 'THỜI GIAN TẠO', detail: <Text style={{ color: settingApp.colorText, fontSize: 14 }}>{Utils.formatTime(props.detail.createdAt, 'L hh:mm A')}</Text> },
                { title: 'MÃ SỐ PHẾU', detail: <Text style={{ color: settingApp.colorText, fontSize: 14 }}>{props.detail.code}</Text> },
            ]
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.closeAll) {
            this.setState({ isCollapsed: false })
        }
    }

    async componentDidMount() {
        const { list } = this.state;
        const { colorText } = settingApp;
        const result = await HaravanHr.getColleague(this.props.detail.createdBy);
        let newList = [];
        if (result && result.data && !result.error) {
            newList = list.map((e, i) => {
                if (e.key && e.key === 'fullName') {
                    return { ...e, detail: <Text style={{ color: colorText, fontSize: 14 }}>{result.data.fullName}</Text> }
                } else if (e.key && e.key === 'userId') {
                    return { ...e, detail: <Text style={{ color: colorText, fontSize: 14 }}>{result.data.userId}</Text> }
                } else if (e.key && e.key === 'jobtitleName') {
                    return { ...e, detail: <Text style={{ color: colorText, fontSize: 14 }}>{result.data.mainPosition.jobtitleName}</Text> }
                } else if (e.key && e.key === 'departmentName') {
                    return { ...e, detail: <Text style={{ color: colorText, fontSize: 14 }}>{result.data.mainPosition.departmentName}</Text> }
                } else {
                    return { ...e }
                }
            })
        } else {
            newList = list.map((e, i) => {
                if (e.key) {
                    return { ...e, detail: <Error /> }
                }
                return { ...e }
            })
        }
        this.setState({ list: newList });
    }

    renderContent() {
        const { list } = this.state;
        const { colorText, color } = settingApp;
        return (
            <View style={{ padding: 10 }}>
                {list.map((e, i) => {
                    return (
                        <View style={{ marginTop: i > 0 ? 20 : 0 }} key={i}>
                            <Text style={{ color: '#9CA7B2', fontSize: 12, marginBottom: 5 }}>{e.title}</Text>
                            {e.detail ?
                                e.detail :
                                <ActivityIndicator size='small' color={color} style={{ height: 20 }} />
                            }
                        </View>
                    )
                })}
            </View>
        )
    }

    render() {
        const { isCollapsed } = this.state;
        return (
            <View style={{ flex: 1, marginBottom: 10 }}>
                <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                    <TouchableOpacity
                        activeOpacity={1}
                        style={styles.header}
                        onPress={() => this.setState({ isCollapsed: !this.state.isCollapsed }, () => this.props.isOpen(this.state.isCollapsed))}
                    >
                        <Text style={{ color: '#ffffff', fontSize: 14 }}>
                            Thông tin người tạo yêu cầu
                    </Text>
                        <Arrow isOpen={isCollapsed} color='#ffffff' />
                    </TouchableOpacity>
                </View>

                <Collapsible
                    duration={150}
                    collapsed={!isCollapsed}
                    style={{ padding: 10 , flex:1}}
                >
                    <View style={{ flex: 1, backgroundColor: '#ffffff', borderRadius: 3, ...settingApp.shadow }}>
                        {this.renderContent()}
                    </View>
                </Collapsible>
            </View>
        )
    }
}

const styles = {
    header: {
        flex: 1,
        flexDirection: 'row',
        height: 50,
        backgroundColor: '#9CA7B2',
        paddingLeft: 10,
        paddingRight: 10,
        alignItems: 'center',
        borderRadius: 3,
        ...settingApp.shadow
    }
}

export default InfoUserCreate