import React, { Component } from 'react';
import { ActivityIndicator, View, Text, TouchableOpacity } from 'react-native';

import { HaravanHr } from '../../../../../Haravan';

import { Arrow, settingApp } from '../../../../../public';

import Collapsible from 'react-native-collapsible';

class Tracker extends Component {
    constructor(props) {
        super(props);

        const tracker = props.detail.map((e, i) => {
            return ({
                id: e.numberId,
                fullName: e.userBriefInfo[0].fullName,
                position: '',
                error: false
            })
        })

        this.state = {
            isCollapsed: false,
            tracker
        }

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.closeAll) {
            this.setState({ isCollapsed: false })
        }
    }

    async componentDidMount() {
        const { tracker } = this.state;
        const data = [];
        for (let item of tracker) {
            const position = await HaravanHr.getColleague(item.id);
            if (position && position.data && !position.error) {
                item = {
                    ...item,
                    position: `${position.data.userId} - ${position.data.mainPosition.jobtitleName}`
                }
            } else {
                item = {
                    ...item,
                    position: 'Lấy thông tin thất bại.',
                    error: true
                }
            }
            data.push(item);
        }

        this.setState({ tracker: data })
    }

    renderUserBriefInfo() {
        const { tracker } = this.state;
        return tracker.map((e, i) => {
            let contentInfo = <ActivityIndicator size='small' color={settingApp.color} style={{ height: 20 }} />;
            const color = e.error ? '#bdc3c7' : settingApp.colorTextor;
            if (e.position) {
                contentInfo = (
                    <Text style={{ fontSize: 14, color }}>
                        {e.position}
                    </Text>
                )
            }
            return (
                <View style={{ marginBottom: 10 }} key={i}>
                    <Text style={{ fontSize: 14, color: settingApp.color }}>
                        {e.fullName}
                    </Text>
                    {contentInfo}
                </View>
            )
        })
    }

    render() {
        const { isCollapsed } = this.state;
        return (
            <View style={{ flex: 1 }}>
                <TouchableOpacity
                    activeOpacity={1}
                    style={styles.header}
                    onPress={() => this.setState({ isCollapsed: !this.state.isCollapsed }, () => this.props.isOpen(this.state.isCollapsed))}
                >
                    <Text style={{ color: '#ffffff', fontSize: 14 }}>
                        Thông tin người sử dụng
                    </Text>
                    <Arrow isOpen={isCollapsed} color='#ffffff' />
                </TouchableOpacity>
                <Collapsible
                    duration={150}
                    collapsed={!isCollapsed}
                    style={{ marginTop: 10 }}
                >
                    <View style={{ flex: 1, backgroundColor: '#ffffff', padding: 10, borderRadius: 3 }}>
                        {this.renderUserBriefInfo()}
                    </View>
                </Collapsible>
            </View>
        )
    }
}

const styles = {
    header: {
        flex: 1,
        flexDirection: 'row',
        height: 50,
        backgroundColor: '#9CA7B2',
        paddingLeft: 10,
        paddingRight: 10,
        alignItems: 'center',
        borderRadius: 3
    }
}

export default Tracker;