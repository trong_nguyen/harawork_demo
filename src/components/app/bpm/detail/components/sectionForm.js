import React, { Component } from 'react';
import { View, Text } from 'react-native';

class SectionForm extends Component {

    shouldComponentUpdate(nextProps, nextState, nextContext) {
         return false;
    }

    render() {
        const { data } = this.props;
        if (data && data.hasOwnProperty('length') && data.length > 0) {
            const item = data[0];
            if (item && item.hasOwnProperty('length') && item.length > 0)
                return (
                    <View style={{ flex: 1, padding: 10 }}>
                        {
                            item.map((e, i) => (
                                <View style={{ flex: 1, marginTop: (i === 0) ? 0 : 10 }} key={i}>
                                    <Text style={{ fontSize: 12, color: '#9CA7B2', fontWeight: '500', lineHeight: 22 }}>{e.title}</Text>
                                    <Text style={{ fontSize: 14, color: '#474747' }}>{e.value}</Text>
                                </View>
                            ))
                        }
                    </View>
                )
        }
        return <View />
    }
}

export default SectionForm;