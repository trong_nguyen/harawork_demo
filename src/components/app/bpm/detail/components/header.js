import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { HeaderWapper } from '../../../../../public';
import { Ionicons, MaterialCommunityIcons } from '@expo/vector-icons';
import Menu, { MenuItem } from 'react-native-material-menu';
import { HaraBPM } from '../../../../../Haravan';

class Header extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            dataTimeLine: null
        }
    }

    componentDidMount() {
        this.loadTimeLine();
    }

    async loadTimeLine() {
        const timeLine = await HaraBPM.getTimeLine(this.props.id);
        if (timeLine && timeLine.data && !timeLine.error) {
            this.setState({ dataTimeLine: timeLine.data })
        }
    }

    showMenu() {
        this.menu.show();
    }

    comment() {
        this.props.navigation.navigate('BPMDetailFormComment', {
            detail: this.state.dataTimeLine,
            actions: () => this.loadTimeLine()
        });
        this.menu.hide();
    }

    copyLink() {
        this.menu.hide();
    }

    render() {
        const { title, navigation } = this.props;
        return (
            <HeaderWapper style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                <TouchableOpacity
                    onPress={() => navigation.pop()}
                    style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                    <Ionicons name='ios-arrow-back' size={23} color='#ffffff' />
                </TouchableOpacity>

                <Text style={{ fontSize: 16, color: '#ffffff', fontWeight: 'bold' }}>
                    {title}
                </Text>

                <TouchableOpacity
                    onPress={() => this.showMenu()}
                    style={{
                        width: 40,
                        height: 44,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'transparent'
                    }}>
                    <MaterialCommunityIcons name='dots-vertical' color='#ffffff' size={25} />
                    <Menu
                        ref={refs => this.menu = refs}
                        button={<View />}
                    >
                        <MenuItem
                            disabled={!(this.state.dataTimeLine)}
                            onPress={() => this.comment()}>
                            Bình luận
                        </MenuItem>
                        <MenuItem onPress={() => this.copyLink()}>
                            Sao chép liên kết
                        </MenuItem>
                    </Menu>
                </TouchableOpacity>
            </HeaderWapper>
        )
    }
}

export default Header;