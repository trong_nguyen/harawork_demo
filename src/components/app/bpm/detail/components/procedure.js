import React, { Component } from 'react';
import { ActivityIndicator, View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { HaravanHr, HaraBPM } from '../../../../../Haravan';
import { Arrow, settingApp, Utils, Error } from '../../../../../public';
import Collapsible from 'react-native-collapsible';
import { MaterialIcons, Ionicons, MaterialCommunityIcons } from '@expo/vector-icons';
import moment from 'moment';

class Procedure extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isCollapsed: false,
            procedure: [],
            isLoading: true
        }

        this.infoCreate = null;
        this.listProcedure = this.getInfo();
    }

    getInfo() {
        const { approveds } = this.props.detail;
        const info = [];
        for (let i = 0; i < approveds.length; i++) {
            const item = [];
            const { actions } = approveds[i];
            for (let j = 0; j < actions.length; j++) {
                const obj = {};
                const { type, isDone, isReject, isWaiting, isValidCondition, name, createdAt } = actions[j];
                if (isValidCondition) {
                    obj.title = name;
                    obj.type = type;
                    obj.createdAt = createdAt;
                    obj.borderLeftColor = type === 1 ? '#5CB85C' : type === 2 ? '#F0AD4E' : '#9CA7B2';
                    obj.isComplete = false;
                    obj.status = { ...actions[j] }
                    obj.detail = <ActivityIndicator size='small' color={settingApp.color} style={{ height: 30 }} />
                } else {
                    obj.isComplete = true;
                    obj.title = name;
                    obj.type = type;
                    obj.createdAt = createdAt;
                    obj.borderLeftColor = '#474747';
                    obj.status = null
                    obj.detail = (
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                            <Ionicons name='md-close-circle' color='#474747' size={23} />
                            <Text style={{ color: '#9CA7B2', fontSize: 12, marginLeft: 10 }}>Không tiến hành</Text>
                        </View>
                    )
                }
                item.push(obj);
            }
            info.push(item);
        }
        return info;
    }

    checkData(input) {
        if (input && input.data && !input.error) {
            return input;
        }
        return false;
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.closeAll) {
            this.setState({ isCollapsed: false })
        }
    }

    async componentDidMount() {
        const { createdBy } = this.props.detail;
        const { color, colorText } = settingApp;
        const infoCreate = await HaravanHr.getColleague(createdBy);
        const setting = await HaraBPM.getSetting();
        const { slaTime } = setting.data[0];
        this.infoCreate = (
            <View>
                <Text style={{ fontSize: 14, color: colorText }}>bởi <Text style={{ color: color }}>{infoCreate.data.fullName}</Text></Text>
                <Text style={{ fontSize: 12, color: '#9CA7B2' }}>{infoCreate.data.mainPosition.jobtitleName}</Text>
            </View>
        );

        let { listProcedure } = this;
        for (let i = 0; i < listProcedure.length; i++) {
            for (let j = 0; j < listProcedure[i].length; j++) {
                if (!listProcedure[i][j].isComplete) {
                    const detail = await this.getDetail(listProcedure[i][j], slaTime);
                    listProcedure[i][j].detail = detail;
                }
            }
        }
        this.listProcedure = listProcedure;
        this.setState({ isLoading: false })
    }

    async getDetail(obj, slaTime) {
        const { color, colorText } = settingApp;
        const { type, createdAt, status } = obj;
        const { isDone, isReject, isWaiting, approved } = status;
        let detail = <View />
        let userInfo = {
            fullName: 'Hệ thống',
            position: null,
            titleStaus: null,
            titleColor: null,
            time: null,
            icon: null

        };
        if (type < 3) {
            userInfo = await HaravanHr.getColleague(approved.approvedBy);
            userInfo.fullName = userInfo.data.fullName;
            userInfo.position = userInfo.data.mainPosition.jobtitleName;
        }
        let time = createdAt;
        if (isDone || (approved && approved.approvedBy && approved.approvedAt)) {
            time = approved && approved.approvedAt || time;
            userInfo.titleStaus = 'Đã duyệt';
            userInfo.titleColor = '#5CB85C';
            userInfo.time = Utils.formatTime(time, 'L hh:mm A');
            userInfo.icon = <Ionicons name='ios-checkmark-circle' size={23} color='#5CB85C' />
        } else if (isWaiting) {
            time = approved && approved.approvedAt || time;
            time = moment(time).add(slaTime, 'hours');
            userInfo.titleStaus = 'Đang chờ hoàn tất';
            userInfo.titleColor = '#0279C7';
            userInfo.time = `ETA: ${Utils.formatTime(time, 'L hh:mm A')}`;
            userInfo.icon = <MaterialIcons name='loop' size={23} color={color} />
        } else if (isReject) {
            time = approved && approved.rejectedAt || time;
            userInfo.titleStaus = 'Đã từ chối';
            userInfo.titleColor = '#D9534F';
            userInfo.time = Utils.formatTime(time, 'L hh:mm A');
            userInfo.icon = <Ionicons name='md-remove-circle' size={23} color='#D9534F' />
        } else {
            userInfo.titleStaus = 'Chưa tiến hành';
            userInfo.titleColor = '#9CA7B2';
            userInfo.time = null;
            userInfo.icon = <MaterialCommunityIcons name='stop-circle' size={23} color='#474747' />
        }

        const checkPosition = !!(userInfo && userInfo.position && userInfo.position.length > 0);
        const checkTime = !!(userInfo && userInfo.time && userInfo.time.length > 0);
        detail = (
            <View>
                <Text style={{ color: colorText, fontSize: 14 }}>
                    <Text style={{ color: userInfo.titleColor, fontWeight: 'bold' }}>{userInfo.titleStaus} </Text>
                    bởi
                    <Text style={{ color: userInfo.position ? color : color }}> {userInfo.fullName}</Text>
                </Text>
                {checkPosition && <Text style={{ color: colorText, fontSize: 12 }}>{userInfo.position}</Text>}
                {checkTime && <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                    {userInfo.icon}
                    <Text style={{ fontSize: 12, color: '#9CA7B2', marginLeft: 10 }}>{userInfo.time}</Text>
                </View>}
            </View>
        )

        return detail;
    }

    renderProcedure() {
        const { createdAt } = this.props.detail;
        const { isLoading } = this.state;
        const { color, colorText, colorSperator } = settingApp;
        return (
            <View style={{ flex: 1 }}>
                <View style={{ padding: 10 }}>
                    <View style={{ borderLeftColor: '#9CA7B2', borderLeftWidth: 5, paddingLeft: 10 }}>
                        <Text style={{ fontSize: 14, color: colorText, marginBottom: 5 }}>Tạo yêu cầu</Text>
                        {isLoading ? <ActivityIndicator size='small' color={color} style={{ height: 30 }} /> : this.infoCreate}
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                            <Ionicons name='ios-checkmark-circle' size={23} color='#5CB85C' />
                            <Text style={{ fontSize: 12, color: '#9CA7B2', marginLeft: 10 }}>{Utils.formatTime(createdAt, 'L hh:mm A')}</Text>
                        </View>
                    </View>
                </View>
                {this.listProcedure.map((e, i) => {
                    if (e.length === 1) {
                        return e.map((m, l) => {
                            const { borderLeftColor, title, detail } = m;
                            return (
                                <View style={{ padding: 10, borderTopColor: colorSperator, borderTopWidth: 1 }} key={l}>
                                    <View style={{ borderLeftColor, borderLeftWidth: 5, paddingLeft: 10 }}>
                                        <Text style={{ fontSize: 14, color: colorText, marginBottom: 5 }}>{title}</Text>
                                        {detail}
                                    </View>
                                </View>
                            )
                        })
                    } else {
                        return (
                            <View style={{ padding: 10, borderTopColor: colorSperator, borderTopWidth: 1 }} key={i} >
                                <View style={{ borderLeftColor: '#0279C7', borderLeftWidth: 5, paddingLeft: 10 }}>
                                    {e.map((m, l) => {
                                        const { borderLeftColor, title, detail } = m;
                                        return (
                                            <View style={{ borderLeftColor, borderLeftWidth: 5, paddingLeft: 20, marginBottom: l === 0 ? 10 : 0 }} key={l}>
                                                <Text style={{ fontSize: 14, color: colorText, marginBottom: 5 }}>{title}</Text>
                                                {detail}
                                            </View>
                                        )
                                    })}
                                </View>
                            </View>
                        )
                    }
                })}
            </View>
        )
    }

    render() {
        const { isCollapsed } = this.state;
        return (
            <View style={{ flex: 1, marginBottom: 10 }}>
                <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                    <TouchableOpacity
                        activeOpacity={1}
                        style={styles.header}
                        onPress={() => this.setState({ isCollapsed: !this.state.isCollapsed }, () => this.props.isOpen(this.state.isCollapsed))}
                    >
                        <Text style={{ color: '#ffffff', fontSize: 14 }}>
                            Chi tiết quy trình xử lý
                    </Text>
                        <Arrow isOpen={isCollapsed} color='#ffffff' />
                    </TouchableOpacity>
                </View>
                <Collapsible
                    duration={150}
                    collapsed={!isCollapsed}
                    style={{ padding: 10 }}
                >
                    <View style={{ flex: 1, backgroundColor: '#ffffff', borderRadius: 3, ...settingApp.shadow }}>
                        <ScrollView scrollEnabled={false}>
                            {this.renderProcedure()}
                        </ScrollView>
                    </View>
                </Collapsible>
            </View >
        )
    }
}

const styles = {
    header: {
        flex: 1,
        flexDirection: 'row',
        height: 50,
        backgroundColor: '#9CA7B2',
        paddingLeft: 10,
        paddingRight: 10,
        alignItems: 'center',
        borderRadius: 3,
        ...settingApp.shadow
    }
}

export default Procedure;