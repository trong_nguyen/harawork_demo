import React, { Component } from 'react';
import { ActivityIndicator, View, Text, TouchableOpacity } from 'react-native';
import { Arrow, settingApp } from '../../../../../public';
import Collapsible from 'react-native-collapsible';
import SectionForm from './sectionForm';
import SectionTable from './sectionTable';

class Section extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isCollapsed: false,
            detail: props.detail.hasOwnProperty('components') ? props.detail : null
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.closeAll) {
            this.setState({ isCollapsed: false })
        }
        if (!this.state.detail && nextProps.detail && nextProps.detail.hasOwnProperty('components')) {
            this.setState({ detail: nextProps.detail })
        }
    }

    render() {
        const { isCollapsed, detail } = this.state;
        const isDisable = detail ? false : true;
        let content = <View />;
        if (detail) {
            const { isTableComponent, components } = detail;
            if (isTableComponent) {
                content = <SectionTable data={components} />
            } else {
                content = <SectionForm data={components} />
            }
        }
        return (
            <View style={{ flex: 1, marginBottom: 10 }}>
                <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                    <TouchableOpacity
                        disabled={isDisable}
                        activeOpacity={1}
                        style={styles.header}
                        onPress={() => this.setState({ isCollapsed: !this.state.isCollapsed }, () => this.props.isOpen(this.state.isCollapsed))}
                    >
                        <Text style={{ color: '#ffffff', fontSize: 14 }}>
                            {this.props.title}
                        </Text>
                        {detail ?
                            <Arrow isOpen={isCollapsed} color='#ffffff' />
                            :
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                                <ActivityIndicator size='small' color='#ffffff' />
                            </View>
                        }
                    </TouchableOpacity>
                </View>

                <Collapsible
                    duration={150}
                    collapsed={!isCollapsed}
                    style={{ padding: 10 }}
                >
                    <View style={{ flex: 1, backgroundColor: '#ffffff', borderRadius: 3, ...settingApp.shadow }}>
                        {content}
                    </View>
                </Collapsible>
            </View>
        )
    }
}

const styles = {
    header: {
        flex: 1,
        flexDirection: 'row',
        height: 50,
        backgroundColor: '#9CA7B2',
        paddingLeft: 10,
        paddingRight: 10,
        alignItems: 'center',
        borderRadius: 3,
        ...settingApp.shadow
    }
}

export default Section;