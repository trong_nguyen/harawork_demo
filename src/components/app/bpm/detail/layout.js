import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import Header from './components/header';
import StatusForm from './components/statusForm';
import Section from './components/section';
import InfoUserCreate from './components/infoUserCreate'
import Procedure from './components/procedure';
import Tracker from './components/tracker';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { settingApp } from '../../../../public';

class Layout extends Component {

    renderContent() {
        const { detail } = this;
        const { closeAll, sections } = this.state;
        const { formSections } = detail;
        const isShowTracker = (detail.trackers && detail.trackers.length > 0);
        const detailSections = sections || formSections;
        return (
            <ScrollView>
                <StatusForm detail={detail} />
                <View style={{ paddingTop: 10 }}>
                    {detailSections.map((e, i) => {
                        return (
                            <View key={i}>
                                <Section
                                    detail={e}
                                    title={e.name}
                                    closeAll={closeAll}
                                    isOpen={(status) => this.isOpen(`Section${i}`, status)}
                                />
                            </View>
                        )
                    })}
                    <InfoUserCreate detail={detail} closeAll={closeAll} isOpen={(status) => this.isOpen('InfoUserCreate', status)} />
                    <Procedure detail={detail} closeAll={closeAll} isOpen={(status) => this.isOpen('Procedure', status)} />
                    {isShowTracker && <Tracker detail={detail.trackers} closeAll={closeAll} isOpen={(status) => this.isOpen('Tracker', status)} />}
                </View>
            </ScrollView>
        )
    }

    renderButton() {
        const isApproved = false;
        const isCompleted = false;
        if (isApproved) {
            return (
                <View style={{ width: settingApp.width, height: 55, flexDirection: 'row', backgroundColor: '#ffffff', ...settingApp.shadow }}>
                    <TouchableOpacity style={{ flex: 1, backgroundColor: settingApp.color, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 14, color: '#ffffff', fontWeight: 'bold' }}>Gửi lại</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: 1, backgroundColor: '#D9534F', justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 14, color: '#ffffff', fontWeight: 'bold' }}>Từ chối</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: 1, backgroundColor: '#5CB85C', justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 14, color: '#ffffff', fontWeight: 'bold' }}>Đồng ý</Text>
                    </TouchableOpacity>
                </View>
            )
        } else if (isCompleted) {
            return (
                <View style={{ width: settingApp.width, height: 55, backgroundColor: '#475C71', justifyContent: 'center', paddingLeft: 20, ...settingApp.shadow }}>
                    <Text style={{ color: '#ffffff', fontSize: 14 }}>Vui lòng hoàn tất trên phiên bản PC web</Text>
                </View>
            )
        } else {
            return <View />
        }
    }


    render() {
        const { formName } = this.detail;
        return (
            <View style={{ flex: 1 }}>
                <Header
                    id={this.detail.id}
                    title={formName}
                    navigation={this.props.navigation}
                />
                <View style={{ flex: 1 }}>
                    {this.renderContent()}
                    {(this.state.listOpen.length > 0) && <TouchableOpacity
                        onPress={() => this.setState({ closeAll: true, listOpen: [] }, () => this.setState({ closeAll: false }))}
                        style={{ position: 'absolute', bottom: 10, right: 10, width: 150, height: 40, backgroundColor: 'transparent' }}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            backgroundColor: '#ffffff',
                            paddingLeft: 10,
                            paddingRight: 10,
                            borderRadius: 40,
                            ...settingApp.shadow
                        }}>
                            <MaterialCommunityIcons name='arrow-collapse' size={23} color='#9CA7B2' />
                            <Text style={{ fontSize: 14, color: '#000' }}>Thu gọn tất cả</Text>
                        </View>
                    </TouchableOpacity>}
                </View>
                {this.renderButton()}
            </View>
        )
    }
}

export default Layout;