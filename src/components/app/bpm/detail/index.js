import Layout from './layout';
import { HaraBPM } from '../../../../Haravan';
import { Toast } from '../../../../public';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../state/action';

class BPMDetailForm extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            closeAll: false,
            listOpen: [],
            sections: null
        }

        const { detail } = props.navigation.state.params;
        this.detail = detail
    }

    async componentDidMount() {
        // try {
            const { colleague } = this.props.app;
            const { id, isReaded } = this.detail;
            const resultSection = await HaraBPM.getDetailSection(id);
            if (resultSection && resultSection.data && !resultSection.error) {
                this.setState({ sections: resultSection.data.sections })
            }
            const resultCheckForm = await HaraBPM.getDetailForm(colleague, id);
            if (resultCheckForm && resultCheckForm.data && !resultCheckForm.error) {
                if (!isReaded) {
                    await HaraBPM.readDetail(id);
                    this.props.actions.bpm.isReaded(id);
                }
            } else {
                throw new Error();
            }
        // } catch (error) {
        //     Toast.show('Phiếu không tồn tại');
        //     this.props.navigation.pop();
        // }
    }

    isOpen(key, status) {
        let { listOpen } = this.state;
        if (status) {
            listOpen.push(key);
        } else {
            const index = listOpen.indexOf(key);
            listOpen.splice(index, 1);
        }
        this.setState({ listOpen });
    }

    componentWillUnmount() {
        this.props.actions.bpm.isReaded(null);
    }

}

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(mapStateToProps, mapDispatchToProps)(BPMDetailForm);