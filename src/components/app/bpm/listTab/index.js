import Layout from './layout';

import { settingApp } from '../../../../public';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../state/action';

class ListTab extends Layout {
    constructor(props) {
        super(props);

        let { title, first, second } = props.navigation.state.params;
        const disableFilter = !(second);

        this.state = {
            title: title || '',
            first,
            second,
            disableFilter,
            isOpenFilter: false
        }

    }

    changeType(position, itemKey) {
        const currentScreen = this.props.navigation.state.key;
        const Screen = `BPMListTab${itemKey[0].toUpperCase() + itemKey.slice(1)}`;
        if ((currentScreen !== Screen) && (itemKey.match(/formWaitingApprove|formWaitingComplete/) != null)) {
            const { listRouterBPM } = settingApp;
            const index = listRouterBPM.findIndex(e => e.key === Screen);
            const item = listRouterBPM[index];

            this.setState({ isOpenFilter: false }, () => {
                const { name, key, first, second } = item;
                this.props.navigation.state.params.changeScreen(key);
                this.props.navigation.navigate({
                    routeName: key,
                    params: { title: name, first, second }
                });
            });
        } else {
            this.setState({ isOpenFilter: false }, () => {
                const keyTab = position === 0 ? 'First' : 'Second';
                this.tab._navigation.navigate(keyTab);
            });
        }
    }

}

const mapStateToProps = (state) => ({ ...state });

const mapDispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListTab);