import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { HeaderIndex, settingApp } from '../../../../public';
import BPMTab from '../../../../route/components/bpm';
import Filter from '../components/filter';
import { Ionicons } from '@expo/vector-icons';

class Layout extends Component {

    renderHeader() {
        const { title, disableFilter } = this.state;
        return (
            <HeaderIndex
                title={
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <TouchableOpacity
                            activeOpacity={1}
                            onPress={() => this.setState({ isOpenFilter: true })}
                            style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}
                            disabled={disableFilter}
                        >
                            <Text style={settingApp.styleTitle}>
                                {title}
                            </Text>
                            {!disableFilter && <View style={{ justifyContent: 'center', marginTop: 5, marginLeft: 5 }}>
                                <Ionicons name="md-arrow-dropdown" size={23} color="#ffffff" />
                            </View>}
                        </TouchableOpacity>
                    </View>
                }
                buttonRight={
                    <View style={{ flex: 1 }}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('BPMSearch')}
                            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
                        >
                            <Ionicons name='md-search' size={23} color='#ffffff' />
                        </TouchableOpacity>
                    </View>
                }
            />
        )
    }

    render() {
        const { first, second, disableFilter } = this.state;
        const { props } = this;
        return (
            <View style={{ flex: 1 }}>
                {this.renderHeader()}
                <BPMTab
                    ref={refs => this.tab = refs}
                    screenProps={{ first, second, propsApp: props }}
                />

                {!disableFilter && <Filter
                    data={[first, second]}
                    propsBPM={props.bpm}
                    changeType={(position, itemKey) => this.changeType(position, itemKey)}
                    modalVisible={this.state.isOpenFilter}
                    close={() => this.setState({ isOpenFilter: false })}
                />}
            </View>
        )
    }
}

export default Layout;