import Layout from './layout';
import { HaraBPM, HaravanHr } from '../../../../Haravan';
import moment from 'moment';
import { Utils } from '../../../../public';

class List extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            list: props.propsApp.bpm[`list${props.keyStore}`],
            isLoading: !(props.propsApp.bpm[`list${props.keyStore}`].length > 0),
            onLoadMore: false,
            refreshing: true
        }

        this.page = 1;
        this.totalPage = null;

        this.api = props.api;
        this.keyStore = props.keyStore;

        this.departmentListName = [];
        this.prvList = [];
    }

    async componentDidMount() {
        const setting = await HaraBPM.getSetting();
        if (setting && setting.data && !setting.error) {
            this.setting = setting.data[0];
        }
        this.loadData();
    }

    async loadData() {
        const { api, page, totalPage, departmentListName, prvList } = this;
        if (!totalPage || (totalPage && !isNaN(totalPage) && page <= totalPage)) {
            const result = await HaraBPM.getList(api, page);
            if (result && result.data && !result.error) {
                const { data, totalCount } = result.data;
                let newList = [];
                for (const item of data) {
                    let createUserName = '';
                    const { createdBy } = item;
                    const itemIndex = departmentListName.findIndex(e => e.key === createdBy);
                    if (itemIndex > -1) {
                        createUserName = departmentListName[itemIndex].fullName;
                    } else {
                        createUserName = await this.getdepartmentListName(createdBy);
                    }

                    const info = this.checkInfoStatus(item);

                    newList.push({ ...item, createUserName, info });
                }

                newList = [...prvList, ...newList];
                this.prvList = newList;

                if (!totalPage) {
                    this.totalPage = Math.ceil(totalCount / 20);

                    this.props.propsApp.actions.bpm.listBPM(newList, this.keyStore);
                }

                this.page = this.page + 1;
                this.setState({
                    list: newList,
                    isLoading: false,
                    onLoadMore: false,
                    refreshing: false
                });
            }
        }
    }

    checkInfoStatus(item) {
        const { approveds, status, createdAt } = item;
        let search = approveds.map(e => e.actions);
        let info = {};
        let index = -1;
        switch (status) {
            case 2:
                search.map((e, i) => {
                    index = e.findIndex(i => i.isWaiting === true);
                    if (index > -1) {
                        if (e[index].type === 1) {
                            info.color = '#0279C7';
                            info.borderLeftColor = '#5CB85C';
                        } else {
                            info.color = '#F0AD4E';
                            info.borderLeftColor = '#F0AD4E';
                        }
                        info.name = e[index].name;
                        let time = createdAt;
                        if (e[index].approved && e[index].approved.approvedAt) {
                            time = e[index].approved.approvedAt;
                        }
                        const slaTime = this.setting && this.setting.slaTime ? this.setting.slaTime : 0;
                        time = moment(time).add(slaTime, 'hours');
                        info.time = `ETA: ${Utils.formatTime(time, 'L hh:mm A')}`;
                    }
                })
                break;
            case 3:
                search.map((e, i) => {
                    index = e.findIndex(i => i.isLastValid === true && i.isValidCondition === true);
                    if (index > -1) {
                        info.color = '#5CB85C';
                        info.name = e[index].name;
                        info.borderLeftColor = '#5CB85C';
                        info.time = Utils.formatTime(createdAt, 'L hh:mm A');
                    } else {
                        info.color = '#9CA7B2';
                        info.name = 'Tạo yêu cầu';
                        info.borderLeftColor = '#9CA7B2';
                        info.time = Utils.formatTime(createdAt, 'L hh:mm A');
                    }
                })
                break;
            case 4:
                search.map((e, i) => {
                    index = e.findIndex(i => i.isReject === true);
                    if (index > -1) {
                        info.color = '#D9534F';
                        info.name = e[index].name;
                        info.borderLeftColor = '#D9534F';
                        info.time = Utils.formatTime(e[index].approved.rejectedAt, 'L hh:mm A');
                    }
                });
                break;
            default:
                info.color = '#9CA7B2';
                info.name = 'Tạo yêu cầu';
                info.borderLeftColor = '#9CA7B2';
                info.time = Utils.formatTime(createdAt, 'L hh:mm A');
                break;
        }

        return info;
    }

    async getdepartmentListName(id) {
        let { departmentListName } = this;
        const result = await HaravanHr.getColleague(id);
        if (result && result.data && !result.error) {
            const { data } = result;
            departmentListName.push({ key: id, fullName: data.fullName });
            this.departmentListName = departmentListName;
            return data.fullName;
        } else {
            return '';
        }
    }

    onEndReached() {
        if (this.page <= this.totalPage && !this.state.onLoadMore) {
            this.setState({ onLoadMore: true }, () => this.loadData());
        }
    }

    onRefresh() {
        this.page = 1;
        this.totalPage = null;
        this.prvList = [];
        this.loadData();
    }

}

export default List;