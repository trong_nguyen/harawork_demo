import React, { Component } from 'react';
import { ActivityIndicator, View, Text, FlatList, Image, TouchableOpacity, RefreshControl } from 'react-native';
import Item from '../components/item';
import { settingApp, imgApp, Loading } from '../../../../public';

class Layout extends Component {

    renderContent() {
        const { onLoadMore } = this.state;
        const { color, isIPhoneX } = settingApp;
        return (
            <FlatList
                data={this.state.list}
                extraData={this.state}
                keyExtractor={(item, index) => item.id}
                style={{ ...settingApp.shadow }}
                ListHeaderComponent={this.renderHeaderList()}
                renderItem={obj => <Item obj={obj} navigation={this.props.propsApp.navigation}/>}
                ListFooterComponent={() => (
                    onLoadMore ?
                        <ActivityIndicator size='small' color={color} style={{ height: 50 }} />
                        :
                        <View style={{ marginBottom: isIPhoneX ? 25 : 10 }} />)
                }
                ListEmptyComponent={this.renderListEmty()}
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={() => this.onRefresh()}
                        tintColor={color}
                        colors={[color]}
                    />
                }
                onEndReached={() => this.onEndReached()}
                onEndReachedThreshold={1}
            />
        )
    }

    renderHeaderList() {
        const { color } = settingApp;
        return (
            <View
                duration={350}
                ref={refs => this.type = refs}
                style={{ flex: 1, height: 55, justifyContent: 'center', alignItems: 'flex-end', paddingRight: 10 }}>
                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('Gruop')}
                    style={{ flexDirection: 'row' }}
                >
                    <Image
                        source={imgApp.group}
                        style={{ width: 20, height: 20, marginRight: 10 }}
                        resizeMode='stretch'
                    />
                    <Text style={{ color: color, fontSize: 14 }}>
                        Hiển thị theo loại phiếu
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderListEmty() {
        const { height, statusBarHeight } = settingApp;
        const marginTop = (height - statusBarHeight) * 0.3;
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop }}>
                <Text style={{ color: settingApp.colorText, fontSize: 14 }}>Không tìm thấy dữ liệu</Text>
            </View>
        )
    }

    render() {
        const { isLoading } = this.state;
        if (isLoading) {
            return (
                <View style={{ flex: 1 }}>
                    <Loading />
                </View>
            )
        } else {
            return (
                <View style={{ flex: 1 }}>
                    {this.renderContent()}
                </View>
            )
        }
    }
}

export default Layout;