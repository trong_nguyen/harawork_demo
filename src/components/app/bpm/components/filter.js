import React, { Component } from 'react';
import {
    Modal,
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    Platform
} from 'react-native';

import { settingApp } from '../../../../public';

class Filter extends Component {

    render() {
        const { data, propsBPM } = this.props;
        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={this.props.modalVisible}
                onRequestClose={() => this.props.close()}
            >
                <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => this.props.close()}
                    style={{ flex: 1 }}>
                    <View style={styles.container} >
                        <ScrollView scrollEnabled={false}>
                            <View style={styles.cover}>
                                <View style={styles.triangle} />
                            </View>
                            <TouchableOpacity activeOpacity={1} style={styles.content}>
                                <View style={{ flex: 1 }}>
                                    {data.map((item, index) => {
                                        const count = propsBPM.listCount[`${item.key}`] || 0;
                                        return (
                                            <View style={{ flex: 1 }} key={index}>
                                                <TouchableOpacity
                                                    onPress={() => this.props.changeType(index, item.key)}
                                                    style={{ flex: 1, backgroundColor: 'transparent' }}>
                                                    <View style={{
                                                        flex: 1,
                                                        height: 55,
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                        paddingLeft: 20,
                                                        backgroundColor: 'transparent'
                                                    }}>
                                                        <Text style={{ color: '#ffffff', fontSize: 16 }}>
                                                            {item.title} ({count})
                                                        </Text>
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        )
                                    })}
                                </View>
                            </TouchableOpacity>
                        </ScrollView>
                    </View>
                </TouchableOpacity>
            </Modal >
        )
    }
}

const styles = {
    container: {
        flex: 1,
        ...Platform.select({
            android: {
                marginTop: settingApp.statusBarHeight + 20,
            },
            ios: {
                marginTop: settingApp.statusBarHeight + 44,
            }
        }),
        backgroundColor: 'rgba(0,0,0,0.5)',
        paddingLeft: 20,
        paddingRight: 20
    },
    cover: {
        backgroundColor: 'transparent',
        height: 23,
        width: 23,
        overflow: "hidden",
        marginLeft: '46%',
        marginTop: -10
    },
    triangle: {
        width: 23,
        height: 23,
        backgroundColor: '#313F4E',
        transform: [{ rotate: '45deg' }],
        borderRadius: 2.5,
        marginTop: 20
    },
    content: {
        flex: 1,
        backgroundColor: '#313F4E',
        paddingTop: 5,
        paddingBottom: 5,
        borderRadius: 10
    }
}

export default Filter;