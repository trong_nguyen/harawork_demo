import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { settingApp, ButtonCustom } from '../../../../public';

import { connect } from 'react-redux';

class Item extends Component {
    constructor(props) {
        super(props);

        const { item, index } = props.obj;
        this.state = {
            item,
            index
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.bpm.idIsReaded === this.state.item.id && !this.state.item.isReaded) {
            this.setState({
                item: {
                    ...this.state.item,
                    isReaded: true
                }
            });
        }
    }

    render() {
        const { item, index } = this.state;
        const { colorText, width } = settingApp;
        const { isReaded, createUserName, formName, info } = item;
        const fontWeight = isReaded ? 'normal' : 'bold';
        return (
            <ButtonCustom
                onPress={() => this.props.navigation.navigate('BPMDetailForm', { detail: item })}
                style={{
                    flex: 1,
                    padding: 10,
                    backgroundColor: '#ffffff',
                    borderLeftColor: info.borderLeftColor,
                    borderLeftWidth: 5,
                    marginBottom: 1
                }}>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ fontSize: 16, color: colorText, fontWeight, maxWidth: (width / 1.8) }} numberOfLines={1}>{formName}</Text>
                    <View style={{ padding: 5, paddingLeft: 10, paddingRight: 10, borderRadius: 20, backgroundColor: info.color }}>
                        <Text
                            numberOfLines={1}
                            style={{ fontSize: 12, color: '#ffffff', fontWeight, maxWidth: (width / 2.5) }}
                        >
                            {info.name}
                        </Text>
                    </View>
                </View>
                <View style={{ flex: 1, marginTop: 10 }}>
                    <Text style={{ fontSize: 12, color: '#9CA7B2' }} >
                        bởi {createUserName}
                    </Text>
                    <Text style={{ fontSize: 12, color: '#9CA7B2' }} >
                        {info.time}
                    </Text>
                </View>
            </ButtonCustom>
        )

    }
}

const mapStateToProps = (state) => ({ ...state });
export default connect(mapStateToProps)(Item);