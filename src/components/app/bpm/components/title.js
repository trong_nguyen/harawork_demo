import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { settingApp } from '../../../../public';

import { connect } from 'react-redux';

class Title extends Component {

    render() {
        const { title, focused, keyCount } = this.props;
        const count = this.props.bpm.listCount[`${keyCount}`] || 0;
        const { color, colorText } = settingApp;
        const { width } = settingApp;
        return (
            <View style={{ width: (width / 2), height: 35, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: 12, color: focused ? color : colorText }}>
                    {title} ({count})
                </Text>
            </View>
        )
    }
}

const mapStateToProps = (state) => ({ ...state });

export default connect(mapStateToProps)(Title);