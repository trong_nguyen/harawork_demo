import Layout from './layout';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../state/action';

class CheckIn extends Layout {
    constructor(props) {
        super(props);
        this.state ={
            error: false,
            message: ''
        }
    }

    checkMess(data){
        if((data.error == true) && (data.message)){
            this.setState({
                error:true,
                message: data.message
            })
        }
    }
}

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(mapStateToProps, mapDispatchToProps)(CheckIn);