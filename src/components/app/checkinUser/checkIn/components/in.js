import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { settingApp, Utils, SvgCP } from '../../../../../public';

class In extends Component {

    render() {
        const { departmentName, checkinTime, checkinNote, shiftName } = this.props.checkInStatus;
        const isCheckInNote = !!(checkinNote && checkinNote.length && checkinNote.length > 0);
        return (
            <View style={{
                flex: 1,
                marginTop: 15,
                backgroundColor: '#ffffff',
                ...settingApp.shadow
            }}>
                <View
                    style={{
                        flex: 1,
                        flexDirection: 'row',
                        padding: 15,
                        borderBottomWidth: 1,
                        borderBottomColor: settingApp.colorSperator
                    }}>
                    <SvgCP.checkInSuccess />
                    <View style={{ height: 40, marginLeft: 10, justifyContent: 'space-between' }}>
                        <Text style={{ color: '#4CD964', fontSize: 16 }}>
                            Chấm vào ca thành công
                                </Text>
                        <Text style={{ color: '#000', fontSize: 12 }}>
                            Giờ chấm vào: {Utils.formatTime(checkinTime, 'LTS')} - {Utils.formatTime(checkinTime, 'L')}
                        </Text>
                    </View>
                </View>
                <View style={{ flex: 1, padding: 15 }}>
                    <View>
                        <Text style={{ color: '#000', fontSize: 15 }}>Đơn vị chấm công</Text>
                        <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{departmentName}</Text>
                    </View>
                    <View style={{ marginTop: 10 }}>
                        <Text style={{ color: '#000', fontSize: 15 }}>Ca làm việc</Text>
                        <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{shiftName}</Text>
                    </View>
                    {isCheckInNote && <View style={{ marginTop: 10 }}>
                        <Text style={{ color: '#000', fontSize: 15 }}>Ghi chú vào ca</Text>
                        <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{checkinNote}</Text>
                    </View>}
                </View>
            </View>
        )
    }
}

export default In;