import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { HeaderIndex, settingApp } from '../../../../../public';

class Header extends Component {

    render() {
        return (
            <HeaderIndex
                title={
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={settingApp.styleTitle}>
                            Chấm công
                        </Text>
                    </View>
                }
                buttonRight={
                    <View
                        style={{ width: 55, height: 44, }}
                    />
                }
            />
        )
    }
}

export default Header;