import React, { Component } from 'react';
import { Alert, ActivityIndicator, View, Text, TextInput } from 'react-native';
import { ButtonCustom, settingApp, Utils, Toast, SvgCP } from '../../../../../public';
import { HaravanHr } from '../../../../../Haravan';

class Out extends Component {
    constructor(props) {
        super(props);


        this.state = {
            isCheckOut: false,
            time: props.checkInStatus.checkinTime,
            checkoutNote: props.checkInStatus.checkoutNote,
            isLoad: false
        }

        this.infoCheckIn = null;
    }

    async checkIn() {
        const { departmentId, shiftId } = this.props.checkInStatus;
        const note = this.note._lastNativeText ? this.note._lastNativeText.trim() : '';
        const { isIP } = this.props.checkInStatus;
        if (!isIP && (!note || (note && note.length && note.length === 0))) {
            Alert.alert(
                'Thông báo',
                `Bạn đang chấm công ngoài IP cho phép của công ty (${this.props.checkInStatus.clientIp}).\nVui lòng ghi rõ lý do.`,
                [
                    { text: 'Đồng ý', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: false }
            )
            return;
        }

        const data = {
            machineId: "Mobile",
            departmentId,
            shiftId,
            machineEmpId: this.props.id,
            note
        }

        this.setState({ isLoad: true }, async () => {
            const resultCheckOut = await HaravanHr.postCheckIn(data, true);
            const infoCheckIn = await HaravanHr.getCheckInStatus();
            if (resultCheckOut && resultCheckOut.data && !resultCheckOut.error ) {
                if(infoCheckIn && infoCheckIn.data && !infoCheckIn.error){
                    this.infoCheckIn = infoCheckIn.data;
                    this.setState({
                    isCheckOut: true,
                    time: resultCheckOut.data.checkout,
                    checkoutNote: resultCheckOut.data.checkoutNote
                    }, () =>{
                        this.setState({ isLoad: false })
                    });
                }
                else if(infoCheckIn && infoCheckIn.error){
                    this.setState({
                    isCheckOut: true,
                    time: resultCheckOut.data.checkout,
                    checkoutNote: resultCheckOut.data.checkoutNote
                    }, () =>{
                        this.setState({ isLoad: false })
                        this.props.checkMessage(infoCheckIn)
                    });
                }
                
            } else if (resultCheckOut && resultCheckOut.error && resultCheckOut.message &&
                infoCheckIn && infoCheckIn.data && !infoCheckIn.error) {
                Toast.show(resultCheckOut.message);
                this.props.getCheckInStatus(infoCheckIn.data);
            } 
            else {
                Alert.alert(
                    'Thông báo',
                    'Xảy ra lỗi, vui lòng thử lai sau.',
                    [
                        { text: 'Đồng ý', onPress: () => console.log('OK Pressed') },
                    ],
                    { cancelable: false }
                )
            }
            this.setState({ isLoad: false })
        });
    }


    render() {
        const {
            departmentName,
            isIP,
            clientIp,
            checkinNote,
            shiftName
        } = this.props.checkInStatus;

        const { isCheckOut, time, checkoutNote, isLoad } = this.state;
        const isCheckInNote = !!(checkinNote && checkinNote.length && checkinNote.length > 0);
        const isCheckOutNote = !!(checkoutNote && checkoutNote.length && checkoutNote.length > 0);
        return (
            <View>
                <View style={{
                    flex: 1,
                    marginTop: 15,
                    backgroundColor: '#ffffff',
                    ...settingApp.shadow
                }}>
                    <View style={{
                        flex: 1,
                        borderBottomWidth: 1,
                        borderBottomColor: settingApp.colorSperator
                    }}>
                        <View
                            style={{
                                flex: 1,
                                flexDirection: 'row',
                                padding: 15
                            }}>
                            <SvgCP.checkInSuccess />
                            <View style={{ height: 40, marginLeft: 10, justifyContent: 'space-between' }}>
                                <Text style={{ color: '#4CD964', fontSize: 16 }}>
                                    {isCheckOut ? 'Kết thúc' : 'Chấm vào'} ca thành công
                                </Text>
                                <Text style={{ color: '#000', fontSize: 12 }}>
                                    Giờ {isCheckOut ? 'kết thúc' : 'chấm vào'}: {Utils.formatTime(time, 'LTS')} - {Utils.formatTime(time, 'L')}
                                </Text>
                            </View>
                        </View>
                        {(!isIP && !isCheckOut) && <View style={{ flex: 1, paddingLeft: 15, paddingRight: 15, marginTop: 10, marginBottom: 10 }}>
                            <View style={{
                                flex: 1,
                                borderLeftColor: '#FF3B30',
                                borderLeftWidth: 5,
                                padding: 10,
                                paddingLeft: 20,
                                backgroundColor: 'rgba(255, 59, 48, 0.08)'
                            }}>
                                <Text style={{ color: '#212121', fontSize: 13 }}>
                                    {`•  Bạn đang chấm công ngoài IP cho phép của công ty (${clientIp}).\n\nVui lòng ghi rõ lý do.`}
                                </Text>
                            </View>
                        </View>}
                    </View>
                    {!isCheckOut && <View style={{ flex: 1 }}>
                        <View style={{ flex: 1, padding: 15, paddingBottom: 0, height: 120 }}>
                            <TextInput
                                ref={refs => this.note = refs}
                                placeholder='Ghi chú'
                                multiline={true}
                                style={{ flex: 1, maxHeight: 120, textAlignVertical: 'top' }}
                                underlineColorAndroid='transparent'
                                selectionColor={settingApp.color}
                            />
                        </View>
                        <View style={{ flex: 1, paddingLeft: 15, paddingRight: 15, marginTop: 10 }}>
                            <ButtonCustom
                                disabled={isLoad}
                                onPress={() => this.checkIn()}
                                style={{ height: 55, backgroundColor: isLoad ? settingApp.colorDisable : settingApp.color, justifyContent: 'center', alignItems: 'center', borderRadius: 3 }}>
                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={{ fontSize: 15, color: '#ffffff', fontWeight: 'bold', marginRight: 10 }}>
                                        Kết thúc ca
                                    </Text>
                                    {isLoad && <ActivityIndicator size='small' color={settingApp.color} />}
                                </View>
                            </ButtonCustom>
                        </View>
                    </View>}
                    <View style={{ flex: 1, padding: 10 }}>
                        <View>
                            <Text style={{ color: '#000', fontSize: 15 }}>Đơn vị chấm công</Text>
                            <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{departmentName}</Text>
                        </View>
                        <View style={{ marginTop: 10 }}>
                            <Text style={{ color: '#000', fontSize: 15 }}>Ca làm việc</Text>
                            <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{shiftName}</Text>
                        </View>
                        {isCheckInNote && <View style={{ marginTop: 10 }}>
                            <Text style={{ color: '#000', fontSize: 15 }}>Ghi chú vào ca</Text>
                            <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{checkinNote}</Text>
                        </View>}
                        {isCheckOutNote && <View style={{ marginTop: 10 }}>
                            <Text style={{ color: '#000', fontSize: 15 }}>Ghi chú kết thúc</Text>
                            <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{checkoutNote}</Text>
                        </View>}
                    </View>
                </View>

                {isCheckOut && <View style={{ flex: 1, marginTop: 15, padding: 15, backgroundColor: '#ffffff' }}>
                    <ButtonCustom
                        onPress={() => this.props.getCheckInStatus(this.infoCheckIn)}
                        style={{ height: 55, backgroundColor: settingApp.color, justifyContent: 'center', alignItems: 'center', borderRadius: 3 }}>
                        <Text style={{ fontSize: 15, color: '#ffffff', fontWeight: 'bold' }}>
                            Chấm vào ca khác
                        </Text>
                    </ButtonCustom>
                </View>}
            </View>
        )
    }
}

export default Out;