import React, { Component } from 'react';
import { ActivityIndicator, View, Text, TouchableOpacity } from 'react-native';
import { settingApp, Item, Error, Utils } from '../../../../../public';
import { Ionicons } from '@expo/vector-icons';
import { HaravanHr } from '../../../../../Haravan';

class Count extends Component {
    constructor(props) {
        super(props);

        this.state = {
            onLoadEnd: false
        }

        this.notApproved = {
            value: <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                <ActivityIndicator size='small' color={settingApp.color} />
            </View>,
            data: null
        }

        this.reject = {
            value: <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                <ActivityIndicator size='small' color={settingApp.color} />
            </View>,
            data: null
        }

        this.noneCheckIn = {
            value: <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                <ActivityIndicator size='small' color={settingApp.color} />
            </View>,
            data: null
        }
    }

    componentDidMount() {
        this.loadData();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.checkIn &&
            nextProps.checkIn.checkInStatus &&
            this.props.checkIn && 
            this.props.checkIn.checkInStatus &&
            nextProps.checkIn.checkInStatus.status !== this.props.checkIn.checkInStatus.status) {
            this.loadData();
        }
    }

    async loadData() {
        const date = new Date();
        const firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        const fromDate = Utils.formatTime(firstDay, 'YYYY-MM-DD');
        const toDate = Utils.formatTime(date, 'YYYY-MM-DD');
        const resultHistory = await HaravanHr.getCheckinHistory(`checkin/history?`, `fromDate=${fromDate}&toDate=${toDate}`);
        const resultNotCheckIn = await HaravanHr.getCheckinHistory(`checkin/distributed?`, `fromDate=${fromDate}&toDate=${toDate}`);
        if ((resultHistory && resultHistory.data && !resultHistory.error) &&
            (resultNotCheckIn && resultNotCheckIn.data && !resultNotCheckIn.error)) {
            let countNoneApproved = 0;
            let countReject = 0;
            const { data } = resultHistory;
            for (const item of data) {
                if (item.status === 1 || item.status === 2) {
                    countNoneApproved = countNoneApproved + 1;
                } else if (item.status === 4) {
                    countReject = countReject + 1;
                }
            }

            this.notApproved = {
                value: (
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('HistoryTab', { activeTab: 'Second' })}
                        style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 10 }}>
                            <View style={{ padding: 2, paddingLeft: 7, paddingRight: 7, borderRadius: 10, backgroundColor: '#FF9500' }}>
                                <Text style={{ color: '#ffffff', fontSize: 14 }} numberOfLines={1}>{countNoneApproved}</Text>
                            </View>
                        </View>
                        <Ionicons name='ios-arrow-forward' size={23} color='#D1D1D6' />
                    </TouchableOpacity>
                ),
                data: null
            }

            this.reject = {
                value: (
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('HistoryTab', { activeTab: 'Third' })}
                        style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 10 }}>
                            <View style={{ padding: 2, paddingLeft: 7, paddingRight: 7, borderRadius: 10, backgroundColor: '#FF3B30' }}>
                                <Text style={{ color: '#ffffff', fontSize: 14 }} numberOfLines={1}>{countReject}</Text>
                            </View>
                        </View>
                        <Ionicons name='ios-arrow-forward' size={23} color='#D1D1D6' />
                    </TouchableOpacity>
                ),
                data: null
            }

            this.noneCheckIn = {
                value: (
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('NotCheckIn')}
                        style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 10 }}>
                            <View style={{ padding: 2, paddingLeft: 7, paddingRight: 7, borderRadius: 10, backgroundColor: '#8E8E93' }}>
                                <Text style={{ color: '#ffffff', fontSize: 14 }} numberOfLines={1}>{resultNotCheckIn.data.length}</Text>
                            </View>
                        </View>
                        <Ionicons name='ios-arrow-forward' size={23} color='#D1D1D6' />
                    </TouchableOpacity>
                ),
                data: null
            }
        } else {
            this.notApproved = {
                value: (
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                        <Error />
                    </View>
                ),
                data: null
            }

            this.reject = {
                value: (
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                        <Error />
                    </View>
                ),
                data: null
            }

            this.noneCheckIn = {
                value: (
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                        <Error />
                    </View>
                ),
                data: null
            }
        }

        this.setState({
            onLoadEnd: true
        });
    }

    render() {
        const { notApproved, reject, noneCheckIn } = this;
        return (
            <View style={{ flex: 1, paddingLeft: 15, backgroundColor: '#ffffff', ...settingApp.shadow }}>
                <Item
                    name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Ca chưa được duyệt</Text>}
                    value={notApproved.value}
                    noneBorder={true}
                />
                <Item
                    name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Ca bị từ chối</Text>}
                    value={reject.value}
                />
                <Item
                    name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Ca chưa chấm công</Text>}
                    value={noneCheckIn.value}
                />
            </View>
        )
    }
}

export default Count;