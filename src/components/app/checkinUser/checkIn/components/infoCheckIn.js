import React, { Component } from 'react';
import { Alert, ActivityIndicator, View, Text, TouchableOpacity, TextInput, AppState , UIManager, LayoutAnimation, NetInfo, Platform} from 'react-native';
import { ButtonCustom, settingApp, Item, Error, Toast, Utils } from '../../../../../public';
import { HaravanHr } from '../../../../../Haravan';
import { MaterialIcons } from '@expo/vector-icons';
import { StackActions, NavigationActions } from 'react-navigation';

class InfoCheckIn extends Component {
    constructor(props) {
        super(props);

        this.state = {
            indexSelectDeparment: null,
            indexSelectShift: null,
            isLoadEnd: false,
            isLoad: false,
            index: null,
            isDitribute: true,
            checkIp: true,
            refreshIp: false,
        }
        _seft = this;
        this.dataDeparment = [];
        this.dataShift = [];
        this.Ip = null;
        NetInfo.addEventListener('connectionChange', () =>this.connectionChange());
    }   

    

    checkData(input) {
        if (input && input.data && !input.error) {
            return input;
        }
        return false;
    }

     connectionChange() {
        this.setState({refreshIp:true}, () =>{
            let newIp = null;
            let infoCheckIn = null;
            const checkIp = setInterval( async() => {
                if(!newIp || newIp === _seft.Ip) {
                    infoCheckIn = await HaravanHr.getCheckInStatus();
                    if(infoCheckIn && infoCheckIn.data && !infoCheckIn.error) {
                        newIp = infoCheckIn.data.clientIp;
                    }
                } else {
                    _seft.Ip = newIp;
                    _seft.setState({refreshIp:false });
                    await _seft.checkIpUser(infoCheckIn);
                    clearInterval(checkIp);
                }
            }, 2000);
        })
    }

    async componentDidMount() {
        const isDitribute = await HaravanHr.getMeDitribute();
        const infoCheckIn = await HaravanHr.getCheckInStatus();
        if(isDitribute && isDitribute.data && !isDitribute.error){
            if( isDitribute.data.isDitribute == true){
                if(infoCheckIn && infoCheckIn.data && !infoCheckIn.error){
                    this.Ip = infoCheckIn.data.clientIp;
                    const listDepartment = await HaravanHr.getDepartmentsCheckin();
                    const listShift = await HaravanHr.getShiftsCheckIn(infoCheckIn.data.departmentId);
                    this.dataDeparment = this.checkData(listDepartment);
                    if(listShift && listShift.data && !listShift.error){
                        this.dataShift = listShift.data.shift;
                    }
                    this.setState({isDitribute: true, checkIp:infoCheckIn.data.isIP})
                }
            }
            else{
                const listDepartment = await HaravanHr.getDepartments();
                const listShift = await HaravanHr.getUserShifts();
                this.dataDeparment = this.checkData(listDepartment);
                if(listShift && listShift.data && !listShift.error){
                    this.dataShift = listShift.data.data;
                }
                this.setState({isDitribute: false, checkIp:infoCheckIn.data.isIP})
            }
            this.setState({refreshIp:false},() =>{
                this.loadData(infoCheckIn.data);
            })  
        }
        else{
            Toast.show(infoCheckIn.message)
            this.setState({
                isLoad:false,
                isLoadEnd:true
            })
        }   
    }

    async checkIpUser(infoCheckIn){
        if(infoCheckIn === undefined) {
            infoCheckIn = await HaravanHr.getCheckInStatus();
        }
        if(infoCheckIn && infoCheckIn.data && !infoCheckIn.error){
            const checkIp = await HaravanHr.getisIp(infoCheckIn.data.clientIp, infoCheckIn.data.departmentId)
            if(checkIp && checkIp.data && !checkIp.error){
                this.setState({checkIp: checkIp.data.isIp})
            }
            else{
                this.setState({checkIp: infoCheckIn.data.isIP})
            }
            this.setState({refreshIp:false})
        }
    }

    async setListShift(){
        const { departmentId, clientIp, isIP } = this.props.checkInStatus;
        const { dataDeparment } = this;
        const { indexSelectDeparment, index, isDitribute } = this.state;
        const checkIp = await HaravanHr.getisIp(clientIp, departmentId)
        if(checkIp && checkIp.data && !checkIp.error){
            this.setState({checkIp: checkIp.data.isIp})
        }else{
            this.setState({checkIp:isIP})
        }
    

        if( (indexSelectDeparment != null) && (index !== indexSelectDeparment) && (isDitribute==true) ){
            const listShift = await HaravanHr.getShiftsCheckIn(dataDeparment.data[indexSelectDeparment].id);
            if(listShift && listShift.data && !listShift.error){
                this.dataShift = listShift.data.shift;
            }
            this.loadShift(indexSelectDeparment)
        }
        else{
            this.setState({isLoad:false})
        }
    }

    loadShift(indexSelectDeparment){
        const { dataShift } = this;
        const { shiftId } = this.props.checkInStatus;
        const indexSelectShift = dataShift.findIndex(e => e.id === shiftId);
        this.setState({
            indexSelectShift,
            isLoadEnd: true,
            index: indexSelectDeparment,
            isLoad:false
        });
    }

    async checkIn() {
        const { indexSelectDeparment, indexSelectShift, checkIp } = this.state;
        const note = this.note._lastNativeText ? this.note._lastNativeText.trim() : null;
        const { isIP } = this.props.checkInStatus;
        if (!checkIp && (!note || (note && note.length && note.length === 0))) {
            Alert.alert(
                'Thông báo',
                `Bạn đang chấm công ngoài IP cho phép của công ty (${this.props.checkInStatus.clientIp}).\nVui lòng ghi rõ lý do.`,
                [
                    { text: 'Đồng ý', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: false }
            )
            return;
        }

        if ((!indexSelectDeparment && indexSelectDeparment !== 0) || indexSelectDeparment === -1 || !this.dataDeparment.data[indexSelectDeparment].id) {
            Alert.alert(
                'Thông báo',
                'Không tìm thấy đơn vị chấm công, vui lòng thử lại sau.',
                [
                    { text: 'Đồng ý', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: false }
            )
            return;
        } else if ((!indexSelectShift && indexSelectShift !== 0) || indexSelectShift === -1 || !this.dataShift[indexSelectShift].id) {
            Alert.alert(
                'Thông báo',
                'Không tìm thấy ca làm việc, vui lòng thử lại sau.',
                [
                    { text: 'Đồng ý', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: false }
            )
            return;
        }

        const data = {
            machineId: "Mobile",
            departmentId: this.dataDeparment.data[indexSelectDeparment].id,
            shiftId: this.dataShift[indexSelectShift].id,
            machineEmpId: this.props.id,
            note
        }
        this.setState({ isLoad: true }, async () => {
            const resultCheckIn = await HaravanHr.postCheckIn(data, false);
            const infoCheckIn = await HaravanHr.getCheckInStatus();
            if (resultCheckIn && resultCheckIn.data && !resultCheckIn.error &&
                infoCheckIn && infoCheckIn.data && !infoCheckIn.error) {
            } else if (resultCheckIn && resultCheckIn.error && resultCheckIn.message &&
                infoCheckIn && infoCheckIn.data && !infoCheckIn.error) {
                Toast.show(resultCheckIn.message);
            } else {
                Alert.alert(
                    'Thông báo',
                    'Xảy ra lỗi, vui lòng thử lai sau.',
                    [
                        { text: 'Đồng ý', onPress: () => console.log('OK Pressed') },
                    ],
                    { cancelable: false }
                )
            }
            this.setState({ isLoad: false },() =>{
                this.props.getCheckInStatus(infoCheckIn.data);
            });
        });
    }

  
    async loadData(infoCheckIn) {
        const { dataDeparment, dataShift } = this;
        const { departmentId, shiftId } = this.props.checkInStatus;
        let { indexSelectShift} = this.state
        if(infoCheckIn){
            indexSelectShift = dataShift.findIndex(e => e.id === infoCheckIn.shiftId);
        }
        const indexSelectDeparment = dataDeparment.data.findIndex(e => e.id === departmentId);
        this.setState({
            indexSelectDeparment,
            indexSelectShift,
            isLoadEnd: true,
            index: indexSelectDeparment,
            isLoad: false
        });
    }

    render() {
        const { colorSperator } = settingApp;
        const { indexSelectDeparment, indexSelectShift, isLoadEnd, isLoad, checkIp, refreshIp } = this.state;
        const { dataDeparment, dataShift } = this;
        let nameDepartmentSelet = ((indexSelectDeparment || (indexSelectDeparment === 0)) && (indexSelectDeparment > -1)) ? dataDeparment.data[indexSelectDeparment].name : null;
        let nameShiftSelect = ((indexSelectShift || (indexSelectShift == 0) ) && (indexSelectShift > -1)) ? dataShift[indexSelectShift].name : null;
        const { isIP, clientIp } = this.props.checkInStatus;
        return (
            <View style={{
                flex: 1,
                marginTop: 15,
                paddingLeft: 15,
                backgroundColor: '#ffffff',
                ...settingApp.shadow
            }}>
                {(!checkIp) && <View style={{ flex: 1, paddingRight: 15, marginTop: 10, marginBottom: 10 }}>
                        <View style={{
                        flex: 1,
                        borderLeftColor: '#FF3B30',
                        borderLeftWidth: 5,
                        padding: 10,
                        paddingLeft: 20,
                        backgroundColor: 'rgba(255, 59, 48, 0.08)'
                    }}>
                        <Text style={{ color: '#212121', fontSize: 13 }}>
                            {`• Bạn đang chấm công ngoài IP cho phép của công ty (${clientIp}).\n\nVui lòng ghi rõ lý do.`}
                        </Text>
                        {refreshIp ? 
                            <View style={{flexDirection:'row', marginTop:10}}>
                                <ActivityIndicator size='small' color={settingApp.colorDisable} />
                                <Text style={{ color:settingApp.colorDisable, fontSize: 13, paddingLeft:5 }}>
                                    {`Đang thay đổi địa chỉ IP. Vui lòng chờ.`}
                                </Text> 
                            </View>
                        : <View/>
                        }
                        
                    </View>
                </View>}

                <Item
                    name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Đơn vị chấm công <Text style={{ color: '#FF3B30' }}>*</Text></Text>}
                    value={isLoadEnd ? (dataDeparment && dataDeparment.data ? (
                        <TouchableOpacity
                            onPress={
                                () =>
                                    this.props.navigation.navigate('ItemSelect',
                                        {
                                            data: dataDeparment.data,
                                            title: 'Đơn vị chấm công',
                                            selected: this.state.indexSelectDeparment,
                                            actions: index => this.setState({ indexSelectDeparment: index , isLoad:true}, () =>{
                                                this.setListShift()
                                            })
                                        }
                                    )
                            }
                            style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 10 }}>
                                <Text style={{ fontSize: 15, color: '#8E8E93' }} numberOfLines={1}>{nameDepartmentSelet}</Text>
                            </View>
                            <MaterialIcons name='keyboard-arrow-right' size={23} color='#D1D1D6' />
                        </TouchableOpacity>
                    ) : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                            <Error />
                        </View> ): <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                            <ActivityIndicator size='small' color={settingApp.color} />
                        </View>
                    }
                    noneBorder={true}
                />
                <Item
                    name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Ca làm việc <Text style={{ color: '#FF3B30' }}>*</Text></Text>}
                    value={isLoadEnd ? ((dataShift.length > 0) ? (
                        <TouchableOpacity
                            onPress={
                                () =>
                                    this.props.navigation.navigate('ItemSelect',
                                        {
                                            data: dataShift,
                                            title: 'Ca làm việc',
                                            selected: this.state.indexSelectShift,
                                            actions: index => this.setState({ indexSelectShift: index })
                                        }
                                    )
                            }
                            style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 10 }}>
                                <Text style={{ fontSize: 15, color: '#8E8E93' }} numberOfLines={1}>{nameShiftSelect}</Text>
                            </View>
                            <MaterialIcons name='keyboard-arrow-right' size={23} color='#D1D1D6' />
                        </TouchableOpacity>
                    ) : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                            <Error />
                        </View>) : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                            <ActivityIndicator size='small' color={settingApp.color} />
                        </View>
                    }
                />
                <View style={{ paddingRight: 15, borderTopColor: colorSperator, borderTopWidth: 1 }}>
                    <View style={{ flex: 1, height: 120, marginTop: 10 }}>
                        <TextInput
                            ref={refs => this.note = refs}
                            placeholder='Ghi chú'
                            multiline={true}
                            style={{ flex: 1, maxHeight: 120, textAlignVertical: 'top' }}
                            underlineColorAndroid='transparent'
                            selectionColor={settingApp.color}
                        />
                    </View>
                </View>

                <View style={{ flex: 1, paddingRight: 15, marginTop: 10, marginBottom: 15 }}>
                    <ButtonCustom
                        disabled={!isLoadEnd || isLoad}
                        onPress={() => this.checkIn()}
                        style={{ height: 55, backgroundColor: (!isLoadEnd || isLoad) ? settingApp.colorDisable : settingApp.color, justifyContent: 'center', alignItems: 'center', borderRadius: 3 }}>
                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{ fontSize: 15, color: '#ffffff', fontWeight: 'bold', marginRight: 10 }}>
                                Vào ca
                            </Text>
                            {(!isLoadEnd || isLoad) && <ActivityIndicator size='small' color={settingApp.color} />}
                        </View>
                    </ButtonCustom>
                </View>
            </View>
        )
    }
}

export default InfoCheckIn;