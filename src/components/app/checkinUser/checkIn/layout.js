import React, { Component } from 'react';
import { ActivityIndicator, View, Text, ScrollView, KeyboardAvoidingView } from 'react-native';
import { settingApp } from '../../../../public';
import { Ionicons } from '@expo/vector-icons';

import Header from './components/header';
import Count from './components/count';
import InfoCheckIn from './components/infoCheckIn';
import In from './components/in';
import Out from './components/out';

class Layout extends Component {

    renderContent() {
        let content = <ActivityIndicator color={settingApp.color} size='large' style={{ marginTop: (settingApp.height * 0.25) }} />
        if (this.props.checkIn.checkInStatus) {
            let { status, isMustCheckout, error, message } = this.props.checkIn.checkInStatus;
            if (error) {
                content = (
                    <View style={{ marginTop: 10, height: 80, backgroundColor: '#ffffff', ...settingApp.shadow }}>
                        <View
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                flexDirection: 'row',
                                backgroundColor: 'transparent',
                                height:80,
                                width:settingApp.width 
                            }}>
                            <Ionicons name='ios-warning' color='#fd7e14' size={23} />
                            <Text style={{ color: settingApp.colorText, fontSize: 14, marginLeft: 10, width:(settingApp.width - 60), flexWrap:'wrap'}}>
                                {message}
                            </Text>
                        </View>
                    </View>
                )
            } else if (status === 0) {
                content = <InfoCheckIn
                    checkInStatus={this.props.checkIn.checkInStatus}
                    navigation={this.props.navigation}
                    id={this.props.app.colleague.id}
                    getCheckInStatus={this.props.actions.checkIn.getCheckInStatus}
                />
            } else {
                if (isMustCheckout) {
                    content = <Out
                        checkInStatus={this.props.checkIn.checkInStatus}
                        id={this.props.app.colleague.id}
                        getCheckInStatus={this.props.actions.checkIn.getCheckInStatus}
                        checkMessage = {(message) => this.checkMess(message)}
                    />;
                } else {
                    content = <In checkInStatus={this.props.checkIn.checkInStatus} />;
                }
            }
        }
        else{
            const { error, message} = this.state
            if(error && message){
                content = (
                    <View style={{ marginTop: 10, height: 80, backgroundColor: '#ffffff', ...settingApp.shadow }}>
                        <View
                            style={{
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center',
                                flexDirection: 'row',
                                backgroundColor: 'transparent',
                            }}>
                            <Ionicons name='ios-warning' color='#fd7e14' size={23} />
                            <Text style={{ color: settingApp.colorText, fontSize: 14, textAlign: 'center', marginLeft: 10 }}>
                                {message}
                            </Text>
                        </View>
                    </View>
                )
            }
        }
        return (
            <ScrollView style={{ paddingTop: 15 }}>
                <Count {...this.props} />
                {content}
                <View style={{ flex: 1, height: 30 }} />
            </ScrollView>
        )
    }

    render() {
        return (
            <KeyboardAvoidingView behavior='padding' style={{ flex: 1 }}>
                <Header />
                <View style={{ flex: 1 }}>
                    {this.renderContent()}
                </View>
            </KeyboardAvoidingView>
        )
    }
}

export default Layout;