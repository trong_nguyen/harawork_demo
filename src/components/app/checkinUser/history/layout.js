import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity } from 'react-native';
import Header from './components/header';
import { EvilIcons } from '@expo/vector-icons';
import { settingApp, Arrow } from '../../../../public';
import Collapsible from 'react-native-collapsible';
import Condition from './components/condition';
import TabHistory from '../../../../route/components/checkIn/tabHistory';

class Layout extends Component {

    renderButton() {
        const { isCollapsed } = this.state;
        return (
            <View style={{ flexDirection: 'row', paddingLeft: 10, paddingRight: 10 }}>
                <View style={{ flex: 1 }}>
                    {this.state.removeFilter && <TouchableOpacity
                        onPress={() => this.condition.setInitCondition()}
                        style={{ flexDirection: 'row', alignItems: 'center', height: 50 }}>
                        <EvilIcons name='close' color='#FF3B30' size={30} />
                        <Text style={{ color: '#FF3B30', fontSize: 16, marginLeft: 5 }}>Đặt lại bộ lọc</Text>
                    </TouchableOpacity>}
                </View>
                <View style={{ flex: 1, alignItems: 'flex-end' }}>
                    <TouchableOpacity
                        onPress={() => this.setState({ isCollapsed: !this.state.isCollapsed })}
                        style={{ flexDirection: 'row', alignItems: 'center', height: 50 }}>
                        <Text style={{ color: settingApp.color, fontSize: 16, marginRight: 5 }}>
                            {!isCollapsed ? 'Bộ lọc nâng cao' : 'Thu gọn bộ lọc'}
                        </Text>
                        <View>
                            <Arrow isOpen={isCollapsed} color={settingApp.color} />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    renderContent() {
        const { isCollapsed } = this.state;
        return (
            <View style={{ flex: 1 }}>
                {this.renderButton()}
                <Collapsible duration={350} collapsed={!isCollapsed} style={{ paddingTop: 1, paddingBottom: 1 }}>
                    <Condition
                        ref={refs => this.condition = refs}
                        setConditon={
                            condition => this.setState({ isCollapsed: false },
                                () => this.setState({ condition }))
                        }
                        removeFilter={this.state.removeFilter}
                        changeRemoveFilter={removeFilter => this.setState({ removeFilter })}
                        {...this.props}
                    />
                </Collapsible>
                <TabHistory
                    ref={refs => this.tabHistory = refs}
                    screenProps={{
                        condition: this.state.condition,
                        mainNavigation: this.props.navigation
                    }}
                />
            </View>
        )
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Header />
                <View style={{ flex: 1 }}>
                    {this.renderContent()}
                </View>
            </View>
        )
    }
}

export default Layout;