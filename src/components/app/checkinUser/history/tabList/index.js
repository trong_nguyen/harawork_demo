import Layout from './layout';
import { HaravanHr } from '../../../../../Haravan';
import { Utils } from '../../../../../public';

import { connect } from 'react-redux';

class TabList extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            list: [],
            isLoadEnd: false,
            refreshing: false,

            totalActualTimekeepings: null,
            totalApproveTimekeeping: null,
            totalHourShiftDistributions: null,
            totalRejectTimeKeeping: null,
            isLoadTotalError: false
        }

        this.url = props.url;
        this.condition = props.condition;
        this.navigation = props.mainNavigation;
    }

    async componentDidMount() {
        this.checkCondition();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.condition !== this.condition) {
            this.condition = nextProps.condition;
            this.checkCondition(nextProps.condition);
        }
        if (nextProps.checkIn &&
            nextProps.checkIn.checkInStatus &&
            this.props.checkIn && 
            this.props.checkIn.checkInStatus &&
            nextProps.checkIn.checkInStatus.status !== this.props.checkIn.checkInStatus.status) {
            this.checkCondition(this.condition);
        }
    }

    async loadData(condition) {
        const result = await HaravanHr.getCheckinHistory(this.url, condition);
        if (result && result.data && !result.error) {
            const count = result.data.length;
            this.props.navigation.setParams({ count });
            this.setState({ list: result.data, isLoadEnd: true, refreshing: false });
        } else {
            this.setState({ isLoadEnd: true, refreshing: false })
        }
    }

    convertHousr(input) {
        if (input && typeof (input) === 'number') {
            input = input.toFixed(1);
        } else {
            totalActualTimekeepings = null;
        }
        return input;
    }

    async loadTotalHours(condition) {
        const result = await HaravanHr.gettotaltimeworking(condition);
        if (result && result.data && !result.error) {
            let {
                totalActualTimekeepings,
                totalApproveTimekeeping,
                totalHourShiftDistributions,
                totalRejectTimeKeeping
            } = result.data;
            totalActualTimekeepings = this.convertHousr(totalActualTimekeepings);
            totalApproveTimekeeping = this.convertHousr(totalApproveTimekeeping);
            totalHourShiftDistributions = this.convertHousr(totalHourShiftDistributions);
            totalRejectTimeKeeping = this.convertHousr(totalRejectTimeKeeping);
            this.setState({
                totalActualTimekeepings,
                totalApproveTimekeeping,
                totalHourShiftDistributions,
                totalRejectTimeKeeping,
                isLoadTotalError: false
            })
        } else {
            this.setState({ isLoadTotalError: true })
        }
    }

    checkCondition(condition) {
        if (condition) {
            this.loadTotalHours(condition)
            this.loadData(condition);
        } else {
            const date = new Date();
            const formDate = new Date(date.getFullYear(), date.getMonth(), 1);
            condition = `fromDate=${Utils.formatTime(formDate, 'YYYY-MM-DD', true)}&toDate=${Utils.formatTime(date, 'YYYY-MM-DD', true)}`;
            this.loadTotalHours(condition)
            this.loadData(condition);
        }
    }

}

const mapStateToProps = (state) => ({ ...state });

export default connect(mapStateToProps)(TabList);