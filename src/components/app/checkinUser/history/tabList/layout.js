import React, { Component } from 'react';
import { View, Text, FlatList, RefreshControl } from 'react-native';
import { settingApp, Loading, NotFound, Item as ItemTotalHours, Error } from '../../../../../public';
import Item from '../components/item';

class Layout extends Component {

    renderTotalHours() {
        const {
            isLoadTotalError,
            totalActualTimekeepings,
            totalApproveTimekeeping,
            totalHourShiftDistributions,
            totalRejectTimeKeeping,
        } = this.state;
        return (
            <View style={{
                flex: 1,
                backgroundColor: '#ffffff',
                paddingLeft: 15,
                paddingRight: 15,
                marginBottom: 20,
                marginTop: 5,
                ...settingApp.shadow
            }}>
                <ItemTotalHours
                    name={<Text style={{ fontSize: 15, color: '#212121' }} numberOfLines={1}>Giờ công được chia</Text>}
                    value={<View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                        {!isLoadTotalError ? <View style={{
                            backgroundColor: '#8E8E93',
                            padding: 5,
                            paddingLeft: 10,
                            paddingRight: 10,
                            borderRadius: 15
                        }}>
                            <Text style={{ fontSize: 13, color: '#ffffff' }} numberOfLines={1}>
                                {totalHourShiftDistributions} giờ
                                </Text>
                        </View> : <Error />}
                    </View>}
                    noneBorder={true}
                />
                <ItemTotalHours
                    name={<Text style={{ fontSize: 15, color: '#212121' }} numberOfLines={1}>Giờ công đã chấm</Text>}
                    value={<View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                        {!isLoadTotalError ? <View style={{
                            backgroundColor: '#21469B',
                            padding: 5,
                            paddingLeft: 10,
                            paddingRight: 10,
                            borderRadius: 15
                        }}>
                            <Text style={{ fontSize: 13, color: '#ffffff' }} numberOfLines={1}>
                                {totalActualTimekeepings} giờ
                            </Text>
                        </View> : <Error />}
                    </View>}
                />
                <ItemTotalHours
                    name={<Text style={{ fontSize: 15, color: '#212121' }} numberOfLines={1}>Giờ công đã duyệt</Text>}
                    value={<View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                        {!isLoadTotalError ? <View style={{
                            backgroundColor: '#4CD964',
                            padding: 5,
                            paddingLeft: 10,
                            paddingRight: 10,
                            borderRadius: 15
                        }}>
                            <Text style={{ fontSize: 13, color: '#ffffff' }} numberOfLines={1}>
                                {totalApproveTimekeeping} giờ
                            </Text>
                        </View> : <Error />}
                    </View>}
                />
                <ItemTotalHours
                    name={<Text style={{ fontSize: 15, color: '#212121' }} numberOfLines={1}>Giờ công đã từ chối</Text>}
                    value={<View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                        {!isLoadTotalError ? <View style={{
                            backgroundColor: '#FF3B30',
                            padding: 5,
                            paddingLeft: 10,
                            paddingRight: 10,
                            borderRadius: 15
                        }}>
                            <Text style={{ fontSize: 13, color: '#ffffff' }} numberOfLines={1}>
                                {totalRejectTimeKeeping} giờ
                            </Text>
                        </View> : <Error />}
                    </View>}
                />
            </View>
        )
    }

    render() {
        const { list, isLoadEnd } = this.state;
        if (!isLoadEnd) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Loading />
                </View>
            )
        } else {
            return (
                <View style={{ flex: 1, paddingTop: 15 }}>
                    <FlatList
                        refreshControl={<RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={() => {
                                this.setState({ refreshing: true }, () =>
                                    this.checkCondition(this.condition))
                            }}
                        />}
                        data={list}
                        extraData={this.state}
                        keyExtractor={(item, index) => `${item.id}`}
                        renderItem={obj => <Item obj={{ ...obj }} navigation={this.navigation} />}
                        contentContainerStyle={{ flexGrow: 1, paddingBottom: 2, ...settingApp.shadow }}
                        ListHeaderComponent={this.renderTotalHours()}
                        ListEmptyComponent={<NotFound />}
                    />
                </View>
            )
        }
    }
}

export default Layout;