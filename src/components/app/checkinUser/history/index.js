import Layout from './layout';

class History extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            isCollapsed: false,
            removeFilter: false,
            condition: null
        }
    }

    componentDidMount() {
        this.activeTab(this.props.navigation.state.params);
    }

    componentWillReceiveProps(nextProps) {
        this.activeTab(nextProps.navigation.state.params);
    }

    activeTab(params) {
        if (params && params.activeTab) {
            const { activeTab } = params;
            this.tabHistory._navigation.navigate(activeTab);
        }
    }

}

export default History;