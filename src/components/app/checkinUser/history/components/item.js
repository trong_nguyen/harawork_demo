import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { ButtonCustom, settingApp, Utils } from '../../../../../public';
import { MaterialIcons } from '@expo/vector-icons';

class Item extends Component {

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (nextProps.obj.index === 1 || JSON.stringify(this.props.obj.item) !== JSON.stringify(nextProps.obj.item)) {
            return true;
        }
        return false;
    }

    checkStatus(status) {
        if (status === 1 || status === 2) {
            return { title: 'Chưa duyệt', color: '#FF9500' }
        } else if (status === 3) {
            return { title: 'Đã duyệt', color: '#4CD964' }
        } else if (status === 4) {
            return { title: 'Từ chối', color: '#FF3B30' }
        } else {
            return null;
        }
    }

    checkWorkingTime(item) {
        const { status, actualWorkingHours, approvedWorkingHours } = item;
        if (status === 1 || status === 2) {
            return actualWorkingHours;
        } else if (status === 3) {
            return approvedWorkingHours;
        } else if (status === 4) {
            return actualWorkingHours;
        } else {
            return 0;
        }
    }

    render() {
        const { colorSperator } = settingApp;
        const { item, index } = this.props.obj;
        const time = Utils.formatTime(item.checkin, 'dddd, DD/MM/YYYY', true);;
        const titleTime = time.charAt(0).toUpperCase() + time.slice(1);
        const titleStatus = this.checkStatus(item.status);
        let workingTime = this.checkWorkingTime(item);
        workingTime = workingTime.toFixed(1);
        if (titleStatus) {
            return (
                <ButtonCustom
                    onPress={() => this.props.navigation.navigate('DetailCheckIn', { item })}
                    style={{ flex: 1, paddingLeft: 15, backgroundColor: '#ffffff' }}>
                    <View style={{
                        paddingRight: 15,
                        paddingTop: 10,
                        paddingBottom: 10,
                        borderTopColor: colorSperator,
                        borderTopWidth: index === 0 ? 0 : 1
                    }}>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ flex: 1 }}>
                                <Text style={{ color: '#000', fontSize: 15 }} numberOfLines={1}>{titleTime}</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end' }}>
                                <Text style={{ color: titleStatus.color, fontSize: 15, marginRight: 5 }}>{titleStatus.title}</Text>
                                <MaterialIcons name='keyboard-arrow-right' size={23} color='#D1D1D6' />
                            </View>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <Text
                                    style={{ color: '#8E8E93', fontSize: 13, marginTop: 5 }}
                                    numberOfLines={1}
                                >
                                    {item.shiftName}
                                </Text>
                            </View>
                            <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                <Text
                                    style={{ color: '#8E8E93', fontSize: 13, marginTop: 5, marginRight: 10 }}
                                    numberOfLines={1}
                                >
                                    [{workingTime} giờ]
                            </Text>
                            </View>
                        </View>
                    </View>
                </ButtonCustom>
            )
        } else {
            return <View />
        }
    }
}

export default Item;