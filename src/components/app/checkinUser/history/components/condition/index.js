import Layout from './layout';
import { HaravanHr } from '../../../../../../Haravan';
import { Utils } from '../../../../../../public';

class Condition extends Layout {
    constructor(props) {
        super(props);

        const date = new Date();
        const formDate = new Date(date.getFullYear(), date.getMonth(), 1);
        const todate = new Date();

        this.state = {
            valueFromDate: formDate,
            isVisibleFormDate: false,
            valueTodate: todate,
            isVisibleToDate: false,

            indexSelectDeparment: null,
            indexSelectShift: null,
            isLoadEnd: false
        }

        this.dataDeparment = [];
        this.dataShift = [];

        this.formDate = formDate;
        this.todate = todate;
    }

    checkData(input) {
        if (input && input.data && !input.error) {
            return input;
        }
        return false;
    }

    async componentDidMount() {
        const listDepartment = await HaravanHr.getDepartments();
        const listShift = await HaravanHr.getShifts();

        this.dataDeparment = this.checkData(listDepartment);
        this.dataShift = this.checkData(listShift);

        this.setState({ isLoadEnd: true });
    }



    validate(type, value) {
        if (type === 'fromDate') {
            if (!!(value <= this.state.valueTodate)) {
                this.setState({ valueFromDate: value })
            } else {
                this.setState({ valueFromDate: this.state.valueTodate })
            }
        } else {
            if (!!(value >= this.state.valueFromDate)) {
                this.setState({ valueTodate: value })
            } else {
                this.setState({ valueTodate: this.state.valueFromDate });
            }
        }
    }

    formatTime(value) {
        return Utils.formatTime(value, 'DD/MM/YYYYY');
    }

    setInitCondition() {
        this.setState({
            valueFromDate: this.formDate,
            valueTodate: this.todate,

            indexSelectDeparment: null,
            indexSelectShift: null
        });

        this.props.setConditon(null);
    }

    componentDidUpdate() {
        const { valueFromDate, valueTodate, indexSelectDeparment, indexSelectShift } = this.state;
        if (this.formatTime(valueFromDate) === this.formatTime(this.formDate) &&
            this.formatTime(valueTodate) === this.formatTime(this.todate) &&
            (indexSelectDeparment !== 0 && !indexSelectDeparment) &&
            (indexSelectShift !== 0 && !indexSelectShift) && this.props.removeFilter) {
            this.props.changeRemoveFilter(false);
        } else if ((this.formatTime(valueFromDate) !== this.formatTime(this.formDate) ||
            this.formatTime(valueTodate) !== this.formatTime(this.todate) ||
            (indexSelectDeparment === 0 || indexSelectDeparment) ||
            (indexSelectShift === 0 || indexSelectShift)) && !this.props.removeFilter) {
            this.props.changeRemoveFilter(true);
        }
    }

    submit() {
        let { valueFromDate, valueTodate, indexSelectDeparment, indexSelectShift } = this.state;
        valueFromDate = valueFromDate ? Utils.formatTime(valueFromDate, 'YYYY-MM-DD', true) : null;
        valueTodate = valueTodate ? Utils.formatTime(valueTodate, 'YYYY-MM-DD', true) : null;
        const departmentId = (indexSelectDeparment || indexSelectDeparment === 0) ? this.dataDeparment.data[indexSelectDeparment].id : null;
        const shiftId = (indexSelectShift || indexSelectShift === 0) ? this.dataShift.data.data[indexSelectShift].id : null;
        const condition = `fromDate=${valueFromDate}&toDate=${valueTodate}&departmentId=${departmentId}&shiftId=${shiftId}`;
        this.props.setConditon(condition);
    }
}

export default Condition;