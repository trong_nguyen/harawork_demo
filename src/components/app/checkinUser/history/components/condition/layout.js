import React, { Component } from 'react';
import { ActivityIndicator, View, Text, TouchableOpacity, Image, ScrollView } from 'react-native';
import { Item, settingApp, imgApp, Utils, Error } from '../../../../../../public';
import { MaterialIcons } from '@expo/vector-icons';
import DatePicker from '../datePicker';

class Layout extends Component {

    render() {
        const { indexSelectDeparment, indexSelectShift, isLoadEnd } = this.state;
        const { dataDeparment, dataShift } = this;
        const nameDepartmentSelet = (indexSelectDeparment || indexSelectDeparment === 0) ? dataDeparment.data[indexSelectDeparment].name : 'Tất cả';
        const nameShiftSelect = (indexSelectShift || indexSelectShift === 0) ? dataShift.data.data[indexSelectShift].name : 'Tất cả';
        return (
            <ScrollView scrollEnabled={false}>
                <View style={{ flex: 1, backgroundColor: '#ffffff', paddingLeft: 15, ...settingApp.shadow, marginBottom: 15 }}>
                    <Item
                        name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Từ ngày</Text>}
                        value={(
                            <TouchableOpacity
                                onPress={() => this.setState({ isVisibleFormDate: true })}
                                style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                                <Text style={{ fontSize: 15, color: '#8E8E93', marginRight: 10 }}>{Utils.formatTime(this.state.valueFromDate, 'DD/MM/YYYY', true)}</Text>
                                <Image
                                    source={imgApp.calendar}
                                    style={{ width: 20, height: 20, tintColor: '#D1D1D6' }}
                                    resizeMode='stretch'
                                />
                            </TouchableOpacity>
                        )}
                        noneBorder={true}
                    />
                    <Item
                        name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Đến ngày</Text>}
                        value={(
                            <TouchableOpacity
                                onPress={() => this.setState({ isVisibleToDate: true })}
                                style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                                <Text style={{ fontSize: 15, color: '#8E8E93', marginRight: 10 }}>{Utils.formatTime(this.state.valueTodate, 'DD/MM/YYYY', true)}</Text>
                                <Image
                                    source={imgApp.calendar}
                                    style={{ width: 20, height: 20, tintColor: '#D1D1D6' }}
                                    resizeMode='stretch'
                                />
                            </TouchableOpacity>
                        )}
                    />
                    <Item
                        name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Bộ phận chấm công</Text>}
                        value={isLoadEnd ? dataDeparment ? (
                            <TouchableOpacity
                                onPress={
                                    () =>
                                        this.props.navigation.navigate('ItemSelect',
                                            {
                                                data: dataDeparment.data,
                                                title: 'Bộ phận chấm công',
                                                selected: this.state.indexSelectDeparment,
                                                actions: index => this.setState({ indexSelectDeparment: index })
                                            }
                                        )
                                }
                                style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 10 }}>
                                    <Text style={{ fontSize: 15, color: '#8E8E93' }} numberOfLines={1}>{nameDepartmentSelet}</Text>
                                </View>
                                <MaterialIcons name='keyboard-arrow-right' size={23} color='#D1D1D6' />
                            </TouchableOpacity>
                        ) : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                                <Error />
                            </View> : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                                <ActivityIndicator size='small' color={settingApp.color} />
                            </View>
                        }
                    />
                    <Item
                        name={<Text style={{ color: '#212121', fontSize: 15 }} numberOfLines={1}>Ca làm việc</Text>}
                        value={isLoadEnd ? dataShift ? (
                            <TouchableOpacity
                                onPress={
                                    () =>
                                        this.props.navigation.navigate('ItemSelect',
                                            {
                                                data: dataShift.data.data,
                                                title: 'Ca làm việc',
                                                selected: this.state.indexSelectShift,
                                                actions: index => this.setState({ indexSelectShift: index })
                                            }
                                        )
                                }
                                style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 10 }}>
                                    <Text style={{ fontSize: 15, color: '#8E8E93' }} numberOfLines={1}>{nameShiftSelect}</Text>
                                </View>
                                <MaterialIcons name='keyboard-arrow-right' size={23} color='#D1D1D6' />
                            </TouchableOpacity>
                        ) : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                                <Error />
                            </View> : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                                <ActivityIndicator size='small' color={settingApp.color} />
                            </View>
                        }
                    />

                    <View style={{
                        flex: 1,
                        borderTopColor: settingApp.colorSperator,
                        borderTopWidth: 1,
                        paddingTop: 20,
                        paddingRight: 15,
                        marginBottom: 10
                    }}>
                        <TouchableOpacity
                            onPress={() => this.submit()}
                            disabled={!this.props.removeFilter}
                            style={{
                                flex: 1,
                                height: 55,
                                backgroundColor: !this.props.removeFilter ? settingApp.colorDisable : settingApp.color,
                                justifyContent: 'center',
                                alignItems: 'center',

                            }}>
                            <Text style={{ color: '#ffffff', fontSize: 15, fontWeight: 'bold' }}>Áp dụng</Text>
                        </TouchableOpacity>
                    </View>

                    <DatePicker
                        value={this.state.valueFromDate}
                        validate={value => this.validate('fromDate', value)}
                        isVisible={this.state.isVisibleFormDate}
                        close={() => this.setState({ isVisibleFormDate: false })}
                    />

                    <DatePicker
                        value={this.state.valueTodate}
                        validate={value => this.validate('toDate', value)}
                        isVisible={this.state.isVisibleToDate}
                        close={() => this.setState({ isVisibleToDate: false })}
                    />
                </View>
            </ScrollView>
        )
    }
}

export default Layout;