import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { ButtonCustom, settingApp, Utils } from '../../../../../public';
import { MaterialIcons } from '@expo/vector-icons';

class Item extends Component {

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if(nextProps.obj.index === 1) {
            return true;
        }
        return false;
    }
    
    render() {
        const { colorSperator } = settingApp;
        const { item, index } = this.props.obj;
        const time = Utils.formatTime(item.applyDay, 'dddd, DD/MM/YYYY', true);;
        const titleTime = time.charAt(0).toUpperCase() + time.slice(1);
        return (
            <ButtonCustom
                onPress={() => this.props.navigation.navigate('DetailCheckIn', { item, isNoneCheckIn: true })}
                style={{ flex: 1, paddingLeft: 15, backgroundColor: '#ffffff' }}>
                <View style={{
                    paddingRight: 15,
                    paddingTop: 10,
                    paddingBottom: 10,
                    borderTopColor: colorSperator,
                    borderTopWidth: index === 0 ? 0 : 1
                }}>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={{ flex: 1 }}>
                            <Text style={{ color: '#000', fontSize: 15 }} numberOfLines={1}>{titleTime}</Text>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end' }}>
                            <Text style={{ color: '#8E8E93', fontSize: 15, marginRight: 5 }}>Chưa chấm công</Text>
                            <MaterialIcons name='keyboard-arrow-right' size={23} color='#D1D1D6' />
                        </View>
                    </View>
                    <View style={{ flex: 1 }}>
                        <Text
                            style={{ color: '#8E8E93', fontSize: 13, marginTop: 5 }}
                            numberOfLines={1}
                        >
                            {item.shiftName}
                        </Text>
                    </View>
                </View>
            </ButtonCustom>
        )
    }
}

export default Item;