import Layout from './layout';
import { HaravanHr } from '../../../../Haravan';
import { Utils } from '../../../../public';

class NotCheckIn extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            list: [],
            isCollapsed: false,
            removeFilter: false,
            condition: null,
            isLoadEnd: false,
            refreshing: false
        }
        this.valueCondition = null;
    }

    componentDidMount() {
        this.checkCondition();
    }

    async loadData(condition) {
        const result = await HaravanHr.getCheckinHistory(`checkin/distributed?`, condition);
        if (result && result.data && !result.error) {
            this.setState({ list: result.data, isLoadEnd: true, refreshing: false });
        } else {
            this.setState({ list: null, isLoadEnd: true, refreshing: false })
        }
    }

    checkCondition(condition) {
        if (condition) {
            this.loadData(condition);
        } else {
            const date = new Date();
            const formDate = new Date(date.getFullYear(), date.getMonth(), 1);
            condition = `fromDate=${Utils.formatTime(formDate, 'YYYY-MM-DD', true)}&toDate=${Utils.formatTime(date, 'YYYY-MM-DD', true)}`;
            this.loadData(condition);
        }
        this.valueCondition = condition;
    }

}

export default NotCheckIn;