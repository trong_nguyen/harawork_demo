import React, { Component } from 'react';
import { View, Text, TouchableOpacity, FlatList, RefreshControl } from 'react-native';
import Header from './components/header';
import { EvilIcons } from '@expo/vector-icons';
import { settingApp, Arrow, Loading } from '../../../../public';
import Collapsible from 'react-native-collapsible';
import Condition from '../history/components/condition';
import Item from './components/item';

class Layout extends Component {

    renderButton() {
        const { isCollapsed } = this.state;
        return (
            <View style={{ flexDirection: 'row', paddingLeft: 10, paddingRight: 10 }}>
                <View style={{ flex: 1 }}>
                    {this.state.removeFilter && <TouchableOpacity
                        onPress={() => this.condition.setInitCondition()}
                        style={{ flexDirection: 'row', alignItems: 'center', height: 50 }}>
                        <EvilIcons name='close' color='#FF3B30' size={30} />
                        <Text style={{ color: '#FF3B30', fontSize: 16, marginLeft: 5 }}>Đặt lại bộ lọc</Text>
                    </TouchableOpacity>}
                </View>
                <View style={{ flex: 1, alignItems: 'flex-end' }}>
                    <TouchableOpacity
                        onPress={() => this.setState({ isCollapsed: !this.state.isCollapsed })}
                        style={{ flexDirection: 'row', alignItems: 'center', height: 50 }}>
                        <Text style={{ color: settingApp.color, fontSize: 16, marginRight: 5 }}>
                            {!isCollapsed ? 'Bộ lọc nâng cao' : 'Thu gọn bộ lọc'}
                        </Text>
                        <View>
                            <Arrow isOpen={isCollapsed} color={settingApp.color} />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    renderContent() {
        const { isCollapsed } = this.state;
        return (
            <View style={{ flex: 1 }}>
                {this.renderButton()}
                <Collapsible duration={350} collapsed={!isCollapsed} style={{ paddingTop: 1, paddingBottom: 1 }}>
                    <Condition
                        ref={refs => this.condition = refs}
                        setConditon={
                            condition => this.setState({ isCollapsed: false },
                                () => this.checkCondition(condition))
                        }
                        removeFilter={this.state.removeFilter}
                        changeRemoveFilter={removeFilter => this.setState({ removeFilter })}
                        {...this.props}
                    />
                </Collapsible>
                {this.renderList()}
            </View>
        )
    }

    renderList() {
        const { list, isLoadEnd } = this.state;
        if (!isLoadEnd) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Loading />
                </View>
            )
        } else {
            if (list) {
                if (list.length > 0) {
                    return (
                        <View style={{ flex: 1 }}>
                            <FlatList
                                refreshControl={<RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={() => {
                                        this.setState({ refreshing: true }, () =>
                                            this.checkCondition(this.valueCondition))
                                    }}
                                />}
                                data={list}
                                extraData={this.state}
                                keyExtractor={(item, index) => `${item.id}`}
                                renderItem={obj => <Item obj={{ ...obj }} navigation={this.props.navigation} />}
                                contentContainerStyle={{ paddingTop: 2, paddingBottom: 2, ...settingApp.shadow }}
                            />
                        </View>
                    )
                } else {
                    return (
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: settingApp.colorText, fontSize: 16 }}>
                                Bạn đã chấm công đầy đủ.
                            </Text>
                        </View>
                    )
                }
            } else {
                return (
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ color: settingApp.colorText, fontSize: 16 }}>
                            Không tìm thấy dữ liệu
                            </Text>
                    </View>
                )
            }
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Header />
                <View style={{ flex: 1 }}>
                    {this.renderContent()}
                </View>
            </View>
        )
    }
}

export default Layout;