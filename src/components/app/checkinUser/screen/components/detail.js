import React, { Component } from 'react';
import { ActivityIndicator, View, Text } from 'react-native';
import { settingApp, Utils, Error } from '../../../../../public';
import { HaravanHr } from '../../../../../Haravan';
import { MaterialIcons, Ionicons } from '@expo/vector-icons';

class Detail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            userApproved: <ActivityIndicator size='small' color={settingApp.color} style={{ marginTop: 5, alignItems: 'flex-start' }} />
        }
    }

    async componentDidMount() {
        if (this.props.isNoneCheckIn) {
            const result = await HaravanHr.getColleagueByEmployeeId(this.props.detail.approverId);
            if (result && result.data && !result.error) {
                this.setState({
                    userApproved: <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{result.data.fullName} - {result.data.mainPosition.jobtitleName}</Text>
                })
            } else {
                this.setState({
                    userApproved: (<View style={{ flex: 1, marginTop: 5, alignItems: 'flex-start' }}>
                        <Error />
                    </View>)
                })
            }
        }
    }

    render() {
        if (this.props.isNoneCheckIn) {
            const {
                departmentName,
                shiftName,
                applyDay,
                actualWorkingHours,
                approvedWorkingHours
            } = this.props.detail;
            let actualWorkingTime = actualWorkingHours || 0;
            actualWorkingTime = actualWorkingTime.toFixed(1);
            let approvedWorkingTime = approvedWorkingHours || 0;
            approvedWorkingTime = approvedWorkingTime.toFixed(1);
            return (
                <View style={{ flex: 1, marginTop: 15, }}>
                    <View style={{
                        flex: 1,
                        backgroundColor: '#ffffff',
                        paddingTop: 10,
                        paddingLeft: 15,
                        paddingRight: 15,
                        marginBottom: 5,
                        ...settingApp.shadow
                    }}>
                        <View style={{ marginBottom: 10 }}>
                            <Text style={{ color: '#000', fontSize: 15 }}>Đơn vị chấm công</Text>
                            <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{departmentName}</Text>
                        </View>
                        <View style={{ marginBottom: 10 }}>
                            <Text style={{ color: '#000', fontSize: 15 }}>Ngày</Text>
                            <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{Utils.formatTime(applyDay, 'DD/MM/YYYY', true)}</Text>
                        </View>
                        <View style={{ marginBottom: 10 }}>
                            <Text style={{ color: '#000', fontSize: 15 }}>Ca làm việc</Text>
                            <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{shiftName}</Text>
                        </View>
                        <View style={{ marginBottom: 10 }}>
                            <Text style={{ color: '#000', fontSize: 15 }}>Người duyệt</Text>
                            {this.state.userApproved}
                        </View>
                        <View style={{ marginBottom: 10 }}>
                            <Text style={{ color: '#000', fontSize: 15 }}>Giờ làm thực tế</Text>
                            <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{actualWorkingTime} giờ</Text>
                        </View>
                        <View style={{ marginBottom: 10 }}>
                            <Text style={{ color: '#000', fontSize: 15 }}>Giờ làm được duyệt</Text>
                            <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{approvedWorkingTime} giờ</Text>
                        </View>
                    </View>
                </View>
            )
        } else {
            const {
                departmentName,
                shiftName,
                checkin,
                checkinNote,
                checkout,
                checkoutNote,
                actualWorkingHours,
                approvedWorkingHours
            } = this.props.detail;
            const isCheckinNote = !!(checkinNote && checkinNote.length && checkinNote.length > 0);
            const isCheckoutNote = !!(checkoutNote && checkoutNote.length && checkoutNote.length > 0);
            const isCheckOut = !!(checkout && checkout.length && checkout.length > 0);
            let actualWorkingTime = actualWorkingHours || 0;
            actualWorkingTime = actualWorkingTime.toFixed(1);
            let approvedWorkingTime = approvedWorkingHours || 0;
            approvedWorkingTime = approvedWorkingTime.toFixed(1);
            return (
                <View style={{ flex: 1, marginTop: 15 }}>
                    <View style={{
                        flex: 1,
                        backgroundColor: '#ffffff',
                        paddingTop: 10,
                        paddingLeft: 15,
                        paddingRight: 15,
                        marginBottom: 5,
                        ...settingApp.shadow
                    }}><View style={{ marginBottom: 10 }}>
                            <Text style={{ color: '#000', fontSize: 15 }}>Đơn vị chấm công</Text>
                            <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{departmentName}</Text>
                        </View>
                        <View style={{ marginBottom: 10 }}>
                            <Text style={{ color: '#000', fontSize: 15 }}>Ca làm việc</Text>
                            <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{shiftName}</Text>
                        </View>
                        <View style={{ marginBottom: 10 }}>
                            <Text style={{ color: '#000', fontSize: 15 }}>Giờ chấm vào</Text>
                            <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{Utils.formatTime(checkin, 'LTS, DD/MM/YYYY', true)}</Text>
                            {!this.props.checkInTrace ?
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <MaterialIcons name='error' size={13} color='#FF3B30' />
                                    <Text style={{ fontSize: 13, color: '#FF3B30', marginLeft: 5 }}>Chấm vào ca ngoài IP</Text>
                                </View>
                                : null}
                        </View>
                        {isCheckinNote && <View style={{ marginBottom: 10 }}>
                            <Text style={{ color: '#000', fontSize: 15 }}>Ghi chú vào ca</Text>
                            <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{checkinNote}</Text>
                        </View>}
                        {isCheckOut && <View style={{ marginBottom: 10 }}>
                            <Text style={{ color: '#000', fontSize: 15 }}>Giờ kết thúc</Text>
                            <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{Utils.formatTime(checkout, 'LTS, DD/MM/YYYY', true)}</Text>
                            {!this.props.checkOutTrace ?
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <MaterialIcons name='error' size={13} color='#FF3B30' />
                                    <Text style={{ fontSize: 13, color: '#FF3B30', marginLeft: 5 }}>Chấm kết thúc ca ngoài IP</Text>
                                </View>
                                : null}
                        </View>}
                        {isCheckoutNote && <View style={{ marginBottom: 10 }}>
                            <Text style={{ color: '#000', fontSize: 15 }}>Ghi chú kết thúc</Text>
                            <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{checkoutNote}</Text>
                        </View>}
                        <View style={{ marginBottom: 10 }}>
                            <Text style={{ color: '#000', fontSize: 15 }}>Giờ làm thực tế</Text>
                            <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{actualWorkingTime} giờ</Text>
                        </View>
                        <View style={{ marginBottom: 10 }}>
                            <Text style={{ color: '#000', fontSize: 15 }}>Giờ làm được duyệt</Text>
                            <Text style={{ marginTop: 5, color: '#8E8E93', fontSize: 13 }}>{approvedWorkingTime} giờ</Text>
                        </View>
                    </View>
                </View>
            )
        }
    }
}

export default Detail;