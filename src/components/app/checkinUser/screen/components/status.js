import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { settingApp } from '../../../../../public';

class Status extends Component {

    checkStatus(status) {
        if (status === 1 || status === 2) {
            return { title: 'Chưa duyệt', color: '#FF9500' }
        } else if (status === 3) {
            return { title: 'Đã duyệt', color: '#4CD964' }
        } else if (status === 4) {
            return { title: 'Từ chối', color: '#FF3B30' }
        } else {
            return null;
        }
    }

    render() {
        let titleStatus = null;
        if(this.props.isNoneCheckIn) {
            titleStatus = { title: 'Chưa chấm công', color: '#8E8E93' }
        } else {
            titleStatus = this.checkStatus(this.props.status);
        }
        const checkApproverNote = !!(this.props.approverNote && this.props.approverNote.length && this.props.approverNote.length > 0);
        return (
            <View style={{
                flex: 1,
                backgroundColor: '#ffffff',
                paddingLeft: 15,
                ...settingApp.shadow
            }}>
                <View style={{
                    flex: 1,
                    height: 50,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    paddingRight: 15
                }}>
                    <Text style={{ color: '#000', fontSize: 15 }}>Trạng thái</Text>
                    <Text style={{ color: titleStatus.color, fontSize: 15 }}>{titleStatus.title}</Text>
                </View>
                {checkApproverNote && <View style={{
                    flex: 1,
                    borderTopColor: settingApp.colorSperator,
                    borderTopWidth: 1,
                    marginBottom: 10,
                    paddingTop: 10,
                    paddingRight: 15
                }}>
                    <Text style={{ color: '#000', fontSize: 15 }}>Ghi chú người duyệt</Text>
                    <Text style={{ color: '#8E8E93', fontSize: 13, marginTop: 5 }}>{this.props.approverNote}</Text>
                </View>}

            </View>
        )
    }
}

export default Status;