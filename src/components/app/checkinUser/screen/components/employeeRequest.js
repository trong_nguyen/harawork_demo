import React, { Component } from 'react';
import { ActivityIndicator, View, Text, TextInput } from 'react-native';
import { settingApp, ButtonCustom } from '../../../../../public';
import { HaravanHr } from '../../../../../Haravan';
import * as Animatable from 'react-native-animatable';

class EmployeeRequest extends Component {
    constructor(props) {
        super(props);

        this.state = {
            disable: true,
            isLoading: false,
            note: '',
            isShow: false
        }

        this.title = props.isNoneCheckIn ? 'Đề nghị quản lý bổ sung công' :
            (props.status === 1 || props.status === 2) ? 'Nhắc quản lý' : null;
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.list  && nextProps.item ) {
            let dataID = JSON.parse(nextProps.list.requestData)
            if(dataID.id != nextProps.item.id){
                this.setState({ isShow: true });
            }
            else{
                this.setState({ isShow: false });
            }
        }
        else{
            this.setState({ isShow: true });
        }
    }

    submit() {
        const { item , isNoneCheckIn} = this.props;
        const { applyDay, checkin, employeeId, shiftId, departmentId, id } = item;
        const note = this.state.note.trim();
        const data = {
            RequestData: JSON.stringify(item),
            dataId: id,
            requestApproveDate: applyDay || checkin,
            requestNote: note,
            requesterId: employeeId,
            status: 1,
            type: isNoneCheckIn ? 1 : 3,
            requestDepartmentId: departmentId,
            requestShiftId: shiftId,
        }
        this.setState({ disable: true, isLoading: true }, async () => {
            const result = await HaravanHr.suggestRemoveLeave(data);
            if (result && result.data && !result.error) {
                this.note.blur();
                const resultAni = await this.view.fadeOutDown();
                if (resultAni.finished) {
                    this.setState({ isShow: false });
                }
                await this.props.loadData();
            }
            this.setState({ disable: false, isLoading: false })
        })

    }

    render() {
        const { width, isIPhoneX } = settingApp;
        const { disable, note, isLoading, isShow } = this.state;
        const { isLoad } = this.props;
        if ( !isLoad && this.title && isShow == true) {
            return (
                <Animatable.View
                    duration={350}
                    animation='slideInUp'
                    ref={refs => this.view = refs}
                    style={{
                        width,
                        height: 150,
                        padding: 15,
                        paddingBottom: isIPhoneX ? 25 : 10,
                        backgroundColor: '#ffffff',
                        ...settingApp.shadow
                    }}>

                    <View style={{ flex: 1 }}>
                        <TextInput
                            ref={refs => this.note = refs}
                            value={note}
                            onChangeText={note => this.setState({ note, disable: (note.trim()).length > 0 ? false : true })}
                            placeholder='Ghi chú'
                            multiline={true}
                            style={{ flex: 1, paddingBottom: 5, maxHeight: 100, textAlignVertical: 'top' }}
                            underlineColorAndroid='transparent'
                            selectionColor={settingApp.color}
                        />
                    </View>

                    <ButtonCustom
                        disabled={disable}
                        onPress={() => this.submit()}
                        style={{ height: 55, backgroundColor: disable ? settingApp.colorDisable : settingApp.color, borderRadius: 3 }}>
                        <View style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                            height: 55,
                            flexDirection: 'row'
                        }}>
                            <Text style={{ fontSize: 15, color: '#ffffff', fontWeight: 'bold', marginRight: 10 }}>
                                {this.title}
                            </Text>
                            {isLoading && <ActivityIndicator size='small' color={settingApp.color} />}
                        </View>
                    </ButtonCustom>

                </Animatable.View>
            )
        } else {
            return <View />
        }
    }
}

export default EmployeeRequest;