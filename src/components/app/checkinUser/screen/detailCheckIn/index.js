import React, { Component } from 'react';
import { Text } from 'react-native';
import Layout from './layout';
import { HaravanHr } from '../../../../../Haravan';
import { Utils } from '../../../../../public';
import { connect } from 'react-redux';

class DetailCheckIn extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            approveUser: null,
            list: null,
            data:null,
            isLoading:true,
            listIp:[],
            checkInTrace: true,
            checkOutTrace: true,
        }
        this.colleague = props.app.colleague;
        this.item = this.props.navigation.state.params.item;
        this.isNoneCheckIn = this.props.navigation.state.params.isNoneCheckIn;
    }

    async componentDidMount() {
        const approveUser = await HaravanHr.getDirectapprover();
        const hrApproved = await HaravanHr.hrApproved();
        
        if (approveUser && approveUser.data && !approveUser.error && approveUser.data.length > 0) {
            const content = this.isNoneCheckIn ? <Text style={{ fontSize: 15, color: '#474747' }}>Bạn đã đề nghị <Text style={{ color: '#212121', fontWeight: 'bold' }}>{`${approveUser.data[0].fullName} - ${approveUser.data[0].mainPosition.jobtitleName}`}</Text> bổ sung công</Text> :
                (this.item.status === 1 || this.item.status === 2) ? <Text style={{ fontSize: 15, color: '#474747' }}>Bạn đã nhắc <Text style={{ color: '#212121', fontWeight: 'bold' }}>{`${approveUser.data[0].fullName} - ${approveUser.data[0].mainPosition.jobtitleName}`}</Text> duyệt công</Text> : null;
            this.setState({ approveUser: content });
        } else if (hrApproved && hrApproved.data && !hrApproved.error && hrApproved.data.length > 0) {
            const content = this.isNoneCheckIn ? <Text style={{ fontSize: 15, color: '#474747' }}>Bạn đã đề nghị <Text style={{ color: '#212121', fontWeight: 'bold' }}>{`${hrApproved.data[0].fullName} - ${hrApproved.data[0].mainPosition.jobtitleName}`}</Text> bổ sung công</Text> :
                (this.item.status === 1 || this.item.status === 2) ? <Text style={{ fontSize: 15, color: '#474747' }}>Bạn đã nhắc <Text style={{ color: '#212121', fontWeight: 'bold' }}>{`${hrApproved.data[0].fullName} - ${hrApproved.data[0].mainPosition.jobtitleName}`}</Text> duyệt công</Text> : null;
            this.setState({ approveUser: content });
        }
        this.loadData();
    }

    async loadData() {
        const { applyDay, employeeId, checkin, shiftId } = this.item;
        let { listIp } = this.state;
        this.colleague.mainPosition.departmentId
        let date = Utils.formatTime(applyDay || checkin, 'YYYY-MM-DD', true)
        const data = {
            type: this.isNoneCheckIn ? 1 : 3,
            status: 1,
            dataId: null,
            requestApproveDate: date,
            requesterId: employeeId,
            requestDepartmentId:this.colleague.mainPosition.departmentId,
            requestShiftId:shiftId
        }
        const list = await HaravanHr.getListEmployeeRequest(data);
        const resDepartIP = await HaravanHr.getIpDepartment(this.colleague.mainPosition.departmentId)
        if (list && list.data && !list.error) {
            if (list.data.length > 0) {
                list.data.map(item =>{
                    let dataJson = JSON.parse(item.requestData)
                    if(dataJson.id === this.item.id){
                        this.setState({ data: item, list:item , isLoading:false})
                    }
                    else{
                        this.setState({ isLoading: false })
                    }

                })
            } else {
                this.setState({ list: null, isLoading: false });
            }
        }

        if(resDepartIP && resDepartIP.data && !resDepartIP.error) {
            if (resDepartIP.data) {
                resDepartIP.data.map(e => {
                    e.ipAddresses.map(m => {
                        listIp.push(m)
                    })
                })
                this.setState({ listIp }, () => this.checkIP())
            }
        }
    }

    async checkIP() {
        const { listIp } = this.state;
        const { checkinTrace, checkoutTrace, departmentId } = this.item;
        if (checkinTrace && (checkinTrace !== null)) {
            const checkIP = await HaravanHr.getisIp(checkinTrace.ipAddress, departmentId)
            if (checkIP && checkIP.data && !checkIP.error) {
                this.setState({ checkInTrace: checkIP.data.isIp })
            }
            else {
                this.setState({ checkInTrace: false })
            }
        }
        else {
            this.setState({ checkInTrace: false })
        }

        if ( checkoutTrace && (checkoutTrace !== null)) {
            const checkIP = await HaravanHr.getisIp(checkoutTrace.ipAddress, departmentId)
            if (checkIP && checkIP.data && !checkIP.error) {
                this.setState({ checkOutTrace: checkIP.data.isIp })
            }
            else {
                this.setState({ checkOutTrace: false })
            }
        }
        else {
            this.setState({ checkOutTrace: false })
        }
    }
}

mapStateToProps = state => ({ ...state });
export default connect(mapStateToProps)(DetailCheckIn);