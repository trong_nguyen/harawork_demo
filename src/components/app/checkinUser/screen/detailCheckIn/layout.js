import React, { Component } from 'react';
import { KeyboardAvoidingView, View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { HeaderWapper } from '../../../../../public';
import { Ionicons } from '@expo/vector-icons';
import Status from '../components/status';
import Detail from '../components/detail';
import EmployeeRequest from '../components/employeeRequest'
import * as Animatable from 'react-native-animatable';

class Layout extends Component {
    renderHeader() {
        return (
            <HeaderWapper style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                <TouchableOpacity
                    onPress={() => this.props.navigation.pop()}
                    style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                    <Ionicons name='ios-arrow-back' size={23} color='#ffffff' />
                </TouchableOpacity>
                <Text style={{ fontSize: 16, color: '#ffffff', fontWeight: 'bold' }}>
                    Chi tiết chấm công
                </Text>
                <View style={{ width: 40, height: 44 }} />
            </HeaderWapper>
        )
    }

    renderContent() {
        const { approveUser, list, data } = this.state;
        return (
            <ScrollView style={{ paddingTop: 15 }}>
                <Status
                    status={this.item.status}
                    isNoneCheckIn={this.isNoneCheckIn}
                    approverNote={this.item.approverNote}
                />
                <Detail
                    detail={this.item}
                    isNoneCheckIn={this.isNoneCheckIn}
                    checkInTrace = {this.state.checkInTrace}
                    checkOutTrace = { this.state.checkOutTrace }
                />
                {(approveUser && data) &&
                    <Animatable.View 
                    animation='fadeInRight'
                    duration={350}
                    style={{ flex: 1, padding: 15 }}>
                        <Text style={{ fontSize: 15, color: '#8E8E93', marginBottom: 10 }}>LỊCH SỬ</Text>
                        <View style={{ flex: 1, flexDirection: 'row', paddingRight: 20 }}>
                            <View style={{ marginRight: 15, alignItems: 'center' }}>
                                <View style={{ width: 12, height: 12, borderRadius: 12, backgroundColor: '#DAE3EA', marginTop: 3 }} />
                                <View style={{ flex: 1, width: 1, marginTop: 10, backgroundColor: '#DAE3EA' }} />
                            </View>
                            <View style={{ flex: 1 }}>
                                <View>
                                   {approveUser}
                                </View>
                                <View>
                                    <View style={styles.cover}>
                                        <View style={styles.triangle} />
                                    </View>
                                    <View style={{ flex: 1, backgroundColor: '#DADADA', borderRadius: 3, padding: 10 }}>
                                        <Text style={{ fontSize: 15, color: '#474747' }}>{data.requestNote}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </Animatable.View>
                }
                <View style={{ flex: 1, height: 30 }} />
            </ScrollView>
        )
    }

    render() {
        return (
            <KeyboardAvoidingView behavior='padding' style={{ flex: 1 }}>
                {this.renderHeader()}
                <View style={{ flex: 1 }}>
                    {this.renderContent()}
                    <EmployeeRequest
                        item={this.item}
                        status={this.item.status}
                        isNoneCheckIn={this.isNoneCheckIn}
                        list={this.state.list}
                        loadData={() => this.loadData()}
                        isLoad ={ this.state.isLoading }
                        
                    />
                </View>
            </KeyboardAvoidingView>
        )
    }
}

const styles = {
    cover: {
        backgroundColor: 'transparent',
        height: 23,
        width: 23,
        overflow: "hidden",
        marginLeft: 10,
        marginTop: -10
    },
    triangle: {
        width: 23,
        height: 23,
        backgroundColor: '#DADADA',
        transform: [{ rotate: '45deg' }],
        borderRadius: 2.5,
        marginTop: 20
    },
}

export default Layout;