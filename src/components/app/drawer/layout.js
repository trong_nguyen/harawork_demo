import React, { Component } from 'react';
import { View, Text, Image, ScrollView, TouchableOpacity } from 'react-native';
import { ButtonCustom, settingApp, imgApp, Avatar, ChildMenu, SvgCP } from '../../../public';
import Configs from '../../../Haravan/config';
import { LinearGradient } from 'expo-linear-gradient';
import { MaterialCommunityIcons } from '@expo/vector-icons';

import MenuUser from './components/1.user';
import MenuContact from './components/2.contact';
import MenuBPM from './components/3.bpm';
import Constants from 'expo-constants';

const isBranchMaster = (Constants.manifest.extra.branch === 'master');
//releaseChannel

class Layout extends Component {

    renderHeader() {
        const { isIPhoneX } = settingApp;
        let { name, picture, email } = this.props.app.authHaravan.userInfo;
        return (
            <LinearGradient
                colors={['#3D68CB', '#274CA2']}
                start={[0, 0]}
                end={[1, 1]}
                location={[0, 1]}
            >
                <View style={{ paddingBottom: 25 }}>
                    <View style={{
                        marginTop: isIPhoneX ? 60 : 45, marginLeft: 25,
                    }}>
                        <Avatar userInfo={{ name, picture }} />
                        <Text style={{ color: 'rgba(255, 255, 255, 0.87)', fontSize: 14, fontWeight: 'bold', marginTop: 5 }}>
                            {name}
                        </Text>
                        <Text style={{ color: '#FFFFFF', fontSize: 14 }}>
                            {email}
                        </Text>
                    </View>
                </View>
            </LinearGradient>
        )
    }

    renderChildMenu() {
        return (
            <View style={{ height: 45, backgroundColor: '#21469B' }}>
                <ChildMenu navigation={this.props.navigation} />
            </View>
        )
    }

    renderButton(router, title,icon){
        return(
            <ButtonCustom
                onPress={() => this.openRouter(router)}
                style={{ backgroundColor: 'transparent', paddingLeft: 20 }}
            >
                <View
                    style={{ width: (settingApp.width * 0.7 + 5), height: 50, flexDirection: 'row', alignItems: 'center' }}>
                    {/* <SvgCP.drawerHome /> */}
                    {icon}
                    <Text style={{ color: '#212121', fontSize: 14, marginLeft: 5 }}>{title}</Text>
                </View>
            </ButtonCustom>
        )
    }

    render() {
        const { isIPhoneX } = settingApp;
        const { hasPermission, orgTCH } = this.state;
        return (
            <View style={{ flex: 1 }}>
                {this.renderHeader()}
                {this.renderChildMenu()}
                <ScrollView>
                    <View style={{ flex: 1 }}>
                        <ButtonCustom
                            onPress={() => this.openIndex()}
                            style={{ backgroundColor: 'transparent', paddingLeft: 20 }}
                        >
                            <View
                                style={{ width: (settingApp.width * 0.7 + 5), height: 50, flexDirection: 'row', alignItems: 'center' }}>
                                <SvgCP.drawerHome />
                                <Text style={{ color: '#212121', fontSize: 14, marginLeft: 5 }}>Trang chủ</Text>
                            </View>
                        </ButtonCustom>

                        {this.renderButton('Contact', 'Danh bạ',(<SvgCP.drawerContact/>))}
                        
                        {!isBranchMaster &&  this.renderButton('MeSalary', 'Lương nhân viên',(<SvgCP.drawerSalary/>))}
                        <MenuUser
                            selectMenu={this.state.selectMenu}
                            setSelectMenu={(selectMenu) => this.setState({ selectMenu })}
                            navigation={this.props.navigation}
                            hasPermission={hasPermission}
                            voteEnable ={ this.voteEnable }
                        />
                        {this.renderButton('Discuss', 'Thảo luận',(<SvgCP.drawerDisscus/>))}
                        {this.renderButton('NotifiList', 'Thông báo',(<SvgCP.drawerNotifyList/>))}
                        {this.renderButton('MailList', 'Thư nội bộ',(<SvgCP.drawerMail/>))}
                        {/* <MenuContact
                            selectMenu={this.state.selectMenu}
                            setSelectMenu={(selectMenu) => this.setState({ selectMenu })}
                            navigation={this.props.navigation}
                        /> */}
                        {!isBranchMaster && <MenuBPM
                            selectMenu={this.state.selectMenu}
                            setSelectMenu={(selectMenu) => this.setState({ selectMenu })}
                            navigation={this.props.navigation}
                        />}
                    </View>
                </ScrollView>

                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    paddingBottom: isIPhoneX ? 20 : 0
                }}>
                    <Text style={{ textAlign: 'right', color: 'gray', fontSize: 12, marginLeft: 20 }}>
                        Version {Configs.version}
                    </Text>

                    <TouchableOpacity
                        onPress={() => this.logOut()}
                        style={{
                            backgroundColor: 'transparent',
                            width: 50,
                            height: 40,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        <Image source={imgApp.logout} style={{ width: 14, height: 14 }} resizeMode='contain' />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

export default Layout;