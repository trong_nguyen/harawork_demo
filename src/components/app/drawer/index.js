import Layout from './layout';
import { AppState } from 'react-native';
import { Utils } from '../../../public';
import { Updates } from 'expo';

import { HaravanHr, HaravanIc } from '../../../Haravan';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../state/action';
import HaravanApi from '../../../Haravan/api/hrvapi';

class Drawer extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            selectMenu: null,
            hasPermission: false,
        }

        seft = this;
        this.currentCloseApp = null;
        this.appState = AppState.currentState;
        this.authHara = props.app.authHaravan;
        this.voteEnable = null
    }

    async componentDidMount() {
        const result = await HaravanHr.checkPermissionUser();
        if (result && result.data && !result.error) {
            if (result.data.length && result.data.length > 0) {
                this.setState({ hasPermission: true });
            }
        }

        await this.updateUserInfo();

        AppState.addEventListener('change',
            (nextAppState) => this.handleAppStateChange(nextAppState));
    }

    componentWillReceiveProps(nextProps){
        this.voteEnable = nextProps.app.checkOrgVote
    }

    async handleAppStateChange(nextAppState) {
        if (this.appState.match(/inactive|background/) && nextAppState === 'active') {
            const currentOpenApp = new Date().getTime();
            if ((currentOpenApp - this.currentCloseApp) >= ((1000 * 60 * 5))) {
                Utils.reload(this.props);
            }
            await this.updateUserInfo();
        } else {
            this.currentCloseApp = new Date().getTime();
        }
        this.appState = nextAppState;
    }

    async updateUserInfo() {
        let { authHaravan } = this.props.app;
        const userInfo = await HaravanApi.getUserInfo(authHaravan.auth.access_token);
        if (userInfo && userInfo.hasOwnProperty('sub')) {
            const colleague = await HaravanHr.getColleague(userInfo.sub);
            if (colleague && colleague.data && !colleague.error) {
                authHaravan = { ...authHaravan, userInfo };
                HaravanApi.SetAuthInfo(authHaravan);
                this.props.actions.authHaravan(authHaravan);
                this.props.actions.getColleague(colleague.data);
            }
        }
    }

    openIndex() {
        this.props.navigation.navigate('Home');
        this.props.navigation.closeDrawer()
        this.setState({ selectMenu: null });
    }

    openRouter(router){
        this.props.navigation.navigate(router);
        this.props.navigation.closeDrawer()
        this.setState({ selectMenu: null });
    }

    seft = null

    async logOut() {
        await HaravanIc.disableForPushNotificationsAsync()
        await HaravanApi.ClearAuthInfo();
        Updates.reload();
    }

}

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(mapStateToProps, mapDispatchToProps)(Drawer);