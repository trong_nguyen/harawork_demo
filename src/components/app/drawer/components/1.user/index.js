import Layout from './layout';
import { Animated } from 'react-native';
import Constants from 'expo-constants'

const isBranchMaster = (Constants.manifest.extra.branch === 'master');
//releaseChannel

class MenuUser extends Layout {
    constructor(props) {
        super(props);
        this.voteEnable = props.voteEnable
        this.state = {
            isCollapsed: true,
            rotateAnimation: new Animated.Value(0),
            list: !isBranchMaster ?
            [
                { name: 'Chấm công', key: 'CheckIn'},
                { name: 'Lịch làm việc', key: 'CalendarJob'},
                { name: 'Nghỉ phép', key: 'Leaves'},
                { name: 'Đăng ký ca', key: 'RegisterWorks'},
                { name: 'Duyệt công', key: 'ApprovedWork', isManager: true},
                { name: 'Duyệt nghỉ phép', key: 'ApprovedLeaves', isManager: true},
                { name: 'Đánh giá 360°', key: 'VotePoint360' },
            ]
            :
            [
                { name: 'Chấm công', key: 'CheckIn' },
                { name: 'Lịch làm việc', key: 'CalendarJob' },
                { name: 'Nghỉ phép', key: 'Leaves' },
                { name: 'Đăng ký ca', key: 'RegisterWorks' },
                { name: 'Duyệt công', key: 'ApprovedWork', isManager: true },
                { name: 'Duyệt nghỉ phép', key: 'ApprovedLeaves', isManager: true },
                { name: 'Đánh giá 360°', key: 'VotePoint360' }
            ],
            selectMenu: null,
        }

        this.name = 'Nhân sự';
    }

    openMenu() {
        Animated.timing(
            this.state.rotateAnimation,
            {
                toValue: !this.state.isCollapsed ? 0 : 1,
                duration: 150,
                useNativeDriver: true
            }
        ).start();

        this.setState({ isCollapsed: !this.state.isCollapsed });
    }

    navigateMenu(obj) {
        const key = obj.key.split('-');
        const routeName = key[0];
        this.props.navigation.navigate({
            routeName,
            key: `${routeName}`
        });
        this.props.navigation.closeDrawer();
        this.props.setSelectMenu(routeName);
    }

    componentWillReceiveProps(nextProps) {
        this.voteEnable = nextProps.voteEnable
        this.setState({ selectMenu: nextProps.selectMenu });
    }

}

export default MenuUser;