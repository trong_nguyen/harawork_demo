import React, { Component } from 'react';
import { Animated, View, Text, FlatList, TouchableOpacity, Image } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import Collapsible from 'react-native-collapsible';
import { settingApp, ButtonCustom } from '../../../../../public';

class Layout extends Component {

    renderMenu(obj) {
        const { selectMenu } = this.state;
        const { item } = obj;
        const isSelect = selectMenu === item.key;
        return (
            <ButtonCustom
                onPress={() => this.navigateMenu(item)}
                style={{ backgroundColor: `rgba(255,255,255,${isSelect ? '0.05' : '0'})`, paddingLeft: 20 }}>
                <View style={{ flex: 1, paddingLeft: 25, height: 50, justifyContent: 'center' }}>
                    <Text style={{ color: '#212121', fontSize: 14 }}>{item.name}</Text>
                </View>
            </ButtonCustom>
        )
    }

    render() {
        const { isCollapsed, rotateAnimation } = this.state;
        const rotateProp = rotateAnimation.interpolate({
            inputRange: [0, 1],
            outputRange: ["0deg", "90deg"]
        })
        return (
            <View style={{ flex: 1 }}>
                <TouchableOpacity
                    onPress={() => this.openMenu()}
                    activeOpacity={1}
                    style={{ width: (settingApp.width * 0.7 + 5), height: 50, flexDirection: 'row', alignItems: 'center', marginLeft: 20 }}>
                    <Image source={this.icon} style={{ width: 16, height: 16 }} resizeMode='contain' />
                    <Text style={{ color: '#212121', fontSize: 14, marginLeft: 10 }}>{this.name}</Text>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                        <Animated.View style={{
                            transform: [
                                {
                                    rotate: rotateProp
                                }
                            ]
                        }}>
                            <MaterialIcons name='keyboard-arrow-right' color='#9CA7B2' size={23} />
                        </Animated.View>
                    </View>
                </TouchableOpacity>
                <Collapsible
                    duration={150}
                    collapsed={isCollapsed}>
                    <View>
                        <FlatList
                            data={this.state.list}
                            extraData={this.state}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={(obj) => this.renderMenu(obj)}
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>
                </Collapsible>
            </View>
        )
    }
}

export default Layout;