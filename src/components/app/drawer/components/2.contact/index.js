import Layout from './layout';
import { Animated } from 'react-native';
import { imgApp } from '../../../../../public';
import Constants from 'expo-constants'

const isBranchMaster = (Constants.manifest.extra.branch === 'master');
//releaseChannel

class MenuContact extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            isCollapsed: true,
            rotateAnimation: new Animated.Value(0),
            list: !isBranchMaster ?
                [
                    { name: 'Thảo luận', key: 'Discuss' },
                    { name: 'Thông báo', key: 'NotifiList' },
                    { name: 'Thư nội bộ', key: 'MailList' }
                ]
                :
                [
                    { name: 'Thông báo', key: 'NotifiList' },
                    { name: 'Thư nội bộ', key: 'MailList' }
                ],
            selectMenu: null
        }

        this.name = 'Giao tiếp nội bộ';
        this.icon = imgApp.contact;
    }

    openMenu() {
        Animated.timing(
            this.state.rotateAnimation,
            {
                toValue: !this.state.isCollapsed ? 0 : 1,
                duration: 150,
                useNativeDriver: true
            }
        ).start();

        this.setState({ isCollapsed: !this.state.isCollapsed });
    }

    navigateMenu(obj) {
        const key = obj.key.split('-');
        const routeName = key[0];
        this.props.navigation.navigate({
            routeName,
            key: `${routeName}`
        });
        this.props.navigation.closeDrawer();
        this.props.setSelectMenu(routeName);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ selectMenu: nextProps.selectMenu });
    }

}

export default MenuContact;