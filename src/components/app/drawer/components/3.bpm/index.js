import Layout from './layout';
import { Animated } from 'react-native';
import { settingApp } from '../../../../../public';

import { HaraBPM } from '../../../../../Haravan';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../../../state/action';

class MenuBPM extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            isCollapsed: true,
            rotateAnimation: new Animated.Value(0),
            list: [...settingApp.listRouterBPM],
            selectMenu: null
        }

        this.name = 'Quy trình vận hành';
    }

    async componentDidMount() {
        // const result = await HaraBPM.getListCount();
        // if (result && result.data && !result.error) {
        //     const { data } = result;
        //     const formMe = data[data.findIndex(e => e.name === 'FormMe')].total;
        //     const formWaitingApproveTotal = data[data.findIndex(e => e.name === 'FormWaitingApproveTotal')].total;
        //     const formWaitingApprove = data[data.findIndex(e => e.name === 'FormWaitingApprove')].total;
        //     const formRejectedOrApproved = data[data.findIndex(e => e.name === 'FormRejectedOrApproved')].total;
        //     const formWaitingCompleteTotal = data[data.findIndex(e => e.name === 'FormWaitingCompleteTotal')].total;
        //     const formWaitingComplete = data[data.findIndex(e => e.name === 'FormWaitingComplete')].total;
        //     const formCompleted = data[data.findIndex(e => e.name === 'FormCompleted')].total;
        //     const formTrack = data[data.findIndex(e => e.name === 'FormTrack')].total;;
        //     const totalIndex = formWaitingApproveTotal + formWaitingCompleteTotal;

        //     this.props.actions.bpm.countList({
        //         totalIndex,
        //         formMe,
        //         formWaitingApproveTotal,
        //         formWaitingApprove,
        //         formRejectedOrApproved,
        //         formWaitingComplete,
        //         formCompleted,
        //         formTrack
        //     });
        // }
    }

    openMenu() {
        Animated.timing(
            this.state.rotateAnimation,
            {
                toValue: !this.state.isCollapsed ? 0 : 1,
                duration: 150,
                useNativeDriver: true
            }
        ).start();

        this.setState({ isCollapsed: !this.state.isCollapsed });
    }

    navigateMenu(obj) {
        const { name, key, first, second } = obj;
        this.props.setSelectMenu(key);
        this.props.navigation.navigate({
            routeName: key,
            params: {
                title: name,
                first,
                second,
                changeScreen: (keyScreen) => this.props.setSelectMenu(keyScreen)
            }
        });
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ selectMenu: nextProps.selectMenu });
    }
}

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(mapStateToProps, mapDispatchToProps)(MenuBPM);