import { Record } from 'immutable';

const InitialState = Record({
    updateHistory: null,
    listLeave: null,
    removeLeave: null,
    suggestRemove: null
});

export default InitialState;
