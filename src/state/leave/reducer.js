import InitialState from './initialState';
import types from './types';

function createReducer(initialState, reducerMap) {
    return (state = initialState, action) => {
        const reducer = reducerMap[action.type]
        return reducer ? reducer(state, action.payload, action.params, action) : state
    }
}

const initialState = new InitialState();

export default createReducer(initialState, {
    [types.UPDATE_HISTORY_SUCCESS]: (state, data) => {
        return state.set('updateHistory', data.body)
    },
    [types.LEAVE_LIST_SUCCESS]: (state, data) => {
        return state.set('listLeave', data.body)
    },
    [types.REMOVE_LEAVE_SUCCESS]: (state, data) => {
        return state.set('removeLeave', data.body)
    },
    [types.SUGGEST_REMOVE_SUCCESS]: (state, data) => {
        return state.set('suggestRemove', data.body)
    }
});
