import key from 'keymirror';

export default key({
    UPDATE_HISTORY: null,
    UPDATE_HISTORY_SUCCESS: null,
    LEAVE_LIST: null,
    LEAVE_LIST_SUCCESS: null,
    REMOVE_LEAVE: null,
    REMOVE_LEAVE_SUCCESS: null,
    SUGGEST_REMOVE: null,
    SUGGEST_REMOVE_SUCCESS: null,
}, "APP");
