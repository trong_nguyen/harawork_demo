import types from './types';

export function updateHistory(status) {
    return {
        type: types.UPDATE_HISTORY,
        payload: {
            body: status
        }
    }
}

export function listLeave(data) {
    return {
        type: types.LEAVE_LIST,
        payload: {
            body: data
        }
    }
}

export function removeLeave(id) {
    return {
        type: types.REMOVE_LEAVE,
        payload: {
            body: id
        }
    }
}

export function suggestRemove(id) {
    return {
        type: types.SUGGEST_REMOVE,
        payload: {
            body: id
        }
    }
}