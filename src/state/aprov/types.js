import key from 'keymirror';

export default key({
    CHECK_APROV: null,
    CHECK_APROV_SUCCESS: null,

    REFRESH_LIST_APR: null,
    REFRESH_LIST_APR_SUCCESS:null,

    CHECK_APR_LEAVE: null,
    CHECK_APR_LEAVE_SUCCESS:null,

    RELOAD_LIST: null,
    RELOAD_LIST_SUCCESS:null,

}, "APP");
