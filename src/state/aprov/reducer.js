import InitialState from './initialState';
import types from './types';

function createReducer(initialState, reducerMap) {
    return (state = initialState, action) => {
        const reducer = reducerMap[action.type]
        return reducer ? reducer(state, action.payload, action.params, action) : state
    }
}

const initialState = new InitialState();

export default createReducer(initialState, {
   
    [types.CHECK_APROV_SUCCESS]: (state, data) => {
        return state.set('checkAprov', data.body)
    },
    [types.REFRESH_LIST_APR_SUCCESS]: (state, data) => {
        return state.set('refreshList', data.body)
    },
    [types.CHECK_APR_LEAVE_SUCCESS]: (state, data) => {
        return state.set('checkAprLeave', data.body)
    },
    [types.RELOAD_LIST_SUCCESS]: (state, data) => {
        return state.set('reload', data.body)
    },
 
});
