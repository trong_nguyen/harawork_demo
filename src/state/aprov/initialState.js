import { Record } from 'immutable';

const InitialState = Record({
    checkAprov:null,
    refreshList:null,
    checkAprLeave:null,
    reload: null
});

export default InitialState;
