import types from './types';
import { keyStore } from '../../public';

export function checkAprov(data) {
    return {
        type: types.CHECK_APROV,
        payload: {
            body: data
        }
    }
}

export function refreshList(data) {
    return {
        type: types.REFRESH_LIST_APR,
        payload: {
            body: data
        }
    }
}

export function checkAprLeave(data) {
    return {
        type: types.CHECK_APR_LEAVE,
        payload: {
            body: data
        }
    }
}

export function reload(data) {
    return {
        type: types.RELOAD_LIST,
        payload: {
            body: data
        }
    }
}



