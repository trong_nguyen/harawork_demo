import { createStore, applyMiddleware } from 'redux';

import appReducer from './reducers';
import { middleware } from '../route';

import createSagaMiddleware from 'redux-saga'
import rootSaga from './middleware';

const sagaMiddleware = createSagaMiddleware()

const store = createStore(
    appReducer,
    applyMiddleware(sagaMiddleware, middleware),
);

sagaMiddleware.run(rootSaga)

export default store;
