import * as app from './app/action';
import * as checkIn from './checkIn/action';
import * as notify from './notify/action';
import * as bpm from './bpm/action';
import * as leave from './leave/action';
import * as aprov from './aprov/action';
import * as mail from './mail/action';
import * as discuss from './discuss/actions';
import * as publicActions from './publicActions/actions';

const actions = {
    ...app,
    checkIn,
    notify,
    bpm,
    leave,
    aprov,
    mail,
    discuss,
    publicActions
}

export default actions;