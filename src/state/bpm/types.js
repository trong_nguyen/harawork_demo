import key from 'keymirror';

export default key({
    COUNT_LIST: null,
    COUNT_LIST_SUCCESS: null,

    LIST_BPM: null,
    LIST_BPM_SUCCESS: null,

    IS_READ: null,
    IS_READ_SUCCESS: null

}, "APP");
