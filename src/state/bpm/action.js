import types from './types';

export function countList(data) {
    return {
        type: types.COUNT_LIST,
        payload: {
            body: data
        }
    }
}

export function listBPM(data, keyStore) {
    return {
        type: types.LIST_BPM,
        payload: {
            body: data,
            keyStore
        },

        keyStore
    }
}

export function isReaded(data) {
    return {
        type: types.IS_READ,
        payload: {
            body: data
        }
    }
}