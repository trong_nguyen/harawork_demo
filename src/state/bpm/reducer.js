import InitialState from './initialState';
import types from './types';

function createReducer(initialState, reducerMap) {
    return (state = initialState, action) => {
        const reducer = reducerMap[action.type]
        return reducer ? reducer(state, action.payload, action.params, action) : state
    }
}

const initialState = new InitialState();

export default createReducer(initialState, {
    [types.COUNT_LIST_SUCCESS]: (state, data) => {
        return state.set('listCount', data.body)
    },

    [types.LIST_BPM_SUCCESS]: (state, data) => {
        return state.set(`list${data.keyStore}`, data.body)
    },

    [types.IS_READ_SUCCESS]: (state, data) => {
        return state.set('idIsReaded', data.body)
    },
});
