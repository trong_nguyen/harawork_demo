import { Record } from 'immutable';

const InitialState = Record({
    listCount: {
        totalIndex: null,
        formMe: null,
        formWaitingApproveTotal: null,
        formWaitingApprove: null,
        formRejectedOrApproved: null,
        formWaitingComplete: null,
        formCompleted: null,
        formTrack: null
    },

    listformWaitingApproveTotal:[],
    listformWaitingCompleteTotal:[],
    listformMe:[],
    listformWaitingApprove:[],
    listformRejectedOrApproved:[],
    listformWaitingComplete:[],
    listformCompleted:[],
    listformTrack:[],

    idIsReaded: null
});

export default InitialState;
