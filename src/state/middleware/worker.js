import { AsyncStorage } from 'react-native';

import { put } from 'redux-saga/effects'

import key from 'keymirror';

const typeSuffix = key({
    SUCCESS: null,
    ACTION_FAIL: null
});

export function* worker(data) {
    let { type, payload, keyStore } = data;
    
    if(keyStore) {
        AsyncStorage.setItem(keyStore, JSON.stringify(payload.body));
    }

    try {
        yield put({
            type: type + "_" + typeSuffix.SUCCESS,
            payload
        })
    } catch (error) {
        console.warn('Error middleware', error)
        yield put({
            type: typeSuffix.ACTION_FAIL,
        })
    }
}
