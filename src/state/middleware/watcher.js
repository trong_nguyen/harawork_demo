import { takeEvery } from 'redux-saga/effects'

import { worker } from './worker';

import Config from '../../Haravan/config';

import { Analytics, PageHit } from 'expo-analytics';

const analytics = new Analytics(Config.id_GA);

export function* watcher() {
    yield takeEvery(action => {
        let result = false;
        let checkType =
            action.type.indexOf('_SUCCESS') > -1 ||
            action.type.indexOf('_FAILS') > -1 ||
            action.type.indexOf('Navigation/') > -1;
        if (!checkType) {
            if (action && action.payload) {
                result = true;
            }
        }
        if (action.type === 'Navigation/NAVIGATE' && action.routeName) {
            analytics.hit(new PageHit(action.routeName))
                .then(() => console.log("success"))
                .catch(e => console.log(e.message));
        }
        return result;
    },
        worker)
}
