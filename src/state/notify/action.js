import types from './types';
import { keyStore } from '../../public';

export function checkStart(data) {
    return {
        type: types.CHECK_STAR,
        payload: {
            body: data
        }
    }
}

export function checkRead(data) {
    return {
        type: types.CHECK_READED,
        payload: {
            body: data
        }
    }
}

export function refreshListNotify(data) {
    return {
        type: types.REFRESH_LISTNOTIFI,
        payload: {
            body: data
        }
    }
}

export function listNotify(data) {
    return {
        type: types.LIST_NOTIFY,
        payload: {
            body: data
        },

        keyStore: keyStore.listNotify
    }
}