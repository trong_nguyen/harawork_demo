import { Record } from 'immutable';

const InitialState = Record({
    listNotify: [],
    checkStart: null,
    checkRead: null,
    refreshListNotify: null,
});

export default InitialState;
