import key from 'keymirror';

export default key({
    LIST_NOTIFY: null,
    LIST_NOTIFY_SUCCESS: null,

    CHECK_STAR: null,
    CHECK_STAR_SUCCESS: null,

    CHECK_READED: null,
    CHECK_READED_SUCCESS: null,
    
    REFRESH_LISTNOTIFI: null,
    REFRESH_LISTNOTIFI_SUCCESS: null,
}, "APP");
