import InitialState from './initialState';
import types from './types';

function createReducer(initialState, reducerMap) {
    return (state = initialState, action) => {
        const reducer = reducerMap[action.type]
        return reducer ? reducer(state, action.payload, action.params, action) : state
    }
}

const initialState = new InitialState();

export default createReducer(initialState, {

    [types.LIST_NOTIFY_SUCCESS]: (state, data) => {
        return state.set('listNotify', data.body)
    },

    [types.CHECK_STAR_SUCCESS]: (state, data) => {
        return state.set('checkStart', data.body)
    },

    [types.CHECK_READED_SUCCESS]: (state, data) => {
        return state.set('checkRead', data.body)
    },

    [types.REFRESH_LISTNOTIFI_SUCCESS]: (state, data) => {
        return state.set('refreshListNotify', data.body)
    }
    
});
