import { Record } from 'immutable';

const InitialState = Record({
    changeTitle: null,
    checkAtions:null,
    addReply:null,
    refreshList:null,
    changeItem:null,
    readContent:null,
    changeImage:null,
    joinGroups:null,
    newComment:null,
});

export default InitialState;
