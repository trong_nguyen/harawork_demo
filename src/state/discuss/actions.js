import types from './types'
import actions from '../action';

export function changeTitle(data){
    return {
        type: types.CHANGE_TITLE,
        payload: {
            body: data
        }
    }
}
export function checkAtions(data){
    return {
        type: types.CHECK_ACTION,
        payload: {
            body: data
        }
    }
}
export function addReply(data){
    return {
        type: types.ADD_REPLY,
        payload: {
            body: data
        }
    }
}
export function refreshList(data){
    return {
        type: types.REFRESH_LIST,
        payload: {
            body: data
        }
    }
}
export function changeItem(data){
    return {
        type: types.CHANGE_ITEM,
        payload: {
            body: data
        }
    }
}
export function readContent(data){
    return {
        type: types.READ_CONTENT,
        payload: {
            body: data
        }
    }
}

export function changeImage(data){
    return {
        type: types.CHANGE_IMAGE,
        payload: {
            body: data
        }
    }
}
export function joinGroups(data){
    return {
        type: types.JOIN_GROUP,
        payload: {
            body: data
        }
    }
}

export function newComment(data){
    return {
        type: types.NEW_COMMENT,
        payload: {
            body: data
        }
    }
}