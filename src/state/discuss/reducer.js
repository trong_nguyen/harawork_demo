import InitialState from './initialState';
import types from './types';

function createReducer(initialState, reducerMap) {
    return (state = initialState, action) => {
        const reducer = reducerMap[action.type]
        return reducer ? reducer(state, action.payload, action.params, action) : state
    }
}

const initialState = new InitialState();

export default createReducer(initialState, {
    [types.CHANGE_TITLE_SUCCESS]: (state, data) => {
        return state.set('changeTitle', data.body)
    },

    [types.CHECK_ACTION_SUCCESS]: (state, data) => {
        return state.set('checkAtions', data.body)
    },

    [types.ADD_REPLY_SUCCESS]: (state, data) => {
        return state.set('addReply', data.body)
    },
    [types.REFRESH_LIST_SUCCESS]: (state, data) => {
        return state.set('refreshList', data.body)
    },
    [types.CHANGE_ITEM_SUCCESS]: (state, data) => {
        return state.set('changeItem', data.body)
    },
    [types.READ_CONTENT_SUCCESS]: (state, data) => {
        return state.set('readContent', data.body)
    },
    [types.CHANGE_IMAGE_SUCCESS]: (state, data) => {
        return state.set('changeImage', data.body)
    },
    [types.JOIN_GROUP]: (state, data) => {
        return state.set('joinGroups', data.body)
    },
    [types.NEW_COMMENT_SUCCESS]: (state, data) => {
        return state.set('newComment', data.body)
    },
});



