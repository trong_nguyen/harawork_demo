import key from 'keymirror'

export default key({
   CHANGE_TITLE:null,
   CHANGE_TITLE_SUCCESS:null,

   CHECK_ACTION:null,
   CHECK_ACTION_SUCCESS: null,

   ADD_REPLY:null,
   ADD_REPLY_SUCCESS: null,

   REFRESH_LIST:null,
   REFRESH_LIST_SUCCESS: null,

   CHANGE_ITEM:null,
   CHANGE_ITEM_SUCCESS: null,

   READ_CONTENT:null,
   READ_CONTENT_SUCCESS: null,

   CHANGE_IMAGE:null,
   CHANGE_IMAGE_SUCCESS: null,

   JOIN_GROUP:null,
   JOIN_GROUP_SUCCESS: null,

   NEW_COMMENT:null,
   NEW_COMMENT_SUCCESS: null,

}, "APP");
