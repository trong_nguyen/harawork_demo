import types from './types';
import { keyStore } from '../../public';

export function authHaravan(authInfo) {
    return {
        type: types.AUTH_HARAVAN,
        payload: {
            body: authInfo
        }
    }
}

export function getColleague(data) {
    return {
        type: types.GET_COLLEAGUE,
        payload: {
            body: data
        },

        keyStore: keyStore.colleague
    }
}

export function removeMail(data){
    return{
        type: types.REMOVE_MAIL,
        payload:{
            body: data
        }
    }
}

export function checkOrgVote(data){
    return{
        type: types.CHECK_ORG_VOTE,
        payload:{
            body: data
        }
    }
}
export function infoBody(data){
    return{
        type: types.INFO_BODY,
        payload:{
            body: data
        }
    }
}
export function reloadNotify(data){
    return{
        type: types.RELOAD_NOTIFY,
        payload:{
            body: data
        }
    }
}

export function checkOrgSalary(data){
    return{
        type: types.CHECK_ORG_SALARY,
        payload:{
            body: data
        }
    }
}