import InitialState from './initialState';
import types from './types';

function createReducer(initialState, reducerMap) {
    return (state = initialState, action) => {
        const reducer = reducerMap[action.type]
        return reducer ? reducer(state, action.payload, action.params, action) : state
    }
}

const initialState = new InitialState();

export default createReducer(initialState, {
    [types.AUTH_HARAVAN_SUCCESS]: (state, data) => {
        return state.set('authHaravan', data.body)
    },

    [types.REFRESH_HISTORY_SUCCESS]: (state, data) => {
        return state.set('isRefreshHistory', data.body)
    },
    
    [types.GET_CHECKIN_STATUS_SUCCESS]: (state, data) => {
        return state.set('checkInStatus', data.body)
    },

    [types.GET_COLLEAGUE_SUCCESS]: (state, data) => {
        return state.set('colleague', data.body)
    },
    
    [types.REMOVE_MAIL_SUCCESS]:(state, data) => {
        return state.set('removeMail', data.body)
    },

    [types.CHECK_ORG_VOTE_SUCCESS]:(state, data) => {
        return state.set('checkOrgVote', data.body)
    },

    [types.INFO_BODY_SUCCESS]:(state, data) => {
        return state.set('infoBody', data.body)
    },

    [types.RELOAD_NOTIFY_SUCCESS]:(state, data) => {
        return state.set('reloadNotify', data.body)
    },

    [types.CHECK_ORG_SALARY_SUCCESS]:(state, data) => {
        return state.set('checkOrgSalary', data.body)
    },
});
