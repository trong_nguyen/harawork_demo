import key from 'keymirror';

export default key({
    AUTH_HARAVAN: null,
    AUTH_HARAVAN_SUCCESS: null,

    REFRESH_HISTORY: null,
    REFRESH_HISTORY_SUCCESS: null,
    
    GET_CHECKIN_STATUS: null,
    GET_CHECKIN_STATUS_SUCCESS: null,

    GET_COLLEAGUE: null,
    GET_COLLEAGUE_SUCCESS: null,

    REMOVE_MAIL: null,
    REMOVE_MAIL_SUCCESS: null,

    CHECK_ORG_VOTE: null,
    CHECK_ORG_VOTE_SUCCESS: null,

    INFO_BODY: null,
    INFO_BODY_SUCCESS: null,

    RELOAD_NOTIFY:null,
    RELOAD_NOTIFY_SUCCESS:null,

    CHECK_ORG_SALARY:null,
    CHECK_ORG_SALARY_SUCCESS:null
}, "APP");
