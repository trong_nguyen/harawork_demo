import { Record } from 'immutable';

const InitialState = Record({
    authHaravan:null,
    isRefreshHistory: null,
    colleague: null,
    checkInStatus: null,
    removeMail: null,
    checkOrgVote:null,
    infoBody:null,
    reloadNotify:null,
    checkOrgSalary:null
});

export default InitialState;
