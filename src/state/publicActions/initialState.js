import { Record } from 'immutable';

const InitialState = Record({
    listUser:null,
    itemUser:null,
    removeItemUser:null,
});

export default InitialState;
