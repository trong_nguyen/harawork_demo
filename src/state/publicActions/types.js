import key from 'keymirror'

export default key({
   LIST_USER:null,
   LIST_USER_SUCCESS:null,

   ITEM_USER:null,
   ITEM_USER_SUCCESS:null,

   REMOVE_ITEM_USER:null,
   REMOVE_ITEM_USER_SUCCESS:null,
}, "APP");
