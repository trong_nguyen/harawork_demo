import types from './types'

export function listUser(data){
    return {
        type: types.LIST_USER,
        payload: {
            body: data
        }
    }
}
export function itemUser(data){
    return {
        type: types.ITEM_USER,
        payload: {
            body: data
        }
    }
}
export function removeItemUser(data){
    return {
        type: types.REMOVE_ITEM_USER,
        payload: {
            body: data
        }
    }
}