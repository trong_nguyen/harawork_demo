import InitialState from './initialState';
import types from './types';

function createReducer(initialState, reducerMap) {
    return (state = initialState, action) => {
        const reducer = reducerMap[action.type]
        return reducer ? reducer(state, action.payload, action.params, action) : state
    }
}

const initialState = new InitialState();

export default createReducer(initialState, {
    [types.LIST_USER_SUCCESS]: (state, data) => {
        return state.set('listUser', data.body)
    },
    [types.ITEM_USER_SUCCESS]: (state, data) => {
        return state.set('itemUser', data.body)
    },
    [types.REMOVE_ITEM_USER_SUCCESS]: (state, data) => {
        return state.set('removeItemUser', data.body)
    },
});


