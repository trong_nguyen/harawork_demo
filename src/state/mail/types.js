import key from 'keymirror';

export default key({
    TYPE_MAIL: null,
    TYPE_MAIL_SUCCESS: null,

    CHECK_STARRED: null,
    CHECK_STARRED_SUCCESS: null,

    CHECK_LABLE: null,
    CHECK_LABLE_SUCCESS: null,

    REMOVE_MAIL: null,
    REMOVE_MAIL_SUCCESS: null,

    READ_MAIL: null,
    READ_MAIL_SUCCESS: null,
    
    LOAD_DETAIL: null,
    LOAD_DETAIL_SUCCESS:null,

    REMOVE_DRAFT: null,
    REMOVE_DRAFT_SUCCESS:null,

    DELETE_DRAFT: null,
    DELETE_DRAFT_SUCCESS:null,

    // tree contrus
    REMOVE_USER:null,
    REMOVE_USER_SUCCESS:null,

    ADD_USER:null,
    ADD_USER_SUCCESS:null,

    LIST_USER:null,
    LIST_USER_SUCCESS:null,

}, "APP");