import InitialState from './initialState';
import types from './types';

function createReducer(initialState, reducerMap) {
    return (state = initialState, action) => {
        const reducer = reducerMap[action.type]
        return reducer ? reducer(state, action.payload, action.params, action) : state
    }
}

const initialState = new InitialState();

export default createReducer(initialState, {
    [types.TYPE_MAIL_SUCCESS]: (state, data) => {
        return state.set('typeMail', data.body)
    },
    [types.CHECK_STARRED_SUCCESS]:(state, data) => {
        return state.set('checkStarred', data.body)
    },
    [types.CHECK_LABLE_SUCCESS]:(state, data) => {
        return state.set('checkLable', data.body)
    },
    [types.REMOVE_MAIL_SUCCESS]:(state, data) => {
        return state.set('removeMail', data.body)
    },
    [types.READ_MAIL_SUCCESS]:(state, data) => {
        return state.set('readMail', data.body)
    },
    [types.LOAD_DETAIL_SUCCESS]:(state, data) => {
        return state.set('loadDetail', data.body)
    },
    [types.REMOVE_DRAFT_SUCCESS]:(state, data) => {
        return state.set('removeDraft', data.body)
    },
    [types.DELETE_DRAFT_SUCCESS]:(state, data) => {
        return state.set('deletedDraft', data.body)
    },
    [types.REMOVE_USER_SUCCESS]:(state, data) => {
        return state.set('removeUser', data.body)
    },
    [types.ADD_USER_SUCCESS]:(state, data) => {
        return state.set('addUser', data.body)
    },
    [types.LIST_USER_SUCCESS]:(state, data) => {
        return state.set('listUser', data.body)
    },
});