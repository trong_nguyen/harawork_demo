import types from './types';

export function typeMail(data) {
    return {
        type: types.TYPE_MAIL,
        payload: {
            body: data
        }
    }
}

export function checkStarred(data){
    return{
        type: types.CHECK_STARRED,
        payload:{
            body: data
        }
    }
}

export function checkLable(data){
    return{
        type: types.CHECK_LABLE,
        payload:{
            body: data
        }
    }
}
export function removeMail(data){
    return{
        type: types.REMOVE_MAIL,
        payload:{
            body: data
        }
    }
}

export function readMail(data){
    return{
        type: types.READ_MAIL,
        payload:{
            body: data
        }
    }
}

export function loadDetail(data){
    return{
        type: types.LOAD_DETAIL,
        payload:{
            body: data
        }
    }
}

export function removeDraft(data){
    return{
        type: types.REMOVE_DRAFT,
        payload:{
            body: data
        }
    }
}

export function deletedDraft(data){
    return{
        type: types.DELETE_DRAFT,
        payload:{
            body: data
        }
    }
}

// tree Contrus
export function removeUser(data){
    return{
        type: types.REMOVE_USER,
        payload:{
            body: data
        }
    }
}

export function addUser(data){
    return{
        type: types.ADD_USER,
        payload:{
            body: data
        }
    }
}

export function listUser(data){
    return{
        type: types.LIST_USER,
        payload:{
            body: data
        }
    }
}