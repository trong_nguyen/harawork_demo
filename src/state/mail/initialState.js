import { Record } from 'immutable';

const InitialState = Record({
    typeMail: 'inbox?',
    checkStarred: null,
    checkLable: null,
    removeMail: null,
    readMail: null,
    loadDetail:null,
    removeDraft:null,
    deletedDraft:null,
    removeUser:null,
    addUser:null,
    listUser:null,
});

export default InitialState;
