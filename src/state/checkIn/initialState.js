import { Record } from 'immutable';

const InitialState = Record({
    checkInStatus: null
});

export default InitialState;
