import types from './types';

export function getCheckInStatus(data) {
    return {
        type: types.GET_CHECKIN_STATUS,
        payload: {
            body: data
        }
    }
}