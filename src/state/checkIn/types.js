import key from 'keymirror';

export default key({
    GET_CHECKIN_STATUS: null,
    GET_CHECKIN_STATUS_SUCCESS: null,
}, "APP");
