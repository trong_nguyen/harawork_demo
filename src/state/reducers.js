import { combineReducers } from 'redux';

import { navReducer } from '../route';

import app from './app/reducer';
import checkIn from './checkIn/reducer';
import notify from './notify/reducer';
import bpm from './bpm/reducer';
import leave from './leave/reducer';
import aprov from './aprov/reducer';
import mail from './mail/reducer';
import discuss from './discuss/reducer';
import publicActions from './publicActions/reducer';

const appReducer = combineReducers({
    app,
    checkIn,
    notify,
    bpm,
    leave,
    aprov,
    mail,
    discuss,
    publicActions,
    nav: navReducer,
});

export default appReducer;
