import moment from 'moment';
import 'moment-timezone';
import 'moment/locale/vi';

import vi from './locales/vi';
import en from './locales/en';

import * as Localization from 'expo-localization';
import i18n from 'i18n-js';

export default function lang() {
    const localization = {
        en: en,
        vi: vi
    };
    i18n.fallbacks = true;
    i18n.translations = localization;
    i18n.locale = Localization.locale;
    // const lang = new DangerZone.Localization.LocaleStore(localization);
    //const lang = new Localization.locale(localization)
    const rp = i18n.locale;
    let indexSplit = rp.indexOf("-");
    let language = rp;
    let locale = language;
    if (indexSplit > 0) {
        language = rp.substring(0, indexSplit);
        locale = rp.substring(indexSplit + 1);
    }
    
    var locales = Object.keys(localization);
    
    if (locales.indexOf(locale) > -1) {
        if (locale != 'en') {
            moment.locale(locale);
        }
    }
    if (locales.indexOf(lang) > -1) {
        if (lang != 'en') {
            lang.setLocale(language);
        }
    }
}


// export default lang;