const listNewItemLoad = require('./skeleton_loading.json');
const listItemLoadFrame = require('./skeleton_frame_loading.json')

export default {
    listNewItemLoad,
    listItemLoadFrame
}