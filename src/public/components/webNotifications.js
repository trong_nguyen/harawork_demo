import React, { Component } from 'react';
import { View, WebView, Text, Platform } from 'react-native';
import HeaderScreen from './headerScreen';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import settingApp from '../config'
import Loading from './loading';
import actions from '../../state/action';

class WebNotifications extends Component{
    constructor(props){
        super(props)
        this.state={
            uri: null,
            isReload: true
        }

        this.data = props.navigation.state.params;
        this.token = props.app.authHaravan;
    }

    componentWillReceiveProps(nextProps){
        if(nextProps && nextProps.app && nextProps.app.reloadNotify && (nextProps.app.reloadNotify==true) ){
            const {access_token} = this.token.auth;
            this.setState({
                uri: `${this.data.link}?access_token=${access_token}&view=true`,
                isReload:false
            }, () =>{
               this.props.actions.reloadNotify(null)
            })                
        }
    }

    checkReload(){
        const { isReload } = this.state;
        const {access_token} = this.token.auth;
        if(isReload == true){
            this.setState({
                uri: `${this.data.link}?access_token=${access_token}&view=true`,
                isReload:false
            })
        }
    }

    componentDidMount(){
        const {access_token} = this.token.auth;
        if(this.data){
            this.setState({
                uri: `${this.data.link}?access_token=${access_token}&view=true`,
                isReload:false
            })
        }
    }

    renderHeader(){
        return(
            <HeaderScreen 
                navigation ={this.props.navigation}
            />
        )
    }

    render(){
        const {access_token} = this.token.auth;
        let { uri } = this.state;
        var jsCode = `document.getElementsByTagName('header')[0].innerHTML = ""`
        return(
            <View style={{flex:1}}>
                {this.renderHeader()}
                {uri ?
                    <WebView
                        source={{uri}}
                        style={{flex:1}}
                        injectedJavaScript={jsCode}
                        domStorageEnabled={true}
                        javaScriptEnabled={true}
                        useWebKit={true}
                        >   
                    </WebView>
                    : 
                    <Loading/>
                }
            </View>
        )
    }
}
const styles={
    container: {
        flex: 1,
        ...Platform.select({
            android: {
                marginTop: settingApp.statusBarHeight,
            },
            ios: {
                marginTop:settingApp.isIPhoneX ? (settingApp.statusBarHeight+24):( settingApp.statusBarHeight),
            },
        }),
        backgroundColor: 'rgba(0,0,0,0.5)',
        width: settingApp.width
    },
}
const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(mapStateToProps, mapDispatchToProps)(WebNotifications);