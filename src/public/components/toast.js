import React, { Component } from 'react';
import { View, Text, Keyboard, TouchableOpacity } from 'react-native';
import { HaravanIc } from '../../Haravan';
import * as Animatable from 'react-native-animatable';
import { Ionicons } from '@expo/vector-icons';
import settingApp from '../config';

class Toast extends Component {
    constructor(props) {
        super(props);

        this.state = {
            msg: '',
            isShow: false
        }

        _this = this;
    }

    async show(msg, action) {
        if (!_this.state.isShow) {
            Keyboard.dismiss();
            if (action) {
                _this.action = action;
                setTimeout(async() => await _this.close || '', 4000);
            }
            _this.setState({ msg }, () => _this.setState({ isShow: true }));
            setTimeout(async() => await _this.close || '', 4000);
        } else {
            await _this.close;
            this.show(msg, action);
        }
    }

    async close() {
        _this.action = null;
        if (_this.toast && _this.toast.animate) {
            const result = await _this.toast.animate('fadeOutDown', 350);
            if (result.finished) {
                _this.setState({ isShow: false, msg: '' });
            }
        }
        else{
            _this.setState({ isShow: false, msg: '' });
        }
    }

    async act() {
        const { type, data, navigation } = _this.action;
        if (type && type === 'undoMail') {
            navigation.navigate('MailDetail', { item:data });
            const result = await HaravanIc.undoMail(data.id);
            // console.log('------------------------------------');
            // console.log('undo Mail', result);
            // console.log('------------------------------------');
        }
        else if (!type) {
            _this.action()
        }
        _this.close() ? _this.close() :''
    }


    render() {
        const { msg, isShow } = this.state;
        const height = settingApp.isIPhoneX ? 72 : 50;
        if (isShow) {
            if (_this.action) {
                return (
                    <Animatable.View
                        animation='fadeInUp'
                        duration={350}
                        ref={refs => this.toast = refs}
                        style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height, backgroundColor: '#475C71' }}>
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', paddingLeft: 15, paddingRight: 15 }}>
                            <View style={{ flex: 1, marginRight: 20, flexDirection: 'row' }}>
                                <Text style={{ color: '#ffffff', fontSize: 16, fontWeight: '600' }} numberOfLines={1}>
                                    {msg}
                                </Text>
                            </View>
                            <TouchableOpacity
                                onPress={() => this.act()}
                                style={{ height: 50, justifyContent: 'center', alignItems: 'flex-end' }}>
                                <Text style={{ color: '#ffffff', fontSize: 18, textAlign: 'right' }}>Hoàn Tác</Text>
                            </TouchableOpacity>
                        </View>
                        {settingApp.isIPhoneX && <View style={{ width: settingApp.width, height: 22 }} />}
                    </Animatable.View>
                )
            } else {
                return (
                    <Animatable.View
                        animation='fadeInUp'
                        duration={350}
                        ref={refs => this.toast = refs}
                        style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height, backgroundColor: '#475C71' }}>
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', paddingLeft: 15 }}>
                            <Text style={{ color: '#ffffff', fontSize: 16, fontWeight: '600', maxWidth: (settingApp.width - 60) }}>
                                {msg}
                            </Text>
                            <TouchableOpacity
                                onPress={() => this.close() ? this.close() : ''}
                                style={{ width: 50, height: 50, justifyContent: 'center', alignItems: 'center' }}>
                                <Ionicons name='ios-close' color='#FFFFFF' size={35} />
                            </TouchableOpacity>
                        </View>
                        {settingApp.isIPhoneX && <View style={{ width: settingApp.width, height: 22 }} />}
                    </Animatable.View>
                )
            }
        } else {
            return <View />
        }
    }
}

const ToastComponent = Toast;
Toast = Toast.prototype;

export {
    ToastComponent,
    Toast
};