import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import settingApp from '../config';
class Item extends Component {

    render() {
        const { name, value, noneBorder } = this.props;
        const { colorSperator } = settingApp;
        return (
            <View style={{
                flex: 1,
                flexDirection: 'row',
                height: 55,
                paddingRight: 15,
                borderTopColor: colorSperator,
                borderTopWidth: noneBorder ? 0 : 1
            }}>
                <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => value && value.props && value.props.onPress && value.props.onPress() ? value.props.onPress : null}
                    style={{ flex: 1, justifyContent: 'center' }}
                >
                    {name}
                </TouchableOpacity>
                <View style={{ flex: 1 }}>
                    {value}
                </View>
            </View>
        )
    }
}

export default Item;