import React, { Component } from 'react';
import { View, WebView, Platform } from 'react-native';

class PDFReader extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true
        }
    }

    render() {
        let url = this.props.source.uri;
        let startInLoadingState = false;
        if (Platform.OS === 'android') {
            url = `http://docs.google.com/gview?embedded=true&url=${url}`;
            startInLoadingState = true;
        }
        return (
            <View style={{ flex: 1 }}>
                <WebView
                    source={{ uri: url }}
                    style={{ flex: 1, backgroundColor: '#000' }}
                    automaticallyAdjustContentInsets={false}
                    javaScriptEnabled={true}
                    injectedJavaScript={`document.getElementsByClassName('ndfHFb-c4YZDc-cYSp0e ndfHFb-c4YZDc-oKVyEf')[0].style.backgroundColor='#000'`}
                    domStorageEnabled={true}
                    decelerationRate="normal"
                    startInLoadingState={startInLoadingState}
                    scalesPageToFit={true}
                    onLoadEnd={() => this.setState({ isLoading: false })}
                />
                {this.state.isLoading &&
                    <View
                        style={{
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0,
                            backgroundColor: '#000'
                        }}
                    />}
            </View>
        )
    }
}


export default PDFReader;