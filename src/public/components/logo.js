import React, { Component } from 'react';
import { View, Image } from 'react-native';
import imgApp from '../image';
import settingApp from '../config';

class Logo extends Component {

    render() {
        const { width, height } = settingApp;
        return (
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Image 
                    source={imgApp.logoLogin}
                    style={{ width: (width * 0.58), height: (height * 0.09), minWidth: 220, minHeight:60 }}
                    resizeMode='contain'
                />
            </View>
        )
    }
}

export default Logo;