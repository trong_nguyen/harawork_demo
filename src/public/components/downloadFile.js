import React, { Component } from 'react';
import { Platform } from 'react-native';
import * as WebBrowser  from 'expo-web-browser';


export default async function downLoadFile(id, userInfo, isView){
    const url = `https://ic.hara.vn/call/im_api/attachment/${id}?isView=${isView}&access_token=${userInfo}`;
    await WebBrowser.openBrowserAsync(url)
    // if (Platform.OS === 'ios') {
    //    await WebBrowser.openAuthSessionAsync(url)
    // }
    // else {
    //     await WebBrowser.openBrowserAsync(url)
    // }
}