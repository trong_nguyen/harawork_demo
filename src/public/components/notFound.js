import React, { Component } from 'react';
import { View, Text } from 'react-native';
import settingApp from '../config';
import { Ionicons } from '@expo/vector-icons';

class NotFound extends Component {

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ opacity: 0.5 }}>
                    <Ionicons name='md-search' color={settingApp.color} size={150} />
                </View>
                <View style={{ alignItems: 'center' }}>
                    <Text style={{ color: settingApp.color, fontSize: 15, fontWeight: 'bold' }}>
                        Không tìm thấy kết quả!
                    </Text>
                    <Text style={{ color: settingApp.colorText, fontSize: 14, marginTop: 10 }}>
                        Hệ thống hiện tại không tìm thấy kết quả.
                    </Text>
                </View>
            </View>
        )
    }
}

export default NotFound;