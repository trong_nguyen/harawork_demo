import * as notifyHR from './notifyHR';
import {HaravanHr} from '../../Haravan'

export async function controlNotifyHR (link) {
    let result = null;
    const leave_detail = 'supervisor/approveleave/'; 
    const me_leave = 'me/leaves/update/'

    if(link.indexOf(leave_detail) > -1){
        result = await notifyHR.getLeaveDetail(link)
    }

    else if(link.indexOf(me_leave) > -1){
        const id = link.substr(link.lastIndexOf('/') + 1);
        let res = await HaravanHr.meLeaveDetail(id)
        if(res && res.data && !res.error){
            const body ={
                data: {
                    item:res.data,
                    leaveType:res.data.leaveTypeName
                },
                routeName:'DetailFormLeave'
            }
            result = body;
        }
    }
    return result
}