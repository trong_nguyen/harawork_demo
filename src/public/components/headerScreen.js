import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import HeaderWapper from './headerWapper';

class HeaderScreen extends Component {

    render() {
        let { title, navigation, buttonLeft, buttonRight } = this.props;
        title = title || '';
        buttonLeft = buttonLeft || <Ionicons name='ios-arrow-back' size={23} color='#ffffff' />;
        buttonRight = buttonRight || <View style={{ width: 50, height: 44 }} />;
        return (
            <HeaderWapper style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                <TouchableOpacity
                    onPress={() => navigation.pop()}
                    style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                    {buttonLeft}
                </TouchableOpacity>
                <Text style={{ fontSize: 16, color: '#ffffff', fontWeight: 'bold' }}>
                    {title}
                </Text>
                {buttonRight}
            </HeaderWapper>
        )
    }
}

export default HeaderScreen;