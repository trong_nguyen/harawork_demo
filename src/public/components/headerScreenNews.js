import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StatusBar, Platform } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import settingApp from '../config';
import * as Animatable from 'react-native-animatable';

class HeaderScreen extends Component {

    buttonLeft(){
        return(
            <TouchableOpacity
                onPress={() => {
                    this.props.navigation.pop()}}
                style={{ backgroundColor: 'transparent', width: 50, height: 44, justifyContent: 'center', alignItems: 'center' }}>
                    <Ionicons name='ios-arrow-back' size={23} color='#000000' />
            </TouchableOpacity>
        )
    }

    render() {
        let { title, buttonRight, buttonLeft, navigation, animated } = this.props;
        let { width, statusBarHeight } = settingApp;
        title = title || '';
        buttonRight = buttonRight || <View style={{ width: 50, height: 44 }} />;
        buttonLeft = buttonLeft || this.buttonLeft();
        return (
            <View style={{
                backgroundColor:'#FFFFFF',
                ...this.props.style
            }}>
                <StatusBar barStyle={Platform.OS === 'ios' ? 'dark-content' : 'default'}/>
                <View style={{ width, height: statusBarHeight, backgroundColor:'#FFFFFF',...this.props.style }} />
                <View style={{ width, height: 44, flexDirection: 'row', alignItems:'center', justifyContent:'space-between'}} >
                        <View style={{ flex: 1, backgroundColor: 'transparent', flexDirection: 'row', justifyContent:'space-between', alignItems:'center' }}>
                            {buttonLeft}
                            <Animatable.View 
                                animation='fadeIn'
                                style={{justifyContent:'center', alignItems:'center', width:settingApp.width*0.6}}>
                                <Text 
                                    numberOfLines={1}
                                    style={{ fontSize: 17, color: '#000000', fontWeight: 'bold'}}>
                                        {title}
                                </Text>
                            </Animatable.View>
                            {buttonRight}
                        </View>
                </View>
            </View>
        )
    }
}

export default HeaderScreen;