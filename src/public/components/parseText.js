import React, { Component } from 'react';
import { ActivityIndicator, View, WebView, Platform, Linking } from 'react-native';
import * as _ from 'lodash';
import * as WebBrowser  from 'expo-web-browser';
import Collapsible from 'react-native-collapsible';

const BODY_TAG_PATTERN = /\<\/ *body\>/;

var script = `
(function() {
var wrapper = document.createElement("div");
wrapper.id = "height-wrapper";
while (document.body.firstChild) {
    wrapper.appendChild(document.body.firstChild);
}
document.body.appendChild(wrapper);
var i = 0;
function updateHeight() {
    document.title = wrapper.clientHeight;
    window.location.hash = ++i;
}
updateHeight();
window.addEventListener("load", function() {
    updateHeight();

    window.onload = function() {
        init();
        doSomethingElse();
      };

});
window.addEventListener("resize", updateHeight);
}());
try{
    window.isHideZopim = true;
    window.isMobileApp = true;

    var originalPostMessage = window.postMessage;
    
    var patchedPostMessage = function(message, targetOrigin, transfer) { 
        originalPostMessage(message, targetOrigin, transfer);
    };
    
    patchedPostMessage.toString = function() { 
        return String(Object.hasOwnProperty).replace('hasOwnProperty', 'postMessage'); 
    };
    
    window.postMessage = patchedPostMessage;

    window.postMessageActionJs = function(url){
       window.postMessage(url,"*");
    };
    
    var imgs = document.querySelectorAll("img");
    for( var i = 0; i < imgs.length; i++ ) {
        imgs[i].style.maxWidth = '300px';
        imgs[i].style.height = 'auto';
        var imgSrc = imgs[i].getAttribute("src");
        if (imgSrc.substring(0, 4) != "http") {
            imgSrc = "https:" + imgSrc;
            imgs[i].src = imgSrc;
        }
        else{
            imgs[i].src = imgSrc;
        }
    }
    
    var iframes = document.querySelectorAll("iframe");
    for( var j = 0; j < iframes.length; j++ ) {
        iframes[j].style.width = '300px';
        iframes[j].style.height = '250px';
        var iSrc = iframes[j].getAttribute("src");
        if (iSrc.substring(0, 4) != "http") {
            iSrc = "https:" + iSrc;
            iframes[j].src = iSrc;
        }
        else{
            iframes[i].src = iSrc;
        }
    }
    
    var atags = document.querySelectorAll("a");
    for( var i = 0; i < atags.length; i++ ) {
        var href = atags[i].href;
        if(href) {
            atags[i].href = "javascript:window.postMessageActionJs('" + href +"')";
        }else{
          atags[i].href = "javascript:void(0)";
        }
    }

}catch(e){
    window.postMessage(e,'*');
}
`;


const style = `
<style>
body, html, #height-wrapper {
    margin: 0;
    padding: 0;
    word-wrap: break-word;
}
#height-wrapper {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
}
</style>
<script>
${script}
</script>
`;

const codeInject = (html) => html.replace(BODY_TAG_PATTERN, style + "</body>");

class ParseText extends Component {
    constructor(props) {
        super(props);

        this.props.minHeight = 100;
        this.state = {
            isClose: true,
            realContentHeight: this.props.minHeight
        }
    }

    componentDidMount() {
        this.setState({ isClose: false })
        // setTimeout(() =>, 500);
    }

    handleNavigationChange(navState) {
        if (navState.title) {
            const realContentHeight = parseInt(navState.title, 10) || 0; // turn NaN to 0
            this.setState({ realContentHeight });
        }
        if (typeof this.props.onNavigationStateChange === "function") {
            this.props.onNavigationStateChange(navState);
        }
    }

    validURL(str) {
        var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
        return regexp.test(str);
    }

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    async onMessage(e) {
        const { data } = e.nativeEvent;
        if (data && data.length > 0) {
            const isURL = this.validURL(data);
            const isEmail = this.validateEmail(data);
            if (isURL) {
                WebBrowser.openBrowserAsync(data);
            } else if (!!(data.indexOf('mailto:') > -1) || isEmail) {
                let email = data;
                if (!!(data.indexOf('mailto:') > -1)) email = email.replace('mailto:', '');
                Linking.openURL(`mailto:${email}`)
                    .catch(e => console.log('Error: ', e));
            }
        }
    }

    render() {
        const { description } = this.props;
        const { source, style, minHeight, ...otherProps } = this.props;
        const html = `
        <!DOCTYPE html>
          <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Document</title>
            </head>
          <style type="text/css">
            body {
                font-size: "17px";
                font-family: 'Arial';
                color: black;
                width: 95%;
                height: 'auto';
                word-wrap: break-word;
                line-height:26px;
            }
            html {
                width: 100%;
                height: 'auto';
                overflow: hidden;
                word-wrap: break-word;
                margin: 0 auto;
                }
        </style>
          <body>`
            + _.unescape(typeof (description) == 'string' ? description : '')
            + `</body>
        </html>`;

        if (!html) {
            throw new Error("WebViewAutoHeight supports only source.html");
        }

        if (!BODY_TAG_PATTERN.test(html)) {
            throw new Error("Cannot find </body> from: " + html);
        }
        const patchPostMessageFunction = function () {
            var all_links = document.querySelectorAll('a[href]');
            if (all_links) {
                for( var i = 0; i < all_links.length; i++ ) {
                    all_links[i].onclick = function() {
                        window.postMessage(all_links[i].href, "*");
                    }
                }
            }

            var originalPostMessage = window.postMessage;
      
            var patchedPostMessage = function (message, targetOrigin, transfer) {
              originalPostMessage(message, targetOrigin, transfer);
            };
      
            patchedPostMessage.toString = function () {
              return String(Object.hasOwnProperty).replace('hasOwnProperty', 'postMessage');
            };
      
            window.postMessage = patchedPostMessage;
          };
      
          const patchPostMessageJsCode = '(' + String(patchPostMessageFunction) + ')();';
        if (Platform.OS === 'ios') {
            return (
                <View style={{ flex: 1 }}>
                    <WebView
                        source={{ html: codeInject(html) }}
                        bounces={true}
                        style={[style, { height:this.state.realContentHeight, marginBottom: 10, backgroundColor:'transparent', overflow:'hidden', padding:5}]}
                        javaScriptEnabled={true}
                        injectedJavaScript={patchPostMessageJsCode}
                        domStorageEnabled={true}
                        onNavigationStateChange={this.handleNavigationChange.bind(this)}
                        scalesPageToFit={false}
                        scrollEnabled={false}
                        onMessage={(e) => this.onMessage(e)}
                        automaticallyAdjustContentInsets={true}
                    />
                </View>
            )
        } else {
            return (
                <View style={{ flex: 1 }}>
                    {/* <Collapsible collapsed={this.state.isClose} collapsedHeight={30}> */}
                        <WebView
                            source={{ html: codeInject(html) }}
                            bounces={true}
                            style={[style, { height: this.state.realContentHeight, marginBottom: 10,backgroundColor:'transparent', overflow:'hidden', padding:5}]}
                            javaScriptEnabled={true}
                            injectedJavaScript={patchPostMessageJsCode}
                            domStorageEnabled={true}
                            onNavigationStateChange={this.handleNavigationChange.bind(this)}
                            scalesPageToFit={true}
                            scrollEnabled={false}
                            onMessage={(e) => this.onMessage(e)}
                            automaticallyAdjustContentInsets={true}
                            useWebKit={true}
                        />
                    {/* </Collapsible> */}
                    {this.state.isClose &&
                        <View style={{ position: 'absolute', top: 5, left: 0, right: 0, justifyContent: 'center', alignItems: 'center' }}>
                            {/* <ActivityIndicator size={'small'} color='#8E8E93' /> */}
                        </View>}
                </View>
            )
        }
    }
}

export default ParseText;