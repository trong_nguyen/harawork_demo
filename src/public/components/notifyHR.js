import {HaravanHr} from '../../Haravan'


export async function getLeaveDetail(link){
    const id = link.substr(link.lastIndexOf('/') + 1); 
    const result = await HaravanHr.getLeavesDeatail(id)
    if(result && result.data && !result.error){
        const typeTime = await checkType(result.data.typeTimeLeave)
        const titleLeaves = await checkStatus(result);
        const data ={
            item:result.data,
            titleLeaves:titleLeaves,
            typeTime: typeTime,
            
        }
        const body ={
            data:data,
            routeName:'ApprovedLeaveDetail'
        }
        return body;
    }
}

export async function checkType(type) {
    if (type == 0) {
        return { type: 'Nghỉ nguyên ngày', time: '8 giờ' }
    } else if (type == 2) {
        return { type: 'Nghỉ buổi sáng', time: '4 giờ'  }
    } else if (type == 3) {
        return { type: 'Nghỉ buổi chiều', time: '4 giờ'  }
    }
}

export async function checkStatus(item) {
    let newList = []
    if (item.requestLeave && item.requestLeave.length > 0) {
        return (
            item.requestLeave.map((e, i) => {
                newList = item.requestLeave.sort((a, b) => {
                    return new Date(b.requestDate) - new Date(a.requestDate);
                });
            })
        )
    }
    if (newList && newList.length > 0) {
        if (newList[0].status == 1
             && newList[0].type == 0) {
            return {
                title: 'Đề nghị hủy',
                requestNote: newList[0].requestNote
            }
        }
    }
}