import React, { Component } from 'react';
import { ActivityIndicator, Animated, View } from 'react-native';
import settingApp from '../config';
import imageApp from '../image';

class AsyncImage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoadEnd: false
        }
        this.thumbnailAnimated = new Animated.Value(0);
        this.imageAnimated = new Animated.Value(0);
    }

    handleThumbnailLoad() {
        Animated.timing(this.thumbnailAnimated, {
            toValue: 1,
        }).start(() => this.setState({ isLoadEnd: true }));
    }

    onImageLoad() {
        Animated.timing(this.imageAnimated, {
            toValue: 1,
        }).start();
    }

    render() {
        let { source, style, resizeMode, type } = this.props;
        let thumbnailSource = source;
        type = type || 'large';
        if (source && source.hasOwnProperty('uri')) {
            const lengthUri = source.uri.length;
            if (lengthUri > 0) {
                const picture = source.uri;
                const typeImage = picture.substr(picture.lastIndexOf('.'));
                source = { uri: picture.replace(typeImage, `_${type}${typeImage}`) }
                thumbnailSource = { uri: picture.replace(typeImage, `_${'small'}${typeImage}`) }
            } else {
                source = imageApp.no_Image;
                thumbnailSource = imageApp.no_Image;
                resizeMode = 'stretch';
            }
        }
        return (
            <View style={{ flex: 1 }}>
                {(!this.state.isLoadEnd) && <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size='small' color={settingApp.colorDisable} />
                </View>}
                <Animated.Image
                    source={thumbnailSource}
                    style={[{ position: 'absolute', left: 0, right: 0, bottom: 0, top: 0,}, { opacity: this.thumbnailAnimated }, style]}
                    onLoad={() => this.handleThumbnailLoad()}
                    onLoadEnd={() => this.handleThumbnailLoad()}
                    blurRadius={1}
                    resizeMode={resizeMode}
                />
                <Animated.Image
                    source={source}
                    style={[{ position: 'absolute', left: 0, right: 0, bottom: 0, top: 0, }, { opacity: this.imageAnimated }, style]}
                    onLoad={() => this.onImageLoad()}
                    resizeMode={resizeMode}
                />
            </View>
        )
    }
}

AsyncImage.prototype.props = {
    source: '',
    style: '',
    resizeMode: '',
    type: '',
}

export default AsyncImage;