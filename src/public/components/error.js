import React, { Component } from 'react';
import { View, Text } from 'react-native';

class Error extends Component {

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: 12, color: '#bdc3c7', marginTop: 5 }}>
                    Lấy thông tin thất bại
                </Text>
            </View>
        )
    }
}

export default Error;