import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import imgApp from '../image';
import settingApp from '../config';
import ButtonCustom from './button';
import HeaderScreenNews from './headerScreenNews'

class NotPermission extends Component {
    constructor(props) {
        super(props);

    }

    renderHeader(){
        return(
            <HeaderScreenNews
                title={'Thu nhập của tôi'}
                navigation={this.props.navigation}
            />
        )
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor:'#FFFFFF' }}>
                {this.renderHeader()}
                <View style={{ flex: 1 , alignItems:'center'}}>
                    <Image
                        source={imgApp.frameNone}
                        style={{ flex: 1, width: 200, height: 200 }}
                        resizeMode='contain'
                    />
                </View>
                <View style={{ flex: 1, alignItems: 'center' }}>
                    <Text style={{ fontSize: 18, color: settingApp.colorText,fontWeight: '600' }}>Bạn không có quyền truy cập</Text>
                    <Text style={{ fontSize: 14, color: settingApp.colorText, textAlign:'center', paddingLeft:15, paddingRight:15}}>Nội dung không tồn tại hoặc bạn không có quyền để xem. Vui lòng liên hệ người quản trị.</Text>
                    <ButtonCustom
                        onPress={() => this.props.navigation.navigate('Home')}
                        style={{
                            height: 50,
                            padding: 20,
                            paddingTop: 0,
                            paddingBottom: 0,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: '#21469B',
                            marginTop: 20,
                            borderRadius: 3,
                            ...settingApp.shadow
                        }}>
                        <Text style={{ fontSize: 14, color: '#ffffff', fontWeight: 'bold' }}>Về trang chính</Text>
                    </ButtonCustom>
                </View>
            </View>
        )
    }
}

export default NotPermission;