import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Svg ,{ Path, Rect} from 'react-native-svg';

class BoxNotifyContent extends Component {

    renderIcon(module) {
        let title = 'Hệ thống';
        let icon = (
            <Svg width="24" height="24" viewBox="0 0 24 24" fill="none">
                <Rect width="24" height="24" rx="12" fill="#2F80ED" />
                <Path fill-rule="evenodd" clip-rule="evenodd" d="M19 11.3186C19 8.27044 16.0505 6 12.6175 6C9.18446 6 6.235 8.27044 6.235 11.3186C6.235 12.5696 6.76697 13.692 7.59273 14.5907L7.19934 18L11.0008 16.4537C11.5344 16.5921 12.0635 16.6372 12.6175 16.6372C16.0505 16.6372 19 14.3667 19 11.3186ZM12.6175 7.06372C15.6733 7.06372 17.9362 9.04815 17.9362 11.3186C17.9362 13.589 15.6733 15.5735 12.6175 15.5735C12.0653 15.5735 11.5885 15.5232 11.1215 15.3831L10.9419 15.3293L8.46191 16.3381L8.70758 14.209L8.52584 14.0273C7.75201 13.2535 7.29875 12.3186 7.29875 11.3186C7.29875 9.04815 9.56166 7.06372 12.6175 7.06372Z" fill="white" />
                <Path fill-rule="evenodd" clip-rule="evenodd" d="M15.2769 9.72301H9.95813V10.7867H15.2769V9.72301Z" fill="white" />
                <Path fill-rule="evenodd" clip-rule="evenodd" d="M13.8997 11.8504H9.95813V12.9142H13.8997V11.8504Z" fill="white" />
            </Svg>
        );

        switch (module) {
            case (1):
                title = 'Hệ thống';
                break;
            case (2):
                title = 'Giao tiếp nội bộ';
                break;
            case (3):
                title = 'Nhân sự';
                break;
            case (5):
                title = 'Công việc';
                break;
            default:''
                break;
        };

        return { icon, title };
    }

    render() {
        const { detail } = this.props;
        const { body, module } = detail;
        const { icon, title } = this.renderIcon(module);
        const user = body.substr(0, body.indexOf('đã') - 1);
        const content = body.substr(body.indexOf('đã') - 1)
        return (
            <View style={{ flex: 1, flexDirection: 'row' }}>
                <View style={{ marginRight: 15 }}>
                    {icon}
                </View>
                <View style={{ flex: 1 }}>
                    <Text style={{ fontSize: 16, color: '#212121', fontWeight: 'bold', marginTop: 5, marginBottom: 5 }}>
                        {title}
                    </Text>

                    <Text style={{ fontSize: 16, color: '#212121', lineHeight: 20 }} numberOfLines={3}>
                        <Text style={{ color: '#2F80ED' }}>{user.trim()} </Text>
                        <Text>{body.trim()}</Text>
                    </Text>
                </View>
            </View>
        )
    }
}

export default BoxNotifyContent;