import React, { Component } from 'react';
import { View, Text } from 'react-native';
import settingApp from '../config';

class TimeLine extends Component {

    render() {
        const { data } = this.props;
        return (
            <View style={{ flex: 1 }}>
                {data.map((e, i) => {
                    return (
                        <View style={{ flex: 1, flexDirection: 'row' }} key={i}>
                            <View style={{ width: 20, flexDirection: 'row', alignItems: 'center' }}>
                                <View style={{
                                    position: 'absolute',
                                    left: 4,
                                    top: 0,
                                    bottom: 0,
                                    borderLeftColor: '#dfe4e8',
                                    borderLeftWidth: 2
                                }} />
                                <View style={{ position: 'absolute', left: 0 }}>
                                    <View
                                        style={{
                                            marginTop: 10,
                                            width: 10,
                                            height: 10,
                                            borderRadius: 11 >> 1,
                                            backgroundColor: '#919eab'
                                        }}
                                    />
                                </View>
                            </View>
                            <View style={{
                                flex: 1,
                                padding: 10,
                                borderRadius: 3,
                                borderColor: settingApp.colorSperator,
                                borderWidth: 1,
                                marginTop: i == 0 ? 0 : 10
                            }}>
                                <Text style={{ color: '#0279C7', fontSize: 14 }}>
                                    {e.fullName}
                                </Text>
                                <Text style={{ color: settingApp.colorTextor, fontSize: 14, marginTop: 10 }}>
                                    {e.actions}
                                </Text>
                                <Text style={{ color: '#95a5a6', fontSize: 12, marginTop: 10 }}>
                                    {e.time}
                                </Text>
                            </View>
                        </View>
                    )
                })}
            </View>
        )
    }
}

export default TimeLine;