import React, { Component } from 'react';
import { View, Platform } from 'react-native';
import settingApp from '../config';

class HeaderWapper extends Component {

    render() {
        let { width, backgroundHeader, statusBarHeight } = settingApp;
        const { children, style, isModal } = this.props;
        statusBarHeight = (Platform.OS === 'android' && isModal) ? 0 : statusBarHeight;
        const backgroundColor = this.props.backgroundColor || '#FFFFFF';
        return (
            <View style={{
                backgroundColor,
            }}>
                <View style={{ width, height: statusBarHeight, backgroundColor: settingApp.color }} />
                <View style={{ width, height: 44, flexDirection: 'row', ...style }} >
                    {children}
                </View>
            </View>
        )
    }
}

export default HeaderWapper;