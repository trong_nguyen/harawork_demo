import React, { Component } from 'react';
import { View, Text, Image, ActivityIndicator } from 'react-native';
import HTML from 'react-native-render-html';
import settingApp from '../config';

class HTMLView extends Component{
    constructor(props){
        super(props)
        this.state={
        }
    }
    componentDidMount(){

    }

    checkContent(){
        const { value } = this.props
        if(value){
            return value
        }
        else{
            return null
        }
    }

    retunImage(img, index){
        let { src } = img;
        let icon = img.alt && img.alt.length>0
        let width =icon ? 12 : ( img.width ? (img.width*1) : 200)
        let height = icon ? 12 : (img.height ? (img.height*1): 200)
        let newsrc = null
        if (src.substring(0, 4) != "http") {
            let imgSrc = "https:" + src;
            newsrc = imgSrc;
        }else{
            newsrc = img.src
        }
        return(
            <Image
                key={index}
                source={{uri:newsrc}}
                style={{flex:1, minWidth:width, minHeight:height, resizeMode:'contain', maxWidth:(settingApp.width-30)}}
            />
        )
    }

    render(){
        let content = this.checkContent()
        if(content){
            return(
                <HTML 
                    html={content} 
                    renderers={{img: (img, index) => this.retunImage(img, index)}}   
                />
            )
        }
        else{
            return(
                <View style={{justifyContent:'center', alignItems:'center', width:settingApp.width, height:50}}>
                    <ActivityIndicator size="small" color="" />
                </View>
            )
        }
        
    }
}
export default HTMLView;