import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { SplashScreen , Updates} from 'expo';
import imgApp from '../image';
import settingApp from '../config';
import ButtonCustom from './button';

class ErrorComponent extends Component {
    constructor(props) {
        super(props);

        SplashScreen.hide();
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 0.5 }} />
                <View style={{ flex: 1 }}>
                    <Image
                        source={imgApp.errorView}
                        style={{ flex: 1, width: undefined, height: undefined }}
                        resizeMode='contain'
                    />
                </View>
                <View style={{ flex: 1, alignItems: 'center' }}>
                    <Text style={{ fontSize: 20, color: '#21469B', fontWeight: 'bold' }}>Có lỗi trong quá trình xử lý</Text>
                    <ButtonCustom
                        onPress={() => Updates.reload()}
                        style={{
                            height: 50,
                            padding: 20,
                            paddingTop: 0,
                            paddingBottom: 0,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: '#21469B',
                            marginTop: 20,
                            borderRadius: 3,
                            ...settingApp.shadow
                        }}>
                        <Text style={{ fontSize: 14, color: '#ffffff', fontWeight: 'bold' }}>Vui lòng thử lại</Text>
                    </ButtonCustom>
                </View>
            </View>
        )
    }
}

export default ErrorComponent;