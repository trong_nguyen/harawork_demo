import React, { Component } from 'react';
import { View, Text, TouchableOpacity,StatusBar, Platform } from 'react-native';
import settingApp from '../config';
import HeaderWapper from './headerWapper';
import { connect } from 'react-redux';
import { DrawerActions } from 'react-navigation';
import Svg ,{ Path,} from 'react-native-svg';

class HeaderNewStyles extends Component {
    constructor(props) {
        super(props);
    }

    actionsMenu() {
        this.props.dispatch(DrawerActions.openDrawer())
    }

    renderButtonLeft() {
        return (
            <TouchableOpacity
                onPress={() => this.actionsMenu()}
                style={{ width: 55, height: 44, backgroundColor: 'transparent', justifyContent: 'center', alignItems: 'center' }}>
                <Svg width="24" height="18" viewBox="0 0 24 18" fill="none">
                    <Path d="M12 18H1.5C0.672 18 0 17.328 0 16.5C0 15.672 0.672 15 1.5 15H12C12.828 15 13.5 15.672 13.5 16.5C13.5 17.328 12.828 18 12 18Z" fill="#4D4D4D"/>
                    <Path d="M22.5 3H1.5C0.672 3 0 2.328 0 1.5C0 0.672 0.672 0 1.5 0H22.5C23.328 0 24 0.672 24 1.5C24 2.328 23.328 3 22.5 3Z" fill="#4D4D4D"/>
                    <Path d="M22.5 10.5H1.5C0.672 10.5 0 9.828 0 9C0 8.172 0.672 7.5 1.5 7.5H22.5C23.328 7.5 24 8.172 24 9C24 9.828 23.328 10.5 22.5 10.5Z" fill="#4D4D4D"/>
                </Svg>
            </TouchableOpacity>
        )
    }

    renderTitle() {
        let { title } = this.props;
        let { orgname } = this.props.app.authHaravan.userInfo;
        if (!title) {
            title = (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={settingApp.styleTitle}>
                        {orgname}
                    </Text>
                </View>
            )
        }
        return (
            <View style={{ flex: 1 }}>
                {title}
            </View>
        )
    }

    renderButtomRight() {
        let { buttonRight } = this.props;
        buttonRight = buttonRight || <View />;
        return (
            <View style={{ width: 55, height: 44, backgroundColor: 'transparent' }}>
                {buttonRight}
            </View>
        )
    }

    render() {
        let { width, backgroundHeader, statusBarHeight } = settingApp;
        return (
            <View style={{
                backgroundColor:'#FFFFFF',
                // ...settingApp.shadow
            }}>
                <StatusBar barStyle={Platform.OS === 'ios' ? 'dark-content' : 'default'}/>
                <View style={{ width, height: statusBarHeight, backgroundColor:'#FFFFFF' }} />
                <View style={{ width, height: 44, flexDirection: 'row'}} >
                        <View style={{ flex: 1, backgroundColor: 'transparent', flexDirection: 'row' }}>
                            {this.renderButtonLeft()}
                            {this.renderTitle()}
                            {this.renderButtomRight()}
                        </View>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => ({ ...state });

export default connect(mapStateToProps)(HeaderNewStyles);