import React, { Component } from 'react';
import { View, Text } from 'react-native';
import settingApp from '../config';
import { HaravanHr } from '../../Haravan';
import { Utils, SvgCP } from '../../public'
import { Feather } from '@expo/vector-icons';

class IconTabNotCheckIn extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isShow: false
        }
    }

    async componentDidMount() {
        const date = new Date();
        const firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        const fromDate = Utils.formatTime(firstDay, 'YYYY-MM-DD');
        const toDate = Utils.formatTime(date, 'YYYY-MM-DD');
        const resultNotCheckIn = await HaravanHr.getCheckinHistory(`checkin/distributed?`, `fromDate=${fromDate}&toDate=${toDate}`);
        if(resultNotCheckIn && resultNotCheckIn.data && !resultNotCheckIn.error) {
            this.setState({ isShow: (resultNotCheckIn.data.length > 0) });
        }
    }

    render() {
        const { focused } = this.props;
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingLeft: 10, paddingRight: 10 }}>
                <SvgCP.noneCheckInTab focused={focused}/>
                {this.state.isShow && <View
                    style={{
                        position: 'absolute',
                        top: 5,
                        right: 0,
                        width: 10,
                        height: 10,
                        borderRadius: 10,
                        backgroundColor: 'red'
                    }}
                />}
            </View>
        )
    }
}

export default IconTabNotCheckIn;