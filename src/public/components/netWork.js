import React, { Component } from 'react';
import { Animated, View, Text, Platform, NetInfo } from 'react-native';
import { Feather } from '@expo/vector-icons';
import { StackActions, NavigationActions } from 'react-navigation';
import { Utils } from '../../public';

import settingApp from '../config';
class NetWork extends Component {
    constructor(props) {
        super(props);

        this.height = settingApp.isIPhoneX ? 55 : Platform.OS === 'ios' ? 40 : 50;
        this.state = {
            top: new Animated.Value(-(this.height + 1)),
            height: new Animated.Value(0)
        }

        this.isConnect = true;
    }

    componentDidMount() {
        NetInfo.addEventListener('connectionChange',
            (connectionInfo) => this.handleFirstConnectivityChange(connectionInfo)
        );
    }

    handleFirstConnectivityChange(connectionInfo) {
        if (connectionInfo.type === 'none' && this.isConnect) {
            this.show(true);
            Animated.timing(
                this.state.height,
                {
                    toValue: settingApp.isIPhoneX ? 15 : 25,
                    duration: 250
                },
            ).start();
            this.isConnect = false;
        } else if (!this.isConnect) {
            this.show(false);
            Animated.timing(
                this.state.height,
                {
                    toValue: 0,
                    duration: 250
                },
            ).start(() => {
                Utils.reload(this.props.store);
            });
            this.isConnect = true;
        }
    }

    show(status) {
        let value = status ? 0 : -(this.height + 1);
        Animated.timing(
            this.state.top,
            {
                toValue: value,
                duration: 250
            },
        ).start();
    }

    render() {
        const { width, backgroundHeader } = settingApp;
        const { height } = this.state;
        return (
            <View style={{ flex: 1 }}>
                <Animated.View style={{ width, height, backgroundColor: backgroundHeader }} />
                {this.props.children}
                <Animated.View style={{
                    position: 'absolute',
                    top: this.state.top,
                    left: 0,
                    right: 0,
                    width,
                    height: this.height,
                    alignItems: 'flex-end',
                    justifyContent: 'center',
                    flexDirection: 'row',
                    paddingBottom: 5,
                    backgroundColor: '#FF9500'
                }}>
                    <Feather name='wifi-off' color='#ffffff' size={12} />
                    <Text style={{ marginLeft: 10, fontSize: 12, color: '#ffffff', fontWeight: 'bold' }}>
                        Mất kết nối mạng. Vui lòng thử lại sau
                    </Text>
                </Animated.View>
            </View>

        )
    }
}

export default NetWork;