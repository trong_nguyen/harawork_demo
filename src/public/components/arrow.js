import React, { Component } from 'react';
import { Animated, View, Text } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';

class Arrow extends Component {
    constructor(props) {
        super(props);

        this.state = {
            position: new Animated.Value(0)
        }
    }



    componentWillReceiveProps(nextProps) {
        Animated.timing(
            this.state.position,
            {
                toValue: !nextProps.isOpen ? 0 : 1,
                duration: 150,
                useNativeDriver: true
            }
        ).start();
    }

    render() {
        let { color } = this.props;
        color = color || '#9CA7B2';
        const rotateProp = this.state.position.interpolate({
            inputRange: [0, 1],
            outputRange: ["0deg", "90deg"]
        })
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                <Animated.View style={{
                    transform: [
                        {
                            rotate: rotateProp
                        }
                    ]
                }}>
                    <MaterialIcons name='keyboard-arrow-right' color={color} size={23} />
                </Animated.View>
            </View>
        )
    }
}

export default Arrow;