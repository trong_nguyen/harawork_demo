import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import settingApp from '../config';
import AsyncImage from './asyncImage';
import Svg ,{ Path,} from 'react-native-svg';

class AvatarCustom extends Component{
    avatar(name) {
        let { fontSize} = this.props;
        fontSize = fontSize || 30;
        if (name && name.length > 0) {
            const arr_color = ['#536DFE', '#5AC8FA', '#FFCC00', '#22C993', '#666AD1'];
            let listName = name.split(' ');
            let shortName = '';
            listName.map(e => {
                shortName = shortName + e[0]
            });
            shortName = shortName.substr(-2);
            let bgColor = '';
            const charCodeShortName = shortName.charCodeAt(0);
            switch (true) {
                case (charCodeShortName >= 65 && charCodeShortName < 70) && arr_color[0] !== undefined: bgColor = arr_color[0]; break;
                case (charCodeShortName >= 70 && charCodeShortName < 75) && arr_color[1] !== undefined: bgColor = arr_color[1]; break;
                case (charCodeShortName >= 75 && charCodeShortName < 80) && arr_color[2] !== undefined: bgColor = arr_color[2]; break;
                case (charCodeShortName >= 80 && charCodeShortName < 85) && arr_color[3] !== undefined: bgColor = arr_color[3]; break;
                case (charCodeShortName >= 85 && charCodeShortName <= 90) && arr_color[4] !== undefined: bgColor = arr_color[4]; break;
                default: bgColor = arr_color[0] !== undefined ? arr_color[0] : bgColor; break;
            }
            return avatar = (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: bgColor }}>
                    <Text style={{ color: '#ffffff', fontSize, fontWeight: '600' }}>
                        {shortName}
                    </Text>
                </View>
            )
        } else {
            return <View />
        }
    }
    avatarPicture(picture) {
        // if (picture && source.hasOwnProperty('uri')) {
        //     const lengthUri = source.uri.length;
        //     if (lengthUri > 0) {
        //         const picture = source.uri;
        //         const typeImage = picture.substr(picture.lastIndexOf('.'));
        //         source = { uri: picture.replace(typeImage, `_${type}${typeImage}`) }
        //         thumbnailSource = { uri: picture.replace(typeImage, `_${'small'}${typeImage}`) }
        //     } else {
        //         source = imageApp.no_Image;
        //         thumbnailSource = imageApp.no_Image;
        //         resizeMode = 'stretch';
        //     }
        // }

        if (picture && picture.length > 0) {
            return avatar = (
                // <AsyncImage
                //     source={{ uri: picture }}
                //     style={{ flex: 1 }}
                //     resizeMode='stretch'
                //     type='small'
                // />
                <Image 
                    source={{ uri: picture }}
                    style={{ flex: 1 }}
                    resizeMode='stretch'
                />
            )
        } else {
            return <View />
        }
    }

    render() {
        let { v } = this.props;
        const { name, picture } = this.props.userInfo;
        const avatar = this.avatar(name);
        const avatarPicture = this.avatarPicture(picture);
        v = v ? (v + 5) : 75;
        return (
            <View style={{ }}>
                <View style={{ width: v, height: v, borderRadius: (v / 2), justifyContent: 'center', alignItems: 'center', backgroundColor: '#FFFFFF' }}>
                    <View style={{ width: (v - 5), height: (v - 5), borderRadius: ((v - 5) / 2), overflow: 'hidden' }}>
                        <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, alignItems: 'center', paddingLeft: 4 }}>
                            <Svg width={`${v}`} height={`${v}`} viewBox="0 0 10 10" fill="none" >
                                <Path d="M6.95455 6.68875C6.25696 6.45833 5.89858 5.95542 5.71662 5.55C6.72145 4.96083 7.24432 3.5625 7.24432 2.5C7.24432 1.11917 6.09972 0 4.6875 0C3.27528 0 2.13068 1.11917 2.13068 2.5C2.13068 3.56333 2.65398 4.9625 3.66009 5.55083C3.47898 5.95958 3.12145 6.46625 2.42557 6.68708C1.1983 7.07583 0 7.20292 0 9.16667V10H9.375V9.16667C9.375 7.23208 8.22997 7.11 6.95455 6.68875Z" fill="#9CA7B2" />
                            </Svg>
                        </View>
                        <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}>
                            {avatar}
                        </View>
                        <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}>
                            {avatarPicture}
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}
export default AvatarCustom