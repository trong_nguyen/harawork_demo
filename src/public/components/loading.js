import React, { Component } from 'react';
import { ActivityIndicator, View } from 'react-native';
import settingApp from '../config';

class Loading extends Component {

    render() {
        return (
            <View style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                backgroundColor: 'rgba(255,255,255,0)',
                justifyContent: 'center',
                alignItems: 'center',
                ...this.props.style
            }}>
                <ActivityIndicator size='large' color={settingApp.color} />
            </View>
        )
    }
}

export default Loading;