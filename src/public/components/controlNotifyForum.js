import { HaravanHr, HaravanIc} from '../../Haravan';

export async function controlNotifyForum (link, infoBody) {
    let data = null;
    let newLink = null
    if(link.indexOf('/detail') > -1){
        newLink=  link.replace('/detail', '').trim()
    }
    else{
        newLink= link
    }
    const id = newLink.substr(newLink.lastIndexOf('/') + 1);
    const result = await HaravanIc.threadDetail(id, infoBody);
    if(result && result.data && !result.error){
        let content  = {...result.data, 
            groupName:result.data.name, 
            groupId:result.data.id
        }
        const body ={
            data: {
               ...content
            },
            routeName:'DetailComment'
        }
        data = body;
    }
    else{
        data = null
    }
    return data
}

export async function controlNotifyGroups (link, infoBody) {
    let data = null;
    const id = link.substr(link.lastIndexOf('/') + 1);
    const result = await HaravanIc.GetDetailGroups(id, this.body)
    if(result && result.data && !result.error){
        let content  = {...result.data}
        const body ={
            data: {
               ...content
            },
            routeName:'DetailGroup'
        }
        data = body;
    }
    else{
        data = null
    }
    return data
}
