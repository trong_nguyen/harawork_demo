import React, { Component } from 'react';
import { View, Platform, Image } from 'react-native';
import { connect } from 'react-redux';

class ReviewImage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true
        }
        this.userInfo = props.app.authHaravan.auth.access_token;
    }

    render() {
        let { id } = this.props;
        return (
            <View style={{ flex: 1 }}>
                <Image
                    source={{ uri: `https://ic.hara.vn/api/attachment/${id}`,
                    method: "GET",
                    credentials: 'include',
                    headers: {
                        'Authorization': `Bearer ${this.userInfo}`
                    }}}
                    style={{ flex: 1, width: undefined, height: undefined, backgroundColor: '#000' }}
                    resizeMode='contain'
                />
            </View>
        )
    }
}


const mapStateToProps = (state) => ({ ...state })
export default connect(mapStateToProps)(ReviewImage);