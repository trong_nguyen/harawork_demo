import React, { Component } from 'react'
import { View } from 'react-native'
import Toaster from 'react-native-toaster';

import { Notifications } from 'expo';
import { NavigationActions } from 'react-navigation';
import settingApp from '../config';
import BoxNotifyContent from './boxNotify_content';
import * as controlNotifyHR from './controlNotifyHR';
import * as controlNotifyForum from './controlNotifyForum';
import Config from '../../Haravan/config';

import { HaravanIc } from '../../Haravan';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../state/action';

class BoxNotify extends Component {
    constructor(props) {
        super(props)
        this.state = { message: null }

        this.notification = false;

        this.routeName = 'WebNotifications';
        this.params = {};
        this.id = null;
        
    }

    componentWillReceiveProps(nextProps) {
        if (!this.notification && nextProps.app.authHaravan && nextProps.app.colleague) {
            this.notification = true;
            Notifications.addListener(detail => this.controlNotifi(detail, nextProps));
        }
    }

    async controlNotifi(detail, props) {
        if (detail) {
            const { data, origin, remote } = detail;
            const { link } = data;
            const { colleague, infoBody } = props.app;
            const announcements_detail = '/announcements/list/all/detail/';
            const announcements_manager_detail = '/announcements/manager/all/detail/';

            const mail_detail = 'internal_mail/inbox/detail/';
            
            const detailComment = `https://ic.${Config.apiHost}/forum`
            const detailGroups = `https://ic.${Config.apiHost}/group`

            let result = null;
            if (link.indexOf(announcements_detail) > -1 || link.indexOf(announcements_manager_detail) > -1) {
                const id = link.substr(link.lastIndexOf('/') + 1);
                result = await HaravanIc.readNotify(id, colleague, false);
                result = this.checkData(result);
                if (result) {
                    this.params = { item: result.data, isReload:true };
                    this.routeName = 'NotifiDetail';
                }
            }
            else if(link.indexOf(mail_detail) > -1){
                const id = link.substr(link.lastIndexOf('/') + 1);
                this.params = { 
                    item:{
                        mailInfo:{id:id},
                        notify:true,
                        }, 
                    isReload:true
                };
                this.routeName = 'MailDetail';
            }
            else if(data.module == 3){
                const result = await controlNotifyHR.controlNotifyHR(link)
                if(result){
                    this.params = {...result.data, isReload:true};
                    this.routeName = result.routeName;
                }
                else{
                    let data = {link:link, colleague:colleague}
                    this.params = {...data, isReload:true}
                }
            }

            else if(link.indexOf(detailComment) > -1){
                const result = await controlNotifyForum.controlNotifyForum(link, infoBody)
                if(result){
                    this.params = {data:{...result.data}, isReload:true};
                    this.routeName = result.routeName;
                    this.props.actions.discuss.newComment(result.data)
                }
                else{
                    let data = {link:link, colleague:colleague}
                    this.params = {...data, isReload:true}
                }
            }

            else if(link.indexOf(detailGroups) > -1){
                const result = await controlNotifyForum.controlNotifyGroups(link, infoBody)
                if(result){
                    this.params = {data:{...result.data}, isReload:true};
                    this.routeName = result.routeName;
                }
                else{
                    // let data = {link:link, colleague:colleague}
                    // this.params = {...data, isReload:true}
                    this.routeName = `Discuss`;
                }
            }
            

            else{
                this.props.actions.reloadNotify(true)
                let data = {link:link, colleague:colleague}
                this.params = {...data, isReload:true}
            }

            if (origin === 'selected') {
                this.gotoPage(link)
            } else if (origin === 'received') {
                this.props.actions.reloadNotify(true)
                const message = {
                    text: (
                        <View style={{ ...styles.container }}>
                            <BoxNotifyContent detail={detail.data} />
                        </View>
                    ),
                    height: 200,
                    duration: 3000
                }
                this.setState({ message }, () => this.setState({ message: null }));
            }
        }
    }

    checkData(input) {
        if (input && input.data && !input.error) {
            return input
        } else {
            return false;
        }
    }

    gotoPage(link) {
        let { routeName, params } = this;
        this.props.actions.reloadNotify(true)
        const navigateAction = NavigationActions.navigate({
            routeName,
            params,
            action: null,
        });
        this.props.dispatch(navigateAction);
    }

    render() {
        return <Toaster message={this.state.message} onPress={() => this.gotoPage()}/>;
    }
}

const styles = {
    container: {
        backgroundColor: '#ffffff',
        borderRadius: 8,
        padding: 15,
        margin: 15,
        marginTop: (settingApp.statusBarHeight + 5),
        ...settingApp.shadow
    },
    text: {
        color: '#000',
        fontWeight: 'bold'
    }
}

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts, dispatch };
};

export default connect(mapStateToProps, mapDispatchToProps)(BoxNotify);