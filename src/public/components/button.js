import React, { Component } from 'react';
import { Button } from 'react-native-material-buttons';

class ButtonCustom extends Component {

    render() {
        return (
            <Button
                disabled={this.props.disabled ? this.props.disabled : false}
                style={{ ...this.props.style }}
                rippleDuration={this.props.rippleDuration ? this.props.rippleDuration : 140}
                onPress={() => this.props.onPress()}
            >
                {this.props.children}
            </Button>
        )
    }
}

export default ButtonCustom;
