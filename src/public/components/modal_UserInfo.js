import React, { Component } from 'react';
import { View, Text, ScrollView, Linking } from 'react-native';
import Modal from "react-native-modal";
import AvatarCustom from './avatarCustom';
import settingApp from '../config';

class ModalUserInfo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            visibleModal: props.visibleModal
        }
    }

    openLinking(value, type) {
        if (type === 'phone') {
            Linking.openURL(`tel:${value}`)
                .catch(e => console.log('Error: ', e));
        } else if (type === 'mail') {
            Linking.openURL(`mailto:${value}`)
                .catch(e => console.log('Error: ', e));
        }
    }

    render() {
        const { visibleModal } = this.state;
        let { item, imageUser } = this.props;
        imageUser = imageUser || ''
        let { fullName, photo, mainPosition, haraId, phone, email, actingPositions } = item;
        console.log('item', item);
        haraId = haraId || 'Chưa cập nhật';
        const checkInfoPosition = !!(mainPosition || (actingPositions && actingPositions.length > 0));
        let titleMainPosition = <View />;
        if (mainPosition && mainPosition.hasOwnProperty('departmentName') && mainPosition.hasOwnProperty('jobtitleName')) {
            const { departmentName, jobtitleName } = mainPosition;
            titleMainPosition = (<Text style={{ fontSize: 14 }}><Text style={{ color: '#0279C7' }}>{jobtitleName}</Text> tại <Text style={{ color: '#0279C7' }}>{departmentName}</Text></Text>)
        }
        return (
            <Modal
                isVisible={visibleModal}
                onBackdropPress={() => this.setState({ visibleModal: false })}
                onBackButtonPress={() => this.setState({ visibleModal: false })}
                onModalHide={() => this.props.onClose()}
            >
                <View
                    style={{
                        backgroundColor: "white",
                        borderRadius: 10,
                        ...settingApp.shadow
                    }}>
                    <View style={{ padding: 20, backgroundColor: '#F4F6F8', borderRadius: 10 }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <AvatarCustom userInfo={{ name: fullName, picture: imageUser }} v={80} />
                        </View>
                        <View style={{ alignItems: 'center', marginTop: 10 }}>
                            <Text style={{ fontSize: 16, color: '#0279C7', fontWeight: 'bold' }}>{fullName}</Text>
                            <Text style={{ fontSize: 14, color: settingApp.colorText, marginTop: 10 }}>
                                {item.userId ? item.userId : '--'} •{' '}
                                {
                                    (phone && phone.length) > 0 ?
                                        <Text
                                            onPress={() => this.openLinking(phone, 'phone')}
                                            style={{ color: '#0279C7', textDecorationLine: 'underline' }}>{phone}</Text>
                                        :
                                        <Text style={{ color: '#0279C7', textDecorationLine: 'underline' }}> Chưa cập nhật</Text>
                                }
                            </Text>
                            {
                                (email && email.length) > 0 ?
                                    <Text
                                        onPress={() => this.openLinking(email, 'mail')}
                                        style={{ color: '#0279C7', textDecorationLine: 'underline', marginTop: 10 }}>
                                        {email}
                                    </Text>
                                    :
                                    <Text style={{ fontSize: 14, color: settingApp.colorText, marginTop: 5 }}>Chưa cập nhật</Text>
                            }
                        </View>
                    </View>
                    {checkInfoPosition && <View
                        style={{
                            borderRadius: 10,
                            marginTop: 20,
                            paddingBottom: 20,
                            maxHeight: (settingApp.height / 3.2)
                        }}>
                        <ScrollView contentContainerStyle={{ alignItems: 'center' }} >
                            {titleMainPosition}
                            {actingPositions.map((e, i) => (
                                <Text style={{ fontSize: 14, marginTop: 10 }} key={i}>
                                    <Text style={{ color: '#0279C7' }}>
                                        {e.jobtitleName}
                                    </Text> tại <Text style={{ color: '#0279C7' }}>
                                        {e.departmentName}
                                    </Text>
                                </Text>
                            ))}
                        </ScrollView>
                    </View>}
                </View>
            </Modal>
        )
    }
}

export default ModalUserInfo;