import React, { Component } from 'react';
import { View, WebView } from 'react-native';

class Editor extends Component {
    constructor(props) {
        super(props);

        this.state = {
            height: 20
        }
    }

    onMessage(e) {
        let { data } = e.nativeEvent;
        data = JSON.parse(data);
        this.props.getHTML(data.html);
        this.setState({ height: (data.height + 20) });
    }

    render() {
        const { height } = this.state;
        return (
            <View style={{ flex: 1, padding: 10 }}>
                <WebView
                    style={{ flex: 1, height }}
                    source={require('./index.html')}
                    onMessage={e => this.onMessage(e)}
                    scrollEnabled={false}
                />
            </View>
        )
    }
}

export default Editor;