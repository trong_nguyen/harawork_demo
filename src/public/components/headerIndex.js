import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import settingApp from '../config';
import HeaderWapper from './headerWapper';
import { connect } from 'react-redux';
import { DrawerActions } from 'react-navigation';
import Svg ,{ Path,} from 'react-native-svg';

class HeaderIndex extends Component {
    constructor(props) {
        super(props);

    }

    actionsMenu() {
        this.props.dispatch(DrawerActions.openDrawer())
    }

    renderButtonLeft() {
        return (
            <View
                style={{ width: 55, height: 44, backgroundColor: 'transparent', justifyContent: 'center', alignItems: 'center' }}>
            </View>
        )
    }

    renderTitle() {
        let { title } = this.props;
        let { orgname } = this.props.app.authHaravan.userInfo;
        if (!title) {
            title = (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={settingApp.styleTitle}>
                        {orgname}
                    </Text>
                </View>
            )
        }
        return (
            <View style={{ flex: 1 }}>
                {title}
            </View>
        )
    }

    renderButtomRight() {
        let { buttonRight } = this.props;
        buttonRight = buttonRight || <View />;
        return (
            <View style={{ width: 55, height: 44, backgroundColor: 'transparent' }}>
                {buttonRight}
            </View>
        )
    }

    render() {
        return (
            <HeaderWapper>
                <View style={{ flex: 1, backgroundColor: 'transparent', flexDirection: 'row' }}>
                    {this.renderButtonLeft()}
                    {this.renderTitle()}
                    {this.renderButtomRight()}
                </View>
            </HeaderWapper>
        )
    }
}

const mapStateToProps = (state) => ({ ...state });
export default connect(mapStateToProps)(HeaderIndex);