const listNotify = 'listNotify';
const colleague = 'colleague';
const historyFromAndCC = 'historyFromAndCC';
const listMail = 'listMail';
const timerApp = 'timerApp'

const listBPM = [ 
    'formWaitingApproveTotal',
    'formWaitingCompleteTotal',
    'formMe',
    'formWaitingApprove',
    'formRejectedOrApproved',
    'formWaitingComplete',
    'formCompleted',
    'formTrack',
 ]


export default {
    listNotify,
    colleague,
    historyFromAndCC,

    listBPM,

    listMail,
    timerApp
}