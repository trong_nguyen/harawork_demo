const background = require('../../../assets/splash.png');
const openDrawer = require('./openDrawer.png');
const banner = require('./banner.png');
const closeDrawer = require('./closeDrawer.png');
const userIcon = require('./userIcon.png');
const logout = require('./logout.png');
const notcheckout = require('./notcheckout.png');
const history = require('./history.png');
const calendar = require('./calendar.png');
const logoLogin = require('./logoLogin.png');
const contact = require('./contact.png');
const star = require('./star.png');
const stared = require('./stared.png');
const headerDefault = require('./headerDefault.png');
const headerStar = require('./headerStar.png');
const headerList = require('./headerList.png');
const no_Image = require('./no_Image.png');
const noImage = require('./noImage.png');
const word = require('./iconWord.png');
const excel = require('./iconExcel.png');
const pdf = require('./sketch.png');
const download = require('./download.png');
const iconWrite = require('./iconWrite.png');
const inbox = require('./inbox.png');
const send = require('./send.png');
const draft = require('./draft.png');
const label = require('./label.png');
const time = require('./time.png');
const tree = require('./tree.png');
const user = require('./user.png');
const searchDeparment = require('./searchDeparment.png');
const searchJobtitle = require('./searchJobtitle.png');
const searchUser = require('./searchUser.png');
const seen = require('./seen.png');
const halfSeen = require('./halfSeen.png');
const notSeen = require('./notSeen.png');
const relay = require('./relay.png');
const transfer = require('./transfer.png');
const bpm = require('./bpm.png');
const group = require('./group.png');
const row = require('./row.png');
const plane = require('./plane.png');
const iconEmployee = require('./employee.png');
const detailLeaves = require('./detailLeaves.png');
const historyLeaves = require('./historyLeaves.png');
const call = require('./call.png');
const errorView = require('./errorView.png')
const frameNone = require('./frame.png');

export default {
    background,
    openDrawer,
    banner,
    closeDrawer,
    userIcon,
    logout,
    notcheckout,
    history,
    calendar,
    logoLogin,
    contact,
    star,
    stared,
    headerDefault,
    headerStar,
    headerList,
    no_Image,
    noImage,
    word,
    excel,
    pdf,
    download,
    iconWrite,
    inbox,
    send,
    draft,
    label,
    time,
    tree,
    user,
    searchDeparment,
    searchJobtitle,
    searchUser,
    seen,
    halfSeen,
    notSeen,
    relay,
    transfer,
    bpm,
    group,
    row,
    plane,
    iconEmployee,
    detailLeaves,
    historyLeaves,
    call,
    errorView,
    frameNone
}