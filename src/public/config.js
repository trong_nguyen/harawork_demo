import { Dimensions, Platform } from 'react-native';
import Constants from 'expo-constants';

const { width, height } = Dimensions.get('window');
const statusBarHeight = Constants.statusBarHeight;
const isIPhoneX = (Platform.OS == 'ios' && statusBarHeight > 20) ? true : false;


const listRouterBPM = [
    {
        name: 'Phiếu cần xử lý',
        first: {
            title: 'Phiếu cần duyệt',
            key: 'formWaitingApprove',
            api: {
                method: 'GET',
                url: 'approval/waiting_approve',
                urlGroup: 'waiting_approve/tab'
            }
        },
        second: {
            title: 'Phiếu cần hoàn tất',
            key: 'formWaitingComplete',
            api: {
                method: 'GET',
                url: 'completion/waiting_complete',
                urlGroup: 'waiting_complete/tab'
            }
        },
        key: 'BPMListTabIndex',
        count: 'totalIndex'
    },
    {
        name: 'Phiếu của tôi',
        first: {
            title: 'Đang chờ xử lý',
            key: 'formMe',
            api: {
                method: 'GET',
                url: 'me/sent',
                urlGroup: 'me/tab'
            }
        },
        second: undefined,
        key: 'BPMListTabFormMe',
        count: 'formMe'
    },
    {
        name: 'Phiếu cần duyệt',
        first: {
            title: 'Đang chờ xử lý',
            key: 'formWaitingApprove',
            api: {
                method: 'GET',
                url: 'approval/waiting_approve',
                urlGroup: 'waiting_approve/tab'
            }
        },
        second: {
            title: 'Đã xử lý',
            key: 'formRejectedOrApproved',
            api: {
                method: 'GET',
                url: 'approval/rejected_or_approved',
                urlGroup: 'rejected_or_approved/tab'
            }
        },
        key: 'BPMListTabFormWaitingApprove',
        count: 'formWaitingApproveTotal'
    },
    {
        name: 'Phiếu cần hoàn tất',
        first: {
            title: 'Đang chờ xử lý',
            key: 'formWaitingComplete',
            api: {
                method: 'GET',
                url: 'completion/waiting_complete',
                urlGroup: 'waiting_complete/tab'
            }
        },
        second: {
            title: 'Đã xử lý',
            key: 'formCompleted',
            api: {
                method: 'GET',
                url: 'completion/completed',
                urlGroup: 'completed/tab'
            }
        },
        key: 'BPMListTabFormWaitingComplete',
        count: 'formWaitingCompleteTotal'
    },
    {
        name: 'Phiếu đang theo dõi',
        first: {
            title: 'Tất cả',
            key: 'formTrack',
            api: {
                method: 'GET',
                url: 'track',
                urlGroup: 'track/tab'
            }
        },
        second: undefined,
        key: 'BPMListTabFormTrack',
        count: 'formTrack'
    }
];

const settingApp = {
    color: '#293a8f',//'#0379C7',
    colorText: '#212121',
    colorDisable: '#979797',
    colorLable: '#9CA7B2',
    blueSky:'#2979FF',
    blackText:'#212121',
    colorButon:'#4D4D4D',
    width,
    height,
    platform: Platform.OS,
    backgroundHeader: '#293a8f', //'#0279C7',
    backgroundAvater: '#F2F4F6',
    colorSperator: '#DAE3EA',
    statusBarHeight,
    isIPhoneX,
    styleTitle: { color: '#ffffff', fontSize: 16, lineHeight: 44, fontWeight: 'bold' },
    shadow: {
        ...Platform.select({
            ios: {
                shadowOffset: { width: 2, height: 2 },
                shadowColor: 'black',
                shadowOpacity: 0.1,
            },
            android: {
                elevation: 1
            }
        })
    },
    listRouterBPM,
    imageSize: (width / 2.20588235294),
    imageFullSize: (width / 1.875)
}

export default settingApp;