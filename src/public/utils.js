import Config from '../Haravan/config';
import moment from 'moment';
import { StackActions, NavigationActions } from 'react-navigation';

export async function permission() { }

export function renderTime(time) {
    if (time) {
        let timeUTC = new Date(time);
        time = moment.tz(new Date(timeUTC), Config.timezone).calendar();
    } else {
        time = '';
    }
    return time;
}

export function formatNumber(value){
    let number = null
    value= value || ''
    number = value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    return number
}

export function getLinkImage(id, token){
    let url = null
    token = token || ''
    url = `https://accounts.${Config.authority}/profile/avatar/${id}&access_token=${token}`
    return url
}

export function renderLongDateTime(time) {
    if (time) {
        let timeUTC = new Date(time);
        time = moment.tz(new Date(timeUTC), Config.timezone).format('LLLL');
        time = time.charAt(0).toUpperCase() + time.substr(1);
        time = time.slice(0, time.lastIndexOf(' '));
    } else {
        time = '';
    }
    return time;
}

export function formatTime(time, type, isSorte) {
    if (time) {
        time = moment.utc(time).local().format(type);
        let checktime = time.split(' ');
        if (!isSorte && checktime && checktime.length > 0) {
            let today = moment().subtract(0, 'days').format('L');
            let yesterday = moment().subtract(1, 'days').format('L');
            let tomorrow = moment().add(1, 'days').format('L');
            if (checktime.indexOf(today) > -1)
                checktime[checktime.indexOf(today)] = 'Hôm nay';
            if (checktime.indexOf(yesterday) > -1)
                checktime[checktime.indexOf(yesterday)] = 'Hôm qua';
            if (checktime.indexOf(tomorrow) > -1)
                checktime[checktime.indexOf(tomorrow)] = 'Ngày mai';
            checktime = checktime.reduce((a, b) => a + ' ' + b);
            time = checktime;
        }
    } else {
        time = '';
    }
    return time;
}

export function formatLocalTime(time, type, isSorte) {
    if (time) {
        time = moment(time).local().format(type);
        let checktime = time.split(' ');
        if (!isSorte && checktime && checktime.length > 0) {
            let today = moment().subtract(0, 'days').format('L');
            let yesterday = moment().subtract(1, 'days').format('L');
            let tomorrow = moment().add(1, 'days').format('L');
            if (checktime.indexOf(today) > -1)
                checktime[checktime.indexOf(today)] = 'Hôm nay';
            if (checktime.indexOf(yesterday) > -1)
                checktime[checktime.indexOf(yesterday)] = 'Hôm qua';
            if (checktime.indexOf(tomorrow) > -1)
                checktime[checktime.indexOf(tomorrow)] = 'Ngày mai';
            checktime = checktime.reduce((a, b) => a + ' ' + b);
            time = checktime;
        }
    } else {
        time = '';
    }
    return time;
}

export function localTime(time) {
    let timeZone = new Date();
    timeZone = timeZone.getTimezoneOffset() * 60000;
    time = new Date(time);
    time = time.setTime(time.getTime() + timeZone);
    time = new Date(time);
    return time;
}

export function checkTypeUrl(url) {
    if (url.match(/\.(jpg|jpeg|png|gif)$/) != null) {
        return 'image';
    } else if (url.match(/\.(doc|docx)$/) != null) {
        return 'word';
    } else if (url.match(/\.(xls|xlsx)$/) != null) {
        return 'excel';
    }else if (url.match(/\.(pdf)$/) != null) {
        return 'pdf';
    }else {
        return 'sketch';
    }
}

export function checkAnnouncementcheck(status) {
    let name = '';
    switch (status) {
        case 1:
            name = 'Đã tạo thông báo';
            break;
        case 2:
            name = 'Đã yêu cầu duyệt thông báo';
            break;
        case 3:
            name = 'Đã huỷ thông báo';
            break;
        case 4:
            name = 'Đã duyệt thông báo';
            break;
        case 5:
            name = 'Đã tạo thông báo nháp';
            break;
        case 6:
            name = 'Đã sửa thông báo';
            break;
        default:
            name = '';
            break;
    }
    return name;
}

export function reload(stateApp) {
    const { getState } = stateApp;
    const { nav } = getState();
    if (!!nav && nav.hasOwnProperty('index') && nav.index === 2) {
        const resetAction = StackActions.reset({
            index: 0,
            key: null,
            actions: [NavigationActions.navigate({ routeName: 'AppContainer' })],
        });
        stateApp.dispatch(resetAction);
    }
}

export function checkRole(role) {
    const AnnouncementTypeCanWriteScopeAuth = [
        'admin',
        'ic_api.admin',
        'ic_api.announcement.admin',
    ];

    if (AnnouncementTypeCanWriteScopeAuth.indexOf(role) > -1) return true;

    return false;
}