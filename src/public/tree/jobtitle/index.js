import Layout from './layout';
import {
    HaravanHr
} from '../../../Haravan';
import { SvgCP } from '../../../public';
import actions from '../../../state/action';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class Jobtitle extends Layout{
    constructor(props){
        super(props)
        this.state = {
            listData: [],
            isLoading:true,
        }
        this.userInfo = props.app.authHaravan.userInfo
    }

    componentDidMount(){
        if(this.listEmploy){
            this.setState({listEmploy:this.listEmploy})
        }
        this.loadData()
    }

    async loadData() {
        const { listData } = this.state;
        const result = await HaravanHr.getJobTructure();
        if (result && result.data && !result.error) {
            result.data.data.map(item => {
                item = {...item, fullName:item.name, typeIC:2, subName:(`Tất cả ${item.name}`)}
                if(listData.length == 0){
                    listData.push(item)
                }
                else{
                    const index = listData.findIndex(m => (m.id === item.id))
                    if(index < 0){
                        listData.push(item)
                    }
                }
            })
            this.setState({
                listData,
                isLoading:false
            })
        }
    }
}
const mapStateToProps = state => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(Jobtitle);