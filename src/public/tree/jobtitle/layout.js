import React, { Component } from 'react';
import { View } from 'react-native';
import ListContent from './component/listContent'
import { Loading, settingApp } from '../../../public';

class Layout extends Component{

    render() {
        const { isLoading } = this.state
        return (
            <View style={{ paddingTop:10, paddingBottom:10, minHeight:(settingApp.height)}}>
                {!isLoading ? <ListContent
                    listData = {this.state.listData}
                /> : <Loading />
                }
            </View>
        )
    }
}
export default Layout;