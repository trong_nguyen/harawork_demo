import React, { Component } from 'react';
import { View, FlatList, Text } from 'react-native';
import Item from './item';
import { Loading, settingApp } from '../../../../public';
import { HaravanHr } from '../../../../Haravan';
import actions from '../../../../state/action';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class ListContent extends Component{
    constructor(props){
        super(props)
        this.state={
            listData:[],
            item: props.item,
            listEmploy:[]
        }

        this.userInfo = props.app.authHaravan.userInfo

        this.page = 1;
        this.totalPage = null;
        this.currentList =[]
    }

    componentDidMount(){
        const { listData } = this.props
        const { item  } = this.state
        if(listData && (listData.length > 0)){
            this.setState({listData:listData})
        }
        else{
            if(item){
                switch (item.typeIC) {
                    case 1:
                        this.loadDataUser(item)
                        break;
                    case 2:
                        this.loadDepart(item)
                        break;
                    default: this.loadJobtitle(item)
                        break;
                }
            }
        }
    }

    checkResult(input){
        if(input && input.data && !input.error){
            return input
        } else{
            return null
        }
    }

    // if((e.count > 0) && e.type && (e.type==2)){
    //     e = {...e, fullName:checkMe ? 'Tôi':e.name, typeIC:4, departmentId:item.id, subName:checkMe? 'Tài khoản của tôi':(e.name+' tại '+item.fullName)}
    //     listData.push(e)
    // }
    // else if( (e.count > 0) && e.type && (e.type==3)){
    //     e = {...e, fullName:e.name, typeIC:2, departmentId:item.id,  subName:(e.name+' tại '+item.fullName)}
    //     listData.push(e)
    // }
    // else if(e.type && (e.type==1)){
    //     e = {...e, fullName:e.name, typeIC:1}
    //     listData.push(e)
    // }
    async loadDepart(item){
        let { listData } = this.state
        if(item && item.departments && item.departments.length > 0){
            item.departments.map(e => {
                const index = listData.findIndex(m => (m.id == e.id))
                if(index < 0){
                    e ={ ...e, typeIC:1, fullName:e.departmentName, subName:(`${e.jobtitleName} tại ${e.departmentName}`)}
                    listData.push(e)
                }
                this.setState({listData})
            })
        }
    }

    async loadJobtitle(item){
        // let {page, totalPage, currentList } = this;
        // let { listData } = this.state;
        // let body =[{ 
        //     departmentId: item.departmentId, 
        //     jobtitleId: item.id
        // }];
        // let newList = listData
        // if(!totalPage || (totalPage && !isNaN(totalPage) && (page < totalPage))){
        //     let result = await HaravanHr.getListCompanystructure(page, body)
        //     result = this.checkResult(result)
        //     if(result && result.data){
        //         let totalcount = result.data.totalCount
        //         result.data.data.map(e =>{
        //             const index = listData.findIndex(m => (m.type == e.type) && (m.id == e.id))
        //             if(index < 0){
        //                 e = {...e, typeIC:4, jobtitle:e.mainPosition.jobtitleName, subName:(e.mainPosition.jobtitleName+' tại '+e.mainPosition.departmentName)}
        //                 newList.push(e)
        //             }
                    
        //         })
        //         this.page = page +1
        //         this.totalPage = Math.ceil(totalcount/20)
        //         this.setState({listData:newList}, () => this.loadMore(item))
        //     }
        // }
    }

    async loadDataUser(item){
        let {page, totalPage, currentList } = this;
        let { listData } = this.state;
        let body =[{ 
            departmentId: item.departmentId, 
            jobtitleId: item.jobtitleId
        }];
        let newList = listData
        if(!totalPage || (totalPage && !isNaN(totalPage) && (page < totalPage))){
            let result = await HaravanHr.getListCompanystructure(page, body)
            result = this.checkResult(result)
            if(result && result.data){
                let totalcount = result.data.totalCount
                result.data.data.map(e =>{
                    const index = listData.findIndex(m => (m.type == e.type) && (m.id == e.id))
                    if(index < 0){
                        e = {...e, typeIC:4, jobtitle:e.mainPosition.jobtitleName, subName:(e.mainPosition.jobtitleName+' tại '+e.mainPosition.departmentName)}
                        newList.push(e)
                    }
                    
                })
                this.page = page +1
                this.totalPage = Math.ceil(totalcount/20)
                this.setState({listData:newList}, () => this.loadMore(item))
            }
        }
    }

    loadMore(item){
        if(this.page <= this.totalPage){
            this.loadJobtitle(item)
        }
    }

    renderItem(obj){
        const { item, index } = obj
        return(
            <Item 
                item={item}
            />
        )
    }
    render(){
        return(
            <View style={{flex:1}}>
                <FlatList
                    scrollEnabled={false}
                    style={{marginBottom:10}}
                    data={this.state.listData}
                    extraData={this.state}
                    keyExtractor={(item, index) => index+''}
                    renderItem={(obj) => this.renderItem(obj)}
                />
            </View>
        )
    }
}
const mapStateToProps = state => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(ListContent);