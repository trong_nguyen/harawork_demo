import React, { Component } from 'react';
import { View, FlatList, Text, TouchableOpacity } from 'react-native';
import { Loading, settingApp, SvgCP, AvatarCustom } from '../../../../public';
import { AntDesign } from '@expo/vector-icons';
import Collapsible from 'react-native-collapsible';
import ListContent from './listContent';
import actions from '../../../../state/action';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class Item extends Component{
    constructor(props){
        super(props)
        this.state={
            item:null,
            isCheck:false,
            isOpen:false,
        }
    }

    componentDidMount(){
        const { item } = this.props
        if(item){
            this.setState({item:item})
        }
    }

    componentWillReceiveProps(nextProps){
        const { item } = this.state
        if(nextProps && nextProps.publicActions && nextProps.publicActions.removeItemUser ){
            let removeItemUser = nextProps.publicActions.removeItemUser;
            if((removeItemUser.typeIC == item.typeIC)  &&
                 (removeItemUser.subName == item.subName)
                 ){
                    if(item.departmentId && (item.departmentId  == removeItemUser.id)){
                        this.setState({isCheck:false},() =>{
                            this.props.actions.publicActions.removeItemUser(null)
                        })
                    }
                    else if(item.id  == removeItemUser.id){
                        this.setState({isCheck:false},() =>{
                            this.props.actions.publicActions.removeItemUser(null)
                        })
                    }
                
            }
        }
    }

    checkIcon(item){
        let icon =  null
        switch (item.typeIC) {
            case 1:
                icon = <SvgCP.icon_Tree check={this.state.isCheck}/>
                break;
            case 2:
                icon = <SvgCP.icon_Jobtitle check={this.state.isCheck}/>
                break;
            case 4:
                icon = <AvatarCustom userInfo={{ name: item.fullName, picture: item.photo ? item.photo : ''}} v={25} fontSize={10} />
                break;
            default: icon = <SvgCP.icon_Home check={this.state.isCheck}/>
                break;
        }
        return icon 
    }

    render(){
        const { item, isOpen, isCheck } = this.state
        if(item){
            let isDisable = (item.typeIC && item.typeIC == 4) 
            let disable = (item.count == 0)
            let icon =  this.checkIcon(item)
            return(
                <View style={{ flex: 1, marginLeft:isDisable ? 50 : 20}}>
                <View
                    style={{
                        flexWrap: 'wrap',
                        alignItems: 'center',
                        flexDirection: 'row',
                        paddingTop: 5
                    }}>
                    {!isDisable && 
                    <TouchableOpacity
                        onPress={() =>{
                            this.setState({ isOpen: !this.state.isOpen })
                            }}
                    >
                        <AntDesign name={isOpen ? 'down':'right'} size={15} color='#9CA7B2' />
                    </TouchableOpacity>}

                    <TouchableOpacity
                        onPress={() => {
                            this.setState({isCheck: !this.state.isCheck},()=>{
                                this.props.actions.publicActions.itemUser(item)
                            })
                            // this.props.addUser(item)
                            }}
                        style={{  alignItems: 'center', backgroundColor:isCheck ? settingApp.blueSky : '#F4F4F4', padding: 10, flexDirection: 'row', height:44, minWidth:(settingApp.width*0.45), maxWidth:(settingApp.width*0.7) }}>
                        {icon }
                        <View style={{ justifyContent: 'center', alignItems: 'center', paddingLeft:5 }}>
                            <Text 
                                numberOfLines={1}
                                style={[styles.text,{color:isCheck ? '#FFFFFF':settingApp.colorText}]}>
                                {item.fullName}
                                {/* {(item.count > 0) && <Text style={[styles.text,{color:isCheck ? '#FFFFFF':settingApp.colorText}]}>{` (${item.count})`}</Text>} */}
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>

                {isOpen &&
                    <ListContent
                        item={item}
                    />
                    }
            </View>
            )
        }
        else{
            return <View/>
        }
    }
}

const styles = {
    text:{
        fontSize:14, 
    }
}
const mapStateToProps = state => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(Item);