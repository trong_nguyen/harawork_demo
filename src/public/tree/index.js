import React, { Component } from 'react';
import { createMaterialTopTabNavigator } from 'react-navigation';
import  {settingApp}  from '../../public';
import Company from './company';
import Jobtitle from './jobtitle'

const Tree = createMaterialTopTabNavigator({
    First: {
        screen:(props) => <Company 
            listEmploy ={props.screenProps.listEmploy}
        />,
        navigationOptions: {
            tabBarLabel: `Theo cây công ty`
        }
    },
    Second: {
        screen:(props) => <Jobtitle 
            listEmploy ={props.screenProps.listEmploy}
        />,
        navigationOptions: {
            tabBarLabel: `Theo chức danh`
        }
    },
}, 
{   
    tabBarOptions: {
        activeTintColor: settingApp.blueSky,
        inactiveTintColor: settingApp.colorDisable,
        labelStyle: {
            marginTop: 1,
            fontSize: 14,
            width: (((settingApp.width -30)) / 2)
        },
        tabStyle: {
            height:54,
            backgroundColor:'transparent',
            width: (((settingApp.width -30)) / 2),
            
        },
        style: {
            borderBottomColor:'#ffffff',
            borderBottomWidth:3,
            width: (settingApp.width -30),
            height: 54,
            backgroundColor: '#ffffff',
            ...settingApp.shadow
        },
        indicatorStyle: {
            borderBottomColor:settingApp.blueSky,
            borderBottomWidth:4,

            borderColor: '#ffffff',
            // borderWidth: 44
        },
        upperCaseLabel: false,
    },
    lazy: false,
    swipeEnabled: true,
}
)
export default Tree;