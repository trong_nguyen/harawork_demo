import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView} from 'react-native';
import ListContent from './component/listContent'
import { Loading, settingApp , SvgCP} from '../../../public';

class Layout extends Component{

    render() {
        const { isLoading, listData, itemInfo, isCheck } = this.state
        return (
            <View 
                style={{paddingTop:10, paddingBottom:10,  flex:1}}>
                {itemInfo && <TouchableOpacity
                    onPress={() => {
                        this.setState({isCheck: !this.state.isCheck},()=>{
                                this.addUser()
                            })
                        }}
                    style={{ marginLeft:20 ,alignItems: 'center', backgroundColor: isCheck ? settingApp.blueSky : '#F4F4F4', padding: 10, flexDirection: 'row', height:44, minWidth:(settingApp.width*0.45), maxWidth:(settingApp.width*0.7) }}>
                    <SvgCP.icon_Home check={this.state.isCheck}/>
                    <View style={{ justifyContent: 'center', alignItems: 'center', paddingLeft:5 }}>
                        <Text 
                            numberOfLines={1}
                            style={{color:isCheck ? '#FFFFFF' : settingApp.colorText, fontSize:14}}>
                            {itemInfo.fullName}
                        </Text>
                    </View>
                </TouchableOpacity>}
                {!isLoading ? <ListContent
                    listData = {this.state.listData}
                /> : <Loading />
                } 
            </View>
        )
    }
}
export default Layout;