import Layout from './layout';
import {
    HaravanHr
} from '../../../Haravan';
import { SvgCP } from '../../../public';
import actions from '../../../state/action';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class Company extends Layout{
    constructor(props){
        super(props)
        this.state = {
            itemInfo:null,
            listData: [],
            isLoading:true,
            isCheck:false,
        }
        this.userInfo = props.app.authHaravan.userInfo
    }

    componentDidMount(){
        this.loadData()
    }

    componentWillReceiveProps(nextProps){
        const { itemInfo } = this.state
        if(nextProps && nextProps.publicActions && nextProps.publicActions.removeItemUser ){
            let removeItemUser = nextProps.publicActions.removeItemUser;
            if((removeItemUser.typeIC == itemInfo.typeIC) && (removeItemUser.id === itemInfo.id)){
                this.setState({isCheck:false},() =>{
                    this.props.actions.publicActions.removeItemUser(null)
                })
            }
        }
    }

    addUser(){
        this.props.actions.publicActions.itemUser(this.state.itemInfo)
    }

    async loadData() {
        const { listData } = this.state;
        const result = await HaravanHr.getCompanysTructure(null, true);
        if (result && result.data && !result.error) {
            result.data.map(item => {
                item = {...item, fullName:item.name, typeIC:1, subName:item.code}
                if(listData.length == 0){
                    listData.push(item)
                }
                else{
                    const index = listData.findIndex(m => (m.id === item.id)&& (m.name === item.name))
                    if(index < 0){
                        listData.push(item)
                    }
                }
            })
            this.setState({
                listData,
                isLoading:false
            }, () => {
                this.setState({
                    itemInfo: { 
                        fullName: this.userInfo.orgname, 
                        subName: 'Tất cả nhân viên', 
                        name: this.userInfo.orgname, 
                        typeIC: 5,
                        orgid:this.userInfo.orgid,
                        id:`${this.userInfo.orgid}${this.userInfo.orgname}`
                    }
                })
            })
        }
    }
}
const mapStateToProps = state => ({ ...state });
const mapDispatchToProps = (dispatch) => {
    let acts = {}
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
}
export default connect(mapStateToProps, mapDispatchToProps)(Company);