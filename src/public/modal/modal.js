<Modal
        easing={Easing.bezier(0.47, 0, 0.745, 0.715)}
        ref={(refs) => { this.modal = refs }}
        style={{
          flex: 1,
          // width: settingApp.width,
          maxHeight: settingApp.height / 3,
          backgroundColor: 'transparent',
          paddingHorizontal: 15
        }}
        position={"bottom"}
        isOpen={false}
        coverScreen={true}
        isDisabled={false}
      >
        <View style={{ maxHeight: settingApp.height / 3, backgroundColor: 'transparent' }}>

          <View style={{
            backgroundColor: 'transparent',
            justifyContent: 'center',
            alignItems: 'center', paddingBottom: 15
          }}>
            <View style={{ backgroundColor: '#fff', height: 2, width: settingApp.width / 9 }} />
          </View>

          {this.renderModalContent()}
        </View>
      </Modal>