import * as HaravanHr from './api/hr';
import * as HaravanIc from './api/ic';
import * as HaravanTask from './api/task';
import * as HaraBPM from './api/bpm';
import * as HaravanAccounts from './api/accounts';
import * as Vote360 from './api/vote360';
import * as HaravanGraphQL from  './api/graphQL';

global._fetch = fetch;
global.fetch = function (uri, options, ...args) {
  //console.log('Actions Fetch: ' + uri)
  return global._fetch(uri, options, ...args).then((response) => {
    //console.log('Response Fetch ' + response.status, uri);
    return response;
  });
};

export {
  HaravanHr,
  HaravanIc,
  HaravanTask,
  HaraBPM,
  HaravanAccounts,
  Vote360,
  HaravanGraphQL
};
