import lang from "../locales";

import * as Localization from 'expo-localization';
import Constants from 'expo-constants'

let deviceId = Constants.deviceId;
let deviceName = Constants.deviceName;

let version = `${Constants.manifest.version}.${Constants.manifest.extra.versionCode}-${Constants.manifest.extra.branch}`;

const Configs = {
    timezone: "Asia/Ho_Chi_Minh",
    deviceid: deviceId,
    device_name: deviceName,
    version: version,
    authority: Constants.manifest.extra.authority,
    apiHost: Constants.manifest.extra.apiHost,
    apiHost360:Constants.manifest.extra.apiHost360,
    clientid: Constants.manifest.extra.clientid,
    secret: Constants.manifest.extra.client_secret,
    scopes: 'offline_access openid profile hac_api.read_users email org userinfo hr_api im_api ef_api ta_api', //cl_api
    sentry: Constants.manifest.extra.sentry_PublicDSN,
    id_GA: Constants.manifest.extra.ID_GA,
    voteFeaturesOrgs:[1, 200000000005],
    // orgTCH = 200000000005
    // orgTest = 1
}
// Expo.DangerZone.Localization.getCurrentTimeZoneAsync().then(rp => {
//     Configs.timezone = rp;
// });

Configs.timezone = Localization.timezone;

export default Configs;