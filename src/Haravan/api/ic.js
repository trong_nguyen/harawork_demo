import Configs from '../config';
import { Notifications } from 'expo';
import HaravanApi from './hrvapi';
import * as Permissions from 'expo-permissions'

const uri = `https://ic.${Configs.apiHost}/api`;

// NotifyCenter
export async function registerForPushNotificationsAsync() {
    const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
    }
    if (finalStatus !== 'granted') {
        return;
    }
    let token = await Notifications.getExpoPushTokenAsync();
    const method = 'POST';
    const body = {
        deviceId: Configs.deviceid,
        token
    }

    HaravanApi.CallApi(`${uri}/notify_center/register/mobile`, method, body);
}

export async function disableForPushNotificationsAsync() {
    const method = 'DELETE';
    const body = {
        deviceId: Configs.deviceid
    }

    HaravanApi.CallApi(`${uri}/notify_center/signout/mobile`, method, body);
}

// announcements
export async function getListNotify(params, type, page, page_size) {

    const method = 'POST';
    const body = {
        ...params
    }

    const result = await HaravanApi.CallApi(`${uri}/announcements${type}?page=${page}&page_size=${page_size}`, method, body);

    return result;
}

export async function statusStar(id, isStarred) {
    const method = 'PUT';
    const result = await HaravanApi.CallApi(`${uri}/announcements/${id}/starred?isStarred=${isStarred}`, method);
    return result;
}

export async function readNotify(id, params, status) {

    const method = 'POST';
    const body = {
        ...params
    }

    const result = await HaravanApi.CallApi(`${uri}/announcements/${id}?isManagerView=${status}`, method, body);
    return result;
}

export async function isReaded(id, params) {

    const method = 'PUT';
    const body = {
        ...params
    }

    const result = await HaravanApi.CallApi(`${uri}/announcements/mark/readed/${id}`, method, body);
    return result;
}

export async function approved(params) {

    const method = 'POST';
    const body = {
        ...params
    }

    const result = await HaravanApi.CallApi(`${uri}/announcements/approved`, method, body);
    return result;
}

export async function reject(params) {

    const method = 'POST';
    const body = {
        ...params
    }

    const result = await HaravanApi.CallApi(`${uri}/announcements/reject`, method, body);

    return result;
}

export async function logs(id) {
    const result = await HaravanApi.CallApi(`${uri}/logs/${id}/1`);
    return result;
}

export async function searchList(params, textSearch, page) {

    const method = 'POST';
    const body = {
        ...params
    }

    const result = await HaravanApi.CallApi(`${uri}/announcements/list/all?text=${textSearch}&page=${page}`, method, body);
    return result;
}

export async function searchManager(params, textSearch, page) {

    const method = 'POST';
    const body = {
        ...params
    }

    const result = await HaravanApi.CallApi(`${uri}/announcements/manager/all/search?status=2&text=${textSearch}&page=${page}`, method, body);
    return result;
}

// label
export async function getLabel(page) {
    const result = await HaravanApi.CallApi(`${uri}/internal_label/list?page=${page}`);
    return result;
}

export async function bulkLabel(params) {
    const method = 'PUT';
    const body = {
        ...params
    }

    const result = await HaravanApi.CallApi(`${uri}/internal_mail/put_label`, method, body);
    return result;
}

export async function addNewLabel(params) {
    const method = 'POST';
    const body = {
        ...params
    }
    const result = await HaravanApi.CallApi(`${uri}/internal_label`, method, body);
    return result;
}


//mail
export async function getListMail(type, page, params, search) {
    const method = 'POST';
    const body = {
        ...params
    }
    search = search || '';

    //
    const result = await HaravanApi.CallApi(`${uri}/internal_mail/${type}query=${search}&page=${page}`, method, body);
    return result;
}

export async function sendMail(params, user) {
    const method = 'POST';
    const body = {
        ...params
    }
    const result = await HaravanApi.CallApi(`${uri}/mail?createUserName=${user}`, method, body);

    return result;
}

export async function getMailDetail(id) {

    const result = await HaravanApi.CallApi(`${uri}/internal_mail/${id}`);

    return result;
}

export async function getMessageMail(id, userInfo) {
    const method = 'POST';
    const body = {
        query: null,
        userInfo: { ...userInfo }
    }
    const result = await HaravanApi.CallApi(`${uri}/messages/mail/${id}`, method, body);
    return result;
}

export async function mailRead(id, userInfo) {
    const method = 'PUT';
    const body = {
        ...userInfo
    }

    const result = await HaravanApi.CallApi(`${uri}/mail/read/${id}`, method, body);
    return result;
}

export async function removeMail(params) {
    const method = 'PUT';

    const body = params;

    const result = await HaravanApi.CallApi(`${uri}/internal_mail/put_remove`, method, body);
    return result;
}

export async function undoMail(id) {
    const method = 'PUT';

    const result = await HaravanApi.CallApi(`${uri}/internal_mail/${id}/undo`, method);

    return result;
}

export async function sentMailDraft(params, draft) {
    const method = 'POST';
    const body = {
        ...params
    }
    const result = await HaravanApi.CallApi(`${uri}/internal_mail?isPostDraft=${draft}`, method, body);
    return result;
}
export async function sentMailCreate(params, draft) {
    const method = 'PUT';
    const body = {
        ...params
    }
    const result = await HaravanApi.CallApi(`${uri}/internal_mail?isPutDraft=${draft}`, method, body);
    return result;
}

//bulk Mail && bulk stared
export async function bulkMail(params) {
    const method = 'PUT';

    const body = params ;
    const result = await HaravanApi.CallApi(`${uri}/internal_mail/put_remove`, method, body);
    return result;
}

export async function bulkStarred(params, isStarred) {
    const method = 'PUT';

    const body = {
        ...params
    };
    const result = await HaravanApi.CallApi(`${uri}/mails/bulk/starred/${isStarred}`, method, body, isStarred);
    return result;
}

//stared
export async function statusStarMail(isStarred, params) {
    const method = 'PUT';
    const body = params;
    
    const result = await HaravanApi.CallApi(`${uri}/internal_mail/put_starred?isStarred=${isStarred}`, method ,body);
    return result;
}

//notify_center
export async function LastSeen() {
    const result = await HaravanApi.CallApi(`${uri}/notify_center/notifications/count/last_seen`);
    return result;
}

//thread
export async function list_pinhome(page, page_size) {
    const result = await HaravanApi.CallApi(`${uri}/thread_forum/news?page=${page}&page_size=${page_size}`);
    return result;
}

export async function threadDetail(id, params) {
    method ='POST';
    const body = {
        ...params
    }
    const result = await HaravanApi.CallApi(`${uri}/thread_forum/${id}`, method, body);
    return result;
}

// reply
export async function PostreplyMailDraft(params, draft) {
    const method = 'POST';
    const body = {
        ...params
    };
    const result = await HaravanApi.CallApi(`${uri}/mail_message?isPostDraft=${draft}`, method ,body);
    return result;
}

export async function PutreplyMailDraft(params, draft) {
    const method = 'PUT';
    const body = {
        ...params
    };
    const result = await HaravanApi.CallApi(`${uri}/mail_message?isPutDraft=${draft}`, method ,body);
    return result;
}

export async function replyMail(params, draft) {
    const method = 'PUT';
    const body = {
        ...params
    };
    const result = await HaravanApi.CallApi(`${uri}/mail_message?isPutDraft=${draft}`, method ,body);
    return result;
}

//put start message
export async function putStartMessage(id, status) {
    const method = 'PUT';
    const result = await HaravanApi.CallApi(`${uri}/mail_message/${id}/starred?isStarred=${status}`, method);
    return result;
}

// delete message
export async function deletaMessage(status, id, mailID, params) {
    const method = 'PUT';
    const body = params;
    const result = await HaravanApi.CallApi(`${uri}/mail_message/${id}/remove?mailId=${mailID}&isDraft=${status}`, method ,body);
    return result;
}

// postImage
export async function postImage(params) {
    const body = params;
    const result = await HaravanApi.PostFormAPI(`${uri}/file/upload_multi`,body);
    return result;
}

//detailSeen
export async function GetUser(id, page, search) {
    search = search || '';
    const result = await HaravanApi.CallApi(`${uri}/mail_message/${id}/get_user?page=${page}&query=${search}`);
    return result;
}

export async function CountUser(id) {
    const result = await HaravanApi.CallApi(`${uri}/mail_message/${id}/count_user`);
    return result;
}

//get Fiel 
export async function GetFile(id, idAttach) {
    const result = await HaravanApi.GetFileCallApi(`${uri}/announcements/${id}/attachment?attachmentId=${idAttach}`);
    return result;
}

//discuss-news-topic https://ic.hara.vn/api/threads/list
export async function GetListTopic(page, search) {
    const method = "POST"
    search = search || '';
    const result = await HaravanApi.CallApi(`${uri}/threads/list?page=${page}&text=${search}`, method);
    return result;
}

export async function GetListNews(page) {
    const result = await HaravanApi.CallApi(`${uri}/thread_forum/news?page=${page}&page_size=10`);
    return result;
}

export async function PutActions(params) {
    const {id, type, action} = params;
    const method = "PUT"
    const result = await HaravanApi.CallApi(`${uri}/thread_forum/${id}/action?type=${type}&isAction=${action}`, method);
    return result;
}

export async function PutNewsComment(id, params) {
    const method = "PUT"
    const body={...params}
    const result = await HaravanApi.CallApi(`${uri}/thread/${id}/comment/update`, method, body);
    return result;
}
export async function updatrTheard(id, params) {
    const method = "PUT"
    const body={...params}
    const result = await HaravanApi.CallApi(`${uri}/thread_forum/${id}/update_thread`, method, body);
    return result;
}
// get List_Featured
export async function GetListFeatured(params) {
    const method = "POST"
    const body={...params}
    const result = await HaravanApi.CallApi(`${uri}/group_forum/featured_group`, method, body);
    return result;
}

export async function GetListGroup(url,params, page) {
    const method = "POST"
    const body={...params}
    const result = await HaravanApi.CallApi(`${uri}/group_forum/${url}?page=${page}&pageSize=10`, method, body);
    return result;
}

export async function GetListThread(idGroup, url, page, search) {
    search = search || ''
    const result = await HaravanApi.CallApi(`${uri}/thread_forum/${idGroup}/${url}?page=${page}&pageSize=20&query=${search}`);
    return result;
}

export async function GetListMember(idGroup, page, search) {
    search = search || ''
    const result = await HaravanApi.CallApi(`${uri}/group_forum/${idGroup}/members?page=${page}&pageSize=20&query=${search}`);
    return result;
}

export async function CreateGroup(params) {
    const metthod = "POST"
    const body = {...params}
    const result = await HaravanApi.CallApi(`${uri}/group_forum`,metthod ,body);
    return result;
}

export async function JoinGroup(id , join ,params) {
    const metthod = "PUT"
    const body = {...params}
    const result = await HaravanApi.CallApi(`${uri}/group_forum/${id}/join?isJoin=${join}`,metthod ,body);
    return result;
}

export async function PinGroup(id, pin) {
    const metthod = "PUT"
    const result = await HaravanApi.CallApi(`${uri}/group_forum/${id}/pin?isPin=${pin}`,metthod);
    return result;
}

export async function RemoveGroup(id, reason, params) {
    const metthod = "PUT"
    const body = {...params}
    const result = await HaravanApi.CallApi(`${uri}/group_forum/${id}/delete?reason=${reason}`,metthod, body);
    return result;
}

export async function RemoveUser(idGroup, idUser, params) {
    const metthod = "PUT"
    const body = {...params}
    const result = await HaravanApi.CallApi(`${uri}/group_forum/${idGroup}/remove_member?memberId=${idUser}`,metthod, body);
    return result;
}

export async function RoleMember(idGroup, idUser, isAdmin, params) {
    const metthod = "PUT"
    const body = {...params}
    const result = await HaravanApi.CallApi(`${uri}/group_forum/${idGroup}/role?memberId=${idUser}&isAdmin=${isAdmin}`,metthod, body);
    return result;
}

export async function EditGroups(idGroup, params) {
    const metthod = "PUT"
    const body = {...params}
    const result = await HaravanApi.CallApi(`${uri}/group_forum/${idGroup}`,metthod, body);
    return result;
}

export async function GetDetailGroups(idGroup, params) {
    const metthod = "POST"
    const body = {...params}
    const result = await HaravanApi.CallApi(`${uri}/group_forum/${idGroup}`,metthod, body);
    return result;
}

// Topic
export async function GetListTopicCare(type,isNews, params, page) {
    const metthod = "POST"
    const body = {...params}
    const result = await HaravanApi.CallApi(`${uri}/thread_forum/list/${type}?isNew=${isNews}&page=${page}&pageSize=20`,metthod, body);
    return result;
}

export async function GetListInteresting( params) {
    const metthod = "POST"
    const body = {...params}
    const result = await HaravanApi.CallApi(`${uri}/thread_forum/list_interesting`,metthod, body);
    return result;
}

export async function GetListCount(type, params) {
    const metthod = "POST"
    const body = {...params}
    const result = await HaravanApi.CallApi(`${uri}/thread_forum/list_count?type=${type}`,metthod, body);
    return result;
}

export async function SearchTopic( params, search, page) {
    search = search || ''
    const metthod = "POST"
    const body = {...params}
    const result = await HaravanApi.CallApi(`${uri}/thread_forum/list/search?page=${page}&pageSize=20&query=${search}`,metthod, body);
    return result;
}

export async function addMember( idGroups, params ) {
    const metthod = "POST"
    const body = params
    const result = await HaravanApi.CallApi(`${uri}/group_forum/${idGroups}/add_members`,metthod, body);
    return result;
}
//
export async function SearchGroups( search, params , page) {
    search = search || ''
    const metthod = "POST"
    const body = {...params}
    const result = await HaravanApi.CallApi(`${uri}/group_forum/list_search?query=${search}&page=${page}&pageSize=20`,metthod, body);
    return result;
}

export async function CreateThread( params ) {
    const metthod = "POST"
    const body = {...params}
    const result = await HaravanApi.CallApi(`${uri}/thread_forum`,metthod, body);
    return result;
}

export async function EditThread( params, idThread ) {
    const metthod = "PUT"
    const body = {...params}
    const result = await HaravanApi.CallApi(`${uri}/thread_forum/${idThread}`,metthod, body);
    return result;
}

export async function isStarredThread( idThread, isStarred, params ) {
    const metthod = "PUT"
    const body = {...params}
    const result = await HaravanApi.CallApi(`${uri}/thread_forum/${idThread}/starred?isStarred=${isStarred}`,metthod, body);
    return result;
}

export async function DeleteThread( idThread) {
    const metthod = "DELETE"
    const result = await HaravanApi.CallApi(`${uri}/thread_forum/${idThread}`,metthod);
    return result;
}

export async function SetPinHome(id, params) {
    const metthod = "PUT"
    const body = {...params}
    const result = await HaravanApi.CallApi(`${uri}/thread_forum/${id}/pin_home`,metthod, body);
    return result;
}

export async function topUserComment() {
    const result = await HaravanApi.CallApi(`${uri}/thread_forum/top_user`);
    return result;
}

export async function getThreadDetail(id, params) {
    const metthod = "POST"
    const body = {...params}
    const result = await HaravanApi.CallApi(`${uri}/thread_forum/${id}`, metthod, body);
    return result;
}

export async function votePoll(id, params) {
    const metthod = "PUT"
    const result = await HaravanApi.CallApi(`${uri}/thread_poll/${id}/vote_option`, metthod, params);
    return result;
}

export async function removePoll(id) {
    const metthod = "PUT"
    const result = await HaravanApi.CallApi(`${uri}/thread_poll/${id}/remove_vote`, metthod);
    return result;
}