import Configs from '../config';
import HaravanApi from './hrvapi';

const uri = `https://eform.${Configs.apiHost}/api`;

export async function getListCount() {
    const method = 'POST';
    const body = {}
    const result = await HaravanApi.CallApi(`${uri}/notificationreaded/form`, method, body);
    return result;
}

export async function getList(api, page) {
    let { url } = api;
    const result = await HaravanApi.CallApi(`${uri}/forms/${url}/?page=${page}`);
    return result;
}

export async function getGroup(api) {
    let { urlGroup } = api;
    const result = await HaravanApi.CallApi(`${uri}/flows/${urlGroup}?queryForm=`);
    return result;
}

export async function getDetailGroup(id, api, page) {
    let { url } = api;
    const result = await HaravanApi.CallApi(`${uri}/forms/${url}?formBuilderId=${id}&query=&page=${page}`);
    return result;
}

export async function getSetting() {
    const result = await HaravanApi.CallApi(`${uri}/approval_setting/common`);
    return result;
}

export async function getTimeLine(formId) {
    const result = await HaravanApi.CallApi(`${uri}/timeline/forms/${formId}`);
    return result;
}

export async function postComment(formId, content) {
    const method = 'POST';
    const body = {
        content,
        attachments: []
    }
    const result = await HaravanApi.CallApi(`${uri}/timeline/postcomment/${formId}`, method, body);
    return result;
}

export async function getDetailForm(user, id) {
    const method = 'POST';
    const body = {
        ...user
    }
    const result = await HaravanApi.CallApi(`${uri}/forms/${id}`, method, body);
    return result;
}

export async function readDetail(id) {
    const method = 'PUT';
    const body = {}
    const result = await HaravanApi.CallApi(`${uri}/forms/read/${id}`, method, body);
    return result;
}

export async function getDetailSection(formId) {
    const result = await HaravanApi.CallApi(`${uri}/mobile/forms/${formId}`);
    return result;
}