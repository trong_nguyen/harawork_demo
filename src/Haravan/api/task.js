import Configs from '../config';
import HaravanApi from './hrvapi';

import ApolloClient from "apollo-boost";
import gql from "graphql-tag";

const uri = `https://tasks.${Configs.apiHost}/api/graphql`;

function getClient() {
    return HaravanApi.GetGraphQl(uri);
}

export async function getTaks() {
    const client = await getClient();
    const reslut = await client.query({
        query: gql`{ 
             taskcountbydates(todate:"2018-08-07T17:00:00.000Z", conditions:{name:""})
            {
                type,
                name,
                count
            }
        }`
    });

    return reslut;
}
