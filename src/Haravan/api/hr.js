import Configs from '../config';
import HaravanApi from './hrvapi';

const uri = `https://hr.${Configs.apiHost}/api`;

// Shift
export async function getShifts() {
    const result = await HaravanApi.CallApi(`${uri}/shifts?isGetAll=true`);
    return result;
}

export async function getUserShifts() {
    const result = await HaravanApi.CallApi(`${uri}/shifts?myShiftOnly=true&isGetAll=true`);
    return result;
}

export async function getShiftsCheckIn(id) {
    const result = await HaravanApi.CallApi(`${uri}/me/getshifscheckin?departmentId=${id}`);
    return result;
}

// Department
export async function getDepartments() {
    const result = await HaravanApi.CallApi(`${uri}/department/all?isGetAll=true`);
    return result;
}

export async function getDepartmentsCheckin() {
    const result = await HaravanApi.CallApi(`${uri}/me/getdepartmentshiftditribution?isGetAll=true`);
    return result;
}

export async function getDepartmentsById(id) {
    const result = await HaravanApi.CallApi(`${uri}/department/${id}`);
    return result;
}

export async function searchDepartments(query) {
    const result = await HaravanApi.CallApi(`${uri}/departments?isCount=true&page_size=8&query=${query}`);
    return result;
}

export async function getAllDepartments(page) {
    const result = await HaravanApi.CallApi(`${uri}/departments?page=${page}&query=`);
    return result;
}


// Me
export async function getMeDitribute() {
    const result = await HaravanApi.CallApi(`${uri}/me/isDitribute`);
    return result;
}

export async function getCheckInStatus() {
    const result = await HaravanApi.CallApi(`${uri}/me/checkin/status`);
    return result;
}

export async function getMeShiftDis() {
    const result = await HaravanApi.CallApi(`${uri}/me/getshiftditribution`);
    return result;
}

export async function postCheckIn(body, isCheckout) {
    const result = await HaravanApi.CallApi(`${uri}/me/checkin?isCheckout=${isCheckout}`, 'POST', body);
    //console.log('result', result)
    return result;
}

export async function getCheckinHistory(url, params) {
    const result = await HaravanApi.CallApi(`${uri}/me/${url}&${params}`);
    return result;
}

export async function detailLeave() {
    const result = await HaravanApi.CallApi(`${uri}/me/leave/detail`);
    return result;
}

export async function meLeaveDetail(id) {
    const result = await HaravanApi.CallApi(`${uri}/me/leave/${id}`);
    return result;
}

export async function getconfigtimeworkinginfo() {
    const result = await HaravanApi.CallApi(`${uri}/me/getconfigtimeworkinginfo`);
    return result;
}

export async function userApproved() {
    const result = await HaravanApi.CallApi(`${uri}/me/directapprover?skip=0`);
    return result;
}

export async function createLeave(params, isSplitLeave) {
    const method = 'POST';
    const body = { ...params };
    const result = await HaravanApi.CallApi(`${uri}/me/leave/create?isSplitLeave=${isSplitLeave}`, method, body);
    return result;
}

export async function updateLeave(id, params, isSplitLeave) {
    const method = 'PUT';
    const body = { ...params };
    const result = await HaravanApi.CallApi(`${uri}/me/leave/update/${id}?isSplitLeave=${isSplitLeave}`, method, body);
    return result;
}

export async function gettotaltimeworking(params) {
    const result = await HaravanApi.CallApi(`${uri}/me/gettotaltimeworking?${params}`);
    return result;
}

export async function getshiftleave() {
    const result = await HaravanApi.CallApi(`${uri}/me/getshiftleave`);
    return result;
}

export async function gettimeworking(fromDate, toDate) {
    const result = await HaravanApi.CallApi(`${uri}/me/gettimeworking?fromDate=${fromDate}&toDate=${toDate}`);
    return result;
}

// colleague
export async function getColleague(params) {
    const result = await HaravanApi.CallApi(`${uri}/colleague?haraId=${params}`);
    return result;
}

export async function getColleagueByEmployeeId(id) {
    const result = await HaravanApi.CallApi(`${uri}/colleague?employeeId=${id}`);
    return result;
}

export async function searchColleague(query) {
    const result = await HaravanApi.CallApi(`${uri}/colleague/search?page_size=8&query=${query}&isActive=true`);
    return result;
}

export async function searchAllColleague(query) {
    const result = await HaravanApi.CallApi(`${uri}/colleague/search?query=${query}`);
    return result;
}

export async function getactualeave(timeleave, employeeId, fromDate, toDate, shiftId) {
    const result = await HaravanApi.CallApi(`${uri}/colleague/getactualeave?timeleave=${timeleave}&employeeId=${employeeId}&fromDate=${fromDate}&toDate=${toDate}&shiftId=${shiftId}`);
    return result;
}

export async function birthday(minNum, fromDate, toDate) {
    const result = await HaravanApi.CallApi(`${uri}/colleague/birthday?minNum=${minNum}&fromDate=${fromDate}&toDate=${toDate}`);
    return result;
}
//list birthday Home Page
export async function listBirthday(page) {
    const result = await HaravanApi.CallApi(`${uri}/colleague/birthday/paging?page=${page}`);
    return result;
}

// jobtitle
export async function getJobtitleById(id) {
    const result = await HaravanApi.CallApi(`${uri}/jobtitle/${id}`);
    return result;
}

export async function searchJobtitle(query) {
    const result = await HaravanApi.CallApi(`${uri}/jobtitles?page_size=8&query=${query}`);
    return result;
}

//companystructure
export async function getCompanysTructure(id,status) {
    const result = await HaravanApi.CallApi(`${uri}/companystructure/${id}?isGetJobtitle=${status}`);
    return result;
}

export async function getJobTructure() {
    const result = await HaravanApi.CallApi(`${uri}/jobtitles?isGetAll=true&isCount=true`);
    return result;
}


export async function getListCompanystructure(page, params) {
    const method = 'POST';
    const body = params ;
    const result = await HaravanApi.CallApi(`${uri}/companystructure/listemp?isCountActing=true&page=${page}&page_size=20`, method, body);
    return result;
}

//leave
export async function leaveType() {
    const result = await HaravanApi.CallApi(`${uri}/leavetypes`);
    return result;
}

export async function leaveHistory(url, params) {
    const result = await HaravanApi.CallApi(`${uri}/me/${url}&${params}`);
    return result;
}

export async function removeLeave(id) {
    const method = 'DELETE';
    const result = await HaravanApi.CallApi(`${uri}/me/leave/remove/${id}`, method);
    return result;
}

export async function suggestRemoveLeave(params) {
    const method = 'POST';
    const body = { ...params };
    const result = await HaravanApi.CallApi(`${uri}/EmployeeRequest`, method, body);
    return result;
}

export async function getListEmployeeRequest(params) {
    const { type, status, requestApproveDate, requesterId, requestDepartmentId,requestShiftId } = params;
    const result = await HaravanApi.CallApi(`${uri}/employeerequest/getbyfilter?type=${type}&status=${status}&requestApproveDate=${requestApproveDate}&requesterId=${requesterId}&requestDepartmentId=${requestDepartmentId}&requestShiftId=${requestShiftId}`);
    return result;
}

export async function getDirectapprover() {
    const result = await HaravanApi.CallApi(`${uri}/me/directapprover?skip=0`);
    return result;
}

//supervisor/leaves
export async function getSupervisorLeaves(url, page) {
    const result = await HaravanApi.CallApi(`${uri}/${url}&page=${page}&page_size=20`);
    return result;
}

export async function getLeavesDeatail(id) {
    const result = await HaravanApi.CallApi(`${uri}/supervisor/approveleavedetail/${id}`);
    return result;
}

export async function getLeavesReject(id, note) {
    const result = await HaravanApi.CallApi(`${uri}/supervisor/leave/reject?id=${id}&approveNote=${note}`);
    return result;
}

export async function getLeavesApprove(id, note) {
    const result = await HaravanApi.CallApi(`${uri}/supervisor/leave/approve?id=${id}&approveNote=${note}`);
    return result;
}

export async function getLeavesTypes() {
    const result = await HaravanApi.CallApi(`${uri}/leaveTypes`);
    return result;
}

export async function putEmployeeRequest(params) {
    const method = 'PUT';
    const body = {
        ...params
    }
    const result = await HaravanApi.CallApi(`${uri}/supervisor/approveemployeerequest`, method, body);
    return result;
}

//approdvedWorks
export async function getListFramesWorks(empolyID, idDepartment, idJod, monDay, sunDay) {
    const result = await HaravanApi.CallApi(`${uri}/supervisor/approve/list?employeeId=${empolyID}&departmentId=${idDepartment}&jobtitleId=${idJod}&fromDate=${monDay}&toDate=${sunDay}&isGetAll=true&ischeckin=true`);
    return result;
}

export async function getRequetsLeave( idDepartment, monDay, sunDay) {
    const result = await HaravanApi.CallApi(`${uri}/supervisor/employeerequest/getbyfilter?fromDate=${monDay}&toDate=${sunDay}&type=1&status=1&requestDepartmentId=${idDepartment}`);
    return result;
}

export async function getJob() {
    const result = await HaravanApi.CallApi(`${uri}/jobtitles?isGetAll=true`);
    return result;
}

export async function getEmploy() {
    const result = await HaravanApi.CallApi(`${uri}/supervisor/approve/list?isGetAll=true`);
    return result;
}

export async function getApprovedDetail(id) {
    const result = await HaravanApi.CallApi(`${uri}/employeeTimekeepingapprove/${id}`);
    return result;
}

export async function postApprovedWorks(link, params, useinfo) {
    const method = 'POST';
    const body = {
        ...params
    }
    const result = await HaravanApi.CallApi(`${uri}/${link}`, method, body, useinfo);
    return result;
}

export async function postCreateWorks(params, type) {
    const method = 'POST';
    const body = {
        ...params
    }
    const result = await HaravanApi.CallApi(`${uri}/supervisor/createtimekeeping?typeTimeApprove=${type}`, method, body);
    return result;
}

export async function getMeListDepartments() {
    const result = await HaravanApi.CallApi(`${uri}/supervisor/departments`);
    return result;
}

export async function getIpDepartment(id) {
    const result = await HaravanApi.CallApi(`${uri}/departmentIp/getbydepartmentid?departmentId=${id}`);
    return result;
}

//registWorks
export async function getMeShift(idDepart) {
    const result = await HaravanApi.CallApi(`${uri}/me/shifts?departmentId=${idDepart}`);
    return result;
}

export async function getShiftdistributions(idDepart, idJod, idEmploy) {
    const result = await HaravanApi.CallApi(`${uri}/supervisor/shiftdistributions?departmentId=${idDepart}&jobtitleId=${idJod}&employeeId=${idEmploy}`);
    return result;
}

export async function registUpdateWorks(id, params) {
    const method = 'PUT';
    const body = {
        ...params
    }
    const result = await HaravanApi.CallApi(`${uri}/me/shiftDistribution/update/${id}`, method, body);
    return result;
}

export async function registCreateWorks(params) {
    const method = 'POST';
    const body = {
        ...params
    }
    const result = await HaravanApi.CallApi(`${uri}/employeeShiftDistribution`, method, body);
    return result;
}

export async function getMeshiftdistributions(from, to, idDepart, idJob, idEmploy) {
    const result = await HaravanApi.CallApi(`${uri}/supervisor/shiftdistributions?fromDate=${from}&toDate=${to}&departmentId=${idDepart}&jobtitleId=${idJob}&employeeId=${idEmploy}&isGetAll=true`);
    return result;
}

//supervisor
export async function hrApproved() {
    const result = await HaravanApi.CallApi(`${uri}/supervisor/directapproverhr?ishr=true`);
    return result;
}

export async function checkPermissionUser() {
    const result = await HaravanApi.CallApi(`${uri}/supervisor/departments?isGetAll=true`);
    return result;
}

//salarySettings
export async function checkLockSalary(fromDate) {
    const result = await HaravanApi.CallApi(`${uri}/me/islocksalary?fromDate=${fromDate}`);
    return result;
}


// checkIP
export async function getisIp(ip ,depart) {
    const result = await HaravanApi.CallApi(`${uri}/departmentip/isip?IpAddress=${ip}&departmentId=${depart}`);
    return result;
}

export async function getDepartmantID(id) {
    const result = await HaravanApi.CallApi(`${uri}/department/${id}?isCount=true`);
    return result;
}

export async function getTotalEmploy(search) {
    search = search || ''
    const result = await HaravanApi.CallApi(`${uri}/colleague/search?statusActive=1&page_size=10&query=${search}`);
    return result;
}

//contact
export async function getListContact(page, statusActive, search, departmentId, jobtitlesId ) {
    search =  search || '';
    departmentId = departmentId || '';
    jobtitlesId = jobtitlesId || '';
    const result = await HaravanApi.CallApi(`${uri}/colleague/search?statusActive=${statusActive}&page=${page}&query=${search}&departmentId=${departmentId}&jobtitlesId=${jobtitlesId}`);
    return result;
}
export async function Directapprover( employID) {
    const result = await HaravanApi.CallApi(`${uri}/supervisor/directapprover?employeeId=${employID}`);
    return result;
}

export async function getPeriodsAll( ) {
    const result = await HaravanApi.CallApi(`${uri}/salaryPeriods/getall`);
    return result;
}

export async function getSalaryFormulas( page, type ) {
    const result = await HaravanApi.CallApi(`${uri}/salaryFormulas?page=${page}&page_size=100&&type=${type}`);
    return result;
}

export async function getSalaryFormulagroups( page ) {
    const result = await HaravanApi.CallApi(`${uri}/salaryFormulagroups?page=${page}&page_size=100&query=&`);
    return result;
}

export async function getMeSalary( perID ) {
    const result = await HaravanApi.CallApi(`${uri}/me/salaryemployees?salaryPeriodId=${perID}`);
    return result;
}

export async function getDetailSalary( perID, employID, formulaId, page ) {
    const result = await HaravanApi.CallApi(`${uri}/salaryemployees/detail?periodId=${perID}&employeeId=${employID}&formulaId=${formulaId}&page_size=100&page=${page}`);
    return result;
}

export async function getSalaryPerDay( perID, employID, formulaId, date ) {
    const result = await HaravanApi.CallApi(`${uri}/salaryemployees/detailbyday?periodId=${perID}&employeeId=${employID}&formulaId=${formulaId}&salaryDay=${date}`);
    return result;
}