import  Constants from 'expo-constants';
import React, { Component } from 'react';
import * as WebBrowser from 'expo-web-browser'
import Configs from '../config';
import { AsyncStorage, Alert } from 'react-native';
import {
    ApolloClient,
    ApolloLink,
    ApolloError,
    HttpLink,
    InMemoryCache,
  } from 'apollo-boost'
import AsyncLock from 'async-lock';
import { Updates } from 'expo';
import Sentry from 'sentry-expo';
import * as AppAuth from 'expo-app-auth';

const { authority, clientid, secret, scopes } = Configs;

const AuthStorageKey = 'AUTH_HARAVAN';
let _authInfo =null;
let _authInfoLoaded = false;
const locker = new AsyncLock();

async function RegisterAuth(refreshToken) {
    // let redirectUrl = Constants.linkingUri;
    // let response_type = "code";
    // let url = `https://accounts.${authority}/connect/authorize?response_type=${encodeURIComponent(response_type)}&scope=${encodeURIComponent(scopes)}` +
    //     `&client_id=${clientid}` +
    //     `&register=true&alwayslogin=true` +
    //     `&redirect_uri=${encodeURIComponent(redirectUrl)}` +
    //     `&prompt=login`;
    // await WebBrowser.openBrowserAsync(url)
    const authConfig = {
        issuer: `https://accounts.${authority}`,
        clientId: clientid,
        grant_type:"refresh_token",
        clientSecret: secret,
        refresh_token:refreshToken,
        scopes: ['offline_access', 'openid', 'profile', 'hac_api.read_users', 'email', 'org', 'userinfo', 'hr_api', 'im_api', 'ef_api', 'ta_api'],
        //redirectUrl: Constants.linkingUri,
        additionalParameters: {
            prompt: 'login',
            hideRegister:'true',
            // response_type:encodeURIComponent(response_type)
            // alwayslogin: 'true'
        }
    };
        const authState = await AppAuth.refreshAsync(authConfig, refreshToken);
        const userInfo = await getUserInfo(authState.accessToken);
        if (userInfo && userInfo.email && userInfo.sub) {
            const infoLogin = {
                auth: { ...authState, access_token: authState.accessToken },
                userInfo: { ...userInfo }
            };
    
            await SetAuthInfo(infoLogin);
            return infoLogin;
        } else {
            return null
        }
}

async function GetAuthInfo() {
    if(!_authInfoLoaded) {
        _authInfoLoaded = true;
        let data = await AsyncStorage.getItem(AuthStorageKey);
        if (data && data.length > 0)
        _authInfo = JSON.parse(data);
    }
    return _authInfo;
}

async function SetAuthInfo(authInfo) {
    _authInfo = authInfo;
    await AsyncStorage.setItem(AuthStorageKey, JSON.stringify(authInfo));
}

async function ClearAuthInfo() {
    try {
        await AppAuth.revokeAsync(authConfig, {
            token: accessToken,
            isClientIdProvided: true,
        });
        await ClearAuthInfo;
        return null;
    } catch ({ message }) { }
    _authInfo = null;
    await AsyncStorage.removeItem(AuthStorageKey);
    Updates.reload();
}

async function __getAccessToken() {
    let authInfo = await GetAuthInfo();
    if (authInfo == null)
        return null;
    if(!authInfo.auth.created_time) {
        console.log("try refresh token if not exist created_time");
        authInfo = await renewToken(authInfo);
    } else {
        let expiredDate = new Date(authInfo.auth.created_time + (authInfo.auth.expires_in * 1000 / 2));
        if (expiredDate <= new Date()) {
            console.log("try refresh token");
            authInfo = await renewToken(authInfo);
           
        }
    }
    return authInfo;
}

async function GetAccessToken() {
    return await locker.acquire("GetAccessToken", () => {
        return __getAccessToken();
    });
}

async function CallApi360(url ,token, method, body ){
    let header = null
    if(!method)
    method="GET";

    if (!body){
        header = {
            method: method,
            credentials: 'include',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'AccessToken': `${token}`
            }        
        }
    }else{
        header = {
            method: method,
            credentials: 'include',
            body: body ? JSON.stringify(body) : null,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'AccessToken': `${token}`
            }        
        }   
    }
    let response = await fetch(url,
            header
        );
        console.log('------------------------------------');
        console.log(response.status, response.url);
        console.log('------------------------------------');
        return await response.json();;
          
}
async function getToken360(url, method, body) {
    //let url = `https://accounts.${authority}/connect/token`;
    if (!method)
    method = "GET";
    let result = await fetch(url, {
        method: method,
        body: body ? JSON.stringify(body) : null,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            // 'AccessToken': `${authInfo.auth.id_token}`
        }  
    });
    console.log('------------------------------------');
    console.log(result.status, result.url);
    console.log('------------------------------------');

    if (result.status == 200) {
        let json = await result.json();
        return json;
    }
    else{
        Sentry.captureMessage('Get Tokent 360 Error: ' + result.url + ' - ' +result.status)
        return null
    }
}

async function CallApiComment(url, method, body) {
    let authInfo = await GetAccessToken();
    if(authInfo == null) {
        await ClearAuthInfo();
        return;
    }
    let response = await fetch(url, {
        method: "POST",
        body: JSON.stringify(body) ,
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json',
            'authorization': `Bearer ${authInfo.auth.access_token}`
        }
    }).then((result) => {
        console.log('result success');
    }).catch((err) => {
        console.log('err',err);
    });;

    if (response.status == 401 || response.status == 403) {
        await ClearAuthInfo();
        return;
    } else if (response.status === 500) {
        return false;
    }
    else {
        return await response.json();
    }
}


async function CallApi(url, method, body) {
    let authInfo = await GetAccessToken();
    if(authInfo === null) {
        await ClearAuthInfo();
        return;
    }

    if (!method)
        method = "GET";
        
    let response = await fetch(url, {
        method: method,
        body: body ? JSON.stringify(body) : null,
        credentials: 'include',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${authInfo.auth.access_token}`
        }
    });

    console.log('------------------------------------');
    console.log(response.status, response.url);
    console.log('------------------------------------');

    if (response.status == 401) {
       await ClearAuthInfo();
       return;
    } 
    else if (response.status === 403) {
        let data={
            error: true,    
            message: "Bạn không có quyền truy cập trang này",
            status:403
        }
        return data;
    }
    else if (response.status === 500) {
        return false;
    }
    else {
        return await response.json();
    }
}

async function refreshToken(refreshToken) {
    var formData = new FormData();
    formData.append("grant_type", "refresh_token");
    formData.append("client_id", clientid);
    formData.append("client_secret", secret);
    formData.append("refresh_token", refreshToken);

    let url = `https://accounts.${authority}/connect/token`;
    
    let result = await fetch(url, {
        method: 'POST',
        body: formData
    });
    if (result.status == 200) {
        console.log("refresh token sucess");
        let json = await result.json();
        return json;
    } else if (result.status == 400) {
        await ClearAuthInfo();
        return null;
    }
    else
        throw new Error(`Error When Refresh Token Status ${result.status}`);
}

async function OpenAuth() {
    try {
        const authConfig = {
            issuer: `https://accounts.${authority}`,
            clientId: clientid,
            clientSecret: secret,
            scopes: ['offline_access', 'openid', 'profile', 'hac_api.read_users', 'email', 'org', 'userinfo', 'hr_api', 'im_api', 'ef_api', 'ta_api'],
            //redirectUrl: Constants.linkingUri,
            additionalParameters: {
                prompt: 'login',
                hideRegister:'true',
                // response_type:encodeURIComponent(response_type)
                // alwayslogin: 'true'
            }
        };
        const authState = await AppAuth.authAsync(authConfig)
        const userInfo = await getUserInfo(authState.accessToken);
        if (userInfo && userInfo.email && userInfo.sub) {
            const infoLogin = {
                auth: { ...authState, access_token: authState.accessToken },
                userInfo: { ...userInfo, fullName:userInfo.name }
            };
            await SetAuthInfo(infoLogin);
            return infoLogin;
        } else {
            return null
        }
    } catch (e) {
        console.log('error', e);
    }
}

async function getUserInfo(access_token) {
	const response = await fetch(`https://accounts.${authority}/connect/userinfo`, {
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json',
			Authorization: `Bearer ${access_token}`,
		}
	});
	if (response.status === 401 || response.status === 403) {
		return null;
	} else {
		return await response.json();
	}
}

async function __getUserInfo(accessToken) {
    const data = await fetch(`https://accounts.${authority}/connect/userinfo`, {
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${accessToken}`,
        },
    }).then(response => {
        if (response.status == 401) {
            return 401;
        }

        return response.json();
    }).catch((error) => {
        //console.log('Error: ', error);
        return null
    });

    return data || {};
}

// function getUserInfo() {
//     return CallApi(`https://accounts.${authority}/connect/userinfo`, "GET")
// }

async function HandleAuth(code) {
    var formData = new FormData();
    formData.append("grant_type", "authorization_code");
    formData.append("client_id", clientid);
    formData.append("client_secret", secret);
    formData.append("code", code);
    formData.append("redirect_uri", Constants.linkingUri);

    let url = `https://accounts.${authority}/connect/token`;

    let result = await fetch(url, {
        method: 'POST',
        body: formData
    });

    if (result.status == 200) {
        let json = await result.json();
        if (json && json.access_token) {
            let resultAuth = { created_time : new Date().getTime(), ...json};
            //let resultAuth = { created_time : new Date().getTime() - (3600 * 24 * 3 * 1000), ...json};
            const resultUserInfo = await __getUserInfo(resultAuth.access_token);
            if (resultUserInfo && resultUserInfo.email && resultUserInfo.sub) {
                const infoLogin = {
                    auth: { ...resultAuth },
                    userInfo: { ...resultUserInfo }
                }
                await SetAuthInfo(infoLogin);
                return infoLogin;
            }
        }

        throw new Error("error when request");
    }
    else
        throw new Error("error when request");
}

async function GetClientHQL(uri){
    const _auth = await GetAccessToken()
    return new ApolloClient({
        link: new HttpLink({
          uri: uri,
          headers: {
            Authorization:`Bearer ${_auth.auth.access_token}`
          },
          credentials:'include'
        }),
        cache: new InMemoryCache(),
        
        onError: (errorObj) => { console.log('errorObj', errorObj)},
      });
}

async function GetGraphQl(uri) {
    let _auth = await GetAccessToken()
        return new ApolloClient({
            headers: {
                Authorization: `Bearer ${_auth.auth.access_token}`
            },
            uri,
            onError: (errorObj) => { console.log('errorObj', errorObj)},
    });
}
async function renewToken(authInfo) {
    let token = authInfo.auth.refreshToken ? authInfo.auth.refreshToken : authInfo.auth.refresh_token
    let newAuthInfo = await refreshToken(token);
    if(!newAuthInfo)
        return null;
    //authInfo.auth = { created_time : new Date().getTime() - (3600 * 24 * 3 * 1000), ...newAuthInfo};
    authInfo.auth = { created_time : new Date().getTime(), ...newAuthInfo};
    await SetAuthInfo(authInfo);
    return authInfo;
}

async function PostFormAPI(url, formData) {

    let authInfo = await GetAccessToken();

    if(authInfo == null) {
        await ClearAuthInfo();
        return;
    }
    
    let response = await fetch(url, {
        method: "POST",
        body: formData ? formData : null,
        credentials: 'include',
        headers: {
            'Content-Type': 'multipart/form-data',
            'Authorization': `Bearer ${authInfo.auth.access_token}`
        }
    });

    if (response.status == 401 || response.status == 403) {
        await ClearAuthInfo();
        return;
    } else if (response.status === 500) {
        return false;
    }
    else {
        return await response.json();
    }
}
async function GetFileCallApi(url) {
    let authInfo = await GetAccessToken();

    if(authInfo == null) {
        await ClearAuthInfo();
        return;
    }
    let response = await fetch(url, {
        method:  "GET",
        credentials: 'include',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${authInfo.auth.access_token}`
        }
    });

    console.log('------------------------------------');
    console.log(response.status, response.url);
    console.log('------------------------------------');
    return await response;
}

export default {
    GetAuthInfo,
    SetAuthInfo,
    ClearAuthInfo,
    CallApi,
    OpenAuth,
    RegisterAuth,
    HandleAuth,
    GetGraphQl,
    getUserInfo,
    PostFormAPI,
    CallApi360,
    GetFileCallApi,
    getToken360,
    GetClientHQL,
    CallApiComment,
}