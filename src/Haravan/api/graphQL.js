import Configs from '../config';
import HaravanApi from './hrvapi';

import ApolloClient from "apollo-boost";
import gql from "graphql-tag";

const uri = `https://tasks.${Configs.apiHost}/api/graphql`;

async function getClient() {
    return await HaravanApi.GetClientHQL(uri);
}

export async function getComment(params) {
    let { id, preview, page, limit, reply_review, reply_page, reply_limit} = params
    try {
        const client = await getClient();
        let reslut = await client.query({
            variables:{},
            query:gql`
                {
                comments(token: "${id}", preview: ${preview}, page_index: ${page}, page_size: ${limit}) {
                    totalCount,
                    data {
                        id,
                        content,
                        attachments { name, url },
                        action_users { id, name, email, avartar },
                        action_count,
                        like_users { id, name, email, avartar },
                        like_count,
                        dislike_users { id, name, email, avartar },
                        dislike_count,
                        praise_users { id, name, email, avartar },
                        praise_count,
                        created_At,
                        updated_At,
                        created_By { id, name, avartar },
                        replies(preview: ${reply_review}, page_index: ${reply_page}, page_size: ${reply_limit}) {
                            totalCount,
                            data {
                                id,
                                content,
                                attachments { id, name, url },
                                action_users { id, name, email, avartar },
                                action_count,
                                like_users { id, name, email, avartar },
                                like_count,
                                dislike_users { id, name, email, avartar },
                                dislike_count,
                                praise_users { id, name, email, avartar },
                                praise_count,
                                created_At,
                                updated_At,
                                created_By { id, name },
                            }
                        }
                    }
                }
            }`
        });
        return reslut;
    } catch (e) {
        console.log('error')
    }
}

export async function getRepliesComment(token, commentId, page) {
    const client = await getClient();
        let reslut = await client.query({
            variables:{},
            query:gql`{
                replied_comments(token: "${token}", commentId: "${commentId}", preview: 1, page_index: ${page}, page_size: 10) {
                    totalCount,
                    data {
                        id,
                        content,
                        attachments { name , url, description },
                        action_users { id, name, email, avartar },
                        action_count,
                        like_users { id, name, email, avartar }
                        like_count,
                        dislike_users { id, name, email, avartar },
                        dislike_count,
                        praise_users { id,  name, email, avartar },
                        praise_count,
                        created_By { id, name, email, avartar },
                        created_At,
                        updated_At
                    }
                }
            }`
        });
        return reslut;
}
export async function actionComment(params) {
    const client = await getClient();
    try {
        let reslut = await client.mutate({
            mutation:gql`
            mutation{
                    ${params}
            }`
        })
        return reslut;
    } catch (e) {
        console.log('error', e);
    }
    
}
export async function addCommentNews(params) {
        let {token, ref_Url, content, topicName, moduleName, topic_Owner_Id,  topic_Follower_Ids, tagged_Ids, attachments} = params
        const client = await getClient();
        let attach = (attachments && (attachments.name)) ? `[{name:"${attachments.name}", url:"${attachments.url}", size:${attachments.size}}]` : `[]`
        try {
            let reslut = await client.mutate({
                mutation:gql`
                    mutation{
                        createComment(comment:{
                            token: "${token}", 
                            ref_Url: "${ref_Url}", 
                            content: "${content}", 
                            moduleName: "${moduleName}", 
                            ${tagged_Ids},
                            attachments:${attach}
                            }) {
                            id
                            }
                        }`
                })
            return reslut;
        } catch (e) {
            console.log('error', e);
        }
}

export async function addNewReplyComment(params){
    let { token, parentId, ref_Url, content, moduleName,tagged_Ids, attachments } = params;
    let attach = (attachments && (attachments.name)) ? `[{nanme:"${attachments.name}", url:"${attachments.url}", size:${attachments.size}}]` : `[]`
    const client = await getClient()
    try {
        let result = await client.mutate({
            mutation: gql`
                mutation {
                    replyComment(comment: { 
                            token: "${token}", 
                            parentId: "${parentId}", 
                            ref_Url: "${ref_Url}", 
                            content: "${content}", 
                            moduleName: "${moduleName}", 
                            ${tagged_Ids}, 
                            attachments: ${attach} 
                        }) {
                        id
                    }
                }`
        })
        return result;
    } catch (e) {
        console.log('=======>>>>>', e);
    }
    
}

export async function deleteComment(params){
    let { token,id } = params;
    const client = await getClient()
    try {
        let result = await client.mutate({
            mutation: gql`
                mutation {
                    deleteComment(comment: { 
                            token: "${token}", 
                            id: "${id}", 
                        })
                }`
        })
        return result;
    } catch (e) {
        console.log('error result', e);
    }
}