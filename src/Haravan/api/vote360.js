import Configs from '../config';
import HaravanApi from './hrvapi';


const uri = `https://${Configs.apiHost360}/api`


// Common
export async function getVoteMonth(month, tokent) {
    const result = await HaravanApi.CallApi360(`${uri}/vote?year_month_vote=${month}`,tokent);
    return result;
}

export async function settingLimit(tokent) {
    const result = await HaravanApi.CallApi360(`${uri}/vote/setting/limit/`,tokent);
    return result;
}

export async function general(tokent) {
    const result = await HaravanApi.CallApi360(`${uri}/vote/setting/general`,tokent);
    return result;
}

export async function voteManager(idVote, idDepart, idUer, tokent) {
    const result = await HaravanApi.CallApi360(`${uri}/vote-point?vote_id=${idVote}&department_id=${idDepart}&voter_id=${idUer}`,tokent);
    return result;
}

export async function voteEmploy(idVote, idDepart, tokent) {
    const result = await HaravanApi.CallApi360(`${uri}/vote-point?vote_id=${idVote}&department_id=${idDepart}`,tokent);
    return result;
}
//  /api/authentication/login-by-token
export async function getUserInfo(body) {
    const method = 'POST';
    const result = await HaravanApi.getToken360(`${uri}/authentication/login-by-token`, method,body);
    return result;
}
export async function votePoint(body, tokent) {
    const method = 'POST';
    const result = await HaravanApi.CallApi360(`${uri}/vote-point`,tokent, method, body );
    return result;
}

