import Configs from '../config';
import HaravanApi from './hrvapi';

const uri = `https://accounts.${Configs.apiHost}/api`;

// Common
export async function searchCommonUser(id) {
    const result = await HaravanApi.CallApi(`${uri}/common/users?ids=${id}`);
    return result;
}