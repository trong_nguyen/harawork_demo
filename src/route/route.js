import { createSwitchNavigator, createStackNavigator } from 'react-navigation';

import AuthLoadingScreen from '../components/authLoading';
import LoginStack from './components/login';
import AppScreen from './components/app';

export default createSwitchNavigator(
    {
        AuthLoading: AuthLoadingScreen,
        Login: LoginStack,
        App: AppScreen
    },
    {
        initialRouteName: 'AuthLoading',
    }
);