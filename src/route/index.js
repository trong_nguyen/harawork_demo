import {
    reduxifyNavigator,
    createReactNavigationReduxMiddleware,
    createNavigationReducer,
} from 'react-navigation-redux-helpers';
import { connect } from 'react-redux';

import AppNavigator from './route';

//const navReducer = createNavigationReducer(AppNavigator);
const navReducer = (state, action) => {
    const nextState = AppNavigator.router.getStateForAction(action, state);

    return nextState || state;
};


const middleware = createReactNavigationReduxMiddleware(
    "root",
    state => state.nav,
);

const App = reduxifyNavigator(AppNavigator, "root");
const mapStateToProps = (state) => ({
  state: state.nav,
});
const AppWithNavigationState = connect(mapStateToProps)(App);

export default AppWithNavigationState;
export {
    navReducer,
    middleware
}