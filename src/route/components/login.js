import { createStackNavigator } from 'react-navigation';
import LoginScreen from '../../components/login';

const LoginStack = createStackNavigator(
    {
        Login: LoginScreen
    },
    {
        headerMode: 'none'
    }
);

export default LoginStack;