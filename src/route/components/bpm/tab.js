import React, { Component } from 'react';
import { createMaterialTopTabNavigator } from 'react-navigation';
import TabSearchScreen from '../../../components/app/bpm/search/tab';
import { settingApp } from '../../../public';

const BPMTabSearch = createMaterialTopTabNavigator(
    {
        First: {
            screen: (props) => <TabSearchScreen
                maniNavigation={props.screenProps.maninNavigation}
            />,
            navigationOptions: {
                tabBarLabel: 'Phiếu của tôi'
            }
        },
        Second: {
            screen: (props) => <TabSearchScreen
                maniNavigation={props.screenProps.maninNavigation}
            />,
            navigationOptions: {
                tabBarLabel: 'Phiếu cần duyệt'
            }
        },
        Third: {
            screen: (props) => <TabSearchScreen
                maniNavigation={props.screenProps.maninNavigation}
            />,
            navigationOptions: {
                tabBarLabel: 'Phiếu cần\nhoàn tất'
            }
        }
    },
    {
        tabBarOptions: {
            activeTintColor: settingApp.color,
            inactiveTintColor: settingApp.colorText,
            labelStyle: {
                fontSize: 10
            },
            style: {
                backgroundColor: '#ffffff'
            },
            indicatorStyle: {
                borderColor: settingApp.color,
                borderWidth: 2
            }
        },
        lazy: false
    }
);

export default BPMTabSearch;