import React, { Component } from 'react';
import { Text } from 'react-native';
import { createMaterialTopTabNavigator } from 'react-navigation';

import ListScreen from '../../../components/app/bpm/list';
import ListGroupScreen from '../../../components/app/bpm/listGroup';

import Title from '../../../components/app/bpm/components/title';
import { settingApp } from '../../../public';

const CheckInStack = createMaterialTopTabNavigator(
    {
        First: {
            screen: createMaterialTopTabNavigator({
                List: {
                    screen: (props) => <ListScreen
                        api={props.screenProps.first.api}
                        keyStore={props.screenProps.first.key}
                        propsApp={props.screenProps.propsApp}
                        navigation={props.navigation}
                    />,
                    navigationOptions: {
                        tabBarVisible: false
                    }
                },
                Gruop: {
                    screen: (props) => <ListGroupScreen
                        api={props.screenProps.first.api}
                        keyStore={props.screenProps.first.key}
                        propsApp={props.screenProps.propsApp}
                        navigation={props.navigation}
                    />,
                    navigationOptions: {
                        tabBarVisible: false
                    }
                }
            },
                {
                    lazy: true,
                    swipeEnabled: false,
                    gesturesEnabled: false,
                }
            ),
            navigationOptions: ({ screenProps }) => {
                const { first, second } = screenProps;
                const { title, key } = first;
                return {
                    swipeEnabled: second ? true : false,
                    tabBarLabel: (props) => <Title title={title} keyCount={key} navigation={props.navigation} />
                }
            }
        },
        Second: {
            screen: createMaterialTopTabNavigator({
                List: {
                    screen: (props) => {
                        const { screenProps } = props;
                        return screenProps.second ? <ListScreen
                            api={props.screenProps.second.api}
                            keyStore={props.screenProps.second.key}
                            propsApp={props.screenProps.propsApp}
                            navigation={props.navigation}
                        /> : null
                    },
                    navigationOptions: {
                        tabBarVisible: false
                    }
                },
                Gruop: {
                    screen: (props) => {
                        const { screenProps } = props;
                        return screenProps.second ? <ListGroupScreen
                            api={props.screenProps.second.api}
                            keyStore={props.screenProps.second.key}
                            propsApp={props.screenProps.propsApp}
                            navigation={props.navigation}
                        /> : null
                    },
                    navigationOptions: {
                        tabBarVisible: false
                    }
                }
            },
                {
                    lazy: true,
                    swipeEnabled: false,
                    gesturesEnabled: false,
                }
            ),
            navigationOptions: ({ screenProps }) => {
                const { first, second } = screenProps;
                const { title, key } = first;
                return {
                    swipeEnabled: second ? true : false,
                    tabBarLabel: (props) => <Title title={title} keyCount={key} navigation={props.navigation} />
                }
            },
            navigationOptions: ({ screenProps }) => {
                const { second } = screenProps;
                return {
                    swipeEnabled: second ? true : false,
                    tabBarOnPress: ({ defaultHandler }) => second ? defaultHandler() : null,
                    tabBarLabel: (props) => second ? <Title title={second.title} keyCount={second.key} navigation={props.navigation} /> : <Text></Text>
                }
            }
        }
    },
    {
        tabBarOptions: {
            style: {
                backgroundColor: '#ffffff'
            },
            indicatorStyle: {
                borderColor: settingApp.color,
                borderWidth: 2
            }
        },
        lazy: true
    }
)

export default CheckInStack;