import StackViewStyleInterpolator from "react-navigation-stack/dist/views/StackView/StackViewStyleInterpolator";
const listModal = [
    'CreateLeave', 
    'UpdateLeave', 
    'CreateNewWorks', 
    'CreateLable', 
    'AddLabels', 
    'SearchUserMail',
    'CreateGroup',
    'RemoveGroup',
];
const transitionConfig = () => ({
    screenInterpolator: sceneProps => {
        const { scenes } = sceneProps;
        if (scenes.length > 1) {
            if (listModal.indexOf(scenes[scenes.length - 1].route.routeName) > -1) {
                return StackViewStyleInterpolator.forVertical(sceneProps);
            } else {
                return StackViewStyleInterpolator.forHorizontal(sceneProps);
            }
        } else {
            return StackViewStyleInterpolator.forHorizontal(sceneProps);
        }
    }
});

// const transitionConfig = () => ({
//     screenInterpolator: sceneProps => {
//         const { layout, position, scene } = sceneProps;
//         const { index } = scene;

//         if (listModal.indexOf(scene.route.routeName) > -1) {
//             const height = layout.initHeight;
//             const translateY = position.interpolate({
//                 inputRange: [index - 1, index, index + 1],
//                 outputRange: [height, 0, 0],
//             });

//             const opacity = position.interpolate({
//                 inputRange: [index - 1, index - 0.99, index, index + 0.99, index + 1],
//                 outputRange: [0, 1, 1, 0.3, 0]
//             });

//             return { opacity, transform: [{ translateY }] };
//         } else {
//             const translateX = position.interpolate({
//                 inputRange: [index - 1, index, index + 1],
//                 outputRange: [layout.initWidth, 0, 0]
//             });

//             const opacity = position.interpolate({
//                 inputRange: [index - 1, index - 0.99, index, index + 0.99, index + 1],
//                 outputRange: [0, 1, 1, 0.3, 0]
//             });
//             return { opacity, transform: [{ translateX }] };
//         }
//     }
// });

export default transitionConfig;