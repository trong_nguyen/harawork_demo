import { createStackNavigator } from 'react-navigation';
import transitionConfig from '../transition';

import IndexStackScreen from '../indexScreen';

//checkInUser
import DetailCheckInScreen from '../../../components/app/checkinUser/screen/detailCheckIn';

//Notifi
import NotifiDetailScreen from '../../../components/app/notifi/notifiDetail';
import NotifiSearchScreen from '../../../components/app/notifi/notifiSearch';

//bpm
import BPMDetailFormScreen from '../../../components/app/bpm/detail';
import BPMDetailFormCommentScreen from '../../../components/app/bpm/comment';
import BPMSearchScreen from '../../../components/app/bpm/search/main';
import BPMSearchSelectItemScreen from '../../../components/app/bpm/search/selectItem';
import BPMSearchSelectMutiItemScreen from '../../../components/app/bpm/search/selectMutiItem';

//leave
import DetailFormLeaveScreen from '../../../components/app/user/leaves/screen/detailFromLeave';
import DetailInfoLeaveScreen from '../../../components/app/user/leaves/screen/detailInfoLeave';
import CreateLeaveScreen from '../../../components/app/user/leaves/createLeave';
import UpdateLeaveScreen from '../../../components/app/user/leaves/updateLeave';
import CreateLeaveItemSelectScreen from '../../../components/app/user/leaves/screen/createLeaveItemSelect';
import CreateLeaveSearchScreen from '../../../components/app/user/leaves/screen/createLeaveSearch';
import CreateLeaveTypeLeaveScreen from '../../../components/app/user/leaves/screen/createLeaveTypeLeave'

//approved
import ApprovedLeaveDetailScreen from '../../../components/app/user/employeeManager/approvedLeaves/approvedLeaveDetail';
import ApprovedListScreen from '../../../components/app/user/employeeManager/approvedWork/worksList';
import ApprovedDetailScreen from '../../../components/app/user/employeeManager/approvedWork/approDetail';
import CreateNewWorksScreen from '../../../components/app/user/employeeManager/approvedWork/createNewWorks'
import SelectDepartScreen from '../../../components/app/user/employeeManager/approvedWork/createNewWorks/component/screen/departments'
import SelectEmployeeScreen from '../../../components/app/user/employeeManager/approvedWork/createNewWorks/component/screen/employeee'
import ShiftScreen from '../../../components/app/user/employeeManager/approvedWork/createNewWorks/component/screen/shift';
import FrameWorksScreen from '../../../components/app/user/employeeManager/approvedWork/createNewWorks/component/screen/frameWorks';

//regist
import RegistFrameDetailSCreen from '../../../components/app/user/registerWorks/components/framesDetails'

//public
import ItemSelectScreen from '../../../components/app/public/itemSelect';
import ListNewScreen from '../../../components/app/public/listNew';
import DetailNewScreen from '../../../components/app/public/detailNew';

//Mail
import CreateLableScreen from '../../../components/app/mail/mailList/components/createLables';
import SearchMailScreen from '../../../components/app/mail/mailSearch';
import MailDetailScreen from '../../../components/app/mail/mailDetail';
import AddLabelsScreen from '../../../components/app/mail/addLabels';
import MailWriteScreen from '../../../components/app/mail/mailWrite';
import SeacrhUserMailScreen from '../../../components/app/mail/mailWrite/components/searchUser';
import DetailSeenMail from '../../../components/app/mail/detailSeen';
import SearchEmployee from '../../../components/app/mail/detailSeen/component/search';

//VotePoint
import ListMonthVoteScreen from '../../../components/app/user/votePoint360/components/listMonthVote';
import ListDepartScreen from '../../../components/app/user/votePoint360/components/listDepart';

import NotFoundScreen from '../../../components/app/notFound';

import WebNotificationScreen from '../../../public/components/webNotifications';

//Discuss
import DetailCommentScreen from '../../../components/app/discuss/screen/detailComment';
import ReplyUserScreen from '../../../components/app/discuss/screen/detailComment/replyUser';
import DetailGroupScreen from '../../../components/app/discuss/screen/detailGroup';
import CreateGroopsSCreen from '../../../components/app/discuss/screen/createGroups';
import ChoiceAvatarScreen from '../../../components/app/discuss/screen/createGroups/component/choiceAvatar';
import TypeGroupScreen from '../../../components/app/discuss/screen/createGroups/component/typeGroups';
import PermissionGroupScreen from '../../../components/app/discuss/screen/createGroups/component/permissionGroups';
import DescriptionScreen from '../../../components/app/discuss/screen/createGroups/component/description';
import SearchUserGroupScreen from '../../../components/app/discuss/public/searchUser';
import SearchAndroidScreen from '../../../components/app/discuss/public/searchAndroid';
import RemoveGroupScreen from '../../../components/app/discuss/group/components/content/screenTab/components/removeGroups';
import CreateThreadScreen from '../../../components/app/discuss/screen/createThread';
import NewsScreen from '../../../components/app/discuss/news';
import SettupHomeScreen from '../../../components/app/discuss/screen/settupToHome';
import DetailVotePollScreen from '../../../components/app/discuss/screen/detailComment/component/detailVote'
import DetailUserVoteScreen from '../../../components/app/discuss/screen/detailComment/detailUserVote'

//Contact
import ContactDetailScreen from '../../../components/app/user/contact/contactDetail';
import ListOptionScreen from '../../../components/app/user/contact/listContact/component/listOption';

//Salary
import DetailSalaryScreen from '../../../components/app/user/salary/detailSalary';
import ListPeriodsScreen from '../../../components/app/user/salary/meSalary/components/listPeriods';
import DetailFomulasScreen from '../../../components/app/user/salary/detailFomulas';
import SalaryPerDayScreen from '../../../components/app/user/salary/salaryPerDay';
import DetailSalaryPerdayScreen from '../../../components/app/user/salary/detailSalaryPerday';

const AppStack = createStackNavigator(
    {
        IndexStack: { screen: IndexStackScreen, navigationOptions: { header: null } },

        DetailCheckIn: { screen: DetailCheckInScreen },

        NotifiDetail: { screen: NotifiDetailScreen },
        NotifiSearch: { screen: NotifiSearchScreen },

        BPMDetailForm: { screen: BPMDetailFormScreen },
        BPMDetailFormComment: { screen: BPMDetailFormCommentScreen },
        BPMSearch: { screen: BPMSearchScreen },
        BPMSearchSelectItem: { screen: BPMSearchSelectItemScreen },
        BPMSearchSelectMutiItem: { screen: BPMSearchSelectMutiItemScreen },

        DetailFormLeave: { screen: DetailFormLeaveScreen },
        DetailInfoLeave: { screen: DetailInfoLeaveScreen },
        CreateLeave: { screen: CreateLeaveScreen, navigationOptions: { gesturesEnabled: false } },
        UpdateLeave: { screen: UpdateLeaveScreen, navigationOptions: { gesturesEnabled: false } },
        CreateLeaveItemSelect: { screen: CreateLeaveItemSelectScreen },
        CreateLeaveSearch: { screen: CreateLeaveSearchScreen },
        CreateLeaveTypeLeave: { screen: CreateLeaveTypeLeaveScreen },

        ApprovedLeaveDetail: { screen: ApprovedLeaveDetailScreen},
        ApprovedList: { screen: ApprovedListScreen },
        ApprovedDetail: { screen: ApprovedDetailScreen},
        CreateNewWorks: { screen: CreateNewWorksScreen, navigationOptions: { gesturesEnabled: true } },
        DepartDetail: { screen: SelectDepartScreen },
        EmployeeDetail: { screen: SelectEmployeeScreen},
        ShiftDetail: { screen: ShiftScreen },
        FrameWorks: { screen: FrameWorksScreen },

        RegistFrameDetail: { screen: RegistFrameDetailSCreen},

        CreateLable:{ screen: CreateLableScreen},
        SearchMail:{ screen: SearchMailScreen},
        MailDetail:{screen: MailDetailScreen},
        AddLabels:{screen: AddLabelsScreen },

        ItemSelect: { screen: ItemSelectScreen },
        ListNew: { screen: ListNewScreen },
        DetailNew: { screen: DetailNewScreen },
        MailWrite:{ screen: MailWriteScreen },
        SearchUserMail:{ screen: SeacrhUserMailScreen},
        DetailSeenMail:{ screen: DetailSeenMail },
        SearchEmployee:{screen:SearchEmployee},

        ListMontVote:{screen:ListMonthVoteScreen},
        ListDepart:{screen:ListDepartScreen},

        NotFound: { screen: NotFoundScreen },
        WebNotifications:{ screen: WebNotificationScreen },

        DetailComment: { screen: DetailCommentScreen},
        ReplyUser:{ screen: ReplyUserScreen},
        DetailGroup:{ screen: DetailGroupScreen},
        CreateGroup:{ screen: CreateGroopsSCreen},
        ChoiceAvatar:{ screen: ChoiceAvatarScreen},
        TypeGroup:{ screen: TypeGroupScreen},
        PermissionGroups:{ screen: PermissionGroupScreen},
        Description:{ screen: DescriptionScreen},
        SearchUserGroup:{ screen: SearchUserGroupScreen},
        SearchAndroid:{ screen:SearchAndroidScreen },
        RemoveGroup:{screen:RemoveGroupScreen},
        CreateThread:{ screen:CreateThreadScreen },
        NewList :{screen:NewsScreen},
        SettupToHome: { screen:SettupHomeScreen },
        DetailVotePoll:{ screen:DetailVotePollScreen },
        DetailUserVote:{ screen:DetailUserVoteScreen },

        ContactDetail:{ screen:ContactDetailScreen},
        ListOption:{ screen:ListOptionScreen},

        DetailSalary:{ screen:DetailSalaryScreen},
        ListPeriods:{ screen:ListPeriodsScreen },
        DetailFomulas:{ screen: DetailFomulasScreen},
        SalaryPerDay:{screen: SalaryPerDayScreen},
        DetailSalaryPerday:{screen: DetailSalaryPerdayScreen},
    },
    {
        headerMode: 'none',
        transitionConfig
    }
);

export default AppStack;