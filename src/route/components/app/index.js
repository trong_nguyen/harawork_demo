import { createStackNavigator } from 'react-navigation';
import AppContainerScreen from './appStack';

const AppStack = createStackNavigator(
    {
        AppContainer: { screen: AppContainerScreen }
    },
    {
        headerMode: 'none'
    }
);


export default AppStack;