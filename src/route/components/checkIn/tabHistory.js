import React, { Component } from 'react';
import { createMaterialTopTabNavigator } from 'react-navigation';
import TabListScreen from '../../../components/app/checkinUser/history/tabList';
import { settingApp } from '../../../public';

const CheckInTabHistory = createMaterialTopTabNavigator(
    {
        First: {
            screen: (props) => <TabListScreen
                url={`checkin/history?`}
                condition={props.screenProps.condition}
                mainNavigation={props.screenProps.mainNavigation}
                {...props}
            />,
            navigationOptions: (props) => {
                const count = props.navigation.getParam('count', 0);
                return {
                    tabBarLabel: `Tất cả (${count})`
                }
            }
        },
        Second: {
            screen: (props) => <TabListScreen
                url={`checkin/history?statuses=1&statuses=2`}
                condition={props.screenProps.condition}
                mainNavigation={props.screenProps.mainNavigation}
                {...props}
            />,
            navigationOptions: (props) => {
                const count = props.navigation.getParam('count', 0);
                return {
                    tabBarLabel: `Chưa duyệt (${count})`
                }
            }
        },
        Third: {
            screen: (props) => <TabListScreen
                url={`checkin/history?statuses=4`}
                condition={props.screenProps.condition}
                mainNavigation={props.screenProps.mainNavigation}
                {...props}
            />,
            navigationOptions: (props) => {
                const count = props.navigation.getParam('count', 0);
                return {
                    tabBarLabel: `Từ chối (${count})`
                }
            }
        }
    },
    {
        tabBarOptions: {
            activeTintColor: '#ffffff',
            inactiveTintColor: settingApp.color,
            labelStyle: {
                marginTop: 1,
                fontSize: 12,
                width: (((settingApp.width) - 30) / 3)
            },
            tabStyle: {
                borderRightColor: settingApp.color,
                borderRightWidth: 1,
                width: (((settingApp.width) - 30) / 3)
            },
            style: {
                width: ((settingApp.width) - 29),
                marginLeft: 15,
                height: 35,
                backgroundColor: '#ffffff',
                borderLeftWidth: 1,
                borderLeftColor: settingApp.color,
                borderTopWidth: 1,
                borderTopColor: settingApp.color,
                borderBottomWidth: 1,
                borderBottomColor: settingApp.color
            },
            indicatorStyle: {
                borderColor: settingApp.color,
                borderWidth: 30
            },
            upperCaseLabel: false,
        },
        lazy: false,
        swipeEnabled: false
    }
);

export default CheckInTabHistory;