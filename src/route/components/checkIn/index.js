import React, { Component } from 'react';
import { Image } from 'react-native';

import { createBottomTabNavigator } from 'react-navigation';
import { settingApp, imgApp, IconTabNotCheckIn, SvgCP } from '../../../public';
import CheckInScreen from '../../../components/app/checkinUser/checkIn';
import HistoryScreen from '../../../components/app/checkinUser/history';
import NotCheckInScreen from '../../../components/app/checkinUser/notCheckIn';


const CheckInStack = createBottomTabNavigator(
    {
        CheckInTab: {
            screen: CheckInScreen,
            navigationOptions: {
                tabBarLabel: 'Chấm công',
                tabBarIcon: ({ focused }) => <SvgCP.checkInTab focused={focused}/>
            }
        },
        HistoryTab: {
            screen: HistoryScreen,
            navigationOptions: {
                tabBarLabel: 'Lịch sử chấm công',
                tabBarIcon: ({ focused }) => <SvgCP.historyTab focused={focused}/>
            }
        },
        NotCheckIn: {
            screen: NotCheckInScreen,
            navigationOptions: {
                tabBarLabel: 'Chưa chấm công',
                tabBarIcon: ({ focused }) => (
                    <IconTabNotCheckIn focused={focused} />
                )
            }
        }
    }, {
        initialRouteName: 'CheckInTab',
        tabBarOptions: {
            activeTintColor: settingApp.color,
            inactiveTintColor: '#C7C7C7',
            labelStyle: {
                fontSize: 10,
            },
            style: {
                backgroundColor: '#ffffff',
                ...settingApp.shadow
            },
        }
    }
);

export default CheckInStack;