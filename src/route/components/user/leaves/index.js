import React, { Component } from 'react';

import { createBottomTabNavigator } from 'react-navigation';
import { settingApp, SvgCP } from '../../../../public';
import DetailLeaveScreen from '../../../../components/app/user/leaves/detailLeaves';
import HistoryLeaveScreen from '../../../../components/app/user/leaves/historyLeaves';

const LeaveStack = createBottomTabNavigator(
    {
        DetailLeave: {
            screen: DetailLeaveScreen,
            navigationOptions: {
                tabBarLabel: 'Phép còn lại',
                tabBarIcon: ({ focused }) => <SvgCP.tabLeaveList focused={focused} />
            }
        },
        HistoryLeave: {
            screen: HistoryLeaveScreen,
            navigationOptions: {
                tabBarLabel: 'Phiếu nghỉ phép',
                tabBarIcon: ({ focused }) => <SvgCP.tabLeaveHistory focused={focused} />
            }
        }
    }, {
        initialRouteName: 'DetailLeave',
        tabBarOptions: {
            activeTintColor: settingApp.color,
            inactiveTintColor: '#C7C7C7',
            labelStyle: {
                fontSize: 10,
            },
            style: {
                backgroundColor: '#ffffff',
                ...settingApp.shadow
            },
        }
    }
);

export default LeaveStack;