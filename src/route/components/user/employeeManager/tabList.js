import React, { Component } from 'react';
import { createMaterialTopTabNavigator } from 'react-navigation';
import { settingApp } from '../../../../public';
import TabListContent from '../../../../components/app/user/employeeManager/approvedLeaves/component/tabList';

const TabList = createMaterialTopTabNavigator({
    First: {
        screen:(props) => <TabListContent 
            url= {`supervisor/leaves?status=1`}
            condition = {props.screenProps.condition}
            collapsed={props.screenProps.collapsed}
            mainNavigation = {props.screenProps.mainNavigation}
            {...props}
        />,
        navigationOptions:(props) => {
            const count = props.navigation.getParam('count', 0);
            return {
                tabBarLabel: `Chưa duyệt (${count})`
            }
        }
    },
    Second: {
        screen:(props) => <TabListContent 
            url= {`supervisor/leaves?status=2`}
            condition = {props.screenProps.condition}
            collapsed={props.screenProps.collapsed}
            mainNavigation = {props.screenProps.mainNavigation}
            {...props}
        />,
        navigationOptions:(props) => {
            const count = props.navigation.getParam('count', 0);
            return {
                tabBarLabel: `Đã duyệt (${count})`
            }
        }
    },
    Third: {
        screen:(props) => <TabListContent 
            url= {`supervisor/leaves?status=3`}
            condition = {props.screenProps.condition}
            collapsed={props.screenProps.collapsed}
            mainNavigation = {props.screenProps.mainNavigation}
            {...props}
        />,
        navigationOptions:(props) => {
            const count = props.navigation.getParam('count', 0);
            return {
                tabBarLabel: `Từ chối (${count})`
            }
        }
    },
}, 
{
    tabBarOptions: {
        activeTintColor: '#ffffff',
        inactiveTintColor: settingApp.color,
        labelStyle: {
            marginTop: 1,
            fontSize: 12,
            width: (((settingApp.width) - 30) / 3)
        },
        tabStyle: {
            borderRightColor: settingApp.color,
            borderRightWidth: 1,
            width: (((settingApp.width) - 30) / 3)
        },
        style: {
            width: ((settingApp.width) - 29),
            marginLeft: 15,
            height: 35,
            backgroundColor: '#ffffff',
            borderLeftWidth: 1,
            borderLeftColor: settingApp.color,
            borderTopWidth: 1,
            borderTopColor: settingApp.color,
            borderBottomWidth: 1,
            borderBottomColor: settingApp.color,
            ...settingApp.shadow
        },
        indicatorStyle: {
            borderColor: settingApp.color,
            borderWidth: 30
        },
        upperCaseLabel: false,
    },
    lazy: false,
    swipeEnabled: false,
}
)
export default TabList;