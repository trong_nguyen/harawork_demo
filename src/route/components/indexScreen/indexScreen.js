import { createStackNavigator, createSwitchNavigator } from 'react-navigation';
import HomeStackScreen from './indexStack';

const IndexStack = createSwitchNavigator(
    {
        HomeStack: { screen: HomeStackScreen }
    }, {
        headerMode: 'none'
        
    }
);


export default IndexStack;