import React, { Component } from 'react';

import { createDrawerNavigator, createStackNavigator } from 'react-navigation';
import { settingApp } from '../../../public';

import IndexStackScreen from './indexScreen';
import DrawerStack from '../../../components/app/drawer';
import Content from '../../../components/content'
const HomeStackScreen = createStackNavigator(
    {
        Content: { screen: Content },
    },
    {
        headerMode: 'none',
    }
    // {
    //     drawerWidth: (settingApp.width * 0.8),
    //     drawerPosition: 'left',
    //     drawerBackgroundColor: '#ffffff',// '#15202B' '#313F4D',
    //     contentComponent: props => <DrawerStack {...props} />
    // }
);

export default HomeStackScreen;

