import React, { Component } from 'react';
import { createSwitchNavigator } from 'react-navigation';
import transitionConfig from '../transition';

import CheckInStack from '../checkIn';
import LeaveStack from '../user/leaves';
import HomeScreen from '../../../components/app/home';
import CalendarJobScreen from '../../../components/app/user/calendarJob';
import NotifiListScreen from '../../../components/app/notifi/notifiList';
import ApprovedWorkScreen from '../../../components/app/user/employeeManager/approvedWork';
import ApprovedLeavesScreen from '../../../components/app/user/employeeManager/approvedLeaves';
import RegisterWorksScreen from '../../../components/app/user/registerWorks';

import ContentScreen from '../../../components/content'
//mail
import MailListScreen from '../../../components/app/mail/mailList'

//VotePoint360
import VotePoint360Screen from '../../../components/app/user/votePoint360'

import BPMListTabScreen from '../../../components/app/bpm/listTab';

//DiscussScreen 
import DiscussScreen from '../discuss'

//Contact

import ContactScreen from '../../../components/app/user/contact/listContact'

//Salary
import MeSalaryScreen from '../../../components/app/user/salary/meSalary'

const IndexSctackScreen = createSwitchNavigator(
    {   
        Content: { screen: ContentScreen, navigationOptions: { gesturesEnabled: false, header: null  } },
        Home: { screen: HomeScreen, navigationOptions: { gesturesEnabled: false , header: null } },
        CalendarJob: { screen: CalendarJobScreen, navigationOptions: { gesturesEnabled: false } },
        CheckIn: { screen: CheckInStack, navigationOptions: { gesturesEnabled: false, header: null  } },
        Leaves: { screen: LeaveStack, navigationOptions: { gesturesEnabled: false } },
        NotifiList: { screen: NotifiListScreen, navigationOptions: { gesturesEnabled: false } },

        ApprovedWork: { screen: ApprovedWorkScreen, navigationOptions:{ gesturesEnabled: false} },
        ApprovedLeaves:{ screen: ApprovedLeavesScreen, navigationOptions:{ gesturesEnabled:false} },

        RegisterWorks:{ screen: RegisterWorksScreen, navigationOptions:{ gesturesEnabled:false}},
        
        MailList: { screen: MailListScreen, navigationOptions:{ gesturesEnabled:false}},

        BPMListTabIndex: { screen: (props) => <BPMListTabScreen {...props} />, navigationOptions: { gesturesEnabled: false } },
        BPMListTabFormMe: { screen: (props) => <BPMListTabScreen {...props} />, navigationOptions: { gesturesEnabled: false } },
        BPMListTabFormWaitingApprove: { screen: (props) => <BPMListTabScreen {...props} />, navigationOptions: { gesturesEnabled: false } },
        BPMListTabFormWaitingComplete: { screen: (props) => <BPMListTabScreen {...props} />, navigationOptions: { gesturesEnabled: false } },
        BPMListTabFormTrack: { screen: (props) => <BPMListTabScreen {...props} />, navigationOptions: { gesturesEnabled: false } },
    
        VotePoint360:{screen: VotePoint360Screen, navigationOptions:{ gesturesEnabled:false}},
        Discuss:{screen: DiscussScreen, navigationOptions:{ gesturesEnabled:false}},

        Contact:{screen:ContactScreen, navigationOptions:{ gesturesEnabled:false}},

        MeSalary:{screen:MeSalaryScreen, navigationOptions:{ gesturesEnabled:false}}
    },
    {
        headerMode: 'none',
        transitionConfig
    }
);

export default IndexSctackScreen;