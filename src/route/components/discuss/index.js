import React, { Component } from 'react';
import { Image } from 'react-native';

import { createMaterialTopTabNavigator } from 'react-navigation';
import { settingApp, imgApp, IconTabNotCheckIn, SvgCP } from '../../../public';
import TopicScreen from '../../../components/app/discuss/topic';
import GroupScreen from '../../../components/app/discuss/group'
import NewsScreen from '../../../components/app/discuss/news';
import RateScreen from '../../../components/app/discuss/rate'

const DiscussStack = createMaterialTopTabNavigator(
    {
        TopicTab:{
            screen: TopicScreen,
            navigationOptions:{
                    tabBarLabel: 'Chủ đề',
                    tabBarIcon: ({ focused }) => <SvgCP.topicIcon focused={focused}/>
            }
        },
        GroupTab: {
            screen: GroupScreen,
            navigationOptions: {
                tabBarLabel: 'Nhóm',
                tabBarIcon: ({ focused }) => <SvgCP.groupIcon focused={focused}/>
            }
        },
        Rating: {
            screen: RateScreen,
            navigationOptions: {
                tabBarLabel: 'Xếp hạng',
                tabBarIcon: ({ focused }) => (
                    <SvgCP.icon_Start focused={focused}/>
                )
            }
        }
    }, {
        animationEnabled:false,
        tabBarPosition:'bottom',
        swipeEnabled: false,
        tabBarOptions: {
            showIcon:true,
            activeTintColor: '#0A1F44',
            inactiveTintColor: '#C7C7C7',
            paddingBottom: -10,
            labelStyle: {
                fontSize: 12,
                fontWeight:'bold'
            },
            style: {
                height:((settingApp.isIPhoneX == true) ? 70 : 60),
                paddingLeft:30,
                paddingRight:30,
                backgroundColor: '#FDFBFB',
                paddingBottom:-10,
                ...settingApp.shadow
            },
            indicatorStyle: {
                borderBottomColor:'#FDFBFB',
                borderBottomWidth:4,
            },
            upperCaseLabel:false
        }
    }
);

export default DiscussStack;