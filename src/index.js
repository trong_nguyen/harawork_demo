import React, { Component } from 'react';
import { AppState, Alert, BackHandler, View, AsyncStorage } from 'react-native';
import AppWithNavigationState from './route';
import { Provider } from 'react-redux';
import { ToastComponent, BoxNotify, NetWork, Utils, keyStore } from './public';
import { NavigationActions } from 'react-navigation';
import store from './state/store';
import { Updates } from 'expo';

class AppComponent extends Component {
    constructor(props) {
        super(props);

        this.appState = AppState.currentState;
        this.timeOut = null;
        this.timeIn = null
    }

    componentDidMount() {
        //this.updateApp();
        AppState.addEventListener('change', nextAppState => this.handleAppStateChange(nextAppState));
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }

    async handleAppStateChange(nextAppState) {
        if (this.appState.match(/inactive|background/) && nextAppState === 'active') {
            //this.updateApp();
            this.getTimeIn()
        }
        else if(this.appState.match(/active/) && (nextAppState === 'background')){
            this.getTimeOut()
        }
        this.appState = nextAppState;
    }

    getTimeOut(){
        this.timeOut = new Date().getTime()
        let data = {
            timeOut:this.timeOut
        }
        try {
            AsyncStorage.setItem(keyStore.timerApp, JSON.stringify(data))
        } catch (e) {
            console.log('error Async', e);
        }
    }

    async getTimeIn(){
        this.timeIn = new Date().getTime()
        try {
            const resTime = await AsyncStorage.getItem(keyStore.timerApp)
            if(resTime){
                let dataTime = JSON.parse(resTime)
                let timeOut = this.timeIn - (dataTime.timeOut *1) 
                if(timeOut >= 600000){
                    Updates.reloadFromCache()
                }
                else{
                    AsyncStorage.removeItem(keyStore.timerApp)
                }
            }
        } catch (e) {
            console.log('error Async getItem', e);
        }
    }

    async updateApp() {
        try {
            const update = await Updates.checkForUpdateAsync();
            if (update.isAvailable) {
                await Updates.fetchUpdateAsync();
                Alert.alert(
                    'Cập nhật phiên bản mới',
                    'Quá trình cập nhật hoàn tất, vui lòng cài đặt.',
                    [
                        { text: 'Huỷ', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                        { text: 'Đồng ý', onPress: () => Updates.reloadFromCache() },
                    ],
                    { cancelable: false }
                )
            }
        } catch (e) {
            // handle or log error
            console.log('Error: ', e);
        }
    }

    onBackPress() {
        const { dispatch, getState } = store;
        const stateApp = getState();
        const { nav } = stateApp;
        const { index } = nav;
        let isBack = false;
        switch (index) {
            case 2:
                isBack = nav.routes[index].routes[0].routes.length < 2 ? false : true;
                break;
            default:
                break;
        }
        if (!isBack) {
            return false;
        }
        dispatch(NavigationActions.back());
        return true;
    };

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Provider store={store}>
                    <View style={{ flex: 1 }}>
                        <NetWork store={store}>
                            <AppWithNavigationState />
                        </NetWork>
                        <BoxNotify />
                        <ToastComponent />
                    </View>
                </Provider>
            </View>
        )
    }
}

export default AppComponent;